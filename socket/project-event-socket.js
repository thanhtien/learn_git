
module.exports = function (socket,io) {

    socket.on( 'public_project', function( data ) {
        var dataBack = JSON.parse(data);
        io.sockets.emit( dataBack.room, {
            data: dataBack
        });
    });

    socket.on( 'donate_project', function( data ) {
        var dataBack = JSON.parse(data);
        io.sockets.emit( dataBack.room, {
            data: dataBack
        });
    });

    socket.on( 'annouce_expired', function( data ) {
        var dataBack = JSON.parse(data);
        io.sockets.emit( dataBack.room, {
            data: dataBack
        });
    });

    socket.on( 'annouce_enough_goal_amount', function( data ) {
        var dataBack = JSON.parse(data);
        io.sockets.emit( dataBack.room, {
            data: dataBack
        });
    });


};
