module.exports = function (socket,io) {
  //Join Room User
  socket.on('join-room', function (data) {
    socket.join(data);
    socket.room=data;
 });
 // Listen Client Chat
 socket.on("user-chat", function(data){
   var room = `comment_project_${data.project_id}`;
   socket.broadcast.to(room).emit("server-chat", data);
  });
  //Listen Client Chat Reply
  socket.on("user-chat-reply", function(data){
    var room = `comment_project_${data.project_id}`;
    socket.broadcast.to(room).emit("server-chat-reply", data);
   });
   //listen User Typing
   socket.on("user-typing", function(data){
     var room = `comment_project_${data.project_id}`;
     socket.broadcast.to(room).emit("server-typing", data.status);
    });


  //User Leave Project
  socket.on("leave-room", function(data){
    socket.leave(data);
  });


};
