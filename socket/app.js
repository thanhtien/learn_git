var socket  = require( 'socket.io' );
var express = require('express');
var app     = express();
var server  = require('http').createServer(app);
var io      = socket.listen( server );
var port    = process.env.PORT || 3000;
var socketController = require('./event-socket');
server.listen(port, function () {
  console.log('Server listening at port %d', port);
});



io.on('connection', function (client) {
  console.log('a user connected');
  socketController(client)
});

 
