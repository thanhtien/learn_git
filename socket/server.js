var socket  = require( 'socket.io' );
var express = require('express');
var app     = express();
var server  = require('http').createServer(app);
var io      = socket.listen( server );
var port    = 4001;
var socketController = require('./event-socket');
var commentController = require('./comment-event-socket');
var socketProjectController = require('./project-event-socket');
server.listen(port, function () {
  console.log('Server listening at port %d', port);
});

io.on('connection', function (socket) {
  socketController(socket,io);
  commentController(socket,io);
  socketProjectController(socket,io)
});
