# Cấu trúc của database :
```
http://prntscr.com/nchu49
```
# Ý nghĩa của các table như sau:
## address_user	:  lưu thông tin của user
## announce	: lưu thông tin thông báo
## backed_projects: lưu thông tin donate
## backing_levels	: lưu thông tin return
## info_backing_edit: : lưu thông tin yêu cầu chỉnh sửa return
## backing_levels_edit : lưu thông tin yêu cầu chỉnh sửa return
## categories	: lưu thông tin của category
## comment	:lưu comment
## expired_projects: lưu những project đã hết hạn được admin pickup để show lên hệ thống
## fanclub_projects: lưu những fanclub được admin pickup để show lên hệ thống
## favourite_projects: : lưu những được yêu thích được admin pickup để show lên hệ thống
## pickup_projects  : lưu các project mà admin đã pickup để hiển thị
## recommends_projects : lưu các dự án mà admin đã pickup để hiển thị
## groups		:hiển thị các quyền của một user trong hệ thống
## listuserdonate	: lưu  thông tin  user đã donate cho project nào
## listuserdonate_fanclub: lưu thông tin user donate cho return nào của project nào
```
⦁	status_join_fanclub : thể hiện user đó đang còn donate hay không ( 0: còn donate, 1: hết donate)
```
## news		:lưu các tin tức mà admin đăng
```
⦁	status  : public tin tức ( 0: chưa public, 1: public)
```
## notification_action : lưu các hoạt động mà user tác động đến các project(tạo project)
## notification_info : lưu các thông báo đến user
## reply_comment 	: lưu các reply của một comment
## report_daily	: lưu các thông báo mà owner thông báo
```
⦁	status  : public tin tức ( 0: chưa public, 1: public)
```
## setting		: lưu thông tin key của Stripe
## subscription	: lưu các email đã đăng ký
## users_groups 	: lưu quyền của từng user (admin, user)
## wish_list	: lưu các project mà user thích theo user_id và project_id
## users		: lưu thông tin của user
```
⦁	username	: tên user
⦁	email		: địa chỉ email của user
⦁	job 		: giới thiệu về bản thân, như nghề nghiệp hiện tại hoặc của trước đây của user
⦁	profileImageURL: image của user
⦁	active		: trạng thái của user( 0 : block , 1 : active)
⦁	eventProjectSocket : nhận thông báo của socket
```
## project		: lưu và hiển thị các thông tin về project
```
⦁	project_type	: ( 0: project normal, 1: fanclub)
⦁	project_name 	: tên của project
⦁	collection_start_date  : ngày bắt đầu dự án
⦁	collection_end_date  : ngày kết thúc dự án ( nếu project là fanclub thì = null)
⦁	thumbnail	: image chính của project
⦁	project_content	: bài viết giới thiệu về project đó
⦁	goal_amount 	: số tiền mục tiêu
⦁	collected_amount: số tiền đã có
```
