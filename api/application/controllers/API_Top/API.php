<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class API extends BD_Controller {
  function __construct()
  {
    // Construct the parent class
    parent::__construct();
    $this->load->database();
    $this->load->library(array('ion_auth', 'form_validation'));
    $this->load->helper(array('url', 'language'));
    $this->load->library('email');
    $this->load->library('ion_auth_model');

  }
    public function Pickup_projects_data(){
        $date = new DateTime();
        $date = $date->format('Y-m-d H:i:s');
        $pickup = $this->Project->getPickupProject();
        $arrayPickup = json_decode($pickup->project_id);
        $listPickup = [];
        foreach ($arrayPickup as $key => $value) {
            $project = $this->Project->getProjectIdJoin($value);
            if(isset($project) && $project) {
                if($project->project_type === '0') {
                    if($project->active ==='yes' && $project->collection_end_date >$date){
                        array_push($listPickup,$project);
                    }
                } else {
                    if($project->active ==='yes'){
                        array_push($listPickup,$project);
                    }
                }
            }

        }
        foreach ($listPickup as $key => $value) {
            $timeProject = getTimeProject($value->collection_end_date);
            $value->format_collection_end_date = $timeProject;
            $fomatImage = getImage($value->thumbnail);
            $value->medium_thumbnail = $fomatImage['medium'];
            $value->small_thumbnail = $fomatImage['small'];


            ////
            $value->now_count=array(
                'now_count'=>$value->now_count
            );

            $value->category=array(
                'id'=>$value->category_id,
                'name'=>$value->name,
                'slug'=>$value->slug
            );

            $value->user = array(
                'id'=>$value->user_id,
                'name'=>$value->username,
            );
            ///
        }
        return $listPickup;
    }
    // Pickup data get
	public function Pickup_projects_get(){
        $listPickup= $this->Pickup_projects_data();
        $this->response($listPickup, 200);
    }

    public function error_get(){
      $data = array(
          'status' => 'Errors!'
      );
      $this->response($data, 400);
    }

    // Project mới được admin chấp nhận đưa lên
    public function New_project_get(){
        $date = new DateTime();
        $date = $date->format('Y-m-d H:i:s');
        $input['order'] = array('projects.modified','DESC');
        // $input['where'] = array('collection_start_date >=' => $date);
        $input['where'] = array('projects.active'=>'yes');
        $input['where2'] = array('projects.collection_end_date >=' => $date);
        $input['where3'] = array('projects.project_type'=>'0');
        // $input['where'] = array('opened'=>'yes');
        $input['limit'] = array(4,0);
        $toppage = $this->Project->getDataProject($input);

        foreach ($toppage as $key => $value) {
            $value->now_count=array(
              'now_count'=>$value->now_count
            );

            $value->category=array(
                'id'=>$value->category_id,
                'name'=>$value->name,
                'slug'=>$value->slug
            );

            $value->user = array(
                'id'=>$value->user_id,
                'name'=>$value->username,
            );
            $timeProject = getTimeProject($value->collection_end_date);
            $value->format_collection_end_date = $timeProject;
        }
        $this->response($toppage, 200);
    }

    public function project_detail_get() {
        $id = $this->get('id');
        $project = $this->Project->getProjectIdJoin($id);

        if(($project->active != 'yes')){
          $data = array(
              'status'=>'error!',
          );
          $this->response($data,404);
        }
        if(!$project){
            $data = array(
                'status'=>'error!',
            );
            $this->response($data,404);
        }else{

        $project->category=array(
            'id'=>$project->category_id,
            'name'=>$project->name,
            'slug'=>$project->slug
        );
        $project->user = array(
            'id'=>$project->user_id,
            'name'=>$project->username,
            'address'=>$project->address,
            'job'=>$project->job,
            'profileImageURL'=>$project->profileImageURL
        );
        $project->thumbnail_img = array();
        if($project->thumbnail_descrip1){
            $project->thumbnail_img[]= array(
                'type'=>'image',
                'thumbnail_descrip'=>$project->thumbnail_descrip1
            );
        }
        if($project->thumbnail_descrip2){

            $project->thumbnail_img[]= array(
                'type'=>'image',
                'thumbnail_descrip'=>$project->thumbnail_descrip2
            );
        }
        if($project->thumbnail_descrip3){
            $project->thumbnail_img[]= array(
                'type'=>'image',
                'thumbnail_descrip'=>$project->thumbnail_descrip3
            );
        }
        if($project->project_type === '0') {
            $timeProject = getTimeProject($project->collection_end_date);
            $project->format_collection_end_date = $timeProject;
        }
        //nvtrong add
            $display_collection_end_date = new DateTime($project->collection_end_date);
            $dataTime = $display_collection_end_date->format("Y/m/d H:i");

             $project->display_collection_end_date = $dataTime;

        //nvtrong add end
        // $comment = $this->Comment_model->getComment($id);
        // $project->comment = $comment;
        $project_return = $this->BackingLevel->getBackingLevelId($project->id);
        foreach ($project_return as $key => $value) {
            if(isset($value->max_count) && $value->max_count && $project->project_type === '1') {

                $first = new DateTime($project->collection_start_date);
                $first_day = $first->format('Y-m-01');
                $last_day = new DateTime(date('Y-m-01'));
                $last_day = $last_day->format('Y-m-01');

                $time_first = strtotime($first_day);
                $time_last = strtotime($last_day);

                $first_year = date('Y', $time_first);
                $last_year = date('Y', $time_last);

                $first_month = date('m', $time_first);
                $last_month = date('m', $time_last);

                $diff = (($last_year - $first_year) * 12) + ($last_month - $first_month);
                $newmonth = ceil($diff/$project->number_month);

                $max_count = $this->ListDonateFanClub->countBackedProject($project->id,$newmonth,$value->id);
                $value->max_count = $value->max_count - $max_count;
            }
        }

        $start = '0';
        $limit = '5';

        $NumberComment = $this->Comment_model->getComment($id);
        $totalComments = count($NumberComment);
        $totalPages = ceil($totalComments / $limit);

        $input['limit']  = array($limit,$start);

        function date_compare($a, $b) {
            $t1 = strtotime($a->created);
            $t2 = strtotime($b->created);
            return $t1 - $t2;
        }

        $comment = $this->Comment_model->getComment($id,$input);
        if(isset($comment) && $comment){
            foreach ($comment as $key => $value) {

                $reply = $this->Reply_Comment_model->getReplyComment($value->id);
                $totalReply = $this->Reply_Comment_model->getReplyComment($value->id,1);

                if(count($totalReply) > 3){
                    $value->getMoreReply = true;
                }else{
                    $value->getMoreReply = false;
                }
                usort($reply, 'date_compare');
                if(isset($reply) && $reply){
                    foreach ($reply as $key => $valueReply) {
                        $user = $this->User->getUserId($valueReply->user_id);

                        $valueReply->username = $user->username;

                        $valueReply->profileImageURL = $user->profileImageURL;

                        $date1 = new DateTime($valueReply->created);
                        $date2 = new DateTime(date('Y-m-d H:i:s'));
                          $time = $date1->diff($date2);

                          $date = date_diff($date1,$date2);
                          $date = $date->format("%a");
                          $format_collection_end_date = array(
                            'date' =>$date,
                            'hour' => $time->h,
                            'minutes' => $time->i,
                            'second' => $time->s
                          );
                        $valueReply->time = $format_collection_end_date;
                    }
                }
                $user = $this->User->getUserId($value->user_id);

                $value->username = $user->username;

                $value->profileImageURL = $user->profileImageURL;

                $date1 = new DateTime($value->created);
                $date2 = new DateTime(date('Y-m-d H:i:s'));
                  $time = $date1->diff($date2);

                  $date = date_diff($date1,$date2);
                  $date = $date->format("%a");
                  $format_collection_end_date = array(
                    'date' =>$date,
                    'hour' => $time->h,
                    'minutes' => $time->i,
                    'second' => $time->s
                  );
                $value->time = $format_collection_end_date;
                $value->reply = $reply;

            }
        }

        usort($comment, 'date_compare');

        // get report daily
        $reportDaily = $this->Report->listReport2($id);
        // count all report daily
        $numberReport = $this->Report->countReportDaily2($id);
        // assign limit = 5
        $limitReport = 5;
        $totalPagesReport = ceil($numberReport / $limitReport);
        // create array

        $data = array(
            'project'=>$project,
            'project_return'=>$project_return,
            'comment' =>array(
                'page_count'=>$totalPages,
                'data' => $comment
            ),
            'report' => array(
                'totalPage' => $totalPagesReport,
                'data' => $reportDaily
            )
        );
        $this->response($data,200);
        }
    }


    // Project được admin add vào yêu thích
    public function Favourite_projects_get(){
        $date = new DateTime();
        $date = $date->format('Y-m-d H:i:s');
        $favourite = $this->Project->getFavouriteProject();
        $arrayFavourite = json_decode($favourite->project_id);
        $listFavourite = [];
        foreach ($arrayFavourite as $key => $value) {
            $project = $this->Project->getProjectIdJoin($value);
            if(isset($project) && $project->active ==='yes' && $project->collection_end_date >$date){
                array_push($listFavourite,$project);
            }
        }

        foreach ($listFavourite as $key => $value) {

            $value->now_count=array(
              'now_count'=>$value->now_count
            );

            $value->category=array(
                'id'=>$value->category_id,
                'name'=>$value->name,
                'slug'=>$value->slug
            );

            $value->user = array(
                'id'=>$value->user_id,
                'name'=>$value->username,
            );
            $timeProject = getTimeProject($value->collection_end_date);
            $value->format_collection_end_date = $timeProject;
        }

        $this->response($listFavourite, 200);
    }


    private function FacnClubData(){
        $date = new DateTime();
        $date = $date->format('Y-m-d H:i:s');
        $fanclub = $this->Project->getFanClubPickup();
        $arrayFanClub = json_decode($fanclub->project_id);
        $listFanClub = [];
        foreach ($arrayFanClub as $key => $value) {
            $project = $this->Project->getProjectIdJoin($value);
            if(isset($project) && $project->active ==='yes'){
                array_push($listFanClub,$project);
            }
        }

        foreach ($listFanClub as $key => $value) {

            $value->now_count=array(
                'now_count'=>$value->now_count
            );

            $value->category=array(
                'id'=>$value->category_id,
                'name'=>$value->name,
                'slug'=>$value->slug
            );

            $value->user = array(
                'id'=>$value->user_id,
                'name'=>$value->username,
            );
            // $timeProject = getTimeProject($value->collection_end_date);
            // $value->format_collection_end_date = $timeProject;
        }
        return $listFanClub;

    }
    // Project được admin add vào yêu thích
    public function FanClub_projects_get(){
        $listFanClub =$this->FacnClubData();
        $this->response($listFanClub, 200);
    }

    private function getListFanClubData($limit='16'){
        $date = new DateTime();
        $date = $date->format('Y-m-d H:i:s');
        $start = $this->get('start');
       // $limit = '16';
        if(!$start || $start =='0'){
            $start = '0';
        }else{
            $start = $start * $limit;
        }
        $input['where'] = array('projects.project_type' => 1);
        $input['where2'] = array('projects.active'=>'yes');
        $Project = $this->Project->getDataProject($input);
        $totalProject = count($Project);
        $totalPages = ceil($totalProject / $limit);

        $input['limit']  = array($limit,$start);
        $fanclub = $this->Project->getDataProject($input);

        // get data
        foreach ($fanclub as $key => $value) {

            $value->now_count=array(
                'now_count'=>$value->now_count
            );

            $value->category=array(
                'id'=>$value->category_id,
                'name'=>$value->name,
                'slug'=>$value->slug
            );

            $value->user = array(
                'id'=>$value->user_id,
                'name'=>$value->username,
            );

        }

        $data = array(
            'page_count'=>$totalPages,
            'data'=>$fanclub
        );
        return $data;
    }
    public function ListFanClub_get(){
        $data = $this->getListFanClubData();
        $this->response($data, 200);
    }


    // Tin tức mới nhất
    public function News_get(){
        $input['order'] = array('created','DESC');
        $input['where'] = array('status'=>'1');
        $input['limit'] = array(4,0);
        $data = $this->News->get_list($input);
        $this->response($data, 200);
    }

    public function new_detail_get($id){
        $data = $this->News->getNewId($id);
        if($data){
            $this->response($data, 200);
        }else{
            $data = array(
                'status'=>'error!'
            );
            $this->response($data, 404);
        }
    }
    public function news_list_month_get(){
        $myData = $this->News->getNewsYearMonth();
        foreach ($myData as &$item){
            $result = $this->News->getNewsYearMonthInMonth($item->YearMonth);
            $item->list = $result;
            $item->count = count($result);
            //$item[]

        }
        $this->response($myData, 200);
        //news_list_month
    }
    public function new_detail_more_get($id){

       $myResult =  $this->News->getNew2NextPre($id);

        $data = array(
            'page_count'=>1,
            'data'=>$myResult['list'],
            'isNext'=>$myResult['next'],
            'isPre'=>$myResult['pre'],
            'next_id'=>$myResult['next_id'],
            'pre_id'=>$myResult['pre_id'],
            );
/*
       $start = $this->get('start');
       $limit = '2';
       if(!$start || $start =='0'){
           $start ='0';
       }else{
           $start = $start*$limit;
       }
       $input['where'] = array('status'=>'1');
       $NumberNews = $this->News->get_data($input);
       $totalNews = count($NumberNews);
       $totalPages = ceil($totalNews / $limit);
       $input['order'] = array('created','DESC');
       $input['where'] = array('status'=>'1','id!='=>$id,);
       $input['limit']  = array($limit,$start);
       $News = $this->News->get_list($input);
       $data = array(
           'page_count'=>$totalPages,
           'data'=>$News
       );
*/
        $this->response($data, 200);
    }
    public function news_list_get(){
        $start = $this->get('start');
        $searchText = $this->get('searchText');
        //

        $limit = '5';
        if(!$start || $start =='0'){
          $start ='0';
        }else{
          $start = $start*$limit;
        }
        $input['where'] = array('status'=>'1');
        if($searchText!=null){
            $input['like']= array('concat(news.des,news.content,news.title)',trim($searchText));
        }
        $NumberNews = $this->News->get_data($input);
        $totalNews = count($NumberNews);
        $totalPages = ceil($totalNews / $limit);
        $input['order'] = array('created','DESC');
        $input['where'] = array('status'=>'1');

        $input['limit']  = array($limit,$start);
        $News = $this->News->get_list($input);
        $data = array(
            'page_count'=>$totalPages,
            'data'=>$News
        );

        $this->response($data, 200);
    }
    // Project được admin add vào đề cử
    public function Recommends_projects_get(){
        // get date time
        $date = new DateTime();
        $date = $date->format('Y-m-d H:i:s');
        // get recommend (chain string id)
        $recommend = $this->Project->getRecommendProject();
        // json endcode chain string -> array
        $arrayRecommend = json_decode($recommend->project_id);
        // create array
        $listRecommend = [];
        foreach ($arrayRecommend as $key => $value) {
            $project = $this->Project->getProjectIdJoin($value);
            if(isset($project) && $project->active ==='yes' && $project->collection_end_date >$date){
                array_push($listRecommend,$project);
            }
        }
        // get data
        foreach ($listRecommend as $key => $value) {

            $value->now_count=array(
              'now_count'=>$value->now_count
            );

            $value->category=array(
                'id'=>$value->category_id,
                'name'=>$value->name,
                'slug'=>$value->slug
            );

            $value->user = array(
                'id'=>$value->user_id,
                'name'=>$value->username,
            );
            $timeProject = getTimeProject($value->collection_end_date);
            $value->format_collection_end_date = $timeProject;
        }
        $this->response($listRecommend, 200);
    }

    // Project Expired
    public function Expired_get(){
        // get time
        $date = new DateTime();
        // format time
        $date = $date->format('Y-m-d H:i:s');
        // get data
        $expired = $this->Project->getExpiredProject();

        // json array
        $arrayExpired = json_decode($expired->project_id);
        // create array
        $listExpired = array();
        foreach ($arrayExpired as $key => $value) {
            // get project folow id
            $project = $this->Project->getProjectIdJoin($value);
            // check and push array
            if(isset($project) && $project->active ==='yes' && $project->collection_end_date < $date){
                array_push($listExpired,$project);
            }
        }
        // get data
        foreach ($listExpired as $key => $value) {

            $value->now_count=array(
              'now_count'=>$value->now_count
            );

            $value->category=array(
                'id'=>$value->category_id,
                'name'=>$value->name,
                'slug'=>$value->slug
            );

            $value->user = array(
                'id'=>$value->user_id,
                'name'=>$value->username,
            );
            $timeProject = getTimeProject($value->collection_end_date);
            $value->format_collection_end_date = $timeProject;
        }

        $this->response($listExpired, 200);
    }
    private function expiredPaginationData(){
        $date = new DateTime();
        $date = $date->format('Y-m-d H:i:s');
        $start = $this->get('start');
        $limit = '15';
        if(!$start || $start =='0'){
            $start = '0';
        }else{
            $start = $start * $limit;
        }
        $input['where'] = array('projects.collection_end_date <=' => $date);
        // $input['where'] = array('opened'=>'yes');
        $input['where2'] = array('projects.active'=>'yes');
        $input['where3'] = array('(projects.collected_amount/projects.goal_amount)*100 >=' => 100);
        $input['where4'] = array('projects.project_type'=>'0');
        $Project = $this->Project->getDataProject($input);
        $totalProject = count($Project);
        $totalPages = ceil($totalProject / $limit);

        $input['limit']  = array($limit,$start);
        $expired = $this->Project->getDataProject($input);

        // get data
        foreach ($expired as $key => $value) {

            $value->now_count=array(
                'now_count'=>$value->now_count
            );

            $value->category=array(
                'id'=>$value->category_id,
                'name'=>$value->name,
                'slug'=>$value->slug
            );

            $value->user = array(
                'id'=>$value->user_id,
                'name'=>$value->username,
            );
            $timeProject = getTimeProject($value->collection_end_date);
            $value->format_collection_end_date = $timeProject;
        }

        $data = array(
            'page_count'=>$totalPages,
            'data'=>$expired
        );
        return $data;
     }
    public function expiredPagination_get(){
        $data = $this->expiredPaginationData();
        $this->response($data, 200);
    }


    // Project sắp gọi đủ vốn
    public function  enough_goal_amount_get(){
        $date = new DateTime();
        $date = $date->format('Y-m-d H:i:s');

        $project_goal = $this->Project->getEnoughAmount($date);
        foreach ($project_goal as $key => $value) {
            $value->now_count= array(
              'now_count'=>$value->now_count
            );

            $value->category=array(
                'id'=>$value->category_id,
                'name'=>$value->name,
                'slug'=>$value->slug
            );

            $value->user = array(
                'id'=>$value->user_id,
                'name'=>$value->username,
            );

            $timeProject = getTimeProject($value->collection_end_date);
            $value->format_collection_end_date = $timeProject;
        }

        $this->response($project_goal, 200);
    }

    private function getDataEnoughGoalAmount(){
        $date = new DateTime();
        $date = $date->format('Y-m-d H:i:s');

        $start = $this->get('start');
        $limit = '16';
        if(!$start || $start =='0'){
            $start = '0';
        }else{
            $start = $start * $limit;
        }
        // var_dump($a);

        $input['limit'] = array($limit,$start);

        $project_goal = $this->Project->getEnoughAmountPagination($date,$input);

        foreach ($project_goal as $key => $value) {

            $value->now_count = array(
                'now_count'=>$value->now_count
            );

            $value->category = array(
                'id'=>$value->category_id,
                'name'=>$value->name,
                'slug'=>$value->slug
            );

            $value->user = array(
                'id'=>$value->user_id,
                'name'=>$value->username,
            );

            $timeProject = getTimeProject($value->collection_end_date);
            $value->format_collection_end_date = $timeProject;
        }

        $totalProject = $this->Project->countEnoughAmount($date);
        $totalPages = ceil($totalProject / $limit);
        $data = array(
            'page_count'=>$totalPages,
            'data'=>$project_goal
        );
        return $data;
    }
    private function getDataEnoughGoalAmount80(){
        $date = new DateTime();
        $date = $date->format('Y-m-d H:i:s');

        $start = $this->get('start');
        $limit = '16';
        if(!$start || $start =='0'){
            $start = '0';
        }else{
            $start = $start * $limit;
        }
        // var_dump($a);

        $input['limit'] = array($limit,$start);

        $project_goal = $this->Project->getEnoughAmountPagination80($date,$input);

        foreach ($project_goal as $key => $value) {

            $value->now_count = array(
                'now_count'=>$value->now_count
            );

            $value->category = array(
                'id'=>$value->category_id,
                'name'=>$value->name,
                'slug'=>$value->slug
            );

            $value->user = array(
                'id'=>$value->user_id,
                'name'=>$value->username,
            );

            $timeProject = getTimeProject($value->collection_end_date);
            $value->format_collection_end_date = $timeProject;
        }

        $totalProject = $this->Project->countEnoughAmount80($date);
        $totalPages = ceil($totalProject / $limit);
        $data = array(
            'page_count'=>$totalPages,
            'data'=>$project_goal
        );
        return $data;
    }
    public function  enoughGoalAmount_get(){
        $data =  getDataEnoughGoalAmount();
        $this->response($data, 200);
    }


    // Project sắp hết hạn
    public function enough_expired_get(){

        $date = new DateTime();
        $date = $date->format('Y-m-d H:i:s');
        $parts = explode('-', $date);
        $datePlusFive = date(
            'Y-m-d H:i:s',
            mktime(0, 0, 0, $parts[1], $parts[2] + 5, $parts[0])
        );

        $near_expired = $this->Project->getProjectEnoughExpired($date,$datePlusFive);

        foreach ($near_expired as $key => $value) {

            $value->now_count = array(
                'now_count'=> $value->now_count
            );

            $value->category=array(
                'id'=>$value->category_id,
                'name'=>$value->name,
                'slug'=>$value->slug
            );

            $value->user = array(
                'id'=>$value->user_id,
                'name'=>$value->username,
            );

            $timeProject = getTimeProject($value->collection_end_date);
            $value->format_collection_end_date = $timeProject;
        }

        $this->response($near_expired, 200);
    }

    private function enoughExpiredData(){
        $date = new DateTime();
        $date = $date->format('Y-m-d H:i:s');
        $parts = explode('-', $date);
        $datePlusFive = date(
            'Y-m-d H:i:s',
            mktime(0, 0, 0, $parts[1], $parts[2] + 5, $parts[0])
        );
        $count_total = $this->Project->countProjectEnoughExpired($date,$datePlusFive);

        $start = $this->get('start');
        $limit = '15';
        if(!$start || $start =='0'){
            $start = '0';
        }else{
            $start = $start * $limit;
        }

        $totalProject = $count_total;
        $totalPages = ceil($totalProject / $limit);

        $input['limit'] = array($limit,$start);
        $near_expired = $this->Project->getProjectEnoughExpiredPagination($date,$datePlusFive,$input);

        foreach ($near_expired as $key => $value) {
            $value->now_count = array(
                'now_count'=> $value->now_count
            );
            $value->category=array(
                'id'=>$value->category_id,
                'name'=>$value->name,
                'slug'=>$value->slug
            );
            $value->user = array(
                'id'=>$value->user_id,
                'name'=>$value->username,
            );

            $timeProject = getTimeProject($value->collection_end_date);
            $value->format_collection_end_date = $timeProject;
        }

        $data = array(
            'page_count'=>$totalPages,
            'data'=>$near_expired
        );
        return $data;
    }
    public function  enoughExpired_get(){


        $data = $this->enoughExpiredData();
        $this->response($data, 200);
    }


    public function search_get(){

        $search = $this->get('search');
        $start = '0';
        $limit = '30';
        $input['limit'] = array($limit,$start);

        $dataSearch = $this->Project->getProjectSearch($search,$input);

        foreach ($dataSearch as $key => $value) {

            $value->now_count = array(
                'now_count'=> $value->now_count
            );

            $value->category = array(
                'id'=>$value->category_id,
                'name'=>$value->name,
                'slug'=>$value->slug
            );

            $value->user = array(
                'id'=>$value->user_id,
                'name'=>$value->username,
            );

            $timeProject = getTimeProject($value->collection_end_date);
            $value->format_collection_end_date = $timeProject;
         }

         $this->response($dataSearch, 200);
     }

    function category_get(){
        $list = $this->Category->get_list();
        $this->response($list,REST_Controller::HTTP_OK);
    }


    function project_category_get($slug){
        $start = $this->get('start');
        $limit = '16';
        if($start == '0'){
          $start = $start;
        }else{
          $start = $start * $limit;
        }
        $category_id = $this->Category->getCategorySlug($slug);
        $project = $this->Project->getProjectCateId($category_id->id,$input = null);
        $totalProject = count($project);
        $totalPages = ceil($totalProject / $limit);

        $input['limit']  = array($limit,$start);
        $list = $this->Project->getProjectCateId($category_id->id,$input);
        $categories = $this->Category->getCategory();
        $user = $this->User->getUser();
        foreach ($list as $key => $value) {
            $value->now_count=array(
                'now_count'=>$value->now_count
            );

            $value->category=array(
                'id'=>$value->category_id,
                'name'=>$value->name,
                'slug'=>$value->slug
            );

            $value->user = array(
                'id'=>$value->user_id,
                'name'=>$value->username,
            );

            $timeProject = getTimeProject($value->collection_end_date);
            $value->format_collection_end_date = $timeProject;
        }

        $data = array(
            'page_count'=>$totalPages,
            'category'=>$category_id->name,
            'data'=>$list,
        );
        $this->response($data, 200);
    }

    public function getSearchData($search){


        $start = $this->get('start');
        $limit = '16';
        if($start == '0'){
            $start = $start;
        }else{
            $start = $start * $limit;
        }

        $project = $this->Project->getProjectSearch($search,$input = null);
        $totalProject = count($project);
        $totalPages = ceil($totalProject / $limit);

        $input['limit']  = array($limit,$start);
        $list = $this->Project->getProjectSearch($search,$input);
        $categories = $this->Category->getCategory();
        $user = $this->User->getUser();
        foreach ($list as $key => $value) {
            $value->now_count=array(
                'now_count'=>$value->now_count
            );

            $value->category=array(
                'id'=>$value->category_id,
                'name'=>$value->name,
                'slug'=>$value->slug
            );

            $value->user = array(
                'id'=>$value->user_id,
                'name'=>$value->username,
            );

            $timeProject = getTimeProject($value->collection_end_date);
            $value->format_collection_end_date = $timeProject;
        }

        $data = array(
            'page_count'=>$totalPages,
            'data'=>$list,
        );
       return $data;
    }

    function project_from_type_get($slug){
//        1. 最近見たプロジェクト =  from-your-access( list những project user xem gần đây)
//2.あとひと押しのプロジェクト= from-support-80 (List những project được support trên 80%)
//3.新着のプロジェクト   =   from-news-project(List những project mới)
//4.注目のプロジェクト    = from-favorite (List những project được quan tâm)
//5.もうすぐ終了のプロジェクト = from-support-90 (List những project gần hoàn thành (trên 90%)
//6.コミュニティ型プロジェクト(để confirm)
//7.成約したプロジェクト(để confirm)
        $from_your_access =  'from-your-access' ;
        $from_support_80 = 'from-support-80' ;
        $from_news_project = 'from-news-project' ;
        $from_favorite =  'from-favorite';
        $from_support_90 = 'from-support-90' ;
        $from_fanclub = 'from-fanclub' ;
        $from_your_expired = 'from-your-expired' ;
        $expiredPagination = 'expiredPagination' ;
        $from_project_100 = 'from-project-100' ; // 2019 08 11

        $FROM_TYPE[$from_your_access] = "最近見たプロジェクト";
        $FROM_TYPE[$from_support_80] = "あとひと押しのプロジェクト";
        $FROM_TYPE[$from_news_project] = "新着のプロジェクト";
        $FROM_TYPE[$from_favorite] = "注目のプロジェクト";
        $FROM_TYPE[$from_support_90] = "もうすぐ終了のプロジェクト";
        $FROM_TYPE[$from_fanclub] = "クラブ";
        $FROM_TYPE[$from_your_expired] = "終了間近のプロジェクト";
        $FROM_TYPE[$expiredPagination] = "過去プロジェクト";
        $FROM_TYPE[$from_project_100] = "成約したプロジェクト";



        $data = array([
            'status' => ' Get failed!'
        ]);
        $start = $this->get('start');
        $limit = '16';
        if($start == '0'){
            $start = $start;
        }else{
            $start = $start * $limit;
        }
        $data = array(
            'page_count'=>0,
            'category'=>$slug,
            'data'=>[],
        );
        $myP = strpos($slug,'search-project');
        $mySearch = "";
//var_dump($myP );
        if($myP===false){

        }else{
            if($myP==0){
                //var_dump($myP );
                $mySearch = str_replace("search-project-","",$slug);
            }
        }

        if($mySearch != ''){
            $mySearch = urldecode($mySearch);
            $data = $this->getSearchData($mySearch);
            $data['category'] = $mySearch;
            $this->response($data, 200);
        }else
        if($slug == $from_project_100){

                 $data = $this->fullGoalAmount_data(15);
//                $data['category'] = $FROM_TYPE[$slug];
//                $this->response($data, 200);
//
//
            //$data = $this->getListFanClubData('15');
            $data['category'] = $FROM_TYPE[$slug];
            $this->response($data, 200);
        }else
        if($slug == $from_fanclub){
            $user_id = $this->getPara('user_id');
            //$data = $this->getAllProjectDataAccessList($user_id);
            //$listFanClub =$this->FacnClubData();
            $data = $this->getListFanClubData('15');
            $data['category'] = $FROM_TYPE[$slug];
            $this->response($data, 200);
        }else
        if($slug == $expiredPagination){

            $data = $this->expiredPaginationData();
            $data['category'] = $FROM_TYPE[$slug];
            $this->response($data, 200);
        }else
         if($slug == $from_your_access){
             $user_id = $this->getPara('user_id');
             $data = $this->getAllProjectDataAccessList($user_id);
             $data['category'] = $FROM_TYPE[$slug];
             $this->response($data, 200);
         }else
             if($slug == $from_your_expired){
                 $data =  $this->enoughExpiredData();
                 $data['category'] = $FROM_TYPE[$slug];
                 $this->response($data, 200);
             }else

        if($slug == $from_news_project){
            $data = $this->getAllProjectData();
            $data['category'] = $FROM_TYPE[$slug];
            $this->response($data, 200);
        }else
        if($slug == $from_support_80){
            $data = $this->getDataEnoughGoalAmount80();
            $data['category'] = $FROM_TYPE[$slug];
            $this->response($data, 200);
        }else
        if($slug == $from_support_90){
            $data = $this->getDataEnoughGoalAmount();
            $data['category'] = $FROM_TYPE[$slug];
            $this->response($data, 200);
        }else
        if($slug == $from_favorite){

            $listPickup= $this->Pickup_projects_data();
            if(count($listPickup)>0){
                $data = array(
                    'page_count'=>1,
                    'category'=>$FROM_TYPE[$slug],
                    'data'=>$listPickup,
                );

                $this->response($data, 200);
            /*
            $arrF = $this->Project->getFavouriteProject();
            $arrFavoId = array_filter(explode(",",str_replace(["[","]",'"'],"", $arrF->project_id))) ;
            $input['array_project_id'] = [];

            if(count($arrFavoId)>0){
                $input['array_project_id'] = $arrFavoId;
            }

            $input['order'] = array('created','DESC');
            $project = $this->Project->getProjectFavo($input);
            $totalProject = count($project);
            $totalPages = ceil($totalProject / $limit);
            $input['limit'] = array(8,0);
            $list = $this->Project->getProjectFavo($input);

            foreach ($list as $key => $value) {
                $value->now_count=array(
                    'now_count'=>$value->now_count
                );
                $value->category=array(
                    'id'=>$value->category_id,
                    'name'=>$value->name,
                    'slug'=>$value->slug
                );

                $value->user = array(
                    'id'=>$value->user_id,
                    'name'=>$value->username,
                );

                $timeProject = getTimeProject($value->collection_end_date);
                $value->format_collection_end_date = $timeProject;
            }

            if(count($project)>0){
                $data = array(
                    'page_count'=>$totalPages,
                    'category'=>$FROM_TYPE[$slug],
                    'data'=>$list,
                );

                $this->response($data, 200);
                **/
            }else{
                $data = array([
                    'status' => ' Get failed2!'
                ]);
                $this->response($data, 400);
            }
        }else{
            return $this->project_category_get($slug);
        }

        $this->response($data, 400);
    }
    public function newProject_get(){
        $date = new DateTime();
        $start = $this->get('start');
        $limit = '16';
        if(!$start || $start =='0'){
          $start ='0';
        }else{
          $start = $start*$limit;
        }
        $date = $date->format('Y-m-d H:i:s');
        $input['where'] = array('projects.collection_end_date >=' => $date);
        // $input['where'] = array('opened'=>'yes');
        $input['where2'] = array('projects.active'=>'yes');
        $Project = $this->Project->getDataProject($input);
        $totalProject = count($Project);
        $totalPages = ceil($totalProject / $limit);

        $input['limit']  = array($limit,$start);
        $input['order'] = array('projects.modified','DESC');
        $toppage = $this->Project->getDataProject($input);

        foreach ($toppage as $key => $value) {

            $value->now_count=array(
                'now_count'=> $value->now_count
            );

            $value->category=array(
                'id'=>$value->category_id,
                'name'=>$value->name,
                'slug'=>$value->slug
            );

            $value->user = array(
                'id'=>$value->user_id,
                'name'=>$value->username,
            );

            $timeProject = getTimeProject($value->collection_end_date);
            $value->format_collection_end_date = $timeProject;

        }
        $data = array(
            'page_count'=>$totalPages,
            'data'=>$toppage
        );
        $this->response($data, 200);
    }
    //api
    public function top_top_project_get(){
        $date = new DateTime();
        $start = $this->get('start');
        $limit = '16';
        if(!$start || $start =='0'){
            $start ='0';
        }else{
            $start = $start*$limit;
        }
        $date = $date->format('Y-m-d H:i:s');
        $input['where'] = array('projects.collection_end_date >=' => $date);
        // $input['where'] = array('opened'=>'yes');
        $input['where2'] = array('projects.active'=>'yes');
        $Project = $this->Project->getDataProject($input);
        $totalProject = count($Project);
        $totalPages = ceil($totalProject / $limit);

        $input['limit']  = array($limit,$start);
        $input['order'] = array('projects.modified','DESC');
        $toppage = $this->Project->getDataProject($input);

        foreach ($toppage as $key => $value) {

            $value->now_count=array(
                'now_count'=> $value->now_count
            );

            $value->category=array(
                'id'=>$value->category_id,
                'name'=>$value->name,
                'slug'=>$value->slug
            );

            $value->user = array(
                'id'=>$value->user_id,
                'name'=>$value->username,
            );

            $timeProject = getTimeProject($value->collection_end_date);
            $value->format_collection_end_date = $timeProject;

        }
        $data = array(
            'page_count'=>$totalPages,
            'data'=>$toppage
        );
        $this->response($data, 200);
    }
    private function getAllProjectData(){
        $date = new DateTime();
        $start = $this->get('start');
        $limit = '9';
        if(!$start || $start =='0'){
            $start ='0';
        }else{
            $start = $start*$limit;
        }
        $date = $date->format('Y-m-d H:i:s');
        $input['where'] = array('projects.collection_end_date >=' => $date);
        // $input['where'] = array('opened'=>'yes');
        $input['where2'] = array('projects.active'=>'yes');
        $Project = $this->Project->getDataProject($input);
        $totalProject = count($Project);
        $totalPages = ceil($totalProject / $limit);

        $input['limit']  = array($limit,$start);
        $input['order'] = array('projects.collection_end_date','DESC');
        $toppage = $this->Project->getDataProject($input);

        foreach ($toppage as $key => $value) {
            $value->now_count=array(
                'now_count'=> $value->now_count
            );

            $value->category=array(
                'id'=>$value->category_id,
                'name'=>$value->name,
                'slug'=>$value->slug
            );
            $value->user = array(
                'id'=>$value->user_id,
                'name'=>$value->username,
            );

            $timeProject = getTimeProject($value->collection_end_date);
            $value->format_collection_end_date = $timeProject;
        }
        $data = array(
            'page_count'=>$totalPages,
            'data'=>$toppage
        );
       return $data;
    }
    public function projectPagination_get(){
//        $date = new DateTime();
//        $start = $this->get('start');
//        $limit = '16';
//        if(!$start || $start =='0'){
//          $start ='0';
//        }else{
//          $start = $start*$limit;
//        }
//        $date = $date->format('Y-m-d H:i:s');
//        $input['where'] = array('projects.collection_end_date >=' => $date);
//        // $input['where'] = array('opened'=>'yes');
//        $input['where2'] = array('projects.active'=>'yes');
//        $Project = $this->Project->getDataProject($input);
//        $totalProject = count($Project);
//        $totalPages = ceil($totalProject / $limit);
//
//        $input['limit']  = array($limit,$start);
//        $input['order'] = array('projects.collection_end_date','DESC');
//        $toppage = $this->Project->getDataProject($input);
//
//        foreach ($toppage as $key => $value) {
//            $value->now_count=array(
//              'now_count'=> $value->now_count
//            );
//
//            $value->category=array(
//                'id'=>$value->category_id,
//                'name'=>$value->name,
//                'slug'=>$value->slug
//            );
//            $value->user = array(
//                'id'=>$value->user_id,
//                'name'=>$value->username,
//            );
//
//            $timeProject = getTimeProject($value->collection_end_date);
//            $value->format_collection_end_date = $timeProject;
//          }
//        $data = array(
//            'page_count'=>$totalPages,
//            'data'=>$toppage
//        );
        $data = $this->getAllProjectData();
        $this->response($data, 200);
    }

    public function getUser_get($name){
        $curUser = $this->User->getDataUser($name);
        $group = $this->ion_auth->get_users_groups($curUser->id)->row();
        $group_name = $group->name;
        $numberProject = $this->Project->getProjectUserId($curUser->id);
        $numberPatron = $this->BackedProject->countBackedProject($curUser->id);
        $safeUserObject = null;
        if($curUser){
            $safeUserObject = array(
              "username"=>$curUser->username,
              "roles"=>$group_name,
              'profileImageURL' =>$curUser->profileImageURL,
              'created' =>$curUser->created_on,
              'sex' =>$curUser->sex,
              'birthday' =>$curUser->birthday,
              'self_description' =>$curUser->self_description,
              'url1' =>$curUser->url1,
              'url2' =>$curUser->url2,
              'url3' =>$curUser->url3,
              'address'=>$curUser->address,
              'country'=>$curUser->country,
              'phone'=>$curUser->phone,
              'postcode'=>$curUser->postcode,
              'address_return'=>$curUser->address_return,
              'numberProject'=>count($numberProject),
              'patron'=>count($numberPatron)
            );
        }
        $safeUserObject = json_encode($safeUserObject);
        $safeUserObject = json_decode($safeUserObject);
        $this->response($safeUserObject, 200); // OK (200) being the HTTP response code
      }
    // Check Project Id exists
    public function checkProjectId_get(){
      //get id
      $id = $this->get('id');
      //Check Id has exists or
      //If it exists but  not active or
      $project = $this->Project->checkProjectId($id);
      // If Id exists and project active
      if($project){
        // Response 200 vs status : true
        $success = array(
          'status'=>'true'
        );
        $this->response($success,200);
      // If id not exists or Id exists but not active
      }else{
        //Response 404 vs status : false
        $error = array(
          'status'=>'false'
        );
        $this->response($error,404);
      }
    }

    public function testGetAllProject_get(){
      $projects = $this->Project->get_data();
      foreach ($projects as $key => $value) {
          var_dump(getImage($value->thumbnail));
      }
      $this->response($projects,200);
    }

    public function testPostDel_post(){
        $a = $this->post('a');
        $this->response($a,200);
    }

    public function testTimeUTC_get(){
        $datetime = new DateTime();
        $datetime = $datetime->format('Y-m-d H:i:s');
        $asia_timestamp = strtotime($datetime);
        echo date_default_timezone_get()."<br>"; // Asia/Calcutta
        date_default_timezone_set('UTC');
        echo date_default_timezone_get()."<br>"; //UTC
        echo $utcDateTime = date("Y-m-d H:i:s", $asia_timestamp);
    }

    public function formatCollectionEndDate_post(){

        $project_name = $this->post('project_name');
        $category = $this->post('category_id');
        $summary = $this->post('description');
        $radio = $this->post('thumbnail_type');
        $video = $this->post('thumbnail_movie_code');

        if(!isset($video)){
            $video = null;
        }
        $thumbnail1 = $this->post('thumbnail_descrip1');
        $thumbnail2 = $this->post('thumbnail_descrip2');
        $thumbnail3 = $this->post('thumbnail_descrip3');

        if(!isset($thumbnail1)){
            $thumbnail1 = base_url().'images/2018/default/noimage-01.png';
        }
        if(!isset($thumbnail2)){
            $thumbnail2 = base_url().'images/2018/default/noimage-01.png';
        }
        if(!isset($thumbnail3)){
            $thumbnail3 = base_url().'images/2018/default/noimage-01.png';
        }
        $collection_end_date = $this->post('collection_end_date');
        $collection_end_date = new DateTime($collection_end_date);
        $collection_end_date = $collection_end_date->format('Y-m-d 23:59:59');
        $goal_amount = $this->post('goal_amount');
        $goal_amount = preg_replace('/,/', '', $goal_amount);
        $project_content = $this->post('project_content');
        $thumbnail = $this->post('thumbnail');
        if(!isset($thumbnail)){
            $thumbnail = base_url().'images/2018/default/noimage-01.png';
        }

        $date1 = new DateTime($collection_end_date);
        $date2 = new DateTime(date('Y-m-d H:i:s'));
        if($date1 > $date2){
          $time = $date1->diff($date2);

          $date = date_diff($date1,$date2);
          $date = $date->format("%a");
          $format_collection_end_date = array(
            'date' =>$date,
            'hour' => $time->h,
            'minutes' => $time->i,
          );
        }else{
          $format_collection_end_date = array(
            'status'=>'終了'
          );
        }
        $data_project = array(
            'project_name' => $project_name,
            'category_id' => $category,
            'description' => $summary,
            'goal_amount' => $goal_amount,
            'thumbnail_descrip1'=> $thumbnail1,
            'thumbnail_descrip2'=> $thumbnail2,
            'thumbnail_descrip3'=> $thumbnail3,
            'thumbnail_type'=>$radio,
            'thumbnail_movie_code'=>$video,
            'collection_start_date'=>$date,
            'collection_end_date'=>$collection_end_date,
            'active' => 'no',
            'created' => $date,
            'modified'=> $date,
            'thumbnail'=>$thumbnail,
            'project_content'=>$project_content,
            'format_collection_end_date' =>$format_collection_end_date
        );
        $this->response($data_project,200);
    }

    public function contactForm_post(){
        $email_admin = $this->config->item('email_admin');

        $email =  $this->post('email');
        $name = $this->post('user_name');
        $subject = $this->post('subject');
        $message = $this->post('message');

        $captcha = $this->post('captcharesponse');
        if(isset($message) && isset($name) && isset($email) && isset($captcha) && $captcha && $email && $name && $message){
            $secretKey = $this->config->item('keyReCaptcha');
            $response=file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=".$secretKey."&response=".$captcha);
            $responseKeys = json_decode($response,true);

            if($responseKeys["success"] !== true) {
                $error = array(
                    'status' => 'error!'
                );
                $this->response($error,500);
            } else {
                $this->load->library('email');
                //use json_encode show data.
                // $this->email->initialize($config);

                $this->email->from('admin@kakuseida.conoha.io', 'KAKUSEIDA');
                $this->email->to($email_admin);
                // $this->email->reply_to('admin@kakuseida.conoha.io', 'KAKUSEIDA');
                $this->email->subject($subject);
                $this->email->message('<pre>
                本日、KAKUSEIDA でお問い合わせがあります。
                お問い合わせ内容は以下のようです。
                -------------------
                お名前  : '.$name.'
                返信先メールアドレス  :   '.$email.'
                お問い合わせ件名    :   '.$subject.'
                お問い合わせ内容    :   '.$message.'
                -------------------
                </pre>');

                if($this->email->send()){

                    $this->email->from('admin@kakuseida.conoha.io', 'KAKUSEIDA');
                    $this->email->to($email);
                    $this->email->subject('【KAKUSEIDA】お問い合わせをご送付、ありがとうございます。');
                    $this->email->message('<pre>'.$name.'様、<br /><br />お世話になります。<br />本日、お問い合わせをご送付、まことにありがとうございました。<br />お問い合わせフォームに記入した内容は以下のようです。一度ご確認ください。<br />-------------------<br />お名前  : '.$name.'<br />返信先メールアドレス  :   '.$email.'<br />お問い合わせ件名    :   '.$subject.'<br />お問い合わせ内容    :   '.$message.'<br />-------------------<br />【KAKUSEIDA】の担当者から二日以内にご連絡いたします。<br />お手数ですが、少々お待ちください。<br />何卒よろしくお願い致します。<br /></pre>');
                    $this->email->send();
                    $user = array(
                        'status' => 'Send Mail Thanh Cong'
                    );
                    $this->response($user, REST_Controller::HTTP_OK);
                }else{
                    $invalid = ['status' => 'Send Mail That Bai'];
                    $this->response($invalid,  REST_Controller::HTTP_BAD_REQUEST);
                }
            }
        }else{
            $invalid = ['status' => 'Send Mail That Bai'];
            $this->response($invalid,  REST_Controller::HTTP_BAD_REQUEST);
        }
    }

    public function statisticDonate_get() {
        $id = $this->get('id');
        $project = $this->Project->ProjectId($id);
        if(!$project){
            $data = array(
                'status'=>'error!',
            );
            $this->response($data,404);
        }else{
            $start = $this->get('start');
            $limit = '10';
            if(!$start || $start ==='0'){
                $start ='0';
            }else{
                $start = $start*$limit;
            }

            if($project->project_type === '1') {
                $curdate = new DateTime();
                $first = new DateTime($project->collection_start_date);
                $first_day = $first->format('Y-m-01');

                $last_day = $curdate->format('Y-m-01');

                $time_first = strtotime($first_day);
                $time_last = strtotime($last_day);

                $first_year = date('Y', $time_first);
                $last_year = date('Y', $time_last);

                $first_month = date('m', $time_first);
                $last_month = date('m', $time_last);

                $diff = (($last_year - $first_year) * 12) + ($last_month - $first_month);
                $newmonth = ceil($diff/$project->number_month);

                $first_day = $first->format('Y-m-01 H:i:s');
                // // use explode format firstday
                $parts = explode('-', $first_day);
                // // first day = first day of month create project




                // // var_dump($project->id);
                // // var_dump($newmonth);
                // if($newmonth === (float)0) {
                //     $last_dat = date(
                //         'Y-m-t H:i:s',
                //         mktime(0, 0, 0, ($parts[1] + $value->number_month), $parts[2] + 1, $parts[0])
                //     );
                // $max_count = $this->ListDonateFanClub->countBackedProject($project->id,$newmonth,$value->id);
                // $value->max_count = $value->max_count - $max_count;
            }
            for ($i=0; $i <=7 ; $i++) {
                if($i === (int)0) {
                    $first_day = date(
                        'Y-m-01 00:00:00',
                        mktime(0, 0, 0, $parts[1], $parts[2] +1, $parts[0])
                    );
                    $last_day = date(
                        'Y-m-t 23:59:59',
                        mktime(0, 0, 0, ($parts[1] + $project->number_month -1), $parts[2] + 1, $parts[0])
                    );

                }else {
                    $first_day = date(
                        'Y-m-01 00:00:00',
                        mktime(0, 0, 0,($parts[1] + ($i*($project->number_month))), $parts[2] +1, $parts[0])
                    );
                    $last_day = date(
                        'Y-m-t 23:59:59',
                        mktime(0, 0, 0, ($parts[1] + ($i*($project->number_month) + $project->number_month -1)), $parts[2] + 1, $parts[0])
                    );
                }
                $input['first_day'] = $first_day;
                $input['last_day'] = $last_day;
                $backedProject = $this->BackedProject->getBackedFanClubById($id,$input);

                $totalNews = count($backedProject);
                $totalPages = ceil($totalNews / $limit);
                $input['limit']  = array($limit,$start);
                $backedProject = $this->BackedProject->getBackedFanClubById($id,$input);
                $total = 0;
                foreach ($backedProject as $key => $value) {
                    var_dump($value);
                    $user = $this->User->getUserId($value->user_id);
                    $value->username = $user->username;
                    $backing = $this->BackingLevel->getBackingId($value->backing_level_id);
                    $value->backing = array(
                        "invest_amount"=>$backing->invest_amount,
                        "return_amount"=>$backing->return_amount,
                        'now_count'=>$backing->now_count,
                        'max_count' =>$backing->max_count,
                        "thumnail"=>$backing->thumnail,
                        'schedule'=>$backing->schedule,
                    );
                    $total = $total + $backing->invest_amount;
                }
                // $total = array(
                //     'collected_amount' => $project->collected_amount,
                // );
                // if($project->goal_amount > $project->collected_amount){
                //     $total['total'] = '-'.($project->goal_amount - $project->collected_amount);
                // }else{
                //     $total['total'] = '+'.($project->collected_amount - $project->goal_amount);
                // }
                var_dump($total);
                $data = array(
                    'page_count'=> $totalPages,
                    'backed'=> $backedProject,
                    'totalProject'=> $total
                );
            }
            exit;



            $this->response($data,200);

        }
    }
    private function fullGoalAmountData(){
        $date = new DateTime();
        $date = $date->format('Y-m-d H:i:s');

        $project_goal = $this->Project->getFullAmount($date);

        foreach ($project_goal['project'] as $key => $value) {
            $value->now_count= array(
              'now_count'=>$value->now_count
            );

            $value->category=array(
                'id'=>$value->category_id,
                'name'=>$value->name,
                'slug'=>$value->slug
            );

            $value->user = array(
                'id'=>$value->user_id,
                'name'=>$value->username,
            );

            $timeProject = getTimeProject($value->collection_end_date);
            $value->format_collection_end_date = $timeProject;
        }
        return $project_goal;
    }
    // Finish Check Id Project exists
    //
    // project goal_amount >= 100%
    public function  full_goal_amount_get(){

        $project_goal = $this->fullGoalAmountData();
        $this->response($project_goal['project'], 200);
    }

    // project goal_amount >= 100%
    private function  fullGoalAmount_data($limit){
        $date = new DateTime();
        $date = $date->format('Y-m-d H:i:s');

        $start = $this->get('page');
        //$limit = '16';
        if(!$start || $start =='0'){
            $start = '0';
        }else{
            $start = $start * $limit;
        }
        // var_dump($a);

        $input['limit'] = array($limit,$start);

        $project_goal = $this->Project->getFullAmount($date,$input);

        foreach ($project_goal['project'] as $key => $value) {

            $value->now_count = array(
                'now_count'=>$value->now_count
            );

            $value->category = array(
                'id'=>$value->category_id,
                'name'=>$value->name,
                'slug'=>$value->slug
            );

            $value->user = array(
                'id'=>$value->user_id,
                'name'=>$value->username,
            );

            $timeProject = getTimeProject($value->collection_end_date);
            $value->format_collection_end_date = $timeProject;
        }

        $totalPages = ceil($project_goal['total'] / $limit);
        $data = array(
            'page_count'=>$totalPages,
            'data'=>$project_goal['project']
        );
        return $data;
    }
    public function  fullGoalAmount_get(){
        $data = $this->fullGoalAmount_data(16);
        $this->response($data, 200);
    }

    private function getAllProjectDataAccessList($user_id){
        $date = new DateTime();
        $start = $this->get('start');
        $limit = '9';
        if(!$start || $start =='0'){
            $start ='0';
        }else{
            $start = $start*$limit;
        }
        $date = $date->format('Y-m-d H:i:s');
      // $input['where'] = array('projects.collection_end_date >=' => $date);
        // $input['where'] = array('opened'=>'yes');
        ////$input['where2'] = array('projects.active'=>'yes');

        $input['join']  = array('access_list', ' projects.id = access_list.project_id');
        $input['where3'] = array('access_list.user_id'=>$user_id);
        $Project = $this->Project->getDataProject($input);
        $totalProject = count($Project);
        $totalPages = ceil($totalProject / $limit);

        $input['limit']  = array($limit,$start);

        $input['order'] = array('projects.collection_end_date','DESC');
        $toppage = $this->Project->getDataProject($input);

        foreach ($toppage as $key => $value) {
            $value->now_count=array(
                'now_count'=> $value->now_count
            );

            $value->category=array(
                'id'=>$value->category_id,
                'name'=>$value->name,
                'slug'=>$value->slug
            );
            $value->user = array(
                'id'=>$value->user_id,
                'name'=>$value->username,
            );

            $timeProject = getTimeProject($value->collection_end_date);
            $value->format_collection_end_date = $timeProject;
        }
        $data = array(
            'page_count'=>$totalPages,
            'data'=>$toppage
        );
        return $data;
    }

    function active_password_get($slug){

        $codeStatus = $this->User->checkActivePassword($slug);
        $myMess = " 新たなパスワードが有効になりました。";
        if($codeStatus==0){
            $myMess = "Active code not found";
        }else{
            if($codeStatus==2){
                $myMess = "新たなパスワードを有効にする期間が切れました。";
            }
        }
        $data = array(
            'status'=>$codeStatus,
            'message'=>$myMess,
        );
        $this->response($data, 200);
    }

}
