<?php

defined('BASEPATH') OR exit('No direct script access allowed');
use \Firebase\JWT\JWT;

class Users extends UserController {

    function __construct()
    {
        parent::__construct();
        $this->auth();
        $this->load->model('ion_auth_model');
    }
    public function GUID()
    {
        if (function_exists('com_create_guid') === true)
        {
            return trim(com_create_guid(), '{}');
        }

        return sprintf('%04X%04X-%04X-%04X-%04X-%04X%04X%04X', mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(16384, 20479), mt_rand(32768, 49151), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535));
    }
    /**
     * [user_profile_get Get User Current folow Auth(MY_Controller)]
     * @return [type] [description]
     */
    public function user_profile_get() {
        // Get Auth -> Get User_id
        $user = $this->user_data;
        $user_id =  $user->id;
        // Get Profile
        $curUser =  $this->User->getUserId($user_id);
        if($curUser->active === '0') {
            $error = array(
                'block' => $curUser->username.'様はアカウント停止中です。'
            );
            $this->response($error,400);
        }
        if(isset($curUser) && $curUser) {
            // Get Group Current(ion_auth)
            // test
            $group = $this->ion_auth->get_users_groups($user_id)->row();
            // Get Group Name
            $group_name = $group->name;
            // Count MY Project
            $numberProject = $this->Project->getProjectUserId($user_id);

            $numberPatron = $this->BackedProject->countBackedInfoDonate($user_id);
            // Create Object
            $safeUserObject = null;

            $listAddress = $this->AddressUser->getAddressUserId($user_id);

            $FormatImage = getImage($curUser->profileImageURL);

            $numberNotification = $this->Notification_info->getNumberNotification($user_id);

            $listFan = $this->ListDonateFanClub->getAllDonateFanClub($user_id);
            $numberPatronfanclub = $listFan['total'];
            $listFanClub = '';

            foreach ($listFan['data'] as $key => $value) {
                if($value->status_join_fanclub === '0'){
                    if($listFanClub === '') {
                        $listFanClub = $value->backing_levels_id;
                    }else{
                        $listFanClub = $listFanClub.','.$value->backing_levels_id;
                    }
                }
            }

            $safeUserObject = array(
                "id"                 =>     $curUser->id,
                "username"           =>     $curUser->username,
                "roles"              =>     $group_name,
                'profileImageURL'    =>     $curUser->profileImageURL,
                'medium_profileImageURL' => $FormatImage['medium'],
                'small_profileImageURL' =>  $FormatImage['small'],
                'created'            =>     $curUser->created_on,
                'sex'                =>     $curUser->sex,
                'birthday'           =>     $curUser->birthday,
                'self_description'   =>     $curUser->self_description,
                'url1'               =>     $curUser->url1,
                'url2'               =>     $curUser->url2,
                'url3'               =>     $curUser->url3,
                'address'            =>     $curUser->address,
                'job'                =>     $curUser->job,
                'email'              =>     $curUser->email,
                'bank_number'        =>     $curUser->bank_number,
                'bank_owner'         =>     $curUser->bank_owner,
                'bank_name'          =>     $curUser->bank_name,
                'bank_branch'        =>     $curUser->bank_branch,
                'bank_type'          =>     $curUser->bank_type,
                'country'            =>     $curUser->country,
                'eventProjectSocket' =>     $curUser->eventProjectSocket,
                'numberProject'      =>     count($numberProject),
                'patron'             =>     count($numberPatron),
                'patronfanclub'      =>     $numberPatronfanclub,
                'listFanClub'        =>     $listFanClub,
                'tokenRefest'        =>     $this->user_data->tokenRefest,
                'listAddress'        =>     $listAddress,
                'number_notification'=>     $numberNotification,
                'statusUnsubscribe'  =>     $curUser->unsubscribe,
                'unsubscribeProject' =>     $curUser->unsubscribeProject,
                'wish_list'          =>     $curUser->wish_list,
            );
            // check exists customer_id
            if(isset($curUser->customer_id) && $curUser->customer_id ) {
                $safeUserObject['number_card'] = '************'.$curUser->last4;
                $safeUserObject['exp_month'] = $curUser->card_month;
                $safeUserObject['exp_year'] = substr($curUser->card_year, -2);
                $safeUserObject['brand'] = $curUser->card_brand;
                $safeUserObject['cvc'] = $curUser->cvc;
            }else{
                $safeUserObject['number_card'] = '';
                $safeUserObject['exp_month'] = '';
                $safeUserObject['exp_year'] = '';
                $safeUserObject['brand'] = '';
                $safeUserObject['cvc'] = '';
            }

            $this->response($safeUserObject, 200);

        } else {

            $error = array('status'=>'user is not exists!');
            $this->response($error, 404);

        }
    }

    /**
     * Change password
     */
    public function changePass_put() {
        // get info user
        $user = $this->user_data;
        $user_id =  $user->id;
        // config for form_validation
        $config = [
            [
                'field' => 'password',
                'label' => 'PassWord',
                'rules' => 'min_length[6]',
                'errors' => [
                    'min_length' => '新しいパスワード（英数字6文字以上）',
                ],
            ],
            [
                'field' => 're_password',
                'label' => 'Re Password',
                'rules' => 'matches[password]',
                'errors' => [
                    'matches' => '新しいパスワード（確認）',
                ],
            ]
        ];



        // config of form validation
        $data = $this->put();
        $data['password'] = $this->getPara('password');
        $data['re_password'] = $this->getPara('re_password');

        ////$updatePassWord = $this->ion_auth->update($user_id, $data);

        //$this->form_validation->set_data($data);
        //$this->form_validation->set_rules($config);
        // form_validation  = true
        //if ($this->form_validation->run() !== false) {
        if (true) {

            $curUser =  $this->User->getUserId($user_id);
            // assign password
            $myGuid =  $this->GUID();
            $data_up['new_password'] =$this->getPara('password');// $this->ion_auth_model->hash_password($this->getPara('password'),$curUser->salt);
            $data_up['active_new_password_code'] = $myGuid;
            $date = date("Y-m-d");

            $mod_date = strtotime($date."+ 3 days");
            $myExpiredDate =  date("Y-m-d",$mod_date) ;
            $data_up['expired_date'] = $myExpiredDate;
            //$old_password = $this->getPara('old_password');


            $email = $curUser->email;
            /*if (false === $this->ion_auth->login($email,$old_password)) {
               $error['status'] = '入力された旧パスワードが正しくありません。!';

                $this->response($error,400);

                return;
            }*/
            // update password
            $updatePassWord = $this->ion_auth->update($user_id, $data_up);
           // $edit = $this->User->editUser($user_id,$data);

            // if success
            if($updatePassWord) {

                // annouce + 200
                $success = array(
                    'status'=>'update success!'
                );

                $dataTemplate = array(

                    'curUser'=> $curUser,
                    'myGuid'=>$myGuid,
                    'myExpiredDate'=>$myExpiredDate

                );

                $this->load->library('email');
                $this->email->from('admin@kakuseida.conoha.io', 'KAKUSEIDA');
                $message = $this->load->view($this->config->item('email_templates', 'ion_auth') . $this->config->item('change_password', 'ion_auth'),$dataTemplate , TRUE);
               // $this->email->clear();
               // $this->email->from($this->config->item('admin_email', 'ion_auth'), $this->config->item('title_change_password', 'ion_auth'));
                $this->email->to($email);
                $this->email->subject($this->config->item('title_change_password', 'ion_auth') );
                $this->email->message($message);

                if ($this->email->send())
                {
                    $success = array(
                        'status'=>'update success!'
                    );
                    $this->response($success,200);

                }
                else
                {
                    $success = array(
                        'status'=>'update success!Email Fail'
                    );
                    $this->response($success,200);

                }

                $this->response($success,200);

            } else {

                // annouce error + 400
                $error = array(
                    'status'=> 'update failed!'
                );
                $this->response($error,400);

            }

        }else{

            $error = $this->form_validation->error_array();
            $this->response($error,400);

        }
    }

    /**
     * [subsidized_project_get get project donate]
     * @return [type] [description]
     */
    public function subsidized_project_get() {
        // get pagination
        $start = $this->get('start');
        $limit = '16';
        if(!$start || $start =='0'){
            $start ='0';
        }else{
            $start = $start*$limit;
        }
        // get info user
        $user = $this->user_data;
        $user_id =  $user->id;
        // get curUser begin $user_id
        $curUser =   $this->User->getUserId($user_id);
        // get project i donate
        $countProject = $this->BackedProject->dataBackedProject($user_id);
        //  count project it and assign = $totalNews
        $totalNews = count($countProject);
        // Opera $totalNews vs $limit
        $totalPages = ceil($totalNews / $limit);
        // input limit for pagination
        $input['limit']  = array($limit,$start);
        // get project donate
        $projectSubsidized = $this->BackedProject->dataBackedProject($user_id,$input);
        //
        $categories = $this->Category->getCategory();
        $user = $this->User->getUser();

        foreach ($projectSubsidized as $key => $value) {
            $backing= $this->BackingLevel->getBackingLevelId($value->id);
            $now_count = 0;
            foreach ($backing as $key => $value2) {
              $now_count = $now_count + $value2->now_count;
              $value->now_count=array(
                'now_count'=>$now_count
              );
            }
            $value->now_count=array(
              'now_count'=>$now_count
            );

                   foreach($categories as $key => $data){
              if($value->category_id == $data->id){
                  $value->category=array(
                    'id'=>$data->id,
                    'name'=>$data->name,
                    'slug'=>$data->slug
                  );
              }
            }

            foreach($user as $key => $data){
                if($value->user_id == $data->id){
                    $value->user = array(
                    'id'=>$data->id,
                    'name'=>$data->username,
                    );
                }
            }
            $timeProject = $this->getTime_get($value->id);
            $value->format_collection_end_date = $timeProject;
        }
        if($curUser){
            $data = array(
              'page_count'=>$totalPages,
              'subsidized_project'=>$projectSubsidized
            );
        }
        $this->response($data, 200);

    }
    /**
     * [post_project_get get project post]
     * @return [type] [description]
     */
    public function post_project_get() {
        // get start
        $start = $this->get('start');
        // get limit
        $limit = '16';
        if(!$start || $start =='0'){
          $start ='0';
        }else{
          $start = $start*$limit;
        }
        // get user
        $user = $this->user_data;
        $user_id =  $user->id;
        $curUser =   $this->User->getUserId($user_id);
        // get number project
        $numberProject = $this->Project->getAllProjectJoinCat($user_id);
        // get total
        $totalNews = count($numberProject);
        // ciel total => number_page
        $totalPages = ceil($totalNews / $limit);
        // get limit
        $input['limit'] = array($limit,$start);
        // get project use limit
        $projectPost = $this->Project->getAllProjectJoinCat($user_id,$input);
        // for projectPost
        foreach ($projectPost as $key => $value) {
            // get image
            $FormatImage = getImage($value->thumbnail);
            $value->meidum_thumbnail = $FormatImage['medium'];
            // now count
            $value->now_count  = array(
                'now_count' => $value->now_count
            );
            // category
            $value->category=array(
                'id'=>$value->category_id,
                'name'=>$value->name,
                'slug'=>$value->slug
            );
            // user
            $value->user = array(
                'id'=>$value->user_id,
                'name'=>$value->username,
            );
            // time exisits project
            $timeProject = getTimeProject($value->collection_end_date);
            $value->format_collection_end_date = $timeProject;
        }
        // check exists user
        if($curUser) {
            // show data
            $data = array(
                'page_count'=>$totalPages,
                'post_project'=>$projectPost
            );
        }
        $this->response($data, 200);
    }

    // // Check Number Bank of your project
    // public function checkNumberBank_post($bank_number) {
    //     $user = $this->user_data;
    //     $user_id =  $user->id;
    //     $checkBank  =  $this->User->getUsersBankNumber($bank_number);
    //     if(isset($checkBank) && $checkBank){
    //         if($checkBank->id === $user_id){
    //             return true;
    //         }else{
    //             return false;
    //         }
    //     }else{
    //         return true;
    //     }
    // }
    //
    public function checkNumberStripe_post($numberStripe,$cvc){
        $kunci = $this->config->item('thekey');

        $user = $this->user_data;
        $user_id =  $user->id;

        $count_length = strlen($numberStripe);
        $count_length = $count_length - 4;

        $last4 = substr($numberStripe,$count_length);

        $getBlackList = $this->BlackList->getCard($last4);
        // var_dump($getBlackList);
        $status = 0;
        if(isset($getBlackList) && $getBlackList) {

            foreach ($getBlackList as $key => $value) {

                $token = $value->hash_token;

                try {
                    $decoded = JWT::decode($token, $kunci, array('HS256'));
                    $result = strpos($decoded, "/") + 1;
                    $checkcvc = substr($decoded, $result);

                    if((int)$cvc !== (int)$checkcvc){
                        $status = $status + 0;
                    }else{
                        $status = $status + 1;
                    }
                } catch (Exception $e) {
                    $status = $status + 0;
                }
            }

            if($status > 0){
                return false;
            }else {
                return true;
            }
        }else {
            return true;
        }

    }

    public function editUser_put() {
        $this->load->library('form_validation');
        $user = $this->user_data;
        $user_id =  $user->id;
        $curUser =   $this->User->getUserId($user_id);
        $curEmail = $curUser->email;
        $image = $this->put('profileImageURL');

        if(!isset($image)){
            $image = $curUser->profileImageURL;
        }

        $data = array(
            'username'=>$this->put('username'),
            'address' =>$this->put('address'),
            'country'=>$this->put('country'),
            'job'=>$this->put('job'),
            'sex'=>$this->put('sex'),
            'phone'=>$this->put('phone'),
            'profileImageURL'=>$image,
            'address_return'=>$this->put('address_return'),
            'birthday'=>$this->put('birthday'),
            'name_return'=>$this->put('name_return'),
            'self_description'=>$this->put('self_description'),
            'url1'=>$this->put('url1'),
            'url2'=>$this->put('url2'),
            'url3'=>$this->put('url3')
        );

        $bank_number = $this->put('bank_number');
        $bank_owner = $this->put('bank_owner');
        $bank_name = $this->put('bank_name');
        $bank_branch = $this->put('bank_branch');
        $bank_type = $this->put('bank_type');

        if(isset($bank_number) && isset($bank_owner) && isset($bank_name) && isset($bank_branch) && isset($bank_type)){
            $data['bank_number'] = $bank_number;
            $data['bank_owner'] = $bank_owner;
            $data['bank_name'] = $bank_name;
            $data['bank_branch'] = $bank_branch;
            $data['bank_type'] = $bank_type;
        }

        $edit = $this->User->editUser($user_id,$data);

        if($edit) {
            $infoUser = $this->profileUser_get($user_id);
            $this->response($infoUser,200);

        }else{
            $data = array(
                'status'=>'エラー！プロフィールの変更がまだできません。',
            );
            $this->response($data,400);
        }
    }


    public function editStripe_put() {

        $user = $this->user_data;

        $user_id =  $user->id;

        $curUser =   $this->User->getUserId($user_id);
        $curEmail = $curUser->email;
        //nvtrong add fix bugs 2019 09 02
        $data = array(
            'email' => $curEmail,
        );
        //nvtrong END
        $kunci = $this->config->item('thekey');

        $dataBase64 = $this->put('data');
        $Hash_Data  = Hash_Data($dataBase64);

        $numberCard = $Hash_Data['number_card'];
        $exp_month  = $Hash_Data['exp_month'];
        $exp_year   = $Hash_Data['exp_year'];
        $cvc        = $Hash_Data['cvc'];

        if(isset($numberCard) && isset($exp_month)
        && isset($exp_year) && isset($cvc)){
            $user = $this->user_data;
            $user_id =  $user->id;

            $checkBlackList = $this->checkNumberStripe_post($numberCard,$cvc);

            if($checkBlackList) {
                require_once APPPATH."third_party/stripe/init.php";
                $key = $this->Setting->getSetting();
                \Stripe\Stripe::setApiKey($key->secret_key);


                try {
                    if(isset($cvc) && $cvc){
                        $stripeToken = \Stripe\Token::create(array(
                            "card" => [
                                "number" => $numberCard,
                                "exp_month" => $exp_month,
                                "exp_year" => $exp_year,
                                "cvc" => $cvc
                            ]
                        ));

                        /* test
                        $stripeToken = new stdClass();
                        $stripeToken->id = '8888';
                        */


                        if(isset($stripeToken->id)){
                            try {
                                $customer = \Stripe\Customer::create(array(
                                    'email' => $curEmail,
                                    'source'  => $stripeToken->id
                                ));
                                $data['customer_id'] = $customer->id;

                                $customer = \Stripe\Customer::retrieve($customer->id);

                                $customerId = $customer['default_source'];
                                $card = $customer->sources->retrieve($customerId);
                                $code = $customerId.'/'.$cvc;
                                $output = JWT::encode($code, $kunci);
                                $data['last4'] = $card->last4;
                                $data['card_month'] = $card->exp_month;
                                $data['card_year'] = $card->exp_year;
                                $data['card_brand'] = $card->brand;
                                $data['hash_token'] = $output;
                                //////nvtrong tesst tessttessttessttesst
                               /*
                                $data['customer_id'] = "cus_EtTBmc7dTestTest";
                                $customerId = "cus_EtTBmc7dTestTest";
                                $card = new stdClass();
                                $card->last4 = '8888';
                                $card->exp_month = '02';
                                $card->exp_year = '2024';
                                $card->brand = 'MasterCard';

                                $code = $customerId.'/'.$cvc;
                                $output = JWT::encode($code, $kunci);
                                $data['last4'] = $card->last4;
                                $data['card_month'] = $card->exp_month;
                                $data['card_year'] = $card->exp_year;
                                $data['card_brand'] = $card->brand;
                                $data['hash_token'] = $output;
                               */
                                //////////////////END test

                            } catch(Stripe_CardError $e) {
                                $error1 = $e->getMessage();
                                $error = array(
                                    'status'=>$error1
                                );
                                $this->response($error,400);
                            } catch (Stripe_InvalidRequestError $e) {
                                // Invalid parameters were supplied to Stripe's API
                                $error2 = $e->getMessage();
                                $error = array(
                                    'status'=>$error2
                                );
                                $this->response($error,400);
                            } catch (Stripe_AuthenticationError $e) {
                                // Authentication with Stripe's API failed
                                $error3 = $e->getMessage();
                                $error = array(
                                    'status'=>$error3
                                );
                                $this->response($error,400);
                            } catch (Stripe_ApiConnectionError $e) {
                                // Network communication with Stripe failed
                                $error4 = $e->getMessage();
                                $error = array(
                                    'status'=>$error4
                                );
                                $this->response($error,400);
                            } catch (Stripe_Error $e) {
                                // Display a very generic error to the user, and maybe send
                                // yourself an email
                                $error5 = $e->getMessage();
                                $error = array(
                                    'status'=>$error5
                                );
                                $this->response($error,400);
                            } catch (Exception $e) {
                                // Something else happened, completely unrelated to Stripe
                                $errrr = json_decode($e->getHttpBody());
                                if($errrr->error->code === "token_already_used"){
                                    $error = array(
                                        'status'=>'このクレジットカード情報は存在しました。'
                                    );
                                }else if($errrr->error->code === "email_invalid"){
                                    $error = array(
                                        'status'=>'メールアドレスを正しく入力してください'
                                    );
                                }else if($errrr->error->code === "card_declined"){
                                    $error = array(
                                        'status'=>'クレジットカードの情報は正しくないので、確認してください'
                                    );
                                }else{
                                    $error = array(
                                        'status'=>'クレジットカードの情報は正しくないので、確認してください'
                                    );
                                }
                                $this->response($error,400);
                            }
                        }
                    }else{
                        $error = array(
                            'status'=>'CVCを記入してください。'
                        );
                        $this->response($error,400);
                    }

                } catch(Stripe_CardError $e) {
                    $error1 = $e->getMessage();
                    $error = array(
                        'status'=>$error1
                    );
                    $this->response($error,400);
                } catch (Stripe_InvalidRequestError $e) {
                    // Invalid parameters were supplied to Stripe's API
                    $error2 = $e->getMessage();
                    $error = array(
                        'status'=>$error2
                    );
                    $this->response($error,400);
                } catch (Stripe_AuthenticationError $e) {
                    // Authentication with Stripe's API failed
                    $error3 = $e->getMessage();
                    $error = array(
                        'status'=>$error3
                    );
                    $this->response($error,400);
                } catch (Stripe_ApiConnectionError $e) {
                    // Network communication with Stripe failed
                    $error4 = $e->getMessage();
                    $error = array(
                        'status'=>$error4
                    );
                    $this->response($error,400);
                } catch (Stripe_Error $e) {
                    // Display a very generic error to the user, and maybe send
                    // yourself an email
                    $error5 = $e->getMessage();
                    $error = array(
                        'status'=>$error5
                    );
                    $this->response($error,400);
                } catch (Exception $e) {
                    // Something else happened, completely unrelated to Stripe
                    $errrr = json_decode($e->getHttpBody());
                    if($errrr->error->code === "invalid_expiry_month"){
                        $error = array(
                            'status'=>'有効期限の年を正しく入力してください'
                        );
                        $this->response($error,400);
                    }else if($errrr->error->code === "incorrect_number"){
                        $error = array(
                            'status'=>'カード番号は正しくありません。'
                        );
                        $this->response($error,400);
                    }else if($errrr->error->code === "invalid_expiry_year"){
                        $error = array(
                            'status'=>'有効期限の年を正しく入力してください'
                        );
                        $this->response($error,400);
                    }else if($errrr->error->code === "invalid_cvc"){
                        $error = array(
                            'status'=>'CVCを正しく入力してください'
                        );
                        $this->response($error,400);
                    }else if($errrr->error->code === "expired_card"){
                        $error = array(
                            'status'=>'有効期限の年を正しく入力してください'
                        );
                        $this->response($error,400);
                    }else if($errrr->error->code === "incorrect_cvc"){
                        $error = array(
                            'status'=>'CVCを正しく入力してください'
                        );
                        $this->response($error,400);
                    }

                }
            }else {
                $error = array(
                    'status'=>'このクレジットカード情報はブロックされたユーザーのですので、別のクレジットカード情報を利用してください'
                );
                $this->response($error,400);
            }

        }else{
            $error = array(
                'status'=>'このクレジットカードが存在しました。'
            );
            $this->response($error,400);
        }



        $edit = $this->User->editUser($user_id,$data);
        if($edit){
            $curUser = $this->User->getUserId($user_id);
            $group = $this->ion_auth->get_users_groups($user_id)->row();
            $group_name = $group->name;

            $listAddress = $this->AddressUser->getAddressUserId($user_id);

            $FormatImage = getImage($curUser->profileImageURL);

            $numberNotification = $this->Notification_info->getNumberNotification($user_id);

            $safeUserObject = array(
                "id"=>$curUser->id,
                "username"=>$curUser->username,
                'name_return'=>$curUser->name_return,
                "address"=>$curUser->address,
                "roles"=>$group_name,
                'profileImageURL' =>$curUser->profileImageURL,
                'medium_profileImageURL' =>  $FormatImage['medium'],
                'small_profileImageURL' =>  $FormatImage['small'],
                'created' =>$curUser->created_on,
                'sex' =>$curUser->sex,
                'birthday' =>$curUser->birthday,
                'self_description' =>$curUser->self_description,
                'url1' =>$curUser->url1,
                'url2' =>$curUser->url2,
                'postcode'=>$curUser->postcode,
                'url3' =>$curUser->url3,
                'job'=>$curUser->job,
                'email'=>$curUser->email,
                'country'=>$curUser->country,
                'phone'=>$curUser->phone,
                'bank_number'=>$curUser->bank_number,
                'bank_owner'=>$curUser->bank_owner,
                'bank_name'=>$curUser->bank_name,
                'bank_branch'=>$curUser->bank_branch,
                'bank_type'=>$curUser->bank_type,
                'postcode'=>$curUser->postcode,
                'eventProjectSocket' =>$curUser->eventProjectSocket,
                'address_return'=>$curUser->address_return,
                'listAddress'=>$listAddress,
                'number_notification'=>     $numberNotification,
                'statusUnsubscribe'  =>     $curUser->unsubscribe,
                'unsubscribeProject' =>     $curUser->unsubscribeProject,
                'number_card'   =>  '************'.$curUser->last4,
                'exp_month'     =>  $curUser->card_month,
                'exp_year'      =>  $curUser->card_year,
                'brand'         =>  $curUser->card_brand,
                'cvc'           =>  $cvc
            );

            $this->response($safeUserObject,200);
        }else{
            $data = array(
                'status'=>'エラー！プロフィールの変更がまだできません。',
            );
            $this->response($data,400);
        }
    }

    /**
    * [createAddressUser_post Create Address for User]
    * @return [type] [Create Address]
    */
    public function createAddressUser_post(){
        // get User Id = auth
        $user = $this->user_data;
        $user_id =  $user->id;
        // get date
        $date = new DateTime();
        //fomat date
        $date = $date->format('Y-m-d H:i:s');
        // config of form_validation
        $config = [
            [
                'field' => 'email',
                'label' => 'Email',
                'rules' => 'required|valid_email',
                'errors' => [
                  'required' => 'メールアドレスを入力してください',
                  'valid_email' => 'メールアドレスが正しくないので、確認してください',
                ],
            ],
            [
                'field' => 'postcode',
                'label' => 'Post Coe',
                'rules' => 'required',
                'errors' => [
                  'required' => 'Please input PostCode',
                ],
            ],
            [
                'field' => 'address_return',
                'label' => 'Address Return',
                'rules' => 'required',
                'errors' => [
                  'required' => 'Please input Address Return',
                ],
            ],
            [
                'field' => 'phone',
                'label' => 'Phone',
                'rules' => 'required',
                'errors' => [
                  'required' => 'Please input Phone ',
                ],
            ],
            [
                'field' => 'name_return',
                'label' => 'Name Return',
                'rules' => 'required',
                'errors' => [
                  'required' => 'Please input Name Return ',
                ],
            ],
        ];
        // set data and rules for form_validation
        $data = $this->post();
        $this->form_validation->set_data($data);
        $this->form_validation->set_rules($config);
        // Validation = true
        if ($this->form_validation->run() !== false) {
            // $data for create AddressUser
            $data = array(
                'user_id'         => $user_id,
                'email'           =>$this->post('email'),
                "postcode"        =>$this->post('postcode'),
                "address_return"  =>$this->post('address_return'),
                "phone"           =>$this->post('phone'),
                "name_return"     =>$this->post('name_return'),
                "chosen_default"  =>0,
                "created"         =>$date
            );
            // Create = model
            $createAddress = $this->AddressUser->createAddressUser($data);
            // Success
            if($createAddress){
                // Return data
                $data['id'] = $createAddress;
                $this->response($data,200);
                // Error
            }else{
                // Return error
                $error = array(
                    'status'=>'Create Address User fail!'
                );
                $this->response($error,400);
            }
            // Form Validation error
        }else{
            // Return error of form validation
            $error = $this->form_validation->error_array();
            $this->response($error,400);
        }
    }

    /**
    * [chosenDefault_get Chosen 1 address is default for user]
    * @return [type] [description]
    */
    public function chosenDefault_get(){
        // Get id ( id of address_user)
        $id = $this->get('id');
        // check $id is exists
        if(isset($id) && $id){
            // Get User_id from Auth
            $user = $this->user_data;
            $user_id =  $user->id;
            // Get Address curent from address_user
            $address = $this->AddressUser->getAddressUserId($user_id,$id);
            // Check $address
            if(isset($address) && $address){
                // Check $address->user_id === $user_id
                if($address->user_id === $user_id){
                    $listAddress = $this->AddressUser->getAddressUserId($user_id);
                    foreach ($listAddress as $key => $value) {
                        if($value->id === $id){
                            $data = array(
                                'chosen_default' => 1
                            );
                            $update = $this->AddressUser->editAddressUser($value->id,$data);
                        }else{
                            $data = array(
                                'chosen_default' => 0
                            );
                            $update = $this->AddressUser->editAddressUser($value->id,$data);
                        }
                    }
                    $newListAddress = $this->AddressUser->getAddressUserId($user_id);
                    $this->response($newListAddress,200);
                }else{
                    $error = array(
                        'status' => 'id nay khong thuoc user hien tai'
                    );
                    $this->response($error,400);
                }
            }else{
                $error = array(
                    'status' => 'This id is ánot exists!'
                );
                $this->response($error,400);
            }
        }else{
            $error = array(
                'status' => 'This id is not exists!'
            );
            $this->response($error,400);
        }
    }

    public function listAddressUser_get(){
        $user = $this->user_data;
        $user_id =  $user->id;
        $curUser =   $this->User->getUserId($user_id);
        if(isset($curUser) && $curUser){
            $listAddress = $this->AddressUser->getAddressUserId($user_id);
            $this->response($listAddress,200);
        }else{
            $error = array(
                'status' => 'This id is not exists!'
            );
            $this->response($error,400);
        }
    }

    public function editAddressUser_put(){
        $id = $this->put('id');
        if(isset($id) && $id){
            $user = $this->user_data;
            $user_id =  $user->id;
            $curUser =   $this->User->getUserId($user_id);
            if(isset($curUser) && $curUser){
                $checkAddressUser = $this->AddressUser->getAddressUserId($user_id,$id);
                if(isset($checkAddressUser) && $checkAddressUser){
                    $config = [
                        [
                            'field' => 'email',
                            'label' => 'Email',
                            'rules' => 'required|valid_email',
                            'errors' => [
                              'required' => 'メールアドレスを入力してください',
                              'valid_email' => 'メールアドレスが正しくないので、確認してください',
                            ],
                        ],
                        [
                            'field' => 'postcode',
                            'label' => 'Post Coe',
                            'rules' => 'required',
                            'errors' => [
                              'required' => 'Please input PostCode',
                            ],
                        ],
                        [
                            'field' => 'address_return',
                            'label' => 'Address Return',
                            'rules' => 'required',
                            'errors' => [
                              'required' => 'Please input Address Return',
                            ],
                        ],
                        [
                            'field' => 'phone',
                            'label' => 'Phone',
                            'rules' => 'required',
                            'errors' => [
                              'required' => 'Please input Phone ',
                            ],
                        ],
                        [
                            'field' => 'name_return',
                            'label' => 'Name Return',
                            'rules' => 'required',
                            'errors' => [
                              'required' => 'Please input Name Return ',
                            ],
                        ],
                    ];
                    $data = $this->put();
                    $this->form_validation->set_data($data);
                    $this->form_validation->set_rules($config);
                    if ($this->form_validation->run() !== false) {
                        $data = array(
                            'email' => $this->put('email'),
                            'postcode' => $this->put('postcode'),
                            'address_return' => $this->put('address_return'),
                            'phone' => $this->put('phone'),
                            'name_return' => $this->put('name_return'),
                        );

                        $updateAddressUser = $this->AddressUser->editAddressUser($id,$data);
                        if($updateAddressUser){
                            $getAddressUser = $this->AddressUser->getAddressUserId($user_id,$id);
                            $this->response($getAddressUser,200);
                        }else{
                            $error = array(
                                'status' => 'Update AddressUser failed!'
                            );
                            $this->response($error,400);
                        }

                    }else{
                        $error = $this->form_validation->error_array();
                        $this->response($error,400);
                    }
                }else{
                    $error = array(
                        'status' => 'id này không thuộc user hiện tại'
                    );
                    $this->response($error,404);
                }
            }else{
                $error = array(
                    'status' => 'user không tồn tại'
                );
                $this->response($error,404);
            }
        }else{
            $error = array(
                'status' => 'id này không tồn tại'
            );
            $this->response($error,404);
        }
    }


    public function deleteAddressUser_delete($id){
        if(isset($id) && $id){
            $user = $this->user_data;
            $user_id =  $user->id;
            $curUser =   $this->User->getUserId($user_id);
            if(isset($curUser) && $curUser){
                $checkAddressUser = $this->AddressUser->getAddressUserId($user_id,$id);
                if(isset($checkAddressUser) && $checkAddressUser){
                    $checkDonate = $this->BackedProject->checkAddressDonate($id);
                    $checkDefault = $this->AddressUser->getAddressUserDefault($user_id,$id);
                    if(isset($checkDonate) && $checkDonate){
                        $error = array(
                            'status' => 'Address này đã có donate nên bạn không thể xóa!'
                        );
                        $this->response($error,400);
                    }else if(isset($checkDefault) && $checkDefault){
                        $error = array(
                            'status' => 'AddressUser này đang là mặc định, vui lòng thay đổi mặc định để xóa!'
                        );
                        $this->response($error,400);
                    }else{
                        $delete = $this->AddressUser->deleteAddressUser($id);
                        if(isset($delete) && $delete){
                            $success = array(
                                'status' => 'Delete AddressUser success!'
                            );
                            $this->response($success,200);
                        }else{
                            $error = array(
                                'status' => 'Delete AddressUser failed!'
                            );
                            $this->response($error,400);
                        }
                    }
                }else{
                    $error = array(
                        'status' => 'id này không thuộc user hiện tại'
                    );
                    $this->response($error,404);
                }
            }else{
                $error = array(
                    'status' => 'id này không tồn tại'
                );
                $this->response($error,404);
            }
        }else{
            $error = array(
                'status' => 'id này không tồn tại'
            );
            $this->response($error,404);
        }
    }

    public function createProject_post(){
        $user = $this->user_data;
        $user_id =  $user->id;
        //Get Date current
        $config = [
            [
                'field' => 'project_name',
                'label' => 'Project Name',
                'rules' => 'required|max_length[150]',
                'errors' => [
                    'required' => 'プロジェクト名を入力してください。',
                    'max_length' => 'プロジェクト名は最大150文字までです。',
                ],
            ],
            [
                'field' => 'description',
                'label' => 'description',
                'rules' => 'max_length[210]',
                'errors' => [
                    'max_length' => '「はじめにご挨拶」の文書は最大255文字までです。',
                ],
            ],
            [
                'field' => 'category_id',
                'label' => 'Category',
                'rules' => 'required',
                'errors' => [
                    'required' => 'プロジェクト名を入力してください。',
                ],
            ],
            [
                'field' => 'video',
                'label' => 'video',
                'rules' => 'valid_url',
                'errors' => [
                    'valid_url' => '動画のURLを入力してください',
                ],
            ],
            [
                'field' => 'end_date',
                'label' => 'End Date',
                'rules' => 'required',
                'errors' => [
                    'required' => '募集終了日を入力してください ',
                ],
            ],
            [
                'field' => 'goal_amount',
                'label' => 'Goal Amount',
                'rules' => 'required|min_length[6]',
                'errors' => [
                    'required' => '目標金額を入力してください',
                    'min_length'=>'目標金額は、10,000円以上の金額で入力してください。'
                ],
            ],
        ];
        $data = $this->post();
        $this->form_validation->set_data($data);
        $this->form_validation->set_rules($config);

        if ($this->form_validation->run() !== false) {
            //Get Date current
            $date = new DateTime();
            //fomat date
            $date = $date->format('Y-m-d H:i:s');
            // listed data with $this->post
            $project_name = $this->post('project_name');
            $category = $this->post('category_id');
            $summary = $this->post('description');
            $radio = $this->post('thumbnail_type');
            // If radio == 1 is Video
            // Add $video
            $video = $this->post('video');

            if(isset($video)){
                $video = null;
            }
            $thumbnail1 = $this->post('thumbnail_description1');
            $thumbnail2 = $this->post('thumbnail_description2');
            $thumbnail3 = $this->post('thumbnail_description3');

            if(!isset($thumbnail1)){
                $thumbnail1 = base_url().'images/2018/default/noimage-01.png';
            }
            if(!isset($thumbnail2)){
                $thumbnail2 = base_url().'images/2018/default/noimage-01.png';
            }
            if(!isset($thumbnail3)){
                $thumbnail3 = base_url().'images/2018/default/noimage-01.png';
            }
            $dateclose = $this->post('end_date');
            $goal_amount = $this->post('goal_amount');
            $goal_amount = preg_replace('/,/', '', $goal_amount);
            $project_content = $this->post('project_content');
            $thumbnail = $this->post('thumbnail');
            if(!isset($thumbnail)){
                $thumbnail = base_url().'images/2018/default/noimage-01.png';
            }
            // Add Data listed on $data
            $data_project = array(
                'project_name' => $project_name,
                'category_id' => $category,
                'description' => $summary,
                'goal_amount' => $goal_amount,
                'thumbnail_descrip1'=> $thumbnail1,
                'thumbnail_descrip2'=> $thumbnail2,
                'thumbnail_descrip3'=> $thumbnail3,
                'thumbnail_type'=>$radio,
                'thumbnail_movie_code'=>$video,
                'collection_start_date'=>$date,
                'collection_end_date'=>$dateclose,
                'user_id' => $user_id,
                'active' => 'no',
                'created' => $date,
                'thumbnail'=>$thumbnail,
                'project_content'=>$project_content
            );
            $project = $this->Project->addProject($data_project);
            $data_project['id'] = $project;
            if($project){
                $date = new DateTime();
                //fomat date
                $date = $date->format('Y-m-d H:i:s');
                $data = $this->post('project_return');
                $countdata = count($data);
                if($countdata >= 1){
                    for ($i=0; $i < $countdata ; $i++) {
                        $data[$i]['project_id'] = $project;
                        if(!isset($data[$i]['schedule'])){
                            $data[$i]['schedule']  = null;
                        }
                        if(!isset($data[$i]['thumnail'])) {
                            $data[$i]['thumnail']  = base_url().'images/2018/default/noimage-01.png';
                        }
                        $data[$i]['now_count'] = 0;
                        $data[$i]['created'] = $date;
                    }
                    $this->db->insert_batch('backing_levels', $data);
                }
                $this->response($data_project,200);
            }
        }else{
            $error = $this->form_validation->error_array();
            $this->response($error,400);
        }
    }

    public function project_detail_get(){
        $id = $this->get('id');
        $page = $this->get('page');
        $project = $this->Project->getProjectIdJoin($id);
        $user = $this->user_data;
        $user_id =  $user->id;
        if(!$project) {
            $data = array(
                'status'=>'error!',
            );
            $this->response($data,404);
        }else{
            if($project->user_id != $user_id ){
                $data = array(
                    'status'=>'error!',
                );
                $this->response($data,404);
            }

            $project->category=array(
                'id'=>$project->category_id,
                'name'=>$project->name,
                'slug'=>$project->slug
            );

            $project->user = array(
                'id'=>$project->user_id,
                'name'=>$project->username,
                'address'=>$project->address,
                'job'=>$project->job,
                'profileImageURL'=>$project->profileImageURL
            );

            $project->thumbnail_img = array();

            if($project->thumbnail_descrip1){
                $project->thumbnail_img[]= array(
                    'type'=>'image',
                    'thumbnail_descrip'=>$project->thumbnail_descrip1
                );
            }

            if($project->thumbnail_descrip2){

                $project->thumbnail_img[]= array(
                    'type'=>'image',
                    'thumbnail_descrip'=>$project->thumbnail_descrip2
                );
            }

            if($project->thumbnail_descrip3){

                $project->thumbnail_img[]= array(
                    'type'=>'image',
                    'thumbnail_descrip'=>$project->thumbnail_descrip3
                );

            }


            $project->video = $project->thumbnail_movie_code;
            $project->end_date = $project->collection_end_date;
            $project->thumbnail_description1 = $project->thumbnail_descrip1;
            $project->thumbnail_description2 = $project->thumbnail_descrip2;
            $project->thumbnail_description3 = $project->thumbnail_descrip3;

            $project_return = $this->BackingLevel->getBackingLevelId($project->id);
            if($project->project_type === '0') {
                $timeProject = getTimeProject($project->collection_end_date);
                $project->format_collection_end_date = $timeProject;
            }

            $limitReport = '5';
            if(!$page || $page =='0'){
              $page ='0';
            }else{
              $page = $page*$limitReport;
            }

            // get report daily
            $reportDaily = $this->Report->listReport($id,$limitReport,$page);
            // count all report daily
            $numberReport = $this->Report->countReportDaily($id);
            // assign limit = 5
            $totalPagesReport = ceil($numberReport / $limitReport);

            // create array
            $data = array(
                'project'=>$project,
                'project_return'=>$project_return,
                'report' => array(
                    'totalPage' => $totalPagesReport,
                    'data' => $reportDaily,
                )
            );
            $this->response($data,200);
        }
    }

    public function statisticDonate_get() {
        $id = $this->get('id');
        $project = $this->Project->ProjectId($id);
        $user = $this->user_data;
        $user_id =  $user->id;
        if(!$project){
            $data = array(
                'status'=>'error!',
            );
            $this->response($data,404);
        }else{

            if($project->user_id === $user_id) {
                $start = $this->get('start');
                $limit = '10';
                if(!$start || $start ==='0'){
                    $start ='0';
                }else{
                    $start = $start*$limit;
                }

                $backedProject = $this->BackedProject->getBackedProjectId($id);
                $totalNews = count($backedProject);
                $totalPages = ceil($totalNews / $limit);
                $input['limit']  = array($limit,$start);
                $backedProject = $this->BackedProject->getBackedProjectId($id,$input);
                foreach ($backedProject as $key => $value) {

                    $backing = $this->BackingLevel->getBackingId($value->backing_level_id);
                    $value->number_month = $project->number_month;
                    $value->backing = array(
                        "name" => $backing->name,
                        "invest_amount"=>$backing->invest_amount,
                        "return_amount"=>$backing->return_amount,
                        'now_count'=>$backing->now_count,
                        'max_count' =>$backing->max_count,
                        "thumnail"=>$backing->thumnail,
                        'schedule'=>$backing->schedule,
                        'project_type' => $project->project_type,
                        'name_return' => $value->name_return,
                        'phone' => $value->phone,
                        'postcode' => $value->postcode,
                        'address_return' => $value->address_return,
                        'email' => $value->email,
                    );

                    if(isset($value->user_id) && $value->user_id) {
                        $user = $this->User->getUserId($value->user_id);
                        $value->username = $user->username;
                        $value->status_user = 'user';
                        if($user->active === '0') {
                            $value->blockUser = 1;
                        }else{
                            $value->blockUser = 0;
                        }
                    }else {
                        $value->username = $value->name_return;
                        $value->status_user = 'not_user';
                    }
                }
                $total = array(
                    'goal_amount' => $project->goal_amount,
                    'collected_amount' => $project->collected_amount,
                    'total' => ($project->collected_amount - $project->goal_amount)
                );
                if($project->goal_amount > $project->collected_amount){
                    $total['total'] = '-'.($project->goal_amount - $project->collected_amount);
                }else{
                    $total['total'] = '+'.($project->collected_amount - $project->goal_amount);
                }
                $data = array(
                    'page_count'=> $totalPages,
                    'backed'=> $backedProject,
                    'totalProject'=> $total
                );
                $this->response($data,200);
            }else{
                $data = array(
                    'status'=>'error!',
                );
                $this->response($data,404);
            }
        }
    }

    /**
    * [createProject2_post create new project from my page app]
    * @return [type] [data project added]
    */
    public function createProject2_post(){
        //include Stripe PHP library
        require_once APPPATH."third_party/stripe/init.php";
        // Get userId to token
        $user = $this->user_data;
        $user_id =  $user->id;
        //Config
        $config = [
        [
            'field' => 'project_name',
            'label' => 'Project Name',
            'rules' => 'required|max_length[150]',
            'errors' => [
                'required' => 'プロジェクト名を入力してください。',
                'max_length' => 'プロジェクト名は最大150文字までです。',
            ],
        ],
        [
            'field' => 'description',
            'label' => 'description',
            'rules' => 'max_length[1000]',
            'errors' => [
                'max_length' => '「はじめにご挨拶」の文書は最大255文字までです。',
            ],
        ],
        [
            'field' => 'category_id',
            'label' => 'Category',
            'rules' => 'required',
            'errors' => [
                'required' => 'プロジェクト名を入力してください。',
            ],
        ],
        [
            'field' => 'thumbnail_movie_code',
            'label' => 'video',
            'rules' => 'valid_url',
            'errors' => [
                'valid_url' => '動画のURLを入力してください',
            ],
        ],
        // [
        //     'field' => 'goal_amount',
        //     'label' => 'Goal Amount',
        //     'rules' => 'required',
        //     'errors' => [
        //         'required' => '目標金額を入力してください',
        //         // 'min_length'=>'目標金額は、10,000円以上の金額で入力してください。'
        //     ],
        // ],
        ];

        // Use post
        $data = $this->post();
        $this->form_validation->set_data($data);
        $this->form_validation->set_rules($config);
        // form validation run
        if ($this->form_validation->run() !== false) {
            //Get Date current
            $date = new DateTime();
            //fomat date
            $date = $date->format('Y-m-d H:i:s');
            // listed data with $this->post
            $project_name = $this->post('project_name');
            $category = $this->post('category_id');
            $summary = $this->post('description');
            $radio = $this->post('thumbnail_type');
            $video = $this->post('thumbnail_movie_code');

            if(!isset($video)){
                $video = null;
            }
            $thumbnail1 = $this->post('thumbnail_descrip1');
            $thumbnail2 = $this->post('thumbnail_descrip2');
            $thumbnail3 = $this->post('thumbnail_descrip3');

            if(!isset($thumbnail1)){
                $thumbnail1 = base_url().'images/2018/default/noimage-01.png';
            }
            if(!isset($thumbnail2)){
                $thumbnail2 = base_url().'images/2018/default/noimage-01.png';
            }
            if(!isset($thumbnail3)){
                $thumbnail3 = base_url().'images/2018/default/noimage-01.png';
            }


            $project_type = $this->post('project_type');

            if(isset($project_type) && $project_type === '1') {
                $status_all_in = '1';
                $dateclose = null;
                $goal_amount = null;
                $number_month = $this->post('number_month');
                //set api key
                $key = $this->Setting->getSetting();
                $stripe = array(
                    "secret_key"      => $key->secret_key,
                    "publishable_key" => $key->public_key
                );
                \Stripe\Stripe::setApiKey($stripe['secret_key']);

                try{
                    $product_fanclub = \Stripe\Product::create([
                        "name" => $project_name,
                        "type" => "service",
                    ]);
                    $product_fanclub_id = $product_fanclub->id;
                } catch(Stripe_CardError $e) {
                    $error1 = $e->getMessage();
                    $error = array(
                        'status'=>$error1);
                        $this->response($error,400);
                } catch (Stripe_InvalidRequestError $e) {
                    // Invalid parameters were supplied to Stripe's API
                    $error2 = $e->getMessage();
                    $error = array(
                        'status'=>$error2);
                        $this->response($error,400);
                } catch (Stripe_AuthenticationError $e) {
                    // Authentication with Stripe's API failed
                    $error3 = $e->getMessage();
                    $error = array(
                        'status'=>$error3);
                        $this->response($error,400);
                } catch (Stripe_ApiConnectionError $e) {
                    // Network communication with Stripe failed
                    $error4 = $e->getMessage();
                    $error = array(
                        'status'=>$error4
                    );
                    $this->response($error,400);
                } catch (Stripe_Error $e) {
                // Display a very generic error to the user, and maybe send
                // yourself an email
                $error5 = $e->getMessage();
                $error = array(
                    'status'=>$error5);
                    $this->response($error,400);
                } catch (Exception $e) {
                    // Something else happened, completely unrelated to Stripe
                    $error6 = $e->getMessage();
                    $error = array(
                        'status'=>$error6);
                    $this->response($error,400);
                }
            } else {
                $project_type = 0;
                $status_all_in = $this->post('status_all_in');
                $dateclose = $this->post('collection_end_date');
                $dateclose = new DateTime($dateclose);
                $dateclose = $dateclose->format('Y-m-d 23:59:59');
                $number_month = null;
                $goal_amount = $this->post('goal_amount');
                $goal_amount = preg_replace('/,/', '', $goal_amount);
                if($goal_amount < 10000){
                    $eror = array (
                        'goal_amount' =>'目標金額は、10,000円以上の金額で入力してください。'
                    );
                    $this->response($eror,400);
                }
                $product_fanclub_id = null;
            }
            $project_content = $this->post('project_content');

            $thumbnail = $this->post('thumbnail');
            if(!isset($thumbnail)){
                $thumbnail = base_url().'images/2018/default/noimage-01.png';
            }

            // Add Data listed on $data
            $data_project = array(
                'project_name'          =>  $project_name,
                'category_id'           =>  $category,
                'description'           =>  $summary,
                'goal_amount'           =>  $goal_amount,
                'thumbnail_descrip1'    =>  $thumbnail1,
                'thumbnail_descrip2'    =>  $thumbnail2,
                'thumbnail_descrip3'    =>  $thumbnail3,
                'thumbnail_type'        =>  $radio,
                'thumbnail_movie_code'  =>  $video,
                'collection_start_date' =>  $date,
                'collection_end_date'   =>  $dateclose,
                'project_type'          =>  $project_type,
                'number_month'          =>  $number_month,
                'user_id'               =>  $user_id,
                'status_all_in'         =>  $status_all_in,
                'active'                =>  'no',
                'created'               =>  $date,
                'modified'              =>  $date,
                'thumbnail'             =>  $thumbnail,
                'project_content'       =>  $project_content,
                'product_fanclub_id'    =>  $product_fanclub_id
            );
            // Create Project
            $project = $this->Project->addProject($data_project);
            // Get Id
            $data_project['id'] = $project;
            // Check Project
            if($project){
                // Pagination
                $start  = '0';
                $limit  = '16';
                $user   = $this->user_data;
                $user_id =  $user->id;
                $curUser =   $this->User->getUserId($user_id);
                $numberProject = $this->Project->getAllProjectJoinCat($user_id);
                $totalNews = count($numberProject);
                $totalPages = ceil($totalNews / $limit);
                $input['limit'] = array($limit,$start);
                // Get Data Project with pagination and user_i
                $projectPost = $this->Project->getAllProjectJoinCat($user_id,$input);

                foreach ($projectPost as $key => $value) {
                    $backing = $this->BackingLevel->getBackingLevelId($value->id);
                    $now_count = 0;
                    foreach ($backing as $key => $value2) {
                        $now_count = $now_count + $value2->now_count;
                        $value->now_count=array(
                            'now_count'=>$now_count
                        );
                    }
                    $value->now_count=array(
                        'now_count'=>$now_count
                    );

                    $value->category=array(
                        'id'=>$value->category_id,
                        'name'=>$value->name,
                        'slug'=>$value->slug
                    );

                    $value->user = array(
                        'id'=>$value->user_id,
                        'name'=>$value->username,
                    );

                    $timeProject = getTimeProject($value->collection_end_date);
                    $value->format_collection_end_date = $timeProject;
                }


                $datacurUser = array(
                    'numberProject'=>$totalNews,
                    'page_count'=>$totalPages,
                    'post_project'=>$projectPost
                );

                $date = new DateTime();
                $date = $date->format('Y-m-d H:i:s');
                // Get Project Return
                $data = $this->post('project_return');
                $dataReturn = count($data);
                // Check isset $dataReturn
                $dataInsertBatch =[];
                if($dataReturn >= 1) {
                    // get for create Project return
                    for ($i=0; $i < $dataReturn ; $i++) {
                        // check data of project return and create data
                        $data[$i]['project_id'] = $project;
                        if(!isset($data[$i]['schedule'])){
                            $data[$i]['schedule']  = null;
                        }
                        if(!isset($data[$i]['max_count']) || $data[$i]['max_count'] === ''){
                            $data[$i]['max_count'] = null;
                        }
                        if(!isset($data[$i]['thumnail'])) {
                            $data[$i]['thumnail']  = base_url().'images/2018/default/noimage-01.png';
                        }
                        $data[$i]['now_count'] = 0;
                        $data[$i]['created'] = $date;
                        if(!isset($data[$i]['return_amount'])){
                            $data[$i]['return_amount'] = '';
                        }
                        if(isset($data[$i]['invest_amount']) && isset($data[$i]['name'])){
                            array_push($dataInsertBatch,$data[$i]);
                        }
                    }
                    // create pro
                    $this->db->insert_batch('backing_levels', $dataInsertBatch);
                }
                $this->response($datacurUser,200);

            }
            // Error of form validation
        }else{
            $error = $this->form_validation->error_array();
            $this->response($error,400);
        }
    }

    /**
    * [editProject2_put edit project from my page app]
    * @return [type] [data project edit]
    */

    public function editProject2_put($id){
        $user = $this->user_data;
        // Get data with project Id
        $project = $this->Project->ProjectId($id);

        if($project->active == 'yes'){
            $data = array(
                'status'=>'Error Server!',
            );
            $this->response($data,500);
        }
        //Get Date current
        $config = [
            [
                'field' => 'project_name',
                'label' => 'Project Name',
                'rules' => 'required|max_length[150]',
                'errors' => [
                    'required' => 'プロジェクト名を入力してください。',
                    'max_length' => 'プロジェクト名は最大150文字までです。',
                ],
            ],
            [
                'field' => 'description',
                'label' => 'description',
                'rules' => 'max_length[255]',
                'errors' => [
                    'max_length' => '「はじめにご挨拶」の文書は最大255文字までです。',
                ],
            ],
            [
                'field' => 'category_id',
                'label' => 'Category',
                'rules' => 'required',
                'errors' => [
                    'required' => 'プロジェクト名を入力してください。',
                ],
            ],
            [
                'field' => 'thumbnail_movie_code',
                'label' => 'video',
                'rules' => 'valid_url',
                'errors' => [
                    'valid_url' => '動画のURLを入力してください',
                ],
            ],
            // [
            //     'field' => 'collection_end_date',
            //     'label' => 'End Date',
            //     'rules' => 'required',
            //     'errors' => [
            //         'required' => '募集終了日を入力してください ',
            //     ],
            // ],
            // [
            //     'field' => 'goal_amount',
            //     'label' => 'Goal Amount',
            //     'rules' => 'required',
            //     'errors' => [
            //         'required' => '目標金額を入力してください',
            //         // 'min_length'=>'目標金額は、10,000円以上の金額で入力してください。'
            //     ],
            // ],
        ];
        $data = $this->put();
        $this->form_validation->set_data($data);
        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() !== false) {
            //Get Date current
            $date = new DateTime();
            //fomat date
            $date = $date->format('Y-m-d H:i:s');
            // listed data with $this->post
            $project_name = $this->put('project_name');
            $category = $this->put('category_id');
            $summary = $this->put('description');
            $radio = $this->put('thumbnail_type');
            // If radio == 1 is Video
            // Add $video
            $video = $this->put('thumbnail_movie_code');
            if(!isset($video)){
                $video = null;
            }

            $opened = $this->put('opened');
            $thumbnail1 = $this->put('thumbnail_descrip1');
            $thumbnail2 = $this->put('thumbnail_descrip2');
            $thumbnail3 = $this->put('thumbnail_descrip3');
            if(!isset($thumbnail1)){
                $thumbnail1 = base_url().'images/2018/default/noimage-01.png';
            }
            if(!isset($thumbnail2)){
                $thumbnail2 = base_url().'images/2018/default/noimage-01.png';
            }
            if(!isset($thumbnail3)){
                $thumbnail3 = base_url().'images/2018/default/noimage-01.png';
            }



            $project_type = $this->put('project_type');

            if(isset($project_type) && $project_type === '1') {
                $status_all_in = '1';
                $dateclose = null;
                $number_month = $this->put('number_month');
                $goal_amount = null;
            }else {
                $status_all_in = $this->put('status_all_in');
                $project_type = 0;
                $dateclose = $this->put('collection_end_date');
                if(isset($dateclose) && $dateclose ){
                    $dateclose = new DateTime($dateclose);
                    $dateclose = $dateclose->format('Y-m-d 23:59:59');
                }else{
                    $eror = array(
                        'collection_end_date' =>'募集終了日を入力してください '
                    );
                    $this->response($eror,400);
                }
                $number_month = null;
                $goal_amount = $this->put('goal_amount');
                $goal_amount = preg_replace('/,/', '', $goal_amount);
                if($goal_amount < 10000){
                    $eror = array(
                        'goal_amount' =>'目標金額は、10,000円以上の金額で入力してください。'
                    );
                    $this->response($eror,400);
                }
            }

            $project_content = $this->put('project_content');
            $thumbnail = $this->put('thumbnail');
            if(!isset($thumbnail)){
                $thumbnail = base_url().'images/2018/default/noimage-01.png';
            }

            // Add Data listed on $data
            $data_project = array(
                'project_name' => $project_name,
                'category_id' => $category,
                'description' => $summary,
                'goal_amount' => $goal_amount,
                'thumbnail_descrip1'=> $thumbnail1,
                'thumbnail_descrip2'=> $thumbnail2,
                'thumbnail_descrip3'=> $thumbnail3,
                'thumbnail_type'=>$radio,
                'thumbnail_movie_code'=>$video,
                'collection_end_date'=>$dateclose,
                'active' => 'no',
                'status_all_in' =>  $status_all_in,
                'modified' => $date,
                'thumbnail'=>$thumbnail,
                'project_content'=>$project_content,
                'project_type' => $project_type,
                'number_month' => $number_month,
            );

            $date = new DateTime();
            //fomat date
            $date = $date->format('Y-m-d H:i:s');
            $data = $this->put('project_return');
            $update = [];
            $insert = [];
            $projectIdDel = [];
            $project = $this->Project->update($id,$data_project);

            $projectReturnId = $this->BackingLevel->getBackingLevelId($id);

            if($project){
                $dataProject = count($data);
                if($dataProject >= 1){
                    for ($i=0; $i < $dataProject ; $i++) {

                        $data[$i]['project_id'] = $id;
                        if(!isset($data[$i]['schedule'])){
                            $data[$i]['schedule']  = null;
                        }
                        if(!isset($data[$i]['max_count']) || $data[$i]['max_count'] === ''){
                            $data[$i]['max_count'] = null;
                        }
                        if(!isset($data[$i]['thumnail'])) {
                            $data[$i]['thumnail']  = base_url().'images/2018/default/noimage-01.png';
                        }
                        if(!isset($data[$i]['return_amount'])){
                            $data[$i]['return_amount'] = '';
                        }
                        $data[$i]['now_count'] = 0;
                        $data[$i]['created'] = $date;
                        if(!empty($data[$i]) && isset($data[$i]['invest_amount']) && isset($data[$i]['name'])){
                            if(!isset($data[$i]['id'])){
                                array_push($insert,$data[$i]);
                            }else{
                                array_push($update,$data[$i]);
                            }
                        }
                    }

                    // $delete = array_diff_key($projectReturnId,$update);
                    //
                    // foreach ($delete as $key => $value) {
                    //     array_push($projectIdDel,$value->id);
                    // }
                    // $arrayProjectIdDel = implode(',', $projectIdDel);

                    if(count($update) >= 1){
                        foreach ($update as $key => $value) {
                            $this->BackingLevel->update($value['id'],$value);
                        }
                    }
                    // if($arrayProjectIdDel != ''){
                    //     $this->BackingLevel->deleteProjectReturn($arrayProjectIdDel);
                    // }
                    if(count($insert) >= 1){
                        $this->db->insert_batch('backing_levels', $insert);
                    }
                }

                $start = '0';
                $limit = '16';
                $user = $this->user_data;
                $user_id =  $user->id;
                $curUser =   $this->User->getUserId($user_id);
                $numberProject = $this->Project->getAllProjectJoinCat($user_id);
                $totalNews = count($numberProject);
                $totalPages = ceil($totalNews / $limit);

                $input['limit'] = array($limit,$start);

                $projectPost = $this->Project->getAllProjectJoinCat($user_id,$input);

                foreach ($projectPost as $key => $value) {
                    $backing = $this->BackingLevel->getBackingLevelId($value->id);
                    $now_count = 0;
                    foreach ($backing as $key => $value2) {
                        $now_count = $now_count + $value2->now_count;
                        $value->now_count=array(
                            'now_count'=>$now_count
                        );
                    }
                    $value->now_count=array(
                        'now_count'=>$now_count
                    );

                    $value->category=array(
                        'id'=>$value->category_id,
                        'name'=>$value->name,
                        'slug'=>$value->slug
                    );

                    $value->user = array(
                        'id'=>$value->user_id,
                        'name'=>$value->username,
                    );

                    $timeProject = getTimeProject($value->collection_end_date);
                    $value->format_collection_end_date = $timeProject;
                }

                $newproject = $this->Project->ProjectId($id);
                $project_return = $this->BackingLevel->getBackingLevelId($id);
                $datacurUser = array(
                    'numberProject'=>$totalNews,
                    'project'=>$newproject,
                    'project_return'=>$project_return,
                    'page_count'=>$totalPages,
                    'post_project'=>$projectPost
                );
                $this->response($datacurUser,200);
            }
        }else{
            $error = $this->form_validation->error_array();
            $this->response($error,400);
        }
    }

    public function editProject_put($id){
        $project = $this->Project->ProjectId($id);
        if($project->active == 'yes'){
            $data = array(
                'status'=>'Error Server!',
            );
            $this->response($data,500);
        }
        //Get Date current
        $config = [
            [
                'field' => 'project_name',
                'label' => 'Project Name',
                'rules' => 'required|max_length[150]',
                'errors' => [
                    'required' => 'プロジェクト名を入力してください。',
                    'max_length' => 'プロジェクト名は最大150文字までです。',
                ],
            ],
            [
                'field' => 'description',
                'label' => 'description',
                'rules' => 'max_length[255]',
                'errors' => [
                    'max_length' => '「はじめにご挨拶」の文書は最大255文字までです。',
                ],
            ],
            [
                'field' => 'video',
                'label' => 'video',
                'rules' => 'valid_url',
                'errors' => [
                    'valid_url' => '動画のURLを入力してください',
                ],
            ],
            [
                'field' => 'end_date',
                'label' => 'End Date',
                'rules' => 'required',
                'errors' => [
                    'required' => '募集終了日を入力してください ',
                ],
            ],
            [
                'field' => 'goal_amount',
                'label' => 'Goal Amount',
                'rules' => 'required|min_length[6]',
                'errors' => [
                    'required' => '目標金額を入力してください',
                    'min_length'=>'目標金額は、10,000円以上の金額で入力してください。'
                ],
            ],
        ];
        $data = $this->put();
        $this->form_validation->set_data($data);
        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() !== false) {
            //Get Date current
            $date = new DateTime();
            //fomat date
            $date = $date->format('Y-m-d H:i:s');
            // listed data with $this->post
            $project_name = $this->put('project_name');
            $category = $this->put('category_id');
            $summary = $this->put('description');
            $radio = $this->put('thumbnail_type');
            // If radio == 1 is Video
            // Add $video
            $video = $this->put('video');
            if(!isset($video)){
                $video = null;
            }

            $opened = $this->put('opened');
            $thumbnail1 = $this->put('thumbnail_descrip1');
            $thumbnail2 = $this->put('thumbnail_descrip2');
            $thumbnail3 = $this->put('thumbnail_descrip3');
            if(!isset($thumbnail1)){
                $thumbnail1 = base_url().'images/2018/default/noimage-01.png';
            }
            if(!isset($thumbnail2)){
                $thumbnail2 = base_url().'images/2018/default/noimage-01.png';
            }
            if(!isset($thumbnail3)){
                $thumbnail3 = base_url().'images/2018/default/noimage-01.png';
            }
            $dateclose = $this->put('end_date');
            $goal_amount = $this->put('goal_amount');
            $goal_amount = preg_replace('/,/', '', $goal_amount);
            $project_content = $this->put('project_content');
            $thumbnail = $this->put('thumbnail');
            if(!isset($thumbnail)){
                $thumbnail = base_url().'images/2018/default/noimage-01.png';
            }
            // Add Data listed on $data
            $data_project = array(
                'project_name' => $project_name,
                'category_id' => $category,
                'description' => $summary,
                'goal_amount' => $goal_amount,
                'thumbnail_descrip1'=> $thumbnail1,
                'thumbnail_descrip2'=> $thumbnail2,
                'thumbnail_descrip3'=> $thumbnail3,
                'thumbnail_type'=>$radio,
                'thumbnail_movie_code'=>$video,
                'collection_end_date'=>$dateclose,
                'active' => 'no',
                'modified' => $date,
                'thumbnail'=>$thumbnail,
                'project_content'=>$project_content
            );

            $date = new DateTime();
            //fomat date
            $date = $date->format('Y-m-d H:i:s');
            $data = $this->put('project_return');
            $update = [];
            $insert = [];
            $projectIdDel = [];

            $project = $this->Project->update($id,$data_project);
            $projectReturnId = $this->BackingLevel->getBackingLevelId($id);

            if($project){
                $a = count($data);
                if($a >= 1){
                    for ($i=0; $i < $a ; $i++) {

                        $data[$i]['project_id'] = $id;
                        if(!isset($data[$i]['schedule'])){
                            $data[$i]['schedule']  = null;
                        }
                        if(!isset($data[$i]['thumnail'])){
                            $data[$i]['thumnail']  = base_url().'images/2018/default/noimage-01.png';
                        }
                        $data[$i]['now_count'] = 0;
                        $data[$i]['created'] = $date;
                        if(!empty($data[$i]) && isset($data[$i]['invest_amount']) && isset($data[$i]['return_amount'])){
                            if(!isset($data[$i]['id'])){
                                array_push($insert,$data[$i]);
                            }else{
                                array_push($update,$data[$i]);
                            }
                        }
                    }

                    $delete = array_diff_key($projectReturnId,$update);
                    foreach ($delete as $key => $value) {
                        array_push($projectIdDel,$value->id);
                    }
                    $arrayProjectIdDel = implode(',', $projectIdDel);

                    if(count($update) >= 1){
                        foreach ($update as $key => $value) {
                            $this->BackingLevel->update($value['id'],$value);
                        }
                    }
                    if($arrayProjectIdDel != ''){
                        $this->BackingLevel->deleteProjectReturn($arrayProjectIdDel);
                    }
                    if(count($insert) >= 1){
                        $this->db->insert_batch('backing_levels', $insert);
                    }
                }
                $newproject = $this->Project->ProjectId($id);
            }
            $project_return = $this->BackingLevel->getBackingLevelId($id);
            $data_project['id'] =$id;
            $data = array(
                'project'=>$newproject,
                'project_return'=>$project_return
            );
            $this->response($data,200);
        }else{
            $error = $this->form_validation->error_array();
            $this->response($error,400);
        }
    }

    public function deleteProject_delete($id){
        $user = $this->user_data;
        $user_id =  $user->id;
        $arrayProjectReturnIdDel = [];
        if(isset($id) && $id){
            $backing = $this->BackingLevel->getBackingLevelId($id);
            foreach ($backing as $key => $value) {
                array_push($arrayProjectReturnIdDel,$value->id);
            }
            $arrayProjectIdDel = implode(',', $arrayProjectReturnIdDel);
            $del = $this->Project->deleteProject($id,$user_id);
            if($del){

                if($arrayProjectIdDel != ''){
                    $this->BackingLevel->deleteProjectReturn($arrayProjectIdDel);
                }
                $success = array(
                    'status'=>'Delete success!'
                );
                $this->response($success,200);
            }else{
                $error = array(
                    'status'=>'エラー！プロジェクト削除がまだできません。'
                );
                $this->response($error,400);
            }
        }else{
            $error = array(
                'status'=>'このプロジェクトは存在しません。'
            );
            $this->response($error,404);
        }
    }

    public function sendProjectActive_get(){
        $key_firebase = $this->config->item('key_firebase');
        $email_admin = $this->config->item('email_admin');
        $user = $this->user_data;
        $user_id =  $user->id;
        $curUser =   $this->User->getUserId($user_id);
        if($curUser->bank_branch === '' || $curUser->bank_name  === '' || $curUser->bank_owner  === '' || $curUser->bank_number  === ''){
            $error = array(
                'status' =>'口座番号を入力してください。'
            );
            $this->response($error,400);
        }
        $date = new DateTime();
        $date = $date->format('Y-m-d H:i:s');
        $id = $this->get('id');
        if(isset($id) && $id){

            $project = $this->Project->ProjectId($id);

            if($project && $project->opened === 'no'){
                $data_project = array(
                    'opened' => 'yes',
                    'modified' => $date
                );

                $projectUpdate = $this->Project->update($id,$data_project);
                if($projectUpdate) {
                    // token users
                    $tokenUser = $curUser->token;
                    // Create Curl
                    $room = 'project_'.$id;
                    if($curUser->eventProjectSocket != null){

                        if (strpos($curUser->eventProjectSocket, $room) !== false) {
                            $roomSocket = $curUser->eventProjectSocket;

                        }else {
                            $roomSocket = $curUser->eventProjectSocket.','.'project_'.$id;
                        }

                    }else{
                        $roomSocket = 'project_'.$id;
                    }

                    $dataUser = array(
                        'eventProjectSocket' => $roomSocket
                    );
                    $editUser = $this->User->update($user_id,$dataUser);


                        // use curl create topics (send notification = firebase)
                        // $curl = curl_init();
                        //
                        // curl_setopt_array($curl, array(
                        //   CURLOPT_URL => "https://iid.googleapis.com/iid/v1/".$tokenUser."/rel/topics/project_".$id,
                        //   CURLOPT_RETURNTRANSFER => true,
                        //   CURLOPT_ENCODING => "",
                        //   CURLOPT_MAXREDIRS => 10,
                        //   CURLOPT_TIMEOUT => 30,
                        //   CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                        //   CURLOPT_CUSTOMREQUEST => "POST",
                        //   CURLOPT_POSTFIELDS => "",
                        //   CURLOPT_HTTPHEADER => array(
                        //     "authorization: key=".$key_firebase."",
                        //     "cache-control: no-cache"
                        //   ),
                        // ));
                        //
                        // $response = curl_exec($curl);
                        // $err = curl_error($curl);
                        //
                        // curl_close($curl);
                    //
                    // $userName = $curUser->username;
                    // $projectName = $project->project_name;

                    $userName = $curUser->username;
                    $projectName = $project->project_name;

                    // send mail for admin
                    $this->load->library('email');
                    //use json_encode show data.

                    $this->email->from('admin@kakuseida.conoha.io', 'KAKUSEIDA');
                    $this->email->to($email_admin);

                    $this->email->subject(
                        'ユーザー名「'.$userName.'」が新規プロジェクト「'.$projectName.'」を企画しました'
                    );
                    $this->email->message(
                        'ユーザー名「'.$userName.'」が新規プロジェクト「'.$projectName.'」を企画しました。管理者画面で確認してください。<br>
                        '.admin_url().'project/edit/'.$id
                    );

                    $this->email->send();

                    // show message
                    $success = array(
                        'status'=>'TRUE'
                    );
                    $this->response($success,200);
                }
            }else{
                $error = array(
                    'status'=>'Id not exists!'
                );
                $this->response($error,404);
            }
        }else{
            $error = array(
                'status'=>'Id not exists!'
            );
            $this->response($error,404);
        }
    }

    public function sendProjectUnActive_get(){

        $email_admin = $this->config->item('email_admin');

        $user = $this->user_data;
        $user_id =  $user->id;
        $curUser =   $this->User->getUserId($user_id);

        $date = new DateTime();
        $date = $date->format('Y-m-d H:i:s');
        $id = $this->get('id');

        if(isset($id) && $id){

            $project = $this->Project->ProjectId($id);
            if(isset($project) && $project->user_id === $user_id){

                if($project && $project->opened === 'yes'){
                    $data_project = array(
                        'opened' => 'no',
                        'modified' => $date
                    );
                    $projectUpdate = $this->Project->update($id,$data_project);
                    if($projectUpdate) {
                        // token users
                        $tokenUser = $curUser->token;

                        // get username + project name
                        $userName = $curUser->username;
                        $projectName = $project->project_name;

                        // send mail for admin
                        $this->load->library('email');
                        //use json_encode show data.

                        $this->email->from('admin@kakuseida.conoha.io', 'KAKUSEIDA');
                        $this->email->to($email_admin);

                        $this->email->subject('('.$userName.')が「'.$projectName.'」を公開する申請をキャンセルしました。');
                        $this->email->message(
                            '('.$userName.')が「'.$projectName.'」を公開する申請をキャンセルしました。'
                        );

                        $this->email->send();

                        // show message
                        $success = array(
                            'status'=>'TRUE'
                        );
                        $this->response($success,200);

                    }
                }else{
                    $error = array(
                        'status'=>'Id not exists!'
                    );
                    $this->response($error,404);
                }
            }else{
                $error = array(
                    'status'=>'Id not exists!'
                );
                $this->response($error,404);
            }
        }else{
            $error = array(
                'status'=>'Id not exists!'
            );
            $this->response($error,404);
        }
    }



    public function checkProjectReturn_get(){
        $project_id = $this->get('project_id');
        $backing_level_id = $this->get('backing_level_id');

        if(isset($project_id) && isset($backing_level_id)){

            $existproject = $this->Project->checkProjectId($project_id);
            $existprojectretrun = $this->BackingLevel->getBackingId($backing_level_id);

            if(isset($existproject) && $existproject){
                if($existproject->project_type === '0'){
                    $getTime = $this->getTime_get($existproject->id);
                    $existproject->format_collection_end_date = $getTime;
                    if(isset($existproject->format_collection_end_date['status']) && $existproject->format_collection_end_date['status'] === '終了' ){
                        $error = array(
                            'status'=>'Project Expired!'
                        );
                        $this->response($error,404);
                    }
                }
                if(isset($existprojectretrun) && $existprojectretrun){
                    if($existprojectretrun->project_id === $existproject->id) {

                        $project_return = $this->BackingLevel->getBackingId($backing_level_id);
                        $project_return->project_type = $existproject->project_type;
                        $this->response($project_return,200);

                    }else{
                        $error = array(
                            'status'=>'ProjectReturn or Project not found!'
                        );
                        $this->response($error,404);
                    }
                }else{
                    $error = array(
                        'status'=>'ProjectReturn not exists!'
                    );
                    $this->response($error,404);
                }
            }else{
                $error = array(
                    'status'=>'Project not exists!'
                );
                $this->response($error,404);
            }
        }else{
            $error = array(
                'status'=>'This id is not exists!'
            );
            $this->response($error,404);
        }

    }

    public function CreateToken_post(){
        $user = $this->user_data;
        $user_id =  $user->id;
        $key = $this->Setting->getSetting();

        require_once APPPATH."third_party/stripe/init.php";

        \Stripe\Stripe::setApiKey($key->secret_key);


        try {
            $cvc = $this->post('cvc');
            if(isset($cvc) && $cvc){

                $checkBlackList = $this->checkNumberStripe_post($this->post('number'),$this->post('cvc'));
                if(!$checkBlackList){
                    $error = array(
                        "status" => "このクレジットカード情報はブロックされたユーザーのですので、別のクレジットカード情報を利用してください"
                    );
                    $this->response($error,400);
                }

                $stripeToken = \Stripe\Token::create(array(
                    "card" => [
                        "number" => $this->post('number'),
                        "exp_month" => $this->post('exp_month'),
                        "exp_year" => $this->post('exp_year'),
                        "cvc" => $this->post('cvc')
                    ]
                ));
                $success = array(
                    'tokenId'=>$stripeToken->id
                );
                $this->response($success,200);
            }else{
                $error = array(
                    'status'=>'CVCを正しく入力してください'
                );
                $this->response($error,400);
            }

        } catch(Stripe_CardError $e) {
            $error1 = $e->getMessage();
            $error = array(
                'status'=>$error1,
            );
            $this->response($error,400);

        } catch (Stripe_InvalidRequestError $e) {
            // Invalid parameters were supplied to Stripe's API
            $error2 = $e->getMessage();
            $error = array(
                'status'=>$error2,
            );
            $this->response($error,400);
        } catch (Stripe_AuthenticationError $e) {
            // Authentication with Stripe's API failed
            $error3 = $e->getMessage();
            $error = array(
                'status'=>$error3,
            );
            $this->response($error,400);
        } catch (Stripe_ApiConnectionError $e) {
            // Network communication with Stripe failed
            $error4 = $e->getMessage();
            $error = array(
                'status'=>$error4,
            );
            $this->response($error,400);
        } catch (Stripe_Error $e) {
            // Display a very generic error to the user, and maybe send
            // yourself an email
            $error5 = $e->getMessage();
            $error = array(
                'status'=>$error5,
            );
            $this->response($error,400);
        } catch (Exception $e) {
            // Something else happened, completely unrelated to Stripe
            $errrr = json_decode($e->getHttpBody());
            if($errrr->error->code === "invalid_expiry_month"){
                $error = array(
                    'status'=>'有効期限の年を正しく入力してください'
                );
            }else if($errrr->error->code === "incorrect_number"){
                $error = array(
                    'status'=>'カード番号は正しくありません。'
                );
            }else if($errrr->error->code === "invalid_expiry_year"){
                $error = array(
                    'status'=>'有効期限の年を正しく入力してください'
                );
            }else if($errrr->error->code === "invalid_cvc"){
                $error = array(
                    'status'=>'CVCを正しく入力してください'
                );
            }else if($errrr->error->code === "expired_card"){
                $error = array(
                    'status'=>'有効期限の年を正しく入力してください'
                );
            }else if($errrr->error->code === "incorrect_cvc"){
                $error = array(
                    'status'=>'CVCを正しく入力してください'
                );
            }
            $this->response($error,400);
        }
    }

    // Donate Use Stripe;
    public function donateStripe_post() {
        require("public/assets/socket/socket.io.php");
        $roomSocket = $this->config->item('roomSocket');
        $portRoomSocket = $this->config->item('portRoomSocket');
        $key_firebase = $this->config->item('key_firebase');

        $user = $this->user_data;
        $user_id =  $user->id;
        $curUser =   $this->User->getUserId($user_id);
        $email = $curUser->email;
        $_POST = $this->post();
        //check whether stripe token is not empty
        //get token, card and user info from the form
        $token  = $this->post('stripeToken');
        $project_id = $_POST['project_id'];
        $backing_level_id = $_POST['backing_level_id'];
        // $comment = '”'.$curUser->username.'"は"'.$projectName.'('.$project_id.')"の"'.$backing_level_id.'"に支援しました。';
        $comment =  'comment';
        $invest_amount = $_POST['invest_amount'];
        $cvc = $_POST['cvc'];
        if(!$cvc){
            $error = array(
                'status' => 'CVCを記入してください。'
            );
            $this->response($error,400);
        }

        if(isset($project_id) && isset($backing_level_id)){
            $date = new DateTime();
            $date = $date->format('Y-m-d H:i:s');

            $existproject = $this->Project->checkProjectId($project_id);

            $existprojectretrun = $this->BackingLevel->getBackingId($backing_level_id);

            if(isset($existproject) && $existproject){
                $number_month = $existproject->number_month;
                $timestart = $existproject->collection_start_date;
                $project_type = $existproject->project_type;
                $userOwner = $this->User->getUserId($existproject->user_id);
                $nameOwner = $userOwner->username;
                $emailOwner = $userOwner->email;
                if($existproject->project_type === '0'){
                    if($existproject->collection_end_date < $date){
                        $error = array(
                            "status" => "error!"
                        );
                        $this->response($error,500);
                    }
                }else{
                    if($existproject->status_fanclub === 'stop'){
                        $error = array(
                            "status" => "error!"
                        );
                        $this->response($error,500);
                    }
                }

                if($user_id === $existproject->user_id){
                    $error = array(
                        "status" => "error!"
                    );
                    $this->response($error,500);
                }

                if(isset($existprojectretrun) && $existprojectretrun){
                    if($existprojectretrun->project_id === $existproject->id) {
                        //include Stripe PHP library
                        require_once APPPATH."third_party/stripe/init.php";

                        //set api key
                        $key = $this->Setting->getSetting();
                        $stripe = array(
                            "secret_key"      => $key->secret_key,
                            "publishable_key" => $key->public_key
                        );

                        \Stripe\Stripe::setApiKey($stripe['secret_key']);

                        //add customer to stripe
                        if(isset($curUser->customer_id) && $curUser->customer_id && !isset($token)){
                            $customer = (object) array('id' => $curUser->customer_id);
                            if(isset($curUser->customer_id) && $curUser->customer_id){
                                $kunci = $this->config->item('thekey');
                                $token = $curUser->hash_token;

                                try {
                                    $decoded = JWT::decode($token, $kunci, array('HS256'));
                                    $result = strpos($decoded, "/") + 1;
                                    $checkcvc = substr($decoded, $result);


                                    if((int)$cvc !== (int)$checkcvc){
                                        $error = array(
                                            "status" => "セキュリティーコード（CVC)は間違っていますので、ご確認下さい。"
                                        );
                                        $this->response($error,400);
                                    } else {
                                        $checkBlackList = $this->checkNumberStripe_post($curUser->last4,$cvc);
                                        if(!$checkBlackList){
                                            $error = array(
                                                "status" => "このクレジットカード情報はブロックされたユーザーのですので、別のクレジットカード情報を利用してください"
                                            );
                                            $this->response($error,400);
                                        }

                                    }
                                } catch (Exception $e) {
                                    $invalid = ['status' => $e->getMessage()];
                                    $this->response($invalid, 400);
                                }
                            }
                        }else if(isset($curUser->customer_id) && $curUser->customer_id && isset($token) || !isset($curUser->customer_id) && isset($token)) {
                            try {
                                $customer = \Stripe\Customer::create(array(
                                    'email' => $email,
                                    'source'  => $token
                                ));
                            } catch(Stripe_CardError $e) {
                                $error1 = $e->getMessage();
                                $error = array(
                                    'status'=>$error1
                                );
                                $this->response($error,400);
                            } catch (Stripe_InvalidRequestError $e) {
                                // Invalid parameters were supplied to Stripe's API
                                $error2 = $e->getMessage();
                                $error = array(
                                    'status'=>$error2
                                );
                                $this->response($error,400);
                            } catch (Stripe_AuthenticationError $e) {
                                // Authentication with Stripe's API failed
                                $error3 = $e->getMessage();
                                $error = array(
                                    'status'=>$error3
                                );

                                $this->response($error,400);
                            } catch (Stripe_ApiConnectionError $e) {
                                // Network communication with Stripe failed
                                $error4 = $e->getMessage();
                                $error = array(
                                    'status'=>$error4
                                );
                                $this->response($error,400);
                            } catch (Stripe_Error $e) {
                                // Display a very generic error to the user, and maybe send
                                // yourself an email
                                $error5 = $e->getMessage();
                                $error = array(
                                    'status'=>$error5
                                );
                                $this->response($error,400);
                            } catch (Exception $e) {
                                // Something else happened, completely unrelated to Stripe
                                $errrr = json_decode($e->getHttpBody());
                                if($errrr->error->code === "token_already_used"){
                                    $error = array(
                                        'status'=>'このクレジットカード情報は存在しました。'
                                    );
                                }else if($errrr->error->code === "email_invalid"){
                                    $error = array(
                                        'status'=>'メールアドレスを正しく入力してください'
                                    );
                                }

                                $this->response($error,400);
                            }
                        }
                        //item information
                        $itemName = $project_id;
                        $itemNumber = $backing_level_id;
                        $itemPrice = $this->post('invest_amount');
                        $currency = "JPY";
                        $orderID = $project_id;

                        //charge a credit or a debit card
                        try {
                            $charge = \Stripe\Charge::create(array(
                                'customer' => $customer->id,
                                'amount'   => $itemPrice,
                                'currency' => $currency,
                                'description' => $itemNumber,
                                'metadata' => array(
                                'item_id' => $itemNumber
                                )
                            ));
                            //retrieve charge details
                            $chargeJson = $charge->jsonSerialize();

                            //check whether the charge is successful
                            if($chargeJson['amount_refunded'] == 0 && empty($chargeJson['failure_code']) && $chargeJson['paid'] == 1 && $chargeJson['captured'] == 1)
                            {
                                //order details
                                $amount = $chargeJson['amount'];
                                $balance_transaction = $chargeJson['balance_transaction'];
                                $currency = $chargeJson['currency'];
                                $status = $chargeJson['status'];
                                $date = date("Y-m-d H:i:s");


                                //insert tansaction data into the database

                                $addressDefault = $this->AddressUser->getAddressUserDefault($user_id);
                                if(!$addressDefault){
                                    $error = array(
                                        'status' => 'error!'
                                    );
                                    $this->response($error,500);
                                }
                                $dataDB = array(
                                    'project_id' => $project_id,
                                    'user_id' => $user_id,
                                    'id_address' => $addressDefault->id,
                                    'postcode'  =>  $addressDefault->postcode,
                                    'address_return'  =>  $addressDefault->address_return,
                                    'email'  =>  $addressDefault->email,
                                    'phone'  =>  $addressDefault->phone,
                                    'name_return'  =>  $addressDefault->name_return,
                                    'backing_level_id' => $backing_level_id,
                                    'invest_amount' => $invest_amount,
                                    'stripe_charge_id' => $charge->id,
                                    'type'=>'Credit',
                                    'comment' => $comment,
                                    'status' => $status,
                                    'created' => $date,
                                    'manual_flag' => '0'
                                );

                                if ($this->db->insert('backed_projects', $dataDB)) {
                                    if($this->db->insert_id() && $status == 'succeeded'){


                                        $dataBackingLevel = $this->BackingLevel->getBackingId($backing_level_id);
                                        $now_count = $dataBackingLevel->now_count;
                                        $dataUpdate = array(
                                            'now_count'=>$now_count + 1,
                                        );
                                        $this->BackingLevel->update($backing_level_id,$dataUpdate);
                                        $dataUserUpdate = array(
                                            'customer_id'=>$customer->id
                                        );
                                        $projectId = $this->Project->getProjectId($project_id);
                                        $collected_amount = $projectId->collected_amount;
                                        $now_countProject = $projectId->now_count;
                                        $dataUpdateProject = array(
                                            'now_count'=>$now_countProject + 1,
                                            'collected_amount'=>$collected_amount + $itemPrice,
                                        );
                                        $updateProject = $this->Project->update($project_id,$dataUpdateProject);
                                        $updateUser = $this->User->update($user_id,$dataUserUpdate);
                                        $listUserDonate = array(
                                            'user_id'=>$user_id,
                                            'project_id'=>$project_id,
                                            'created'=>$date
                                        );
                                        $checkIsDonate = $this->ListUserDonate->checkIsDonate($user_id,$project_id);
                                        if(!$checkIsDonate){
                                            $createList = $this->ListUserDonate->createListUserDonate($listUserDonate);
                                        }


                                        // create Datetime
                                        $datetime = new DateTime();
                                        $datetime = $datetime->format('Y-m-d H:i:s');
                                        // get strtotime + change time zone
                                        $asia_timestamp = strtotime($datetime);
                                        date_default_timezone_set('UTC');
                                        // get Date vs time zone
                                        $date = date("Y-m-d H:i:s", $asia_timestamp);

                                        $checkMoneyProject = $this->Project->getEnoughAmountById($project_id,$date);

                                        if(isset($checkMoneyProject) && $checkMoneyProject) {

                                            // array create notification info
                                            $dataNotification = array (
                                                'user_id' => $checkMoneyProject->user_id,
                                                'user_action' => 'Admin',
                                                'title' =>  'あと少しで目標100％になるプロジェクト',
                                                'message' =>$checkMoneyProject->project_name.'プロジェクトの目標金額にもうすぐ到達します',
                                                'link'=> "project-detail/".$checkMoneyProject->id,
                                                'status' => 0,
                                                'thumbnail' => base_url()."images/2018/default/logo-icon.png",
                                                'created' => $date
                                            );
                                            // create notification info
                                            $createNotification = $this->Notification_info->create($dataNotification);

                                            $socketio = new SocketIO();
                                            // // array
                                            $data = array(
                                                'room' => 'project_'.$checkMoneyProject->id,
                                                'title' =>  'あと少しで目標100％になるプロジェクト',
                                                'message' =>$checkMoneyProject->project_name.'プロジェクトの目標金額にもうすぐ到達します',
                                                'url' => app_url()."project-detail/".$checkMoneyProject->id,
                                                'ava' => base_url()."images/2018/default/logo-icon.png"
                                            );

                                            $socketio->send($roomSocket, $portRoomSocket, 'annouce_project',json_encode($data));

                                            $curl = curl_init();

                                            curl_setopt_array($curl, array(
                                              CURLOPT_URL => "https://fcm.googleapis.com/fcm/send",
                                              CURLOPT_RETURNTRANSFER => true,
                                              CURLOPT_ENCODING => "",
                                              CURLOPT_MAXREDIRS => 10,
                                              CURLOPT_TIMEOUT => 30,
                                              CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                                              CURLOPT_CUSTOMREQUEST => "POST",
                                              CURLOPT_POSTFIELDS => "{   \"notification\": {\r\n        \"title\": \"あと少しで目標100％になるプロジェクト\",\r\n        \"body\": \"".$checkMoneyProject->project_name."プロジェクトの目標金額にもうすぐ到達します\",\r\n        \"click_action\": \"".app_url()."project-detail/".$checkMoneyProject->id."\",\r\n        \"icon\": \"".base_url()."images/2018/default/logo-icon.png\"\r\n    },\r\n  \"to\" : \"/topics/project_".$checkMoneyProject->id."\"\r\n}",
                                              CURLOPT_HTTPHEADER => array(
                                                "Authorization: key=".$key_firebase."",
                                                "Content-Type: application/json",
                                              ),
                                            ));

                                            $response = curl_exec($curl);
                                            $err = curl_error($curl);

                                            curl_close($curl);

                                            $updateProject = array(
                                                'status_goal_amount' => '1'
                                            );

                                            $update = $this->Project->update($checkMoneyProject->id,$updateProject);
                                        }

                                        // get data for socket + create notification_info + firebase
                                        $userDonate = $curUser->username;
                                        $investAmountDonate = $invest_amount;
                                        $imageUserDonate = $curUser->profileImageURL;
                                        $projectId = $project_id;
                                        $projectName = $existproject->project_name;
                                        $returnName = $existprojectretrun->name;
                                        // array create notification info
                                        $dataNotification = array (
                                            'user_id' => $existproject->user_id,
                                            'user_action' => $userDonate,
                                            'title' => '支援があります',
                                            'message' => ''.$userDonate.' さんが 「'.$projectName.'」に'.number_format($investAmountDonate).'円支援しました',
                                            'link'=> "my-page/statistic/".$projectId,
                                            'status' => 0,
                                            'thumbnail' => $imageUserDonate,
                                            'created' => $date
                                        );
                                        // create notification info
                                        $createNotification = $this->Notification_info->create($dataNotification);
                                        // check exists
                                        if($createNotification) {
                                            // use SocketIO annouce for user
                                            $socketio = new SocketIO();
                                            // // array
                                            $data = array(
                                                'room' => 'project_'.$projectId,
                                                'title' => '支援があります',
                                                'message' => $userDonate."さんが「".$projectName."」に".number_format($investAmountDonate)."円支援しました",
                                                'url'=> app_url()."my-page/statistic/".$projectId,
                                                'ava' => $imageUserDonate
                                            );
                                            $socketio->send($roomSocket, $portRoomSocket, 'donate_project',json_encode($data));

                                            $curl = curl_init();

                                            curl_setopt_array($curl, array(
                                              CURLOPT_URL => "https://fcm.googleapis.com/fcm/send",
                                              CURLOPT_RETURNTRANSFER => true,
                                              CURLOPT_ENCODING => "",
                                              CURLOPT_MAXREDIRS => 10,
                                              CURLOPT_TIMEOUT => 30,
                                              CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                                              CURLOPT_CUSTOMREQUEST => "POST",
                                              CURLOPT_POSTFIELDS => "{   \"notification\": {\r\n        \"title\": \"支援があります\",\r\n        \"body\": \"".$userDonate."さんが「".$projectName."」」に ".number_format($investAmountDonate)."円支援しました\",\r\n        \"click_action\": \"".app_url()."my-page/statistic/".$projectId."\",\r\n        \"icon\": \"".base_url()."images/2018/default/logo-icon.png\"\r\n    },\r\n  \"to\" : \"/topics/project_".$projectId."\"\r\n}",
                                              CURLOPT_HTTPHEADER => array(
                                                "Authorization: key=".$key_firebase,
                                                "Content-Type: application/json",
                                              ),
                                            ));

                                            $response = curl_exec($curl);
                                            $err = curl_error($curl);

                                            curl_close($curl);

                                            $this->load->library('email');

                                            $this->email->from('admin@kakuseida.conoha.io', 'KAKUSEIDA');
                                            $this->email->to($email);
                                            // $this->email->reply_to('admin@kakuseida.conoha.io', 'KAKUSEIDA');
                                            $this->email->subject('「'.$projectName.'」プロジェクトにご支援いただきありがとうございます。');
                                            $this->email->message(
                                            $userDonate.'様が選択されたリターン品の名前は'.$returnName.'です。<br>
                                            KAKUSEIDA運営事務局です。<br>
                                            このたびは、「'.$projectName.'」プロジェクトにご支援いただきありがとうございます。<br>
                                            '.$userDonate.'様が選択されたリターン品の名前は'.$returnName.'です。<br>
                                            リターンの詳細、送付に関しましては企画者からの連絡をお待ちください。<br>
                                            今後ともKAKUSEIDAをよろしくお願い致します。<br>');
                                            $this->email->send();


                                            $this->email->from('admin@kakuseida.conoha.io', 'KAKUSEIDA');
                                            $this->email->to($emailOwner);
                                            // $this->email->reply_to('admin@kakuseida.conoha.io', 'KAKUSEIDA');
                                            $this->email->subject($nameOwner.'様が企画した「'.$projectName.'」プロジェクトに、新規支援がありました。');
                                            $this->email->message(
                                            $nameOwner.'様が企画した「'.$projectName.'」プロジェクトに、新規支援がありました。<br>
                                            KAKUSEIDA運営事務局です。<br>
                                            '.$nameOwner.'様が企画した「'.$projectName.'」プロジェクトに、新規支援がありました。<br>
                                            選択されたリターン品の名前は'.$returnName.'です。<br>
                                            プロジェクト画面にログインして詳細を確認してください。<br>
                                            <a href="'.app_url().'project-detail/'.$project_id.'">'.app_url().'project-detail/'.$project_id.'</a><br />');
                                            $this->email->send();
                                            // Session send message for admin
                                        }

                                        $success = array(
                                            'status'=>'Donate Success!'
                                        );
                                        $this->response($success,200);
                                    }
                                }
                            }
                        } catch(Stripe_CardError $e) {
                            $error1 = $e->getMessage();
                            $error = array(
                                'status'=>$error1);
                                $this->response($error,400);
                        } catch (Stripe_InvalidRequestError $e) {
                            // Invalid parameters were supplied to Stripe's API
                            $error2 = $e->getMessage();
                            $error = array(
                                'status'=>$error2);
                                $this->response($error,400);
                        } catch (Stripe_AuthenticationError $e) {
                            // Authentication with Stripe's API failed
                            $error3 = $e->getMessage();
                            $error = array(
                                'status'=>$error3);
                                $this->response($error,400);
                        } catch (Stripe_ApiConnectionError $e) {
                            // Network communication with Stripe failed
                            $error4 = $e->getMessage();
                            $error = array(
                                'status'=>$error4
                            );
                            $this->response($error,400);
                        } catch (Stripe_Error $e) {
                            // Display a very generic error to the user, and maybe send
                            // yourself an email
                            $error5 = $e->getMessage();
                            $error = array(
                            'status'=>$error5);
                            $this->response($error,400);
                        } catch (Exception $e) {
                            // Something else happened, completely unrelated to Stripe
                            $error6 = $e->getMessage();
                            $error = array(
                                'status'=>$error6);
                            $this->response($error,400);
                        }

                    }else{
                        $error = array(
                            'status'=>'ProjectReturn or Project not found!'
                        );
                        $this->response($error,404);
                    }
                }else{
                    $error = array(
                        'status'=>'ProjectReturn not exists!'
                    );
                    $this->response($error,404);
                }
            }else{
                $error = array(
                    'status'=>'Project not exists!'
                );
                $this->response($error,404);
            }
        }else{
            $error = array(
                'status'=>'This id is not exists!'
            );
            $this->response($error,404);
        }
    }


    public function donateBank_post(){
        $date = new DateTime();
        //fomat date
        $date = $date->format('Y-m-d H:i:s');

        $user = $this->user_data;
        $user_id =  $user->id;
        $curUser =   $this->User->getUserId($user_id);
        $email = $curUser->email;
        $userName = $curUser->username;
        $bank_number = $curUser->bank_number;

        // data nhap vao
        $project_id = $this->post('project_id');
        $backing_level_id = $this->post('backing_level_id');
        $invest_amount = $this->post('invest_amount');
        //Xem Project Detail
        $project = $this->Project->getProjectId($project_id);
        if($user_id === $project->user_id){
            $error = array(
                "status" => "error!"
            );
            $this->response($error,500);
        }
        $projectName = $project->project_name;
        if(!isset($bank_number) || $bank_number === ''){
            $bank_number = $this->post('bank_number');
        }
        $addressDefault = $this->AddressUser->getAddressUserDefault($user_id);
        if(!$addressDefault){
            $error = array(
                'status' => 'error!'
            );
            $this->response($error,500);
        }
        $dataDB = array(
            'project_id' => $project_id,
            'user_id' => $user_id,
            'id_address' => $addressDefault->id,
            'email'  =>  $addressDefault->email,
            'postcode'  =>  $addressDefault->postcode,
            'address_return'  =>  $addressDefault->address_return,
            'phone'  =>  $addressDefault->phone,
            'name_return'  =>  $addressDefault->name_return,
            'backing_level_id' => $backing_level_id,
            'invest_amount' => $invest_amount,
            // 'stripe_charge_id' => $charge->id,
            'type'=>'Bank',
            'comment' => '”'.$userName.'"は"'.$projectName.'('.$project_id.')"の"'.$backing_level_id.'"に支援しました。',
            'status' => 'unsuccess',
            'bank_number'=>$bank_number,
            'created' => $date,
            'manual_flag' => '0'
        );

        if(isset($bank_number) && $bank_number){

            if(isset($project_id) && isset($backing_level_id)){
                $existproject = $this->Project->checkProjectId($project_id);
                $existprojectretrun = $this->BackingLevel->getBackingId($backing_level_id);
                if(isset($existproject) && $existproject){
                    if(isset($existprojectretrun) && $existprojectretrun){
                        if($existprojectretrun->project_id === $existproject->id) {

                            $donateBank = $this->BackedProject->create($dataDB);
                            if(isset($donateBank) && $donateBank){
                                $success = array(
                                    'status'=>'Donate Success!'
                                );
                                $this->response($success,200);
                            }
                        }else{
                            $error = array(
                                'status'=>'ProjectReturn or Project not found!'
                            );
                            $this->response($error,404);
                        }
                    }else{
                        $error = array(
                            'status'=>'ProjectReturn not exists!'
                        );
                        $this->response($error,404);
                    }
                }else{
                    $error = array(
                        'status'=>'Project not exists!'
                    );
                    $this->response($error,404);
                }
            }else{
                $error = array(
                    'status'=>'This id is not exists!'
                );
                $this->response($error,404);
            }
        }else{
            $error = array(
                'status'=>'Please Input Your Bank Card'
            );
            $this->response($error,404);
        }
    }

    public function StopFanClub_get(){
        // info
        $fanclub_id = $this->get('project_id');
        $user = $this->user_data;
        $user_id =  $user->id;

        // get FanClub
        $fanClub = $this->Project->getFanClubsById($fanclub_id);
        // Get Profile
        $curUser =  $this->User->getUserId($user_id);

        // check usercurrent vs user of fanclub
        if($curUser->id === $fanClub->user_id) {
            $dataFanClub = array(
                'active' => 'cls'
            );
            $StopFanClub = $this->Project->update($fanclub_id,$dataFanClub);
            if($StopFanClub) {
                $fanClub->active = 'cls';
                $success = array(
                    'data' => $fanClub
                );
                $this->response($success,200);
            }else {
                $error = array(
                    'status' => 'Close FanClub Fail!'
                );
                $this->response($error,400);
            }
        }else {
            $error = array(
                'status' => 'Not Found!'
            );
            $this->response($error,404);
        }
    }

    public function sendMailDonor_post() {
        require("public/assets/socket/socket.io.php");
        $roomSocket = $this->config->item('roomSocket');
        $portRoomSocket = $this->config->item('portRoomSocket');
        // get key firebase
        $key_firebase = $this->config->item('key_firebase');

        // get info user
        $user = $this->user_data;
        $user_id =  $user->id;
        // Get Profile
        $curUser =  $this->User->getUserId($user_id);

        $email = $this->post('email');
        $idBacked = $this->post('id');
        $Backed = $this->BackedProject->getBackedProjectById($idBacked);

        $Project = $this->Project->getProjectId($Backed->project_id);

        $DonorId = $Backed->user_id;
        $Donor = $this->User->getUserId($DonorId);
        $DonorName = $Donor->username;
        $tokenDonor = $Donor->token;

        $nameOwner = $curUser->username;
        $projectName = $Project->project_name;

        if($Project->user_id === $curUser->id) {
            if($Backed && $Backed->sendPackage === '0') {
                $this->load->library('email');

                $this->email->from('admin@kakuseida.conoha.io', 'KAKUSEIDA');
                $this->email->to($email);
                // $this->email->reply_to('admin@kakuseida.conoha.io', 'KAKUSEIDA');
                $this->email->subject($DonorName.'様に選択していただいたリターン品を送付致しました');

                $this->email->message(
                    '<br>'.$DonorName.'様<br>
                    <br>
                    お世話になっております。<br>
                    KAKUSEIDAにて「'.$projectName.'」プロジェクトを企画致しました'.$nameOwner.'と申します。<br>
                    <br>
                    「'.$projectName.'」プロジェクトを支援していただき、誠にありがとうございます。<br>
                    私の活動にご理解いただき、ご協力いただいたご厚意に深く感謝しております。<br>
                    <br>
                    このたび、'.$DonorName.' 様に選択していただいたリターン品を送付致しました事をご連絡致します。<br>
                    リターン品に関して、質問等ありましたら遠慮なくお問い合わせください。<br>
                    <br>
                    また、活動の詳細は、改めてご報告致します。<br>
                    今後とも、どうか末永いお力添えを頂きますよう<br>
                    宜しくお願い申し上げます。<br>
                    <br>
                    メールにて恐縮ではございますが、<br>
                    取り急ぎお礼を申し上げます。<br>'
                );
                $sendMail = $this->email->send();
                if($sendMail) {

                    $data = array (
                        'sendPackage' => '1'
                    );
                    $update = $this->BackedProject->update($idBacked,$data);

                    if($update) {

                        $datetime = new DateTime();
                        $datetime = $datetime->format('Y-m-d H:i:s');
                        // get strtotime + change time zone
                        $asia_timestamp = strtotime($datetime);
                        date_default_timezone_set('UTC');
                        // get Date vs time zone
                        $date = date("Y-m-d H:i:s", $asia_timestamp);

                        // array create notification info
                        $dataNotification = array (
                            'user_id' => $DonorId,
                            'user_action' => 'Admin',
                            'title' =>  'プロジェクトのリターン品を送付致しました',
                            'message' =>'「'.$projectName.'」プロジェクトのリターン品を送付致しました',
                            'link'=> "project-detail/".$Backed->project_id,
                            'status' => 0,
                            'thumbnail' => base_url()."images/2018/default/logo-icon.png",
                            'created' => $date
                        );
                        // create notification info
                        $createNotification = $this->Notification_info->create($dataNotification);

                        $socketio = new SocketIO();
                        // // array
                        $data = array(
                            'room' => 'user_'.$DonorId,
                            'title' => 'プロジェクトのリターン品を送付致しました',
                            'message' => '「'.$projectName.'」プロジェクトのリターン品を送付致しました',
                            'url'=> app_url()."project-detail/".$Backed->project_id,
                            'ava' => base_url().'images/2018/default/logo-icon.png'
                        );


                        $socketio->send($roomSocket, $portRoomSocket, 'annouce_project',json_encode($data));

                        $curl = curl_init();

                        curl_setopt_array($curl, array(
                          CURLOPT_URL => "https://fcm.googleapis.com/fcm/send",
                          CURLOPT_RETURNTRANSFER => true,
                          CURLOPT_ENCODING => "",
                          CURLOPT_MAXREDIRS => 10,
                          CURLOPT_TIMEOUT => 30,
                          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                          CURLOPT_CUSTOMREQUEST => "POST",
                          CURLOPT_POSTFIELDS => "{   \"notification\": {\r\n        \"title\": \"プロジェクトのリターン品を送付致しました\",\r\n        \"body\": \"「".$projectName."」プロジェクトのリターン品を送付致しました\",\r\n        \"click_action\": \"".app_url()."project-detail/".$Backed->project_id."\",\r\n        \"icon\": \"".base_url()."images/2018/default/logo-icon.png\"\r\n    },\r\n  \"to\" : \"".$tokenDonor."\"\r\n}",
                          CURLOPT_HTTPHEADER => array(
                            "Authorization: key=".$key_firebase."",
                            "Content-Type: application/json",
                          ),
                        ));

                        $response = curl_exec($curl);
                        $err = curl_error($curl);

                        curl_close($curl);

                        $success = array(
                            'status' => 'sendMail thanh cong!'
                        );
                        $this->response($success,200);
                    } else {
                        $error = array(
                            'status' => 'リターン品送付メールの送信を完了しました '
                        );
                        $this->response($error,400);
                    }
                } else {
                    $error = array(
                        'status' => 'リターン品送付メールの送信を完了しました '
                    );
                    $this->response($error,400);
                }
            }else {
                $error = array(
                    'status' => 'リターン品送付メールの送信を完了しました '
                );
                $this->response($error,400);
            }
        }else {
            $error = array(
                'status' => 'Not Found!'
            );
            $this->response($error,404);
        }
    }

    /**
     * [getAllOwnerDonater_get get all project i donate]
     * @return [type] [description]
     */
    public function getAllOwnerDonater_get() {
        // get info user
        $user = $this->user_data;
        $user_id =  $user->id;
        // get page
        $page = $this->get('page');
        // limit
        $limit = '10';
        // get page
        if(!$page || $page =='0'){
          $page = '0';
        }else{
          $page = $page * $limit;
        }
        // limit use CI
        $input['limit'] = array($limit,$page);
        // Get Profile
        $curUser =  $this->User->getUserId($user_id);

        if(isset($curUser) && $curUser){

            $getAllProjectDonate = $this->BackedProject->getOwnerProjectDonate($user_id,$input);

            $totalPages = ceil($getAllProjectDonate['total'] / $limit);

            $data = array(
                'totalPages' => $totalPages,
                'email'     => $getAllProjectDonate['project']
            );

            $this->response($data,200);

        }else {

            $error = array(
                'status' => 'Error!'
            );

            $this->response($error,404);

        }
    }
}
