<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Upload extends UserController {
    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->auth();
    }
    public function profile_post() {
        $user = $this->user_data;
        $user_id =  $user->id;
        $curUser =   $this->User->getUserId($user_id);
        $username = $curUser->username;
        $time = time();
        $date = new Datetime();
        $month = date('m');
        $year = date('Y');
        if(isset($_FILES["files"])){
            $new_name = time().$_FILES["files"]['name'];
        }else{
            $error = array('error' => 'ファイルの形式は*.jpg、*.jpeg、*.pngだけでください。');
            $this->response($error, 400);
        }

        $config['upload_path']          =   './static/images/'.$year.'/'.$username;
        $config['allowed_types']        =   'jpg|png|jpeg';
        $config['file_name']            =   $new_name;
        $config['encrypt_name']         =   TRUE;

        //check thư mục đã tồn tại chưa nếu chưa thì tạo
        if (!file_exists('./static/images/'.$year.'/'.$username)) {
            mkdir('./static/images/'.$year.'/'.$username , 0777, true);
        }
        //load thư viện vả sử dụng thư viện upload
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        // kiểm tra file đã upload chưa
        if ( ! $this->upload->do_upload('files')) {
            // Đá lỗi
            $error = array('error' => 'アップロードされた画像の容量は2MB以上ですので、2MB以下の画像を再度アップロードしてください。');
            $this->response($error, 400);
        } else {
            $dataname = $this->upload->data();
            $filename = $dataname['file_name'];

            //create new forder vs quyền 777;
            if (!file_exists('./static/images/'.$year.'/'.$username.'/profileImage')) {
                mkdir('./static/images/'.$year.'/'.$username.'/profileImage' , 0777, true);
            }
            //sử dụng thư viện image_lib và ImageMagick
            $this->load->library('image_lib');
            //config
            $config = array (
                'image_library' => 'ImageMagick',
                'library_path' => '/usr/bin',
                'source_image' => './static/images/'.$year.'/'.$username.'/'.$filename,
                'quality' => '80%',
                'maintain_ratio' => false,
                'x_axis' => 0,
                'y_axis' => 0,
                'new_image' => './static/images/'.$year.'/'.$username.'/profileImage/small_'.$filename,
                'width' =>  50,
                'height' => 50
            );

            $this->image_lib->initialize($config);
            $this->image_lib->resize('files');

            $config1 = array(
                'image_library' => 'ImageMagick',
                'library_path' => '/usr/bin',
                'source_image' => './static/images/'.$year.'/'.$username.'/'.$filename,
                'quality' => '80%',
                'maintain_ratio' => false,
                'x_axis' => 0,
                'y_axis' => 0,
                'new_image' => './static/images/'.$year.'/'.$username.'/profileImage/medium_'.$filename,
                'width' =>  240,
                'height' => 240
            );

            $this->image_lib->initialize($config1);
            $this->image_lib->resize('files');

            $config2 = array(
                'image_library' => 'ImageMagick',
                'library_path' => '/usr/bin',
                'source_image' => './static/images/'.$year.'/'.$username.'/'.$filename,
                'quality' => '80%',
                'maintain_ratio' => false,
                'x_axis' => 0,
                'y_axis' => 0,
                'new_image' => './static/images/'.$year.'/'.$username.'/profileImage/'.$filename,
                'width' =>  300,
                'height' => 300
            );

            //sử dụng $new_config
            $this->image_lib->initialize($config2);
            // kiểm tra và đá lỗi
            if (!$this->image_lib->resize('files')){
                $error = array('error' => strip_tags($this->image_lib->display_errors()));
                $this->response($error, 400);
            } else {
                $firstIndex = stripos($filename, '.');
                $pathinfo =  pathinfo($filename, PATHINFO_EXTENSION);
                $name = substr($filename, 0, $firstIndex);
                $data = array(
                    'base_url'=> base_url(),
                    'forder' => 'images/'.$year.'/'.$username.'/profileImage/',
                    'name_image' => $name.'.',
                    'mime_type' => $pathinfo
                );
                $this->response($data, 200);
            }
        }
    }

    public function uploads_post(){
        $user = $this->user_data;
        $user_id =  $user->id;
        $curUser =   $this->User->getUserId($user_id);
        $username = $curUser->username;
        $time = time();
        $date = new Datetime();
        $month = date('m');
        $year = date('Y');
        if(isset($_FILES["files"])){
            $new_name = time().$_FILES["files"]['name'];
        }else{
            $error = array('error' => 'ファイルの形式は*.jpg、*.jpeg、*.pngだけでください。');
            $this->response($error, 400);
        }

        $config['upload_path']          = './static/images/'.$year.'/'.$username;
        $config['allowed_types']        = 'jpg|png|jpeg';
        $config['file_name'] = $new_name;
        $config['encrypt_name'] = TRUE;

        //check thư mục đã tồn tại chưa nếu chưa thì tạo
        if (!file_exists('./static/images/'.$year.'/'.$username)) {
            mkdir('./static/images/'.$year.'/'.$username , 0777, true);
        }
        //load thư viện vả sử dụng thư viện upload
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        // kiểm tra file đã upload chưa
        if ( ! $this->upload->do_upload('files')) {
            // Đá lỗi
            $error = array('error' => 'アップロードされた画像の容量は2MB以上ですので、2MB以下の画像を再度アップロードしてください。');
            $this->response($error, 400);
        } else {
            $dataname = $this->upload->data();
            $filename = $dataname['file_name'];
            //create new forder vs quyền 777;
            if (!file_exists('./static/images/'.$year.'/'.$username.'/projectImage/thumbnail')) {
                mkdir('./static/images/'.$year.'/'.$username.'/projectImage/thumbnail' , 0777, true);
            }
            //sử dụng thư viện image_lib và ImageMagick
            $this->load->library('image_lib');
            //config
            $config = array (
                'image_library' => 'ImageMagick',
                'library_path' => '/usr/bin',
                'source_image' => './static/images/'.$year.'/'.$username.'/'.$filename,
                'quality' => '80%',
                'maintain_ratio' => false,
                'x_axis' => 0,
                'y_axis' => 0,
                'new_image' => './static/images/'.$year.'/'.$username.'/projectImage/thumbnail/small_'.$filename,
                'width' =>  50,
                'height' => 50
            );

            $this->image_lib->initialize($config);
            $this->image_lib->resize('files');


            $config1 = array(
                'image_library' => 'ImageMagick',
                'library_path' => '/usr/bin',
                'source_image' => './static/images/'.$year.'/'.$username.'/'.$filename,
                'quality' => '80%',
                'maintain_ratio' => false,
                'x_axis' => 0,
                'y_axis' => 0,
                'new_image' => './static/images/'.$year.'/'.$username.'/projectImage/thumbnail/medium_'.$filename,
                'width' =>  300,
                'height' => 200
            );

            $this->image_lib->initialize($config1);
            $this->image_lib->resize('files');

            $config2 = array(
                'image_library' => 'ImageMagick',
                'library_path' => '/usr/bin',
                'source_image' => './static/images/'.$year.'/'.$username.'/'.$filename,
                'quality' => '80%',
                'maintain_ratio' => false,
                'x_axis' => 0,
                'y_axis' => 0,
                'new_image' => './static/images/'.$year.'/'.$username.'/projectImage/thumbnail/'.$filename,
                'width' =>  600,
                'height' => 400
            );

            //sử dụng $new_config
            $this->image_lib->initialize($config2);
            // kiểm tra và đá lỗi
            if (!$this->image_lib->resize('files')){
                $error = array('error' => strip_tags($this->image_lib->display_errors()));
                $this->response($error, 400);
            } else {
                $firstIndex = stripos($filename, '.');
                $pathinfo =  pathinfo($filename, PATHINFO_EXTENSION);
                $name = substr($filename, 0, $firstIndex);
                $data = array(
                    'base_url'=> base_url(),
                    'forder' => 'images/'.$year.'/'.$username.'/projectImage/thumbnail/',
                    'name_image' => $name.'.',
                    'mime_type' => $pathinfo
                );
                $this->response($data, 200);
            }
        }
    }


    public function thumbnail_post(){
        $user = $this->user_data;
        $user_id =  $user->id;
        $curUser =   $this->User->getUserId($user_id);
        $username = $curUser->username;
        $time = time();
        $date = new Datetime();
        $month = date('m');
        $year = date('Y');
        if(isset($_FILES["files"])){
            $new_name = time().$_FILES["files"]['name'];
        }else{
            $error = array('error' => 'ファイルの形式は*.jpg、*.jpeg、*.pngだけでください。');
            $this->response($error, 400);
        }

        $config['upload_path']          =   './static/images/'.$year.'/'.$username;
        $config['allowed_types']        =    'jpg|png|jpeg';
        $config['file_name']            =    $new_name;
        $config['encrypt_name']         =    TRUE;

        //check thư mục đã tồn tại chưa nếu chưa thì tạo
        if (!file_exists('./static/images/'.$year.'/'.$username)) {
            mkdir('./static/images/'.$year.'/'.$username , 0777, true);
        }

        //load thư viện vả sử dụng thư viện upload
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        // kiểm tra file đã upload chưa
        if ( ! $this->upload->do_upload('files')) {
            // Đá lỗi
            $error = array('error' => 'アップロードされた画像の容量は2MB以上ですので、2MB以下の画像を再度アップロードしてください。');
            $this->response($error, 400);
        } else {
            $dataname = $this->upload->data();
            $filename = $dataname['file_name'];
            //create new forder vs quyền 777;
            if (!file_exists('./static/images/'.$year.'/'.$username.'/projectImage/thumbnail')) {
                mkdir('./static/images/'.$year.'/'.$username.'/projectImage/thumbnail' , 0777, true);
            }
            //sử dụng thư viện image_lib và ImageMagick
            $this->load->library('image_lib');
            //config
            $config = array (
                'image_library' => 'ImageMagick',
                'library_path' => '/usr/bin',
                'source_image' => './static/images/'.$year.'/'.$username.'/'.$filename,
                'quality' => '80%',
                'maintain_ratio' => false,
                'x_axis' => 0,
                'y_axis' => 0,
                'new_image' => './static/images/'.$year.'/'.$username.'/projectImage/thumbnail/small_'.$filename,
                'width' =>  50,
                'height' => 50
            );

            $this->image_lib->initialize($config);
            $this->image_lib->resize('files');


            $config1 = array(
                'image_library' => 'ImageMagick',
                'library_path' => '/usr/bin',
                'source_image' => './static/images/'.$year.'/'.$username.'/'.$filename,
                'quality' => '80%',
                'maintain_ratio' => false,
                'x_axis' => 0,
                'y_axis' => 0,
                'new_image' => './static/images/'.$year.'/'.$username.'/projectImage/thumbnail/medium_'.$filename,
                'width' =>  300,
                'height' => 200
            );

            $this->image_lib->initialize($config1);
            $this->image_lib->resize('files');

            $config2 = array(
                'image_library' => 'ImageMagick',
                'library_path' => '/usr/bin',
                'source_image' => './static/images/'.$year.'/'.$username.'/'.$filename,
                'quality' => '80%',
                'maintain_ratio' => false,
                'x_axis' => 0,
                'y_axis' => 0,
                'new_image' => './static/images/'.$year.'/'.$username.'/projectImage/thumbnail/'.$filename,
                'width' =>  600,
                'height' => 400
            );

            //sử dụng $new_config
            $this->image_lib->initialize($config2);
            // kiểm tra và đá lỗi
            if (!$this->image_lib->resize('files')){
                $error = array('error' => strip_tags($this->image_lib->display_errors()));
                $this->response($error, 400);
            } else {
                $firstIndex = stripos($filename, '.');
                $pathinfo =  pathinfo($filename, PATHINFO_EXTENSION);
                $name = substr($filename, 0, $firstIndex);
                $data = array(
                    'base_url'=> base_url(),
                    'forder' => 'images/'.$year.'/'.$username.'/projectImage/thumbnail/',
                    'name_image' => $name.'.',
                    'mime_type' => $pathinfo
                );
                $this->response($data, 200);
            }
        }
    }

    public function thumbnailDes_post(){
        $user = $this->user_data;
        $user_id =  $user->id;
        $curUser =   $this->User->getUserId($user_id);
        $username = $curUser->username;
        $time = time();
        $date = new Datetime();
        $month = date('m');
        $year = date('Y');
        if(isset($_FILES["files"])){
            $new_name = time().$_FILES["files"]['name'];
        }else{
            $error = array('error' => 'ファイルの形式は*.jpg、*.jpeg、*.pngだけでください。');
            $this->response($error, 400);
        }

        $config['upload_path']          = './static/images/'.$year.'/'.$username;
        $config['allowed_types']        = 'jpg|png|jpeg';
        $config['file_name'] = $new_name;
        $config['encrypt_name'] = TRUE;

        //check thư mục đã tồn tại chưa nếu chưa thì tạo
        if (!file_exists('./static/images/'.$year.'/'.$username)) {
            mkdir('./static/images/'.$year.'/'.$username , 0777, true);
        }
        //load thư viện vả sử dụng thư viện upload
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        // kiểm tra file đã upload chưa
        if ( ! $this->upload->do_upload('files')) {
            // Đá lỗi
            $error = array('error' => 'アップロードされた画像の容量は2MB以上ですので、2MB以下の画像を再度アップロードしてください。');
            $this->response($error, 400);
        } else {
            $dataname = $this->upload->data();
            $filename = $dataname['file_name'];
            //create new forder vs quyền 777;
            if (!file_exists('./static/images/'.$year.'/'.$username.'/projectImage/thumbnailDes')) {
                mkdir('./static/images/'.$year.'/'.$username.'/projectImage/thumbnailDes' , 0777, true);
            }
            //sử dụng thư viện image_lib và ImageMagick
            $this->load->library('image_lib');
            //config
            $config = array (
                'image_library' => 'ImageMagick',
                'library_path' => '/usr/bin',
                'source_image' => './static/images/'.$year.'/'.$username.'/'.$filename,
                'quality' => '80%',
                'maintain_ratio' => false,
                'x_axis' => 0,
                'y_axis' => 0,
                'new_image' => './static/images/'.$year.'/'.$username.'/projectImage/thumbnailDes/small_'.$filename,
                'width' =>  50,
                'height' => 50
            );

            $this->image_lib->initialize($config);
            $this->image_lib->resize('files');


            $config1 = array(
                'image_library' => 'ImageMagick',
                'library_path' => '/usr/bin',
                'source_image' => './static/images/'.$year.'/'.$username.'/'.$filename,
                'quality' => '80%',
                'maintain_ratio' => false,
                'x_axis' => 0,
                'y_axis' => 0,
                'new_image' => './static/images/'.$year.'/'.$username.'/projectImage/thumbnailDes/medium_'.$filename,
                'width' =>  300,
                'height' => 200
            );

            $this->image_lib->initialize($config1);
            $this->image_lib->resize('files');

            $config2 = array(
                'image_library' => 'ImageMagick',
                'library_path' => '/usr/bin',
                'source_image' => './static/images/'.$year.'/'.$username.'/'.$filename,
                'quality' => '80%',
                'maintain_ratio' => false,
                'x_axis' => 0,
                'y_axis' => 0,
                'new_image' => './static/images/'.$year.'/'.$username.'/projectImage/thumbnailDes/'.$filename,
                'width' =>  600,
                'height' => 400
            );

            //sử dụng $new_config
            $this->image_lib->initialize($config2);
            // kiểm tra và đá lỗi
            if (!$this->image_lib->resize('files')){
                $error = array('error' => strip_tags($this->image_lib->display_errors()));
                $this->response($error, 400);
            } else {
                $firstIndex = stripos($filename, '.');
                $pathinfo =  pathinfo($filename, PATHINFO_EXTENSION);
                $name = substr($filename, 0, $firstIndex);
                $data = array(
                    'base_url'=> base_url(),
                    'forder' => 'images/'.$year.'/'.$username.'/projectImage/thumbnailDes/',
                    'name_image' => $name.'.',
                    'mime_type' => $pathinfo
                );
                $this->response($data, 200);
            }
        }
    }

    public function thumbnailDes1_post(){
        $user = $this->user_data;
        $user_id =  $user->id;
        $curUser =   $this->User->getUserId($user_id);
        $username = $curUser->username;
        $time = time();
        $date = new Datetime();
        $month = date('m');
        $year = date('Y');
        if(isset($_FILES["filesdes1"])){
            $new_name = time().$_FILES["filesdes1"]['name'];
        }else{
            $error = array('error' => 'ファイルの形式は*.jpg、*.jpeg、*.pngだけでください。');
            $this->response($error, 400);
        }

        $config['upload_path']          = './static/images/'.$year.'/'.$username;
        $config['allowed_types']        = 'jpg|png|jpeg';
        $config['file_name'] = $new_name;
        $config['encrypt_name'] = TRUE;

        //check thư mục đã tồn tại chưa nếu chưa thì tạo
        if (!file_exists('./static/images/'.$year.'/'.$username)) {
            mkdir('./static/images/'.$year.'/'.$username , 0777, true);
        }
        //load thư viện vả sử dụng thư viện upload
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        // kiểm tra file đã upload chưa
        if ( ! $this->upload->do_upload('filesdes1')) {
            // Đá lỗi
            $error = array('error' => 'アップロードされた画像の容量は2MB以上ですので、2MB以下の画像を再度アップロードしてください。');
            $this->response($error, 400);
        } else {
            $dataname = $this->upload->data();
            $filename = $dataname['file_name'];
            //create new forder vs quyền 777;
            if (!file_exists('./static/images/'.$year.'/'.$username.'/projectImage/thumbnailDes')) {
                mkdir('./static/images/'.$year.'/'.$username.'/projectImage/thumbnailDes' , 0777, true);
            }
            //sử dụng thư viện image_lib và ImageMagick
            $this->load->library('image_lib');
            //config
            $config = array (
                'image_library' => 'ImageMagick',
                'library_path' => '/usr/bin',
                'source_image' => './static/images/'.$year.'/'.$username.'/'.$filename,
                'quality' => '80%',
                'maintain_ratio' => false,
                'x_axis' => 0,
                'y_axis' => 0,
                'new_image' => './static/images/'.$year.'/'.$username.'/projectImage/thumbnailDes/small_'.$filename,
                'width' =>  50,
                'height' => 50
            );

            $this->image_lib->initialize($config);
            $this->image_lib->resize('files');


            $config1 = array(
                'image_library' => 'ImageMagick',
                'library_path' => '/usr/bin',
                'source_image' => './static/images/'.$year.'/'.$username.'/'.$filename,
                'quality' => '80%',
                'maintain_ratio' => false,
                'x_axis' => 0,
                'y_axis' => 0,
                'new_image' => './static/images/'.$year.'/'.$username.'/projectImage/thumbnailDes/medium_'.$filename,
                'width' =>  300,
                'height' => 200
            );

            $this->image_lib->initialize($config1);
            $this->image_lib->resize('files');

            $config2 = array(
                'image_library' => 'ImageMagick',
                'library_path' => '/usr/bin',
                'source_image' => './static/images/'.$year.'/'.$username.'/'.$filename,
                'quality' => '80%',
                'maintain_ratio' => false,
                'x_axis' => 0,
                'y_axis' => 0,
                'new_image' => './static/images/'.$year.'/'.$username.'/projectImage/thumbnailDes/'.$filename,
                'width' =>  600,
                'height' => 400
            );

            //sử dụng $new_config
            $this->image_lib->initialize($config2);
            // kiểm tra và đá lỗi
            if (!$this->image_lib->resize('filesdes1')){
                $firstIndex = stripos($filename, '.');
                $pathinfo =  pathinfo($filename, PATHINFO_EXTENSION);
                $name = substr($filename, 0, $firstIndex);
                $data = array(
                    'base_url'=> base_url(),
                    'forder' => 'images/'.$year.'/'.$username.'/',
                    'name_image' => $name.'.',
                    'mime_type' => $pathinfo
                );
                $this->response($data, 200);
            } else {
                $firstIndex = stripos($filename, '.');
                $pathinfo =  pathinfo($filename, PATHINFO_EXTENSION);
                $name = substr($filename, 0, $firstIndex);
                $data = array(
                    'base_url'=> base_url(),
                    'forder' => 'images/'.$year.'/'.$username.'/projectImage/thumbnailDes/',
                    'name_image' => $name.'.',
                    'mime_type' => $pathinfo
                );
                $this->response($data, 200);
            }
        }
    }


    public function thumbnailReturn_post(){
        $user = $this->user_data;
        $user_id =  $user->id;
        $curUser =   $this->User->getUserId($user_id);
        $username = $curUser->username;
        $time = time();
        $date = new Datetime();
        $month = date('m');
        $year = date('Y');
        if(isset($_FILES["files"])){
            $new_name = time().$_FILES["files"]['name'];
        }else{
            $error = array('error' => 'ファイルの形式は*.jpg、*.jpeg、*.pngだけでください。');
            $this->response($error, 400);
        }

        $config['upload_path']          = './static/images/'.$year.'/'.$username;
        $config['allowed_types']        = 'jpg|png|jpeg';
        $config['file_name'] = $new_name;
        $config['encrypt_name'] = TRUE;

        //check thư mục đã tồn tại chưa nếu chưa thì tạo
        if (!file_exists('./static/images/'.$year.'/'.$username)) {
            mkdir('./static/images/'.$year.'/'.$username , 0777, true);
        }
        //load thư viện vả sử dụng thư viện upload
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        // kiểm tra file đã upload chưa
        if ( ! $this->upload->do_upload('files')) {
            // Đá lỗi
            $error = array('error' => 'アップロードされた画像の容量は2MB以上ですので、2MB以下の画像を再度アップロードしてください。');
            $this->response($error, 400);
        } else {
            $dataname = $this->upload->data();
            $filename = $dataname['file_name'];
            //create new forder vs quyền 777;
            if (!file_exists('./static/images/'.$year.'/'.$username.'/projectImage/thumbnailReturn')) {
                mkdir('./static/images/'.$year.'/'.$username.'/projectImage/thumbnailReturn' , 0777, true);
            }
            //sử dụng thư viện image_lib và ImageMagick
            $this->load->library('image_lib');
            //config
            $config = array (
                'image_library' => 'ImageMagick',
                'library_path' => '/usr/bin',
                'source_image' => './static/images/'.$year.'/'.$username.'/'.$filename,
                'quality' => '80%',
                'maintain_ratio' => false,
                'x_axis' => 0,
                'y_axis' => 0,
                'new_image' => './static/images/'.$year.'/'.$username.'/projectImage/thumbnailReturn/small_'.$filename,
                'width' =>  50,
                'height' => 50
            );

            $this->image_lib->initialize($config);
            $this->image_lib->resize('files');


            $config1 = array(
                'image_library' => 'ImageMagick',
                'library_path' => '/usr/bin',
                'source_image' => './static/images/'.$year.'/'.$username.'/'.$filename,
                'quality' => '80%',
                'maintain_ratio' => false,
                'x_axis' => 0,
                'y_axis' => 0,
                'new_image' => './static/images/'.$year.'/'.$username.'/projectImage/thumbnailReturn/medium_'.$filename,
                'width' =>  300,
                'height' => 200
            );

            $this->image_lib->initialize($config1);
            $this->image_lib->resize('files');

            $config2 = array(
                'image_library' => 'ImageMagick',
                'library_path' => '/usr/bin',
                'source_image' => './static/images/'.$year.'/'.$username.'/'.$filename,
                'quality' => '80%',
                'maintain_ratio' => false,
                'x_axis' => 0,
                'y_axis' => 0,
                'new_image' => './static/images/'.$year.'/'.$username.'/projectImage/thumbnailReturn/'.$filename,
                'width' =>  600,
                'height' => 400
            );

            //sử dụng $new_config
            $this->image_lib->initialize($config2);
            // kiểm tra và đá lỗi
            if (!$this->image_lib->resize('files')){
                $error = array('error' => strip_tags($this->image_lib->display_errors()));
                $this->response($error, 400);
            } else {
                $firstIndex = stripos($filename, '.');
                $pathinfo =  pathinfo($filename, PATHINFO_EXTENSION);
                $name = substr($filename, 0, $firstIndex);
                $data = array(
                    'base_url'=> base_url(),
                    'forder' => 'images/'.$year.'/'.$username.'/projectImage/thumbnailReturn/',
                    'name_image' => $name.'.',
                    'mime_type' => $pathinfo
                );
                $this->response($data, 200);
            }
        }
    }

    public function thumbnailEditor_post(){
        $user = $this->user_data;
        $user_id =  $user->id;
        $curUser =   $this->User->getUserId($user_id);
        $username = $curUser->username;
        $time = time();
        $date = new Datetime();
        $month = date('m');
        $year = date('Y');
        if(isset($_FILES["files"])){
            $new_name = time().$_FILES["files"]['name'];
        }else{
            $error = array('error' => 'ファイルの形式は*.jpg、*.jpeg、*.pngだけでください。');
            $this->response($error, 400);
        }

        $config['upload_path']          = './static/images/'.$year.'/'.$username;
        $config['allowed_types']        = 'jpg|png|jpeg';
        $config['file_name'] = $new_name;
        $config['encrypt_name'] = TRUE;

        //check thư mục đã tồn tại chưa nếu chưa thì tạo
        if (!file_exists('./static/images/'.$year.'/'.$username)) {
            mkdir('./static/images/'.$year.'/'.$username , 0777, true);
        }
        //load thư viện vả sử dụng thư viện upload
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        // kiểm tra file đã upload chưa
        if ( ! $this->upload->do_upload('files')) {
            // Đá lỗi
            $error = array('error' => 'アップロードされた画像の容量は2MB以上ですので、2MB以下の画像を再度アップロードしてください。');
            $this->response($error, 400);
        } else {
            $dataname = $this->upload->data();
            $filename = $dataname['file_name'];
            //create new forder vs quyền 777;
            if (!file_exists('./static/images/'.$year.'/'.$username.'/projectImage/thumbnailEditor')) {
                mkdir('./static/images/'.$year.'/'.$username.'/projectImage/thumbnailEditor' , 0777, true);
            }
            //sử dụng thư viện image_lib và ImageMagick
            $this->load->library('image_lib');
            //config
            $new_config['image_library']  = 'ImageMagick';
            $new_config['library_path'] = '/usr/bin';
            $new_config['source_image'] = './static/images/'.$year.'/'.$username.'/'.$filename;
            $new_config['new_image'] = './static/images/'.$year.'/'.$username.'/projectImage/thumbnailEditor/'.$filename;
            $new_config['quality'] = '80%';

            //sử dụng $new_config
            $this->image_lib->initialize($new_config);
            // kiểm tra và đá lỗi
            if (!$this->image_lib->resize('files')){
                $error = array('error' => strip_tags($this->image_lib->display_errors()));
                $this->response($error, 400);
            } else {
                $firstIndex = stripos($filename, '.');
                $pathinfo =  pathinfo($filename, PATHINFO_EXTENSION);
                $name = substr($filename, 0, $firstIndex);
                $data = array(
                    'base_url'=> base_url(),
                    'forder' => 'images/'.$year.'/'.$username.'/projectImage/thumbnailEditor/',
                    'name_image' => $name.'.',
                    'mime_type' => $pathinfo
                );
                $this->response($data, 200);
            }
        }
    }

    public function UploadImage_post() {
        $user = $this->user_data;
        $user_id =  $user->id;
        $curUser =   $this->User->getUserId($user_id);
        $username = $curUser->username;

        $time = time();
        $date = new Datetime();
        $month = date('m');
        $year = date('Y');
        if(isset($_FILES["files"])){
            $new_name = $_FILES["files"]['name'];
        } else {
            $error = array('error' => 'ファイルの形式は*.jpg、*.jpeg、*.pngだけでください。');
            $this->response($error, 400);
        }

        $config['upload_path']          = './static/images/'.$year.'/'.$username;
        $config['allowed_types']        = 'jpg|png|jpeg';
        $config['file_name'] = $new_name;
        $config['encrypt_name'] = TRUE;

        //check thư mục đã tồn tại chưa nếu chưa thì tạo
        if (!file_exists('./static/images/'.$year.'/'.$username)) {
            mkdir('./static/images/'.$year.'/'.$username , 0777, true);
        }

        //load thư viện vả sử dụng thư viện upload
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        // kiểm tra file đã upload chưa
        if ( ! $this->upload->do_upload('files')) {
            // Đá lỗi
            $error = array('error' => 'アップロードされた画像の容量は2MB以上ですので、2MB以下の画像を再度アップロードしてください。');
            $this->response($error, 400);

        } else {
            $dataname = $this->upload->data();
            $filename = $dataname['file_name'];
            //create new forder vs quyền 777;
            if (!file_exists('./static/images/'.$year.'/'.$username.'/projectImage/thumbnail')) {
                mkdir('./static/images/'.$year.'/'.$username.'/projectImage/thumbnail' , 0777, true);
            }
            //sử dụng thư viện image_lib và ImageMagick
            $this->load->library('image_lib');
            //config

            $config = array (
                'image_library' => 'ImageMagick',
                'library_path' => '/usr/bin',
                'source_image' => './static/images/'.$year.'/'.$username.'/'.$filename,
                'quality' => '80%',
                'maintain_ratio' => false,
                'x_axis' => 0,
                'y_axis' => 0,
                'new_image' => './static/images/'.$year.'/'.$username.'/projectImage/thumbnail/small_'.$filename,
                'width' =>  50,
                'height' => 50
            );

            $this->image_lib->initialize($config1);
            $this->image_lib->resize('files');


            $config2 = array(
                'image_library' => 'ImageMagick',
                'library_path' => '/usr/bin',
                'source_image' => './static/images/'.$year.'/'.$username.'/'.$filename,
                'quality' => '80%',
                'maintain_ratio' => false,
                'x_axis' => 0,
                'y_axis' => 0,
                'new_image' => './static/images/'.$year.'/'.$username.'/projectImage/thumbnail/medium_'.$filename,
                'width' =>  240,
                'height' => 240
            );

            $this->image_lib->initialize($config2);
            $this->image_lib->resize('files');

            $config1 = array(
                'image_library' => 'ImageMagick',
                'library_path' => '/usr/bin',
                'source_image' => './static/images/'.$year.'/'.$username.'/'.$filename,
                'quality' => '80%',
                'maintain_ratio' => false,
                'x_axis' => 0,
                'y_axis' => 0,
                'new_image' => './static/images/'.$year.'/'.$username.'/projectImage/thumbnail/'.$filename,
                'width' =>  600,
                'height' => 400
            );


            //sử dụng $new_config
            $this->image_lib->initialize($config);
            // kiểm tra và đá lỗi
            if (!$this->image_lib->resize('files')){
                $error = array('error' => strip_tags($this->image_lib->display_errors()));
                $this->response($error, 400);
            } else {
                $firstIndex = stripos($filename, '.');
                $pathinfo =  pathinfo($filename, PATHINFO_EXTENSION);
                $name = substr($filename, 0, $firstIndex);
                $data = array(
                    'base_url'=> base_url(),
                    'forder' => 'images/'.$year.'/'.$username.'/projectImage/thumbnail/',
                    'name_image' => $name.'.',
                    'mime_type' => $pathinfo
                );
                $this->response($data, 200);
            }
        }
    }
}
