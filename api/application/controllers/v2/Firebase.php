<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Firebase extends UserController {
    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->auth();
    }
    /**
     * [createComment_post Create Comment]
     * @return [type] [description]
    */
    public function createTokenNotification_get(){
        $key_firebase = $this->config->item('key_firebase');
        // get user_id flow $this->auth();
        $user = $this->user_data;
        $user_id =  $user->id;

        // get Token by get in token
        // $idToken = $this->get('token');
        // Get User
        $user = $this->User->getUserId($user_id);

        // Check User is exists!
        // var_dump($user);
        // if(isset($user) && $user){

            // Check user has token , if no, then create token and yes then no create token
            // if($user->statusToken === true){
                $ch = curl_init( 'https://fcm.googleapis.com/fcm/send' );
                # Setup request to send json via POST.
                $payload = json_encode( array(
                    "notification" => array(
                        "title" => "Firebase",
                        "body" => "Firebase is awesome",
                        "click_action" => "http://localhost:3000/",
                        "icon" => "".base_url()."images/2018/default/logo.png"
                    ),
                    "to" => "topics/newmovie"
                ) );
                curl_setopt( $ch, CURLOPT_POSTFIELDS, $payload );
                curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json','Authorization:key='.$key_firebase.''));
                # Return response instead of printing.
                curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
                # Send request.
                $result = curl_exec($ch);
                curl_close($ch);
                # Print response.
                var_dump($result);
            // }else{
            //
            // }
        // }
    }


    // public function CreateDonateNotification_get(){
    //     require("public/assets/socket/socket.io.php");
    //     $date = new DateTime();
    //     $date = $date->format('Y-m-d H:i:s');
    //     // array create notification info
    //     $dataNotification = array(
    //         'user_id' => $data['project']->user_id,
    //         'title' => 'Public Project '.$data['project']->id,
    //         'message' => 'Admin has public your project '.$data['project']->id.' of you',
    //         'link'=> "".app_url()."project-detail/".$data['project']->id."",
    //         'status' => 0,
    //         'created' => $date
    //     );
    //     // create notification info
    //     $createNotification = $this->notification_info->insert($dataNotification);
    //     // check exists
    //     if($createNotification){
    //         $curentId = $this->ion_auth->get_users_groups()->row()->id;
    //         $currentUser = $this->users_model->getUserId($curentId);
    //         $currentImage = $currentUser->profileImageURL;
    //         // use SocketIO annouce for user
    //         $socketio = new SocketIO();
    //         // array
    //         $data = array(
    //             'room' => 'project_'.$id,
    //             'title' => 'Public Project '.$id,
    //             'message' => 'Admin has public your project '.$id.' of you',
    //             'url'=> "".app_url()."project-detail/".$id."",
    //             'ava' => $currentImage
    //         );
    //
    //         $socketio->send('socket.kakuseida-test.tk', 4000, 'public_project',json_encode($data));
    //
    //         $curl = curl_init();
    //
    //         curl_setopt_array($curl, array(
    //           CURLOPT_URL => "https://fcm.googleapis.com/fcm/send",
    //           CURLOPT_RETURNTRANSFER => true,
    //           CURLOPT_ENCODING => "",
    //           CURLOPT_MAXREDIRS => 10,
    //           CURLOPT_TIMEOUT => 30,
    //           CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    //           CURLOPT_CUSTOMREQUEST => "POST",
    //           CURLOPT_POSTFIELDS => "{   \"notification\": {\r\n        \"title\": \"Public Project".$id."\",\r\n        \"body\": \"Admin has public your project ".$id." of you\",\r\n        \"click_action\": \"".app_url()."project-detail/".$id."\",\r\n        \"icon\": \"http://url-to-an-icon/icon.png\"\r\n    },\r\n  \"to\" : \"/topics/project_".$project_id."\"\r\n}",
    //           CURLOPT_HTTPHEADER => array(
    //             "Authorization: key=AAAAwveLW4Y:APA91bEfLmYe1SMXnfffck1kuAkdXUYO6ir8_pfeBCBaIhiFYXwtlWEPQQGvgEmjDlkr2tfi2K7K_Oh0i4UhMT5ZynbLU4FnN8Ex3Qfgnbsazxxi9GBxbKzMqLm4N35R016HChA-LZtP",
    //             "Content-Type: application/json",
    //             "Postman-Token: 9909be04-d13c-4765-91e4-3d052b35d22d",
    //             "cache-control: no-cache"
    //           ),
    //         ));
    //
    //         $response = curl_exec($curl);
    //         $err = curl_error($curl);
    //
    //         curl_close($curl);
    //         // Session send message for admin
    //         $this->session->set_flashdata('msg', '更新が保存されました。');
    //     }
    //
    // }

    public function sendTokenToServer_post(){
        // get user_id flow $this->auth();
        $user = $this->user_data;
        $user_id =  $user->id;

        // Get User
        $user = $this->User->getUserId($user_id);

        // Check User is exists
        if(isset($user) && $user){
            // get token = post
            $tokenNotification = $this->post('token');

            $data = array(
                'token' => $tokenNotification
            );

            $updateTokenNotification = $this->User->editUser($user_id,$data);

            if($updateTokenNotification){
                $success = array(
                    'status' => 'OK'
                );
                $this->response($success,200);
            }else{
                $error = array(
                    'status' => 'NOT OK'
                );
                $this->response($error,400);
            }
        }else{
            $error = array(
                'status' => 'NOT Found'
            );
            $this->response($error,404);
        }
    }
    //  Cronjob Project Enough Expired then annouce for owner This project enough Expired
    public function projectEnoughExpired_get(){
        // Get date
        $date = new DateTime();
        // format date
        $date = $date->format('Y-m-d H:i:s');
        // explode $date
        $parts = explode('-', $date);
        // add 5 day for check project enough expired
        $datePlusFive = date(
            'Y-m-d H:i:s',
            mktime(0, 0, 0, $parts[1], $parts[2] + 5, $parts[0])
        );
        // get projects enough expired
        $near_expired = $this->Project->getProjectEnoughExpired($date,$datePlusFive);

        $this->response($near_expired, 200);

    }

    /**
     * Create Curl CallBack Firebase
     */
    public function Curl_post() {
        // get key firebase
        $key_firebase = $this->config->item('key_firebase');
        // get user by auth
        $user = $this->user_data;
        // get user_id
        $user_id =  $user->id;
        // get data curent User
        $curUser =   $this->User->getUserId($user_id);
        // get token firebase of user
        $tokenUser = $curUser->token;
        // get param & id
        $param = $this->post('action');
        $projectId = $this->post('project_id');
        // check param
        if(isset($param) && $param) {
            // check param = create
            if($param === 'create'){
                $link = "https://iid.googleapis.com/iid/v1/".$tokenUser."/rel/topics/project_".$projectId;
                $body = "";
            // check param = sendProjectDoante
            }else if($param === 'sendProjectComment') {
                $comment = $this->post('comment');

                $userName = $curUser->username;
                $imageUser = $curUser->profileImageURL;
                $project = $this->Project->ProjectId($projectId);
                $projectName = $project->project_name;
                $link = "https://fcm.googleapis.com/fcm/send";
                $body =  "{   \r\n\t\"notification\": {\r\n        \"title\": \"$userName ～さんは「".substr($projectName,0,30)."」にコメントしました。\",\r\n        \"body\": \" ".substr($comment,0,30)." \",\r\n        \"click_action\": \" ".app_url()."project-detail/".$projectId." \",\r\n        \"icon\": \"".base_url()."images/2018/default/logo.png\"\r\n    },\r\n\t\"to\" : \"/topics/project_$projectId\"\r\n}";

            }else if($param === 'sendProjectReply') {

                $comment_id = $this->post('comment_id');
                $comment = $this->post('comment');

                $commentProject = $this->Comment_model->getCommentId($comment_id);

                $userName = $curUser->username;
                $imageUser = $curUser->profileImageURL;
                $project = $this->Project->ProjectId($projectId);
                $projectName = $project->project_name;

                $userComment = $this->User->getUserId($commentProject->user_id);
                if(isset($userComment->token) && $userComment->token){
                    $tokenUser = $userComment->token;
                }else{
                    $tokenUser = '';
                }


                if($project->user_id === $user_id){
                    if($commentProject->user_id !== $user_id){

                        $link = "https://fcm.googleapis.com/fcm/send";
                        $body =  "{   \r\n\t\"notification\": {\r\n        \"title\": \"$userName ～さんは「".substr($projectName,0,30)."」であなたのコメントに返信しました。\",\r\n        \"body\": \" ".substr($comment,0,30)." \",\r\n        \"click_action\": \" ".app_url()."project-detail/".$projectId." \",\r\n        \"icon\": \"".base_url()."images/2018/default/logo.png\"\r\n    },\r\n\t\"to\" : \"$tokenUser\"\r\n}";
                    }else{
                        $link = '';
                        $body  = '';
                    }
                }else{
                    if($commentProject->user_id !== $user_id){

                        $curl = curl_init();

                        curl_setopt_array($curl, array(
                          CURLOPT_URL => "https://fcm.googleapis.com/fcm/send",
                          CURLOPT_RETURNTRANSFER => true,
                          CURLOPT_ENCODING => "",
                          CURLOPT_MAXREDIRS => 10,
                          CURLOPT_TIMEOUT => 30,
                          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                          CURLOPT_CUSTOMREQUEST => "POST",
                          CURLOPT_POSTFIELDS => "{   \r\n\t\"notification\": {\r\n        \"title\": \"$userName ～さんは「".substr($projectName,0,30)."」であなたのコメントに返信しました。\",\r\n        \"body\": \" ".substr($comment,0,30)." \",\r\n        \"click_action\": \" ".app_url()."project-detail/".$projectId." \",\r\n        \"icon\": \"".base_url()."images/2018/default/logo.png\"\r\n    },\r\n\t\"to\" : \"$tokenUser\"\r\n}",
                          CURLOPT_HTTPHEADER => array(
                            "Authorization: key=".$key_firebase,
                            "Content-Type: application/json",
                          ),
                        ));

                        $response = curl_exec($curl);
                        $err = curl_error($curl);

                        curl_close($curl);
                    }
                    $link = "https://fcm.googleapis.com/fcm/send";
                    $body =  "{   \r\n\t\"notification\": {\r\n        \"title\": \"$userName ～さんは「".substr($projectName,0,30)."」にコメントしました。\",\r\n        \"body\": \" ".substr($comment,0,30)." \",\r\n        \"click_action\": \" ".app_url()."project-detail/".$projectId." \",\r\n        \"icon\": \"".base_url()."images/2018/default/logo.png\"\r\n    },\r\n\t\"to\" : \"/topics/project_$projectId\"\r\n}";
                }

            }else {
                $link = '';
                $body  = '';
            }
        } else {
            $link = '';
            $body  = '';
        }

        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => $link,
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS => $body,
          CURLOPT_HTTPHEADER => array(
            "Authorization: key=".$key_firebase,
            "Content-Type: application/json",
          ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);
        $success = array(
            'status' => 'Success!'
        );
        $this->response($success,200);
    }

}
