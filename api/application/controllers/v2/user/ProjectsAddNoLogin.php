<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class ProjectsAddNoLogin extends BD_Controller {
    function __construct()
    {
        parent::__construct();

    }
    public function auth()
    {
        $date = new DateTime();
        $this->load->library(array('ion_auth', 'form_validation'));
        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        $this->methods['users_get']['limit'] = 500; // 500 requests per hour per user/key
        $this->methods['users_post']['limit'] = 100; // 100 requests per hour per user/key
        $this->methods['users_delete']['limit'] = 50; // 50 requests per hour per user/key

        //JWT Auth middleware
        $headers = $this->input->get_request_header('authorization');
        $kunci = $this->config->item('thekey'); //secret key for encode and decode
        $token= "token";

        if (!empty($headers)) {
            if (preg_match('/Bearer\s(\S+)/', $headers , $matches)) {
                $token = $matches[1];
            }
        }else{
            return false;
        }
        try {
            JWT::$leeway = 6000;
            $date = new DateTime();
            $decoded = JWT::decode($token, $kunci, array('HS256'));
            $decoded->iat = $date->getTimestamp();
            $decoded->exp = $date->getTimestamp() + 60*60*24*30*3;
            $tokenRefest = JWT::encode($decoded, $kunci);
            $decoded->tokenRefest = $tokenRefest;
            $this->user_data = $decoded;
            return true;
        } catch (Exception $e) {
            $invalid = ['status' => $e->getMessage()]; //Respon if credential invalid
           return false;
        }
        return false;
    }
    public function getTopTop9_get() {
        $user_id = $this->getPara('user_id');
        $accessProList = $this->AccessList->get3RecentAccess($user_id);
        $PercentList = $this->AccessList->get3OneLastPercentProject();;
        $proTwo3Last = $this->AccessList->getLimit3LastProject();

        /**TEST debug*/
        // init $accessProList 1 row,$PercentList = 1 row,$proTwo3Last = 3 row
        //case 1
        //case 2
         // $PercentList = [];
        //case 3
         //$PercentList = [];$accessProList =[];
        //case 4
        // $accessProList =[];
        //case 5
        //$accessProList =[];$PercentList[] = $PercentList[0];
        //case 6
        //$proTwo3Last = $accessProList =[];$PercentList[] = $PercentList[0];$PercentList[] = $PercentList[0];
        //case 7
       // $proTwo3Last =$PercentList =[]; $accessProList[] = $accessProList[0];$accessProList[] = $accessProList[0];
        //case 8
        // $proTwo3Last = [];$PercentList[] = $PercentList[0];
        //case 9
        //$proTwo3Last = [];$accessProList[] = $accessProList[0];$accessProList[] = $accessProList[0];

        $accessCount = count($accessProList);
        $percentListCount = count($PercentList);
        $proTwo3LastCount = count($proTwo3Last);

        //case 1: 1 2 3 $accessProList         $PercentList         $proTwo3Last =>OK
        if (($accessCount>0) && ($percentListCount >0 )&&  ($proTwo3LastCount >0)){
            $arr[] = $accessProList[0];
            $arr[] = $PercentList[0];
            $arr[] = $proTwo3Last[0];
            $arr = $this->addEmptyProjects($arr);
            $resutlStatus = array(
                'status' => '0',
                'caseReturn' => '1',
                'data' => $arr
            );
            $this->response($resutlStatus,200);
        }

        //case 2: 1 3 3  $accessProList         $PercentList         $proTwo3Last
        if (($accessCount>0) && ($percentListCount == 0 )&&  ($proTwo3LastCount >0)){
            $arr[] = $accessProList[0];
            $arr[] = $proTwo3Last[0];
            if($proTwo3LastCount>1){
                $arr[] = $proTwo3Last[1];
            }
            $arr = $this->addEmptyProjects($arr);
            $resutlStatus = array(
                'status' => '0',
                'caseReturn' => '2',
                'data' => $arr
            );
            $this->response($resutlStatus,200);
        }

        //case 3 : 3 3 3  $accessProList         $PercentList         $proTwo3Last  ==>ok
        if (($accessCount==0) && ($percentListCount == 0 )&&  ($proTwo3LastCount >0)){
            $arr[] = $proTwo3Last[0];

            if($proTwo3LastCount>1){
                $arr[] = $proTwo3Last[1];
            }
            if($proTwo3LastCount>2){
                $arr[] = $proTwo3Last[2];
            }
            $arr = $this->addEmptyProjects($arr);
            $resutlStatus = array(
                'status' => '0',
                'caseReturn' => '3',
                'data' => $arr
            );
            $this->response($resutlStatus,200);
        }

        //case 4 : 2 3 3 $accessProList         $PercentList         $proTwo3Last  `goal_amount` IN ('100000') ==>ok
        if (($accessCount==0) && ($percentListCount ==1  )&&  ($proTwo3LastCount >0)){
            $arr[] = $PercentList[0];

            if($proTwo3LastCount>0){
                $arr[] = $proTwo3Last[0];
            }
            if($proTwo3LastCount>1){
                $arr[] = $proTwo3Last[1];
            }
            $arr = $this->addEmptyProjects($arr);
            $resutlStatus = array(
                'status' => '0',
                'caseReturn' => '4',
                'data' => $arr
            );
            $this->response($resutlStatus,200);
        }

        //case 5 : 2 2 3 $accessProList         $PercentList         $proTwo3Last ==>ok
        if (($accessCount==0) && ($percentListCount >  1) &&  ($proTwo3LastCount >0)){
            $arr[] = $PercentList[0];
            if($percentListCount>1){
                $arr[] = $PercentList[1];
            }
            if($proTwo3LastCount>0){
                $arr[] = $proTwo3Last[0];
            }
            $arr = $this->addEmptyProjects($arr);
            $resutlStatus = array(
                'status' => '0',
                'caseReturn' => '5',
                'data' => $arr
            );
            $this->response($resutlStatus,200);
        }

        //case 6 : 2 2 2 $accessProList         $PercentList         $proTwo3Last ==>ok
        if (($accessCount==0) && ($percentListCount >  0) &&  ($proTwo3LastCount ==0)){
            $arr[] = $PercentList[0];
            if($percentListCount>1){
                $arr[] = $PercentList[1];
            }
            if($percentListCount>2){
                $arr[] = $PercentList[2];
            }
            $arr = $this->addEmptyProjects($arr);
            $resutlStatus = array(
                'status' => '0',
                'caseReturn' => '6',
                'data' => $arr
            );
            $this->response($resutlStatus,200);
        }

        //case 7 : 1 1 1 $accessProList         $PercentList         $proTwo3Last ==>ok
        if (($accessCount> 0) && ($percentListCount ==  0) &&  ($proTwo3LastCount ==0)){
            $arr[] = $accessProList[0];
            if($accessCount>1){
                $arr[] = $accessProList[1];
            }
            if($accessCount>2){
                $arr[] = $accessProList[2];
            }
            $arr = $this->addEmptyProjects($arr);
            $resutlStatus = array(
                'status' => '0',
                'caseReturn' => '7',
                'data' => $arr
            );
            $this->response($resutlStatus,200);
        }

        //case 8 : 1 2 2 $accessProList         $PercentList         $proTwo3Last ==>ok
        if (($accessCount==1 ) && ($percentListCount >  0) &&  ($proTwo3LastCount ==0)){
            $arr[] = $accessProList[0];

            if($percentListCount>0){
                $arr[] = $PercentList[0];
            }
            if($percentListCount>1){
                $arr[] = $PercentList[1];
            }
            $arr = $this->addEmptyProjects($arr);
            $resutlStatus = array(
                'status' => '0',
                'caseReturn' => '8',
                'data' => $arr
            );
            $this->response($resutlStatus,200);
        }

        //case 9 : 1 1 2 $accessProList         $PercentList         $proTwo3Last ==>ok
        if (($accessCount>1) &&  ($proTwo3LastCount ==0) && ($percentListCount>0)){
            $arr[] = $accessProList[0];

            if($accessCount>1){
                $arr[] = $accessProList[1];
            }
            if($percentListCount>0){
                $arr[] = $PercentList[0];
            }
            $arr = $this->addEmptyProjects($arr);
            $resutlStatus = array(
                'status' => '0',
                'caseReturn' => '9',
                'data' => $arr
            );
            $this->response($resutlStatus,200);
        }



    }
    private function addEmptyProjects($projectReturns) {
        $myObjEmp = array("project_name"=>"null","id"=>"0","thumnail"=>"");



        if(count($projectReturns) ==0){
            $projectReturns [] = $myObjEmp;
            $projectReturns [] = $myObjEmp;
            $projectReturns [] = $myObjEmp;
            return $projectReturns;
        }
        if(count($projectReturns) ==1){
            $projectReturns [] = $myObjEmp;
            $projectReturns [] = $myObjEmp;
            return $projectReturns;
        }
        if(count($projectReturns) ==2){
            $projectReturns [] = $myObjEmp;
            return $projectReturns;
        }
        return $projectReturns;

    }
    public function getTopTop_get() {
        $user_id = $this->getPara('user_id');
        $onePro = $this->AccessList->getTopTop($user_id);
        $proTwoPercent = $this->AccessList->getOneLastPercentProject();;
        $proTwo3Last = $this->AccessList->getLimit3LastProject();
        $myObjEmp = (object)array("project_name"=>"null","id"=>"0","thumnail"=>"");
        $isFromAccess = 1;
        if($proTwo3Last == null){
            $proTwo3Last[] = $myObjEmp ;
            $proTwo3Last[] = $myObjEmp ;
            $proTwo3Last[] = $myObjEmp ;
        }

        if($onePro == null){

            if(count($proTwo3Last)>1){
                $onePro = $proTwo3Last[1];
                $onePro['isFromAccess'] = 0;
                $isFromAccess = 0;
            }else{
                $onePro = $myObjEmp ;
            }

        }else{
            $onePro->isFromAccess = 1;
            $isFromAccess = 1;
        }
        if($proTwoPercent == null){
            if(count($proTwo3Last)>=1){
                if($isFromAccess == 1){
                    $proTwoPercent = $proTwo3Last[1];
                    $proTwoPercent['isPercent'] =  0;
                }else{

                    if(count($proTwo3Last)>2){
                        $proTwoPercent = $proTwo3Last[2];
                        $proTwoPercent['isPercent'] =  0;
                    }else{
                        $proTwoPercent = $myObjEmp ;
                    }
                }

            }else{
                $proTwoPercent = $myObjEmp ;
            }

        }else{
            $proTwoPercent->isPercent = 1;
        }
        $proThree = $proTwo3Last[0];

        $arr[] = $onePro;
        $arr[] = $proTwoPercent;
        $arr[] = $proThree;
        $resutlStatus = array(
            'status' => '0',
            'data' => $arr
        );
        $this->response($arr,200);
    }
    public function addAccess_post() {

        $user_id = $this->getPara('user_id');
        $project_id = $this->getPara('project_id');
        $project = $this->Project->getProjectId($project_id);
        if($project==null){
            $resutlStatus = array(
                'status' => '0',
                'message' => 'Project is not exist'
            );
            $this->response($resutlStatus,200);
            return;
        }

        $checkAddWishList = $this->AccessList->deleteProjectUser($user_id,$project_id);
        $datetime = new DateTime();
        // format date
        $datetime = $datetime->format('Y-m-d H:i:s');
        // change time current -> time UTC
        $asia_timestamp = strtotime($datetime);
        date_default_timezone_set('UTC');
        $date = date("Y-m-d H:i:s", $asia_timestamp);

        $dataAcessList = array(
            'user_id' => $user_id,
            'project_id' => $project_id,
            'created' => $date
        );

        $createWishList = $this->AccessList->create($dataAcessList);
        $resutlStatus = array(
            'status' => '1',
            'message' => 'Ok'
        );
        $this->response($resutlStatus,200);
//            $error = array(
//                'status' => 'error'
//            );
//            $this->response($error,404);


    }

    public function getReportTop_get(){

        $topThree = $this->ReportDailyDb->getTopThree();
        foreach ($topThree as &$item){

            $item->updated = DateTime::createFromFormat('Y-m-d H:i:s',$item->updated)->format('Y/m/d');
        }
        $this->response($topThree,200);
    }
}
