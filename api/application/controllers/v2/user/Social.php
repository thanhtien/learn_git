<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Social extends UserController {
    function __construct() {
        // Construct the parent class
        parent::__construct();
        $this->auth();
    }

    public function loginFacebook_post() {
        $user = $this->user_data;
        $user_id =  $user->id;
        $app_id = $this->config->item('appIdFacebook');
        $app_secret = $this->config->item('appSecretFacebook');
        $accessToken = $this->post('accessToken');

        require FCPATH . 'vendor/autoload.php';


        $curUser = $this->User->getUserId($user_id);

        if(isset($curUser) && $curUser) {
            // require_once 'C:/wamp/www/facebook/vendor/autoload.php'; // change path as needed
            $fb = new \Facebook\Facebook([
                'app_id' => $app_id,
                'app_secret' => $app_secret,
                'default_graph_version' => 'v2.10',
            ]);
            try {
                $response = $fb->get('/me?fields=id,name,gender,email,picture.width(240).height(240),cover', $accessToken);
            } catch (\Facebook\Exceptions\FacebookResponseException $e) {
                // When Graph returns an error
                $this->response($e->getMessage(), 400);
            } catch (\Facebook\Exceptions\FacebookSDKException $e) {
                // When validation fails or other local issues
                $this->response($e->getMessage(), 400);
            }

            $infoFacebook = $response->getGraphNode();
            $idFacebook = $infoFacebook['id'];
            $nameFacebook = replaceName($userNameFacebook);

            $checkFacebookId = $this->User->checkIdFacebook($idFacebook);

            if(isset($checkFacebookId) && $checkFacebookId){
                $error = array(
                    'status' => ' this account social is exists!'
                );
                $this->response($error,400);
            }else {
                $dataUser = array(
                    'facebook_id' => $idFacebook,
                );
                $updateUser = $this->ion_auth->update($user_id,$dataUser);
                if($updateUser) {
                    $infoUser = $this->profileUser_get($user_id);
                    $this->response($infoUser,200);
                }else {
                    $error = array(
                        'status' => 'update social fail!'
                    );
                    $this->response($error,400);
                }
            }

        }else {
            $error = array(
                'status' => 'This user not exists!'
            );
            $this->response($error,404);
        }
    }


    public function loginTwitter_post()
    {
        $consumerKey  = $this->config->item('consumerKey');
        $consumerSecret = $this->config->item('consumerSecret');

        require FCPATH . 'vendor/autoload.php';
        $connection = new TwitterOAuth($consumerKey, $consumerSecret);
        $request_token = $connection->oauth("oauth/request_token", array('oauth_callback','http%3A%2F%2Flocalhost%3A3000%2Ftwitter-callback'));
        $this->response($request_token, 200);
    }

    public function loginNewTwitter_post()
    {
        $user = $this->user_data;
        $user_id =  $user->id;

        $curUser = $this->User->getUserId($user_id);
        if(isset($curUser) && $curUser) {
            $consumerKey  = $this->config->item('consumerKey');
            $consumerSecret = $this->config->item('consumerSecret');
            require FCPATH . 'vendor/autoload.php';

            $connection = new TwitterOAuth($consumerKey, $consumerSecret);
            $request_token = $connection->oauth("oauth/request_token", array('oauth_callback','http%3A%2F%2Flocalhost%3A3000%2Ftwitter-callback'));

            $oauth_token = $this->post('oauth_token');
            $oauth_verifier = $this->post('oauth_verifier');

            $newConnection = new TwitterOAuth($consumerKey, $consumerSecret, $oauth_token, '12312312');
            $access_token = $newConnection->oauth("oauth/access_token", ["oauth_verifier" => $oauth_verifier]);


            $connection = new TwitterOAuth($consumerKey, $consumerSecret, $access_token['oauth_token'], $access_token['oauth_token_secret']);
            $user = $connection->get("account/verify_credentials");

            $idTwitter = $access_token['user_id'];
            $userNameTwitter = $access_token['screen_name'];
            $nameTwitter = replaceName($userNameTwitter);

            $checkTwitterId = $this->User->checkIdTwitter($idTwitter);

            if(isset($checkTwitterId) && $checkTwitterId){
                $error = array(
                    'status' => ' this account social is exists!'
                );
                $this->response($error,400);
            }else {
                $dataUser = array(
                    'twitter_id' => $idTwitter,
                );
                $updateUser = $this->ion_auth->update($user_id,$dataUser);
                if($updateUser) {
                    $infoUser = $this->profileUser_get($user_id);
                    $this->response($infoUser,200);
                }else {
                    $error = array(
                        'status' => 'update social fail!'
                    );
                    $this->response($error,400);
                }
            }

        }else {
            $error = array(
                'status' => 'This user not exists!'
            );
            $this->response($error,404);
        }

    }


}
