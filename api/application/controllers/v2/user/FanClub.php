<?php

defined('BASEPATH') OR exit('No direct script access allowed');
use \Firebase\JWT\JWT;
class FanClub extends UserController {
    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->auth();
    }

    public function checkNumberStripe_post($numberStripe,$cvc){
        $kunci = $this->config->item('thekey');

        $user = $this->user_data;
        $user_id =  $user->id;

        $count_length = strlen($numberStripe);
        $count_length = $count_length - 4;

        $last4 = substr($numberStripe,$count_length);

        $getBlackList = $this->BlackList->getCard($last4);
        // var_dump($getBlackList);
        $status = 0;
        if(isset($getBlackList) && $getBlackList) {

            foreach ($getBlackList as $key => $value) {

                $token = $value->hash_token;

                try {
                    $decoded = JWT::decode($token, $kunci, array('HS256'));
                    $result = strpos($decoded, "/") + 1;
                    $checkcvc = substr($decoded, $result);

                    if((int)$cvc !== (int)$checkcvc){
                        $status = $status + 0;
                    }else{
                        $status = $status + 1;
                    }
                } catch (Exception $e) {
                    $status = $status + 0;
                }
            }

            if($status > 0){
                return false;
            }else {
                return true;
            }
        }else {
            return true;
        }

    }

    /**
     * [StopFanClub_post Stop fanclub with title and message + date_close]
     */
    public function StopFanClub_post() {
        // get user
        $user = $this->user_data;
        $user_id =  $user->id;
        // get email admin
        $email_admin = $this->config->item('email_admin');
        $curUser = $this->User->getUserId($user_id);
        // get info fanclub
        $idFanClub = $this->post('id');
        $title = $this->post('title_fanclub');
        $message = $this->post('message_fanclub');
        // fanClub
        $FanClub = $this->Project->getProjectId($idFanClub);
        if((int)$FanClub->user_id  !== (int)$user_id) {
            $error = array(
                'status' => '本ファンクラブを解散する権限がありません'
            );
            $this->response($error,404);
        }
        // check fanclub
        if(isset($FanClub) && $FanClub->project_type === '1'){
            // start day
            $startDay = new DateTime($FanClub->collection_start_date);
            $date = $startDay->format('Y-m-01');
            // current day
            $curentDay = new DateTime();
            $date2 = $curentDay->format('Y-m-01');

            $ts1 = strtotime($date);
            $ts2 = strtotime($date2);

            $year1 = date('Y', $ts1);
            $year2 = date('Y', $ts2);

            $month1 = date('m', $ts1);
            $month2 = date('m', $ts2);

            $diff = (($year2 - $year1) * 12) + ($month2 - $month1);

            // create month = ?month / month of project
            if($diff >= (int)$FanClub->number_month){
                $newmonth = ($diff/$FanClub->number_month);
                $checkMonth = ceil($diff/$FanClub->number_month);
                if($newmonth === (int)$checkMonth){
                    $newmonth = $checkMonth;
                }else {
                    $newmonth = $checkMonth - 1;
                }
            }else {
                $newmonth = 0;
            }

            if((int)$newmonth === (int)0) {
                $error = array(
                    'status' => "解散は解散をユーザーに通知してから一ヵ月以上経過してからでないとできません"
                );
                $this->response($error,400);
            }
            $newCurrent = $curentDay->format('Y-m-d');
            if($FanClub->status_fanclub === 'progress'){
                $dataFan = array(
                    'status_fanclub' => 'wait',
                    'title_fanclub'  => $title,
                    'message_fanclub'  => $message,
                    'modified' => $newCurrent
                );
                $updateFan = $this->Project->update($idFanClub,$dataFan);
                if($updateFan) {
                    $this->load->library('email');

                    $this->email->from('admin@kakuseida.conoha.io', 'KAKUSEIDA');
                    $this->email->to($email_admin);
                    // $this->email->reply_to('admin@kakuseida.conoha.io', 'KAKUSEIDA');
                    $this->email->subject($curUser->username.'さんから【'.$FanClub->project_name.'】を解散するリクエストが送付されました');
                    $this->email->message(
                        $curUser->username.'さんから【'.$FanClub->project_name.'】を解散するリクエストが送付されました<br>
                        解散したい理由：<br>
                        '.$message.'<br>
                        以下のURLにアクセスして、確認してください。<br>
                        <a href="'.admin_url().'project/edit/'.$idFanClub.'?tab=4">'.admin_url().'project/edit/'.$idFanClub.'?tab=4</a>'
                    );

                    $this->email->send();
                    $success = array(
                        'status' => 'ファンクラブ解散の申請が送付されました'
                    );
                    $this->response($success,200);
                }else{
                    $error = array(
                        'status' => 'エラーです。ファンクラブの解散の申請が送付できませんでした。'
                    );
                    $this->response($error,400);
                }
            }else {
                $error = array(
                    'status' => '本ファンクラブは現在解散の承認待ちです'
                );
                $this->response($error,404);
            }
        }else {
            $error = array(
                'status' => 'Not Found Id!'
            );
            $this->response($error,404);
        }
    }

    public function ListDonateFanClub_get(){
        $start = $this->get('start');
        $limit = '10';
        if(!$start || $start =='0'){
            $start ='0';
        }else{
            $start = $start*$limit;
        }
        $user = $this->user_data;

        $user_id =  $user->id;
        $curUser =   $this->User->getUserId($user_id);
        $input['limit']  = array($limit,$start);

        $fanclub = $this->ListDonateFanClub->getAllDonateFanClub($user_id,$input);

        $totalNews = $fanclub['total'];
        $totalPages = ceil($totalNews / $limit);

        if(isset($fanclub['data']) && $fanclub['data']){
            foreach ($fanclub['data'] as $key => $value) {
                $backingLevel = $this->BackingLevel->getLevelId($value->backing_levels_id);
                $value->now_count = $backingLevel->now_count;
                $value->return_amount = $backingLevel->return_amount;
                $value->invest_amount = $backingLevel->invest_amount;
            }
        }
        if($curUser){
            $data = array(
                'page_count'=>$totalPages,
                'data'=>$fanclub['data']
            );
            $this->response($data, 200);
        }else{
            $error = array(
                'status' => 'not found!'
            );
            $this->response($error, 404);
        }
    }

    public function stopDonateFanClub_get() {
        // get user
        $user = $this->user_data;
        $user_id =  $user->id;

        $project_id = $this->get('project_id');
        $backing_levels_id = $this->get('backing_levels_id');

        $fanClub = $this->ListDonateFanClub->getInfoFanClubDonate($user_id,$project_id,$backing_levels_id);
        if(isset($fanClub) && $fanClub) {
            if($fanClub->status_join_fanclub === '0'){
                //include Stripe PHP library
                require_once APPPATH."third_party/stripe/init.php";

                //set api key
                $key = $this->Setting->getSetting();
                $stripe = array(
                    "secret_key"      => $key->secret_key,
                    "publishable_key" => $key->public_key
                );


                \Stripe\Stripe::setApiKey($stripe['secret_key']);
                $subscription = \Stripe\Subscription::retrieve($fanClub->sub_fanclub_id);
                if($subscription['status'] === 'canceled') {
                    $project = $this->Project->getProjectId($project_id);
                    if(isset($project) && $project){
                        $error = array(
                            'status' => '「'.$project->project_name.'」から退会しました。'
                        );
                        $this->response($error,400);
                    }else{
                        $error = array(
                            'status' => 'このプロジェクトは存在しません。'
                        );
                        $this->response($error,404);
                    }

                }
                $cancelDonate = $subscription->cancel();

                if(isset($cancelDonate) && $cancelDonate['status'] === "canceled") {

                    $data = array(
                        'status_join_fanclub' => '1'
                    );
                    $updateFanClub = $this->ListDonateFanClub->update($fanClub->id,$data);
                    if($updateFanClub) {
                        $project = $this->Project->getProjectId($project_id);
                        $backing_level = $this->BackingLevel->getLevelId($backing_levels_id);
                        $backed = $this->BackedProject->BackedDonate($user_id,$project_id,$backing_levels_id);

                        $dataBackingLevel = array(
                            'now_count' => $backing_level->now_count - 1
                        );
                        $updateBacking = $this->BackingLevel->update($backing_level->id,$dataBackingLevel);
                        $dataProject = array(
                            'collected_amount' => $project->collected_amount - $backing_level->invest_amount,
                            'now_count' => $project->now_count - 1
                        );
                        $updateProject = $this->Project->update($project->id,$dataProject);

                        $delBacked = $this->BackedProject->delete($backed->id);

                        if($updateBacking && $updateProject && $delBacked) {
                            $success = array(
                                'id' => $fanClub->id
                            );
                            $this->response($success,200);
                        }else {
                            $error = array(
                                'status' => 'エラー！まだ退会できません。'
                            );
                            $this->response($error,400);
                        }
                    } else {
                        $error = array(
                            'status' => 'エラー！まだ退会できません。'
                        );
                        $this->response($error,400);
                    }
                }else {
                    $error = array(
                        'status' => '「'.$project->project_name.'」から退会しました。'
                    );
                    $this->response($error,400);
                }
            } else {
                $error = array(
                    'status' => '「'.$project->project_name.'」から退会しました。'
                );
                $this->response($error,400);
            }
        }else{
            $error = array(
                'status' => 'Not Found!'
            );
            $this->response($error,404);
        }
    }


    public function DonateFanClub_post(){
        $roomSocket = $this->config->item('roomSocket');
        $portRoomSocket = $this->config->item('portRoomSocket');
        $key_firebase = $this->config->item('key_firebase');

        $user = $this->user_data;
        $user_id =  $user->id;
        $curUser =   $this->User->getUserId($user_id);
        $email = $curUser->email;
        $_POST = $this->post();
        //check whether stripe token is not empty
        //get token, card and user info from the form
        $token = $this->post('stripeToken');
        $project_id = $_POST['project_id'];
        $backing_level_id = $_POST['backing_level_id'];
        $cvc = $_POST['cvc'];
        // $comment = '”'.$curUser->username.'"は"'.$projectName.'('.$project_id.')"の"'.$backing_level_id.'"に支援しました。';
        $comment =  'comment';
        // $invest_amount = $_POST['invest_amount'];


        if(isset($project_id) && isset($backing_level_id)){
            $date = new DateTime();
            $date = $date->format('Y-m-d H:i:s');

            $existproject = $this->Project->checkProjectId($project_id);

            $existprojectretrun = $this->BackingLevel->getBackingId($backing_level_id);
            if(isset($existproject) && $existproject) {

                $number_month = $existproject->number_month;
                $timestart = $existproject->collection_start_date;
                $project_type = $existproject->project_type;
                $userOwner = $this->User->getUserId($existproject->user_id);
                $nameOwner = $userOwner->username;
                $emailOwner = $userOwner->email;

                $date1 = new DateTime($existproject->collection_start_date);
                $date2 = new DateTime(date('Y-m-d'));
                $time = $date1->diff($date2);

                $date = date_diff($date1,$date2);
                $date = $date->format("%a");
                $number_day = $existproject->number_month*30;

                if((int)$date > (int)$number_day) {
                    $month = ($date/$number_day);
                    $checkMonth = ceil($date/$number_day);
                    if($month === (int)$checkMonth) {
                        $month = $checkMonth;
                    } else {
                        $month = $checkMonth - 1;
                    }
                }else{
                    $month = 0;
                }

                if((int)$month === (int)0) {
                    $dayDonate =$number_day;
                }else{
                    $dayDonate =$number_day*($month + 1);
                }

                $date1 = $date1->format('Y-m-d');

                $parts = explode('-', $date1);

                $dateAnnouce = date(
                    'Y-m-d',
                    mktime(0, 0, 0, $parts[1], $parts[2] + ($dayDonate), $parts[0])
                );

                $currentDay = $date2->format('Y-m-d');


                if( $existproject->project_type === '1' && $existproject->status_fanclub === 'stop'){
                        $error = array(
                            "status" => "error!"
                        );
                        $this->response($error,500);
                }

                if($user_id === $existproject->user_id){
                    $error = array(
                        "status" => "error!"
                    );
                    $this->response($error,500);
                }

                if(isset($existprojectretrun) && $existprojectretrun){

                    if($existprojectretrun->project_id === $existproject->id) {
                        $invest_amount = $existprojectretrun->invest_amount;
                        //include Stripe PHP library
                        require_once APPPATH."third_party/stripe/init.php";

                        //set api key
                        $key = $this->Setting->getSetting();
                        $stripe = array(
                            "secret_key"      => $key->secret_key,
                            "publishable_key" => $key->public_key
                        );

                        \Stripe\Stripe::setApiKey($stripe['secret_key']);

                        //add customer to stripe
                        if(isset($curUser->customer_id) && $curUser->customer_id && !isset($token)){
                            $customer = (object) array('id' => $curUser->customer_id);
                            if(isset($curUser->customer_id) && $curUser->customer_id){
                                $kunci = $this->config->item('thekey');
                                $token = $curUser->hash_token;

                                try {
                                    $decoded = JWT::decode($token, $kunci, array('HS256'));

                                    $result = strpos($decoded, "/") + 1;
                                    $checkcvc = substr($decoded, $result);

                                    if((int)$cvc !== (int)$checkcvc){
                                        $error = array(
                                            "status" => "セキュリティーコード（CVC)は間違っていますので、ご確認下さい。"
                                        );
                                        $this->response($error,400);
                                    } else {
                                        $checkBlackList = $this->checkNumberStripe_post($curUser->last4,$cvc);
                                        if(!$checkBlackList){
                                            $error = array(
                                                "status" => "card nay da bi khoa, vui long su dung card khac di tk cho nay li vai lzzzzzz!"
                                            );
                                            $this->response($error,400);
                                        }

                                    }
                                } catch (Exception $e) {
                                    $invalid = ['status' => $e->getMessage()];
                                    $this->response($invalid, 400);
                                }
                            }
                        }else if(isset($curUser->customer_id) &&
                        $curUser->customer_id && isset($token)
                         || !isset($curUser->customer_id) && isset($token)) {
                            try {
                                $customer = \Stripe\Customer::create(array(
                                    'email' => $email,
                                    'source'  => $token
                                ));
                            } catch(Stripe_CardError $e) {
                                $error1 = $e->getMessage();
                                $error = array(
                                    'status'=>$error1
                                );
                                $this->response($error,400);
                            } catch (Stripe_InvalidRequestError $e) {
                                // Invalid parameters were supplied to Stripe's API
                                $error2 = $e->getMessage();
                                $error = array(
                                    'status'=>$error2
                                );
                                $this->response($error,400);
                            } catch (Stripe_AuthenticationError $e) {
                                // Authentication with Stripe's API failed
                                $error3 = $e->getMessage();
                                $error = array(
                                    'status'=>$error3
                                );

                                $this->response($error,400);
                            } catch (Stripe_ApiConnectionError $e) {
                                // Network communication with Stripe failed
                                $error4 = $e->getMessage();
                                $error = array(
                                    'status'=>$error4
                                );
                                $this->response($error,400);
                            } catch (Stripe_Error $e) {
                                // Display a very generic error to the user, and maybe send
                                // yourself an email
                                $error5 = $e->getMessage();
                                $error = array(
                                    'status'=>$error5
                                );
                                $this->response($error,400);
                            } catch (Exception $e) {
                                // Something else happened, completely unrelated to Stripe
                                $errrr = json_decode($e->getHttpBody());
                                if($errrr->error->code === "token_already_used"){
                                    $error = array(
                                        'status'=>'このクレジットカード情報は存在しました。'
                                    );
                                }else if($errrr->error->code === "email_invalid"){
                                    $error = array(
                                        'status'=>'メールアドレスを正しく入力してください'
                                    );
                                }else if($errrr->error->code === "card_declined"){
                                    $error = array(
                                        'status'=>'クレジットカードの情報は正しくないので、確認してください'
                                    );
                                }else{
                                    $error = array(
                                        'status'=>'クレジットカードの情報は正しくないので、確認してください'
                                    );
                                }

                                $this->response($error,400);
                            }
                        } else {
                            $error = array(
                                'status' => 'クレジットカードの情報は正しくないので、確認してください'
                            );
                            $this->response($error,400);
                        }
                        //item information
                        $itemName = $project_id;
                        $itemNumber = $backing_level_id;
                        $itemPrice = $existprojectretrun->invest_amount;
                        $currency = "JPY";
                        $orderID = $project_id;

                        $checkDonate = $this->ListDonateFanClub->checkIsDonate($user_id,$project_id,$backing_level_id);
                        $startDonate = "0";
                        $editStartDonate = "0";
                        if(isset($checkDonate) && $checkDonate) {
                            if($checkDonate->status_join_fanclub === '1'){
                                $startDonate = "1";
                                $editStartDonate = "1";
                            }else{
                                $error = array(
                                    'status' => 'このリターン品を1回支援しましたので、2回目選択できません。'
                                );
                                $this->response($error,400);
                            }
                        }else{
                            $startDonate = "1";
                        }

                        if($startDonate === '1') {
                            if($currentDay === $dateAnnouce){
                                $dateAnnouce = date(
                                    'Y-m-d 20:20:20',
                                    mktime(0, 0, 0, $parts[1], $parts[2] + ($dayDonate*2), $parts[0])
                                );
                            }

                            $time = strtotime($dateAnnouce);

                            try {
                                $subJson = \Stripe\Subscription::create([
                                    "customer" => $customer->id,
                                    "items" => [
                                        [
                                            "plan" => $existprojectretrun->plan_fanclub_id,
                                            "quantity" => 1,
                                        ]
                                    ],
                                    "trial_end" => $time,
                                ]);

                                $donateNormal = \Stripe\Charge::create(array(
                                    'customer' => $customer->id,
                                    'amount'   => $itemPrice,
                                    'currency' => $currency,
                                    'description' => $itemNumber,
                                ));
                                //retrieve charge details
                                $donateNormal = $donateNormal->jsonSerialize();
                                //check whether the charge is successful

                            } catch(Stripe_CardError $e) {
                                $error1 = $e->getMessage();
                                $error = array(
                                    'status'=>$error1);
                                    $this->response($error,400);
                            } catch (Stripe_InvalidRequestError $e) {
                                // Invalid parameters were supplied to Stripe's API
                                $error2 = $e->getMessage();
                                $error = array(
                                    'status'=>$error2);
                                    $this->response($error,400);
                            } catch (Stripe_AuthenticationError $e) {
                                // Authentication with Stripe's API failed
                                $error3 = $e->getMessage();
                                $error = array(
                                    'status'=>$error3);
                                    $this->response($error,400);
                            } catch (Stripe_ApiConnectionError $e) {
                                // Network communication with Stripe failed
                                $error4 = $e->getMessage();
                                $error = array(
                                    'status'=>$error4
                                );
                                $this->response($error,400);
                            } catch (Stripe_Error $e) {
                            // Display a very generic error to the user, and maybe send
                            // yourself an email
                            $error5 = $e->getMessage();
                            $error = array(
                                'status'=>$error5);
                                $this->response($error,400);
                            } catch (Exception $e) {
                                // Something else happened, completely unrelated to Stripe
                                $error6 = $e->getMessage();
                                $error = array(
                                    'status'=>$error6);
                                $this->response($error,400);
                            }
                        }
                        $date = new DateTime();
                        $date = $date->format('Y-m-d H:i:s');
                        if(isset($subJson) && $subJson) {
                            if(($subJson->status === "trialing" && $donateNormal['status'] === "succeeded") || $subJson->status === "active") {
                                $status = "succeeded";
                            }
                            $addressDefault = $this->AddressUser->getAddressUserDefault($user_id);
                            if(!$addressDefault){
                                $error = array(
                                    'status' => 'error!'
                                );
                                $this->response($error,500);
                            }

                            if($editStartDonate === '0'){
                                $dataDB = array(
                                    'project_id' => $project_id,
                                    'user_id' => $user_id,
                                    'id_address' => $addressDefault->id,
                                    'postcode'  =>  $addressDefault->postcode,
                                    'email'  =>  $addressDefault->email,
                                    'address_return'  =>  $addressDefault->address_return,
                                    'phone'  =>  $addressDefault->phone,
                                    'name_return'  =>  $addressDefault->name_return,
                                    'backing_level_id' => $backing_level_id,
                                    'invest_amount' => $invest_amount,
                                    'stripe_charge_id' => $donateNormal['id'],
                                    'type'=>'Credit',
                                    'comment' => $comment,
                                    'status' => $status,
                                    'created' => $date,
                                    'manual_flag' => '0'
                                );
                                if($this->db->insert('backed_projects', $dataDB)){
                                    if($this->db->insert_id() && $status == 'succeeded'){

                                        $dataBackingLevel = $this->BackingLevel->getBackingId($backing_level_id);
                                        $now_count = $dataBackingLevel->now_count;
                                        $dataUpdate = array(
                                            'now_count'=>$now_count + 1,
                                        );
                                        $this->BackingLevel->update($backing_level_id,$dataUpdate);
                                        $dataUserUpdate = array(
                                            'customer_id'=>$customer->id
                                        );
                                        $projectId = $this->Project->getProjectId($project_id);
                                        $collected_amount = $projectId->collected_amount;
                                        $now_countProject = $projectId->now_count;
                                        $dataUpdateProject = array(
                                            'now_count'=>$now_countProject + 1,
                                            'collected_amount'=>$collected_amount + $itemPrice,
                                        );
                                        $updateProject = $this->Project->update($project_id,$dataUpdateProject);
                                        $updateUser = $this->User->update($user_id,$dataUserUpdate);
                                        $listUserDonate = array(
                                            'user_id'=>$user_id,
                                            'project_id'=>$project_id,
                                            'backing_levels_id' => $backing_level_id,
                                            'month' => $month,
                                            'address_return' => $addressDefault->address_return,
                                            'sub_fanclub_id'=> $subJson->id,
                                            'created'=>$date
                                        );
                                        $createList = $this->ListDonateFanClub->createListUserDonate($listUserDonate);

                                        $datetime = new DateTime();
                                        $datetime = $datetime->format('Y-m-d H:i:s');
                                        // get strtotime + change time zone
                                        $asia_timestamp = strtotime($datetime);
                                        date_default_timezone_set('UTC');
                                        // get Date vs time zone
                                        $date = date("Y-m-d H:i:s", $asia_timestamp);

                                        // get data for socket + create notification_info + firebase
                                        $userDonate = $curUser->username;
                                        $investAmountDonate = $invest_amount;
                                        $imageUserDonate = $curUser->profileImageURL;
                                        $projectId = $project_id;
                                        $projectName = $existproject->project_name;
                                        $returnName = $existprojectretrun->name;
                                        // array create notification info
                                        $dataNotification = array (
                                            'user_id' => $existproject->user_id,
                                            'user_action' => $userDonate,
                                            'title' => '支援があります',
                                            'message' => ''.$userDonate.' さんが 「'.$projectName.'」に'.number_format($investAmountDonate).'円支援しました',
                                            'link'=> "my-page/statistic/".$projectId,
                                            'status' => 0,
                                            'thumbnail' => $imageUserDonate,
                                            'created' => $date
                                        );
                                        // create notification info
                                        $createNotification = $this->Notification_info->create($dataNotification);
                                        // check exists
                                        if($createNotification) {
                                            // use SocketIO annouce for user
                                            require("public/assets/socket/socket.io.php");
                                            $socketio = new SocketIO();
                                            // // array
                                            $data = array(
                                                'room' => 'project_'.$projectId,
                                                'title' => '支援があります',
                                                'message' => $userDonate."さんが「".$projectName."」に".number_format($investAmountDonate)."円支援しました",
                                                'url'=> app_url()."my-page/statistic/".$projectId,
                                                'ava' => $imageUserDonate
                                            );
                                            $socketio->send($roomSocket, $portRoomSocket, 'donate_project',json_encode($data));

                                            $curl = curl_init();

                                            curl_setopt_array($curl, array(
                                              CURLOPT_URL => "https://fcm.googleapis.com/fcm/send",
                                              CURLOPT_RETURNTRANSFER => true,
                                              CURLOPT_ENCODING => "",
                                              CURLOPT_MAXREDIRS => 10,
                                              CURLOPT_TIMEOUT => 30,
                                              CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                                              CURLOPT_CUSTOMREQUEST => "POST",
                                              CURLOPT_POSTFIELDS => "{   \"notification\": {\r\n        \"title\": \"支援があります\",\r\n        \"body\": \"".$userDonate."さんが「".$projectName."」」に ".number_format($investAmountDonate)."円支援しました\",\r\n        \"click_action\": \"".app_url()."my-page/statistic/".$projectId."\",\r\n        \"icon\": \"".base_url()."images/2018/default/logo-icon.png\"\r\n    },\r\n  \"to\" : \"/topics/project_".$projectId."\"\r\n}",
                                              CURLOPT_HTTPHEADER => array(
                                                "Authorization: key=".$key_firebase,
                                                "Content-Type: application/json",
                                              ),
                                            ));

                                            $response = curl_exec($curl);
                                            $err = curl_error($curl);

                                            curl_close($curl);

                                            $this->load->library('email');

                                            $this->email->from('admin@kakuseida.conoha.io', 'KAKUSEIDA');
                                            $this->email->to($email);
                                            // $this->email->reply_to('admin@kakuseida.conoha.io', 'KAKUSEIDA');
                                            $this->email->subject('「'.$projectName.'」プロジェクトにご支援いただきありがとうございます。');
                                            $this->email->message(
                                            $userDonate.'様が選択されたリターン品の名前は'.$returnName.'です。<br>
                                            KAKUSEIDA運営事務局です。<br>
                                            このたびは、「'.$projectName.'」プロジェクトにご支援いただきありがとうございます。<br>
                                            '.$userDonate.'様が選択されたリターン品の名前は'.$returnName.'です。<br>
                                            リターンの詳細、送付に関しましては企画者からの連絡をお待ちください。<br>
                                            今後ともKAKUSEIDAをよろしくお願い致します。<br>');
                                            $this->email->send();


                                            $this->email->from('admin@kakuseida.conoha.io', 'KAKUSEIDA');
                                            $this->email->to($emailOwner);
                                            // $this->email->reply_to('admin@kakuseida.conoha.io', 'KAKUSEIDA');
                                            $this->email->subject($nameOwner.'様が企画した「'.$projectName.'」プロジェクトに、新規支援がありました。');
                                            $this->email->message(
                                            $nameOwner.'様が企画した「'.$projectName.'」プロジェクトに、新規支援がありました。<br>
                                            KAKUSEIDA運営事務局です。<br>
                                            '.$nameOwner.'様が企画した「'.$projectName.'」プロジェクトに、新規支援がありました。<br>
                                            選択されたリターン品の名前は'.$returnName.'です。<br>
                                            プロジェクト画面にログインして詳細を確認してください。<br>
                                            <a href="'.app_url().'project-detail/'.$project_id.'">'.app_url().'project-detail/'.$project_id.'</a><br />');
                                            $this->email->send();
                                        }
                                    }
                                }
                            }else {
                                $dataUpdateDonate = array(
                                    'sub_fanclub_id'=> $subJson->id,
                                    'status_join_fanclub' => '0',
                                );
                                $updateList = $this->ListDonateFanClub->update($checkDonate->id,$dataUpdateDonate);

                                $dataDB = array(
                                    'project_id' => $project_id,
                                    'user_id' => $user_id,
                                    'id_address' => $addressDefault->id,
                                    'postcode'  =>  $addressDefault->postcode,
                                    'email'  =>  $addressDefault->email,
                                    'address_return'  =>  $addressDefault->address_return,
                                    'phone'  =>  $addressDefault->phone,
                                    'name_return'  =>  $addressDefault->name_return,
                                    'backing_level_id' => $backing_level_id,
                                    'invest_amount' => $invest_amount,
                                    'stripe_charge_id' => $donateNormal['id'],
                                    'type'=>'Credit',
                                    'comment' => $comment,
                                    'status' => $status,
                                    'created' => $date,
                                    'manual_flag' => '0'
                                );
                                $createBacked = $this->BackedProject->create($dataDB);

                                $dataBackingLevel = $this->BackingLevel->getBackingId($backing_level_id);
                                $now_count = $dataBackingLevel->now_count;
                                $dataUpdate = array(
                                    'now_count'=>$now_count + 1,
                                );
                                $this->BackingLevel->update($backing_level_id,$dataUpdate);
                                $dataUserUpdate = array(
                                    'customer_id'=>$customer->id
                                );
                                $projectId = $this->Project->getProjectId($project_id);
                                $collected_amount = $projectId->collected_amount;
                                $now_countProject = $projectId->now_count;
                                $dataUpdateProject = array(
                                    'now_count'=>$now_countProject + 1,
                                    'collected_amount'=>$collected_amount + $itemPrice,
                                );
                                $updateProject = $this->Project->update($project_id,$dataUpdateProject);
                                $updateUser = $this->User->update($user_id,$dataUserUpdate);



                                $datetime = new DateTime();
                                $datetime = $datetime->format('Y-m-d H:i:s');
                                // get strtotime + change time zone
                                $asia_timestamp = strtotime($datetime);
                                date_default_timezone_set('UTC');
                                // get Date vs time zone
                                $date = date("Y-m-d H:i:s", $asia_timestamp);

                                // get data for socket + create notification_info + firebase
                                $userDonate = $curUser->username;
                                $investAmountDonate = $invest_amount;
                                $imageUserDonate = $curUser->profileImageURL;
                                $projectId = $project_id;
                                $projectName = $existproject->project_name;
                                // array create notification info
                                $dataNotification = array (
                                    'user_id' => $existproject->user_id,
                                    'user_action' => $userDonate,
                                    'title' => '支援があります',
                                    'message' => ''.$userDonate.' さんが 「'.$projectName.'」に'.number_format($investAmountDonate).'円支援しました',
                                    'link'=> "my-page/statistic/".$projectId,
                                    'status' => 0,
                                    'thumbnail' => $imageUserDonate,
                                    'created' => $date
                                );
                                // create notification info
                                $createNotification = $this->Notification_info->create($dataNotification);
                                // check exists
                                if($createNotification) {
                                    // use SocketIO annouce for user
                                    require("public/assets/socket/socket.io.php");
                                    $socketio = new SocketIO();
                                    // // array
                                    $data = array(
                                        'room' => 'project_'.$projectId,
                                        'title' => '支援があります',
                                        'message' => $userDonate."さんが「".$projectName."」に".number_format($investAmountDonate)."円支援しました",
                                        'url'=> app_url()."my-page/statistic/".$projectId,
                                        'ava' => $imageUserDonate
                                    );
                                    $socketio->send($roomSocket, $portRoomSocket, 'donate_project',json_encode($data));

                                    $curl = curl_init();

                                    curl_setopt_array($curl, array(
                                      CURLOPT_URL => "https://fcm.googleapis.com/fcm/send",
                                      CURLOPT_RETURNTRANSFER => true,
                                      CURLOPT_ENCODING => "",
                                      CURLOPT_MAXREDIRS => 10,
                                      CURLOPT_TIMEOUT => 30,
                                      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                                      CURLOPT_CUSTOMREQUEST => "POST",
                                      CURLOPT_POSTFIELDS => "{   \"notification\": {\r\n        \"title\": \"支援があります\",\r\n        \"body\": \"".$userDonate."さんが「".$projectName."」」に ".number_format($investAmountDonate)."円支援しました\",\r\n        \"click_action\": \"".app_url()."my-page/statistic/".$projectId."\",\r\n        \"icon\": \"".base_url()."images/2018/default/logo-icon.png\"\r\n    },\r\n  \"to\" : \"/topics/project_".$projectId."\"\r\n}",
                                      CURLOPT_HTTPHEADER => array(
                                        "Authorization: key=".$key_firebase,
                                        "Content-Type: application/json",
                                      ),
                                    ));

                                    $response = curl_exec($curl);
                                    $err = curl_error($curl);

                                    curl_close($curl);
                                }
                            }
                            $success = array(
                                'status'=>'成功！支援ができました。'
                            );
                            $this->response($success,200);

                        }else{
                            $error = array(
                                'status'=>'エラー！支援がまだできません。'
                            );
                            $this->response($error,400);
                        }
                    }else{
                        $error = array(
                            'status'=>'このリターン品は存在しません。'
                        );
                        $this->response($error,404);
                    }
                }else{
                    $error = array(
                        'status'=>'このプロジェクトは存在しません。'
                    );
                    $this->response($error,404);
                }
            }else{
                $error = array(
                    'status'=>'このユーザーは存在しません。'
                );
                $this->response($error,404);
            }
        }else{
            $error = array(
                'status'=>'このプロジェクトは存在しません。'
            );
            $this->response($error,404);
        }
    }

    public function testPayOut_get(){
        $user = $this->user_data;
        $user_id =  $user->id;
        $curUser = $this->User->getUserId($user_id);
        require_once APPPATH."third_party/stripe/init.php";

        //set api key
        $key = $this->Setting->getSetting();
        $stripe = array(
            "secret_key"      => $key->secret_key,
            "publishable_key" => $key->public_key
        );
        $customer_id =  $curUser->customer_id;
        \Stripe\Stripe::setApiKey($stripe['secret_key']);

        $customer = \Stripe\Customer::retrieve($customer_id);

        //
        // $a = \Stripe\Payout::create([
        //   "amount" => 120000,
        //   "currency" => "JPY",
        // ]);


        $bank_data = \Stripe\Token::create(array(
            "bank_account" => array(
            "country" => "US",
            "currency" => "usd",
            "account_holder_name" => "Charlotte Thomas",
            "account_holder_type" => "individual",
            "routing_number" => "110000000",
            "account_number" => "000123456789"
          )
        )   );
        $bank_token = $bank_data->id;
        $bank_account = $customer->sources->create(array("source" => $bank_token));

        var_dump($bank_account);
        $payout_data = \Stripe\Payout::create(array(
          "amount" => 100,
          "currency" => "usd",
        ));

    }


}
