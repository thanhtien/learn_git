<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Notification extends UserController {
    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->auth();
    }
    /**
     * [createComment_post Create Comment]
     * @return [type] [description]
    */
    public function createTokenNotification_get() {
        // get User in  Auth
        $token = $this->get('token');
        $user = $this->user_data;
        $user_id =  $user->id;

        $data = array(
            'token' => $token,
        );
        $edit = $this->User->editUser($user_id,$data);
        if(isset($edit) && $edit){
            $success = array(
                'status' => 'update token notification success!'
            );
            $this->response($success,200);
        }else{
            $error = array(
                'status' => 'update token notification error!'
            );
            $this->response($success,400);
        }
    }
    /**
     * Get Notification and count notification.
     */
    public function getNotification_get() {
        // get user
        $user = $this->user_data;
        $user_id =  $user->id;
        // get data user
        $curUser =   $this->User->getUserId($user_id);
        // totalPage
        $limit = 10;
        // count notification
        $NumberComment = $this->Notification_info->CountAllNotificationById($user_id);
        // total page of get notification
        $totalPages = ceil($NumberComment / $limit);
        // check user exists
        if(isset($curUser) && $curUser) {
            // get Notification and count notification(status = 1)
            $total = $this->Notification_info->getAllNotification($user_id);

            foreach ($total['notification'] as $key => $value) {

                $date1 = new DateTime($value->created);
                $date2 = new DateTime(date('Y-m-d H:i:s'));
                $time = $date1->diff($date2);

                $date = date_diff($date1,$date2);
                $date = $date->format("%a");
                $format_collection_end_date = array(
                    'date' =>$date,
                    'hour' => $time->h,
                    'minutes' => $time->i,
                    'second' => $time->s
                );
                $value->date_format = $format_collection_end_date;
            }

            // create array
            $data = array(
                'amount' => $total['amount'],
                'total'  => $totalPages,
                'data'   => $total['notification']
            );
            // response
            $this->response($data,200);
        }else {
            $error = array();
            $this->response($error,200);
        }
    }

    public function LoadMoreNotification_get(){
        // get user
        $user = $this->user_data;
        $user_id =  $user->id;
        // get data user
        $curUser =   $this->User->getUserId($user_id);
        // check user exists
        if(isset($curUser) && $curUser) {

            $start = $this->get('page');
            $limit = '5';
            if(!$start || $start =='0'){
              $start ='0';
            }else{
              $start = $start*$limit;
            }
            $input['limit']  = array($limit,$start);
            $NumberComment = $this->Notification_info->CountAllNotificationById($user_id);
            // total page of get notification
            $totalPages = ceil($NumberComment / $limit);
            $NumberNotification = $this->Notification_info->LoadMoreNotification($user_id,$input);
            foreach ($NumberNotification as $key => $value) {

                $date1 = new DateTime($value->created);
                $date2 = new DateTime(date('Y-m-d H:i:s'));
                    $time = $date1->diff($date2);

                    $date = date_diff($date1,$date2);
                    $date = $date->format("%a");
                    $format_collection_end_date = array(
                        'date' =>$date,
                        'hour' => $time->h,
                        'minutes' => $time->i,
                        'second' => $time->s
                    );
                $value->date_format = $format_collection_end_date;
            }
            $data = array(
                'page_count' => $totalPages,
                'data' => $NumberNotification
            );
            $this->response($data,200);

        }
    }

    public function clickNotification_get(){
        // get user
        $user = $this->user_data;
        $user_id =  $user->id;
        // get data user
        $curUser =   $this->User->getUserId($user_id);

        // check user exists
        if(isset($curUser) && $curUser) {

            $limit = 5;
            // count notification
            $NumberComment = $this->Notification_info->CountAllNotificationById($user_id);
            // total page of get notification
            $totalPages = ceil($NumberComment / $limit);

            $start = $this->get('page');
            $limit = '10';
            if(!$start || $start === '0'){
              $start ='0';
            }else{
              $start = $start*$limit;
            }
            $NumberNotification = $this->Notification_info->getNotificationStatus($user_id);

            $data = array(
                'check_status' => 1
            );
            foreach ($NumberNotification as $key => $value) {
                $this->Notification_info->update($value->id,$data);
            }

            $total = $this->Notification_info->getAllNotification($user_id);
            // get all new notification(status = 0)
            $data = array(
                'amount' => $total['amount'],
                'total'  => $totalPages,
                'data'   => $total['notification']
            );
            // response
            $this->response($data,200);
        }
    }

    public function clickNotificationSeen_get() {
        $notificationId = $this->get('id');
        // get user
        $user = $this->user_data;
        $user_id =  $user->id;
        // get data user
        $curUser =  $this->User->getUserId($user_id);

        // check user exists
        if(isset($curUser) && $curUser) {
            $data = array(
                'status' => 1
            );
            $this->Notification_info->update($notificationId,$data);

            $data = array(
                'status' => 'true'
            );
            // response
            $this->response($data,200);
        }
    }
    /**
     * [announceFromAdmin_get get info annnounce admin send all user
     * in the system]
     * @return [type] [description]
     */
    public function announceFromAdmin_get(){
        $user = $this->user_data;
        $user_id =  $user->id;
        // get data user
        $curUser =   $this->User->getUserId($user_id);
        if(isset($curUser) && $curUser){
            $infoAnnounce = $this->Announce->getInfoAnnounce();
            if(isset($infoAnnounce) && $infoAnnounce) {
                $this->response($infoAnnounce,200);
            }else{
                $this->response(NULL,200);
            }
        } else {
            $error = array(
                'status' => 'error!'
            );
            $this->response($error,404);
        }
    }

    public function DetailNotificationFromAdmin_get(){
        $user = $this->user_data;
        $user_id =  $user->id;
        // get data user
        $curUser =   $this->User->getUserId($user_id);
        if(isset($curUser) && $curUser){
            $notificationId = $this->get('id');
            $data = $this->Announce->getAnnounceById($notificationId);
            if(isset($data) && $data) {
                $this->response($data,200);
            }else {
                $error = array(
                    'status' => 'error!'
                );
                $this->response($error,404);
            }
        } else {
            $error = array(
                'status' => 'error!'
            );
            $this->response($error,404);
        }
    }

    // public function

}
