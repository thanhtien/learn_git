<?php

  defined('BASEPATH') OR exit('No direct script access allowed');

  class HelloAnalytics extends UserController {

    function __construct(){
      parent::__construct();

    }

    public function initializeAnalytics() {
      // Creates and returns the Analytics Reporting service object.

      // Use the developers console and download your service account
      // credentials in JSON format. Place them in this directory or
      // change the key file location if necessary.

      $KEY_FILE_LOCATION = __DIR__ . '/service-account-credentials.json';

      // Create and configure a new client object.
      $client = new Google_Client();
      $client->setApplicationName("Hello Analytics Reporting");
      $client->setAuthConfig($KEY_FILE_LOCATION);
      $client->setScopes(['https://www.googleapis.com/auth/analytics.readonly']);
      $analytics = new Google_Service_Analytics($client);

      return $analytics;
    }

    public function getFirstProfileId($analytics) {
      // Get the user's first view (profile) ID.

      // Get the list of accounts for the authorized user.
      $accounts = $analytics->management_accounts->listManagementAccounts();

      if (count($accounts->getItems()) > 0) {
        $items = $accounts->getItems();
        $firstAccountId = $items[0]->getId();

        // Get the list of properties for the authorized user.
        $properties = $analytics->management_webproperties
            ->listManagementWebproperties($firstAccountId);

        if (count($properties->getItems()) > 0) {
          $items = $properties->getItems();
          $firstPropertyId = $items[0]->getId();

          // Get the list of views (profiles) for the authorized user.
          $profiles = $analytics->management_profiles
              ->listManagementProfiles($firstAccountId, $firstPropertyId);

          if (count($profiles->getItems()) > 0) {
            $items = $profiles->getItems();

            // Return the first view (profile) ID.
            return $items[0]->getId();

          } else {
            throw new Exception('No views (profiles) found for this user.');
          }
        } else {
          throw new Exception('No properties found for this user.');
        }
      } else {
        throw new Exception('No accounts found for this user.');
      }
    }

    public function getResults($analytics, $profileId, $link, $first_day ,$last_day) {
      // Calls the Core Reporting API and queries for the number of sessions
      // for the last seven days.
      $optParams = array('filters' =>'ga:pagePath=='.$link'','dimensions'=>'ga:date');

       return $analytics->data_ga->get(
        'ga:' . $profileId,
        $first_day,
        $last_day,
        'ga:pageviews',
        $optParams
      );
    }



    public function index_get() {
        
        $link = $this->input->get('link');
        $first_day = $this->input->get('first_day');
        $last_day = $this->input->get('last_day');

        require_once APPPATH."third_party/google-api-php-client/vendor/autoload.php";
        $analytics = $this->initializeAnalytics();

        $profile = $this->getFirstProfileId($analytics);


        $results = $this->getResults($analytics, $profile, $link, $first_day ,$last_day);


        if (count($results->getRows()) > 0) {

        $rows = $results->getRows();
            //200 status with data
        var_dump($rows);

        } else {
            //Get NOT FOUND 200 array emtpy
            print "No results found.\n";
        }

    }
}

?>
