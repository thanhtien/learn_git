<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class MailBoxs extends UserController {
    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->auth();
    }
    /**
     * [getMailSend_get get mail i send for owner i donate]
     * @return [type] [description]
     */
    public function getMailSend_get(){
        // Get Auth -> Get User_id
        $user = $this->user_data;
        $user_id =  $user->id;
        // get page
        $page = $this->get('page');
        // limit
        $limit = '20';
        // get page
        if(!$page || $page =='0'){
          $page = '0';
        }else{
          $page = $page * $limit;
        }
        // limit use CI
        $input['limit'] = array($limit,$page);
        // Get Profile
        $curUser =  $this->User->getUserId($user_id);

        if(isset($curUser) && $curUser) {
            $listEmail = $this->MailBox->getMailSend($user_id,$input);
            $totalPages = ceil($listEmail['total'] / $limit);
            $data = array(
                'totalPages' => $totalPages,
                'data'     => $listEmail['mail']
            );

            $this->response($data,200);

        }

    }

    /**
     * [getMailSend_get get mail i receive]
     * @return [type] [description]
     */
    public function getMailReceive_get(){
        // Get Auth -> Get User_id
        $user = $this->user_data;
        $user_id =  $user->id;
        // get page
        $page = $this->get('page');
        // limit
        $limit = '20';
        // get page
        if(!$page || $page =='0'){
          $page = '0';
        }else{
          $page = $page * $limit;
        }
        // limit use CI
        $input['limit'] = array($limit,$page);
        // Get Profile
        $curUser =  $this->User->getUserId($user_id);

        if(isset($curUser) && $curUser) {
            $listEmail = $this->MailBox->getMailReceive($user_id,$input);
            $totalPages = ceil($listEmail['total'] / $limit);
            $data = array(
                'totalPages' => $totalPages,
                'data'     => $listEmail['mail']
            );

            $this->response($data,200);

        }
    }

    /**
     * [sendMailBox_post send mail and sendnotifcation cho user]
     * @return [type] [description]
     */
    public function sendMailBox_post() {
        // get data config
        require("public/assets/socket/socket.io.php");
        $roomSocket = $this->config->item('roomSocket');
        $portRoomSocket = $this->config->item('portRoomSocket');
        // get key firebase
        $key_firebase = $this->config->item('key_firebase');

        // Get Auth -> Get User_id
        $user = $this->user_data;
        $user_id =  $user->id;

        // get info
        $message = $this->post('message');
        $project_id = $this->post('project_id');
        $user_id_receive = $this->post('user_id');

        // get date
        $datetime = new DateTime();
        // format date
        $datetime = $datetime->format('Y-m-d H:i:s');
        // change time current -> time UTC
        $asia_timestamp = strtotime($datetime);
        date_default_timezone_set('UTC');
        $date = date("Y-m-d H:i:s", $asia_timestamp);

        if(isset($user_id_receive) && $user_id_receive && isset($project_id) && $project_id){

            // Get Profile
            $curUser =  $this->User->getUserId($user_id);
            $curUserReceive =  $this->User->getUserId($user_id_receive);

            if(isset($curUser) && $curUser && isset($curUserReceive) && $curUserReceive) {
                $data = array(
                    'user_id_send' => $user_id,
                    'message' => $message,
                    'status' => 0,
                    'user_id_receive' => $user_id_receive,
                    'project_id' => $project_id,
                    'created' => $date,
                );
                $createMail = $this->MailBox->create($data);
                if($createMail) {

                    $datetime = new DateTime();
                    $datetime = $datetime->format('Y-m-d H:i:s');
                    // get strtotime + change time zone
                    $asia_timestamp = strtotime($datetime);
                    date_default_timezone_set('UTC');
                    // get Date vs time zone
                    $date = date("Y-m-d H:i:s", $asia_timestamp);

                    // array create notification info
                    // (sender)さんからメッセージが届きました
                    $dataNotification = array (
                        'user_id'       =>  $user_id_receive,
                        'user_action'   =>  'Admin',
                        'title'         =>  '終了間際のプロジェクト',
                        'message'       =>  $curUserReceive->username.'さんからメッセージが届きました',
                        'link'          =>  "my-page/emailBox/page=1",
                        'status'        =>  0,
                        'thumbnail'     =>  base_url()."images/2018/default/logo-icon.png",
                        'created'       =>  $date
                    );
                    // create notification info
                    $createNotification = $this->Notification_info->create($dataNotification);

                    $socketio = new SocketIO();
                    // // array
                    $data = array(
                        'room'        =>  'user_'.$user_id_receive,
                        'title'       =>  '終了間際のプロジェクト',
                        'message'     =>  $curUserReceive->username.'さんからメッセージが届きました',
                        'url'         =>  app_url()."my-page/emailBox/page=1",
                        'ava'         =>  base_url().'images/2018/default/logo-icon.png'
                    );


                    $socketio->send($roomSocket, $portRoomSocket, 'annouce_project',json_encode($data));

                    $tokenDonor = $curUserReceive->token;

                    $curl = curl_init();
                    curl_setopt_array($curl, array(
                        CURLOPT_URL => "https://fcm.googleapis.com/fcm/send",
                        CURLOPT_RETURNTRANSFER => true,
                        CURLOPT_ENCODING => "",
                        CURLOPT_MAXREDIRS => 10,
                        CURLOPT_TIMEOUT => 30,
                        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                        CURLOPT_CUSTOMREQUEST => "POST",
                        CURLOPT_POSTFIELDS => "{   \"notification\": {\r\n        \"title\": \"終了間際のプロジェクト\",\r\n        \"body\": \"".$curUserReceive->username."さんからメッセージが届きました\",\r\n        \"click_action\": \"".app_url()."my-page/emailBox/page=1\",\r\n        \"icon\": \"".base_url()."images/2018/default/logo-icon.png\"\r\n    },\r\n  \"to\" : \"".$tokenDonor."\"\r\n}",
                        CURLOPT_HTTPHEADER => array(
                        "Authorization: key=".$key_firebase."",
                        "Content-Type: application/json",
                        ),
                    ));

                    $response = curl_exec($curl);
                    $err = curl_error($curl);

                    curl_close($curl);

                    // get page
                    $page = '0';
                    // limit
                    $limit = '20';

                    // limit use CI
                    $input['limit'] = array($limit,$page);

                    $listEmail = $this->MailBox->getMailSend($user_id,$input);
                    $totalPages = ceil($listEmail['total'] / $limit);
                    $data = array(
                    'totalPages' => $totalPages,
                    'data'     => $listEmail['mail']
                    );

                    $this->response($data,200);
                }
            } else {
                $error = array(
                    'status' => 'Error!'
                );
                $this->response($error,404);
            }
        }else{
            $error = array(
                'status' => 'Error!'
            );
            $this->response($error,404);
        }
    }

    public function getInfoOwner_get(){
        $projectId = $this->get('project_id');
        $Owner = $this->Project->getOwnerByProjectId($projectId);
        if(isset($Owner) && $Owner){
            $this->response($Owner,200);
        }else {
            $error = array(
                'status' => 'Error!'
            );
            $this->response($error,404);
        }
    }

    public function getInfoUser_get(){
        $user_id = $this->get('user_id');
        $Owner = $this->User->getUserId($user_id);
        if(isset($Owner) && $Owner){
            $data = array(
                'id' => $Owner->id,
                'username' => $Owner->username,
                'profileImageURL' => $Owner->profileImageURL
            );
            $this->response($data,200);
        }else {
            $error = array(
                'status' => 'Error!'
            );
            $this->response($error,404);
        }
    }

    /**
     * [detailMailBox_get get detail email ]
     * @return [type] [description]
     */
    public function detailMailBox_get(){
        // get info
        $idMailBox = $this->get('id');
        $user = $this->user_data;
        $user_id =  $user->id;

        // Get Profile
        $curUser =  $this->User->getUserId($user_id);
        $mailBox = $this->MailBox->checkMailBox($idMailBox);

        if(isset($mailBox) && $mailBox && isset($curUser) && $curUser) {
            if($user_id === $mailBox->user_id_send) {
                $Email = $this->MailBox->getDetailEmailSend($idMailBox);
                $this->response($Email,200);
            }else if($user_id === $mailBox->user_id_receive){
                $Email = $this->MailBox->getDetailEmailReceive($idMailBox);
                $this->response($Email,200);
            }
        }else {
            $error = array(
                'status' => 'Error!'
            );
            $this->response($error,404);
        }
    }

    public function checkReadMailBox_get(){
        $idMailBox = $this->get('id');
        $user = $this->user_data;
        $user_id =  $user->id;
        // Get Profile
        $curUser =  $this->User->getUserId($user_id);
        $mailBox = $this->MailBox->checkMailBox($idMailBox);

        if(isset($mailBox) && $mailBox && isset($curUser) && $curUser ) {
            $dataMailBox = array(
                'status' => 1
            );
            $udpate = $this->MailBox->update($idMailBox,$dataMailBox);
            if($udpate) {
                $mailBox->status = '1';
                $success = array(
                    'status' => 'success!'
                );
                $this->response($success,200);
            }
        }else {

            $error = array(
                'status' => 'Error!'
            );
            $this->response($error,404);

        }
    }
}
