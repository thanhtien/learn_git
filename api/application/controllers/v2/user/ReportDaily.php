<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class ReportDaily extends UserController {
    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->auth();
    }
    /**
     * Create Report Daily Owner Project for Donnor
     */
    public function createReportDaily_post(){
        // get User current(Owner)
        $user = $this->user_data;
        $user_id =  $user->id;
        // get project id to client
        $projectId = $this->post('project_id');
        // get data project to ProjectId
        $project = $this->Project->ProjectId($projectId);
        // check Project and else
        if(isset($project) && $project) {
            // check user current is owner project and else
            if($project->user_id === $user_id){

                $config = [
                [
                    'field' => 'title',
                    'label' => 'title Name',
                    'rules' => 'required',
                    'errors' => [
                        'required' => 'プロジェクト名を入力してください。',
                    ],
                ],
                [
                    'field' => 'content',
                    'label' => 'content',
                    'rules' => 'required',
                    'errors' => [
                        'required' => 'プロジェクト名を入力してください。',
                    ],
                ],
                ];
                // Use post
                $data = $this->post();
                $this->form_validation->set_data($data);
                $this->form_validation->set_rules($config);
                // form validation run and else
                if ($this->form_validation->run() !== false) {
                    // get data of report Daily
                    $title = $this->post('title');
                    $thumbnail = $this->post('thumbnail');
                    $content = $this->post('content');
                    $status = $this->post('status');
                    if(isset($status) && $status){
                        $statuspublic = $status;
                    }else{
                        $statuspublic = 0;
                    }
                    // check thumbnail exists if no( assign = image default)
                    if(!isset($thumbnail) || $thumbnail === '' ){
                        $thumbnail = base_url().'images/2018/default/noimage-01.png';
                    }

                    // get Datetime
                    $date = new DateTime();
                    $date = $date->format('Y-m-d H:i:s');
                    // create $dataReport
                    $dataReport = array(
                        'title' => $title,
                        'thumbnail' => $thumbnail,
                        'content' => $content,
                        'project_id' => $projectId,
                        'status'  => $statuspublic,
                        'created' => $date,
                        'updated' => $date,
                    );
                    // create report
                    $createReport = $this->Report->create($dataReport);
                    // check create success and else
                    // var_dump($createReport);
                    if($createReport){
                        $dataReport['id'] = $createReport;

                        // $success = array(
                        //     'status' => 'Create Report daily for project success!'
                        // );
                        $this->response($dataReport,200);
                    }else{
                        $error = array(
                            'status' => 'エラー！記事作成ができません'
                        );
                        $this->response($error,400);
                    }
                }else{
                    $error = $this->form_validation->error_array();
                    $this->response($error,400);
                }

            }else{
                $error = array(
                    'status' => 'Project not Found!'
                );
                $this->response($error,404);
            }
        }else{
            $error = array(
                'status' => 'Project not Found!'
            );
            $this->response($error,404);
        }
    }
    /**
     * Edit Report Daily
     */
    public function EditReportDaily_put(){
        // get reportId
        $reportId = $this->put('id');
        // check reportId and else
        if(isset($reportId) && $reportId){
            // get user current
            $user = $this->user_data;
            $user_id =  $user->id;
            // get data report by reportId
            $report = $this->Report->getReportById($reportId);
            // get data project by report->projectid
            $project = $this->Project->getProjectId($report->project_id);
            if($project->user_id === $user_id){
                $config = [
                [
                    'field' => 'title',
                    'label' => 'title Name',
                    'rules' => 'required',
                    'errors' => [
                        'required' => 'プロジェクト名を入力してください。',
                    ],
                ],
                [
                    'field' => 'content',
                    'label' => 'content',
                    'rules' => 'required',
                    'errors' => [
                        'required' => 'プロジェクト名を入力してください。',
                    ],
                ],
                ];
                // Use post
                $data = $this->put();
                $this->form_validation->set_data($data);
                $this->form_validation->set_rules($config);
                // form validation run and else
                if ($this->form_validation->run() !== false) {
                    // get data by $this->put
                    $title = $this->put('title');
                    $thumbnail = $this->put('thumbnail');
                    $content = $this->put('content');
                    $status = $this->put('status');
                    // check thumbnail exists if no( assign = image default)
                    if(!isset($thumbnail) || $thumbnail === ''){
                        $thumbnail = $report->thumbnail;
                    }
                    $date = new DateTime();
                    $date = $date->format('Y-m-d H:i:s');
                    // create $dataReport
                    $dataReport = array(
                        'title' => $title,
                        'thumbnail' => $thumbnail,
                        'content' => $content,
                        'updated' => $date,
                        'status'  => $status
                    );
                    // create report
                    $updateReport = $this->Report->update($reportId,$dataReport);
                    if($updateReport){
                        $dataReport['id'] = $reportId;
                        $this->response($dataReport,200);
                    }else{
                        $error = array(
                            'status' => 'エラー！記事を編集できません'
                        );
                        $this->response($error,400);
                    }
                }else{
                    $error = $this->form_validation->error_array();
                    $this->response($error,400);
                }
            }else{
                $error = array(
                    'status' => 'Not Found Repot!'
                );
                $this->response($error,404);
            }
        }else{
            $error = array(
                'status' => 'Not Found Id!'
            );
            $this->response($error,404);
        }
    }
    /**
     * Delete Report daily
     */
    public function delReportDaily_delete($reportId) {
        // get reportId
        // $reportId = $this->delete('id');
        // check reportId and else
        if(isset($reportId) && $reportId){
            // get user current
            $user = $this->user_data;
            $user_id =  $user->id;
            // get data report by reportId
            $report = $this->Report->getReportById($reportId);
            // get data project by report->projectid
            $project = $this->Project->getProjectId($report->project_id);
            if($project->user_id === $user_id){
                $delReport = $this->Report->delReportById($reportId);
                if($delReport){
                    $success = array(
                        'status' => '成功！記事を削除できました'
                    );
                    $this->response($success,200);
                }else{
                    $error = array(
                        'status' => 'エラー！記事を削除できません'
                    );
                    $this->response($error,400);
                }
            }else{
                $error = array(
                    'status' => 'Not Found Repot!'
                );
                $this->response($error,404);
            }
        }else{
            $error = array(
                'status' => 'Not Found Id!'
            );
            $this->response($error,404);
        }
    }

    public function listReport_get() {
        // get user
        $user = $this->user_data;
        $user_id =  $user->id;

        $projectId = $this->get('project_id');
        $page = $this->get('page');

        $limit = '5';
        if(!$page || $page =='0'){
          $pagelimit ='0';
        }else{
          $pagelimit = $page*$limit;
        }

        if(isset($projectId) && $projectId){

            $project = $this->Project->ProjectId($projectId);

            if($project->user_id === $user_id){
                $reportDaily = $this->Report->listReport($projectId,$limit,$pagelimit);


                $numberReport = $this->Report->countReportDaily($projectId);

                $totalPages = ceil($numberReport / $limit);
                $data = array(
                    'page_count' => $totalPages,
                    'data' => $reportDaily
                );
                $this->response($data,200);
            }else{
                $error = array(
                    'status' => 'Not found report!'
                );
                $this->response($error,404);
            }
        }else{
            $error = array(
                'status' => 'Not found report!'
            );
            $this->response($error,404);
        }
    }

    public function ViewReportDetail_get(){
        $reportId = $this->get('id');

        if(isset($reportId) && $reportId){
            $report = $this->Report->getReportById($reportId);
            if(isset($report) && $report){
                $this->response($report,200);
            }else{
                $error = array(
                    'status' => 'Not found report!'
                );
                $this->response($error,404);
            }
        }else{
            $error = array(
                'status' => 'Not found report!'
            );
            $this->response($error,404);
        }
    }
}
