<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Projects extends UserController {
    function __construct()
    {
        parent::__construct();
        $this->auth();
    }

    public function testSendMail_get(){
        //use json_encode show data.
        $email = 'quyen123';
        $userName = 'quyen123';
        $projectName = 'quyen123';

        $this->load->library('email');
        $this->email->from('admin@kakuseida.conoha.io', 'KAKUSEIDA');
        $this->email->to('hodacquyenpx@gmail.com');

        $this->email->subject('「'.$userName.'」Toi Ngay Donate Cho「'.$projectName);
        $this->email->message(
            '「'.$userName.'」Donate Cho Project「'.$projectName.'」link:'.app_url().'project-detail/'.$value['project_id']
        );

        var_dump($this->email->send());
    }

    // /**
    // * [createProject2_post create new project from my page app]
    // * @return [type] [data project added]
    // */
    // public function createProject_post(){
    //
    //     // Get userId to token
    //     $user = $this->user_data;
    //     $user_id =  $user->id;
    //     //Config
    //     $config = [
    //     [
    //         'field' => 'project_name',
    //         'label' => 'Project Name',
    //         'rules' => 'required|max_length[150]',
    //         'errors' => [
    //             'required' => 'プロジェクト名を入力してください。',
    //             'max_length' => 'プロジェクト名は最大150文字までです。',
    //         ],
    //     ],
    //     [
    //         'field' => 'description',
    //         'label' => 'description',
    //         'rules' => 'max_length[255]',
    //         'errors' => [
    //             'max_length' => '「はじめにご挨拶」の文書は最大255文字までです。',
    //         ],
    //     ],
    //     [
    //         'field' => 'category_id',
    //         'label' => 'Category',
    //         'rules' => 'required',
    //         'errors' => [
    //             'required' => 'プロジェクト名を入力してください。',
    //         ],
    //     ],
    //     [
    //         'field' => 'thumbnail_movie_code',
    //         'label' => 'video',
    //         'rules' => 'valid_url',
    //         'errors' => [
    //             'valid_url' => '動画のURLを入力してください',
    //         ],
    //     ],
    //     [
    //         'field' => 'collection_end_date',
    //         'label' => 'End Date',
    //         'rules' => 'required',
    //         'errors' => [
    //             'required' => '募集終了日を入力してください ',
    //         ],
    //     ],
    //     [
    //         'field' => 'goal_amount',
    //         'label' => 'Goal Amount',
    //         'rules' => 'required',
    //         'errors' => [
    //             'required' => '目標金額を入力してください',
    //             // 'min_length'=>'目標金額は、10,000円以上の金額で入力してください。'
    //         ],
    //     ],
    //     ];
    //     // Use post
    //     $data = $this->post();
    //     $this->form_validation->set_data($data);
    //     $this->form_validation->set_rules($config);
    //     // form validation run
    //     if ($this->form_validation->run() !== false) {
    //         //Get Date current
    //         $date = new DateTime();
    //         //fomat date
    //         $date = $date->format('Y-m-d H:i:s');
    //         // listed data with $this->post
    //         $project_name = $this->post('project_name');
    //         $category = $this->post('category_id');
    //         $summary = $this->post('description');
    //         $radio = $this->post('thumbnail_type');
    //         $video = $this->post('thumbnail_movie_code');
    //
    //         if(!isset($video)){
    //             $video = null;
    //         }
    //         $thumbnail1 = $this->post('thumbnail_descrip1');
    //         $thumbnail2 = $this->post('thumbnail_descrip2');
    //         $thumbnail3 = $this->post('thumbnail_descrip3');
    //
    //         if(!isset($thumbnail1)){
    //             $thumbnail1 = base_url().'images/2018/default/noimage-01.png';
    //         }
    //         if(!isset($thumbnail2)){
    //             $thumbnail2 = base_url().'images/2018/default/noimage-01.png';
    //         }
    //         if(!isset($thumbnail3)){
    //             $thumbnail3 = base_url().'images/2018/default/noimage-01.png';
    //         }
    //         $dateclose = $this->post('collection_end_date');
    //         $dateclose = new DateTime($dateclose);
    //         $dateclose = $dateclose->format('Y-m-d 23:59:59');
    //         $goal_amount = $this->post('goal_amount');
    //         $goal_amount = preg_replace('/,/', '', $goal_amount);
    //         if($goal_amount < 10000){
    //             $eror = array(
    //                 'goal_amount' =>'目標金額は、10,000円以上の金額で入力してください。'
    //             );
    //             $this->response($eror,400);
    //         }
    //         $project_content = $this->post('project_content');
    //         $thumbnail = $this->post('thumbnail');
    //         if(!isset($thumbnail)){
    //             $thumbnail = base_url().'images/2018/default/noimage-01.png';
    //         }
    //         // Add Data listed on $data
    //         $data_project = array(
    //             'project_name' => $project_name,
    //             'category_id' => $category,
    //             'description' => $summary,
    //             'goal_amount' => $goal_amount,
    //             'thumbnail_descrip1'=> $thumbnail1,
    //             'thumbnail_descrip2'=> $thumbnail2,
    //             'thumbnail_descrip3'=> $thumbnail3,
    //             'thumbnail_type'=>$radio,
    //             'thumbnail_movie_code'=>$video,
    //             'collection_start_date'=>$date,
    //             'collection_end_date'=>$dateclose,
    //             'user_id' => $user_id,
    //             'active' => 'no',
    //             'created' => $date,
    //             'modified'=> $date,
    //             'thumbnail'=>$thumbnail,
    //             'project_content'=>$project_content
    //         );
    //         // Create Project
    //         $project = $this->Project->addProject($data_project);
    //         // Get Id
    //         $data_project['id'] = $project;
    //         // Check Project
    //         if($project){
    //             // Pagination
    //             $start = '0';
    //             $limit = '16';
    //             $user = $this->user_data;
    //             $user_id =  $user->id;
    //             $curUser =   $this->User->getUserId($user_id);
    //             $numberProject = $this->Project->getAllProject($user_id);
    //             $totalNews = count($numberProject);
    //             $totalPages = ceil($totalNews / $limit);
    //             $input['limit'] = array($limit,$start);
    //             // Get Data Project with pagination and user_i
    //             $projectPost = $this->Project->getAllProject($user_id,$input);
    //             // Get now count
    //             foreach ($projectPost as $key => $value) {
    //                 $dataCount = $this->BackingLevel->getBackingLevelId($value->id);
    //                 $now_count = 0;
    //                 foreach ($dataCount as $key => $value2) {
    //                     $now_count = $now_count + $value2->now_count;
    //                     $value->now_count=array(
    //                         'now_count'=>$now_count
    //                     );
    //                 }
    //                 $value->now_count=array(
    //                     'now_count'=>$now_count
    //                 );
    //             }
    //
    //             // get category
    //             foreach ($projectPost as $key => $value) {
    //                 $category = $categories = $this->Category->getCategoryId($value->category_id);
    //                 $value->category = array(
    //                     'id'=>$category->id,
    //                     'name'=>$category->name,
    //                     'slug'=>$category->slug
    //                 );
    //             }
    //             // get User
    //             foreach ($projectPost as $key => $value) {
    //                 $curUser =   $this->User->getUserId($value->user_id);
    //                 $value->user = array(
    //                     'id'=>$curUser->id,
    //                     'name'=>$curUser->username,
    //                 );
    //             }
    //
    //             $datacurUser = array(
    //                 'numberProject'=>$totalNews,
    //                 'page_count'=>$totalPages,
    //                 'post_project'=>$projectPost
    //             );
    //
    //             $date = new DateTime();
    //             $date = $date->format('Y-m-d H:i:s');
    //             // Get Project Return
    //             $data = $this->post('project_return');
    //             $dataReturn = count($data);
    //             // Check isset $dataReturn
    //             $dataInsertBatch =[];
    //             if($dataReturn >= 1){
    //                 // get for create Project return
    //                 for ($i=0; $i < $dataReturn ; $i++) {
    //                     // check data of project return and create data
    //                     $data[$i]['project_id'] = $project;
    //                     if(!isset($data[$i]['schedule'])){
    //                         $data[$i]['schedule']  = null;
    //                     }
    //                     if(!isset($data[$i]['max_count']) || $data[$i]['max_count'] === ''){
    //                         $data[$i]['max_count'] = null;
    //                     }
    //                     if(!isset($data[$i]['thumnail'])) {
    //                         $data[$i]['thumnail']  = base_url().'images/2018/default/noimage-01.png';
    //                     }
    //                     $data[$i]['now_count'] = 0;
    //                     $data[$i]['created'] = $date;
    //                     if(!isset($data[$i]['return_amount'])){
    //                         $data[$i]['return_amount'] = '';
    //                     }
    //                     if(isset($data[$i]['invest_amount'])){
    //                         array_push($dataInsertBatch,$data[$i]);
    //                     }
    //                 }
    //                 // create pro
    //                 $this->db->insert_batch('backing_levels', $dataInsertBatch);
    //             }
    //             $subscribe = $this->post('subscribe');
    //             if(isset($subscribe) && $subscribe){
    //                 // Create Notification action
    //                 $dataNotificationAction = array(
    //                     'action'  => $project,
    //                     'user_id' => $user_id,
    //                     'topics'  => 'project'
    //                 );
    //             }
    //             $this->response($datacurUser,200);
    //         }
    //         // Error of form validation
    //     }else{
    //         $error = $this->form_validation->error_array();
    //         $this->response($error,400);
    //     }
    // }

    /**
     * [updateProjectActive_put update project when project active
     * (increase invest amount and create project return )
     * not delete and edit project return]
     * @return [type] [description]
     */
    public function updateProjectActive_post() {
        $emailAdmin = $this->config->item('email_admin');
        // get id project
        $idProject = $this->post('id');
        // Get userId to token
        $user = $this->user_data;
        $user_id =  $user->id;

        $curUser =  $this->User->getUserId($user_id);
        // get Project
        $project = $this->Project->getProjectId($idProject);
        // check user_id === owner project
        $dateCurrent = new DateTime();
        $dateCurrent = $dateCurrent->format('Y-m-d 23:59:59');

        if($project->project_type === '0'){
            if($project->collection_end_date < $dateCurrent || $project->active !== 'yes') {
                $error = array(
                    'status' => 'Error!'
                );
                $this->response($error,404);
            }
        } else {
            if($project->status_fanclub === 'stop' || $project->active !== 'yes') {
                $error = array(
                    'status' => 'Error!'
                );
                $this->response($error,404);
            }
        }

        if($project->user_id === $user_id) {
            $title = $this->post('title');
            $show_order = $this->post('show_order');
            $message = $this->post('message');
            $project_return = $this->post('project_return');
            if($project->project_type === '0'){
                $goal_amount = $this->post('goal_amount');
                // check goal_amount < project->goal_amount(amount currrent)
                if((float)$goal_amount < (float)$project->goal_amount) {
                    $error = array(
                        'status' => '更新の目標金額は現在のより高くしてください。'
                    );
                    $this->response($error,301);
                }
            } else {
                $goal_amount = null;
            }

            // get Biggest list_status
            $backingEdit = $this->InfoBackingEdit->checkBackingByProjectId($idProject);
            // check exists list_status
            if(isset($backingEdit->list_status) && $backingEdit->list_status) {
                // assign data
                $data = $backingEdit->list_status + 1;
            } else {
                // assign data
                $data = 1;
            }
            // check Has Project return
            $checkExists = $this->InfoBackingEdit->getExistsBackingByProjectId($idProject);

            // check $checkExists
            if(isset($checkExists) && $checkExists){
                // announce error
                $error = array(
                    'status' => '現在、同一プロジェクトで審査中の更新案件があります。審査中の更新が承認された後に、新たな更新を申請してください。ご迷惑をおかけしますがよろしくお願い致します。'
                );
                $this->response($error,400);
            }

            // get date
            $datetime = new DateTime();
            // format date
            $datetime = $datetime->format('Y-m-d H:i:s');
            // change time current -> time UTC
            $asia_timestamp = strtotime($datetime);
            date_default_timezone_set('UTC');
            $date = date("Y-m-d H:i:s", $asia_timestamp);

            // create array
            $dataInfo = array(
                'goal_amount' => $goal_amount,
                'project_id' => $idProject,
                'list_status' => $data,
                'title' => $title,
                'message' => $message,
                'status' => '0',
                'created' => $date,

            );
            $insertListBacking = $this->InfoBackingEdit->create($dataInfo);
            if($insertListBacking) {

                // count array  =  ?
                $dataReturn = count($project_return);
                // Check isset $dataReturn
                $dataInsertBatch = array();
                // if $dataReturn >= 1
                if($dataReturn >= 1) {

                    // get for create Project return
                    for ($i=0; $i < $dataReturn ; $i++) {
                        // check data of project return and create data
                        $project_return[$i]['project_id'] = $idProject;
                        if(!isset($project_return[$i]['schedule'])){
                            $project_return[$i]['schedule']  = null;
                        }
                        if(!isset($project_return[$i]['max_count']) || $project_return[$i]['max_count'] === ''){
                            $project_return[$i]['max_count'] = null;
                        }
                        if(!isset($project_return[$i]['thumnail'])) {
                            $project_return[$i]['thumnail']  = base_url().'images/2018/default/noimage-01.png';
                        }
                        $project_return[$i]['now_count'] = 0;
                        $project_return[$i]['created'] = $date;
                        if(!isset($project_return[$i]['return_amount'])){
                            $project_return[$i]['return_amount'] = '';
                        }
                        $project_return[$i]['list_status'] = $data;
                        //
                        // $project_return[$i]['goal_amount'] = $goal_amount;
                        // $project_return[$i]['title'] = $title;
                        // $project_return[$i]['message'] = $message;
                        array_push($dataInsertBatch,$project_return[$i]);
                    }

                    // create project_return by backing_levels_edit
                    $insertBacking = $this->db->insert_batch('backing_levels_edit', $dataInsertBatch);

                    // install success
                }

                $this->load->library('email');
                $this->email->from('admin@kakuseida.conoha.io', 'KAKUSEIDA');
                $this->email->to($emailAdmin);
                $this->email->subject('【'.$project->project_name.'】の更新が申請されました。');
                $this->email->message('【'.$curUser->username.'】さんの【'.$project->project_name.'】の更新が申請されました。<br>
                    詳しくは、以下のURLでご確認ください。<br>
                    <a href="'.admin_url().'project/edit/'.$idProject.'?tab=2'.'">'.admin_url().'project/edit/'.$idProject.'?tab=2'.'</a>'
                );
                $this->email->send();
                // announce
                $success = array(
                    'status' => 'Update Success'
                );
                $this->response($success,200);
            // error
            }else {
                $error = array(
                    'status' => 'Update failure'
                );
                $this->response($error,400);
            }
        }else {
            $error = array(
                'status' => 'Error Not Found!'
            );
            $this->response($error,404);
        }
    }

    public function projectReturnClaimActiveDetail_get(){
        $project_id = $this->get('project_id');
        $list_status = $this->get('list_status');
        $project = $this->Project->getProjectId($project_id);

        if(isset($project) && $project ){
            $ListBacked = $this->InfoBackingEdit->getListBacking($project_id,$list_status);

            if(isset($ListBacked) && $ListBacked){
                $getInfo = $this->BackingLevelEdit->getListBacking($project_id,$list_status);

                $title = $ListBacked->title;
                $message = $ListBacked->message;
                $goal_amount = $ListBacked->goal_amount;
                $list_status = $ListBacked->list_status;
                $status = $ListBacked->status;

                $data = array(
                    'goal_amount' => $goal_amount,
                    'list_status' => $list_status,
                    'title' => $title,
                    'status' => $status,
                    'message' => $message,
                    'project_return' => $getInfo,
                );
                $this->response($data,200);
            }else {
                $error = array(
                    'status' => 'Not Found Project Id'
                );
                $this->response($error,404);
            }
        }else{
            $error = array(
                'status' => 'Not Found Project Id'
            );
            $this->response($error,404);
        }
    }


    public function listProjectReturnClaimActive_get() {
        $project_id = $this->get('project_id');

        $project = $this->Project->getProjectId($project_id);
        // Get userId to token
        $user = $this->user_data;
        $user_id =  $user->id;
        if(isset($project) && $project ){
            if($user_id === $project->user_id) {
                $start = $this->get('page');
                $limit = '10';
                if(!$start || $start =='0'){
                  $start = '0';
                }else{
                  $start = $start * $limit;
                }
                $dateCurrent = new DateTime();
                $dateCurrent = $dateCurrent->format('Y-m-d 23:59:59');
                if($project->project_type === '0'){
                    if($project->collection_end_date < $dateCurrent || $project->active !== 'yes') {
                        $error = array(
                            'status' => 'Error!'
                        );
                        $this->response($error,404);
                    }
                } else {
                    if($project->status_fanclub === 'stop' || $project->active !== 'yes') {
                        $error = array(
                            'status' => 'Error!'
                        );
                        $this->response($error,404);
                    }
                }

                $input['limit'] = array($limit,$start);
                $checkStatus = $this->InfoBackingEdit->checkStatusByProjectId($project_id);
                if(isset($checkStatus) && $checkStatus){
                    $checkStatus = 0;
                }else{
                    $checkStatus = 1;
                }

                $ListBacked = $this->InfoBackingEdit->getBackingByProjectId2($project_id,$input);
                foreach ($ListBacked['data'] as $key => $value) {
                    $getInfo = $this->BackingLevelEdit->getListBacking($value->project_id,$value->list_status);
                    $value->project_return = $getInfo;
                }
                $totalPages = ceil($ListBacked['total'] / $limit);
                $data = array(
                    'project_type' => $project->project_type,
                    'page_count' => $totalPages,
                    'status' => $checkStatus,
                    'data' => $ListBacked['data'],

                );

                $this->response($data,200);
            }else {
                $error = array(
                    'status' => 'Not Found Project Id'
                );
                $this->response($error,404);
            }
        }else{
            $error = array(
                'status' => 'Not Found Project Id'
            );
            $this->response($error,404);
        }
    }














    // public function projectReturnClaimActive_put(){
    //     $user = $this->user_data;
    //     $user_id =  $user->id;
    //     $project_id = $this->put('project_id');
    //     $list_status = $this->put('list_status');
    //     $project_return = $this->put('project_return');
    //     $goal_amount = $this->put('goal_amount');
    //     $title = $this->put('title');
    //     $message = $this->put('message');
    //     $project = $this->Project->getProjectId($project_id);
    //
    //     $projectReturnId = $this->BackingLevelEdit->getListBacking($project_id,$list_status);
    //     if($projectReturnId[0]['status'] === '2'){
    //         $error = array(
    //             'status' => 'Project da public'
    //         );
    //         $this->response($error,404);
    //     }
    //     if(isset($project) && $project ){
    //
    //         if($project->user_id === $user_id){
    //             $date = new DateTime();
    //             $date = $date->format('Y-m-d H:i:s');
    //
    //             $insert = array();
    //             $update = array();
    //             $updateId = array();
    //             $dataProject = count($project_return);
    //             if($dataProject >= 1){
    //                 for ($i=0; $i < $dataProject ; $i++) {
    //
    //                     $project_return[$i]['project_id'] = $project_id;
    //                     if(!isset($project_return[$i]['schedule'])){
    //                         $project_return[$i]['schedule']  = null;
    //                     }
    //                     if(!isset($project_return[$i]['max_count']) || $project_return[$i]['max_count'] === ''){
    //                         $project_return[$i]['max_count'] = null;
    //                     }
    //                     if(!isset($project_return[$i]['thumnail'])) {
    //                         $project_return[$i]['thumnail']  = base_url().'images/2018/default/noimage-01.png';
    //                     }
    //                     if(!isset($project_return[$i]['return_amount'])){
    //                         $project_return[$i]['return_amount'] = '';
    //                     }
    //
    //                     $project_return[$i]['now_count'] = 0;
    //                     $project_return[$i]['title'] = $title;
    //                     $project_return[$i]['message'] = $message;
    //                     $project_return[$i]['goal_amount'] = $goal_amount;
    //                     $project_return[$i]['created'] = $date;
    //                     $project_return[$i]['modified'] = $date;
    //                     $project_return[$i]['status'] = '0';
    //                     $project_return[$i]['list_status'] = $list_status;
    //                     if(!empty($project_return[$i]) && isset($project_return[$i]['invest_amount'])){
    //                         if(!isset($project_return[$i]['id'])){
    //                             array_push($insert,$project_return[$i]);
    //                         }else{
    //                             array_push($update,$project_return[$i]);
    //                             array_push($updateId,$project_return[$i]['id']);
    //                         }
    //                     }
    //                 }
    //
    //                 $newarrarytest = array();
    //
    //                 foreach ($projectReturnId as $key => $value) {
    //                     array_push($newarrarytest,(int)$value['id']);
    //                 }
    //
    //                 $delete = (array_diff($newarrarytest, $updateId));
    //                 // var_dump($projectReturnId);
    //
    //                 $projectIdDel = array();
    //                 foreach ($delete as $key => $value) {
    //                     array_push($projectIdDel,$value);
    //                 }
    //
    //                 $arrayProjectIdDel = implode(',', $projectIdDel);
    //
    //                 if(count($update) >= 1){
    //                     foreach ($update as $key => $value) {
    //                         $this->BackingLevelEdit->update($value['id'],$value);
    //                     }
    //                 }
    //                 if($arrayProjectIdDel != ''){
    //                     $this->BackingLevelEdit->deleteProjectReturn($arrayProjectIdDel);
    //                 }
    //                 if(count($insert) >= 1){
    //                     $this->db->insert_batch('backing_levels_edit', $insert);
    //                 }
    //             }
    //             $ListBacked = $this->BackingLevelEdit->getListBacking2($project_id,$list_status);
    //             if(isset($ListBacked) && $ListBacked){
    //                 $getInfo = $this->BackingLevelEdit->getListBacking($project_id,$list_status);
    //                 $title = $getInfo[0]['title'];
    //                 $message = $getInfo[0]['message'];
    //                 $goal_amount = $getInfo[0]['goal_amount'];
    //                 $list_status = $getInfo[0]['list_status'];
    //                 $status = $getInfo[0]['status'];
    //
    //                 $data = array(
    //                     'goal_amount' => $goal_amount,
    //                     'list_status' => $list_status,
    //                     'title' => $title,
    //                     'status' => $status,
    //                     'message' => $message,
    //                     'project_return' => $ListBacked,
    //                 );
    //                 $this->response($data,200);
    //             }
    //             // var_dump($projectReturnId);
    //             // exit;
    //             // $this->response($projectReturnId,200);
    //         }
    //     }else{
    //         $error = array(
    //             'status' => 'Not Found Project Id'
    //         );
    //         $this->response($error,404);
    //     }
    // }
    //
    // public function deleteProjectReturnPublic_delete($project_id,$list_status) {
    //     $user = $this->user_data;
    //     $user_id =  $user->id;
    //
    //     $project = $this->Project->getProjectId($project_id);
    //
    //     if(isset($project) && $project) {
    //         if($project->user_id === $user_id){
    //             $projectReturnId = $this->BackingLevelEdit->getListBacking($project_id,$list_status);
    //             if(isset($projectReturnId) && $projectReturnId){
    //                 $projectReturn = array();
    //                 foreach ($projectReturnId as $key => $value) {
    //                     array_push($projectReturn,(int)$value['id']);
    //                 }
    //                 $arrayProjectIdDel = implode(',', $projectReturn);
    //                 if($arrayProjectIdDel != ''){
    //                     $this->BackingLevelEdit->deleteProjectReturn($arrayProjectIdDel);
    //                     $success = array(
    //                         'status' => 'Delete Success'
    //                     );
    //                     $this->response($success,200);
    //                 }
    //
    //             }else {
    //                 $error = array(
    //                     'status' => 'Not Found Project'
    //                 );
    //                 $this->response($error,404);
    //             }
    //         }else{
    //             $error = array(
    //                 'status' => 'Not Found Project'
    //             );
    //             $this->response($error,404);
    //         }
    //     }else{
    //         $error = array(
    //             'status' => 'Not Found Project'
    //         );
    //         $this->response($error,404);
    //     }
    // }


    /**
    * [createProject2_post create new project from my page app]
    * @return [type] [data project added]
    */
    public function createProject_FanClub_post(){
        // Get userId to token
        $user = $this->user_data;
        $user_id =  $user->id;
        //Config
        $config = [
        [
            'field' => 'project_name',
            'label' => 'Project Name',
            'rules' => 'required|max_length[150]',
            'errors' => [
                'required' => 'プロジェクト名を入力してください。',
                'max_length' => 'プロジェクト名は最大150文字までです。',
            ],
        ],
        [
            'field' => 'description',
            'label' => 'description',
            'rules' => 'max_length[255]',
            'errors' => [
                'max_length' => '「はじめにご挨拶」の文書は最大255文字までです。',
            ],
        ],
        [
            'field' => 'category_id',
            'label' => 'Category',
            'rules' => 'required',
            'errors' => [
                'required' => 'プロジェクト名を入力してください。',
            ],
        ],
        [
            'field' => 'thumbnail_movie_code',
            'label' => 'video',
            'rules' => 'valid_url',
            'errors' => [
                'valid_url' => '動画のURLを入力してください',
            ],
        ],
        [
            'field' => 'collection_end_date',
            'label' => 'End Date',
            'rules' => 'required',
            'errors' => [
                'required' => '募集終了日を入力してください ',
            ],
        ],
        [
            'field' => 'goal_amount',
            'label' => 'Goal Amount',
            'rules' => 'required',
            'errors' => [
                'required' => '目標金額を入力してください',
                // 'min_length'=>'目標金額は、10,000円以上の金額で入力してください。'
            ],
        ],
        ];
        // Use post
        $data = $this->post();
        $this->form_validation->set_data($data);
        $this->form_validation->set_rules($config);
        // form validation run
        if ($this->form_validation->run() !== false) {
            //Get Date current
            $date = new DateTime();
            //fomat date
            $date = $date->format('Y-m-d H:i:s');
            // listed data with $this->post
            $project_name = $this->post('project_name');
            $category = $this->post('category_id');
            $summary = $this->post('description');
            $radio = $this->post('thumbnail_type');
            $video = $this->post('thumbnail_movie_code');

            if(!isset($video)){
                $video = null;
            }
            $thumbnail1 = $this->post('thumbnail_descrip1');
            $thumbnail2 = $this->post('thumbnail_descrip2');
            $thumbnail3 = $this->post('thumbnail_descrip3');

            if(!isset($thumbnail1)){
                $thumbnail1 = base_url().'images/2018/default/noimage-01.png';
            }
            if(!isset($thumbnail2)){
                $thumbnail2 = base_url().'images/2018/default/noimage-01.png';
            }
            if(!isset($thumbnail3)){
                $thumbnail3 = base_url().'images/2018/default/noimage-01.png';
            }
            $dateclose = $this->post('collection_end_date');
            $dateclose = new DateTime($dateclose);
            $dateclose = $dateclose->format('Y-m-d 23:59:59');
            $goal_amount = $this->post('goal_amount');
            $goal_amount = preg_replace('/,/', '', $goal_amount);
            if($goal_amount < 10000){
                $eror = array (
                    'goal_amount' =>'目標金額は、10,000円以上の金額で入力してください。'
                );
                $this->response($eror,400);
            }

            $project_type = $this->post('project_type');

            if(isset($project_type) && $project_type !== 0) {
                $project_type = 1;
                $number_month = $this->post('number_month');
                if(!isset($number_month) || $number_month === ''){
                    $number_month = 1;
                }
                $dateclose = null;
            }else{
                $number_month = 0;
                $project_type = 0;
            }

            $project_content = $this->post('project_content');
            $thumbnail = $this->post('thumbnail');
            if(!isset($thumbnail)){
                $thumbnail = base_url().'images/2018/default/noimage-01.png';
            }
            // Add Data listed on $data
            $data_project = array(
                'project_name' => $project_name,
                'category_id' => $category,
                'description' => $summary,
                'project_type' => $project_type,
                'number_month' => $number_month,
                'goal_amount' => $goal_amount,
                'thumbnail_descrip1'=> $thumbnail1,
                'thumbnail_descrip2'=> $thumbnail2,
                'thumbnail_descrip3'=> $thumbnail3,
                'thumbnail_type'=>$radio,
                'thumbnail_movie_code'=>$video,
                'collection_start_date'=>$date,
                'collection_end_date'=>$dateclose,
                'user_id' => $user_id,
                'active' => 'no',
                'created' => $date,
                'modified'=> $date,
                'thumbnail'=>$thumbnail,
                'project_content'=>$project_content
            );
            // Create Project
            $project = $this->Project->addProject($data_project);
            // Get Id
            $data_project['id'] = $project;
            // Check Project
            if($project){
                // Pagination
                $start = '0';
                $limit = '16';
                $user = $this->user_data;
                $user_id =  $user->id;
                $curUser =   $this->User->getUserId($user_id);
                $numberProject = $this->Project->getAllProjectJoinCat($user_id);
                $totalNews = count($numberProject);
                $totalPages = ceil($totalNews / $limit);
                $input['limit'] = array($limit,$start);
                // Get Data Project with pagination and user_i
                $projectPost = $this->Project->getAllProjectJoinCat($user_id,$input);

                foreach ($projectPost as $key => $value) {
                    $backing = $this->BackingLevel->getBackingLevelId($value->id);
                    $now_count = 0;
                    foreach ($backing as $key => $value2) {
                        $now_count = $now_count + $value2->now_count;
                        $value->now_count=array(
                            'now_count'=>$now_count
                        );
                    }
                    $value->now_count=array(
                        'now_count'=>$now_count
                    );

                    $value->category=array(
                        'id'=>$value->category_id,
                        'name'=>$value->name,
                        'slug'=>$value->slug
                    );

                    $value->user = array(
                        'id'=>$value->user_id,
                        'name'=>$value->username,
                    );

                    $timeProject = getTimeProject($value->collection_end_date);
                    $value->format_collection_end_date = $timeProject;
                }


                $datacurUser = array(
                    'numberProject'=>$totalNews,
                    'page_count'=>$totalPages,
                    'post_project'=>$projectPost
                );

                $date = new DateTime();
                $date = $date->format('Y-m-d H:i:s');
                // Get Project Return
                $data = $this->post('project_return');
                $dataReturn = count($data);
                // Check isset $dataReturn
                $dataInsertBatch =[];
                if($dataReturn >= 1) {
                    // get for create Project return
                    for ($i=0; $i < $dataReturn ; $i++) {
                        // check data of project return and create data
                        $data[$i]['project_id'] = $project;
                        if(!isset($data[$i]['schedule'])){
                            $data[$i]['schedule']  = null;
                        }
                        if(!isset($data[$i]['max_count']) || $data[$i]['max_count'] === ''){
                            $data[$i]['max_count'] = null;
                        }

                        if(!isset($data[$i]['thumnail'])) {
                            $data[$i]['thumnail']  = base_url().'images/2018/default/noimage-01.png';
                        }
                        $data[$i]['now_count'] = 0;
                        $data[$i]['created'] = $date;
                        if(!isset($data[$i]['return_amount'])){
                            $data[$i]['return_amount'] = '';
                        }
                        if(isset($data[$i]['invest_amount'])){
                            array_push($dataInsertBatch,$data[$i]);
                        }
                    }
                    // create pro
                    $this->db->insert_batch('backing_levels', $dataInsertBatch);
                }

                $this->response($datacurUser,200);
            }
            // Error of form validation
        }else{
            $error = $this->form_validation->error_array();
            $this->response($error,400);
        }
    }

    /**
     * [addWishList_get add wishlist in table + add project_id on info of user]
     */
    public function addWishList_post() {
        // Get userId to token
        $user = $this->user_data;
        $user_id =  $user->id;

        $project_id = $this->post('project_id');

        $curUser =   $this->User->getUserId($user_id);

        $project = $this->Project->getProjectId($project_id);

        if(isset($project) && $project->active === 'yes' && $project->status_fanclub === 'progress'  && isset($curUser) && $curUser) {

            $checkAddWishList = $this->WishList->checkAddWishList($user_id,$project_id);

            if(isset($checkAddWishList) && $checkAddWishList ){
                $error = array(
                    'status' => 'bạn đã like project này rồi!'
                );
                $this->response($error,400);
            }else {

                $datetime = new DateTime();
                // format date
                $datetime = $datetime->format('Y-m-d H:i:s');
                // change time current -> time UTC
                $asia_timestamp = strtotime($datetime);
                date_default_timezone_set('UTC');
                $date = date("Y-m-d H:i:s", $asia_timestamp);

                $dataWishList = array(
                    'user_id' => $user_id,
                    'project_id' => $project_id,
                    'created' => $date
                );

                $createWishList = $this->WishList->create($dataWishList);

                if($createWishList) {

                    $wish_list = $curUser->wish_list;

                    if($wish_list === null || $wish_list === '') {

                        $wish_list = $project_id;

                    }else{

                        $wish_list = $wish_list.','.$project_id;

                    }

                    $dataUser = array(
                        'wish_list' => $wish_list
                    );
                    $updateUser = $this->User->update($user_id,$dataUser);
                    if($updateUser) {
                        $infoWishList = $this->WishList->checkAddWishList($user_id,$project_id);

                        $data = array(
                            'id' => $createWishList,
                            'user_id' => $user_id,
                            'project_id' => $project_id,
                            'project_name' => $project->project_name,
                            'thumbnail' => $project->thumbnail,
                            'created' => $infoWishList->created,
                        );
                        $this->response($data,200);

                    }else {
                        $error = array(
                            'status' => 'add wish list failed!'
                        );
                        $this->response($error,400);
                    }
                } else {
                    $error = array(
                        'status' => 'add wish list failed!'
                    );
                    $this->response($error,400);
                }
            }
        }else {
            $error = array(
                'status' => 'error'
            );
            $this->response($error,404);
        }
    }
    /**
     * [leaveWishList_get leave wishlist in table + leave project_id on info of user]
     * @return [type] [description]
     */
    public function leaveWishList_post() {
        // Get userId to token
        $user = $this->user_data;
        $user_id =  $user->id;

        $project_id = $this->post('project_id');

        $curUser =   $this->User->getUserId($user_id);

        $project = $this->Project->getProjectId($project_id);

        if(isset($project) && $project->active === 'yes' && $project->status_fanclub === 'progress' && isset($curUser) && $curUser) {

            $checkAddWishList = $this->WishList->checkAddWishList($user_id,$project_id);

            if(isset($checkAddWishList) && $checkAddWishList ) {

                $deleteWishList = $this->WishList->delete($checkAddWishList->id);

                if($deleteWishList) {

                    $arrayWishList = explode(",",$curUser->wish_list);

                    $wish_list = array_diff($arrayWishList,array($project_id));

                    $wish_list = implode(",", $wish_list);

                    $dataUser = array(
                        'wish_list' => $wish_list
                    );
                    $updateUser = $this->User->update($user_id,$dataUser);
                    if($updateUser) {
                        $success = array(
                            'status' => 'leave wish list success!'
                        );
                        $this->response($success,200);
                    }else {
                        $error = array(
                            'status' => 'leave wish list failed!'
                        );
                        $this->response($error,400);
                    }
                } else {
                    $error = array(
                        'status' => 'leave wish list failed!'
                    );
                    $this->response($error,400);
                }
            }else {
                $error = array(
                    'status' => 'bạn chưa like project này rồi!'
                );
                $this->response($error,400);

            }
        }else {
            $error = array(
                'status' => 'error'
            );
            $this->response($error,404);
        }
    }

    /**
     * [leaveWishList_get leave wishlist in table + leave project_id on info of user]
     * @return [type] [description]
    */
    public function leaveWishListPaginate_post() {
        // Get userId to token
        $user = $this->user_data;
        $user_id =  $user->id;

        $project_id = $this->post('project_id');
        $Fistpage = $this->post('page');

        $limit = '10';
        if(!$Fistpage || $Fistpage =='0'){
          $page = '0';
        }else{
          $page = $Fistpage * $limit;
        }
        $input['limit']  = array($limit,$page);

        $curUser =   $this->User->getUserId($user_id);

        $project = $this->Project->getProjectId($project_id);

        if(isset($project) && $project && isset($curUser) && $curUser) {

            $checkAddWishList = $this->WishList->checkAddWishList($user_id,$project_id);

            if(isset($checkAddWishList) && $checkAddWishList ) {

                $deleteWishList = $this->WishList->delete($checkAddWishList->id);
                if($deleteWishList) {

                    $arrayWishList = explode(",",$curUser->wish_list);

                    $wish_list = array_diff($arrayWishList,array($project_id));

                    $wish_list = implode(",", $wish_list);

                    $dataUser = array(
                        'wish_list' => $wish_list
                    );
                    $updateUser = $this->User->update($user_id,$dataUser);
                    if($updateUser) {

                        $listWishList = $this->WishList->getWishList($user_id,$input);
                        if(isset($listWishList['project']) && $listWishList['project']){
                            $totalPages = ceil($listWishList['total'] / $limit);
                            $data = array(
                                'page_count'=>$totalPages,
                                'data'=>$listWishList['project'],
                                'status' => false
                            );
                            $this->response($data,200);
                        }else{
                            if((int)$Fistpage === 0){
                              $page = '0';
                            }else{
                              $page = ($Fistpage - 1) * $limit;
                            }
                            $input['limit']  = array($limit,$page);

                            $listWishList = $this->WishList->getWishList($user_id,$input);
                            $totalPages = ceil($listWishList['total'] / $limit);
                            if($page === '0') {
                                $data = array(
                                    'page_count'=>$totalPages,
                                    'data'=>$listWishList['project'],
                                    'status' => false
                                );
                            }else {
                                $data = array(
                                    'page_count'=>$totalPages,
                                    'data'=>$listWishList['project'],
                                    'status' => true
                                );
                            }

                            $this->response($data,200);
                        }

                    }else {
                        $error = array(
                            'status' => 'leave wish list failed!'
                        );
                        $this->response($error,400);
                    }
                } else {
                    $error = array(
                        'status' => 'leave wish list failed!'
                    );
                    $this->response($error,400);
                }
            }else {
                $error = array(
                    'status' => 'bạn chưa like project này rồi!'
                );
                $this->response($error,400);

            }
        }else {
            $error = array(
                'status' => 'error'
            );
            $this->response($error,404);
        }
    }
    /**
     * [listWishList_get get list wishlist page]
     * @return [type] [description]
     */
    public function listWishList_get(){
        // get infouser
        $user = $this->user_data;
        $user_id =  $user->id;

        $curUser = $this->User->getUserId($user_id);

        $page = $this->get('page');

        $limit = '10';
        if(!$page || $page =='0'){
          $page = '0';
        }else{
          $page = $page * $limit;
        }

        $input['limit']  = array($limit,$page);

        if(isset($curUser) && $curUser){
            $listWishList = $this->WishList->getWishList($user_id,$input);
            $totalPages = ceil($listWishList['total'] / $limit);

            $data = array(
                'page_count'=>$totalPages,
                'data'=>$listWishList['project']
            );
            $this->response($data,200);
        }

    }

    public function delProjectReturnEdit_delete($project_id,$backing_level_id){

        $user = $this->user_data;
        $user_id =  $user->id;

        $getInfo = $this->BackingLevel->getProjectBackingId($project_id,$backing_level_id);

        if($getInfo->opened === 'no' && $getInfo->active === 'no' && $user_id === $getInfo->user_id) {

            $delInfo = $this->BackingLevel->delete($backing_level_id);
            if($delInfo) {
                $success = array(
                    'status' => 'Xoa thanh cong'
                );
                $this->response($success,200);
            }else {
                $error = array(
                    'status' => 'Xoa that bai'
                );
                $this->response($error,400);
            }
        }else{
            $error = array(
                'status' => 'Xoa that bai'
            );
            $this->response($error,400);
        }
    }

    /**
     * [NotificationForProject_get get all notification relate to
     * project]
     */
    public function NotificationForProject_get(){
        // get id user
        $user = $this->user_data;
        $user_id =  $user->id;
        // get user
        $curUser =   $this->User->getUserId($user_id);

        // get id project && page
        $projectId = $this->get('project_id');
        $page = $this->get('page');
        // limit
        $limit = '5';
        if(!$page || $page =='0'){
          $page = '0';
        }else{
          $page = $page * $limit;
        }
        // get Project
        $project = $this->Project->getProjectId($projectId);
        // check project->user_id === user_id
        if( $project->user_id === $user_id
            && isset($project) && $project
            && isset($curUser) && $curUser
        ) {

            $input['limit']  = array($limit,$page);
            $Notification = $this->Notification_info->getNotificationProject($projectId,$input);
            $totalPages = ceil($Notification['total'] / $limit);
            //
            $data = array(
                'page_count'=>$totalPages,
                'data'=>$Notification['project']
            );
            $this->response($data, 200);
        }else {
            $error = array(
                'status' => 'error!'
            );
            $this->response($error, 404);
        }
    }

    public function addAccess_post() {
        // Get userId to token
        $user = $this->user_data;
        $user_id =  $user->id;

        $project_id = $this->post('project_id');

        $curUser =   $this->User->getUserId($user_id);

        $project = $this->Project->getProjectId($project_id);

        $checkAddWishList = $this->AccessList->deleteProjectUser($user_id,$project_id);
        $datetime = new DateTime();
        // format date
        $datetime = $datetime->format('Y-m-d H:i:s');
        // change time current -> time UTC
        $asia_timestamp = strtotime($datetime);
        date_default_timezone_set('UTC');
        $date = date("Y-m-d H:i:s", $asia_timestamp);

        $dataAcessList = array(
            'user_id' => $user_id,
            'project_id' => $project_id,
            'created' => $date
        );

        $createWishList = $this->AccessList->create($dataAcessList);
        $resutlStatus = array(
            'status' => '1',
            'message' => 'Ok'
        );
        $this->response($resutlStatus,200);
//            $error = array(
//                'status' => 'error'
//            );
//            $this->response($error,404);


    }
}
