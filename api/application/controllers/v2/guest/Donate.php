<?php

defined('BASEPATH') OR exit('No direct script access allowed');
use \Firebase\JWT\JWT;

class Donate extends BD_Controller {
    function __construct()
    {
        // Construct the parent class
        parent::__construct();
    }

    public function checkNumberStripe_post($numberStripe,$cvc){
        $kunci = $this->config->item('thekey');

        $count_length = strlen($numberStripe);
        $count_length = $count_length - 4;

        $last4 = substr($numberStripe,$count_length);

        $getBlackList = $this->BlackList->getCard($last4);
        // var_dump($getBlackList);
        $status = 0;
        if(isset($getBlackList) && $getBlackList) {

            foreach ($getBlackList as $key => $value) {

                $token = $value->hash_token;

                try {
                    $decoded = JWT::decode($token, $kunci, array('HS256'));
                    $result = strpos($decoded, "/") + 1;
                    $checkcvc = substr($decoded, $result);

                    if((int)$cvc !== (int)$checkcvc){
                        $status = $status + 0;
                    }else{
                        $status = $status + 1;
                    }
                } catch (Exception $e) {
                    $status = $status + 0;
                }
            }

            if($status > 0){
                return false;
            }else {
                return true;
            }
        }else {
            return true;
        }

    }

    public function donateStripe_post() {
        $roomSocket = $this->config->item('roomSocket');
        $portRoomSocket = $this->config->item('portRoomSocket');
        $key_firebase = $this->config->item('key_firebase');
        $kunci = $this->config->item('thekey');

        $email = $this->post('email');
        $name = $this->post('name');
        $customerId = $this->post('customerId');
        $project_id = $this->post('project_id');
        $backing_level_id = $this->post('backing_level_id');


        $project = $this->Project->getProjectAndUserById($project_id);
        $backing_level= $this->BackingLevel->getLevelId($backing_level_id);
        $date = new Datetime();
        $date = $date->format('Y-m-d');

        if(
            isset($project)
            && isset($backing_level)
            && $project->id === $backing_level->project_id
            && $project->project_type === '0'
            && $project->collection_end_date > $date
        ) {

            $projectName = $project->project_name;
            $emailOwner = $project->email;
            $nameOwner = $project->username;
            $invest_amount = $backing_level->invest_amount;
            $now_countBacking = $backing_level->now_count;
            $collected_amount = $project->collected_amount;
            $now_countProject = $project->now_count;
            $returnName = $backing_level->name;

            $comment = "guest は".$projectName."(".$project_id.")の".$backing_level_id."に支援しました。";

            if($customerId) {

                //item information
                $itemName = $project_id;
                $itemNumber = $backing_level_id;
                $itemPrice = $invest_amount;
                $currency = "JPY";

                try {
                    require_once APPPATH."third_party/stripe/init.php";
                    $key = $this->Setting->getSetting();
                    \Stripe\Stripe::setApiKey($key->secret_key);

                    $charge = \Stripe\Charge::create(array(
                        'customer' => $customerId,
                        'amount'   => $itemPrice,
                        'currency' => $currency,
                        'description' => 'Donate for project '.$projectName,
                        'metadata' => array(
                        'item_id' => $itemNumber
                        )
                    ));
                    //retrieve charge details
                    $chargeJson = $charge->jsonSerialize();

                    //check whether the charge is successful
                    if($chargeJson['amount_refunded'] == 0 && empty($chargeJson['failure_code']) && $chargeJson['paid'] == 1 && $chargeJson['captured'] == 1)
                    {
                        //order details
                        $amount = $chargeJson['amount'];
                        $balance_transaction = $chargeJson['balance_transaction'];
                        $currency = $chargeJson['currency'];
                        $status = $chargeJson['status'];
                        $date = date("Y-m-d H:i:s");

                        $dataDB = array(
                            'project_id' => $project_id,
                            // 'user_id' => $user_id,
                            // 'id_address' => $addressDefault->id,
                            // 'postcode'  =>  $addressDefault->postcode,
                            // 'address_return'  =>  $addressDefault->address_return,
                            // 'phone'  =>  $addressDefault->phone,
                            'email'  =>  $email,
                            'name_return'  =>  $name,
                            'backing_level_id' => $backing_level_id,
                            'invest_amount' => $invest_amount,
                            'stripe_charge_id' => $charge->id,
                            'type'=>'Credit',
                            'comment' => $comment,
                            'status' => $status,
                            'created' => $date,
                            'manual_flag' => '0'
                        );

                        if ($this->db->insert('backed_projects', $dataDB)) {
                            if($this->db->insert_id() && $status == 'succeeded'){

                                $dataUpdate = array(
                                    'now_count'=>$now_countBacking + 1,
                                );
                                $this->BackingLevel->update($backing_level_id,$dataUpdate);

                                $dataUpdateProject = array(
                                    'now_count'=>$now_countProject + 1,
                                    'collected_amount'=>$collected_amount + $itemPrice,
                                );
                                $updateProject = $this->Project->update($project_id,$dataUpdateProject);

                                // create Datetime
                                $datetime = new DateTime();
                                $datetime = $datetime->format('Y-m-d H:i:s');
                                // get strtotime + change time zone
                                $asia_timestamp = strtotime($datetime);
                                date_default_timezone_set('UTC');
                                // get Date vs time zone
                                $date = date("Y-m-d H:i:s", $asia_timestamp);

                                // get data for socket + create notification_info + firebase
                                $userDonate = $name;
                                $investAmountDonate = $invest_amount;
                                $imageUserDonate = base_url('').'images/2018/default/logo-icon.png';
                                $projectId = $project_id;

                                // array create notification info
                                $dataNotification = array (
                                    'user_id' => $project->user_id,
                                    'user_action' => $userDonate,
                                    'title' => '支援があります',
                                    'message' => ''.$userDonate.' さんが 「'.$projectName.'」に'.number_format($investAmountDonate).'円支援しました',
                                    'link'=> "my-page/statistic/".$projectId,
                                    'status' => 0,
                                    'thumbnail' => $imageUserDonate,
                                    'created' => $date
                                );
                                // create notification info
                                $createNotification = $this->Notification_info->create($dataNotification);
                                // check exists
                                if($createNotification) {
                                    // use SocketIO annouce for user
                                    require("public/assets/socket/socket.io.php");
                                    $socketio = new SocketIO();
                                    // // array
                                    $data = array(
                                        'room' => 'project_'.$projectId,
                                        'title' => '支援があります',
                                        'message' => $userDonate."さんが「".$projectName."」に".number_format($investAmountDonate)."円支援しました",
                                        'url'=> app_url()."my-page/statistic/".$projectId,
                                        'ava' => $imageUserDonate
                                    );
                                    $socketio->send($roomSocket, $portRoomSocket, 'donate_project',json_encode($data));

                                    $curl = curl_init();

                                    curl_setopt_array($curl, array(
                                      CURLOPT_URL => "https://fcm.googleapis.com/fcm/send",
                                      CURLOPT_RETURNTRANSFER => true,
                                      CURLOPT_ENCODING => "",
                                      CURLOPT_MAXREDIRS => 10,
                                      CURLOPT_TIMEOUT => 30,
                                      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                                      CURLOPT_CUSTOMREQUEST => "POST",
                                      CURLOPT_POSTFIELDS => "{   \"notification\": {\r\n        \"title\": \"支援があります\",\r\n        \"body\": \"".$userDonate."さんが「".$projectName."」」に ".number_format($investAmountDonate)."円支援しました\",\r\n        \"click_action\": \"".app_url()."my-page/statistic/".$projectId."\",\r\n        \"icon\": \"".base_url()."images/2018/default/logo-icon.png\"\r\n    },\r\n  \"to\" : \"/topics/project_".$projectId."\"\r\n}",
                                      CURLOPT_HTTPHEADER => array(
                                        "Authorization: key=".$key_firebase,
                                        "Content-Type: application/json",
                                      ),
                                    ));

                                    $response = curl_exec($curl);
                                    $err = curl_error($curl);

                                    curl_close($curl);

                                    $this->load->library('email');

                                    $this->email->from('admin@kakuseida.conoha.io', 'KAKUSEIDA');
                                    $this->email->to($email);
                                    // $this->email->reply_to('admin@kakuseida.conoha.io', 'KAKUSEIDA');
                                    $this->email->subject('「'.$projectName.'」プロジェクトにご支援いただきありがとうございます。');
                                    $this->email->message(
                                    $email.'様<br>
                                    KAKUSEIDA運営事務局です。<br>
                                    このたびは、「'.$projectName.'」プロジェクトにご支援いただきありがとうございます。<br>
                                    '.$email.'様が選択されたリターン品の名前は'.$returnName.'です。<br>
                                    プロジェクト企画者に登録したメールアドレス情報が送付されました。<br>
                                    リターンの詳細、送付に関しましては企画者からの連絡をお待ちください。<br>
                                    今後ともKAKUSEIDAをよろしくお願い致します。<br>');
                                    $this->email->send();


                                    $this->email->from('admin@kakuseida.conoha.io', 'KAKUSEIDA');
                                    $this->email->to($emailOwner);
                                    // $this->email->reply_to('admin@kakuseida.conoha.io', 'KAKUSEIDA');
                                    $this->email->subject($name.'様が企画した「'.$projectName.'」プロジェクトに、新規支援がありました。');
                                    $this->email->message(
                                    $nameOwner.'様<br>
                                    KAKUSEIDA運営事務局です。<br>
                                    '.$name.'様が企画した「'.$projectName.'」プロジェクトに、新規支援がありました。<br>
                                    選択されたリターン品の名前は'.$returnName.'です。<br>
                                    支援者は会員登録せずに支援してくださいました。<br>
                                    支援者のメールアドレスは'.$email.'です。<br>
                                    リターンに関しましては、個別に連絡して対応してください。<br>
                                    また、プロジェクト画面にログインして詳細を確認してください。<br>
                                    <a href="'.app_url().'project-detail/'.$project_id.'">'.app_url().'project-detail/'.$project_id.'</a><br />');
                                    $this->email->send();


                                    // Session send message for admin
                                }

                                $success = array(
                                    'status'=>'Donate Success!'
                                );
                                $this->response($success,200);
                            }
                        }
                    }
                } catch(Stripe_CardError $e) {
                    $error1 = $e->getMessage();
                    $error = array(
                        'status'=>$error1);
                        $this->response($error,400);
                } catch (Stripe_InvalidRequestError $e) {
                    // Invalid parameters were supplied to Stripe's API
                    $error2 = $e->getMessage();
                    $error = array(
                        'status'=>$error2);
                        $this->response($error,400);
                } catch (Stripe_AuthenticationError $e) {
                    // Authentication with Stripe's API failed
                    $error3 = $e->getMessage();
                    $error = array(
                        'status'=>$error3);
                        $this->response($error,400);
                } catch (Stripe_ApiConnectionError $e) {
                    // Network communication with Stripe failed
                    $error4 = $e->getMessage();
                    $error = array(
                        'status'=>$error4
                    );
                    $this->response($error,400);
                } catch (Stripe_Error $e) {
                    // Display a very generic error to the user, and maybe send
                    // yourself an email
                    $error5 = $e->getMessage();
                    $error = array(
                    'status'=>$error5);
                    $this->response($error,400);
                } catch (Exception $e) {
                    // Something else happened, completely unrelated to Stripe
                    $error6 = $e->getMessage();
                    $error = array(
                        'status'=>$error6);
                    $this->response($error,400);
                }
            }

        }else {
            $error = array(
                'status' => 'Not Found!'
            );
            $this->response($error,404);
        }
    }

    public function createInfoCard_post() {
        $dataBase64 = $this->post('data');
        $Hash_Data  = Hash_Data($dataBase64);

        $email      = $Hash_Data['email'];
        $name       = $Hash_Data['name'];
        $numberCard = $Hash_Data['number_card'];
        $exp_month  = $Hash_Data['exp_month'];
        $exp_year   = $Hash_Data['exp_year'];
        $cvc        = $Hash_Data['cvc'];
        $project_id = $Hash_Data['project_id'];
        $backing_level_id = $Hash_Data['backing_level_id'];

        $project = $this->Project->getProjectAndUserById($project_id);
        $backing_level = $this->BackingLevel->getLevelId($backing_level_id);
        $date = new Datetime();
        $date = $date->format('Y-m-d');

        if(
            isset($project)
            && isset($backing_level)
            && $project->id === $backing_level->project_id
            && $project->project_type === '0'
            && $project->collection_end_date > $date
        ) {

            $projectName = $project->project_name;
            $emailOwner = $project->email;
            $nameOwner = $project->username;
            $invest_amount = $backing_level->invest_amount;
            $now_countBacking = $backing_level->now_count;
            $collected_amount = $project->collected_amount;
            $now_countProject = $project->now_count;

            $comment = "guest は".$projectName."(".$project_id.")の".$backing_level_id."に支援しました。";

            if(
                isset($numberCard)
                && isset($exp_month)
                && isset($exp_year)
                && isset($cvc)
            ) {

                require_once APPPATH."third_party/stripe/init.php";
                $key = $this->Setting->getSetting();
                \Stripe\Stripe::setApiKey($key->secret_key);


                try {

                    if(isset($cvc) && $cvc){

                        $stripeToken = \Stripe\Token::create(array(
                            "card" => [
                                "number" => $numberCard,
                                "exp_month" => $exp_month,
                                "exp_year" => $exp_year,
                                "cvc" => $cvc
                            ]
                        ));

                        if(isset($stripeToken->id)){

                            $checkBlackList = $this->checkNumberStripe_post($numberCard,$cvc);
                            if(!$checkBlackList){
                                $error = array(
                                    "status" => "このクレジットカード情報はブロックされたユーザーのですので、別のクレジットカード情報を利用してください"
                                );
                                $this->response($error,400);
                            }

                            try {

                                $customer = \Stripe\Customer::create(array(
                                    'email' => $email,
                                    'source'  => $stripeToken->id
                                ));

                                $success = array(
                                    'customerId' => $customer->id
                                );

                                $this->response($success,200);
                            } catch(Stripe_CardError $e) {
                                $error1 = $e->getMessage();
                                $error = array(
                                    'status'=>$error1
                                );
                                $this->response($error,400);

                            } catch (Stripe_InvalidRequestError $e) {
                                // Invalid parameters were supplied to Stripe's API
                                $error2 = $e->getMessage();
                                $error = array(
                                    'status'=>$error2
                                );
                                $this->response($error,400);

                            } catch (Stripe_AuthenticationError $e) {
                                // Authentication with Stripe's API failed
                                $error3 = $e->getMessage();
                                $error = array(
                                    'status'=>$error3
                                );
                                $this->response($error,400);

                            } catch (Stripe_ApiConnectionError $e) {
                                // Network communication with Stripe failed
                                $error4 = $e->getMessage();
                                $error = array(
                                    'status'=>$error4
                                );
                                $this->response($error,400);

                            } catch (Stripe_Error $e) {
                                // Display a very generic error to the user, and maybe send
                                // yourself an email
                                $error5 = $e->getMessage();
                                $error = array(
                                    'status'=>$error5
                                );
                                $this->response($error,400);

                            } catch (Exception $e) {
                                // Something else happened, completely unrelated to Stripe
                                $errrr = json_decode($e->getHttpBody());
                                if($errrr->error->code === "token_already_used"){
                                    $error = array(
                                        'status'=>'このクレジットカード情報は存在しました。'
                                    );
                                }else if($errrr->error->code === "email_invalid"){
                                    $error = array(
                                        'status'=>'メールアドレスを正しく入力してください'
                                    );
                                }
                                $this->response($error,400);
                            }
                        }

                    }else{
                        $error = array(
                            'status'=>'CVCを記入してください。'
                        );
                        $this->response($error,400);
                    }

                } catch(Stripe_CardError $e) {
                    $error1 = $e->getMessage();
                    $error = array(
                        'status'=>$error1
                    );
                    $this->response($error,400);

                } catch (Stripe_InvalidRequestError $e) {
                    // Invalid parameters were supplied to Stripe's API
                    $error2 = $e->getMessage();
                    $error = array(
                        'status'=>$error2
                    );
                    $this->response($error,400);

                } catch (Stripe_AuthenticationError $e) {
                    // Authentication with Stripe's API failed
                    $error3 = $e->getMessage();
                    $error = array(
                        'status'=>$error3
                    );
                    $this->response($error,400);

                } catch (Stripe_ApiConnectionError $e) {
                    // Network communication with Stripe failed
                    $error4 = $e->getMessage();
                    $error = array(
                        'status'=>$error4
                    );
                    $this->response($error,400);

                } catch (Stripe_Error $e) {
                    // Display a very generic error to the user, and maybe send
                    // yourself an email
                    $error5 = $e->getMessage();
                    $error = array(
                        'status'=>$error5
                    );
                    $this->response($error,400);

                } catch (Exception $e) {
                    // Something else happened, completely unrelated to Stripe
                    $errrr = json_decode($e->getHttpBody());
                    if($errrr->error->code === "invalid_expiry_month"){
                        $error = array(
                            'status'=>'有効期限の年を正しく入力してください'
                        );
                    }else if($errrr->error->code === "incorrect_number"){
                        $error = array(
                            'status'=>'カード番号は正しくありません。'
                        );
                    }else if($errrr->error->code === "invalid_expiry_year"){
                        $error = array(
                            'status'=>'有効期限の年を正しく入力してください'
                        );
                    }else if($errrr->error->code === "invalid_cvc"){
                        $error = array(
                            'status'=>'CVCを正しく入力してください'
                        );
                    }else if($errrr->error->code === "expired_card"){
                        $error = array(
                            'status'=>'有効期限の年を正しく入力してください'
                        );
                    }else if($errrr->error->code === "incorrect_cvc"){
                        $error = array(
                            'status'=>'CVCを正しく入力してください'
                        );
                    }
                    $this->response($error,400);
                }
            }

        }else {
            $error = array(
                'status' => 'Not Found!'
            );
            $this->response($error,404);
        }
    }

}
