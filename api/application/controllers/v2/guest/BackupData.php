<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class BackupData extends BD_Controller {
    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->load->helper('file');
    }

    public function backup_get() {
        $mysqlUserName      = "root";
        $mysqlPassword      = "";
        $mysqlHostName      = "localhost";
        $DbName             = "kakuseida-test";
        $backup_name        = "mybackup.sql";
        $tables             = "Your tables";

        //or add 5th parameter(array) of specific tables:    array("mytable1","mytable2","mytable3") for multiple tables

        $backup= $this->Export_Database($mysqlHostName,$mysqlUserName,$mysqlPassword,$DbName,  $tables=false, $backup_name=false );
        $db_name = 'backup-on-'. date("Y-m-d-H-i-s") .'.sql';
        $save = 'public/assets/backup_data/'.$db_name;
        // write_file($save, $backup);
        if(write_file($save, $backup)){
            $this->testKakuBackup_get();
        }

    }

    function Export_Database($host,$user,$pass,$name,  $tables=false, $backup_name=false ) {
        $mysqli = new mysqli($host,$user,$pass,$name);
        $mysqli->select_db($name);
        $mysqli->query("SET NAMES 'utf8'");

        $queryTables    = $mysqli->query('SHOW TABLES');
        while($row = $queryTables->fetch_row())
        {
            $target_tables[] = $row[0];
        }
        if($tables !== false)
        {
            $target_tables = array_intersect( $target_tables, $tables);
        }
        foreach($target_tables as $table)
        {
            $result         =   $mysqli->query('SELECT * FROM '.$table);
            $fields_amount  =   $result->field_count;
            $rows_num=$mysqli->affected_rows;
            $res            =   $mysqli->query('SHOW CREATE TABLE '.$table);
            $TableMLine     =   $res->fetch_row();
            $content        = (!isset($content) ?  '' : $content) . "\n\n".$TableMLine[1].";\n\n";

            for ($i = 0, $st_counter = 0; $i < $fields_amount;   $i++, $st_counter=0)
            {
                while($row = $result->fetch_row())
                { //when started (and every after 100 command cycle):
                    if ($st_counter%100 == 0 || $st_counter == 0 )
                    {
                            $content .= "\nINSERT INTO ".$table." VALUES";
                    }
                    $content .= "\n(";
                    for($j=0; $j<$fields_amount; $j++)
                    {
                        $row[$j] = str_replace("\n","\\n", addslashes($row[$j]) );
                        if (isset($row[$j]))
                        {
                            $content .= '"'.$row[$j].'"' ;
                        }
                        else
                        {
                            $content .= '""';
                        }
                        if ($j<($fields_amount-1))
                        {
                                $content.= ',';
                        }
                    }
                    $content .=")";
                    //every after 100 command cycle [or at last line] ....p.s. but should be inserted 1 cycle eariler
                    if ( (($st_counter+1)%100==0 && $st_counter!=0) || $st_counter+1==$rows_num)
                    {
                        $content .= ";";
                    }
                    else
                    {
                        $content .= ",";
                    }
                    $st_counter=$st_counter+1;
                }
            } $content .="\n\n\n";
        }
        //$backup_name = $backup_name ? $backup_name : $name."___(".date('H-i-s')."_".date('d-m-Y').")__rand".rand(1,11111111).".sql";
        $backup_name = $backup_name ? $backup_name : $name.".sql";
        header('Content-Type: application/octet-stream');
        header("Content-Transfer-Encoding: Binary");
        header("Content-disposition: attachment; filename=\"".$backup_name."\"");
        return $content;
    }

    public function testKakuBackup_get(){
        $url_array = explode('?', 'http://'.$_SERVER ['HTTP_HOST'].$_SERVER['REQUEST_URI']);
        $url = $url_array[0];

        require_once 'public/assets/google-api-php-client/src/Google_Client.php';
        require_once 'public/assets/google-api-php-client/src/contrib/Google_DriveService.php';
        $client = new Google_Client();
        $client->setClientId('74226131022-9qu88k25ioopkg446ikk82tumb77gvg8.apps.googleusercontent.com');
        $client->setClientSecret('sXrE1P_B3PRKPAUtIBbdPClG');
        $client->setRedirectUri($url);
        $client->setScopes(array('https://www.googleapis.com/auth/drive'));

        if (isset($_GET['code'])) {
            $_SESSION['accessToken'] = $client->authenticate($_GET['code']);
            header('location:'.$url);exit;
        } elseif (!isset($_SESSION['accessToken'])) {

            $client->authenticate();
        }
        $files= array();
        $dir = dir('public/assets/backup_data');
        while ($file = $dir->read()) {
            if ($file != '.' && $file != '..') {
                $files[] = $file;
            }
        }
        $dir->close();


        $client->setAccessToken($_SESSION['accessToken']);
        $service = new Google_DriveService($client);
        $finfo = finfo_open(FILEINFO_MIME_TYPE);
        $file = new Google_DriveFile();
        foreach ($files as $file_name) {
            $file_path = 'public/assets/backup_data/'.$file_name;
            $mime_type = finfo_file($finfo, $file_path);
            $file->setTitle($file_name);
            $file->setDescription('This is a '.$mime_type.' document');
            $file->setMimeType($mime_type);
            $service->files->insert(
                $file,
                array(
                    'data' => file_get_contents($file_path),
                    'mimeType' => $mime_type
                )
            );
        }
        finfo_close($finfo);

        if (file_exists('public/assets/backup_data/')) {
            $files = glob('public/assets/backup_data/*'); // get all file names
            foreach($files as $file){ // iterate files
              if(is_file($file))
                unlink($file); // delete file
            }
        }
    }

}
