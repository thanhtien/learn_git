<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class LoadComment extends BD_Controller {
    function __construct()
    {
        // Construct the parent class
        parent::__construct();
    }
    /**
     * [createComment_post Create Comment]
     * @return [type] [description]
     */
     public function loadComment_get(){
        $project_id = $this->get('project_id');
        if(isset($project_id) && $project_id){
        $project = $this->Project->ProjectId($project_id);
        if(isset($project) && $project){
         $start = $this->get('page');
         $limit = '5';
         if(!$start || $start =='0'){
           $start ='0';
         }else{
           $start = $start*$limit;
         }
         $NumberComment = $this->Comment_model->getComment($project_id);
         $totalComments = count($NumberComment);
         $totalPages = ceil($totalComments / $limit);

         $input['limit']  = array($limit,$start);
         function date_compare($a, $b)
         {
             $t1 = strtotime($a->created);
             $t2 = strtotime($b->created);
             return $t1 - $t2;
         }

         $comment = $this->Comment_model->getComment($project_id,$input);

         foreach ($comment as $key => $value) {

             $reply = $this->Reply_Comment_model->getReplyComment($value->id);
             $totalReply = $this->Reply_Comment_model->getReplyComment($value->id,1);

             if(count($totalReply) > 3){
                 $value->getMoreReply = true;
             }else{
                 $value->getMoreReply = false;
             }

             usort($reply, 'date_compare');
             if(isset($reply) && $reply){
                 foreach ($reply as $key => $valueReply) {
                     $user = $this->User->getUserId($valueReply->user_id);

                     $valueReply->username = $user->username;
                     $valueReply->profileImageURL = $user->profileImageURL;

                     $date1 = new DateTime($valueReply->created);
                     $date2 = new DateTime(date('Y-m-d H:i:s'));
                       $time = $date1->diff($date2);

                       $date = date_diff($date1,$date2);
                       $date = $date->format("%a");
                       $format_collection_end_date = array(
                         'date' =>$date,
                         'hour' => $time->h,
                         'minutes' => $time->i,
                         'second' => $time->s
                       );
                     $valueReply->time = $format_collection_end_date;
                 }
             }
             $user = $this->User->getUserId($value->user_id);

             $value->username = $user->username;
             $value->profileImageURL = $user->profileImageURL;

             $date1 = new DateTime($value->created);
             $date2 = new DateTime(date('Y-m-d H:i:s'));
               $time = $date1->diff($date2);

               $date = date_diff($date1,$date2);
               $date = $date->format("%a");
               $format_collection_end_date = array(
                 'date' =>$date,
                 'hour' => $time->h,
                 'minutes' => $time->i,
                 'second' => $time->s
               );
             $value->time = $format_collection_end_date;
             $value->reply = $reply;

         }
         usort($comment, 'date_compare');
         $data = array(
             'page_count'=>$totalPages,
             'data'=>$comment
         );
         $this->response($data,200);
     }else{
         $error = array(
             'status' => 'error not found!'
         );
         $this->response($error,404);
     }
     }else{
         $error = array(
             'status' => 'error not found!'
         );
         $this->response($error,404);
     }
    }

    public function loadMoreReply_get(){
         $comment_id = $this->get('comment_id');

         $project_id = $this->get('project_id');
         $project = $this->Project->ProjectId($project_id);
         $more = true;
         $reply = $this->Reply_Comment_model->getReplyComment($comment_id,$more);
         function date_compare($a, $b)
         {
             $t1 = strtotime($a->created);
             $t2 = strtotime($b->created);
             return $t1 - $t2;
         }
         usort($reply, 'date_compare');
         if(isset($reply) && $reply){
             foreach ($reply as $key => $valueReply) {
                 $user = $this->User->getUserId($valueReply->user_id);

                 $valueReply->username = $user->username;

                 $valueReply->profileImageURL = $user->profileImageURL;

                 $date1 = new DateTime($valueReply->created);
                 $date2 = new DateTime(date('Y-m-d H:i:s'));
                   $time = $date1->diff($date2);

                   $date = date_diff($date1,$date2);
                   $date = $date->format("%a");
                   $format_collection_end_date = array(
                     'date' =>$date,
                     'hour' => $time->h,
                     'minutes' => $time->i,
                     'second' => $time->s
                   );
                 $valueReply->time = $format_collection_end_date;
             }
         }
         $data = array(
             'comment_id'    =>  $comment_id,
             'reply' =>  $reply
         );
         $this->response($data,200);
     }
}
