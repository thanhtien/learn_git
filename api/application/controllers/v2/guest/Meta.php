<?php

defined('BASEPATH') OR exit('No direct script access allowed');
use \Firebase\JWT\JWT;
class Meta extends BD_Controller {
    function __construct() {
        // Construct the parent class
        parent::__construct();
    }

    public function changeStripeUser_post() {
        $user_id = $this->post('id');
        $kunci = $this->config->item('thekey');
        var_dump($kunci);
        $curUser =   $this->User->getUserId($user_id);
        require_once APPPATH."third_party/stripe/init.php";
        $key = $this->Setting->getSetting();
        \Stripe\Stripe::setApiKey($key->secret_key);
        if($curUser) {
            $customerId = $curUser->customer_id;
            $cvc = $curUser->cvc;
            $code = $customerId.'/'.$cvc;
            $output = JWT::encode($code, $kunci);
            $data['hash_token'] = $output;
            $edit = $this->User->editUser($user_id,$data);
            var_dump($edit);
            var_dump($data);
        }
    }

    public function checkStripeToken_post(){
        $user_id = $this->post('id');
        $kunci = $this->config->item('thekey');
        var_dump($kunci);
        $curUser =   $this->User->getUserId($user_id);
        require_once APPPATH."third_party/stripe/init.php";
        $key = $this->Setting->getSetting();
        \Stripe\Stripe::setApiKey($key->secret_key);
        if($curUser) {
            $token = $curUser->hash_token;
            $cvc = $curUser->cvc;
            try {

                $decoded = JWT::decode($token, $kunci, array('HS256'));
                $result = strpos($decoded, "/") + 1;
                $checkcvc = substr($decoded, $result);
                if((int)$cvc !== (int)$checkcvc){
                    $error = array(
                        "status" => "セキュリティーコード（CVC)は間違っていますので、ご確認下さい。"
                    );
                    $this->response($error,400);
                }
                $this->response($checkcvc,400);

            } catch (Exception $e) {
                $invalid = ['status' => $e->getMessage()];
                $this->response($invalid, 400);
            }
        }
    }

    public function projectDetail_get($id){
        if(isset($id) && $id){

            $project = $this->Project->getProjectId($id);

            if(isset($project) && $project){
                $dataSafe = array(
                    'title' => $project->project_name,
                    'description' => $project->description,
                    'thumbnail' => $project->thumbnail
                );
                $this->response($dataSafe,200);
            }else{
                $error = array(
                    'status' => 'Project not found!'
                );
                $this->response($error,404);
            }

        }else{
            $error = array(
                'status' => 'Id not found!'
            );
            $this->response($error,404);
        }
    }

    public function newDetail_get($id){
        if(isset($id) && $id){

            $new = $this->News->getNewId($id);

            if(isset($new) && $new){
                $dataSafe = array(
                    'title' => $new->title,
                    'description' => $new->des,
                    'thumbnail' => $new->thumnail
                );
                $this->response($dataSafe,200);
            }else{
                $error = array(
                    'status' => 'Project not found!'
                );
                $this->response($error,404);
            }

        }else{
            $error = array(
                'status' => 'Id not found!'
            );
            $this->response($error,404);
        }
    }
}
