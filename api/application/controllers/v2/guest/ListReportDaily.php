<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class ListReportDaily extends BD_Controller {
    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        // $this->auth();
    }
    // unsubscribe notification when logout or expired token
    /**
     *
     */
    public function listReport_get() {
        $projectId = $this->get('project_id');
        if(isset($projectId) && $projectId){
            // get data report limit 5
            $reportDaily = $this->Report->listReport2($projectId);
            // count all report daily
            $numberReport = $this->Report->countReportDaily2($projectId);
            // assign limit = 5
            $limit = 5;
            $totalPages = ceil($numberReport / $limit);
            // create array
            $data = array(
                'totalPage' => $totalPages,
                'report' => $reportDaily
            );
            //
            $this->response($data,200);
        }else{
            $error = array(
                'status' => 'error!'
            );
            $this->response($error,404);
        }
    }

    public function loadMoreListReport_get() {
        $projectId = $this->get('project_id');
        $page = $this->get('page');

        $limit = '5';
        if(!$page || $page =='0'){
          $pagelimit ='0';
        }else{
          $pagelimit = $page*$limit;
        }
        if(isset($projectId) && $projectId){

            $reportDaily = $this->Report->listReport2($projectId,$limit,$pagelimit);


            $numberReport = $this->Report->countReportDaily2($projectId);

            $totalPages = ceil($numberReport / $limit);
            $data = array(
                'totalPage' => $totalPages,
                'report' => $reportDaily
            );
            $this->response($data,200);
        }else{
            $error = array(
                'status' => 'Not found report!'
            );
            $this->response($error,404);
        }
    }

    public function ReportDetail_get(){
        $reportId = $this->get('id');

        if(isset($reportId) && $reportId){
            $report = $this->Report->getReportByDetailId($reportId);
            if(isset($report) && $report){
                $this->response($report,200);
            }else{
                $error = array(
                    'status' => 'Not found report!'
                );
                $this->response($error,404);
            }
        }else{
            $error = array(
                'status' => 'Not found report!'
            );
            $this->response($error,404);
        }
    }


    public function ReportDailyDetail_get(){
        $reportId = $this->get('id');

        if(isset($reportId) && $reportId){
            $report = $this->Report->getReportByDetailId($reportId);
            if(isset($report) && $report){
                $this->response($report,200);
            }else{
                $error = array(
                    'status' => 'Not found report!'
                );
                $this->response($error,404);
            }
        }else{
            $error = array(
                'status' => 'Not found report!'
            );
            $this->response($error,404);
        }
    }

}
