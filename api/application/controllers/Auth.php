<?php

defined('BASEPATH') or exit('No direct script access allowed');
use \Firebase\JWT\JWT;
use Abraham\TwitterOAuth\TwitterOAuth;

class Auth extends BD_Controller
{
    public function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->load->helper('email');
        $this->load->library(array('ion_auth', 'form_validation'));
    }
    public function Login_post()
    {
        $kunci = $this->config->item('thekey');
        $data = $this->post('data');
        $Data = Hash_Data($data);
        $email = $Data['email'];
        $password = $Data['password'];
        if ($this->ion_auth->login($email,$password)) {
            $group = $this->ion_auth->get_users_groups()->row();
            $user = $this->ion_auth->user()->row();
            $numberProject = $this->Project->getProjectUserId($user->id);
            $numberPatron = $this->BackedProject->countBackedProject($user->id);
            $token['id'] = $user->id;  //From here$g
            $token['role'] = $group->id;  //From here
            $token['username'] = $this->post('email');
            $token['name'] = $user->username;
            $date = new DateTime();
            $token['iat'] = $date->getTimestamp();
            $token['exp'] = $date->getTimestamp() + 60*60*24*30*3; //To here is to generate token
            $output['token'] = JWT::encode($token, $kunci); //This is the output token
            $output['socket_uuid'] = 'EVENT_'.$user->id;
            $output['profile'] = array(
                "username"=>$user->username,
                "roles"=>$group->name,
                'profileImageURL' =>$user->profileImageURL,
                'created' =>$user->created_on,
                'sex' =>$user->sex,
                'birthday' =>$user->birthday,
                'self_description' =>$user->self_description,
                'url1' =>$user->url1,
                'url2' =>$user->url2,
                'url3' =>$user->url3,
                'address'=>$user->address,
                'job'=>$user->job,
                'country'=>$user->country,
                'phone'=>$user->phone,
                'postcode'=>$user->postcode,
                'address_return'=>$user->address_return,
                'numberProject'=>count($numberProject),
                'patron'=>count($numberPatron),
            );
            $this->response($output, REST_Controller::HTTP_OK); //This is the respon if success
        } else {
            $checkUSer = $this->User->checkUser($email);
            if(isset($checkUSer) && $checkUSer && $checkUSer->active === '0'){
                $invalidLogin = ['status' => '総合的な理由により、アカウントを停止されました。'];
            }else{
                $invalidLogin = ['status' => 'ユーザー名かパスワードが正しくない'];
            }

            $this->response($invalidLogin, REST_Controller::HTTP_BAD_REQUEST); //This is the respon if failed
        }
    }

    public function loginFacebook_post() {
        $app_id = $this->config->item('appIdFacebook');
        $app_secret = $this->config->item('appSecretFacebook');
        $accessToken = $this->post('accessToken');
        // require_once 'C:/wamp/www/facebook/vendor/autoload.php'; // change path as needed
        require FCPATH . 'vendor/autoload.php';
        $fb = new \Facebook\Facebook([
            'app_id' => $app_id,
            'app_secret' => $app_secret,
            'default_graph_version' => 'v2.10',
        ]);
        try {
            $response = $fb->get('/me?fields=id,name,gender,email,picture.width(240).height(240),cover', $accessToken);
        } catch (\Facebook\Exceptions\FacebookResponseException $e) {
            // When Graph returns an error
            $this->response($e->getMessage(), 400);
        } catch (\Facebook\Exceptions\FacebookSDKException $e) {
            // When validation fails or other local issues
            $this->response($e->getMessage(), 400);
        }

        $infoFacebook = $response->getGraphNode();
        $idFacebook = $infoFacebook['id'];
        $userNameFacebook = $infoFacebook['name'];
        $nameFacebook = replaceName($userNameFacebook);
        $profileImageURL = $infoFacebook['picture']['url'];

        $checkFacebookId = $this->User->checkIdFacebook($idFacebook);
        if(isset($checkFacebookId->email)){
            $checkSubscription = $this->Subscription->getDataEmail($checkFacebookId->email);
        }else{
            $checkSubscription = false;
        }

        $kunci = $this->config->item('thekey');

        if(isset($checkFacebookId) && $checkFacebookId && isset($checkSubscription) && $checkSubscription){
            if($checkFacebookId->active === '0'){
                $invalidLogin = array(
                    'status' => '総合的な理由により、アカウントを停止されました。'
                );
                $this->response($invalidLogin, REST_Controller::HTTP_BAD_REQUEST); //This is the respon if failed
            }else if($checkSubscription->registered === '1'){
                $user_groups = $this->ion_auth->get_users_groups($checkFacebookId->id)->row();
                $newtoken['id'] = $checkFacebookId->id;  //From here$g
                $newtoken['role'] = $user_groups->id;  //From here
                $date = new DateTime();
                $newtoken['iat'] = $date->getTimestamp();
                $newtoken['exp'] = $date->getTimestamp() + 60*60*24*30*3; //To here is to generate newtoken
                $output['token'] = JWT::encode($newtoken, $kunci); //This is the output token
                $output['exp'] = $date->getTimestamp() + 60*60*24*30*3;
                $output['_base'] = $user_groups->name;
                $this->response($output, REST_Controller::HTTP_OK);
            }else{
                $data = array(
                    'id' => $checkFacebookId->id,
                    'loginSocial' => '0'
                );
                $this->response($data, REST_Controller::HTTP_OK);
            }
        }else if(isset($checkFacebookId) && $checkFacebookId && $checkSubscription === false){
            $data = array(
                'id' => $checkFacebookId->id,
                'loginSocial' => '0'
            );
            $this->response($data, REST_Controller::HTTP_OK);
        }else if(!isset($checkFacebookId)) {
            $email = '';
            $identity = $nameFacebook;
            $password = '';
            $additional_data = array(
              'profileImageURL' =>  $profileImageURL,
              'social_status'   =>  '1',
              'facebook_id'     =>  $idFacebook
            );
            $new_user =  $this->ion_auth->register($identity, $password, $email, $additional_data);
            if ($new_user) {
                $dataUser = array(
                    'eventProjectSocket' => 'all_user,user_'.$new_user
                );
                $this->User->editUser($new_user, $dataUser);
                $data = array(
                    'id' => $new_user,
                    'loginSocial' => '0'
                );
                $this->response($data, REST_Controller::HTTP_OK);
            } else {
                $invalid = array(
                    'status' => 'Create user failed'
                );
                $this->response($invalid, REST_Controller::HTTP_BAD_REQUEST);
            }
        }
    }

    public function CreateSocial_post() {
        // load library
        $kunci = $this->config->item('thekey');

        // get info  = post
        $user_id = $this->post('user_id');
        $email = strtolower($this->post('email'));
        $password = $this->post('password');
        $username = $this->post('username');

        // create date
        $date = new DateTime();
        $date2 = $date->format('Y-m-d H:i:s');

        // create token
        $token['user_id'] = $user_id;
        $token['email'] = $email;
        $token['iat'] = $date->getTimestamp();
        $token['exp'] = $date->getTimestamp() + 60*60*24; //To here is to generate token
        $output['token'] = JWT::encode($token, $kunci); //This is the output token

        $checkEmail = $this->Subscription->getDataEmail($email);
        if(isset($checkEmail) && $checkEmail){
            $error = array(
                'status'=>'このメールアドレスが存在しました。'
            );
            $this->response($error,400);
        }

        $checkUserName = $this->User->checkUsername($username);
        if(isset($checkUserName) && $checkUserName){
            $error = array(
                'status'=>'このユーザー名が存在しました。'
            );
            $this->response($error,400);
        }
        // config for form_validation
        $config = [
            [
                'field' => 'email',
                'label' => 'Email',
                'rules' => 'valid_email',
                'errors' => [
                  'valid_email' => 'メールアドレスが正しくないので、確認してください',
                ],
            ],
        ];

        // config of form validation
        $data = $this->post();
        $this->form_validation->set_data($data);
        $this->form_validation->set_rules($config);

        // form_validation  = true
        if ($this->form_validation->run() !== false) {

            $curUser = $this->User->getUserSocialId($user_id);

            if(isset($curUser) && $curUser){

                $this->load->library('email');

                $this->email->from('admin@kakuseida.conoha.io', 'KAKUSEIDA');
                $this->email->to($email);
                // $this->email->reply_to('admin@kakuseida.conoha.io', 'KAKUSEIDA');
                $this->email->subject('【KAKUSEIDA】本人確認のお知らせ');
                $this->email->message(
                    'クラウドファンディングKAKUSEIDAをご利用いただき、誠にありがとうございます。<br />
                    <a href="'.app_url().'create-social/'.$output['token'].'">こちら</a>のURLをクリックして、会員登録を続けてください。<br />'
                );

                if(isset($curUser->email) && $curUser->email){
                    $checkSubscription = $this->Subscription->getDataEmail($curUser->email);
                    if(isset($checkSubscription) && $checkSubscription->registered === '1'){
                        $error = array(
                            'status' => 'This account is exists!'
                        );
                        $this->response($error,400);
                    }else if(isset($checkSubscription) && $checkSubscription->registered === '0'){
                        // if ($this->email->send()) {
                            $dataUser = array(
                                'username' => $username,
                                'email' => $email,
                                'password' => $password,
                                'created' => $date2,
                            );
                            $updateUser = $this->ion_auth->update($user_id,$dataUser);

                            $dataSubScription = array(
                                'email'=> $email,
                                'token'=> $output['token'],
                                'registered' =>'0',
                                'created' => $date2
                            );
                            $regEmail = $this->Subscription->update($checkSubscription->id,$dataSubScription);
                            $this->email->send();
                            $success = array(
                                'status' => 'success'
                            );
                            $this->response($success,200);
                    }else{
                        $dataUser = array(
                            'username' => $username,
                            'email' => $email,
                            'password' => $password,
                            'created' => $date2,
                        );
                        $updateUser = $this->ion_auth->update($user_id,$dataUser);

                        $dataSubScription = array(
                            'email'=> $email,
                            'token'=> $output['token'],
                            'registered' =>'0',
                            'created' => $date2
                        );
                        $regEmail = $this->Subscription->create($dataSubScription);
                        $this->email->send();
                        $success = array(
                            'status' => 'success'
                        );

                        $this->response($success,200);
                    }
                }else{
                        $dataUser = array(
                            'username' => $username,
                            'email' => $email,
                            'password' => $password,
                            'created' => $date2,
                        );
                        $updateUser = $this->ion_auth->update($user_id,$dataUser);

                        $dataSubScription = array(
                            'email'=> $email,
                            'token'=> $output['token'],
                            'registered' =>'0',
                            'created' => $date2
                        );
                        $regEmail = $this->Subscription->create($dataSubScription);
                        $this->email->send();
                        $success = array(
                            'status' => 'success'
                        );

                        $this->response($success,200);
                }
            }else {
                $error = array(
                    'status' => 'This id not exists'
                );
                $this->response($error, 404);
            }
        } else {
            $error = $this->form_validation->error_array();
            $this->response($error, 400);
        }
    }

    public function finishSocial_get() {

        $kunci = $this->config->item('thekey');
        $token = $this->get('token');
        try {
            $decoded = JWT::decode($token, $kunci, array('HS256'));

            $checkSubscription = $this->Subscription->getDataEmail($decoded->email);
            if(isset($checkSubscription) && $checkSubscription->registered === '0'){
                $dataUser = array(
                    'active' => '1',
                    'social_status' => '0',
                );
                $updateUser = $this->ion_auth->update($decoded->user_id,$dataUser);

                $dataSubScription = array(
                    'token'=> '',
                    'registered' =>'1',
                );
                $regEmail = $this->Subscription->update($checkSubscription->id,$dataSubScription);


                if($updateUser) {
                    $user_groups = $this->ion_auth->get_users_groups($decoded->user_id)->row();
                    $newtoken['id'] = $decoded->user_id;  //From here$g
                    $newtoken['role'] = $user_groups->id;  //From here
                    $date = new DateTime();
                    $newtoken['iat'] = $date->getTimestamp();
                    $newtoken['exp'] = $date->getTimestamp() + 60*60*24*30*3; //To here is to generate newtoken
                    $output['token'] = JWT::encode($newtoken, $kunci); //This is the output token
                    $output['exp'] = $date->getTimestamp() + 60*60*24*30*3;
                    $output['_base'] = $user_groups->name;
                    $this->response($output, REST_Controller::HTTP_OK);
                }else{
                    $success = array(
                        'status' => 'Not Update Success!'
                    );
                    $this->response($success,400);
                }

            }else {
                $success = array(
                    'status' => 'error'
                );
                $this->response($success,404);
            }
        } catch (Exception $e) {
            $invalid = ['status' => $e->getMessage()];
            $this->response($invalid, 400);
        }
    }

    public function loginTwitter_post() {
        $consumerKey  = $this->config->item('consumerKey');
        $consumerSecret = $this->config->item('consumerSecret');

        require FCPATH . 'vendor/autoload.php';
        $connection = new TwitterOAuth($consumerKey, $consumerSecret);
        $request_token = $connection->oauth("oauth/request_token", array('oauth_callback','http%3A%2F%2Flocalhost%3A3000%2Ftwitter-callback'));
        $this->response($request_token, 200);
    }

    public function loginNewTwitter_post() {

        $consumerKey  = $this->config->item('consumerKey');
        $consumerSecret = $this->config->item('consumerSecret');
        require FCPATH . 'vendor/autoload.php';

        $connection = new TwitterOAuth($consumerKey, $consumerSecret);
        $request_token = $connection->oauth("oauth/request_token", array('oauth_callback','http%3A%2F%2Flocalhost%3A3000%2Ftwitter-callback'));

        $oauth_token = $this->post('oauth_token');
        $oauth_verifier = $this->post('oauth_verifier');

        $newConnection = new TwitterOAuth($consumerKey, $consumerSecret, $oauth_token, '12312312');
        $access_token = $newConnection->oauth("oauth/access_token", ["oauth_verifier" => $oauth_verifier]);


        $connection = new TwitterOAuth($consumerKey, $consumerSecret, $access_token['oauth_token'], $access_token['oauth_token_secret']);
        $user = $connection->get("account/verify_credentials");

        $idTwitter = $access_token['user_id'];
        $userNameTwitter = $access_token['screen_name'];
        $nameTwitter = replaceName($userNameTwitter);
        $profileImageURL = $user->profile_image_url_https;

        $profileImageURL = str_replace('normal', '400x400', $profileImageURL);

        $checkTwitterId = $this->User->checkIdTwitter($idTwitter);
        $kunci = $this->config->item('thekey');

        if(isset($checkTwitterId->email)){
            $checkSubscription = $this->Subscription->getDataEmail($checkTwitterId->email);
        }else{
            $checkSubscription = false;
        }

        if(isset($checkTwitterId) && $checkTwitterId && isset($checkSubscription) && $checkSubscription){
            if($checkTwitterId->active === '0'){
                $invalidLogin = array(
                    'status' => '総合的な理由により、アカウントを停止されました。'
                );
                $this->response($invalidLogin, REST_Controller::HTTP_BAD_REQUEST); //This is the respon if failed
            }else if($checkSubscription->registered === '1'){
                $user_groups = $this->ion_auth->get_users_groups($checkTwitterId->id)->row();
                $newtoken['id'] = $checkTwitterId->id;  //From here$g
                $newtoken['role'] = $user_groups->id;  //From here
                $date = new DateTime();
                $newtoken['iat'] = $date->getTimestamp();
                $newtoken['exp'] = $date->getTimestamp() + 60*60*24*30*3; //To here is to generate newtoken
                $output['token'] = JWT::encode($newtoken, $kunci); //This is the output token
                $output['exp'] = $date->getTimestamp() + 60*60*24*30*3;
                $output['_base'] = $user_groups->name;
                $this->response($output, REST_Controller::HTTP_OK);
            }else{
                $data = array(
                    'id' => $checkTwitterId->id,
                    'loginSocial' => '0'
                );
                $this->response($data, REST_Controller::HTTP_OK);
            }
        }else if(isset($checkTwitterId) && $checkTwitterId && $checkSubscription === false){
            $data = array(
                'id' => $checkTwitterId->id,
                'loginSocial' => '0'
            );
            $this->response($data, REST_Controller::HTTP_OK);
        }else if(!isset($checkTwitterId)) {
            $email = '';
            $identity = $nameTwitter;
            $password = '';
            $additional_data = array(
              'profileImageURL' =>  $profileImageURL,
              'social_status'   =>  '1',
              'twitter_id'     =>  $idTwitter
            );
            $new_user =  $this->ion_auth->register($identity, $password, $email, $additional_data);
            if ($new_user) {
                $dataUser = array(
                    'eventProjectSocket' => 'all_user,user_'.$new_user
                );
                $this->User->editUser($new_user, $dataUser);
                $data = array(
                    'id' => $new_user,
                    'loginSocial' => '0'
                );
                $this->response($data, REST_Controller::HTTP_OK);
            } else {
                $invalid = array(
                    'status' => 'Create user failed'
                );
                $this->response($invalid, REST_Controller::HTTP_BAD_REQUEST);
            }
        }
    }


    public function registerEmail_post() {

        $email = strtolower($this->post('email'));
        $kunci = $this->config->item('thekey');
        $token['email'] = $email;
        $date = new DateTime();
        $date2 = $date->format('Y-m-d H:i:s');
        $token['iat'] = $date->getTimestamp();
        $token['exp'] = $date->getTimestamp() + 60*60*24; //To here is to generate token
        $output['token'] = JWT::encode($token, $kunci); //This is the output token

        $this->form_validation->set_rules(
            'email',
            'Email',
            'trim|required|valid_email|is_unique[subscription.email]|is_unique[black_list.email]',
            array('is_unique' => 'このメールアドレスが存在しました。')
        );

        $this->form_validation->set_data($this->post());

        if ($this->form_validation->run() === true) {
            $data = array(
                'email'=> $email,
                'token'=> $output['token'],
                'registered' =>'0',
                'created' => $date2
            );
            $this->load->library('email');


            $this->email->from('admin@kakuseida.conoha.io', 'KAKUSEIDA');
            $this->email->to($email);
            // $this->email->reply_to('admin@kakuseida.conoha.io', 'KAKUSEIDA');
            $this->email->subject('【KAKUSEIDA】本人確認のお知らせ');
            $this->email->message(
                'クラウドファンディングKAKUSEIDAをご利用いただき、誠にありがとうございます。<br />
                <a href="'.app_url().'create-user/'.$output['token'].'">こちら</a>のURLをクリックして、会員登録を続けてください。<br />'
            );

            if ($this->email->send()) {

                $regEmail = $this->Subscription->create($data);

                if ($regEmail) {
                    $user = array(
                        'status' => 'Create User Success and Send Mail Success !'
                    );

                    $this->response($user, REST_Controller::HTTP_OK);

                } else {
                    $invalid = ['status' => 'Create user failed'];
                    $this->response($invalid, REST_Controller::HTTP_BAD_REQUEST);
                }
            } else {
                $invalid = ['status' => 'このメールアドレスが存在していません'];
                $this->response($invalid, REST_Controller::HTTP_BAD_REQUEST);
            }
        } else {
            $register = $this->Subscription->getDataEmail($email);
            if ($register && $register->registered =='0') {
                $kunci = $this->config->item('thekey');
                $token= "token";
                $token = $register->token;
                try {
                    $decoded = JWT::decode($token, $kunci, array('HS256'));
                    $invalid = ['status' => '受信メールをご確認ください'];
                    $this->response($invalid, 400);
                } catch (Exception $e) {

                    $data = array(
                        'token' => $output['token']
                    );

                    $reRegit = $this->Subscription->update($register->id, $data);
                    if ($reRegit) {
                        $linkMessage = $output['token'];
                        //use json_encode show data.
                        $this->email->initialize($config);

                        $this->email->from('admin@kakuseida.conoha.io', 'KAKUSEIDA');
                        $this->email->to($email);
                        // $this->email->reply_to('admin@kakuseida.conoha.io', 'KAKUSEIDA');
                        $this->email->subject('【KAKUSEIDA】本人確認のお知らせ');
                        $this->email->message(
                            'クラウドファンディングKAKUSEIDAをご利用いただき、誠にありがとうございます。<br />
                            <a href="'.app_url().'create-user/'.$linkMessage.'">こちら</a>のURLをクリックして、会員登録を続けてください。<br />
                            '
                        );
                        if ($this->email->send()) {
                            $user = array(
                                'status' => 'Create User Success and Send Mail Success !'
                            );
                            $this->response($user, REST_Controller::HTTP_OK);
                        } else {
                            $invalid = ['status' => 'Create user failed'];
                            $this->response($invalid, REST_Controller::HTTP_BAD_REQUEST);//401
                        }
                    }
                }
            } else {
                $invalid = ['status' => 'このメールアドレスは既に存在します'];
                $this->response($invalid, REST_Controller::HTTP_BAD_REQUEST);//401
            }
        }
    }

    public function checkToken_post()
    {
        $kunci = $this->config->item('thekey');
        $token= "token";
        $token = $this->post('token');

        try {
            $decoded = JWT::decode($token, $kunci, array('HS256'));
            $invalid = ['status' =>'This token can use!'];
            $this->response($invalid, 200);
        } catch (Exception $e) {
            $invalid = ['status' => $e->getMessage()];
            $this->response($invalid, 400);
        }
    }

    public function createUser_post()
    {
        $date = new DateTime();
        $date = $date->format('Y-m-d H:i:s');
        $kunci = $this->config->item('thekey');
        $token= "token";
        $token = $this->post('token');

        try {
            $decoded = JWT::decode($token, $kunci, array('HS256'));
            $this->user_data = $decoded;
        } catch (Exception $e) {
            $invalid = ['status' => 'このURLは無効です'];
            $this->response($invalid, 401);
        }

        $register = $this->Subscription->getDataEmail($decoded->email);

        if (!$this->User->getDataEmail($decoded->email) && $register->registered == '0') {
            $this->form_validation->set_rules(
                'username',
                'Username',
                'is_unique[users.username]'
            );
            $this->form_validation->set_data($this->post());
            if ($this->form_validation->run() === true) {
                $email = $decoded->email;
                $identity = $this->post('username');
                $password = $this->post('password');

                $additional_data = array(
                    'profileImageURL'=>base_url().'images/2018/default/default-profile_01.png',
                    'created'=>$date,
                );
                $new_user =  $this->ion_auth->register($identity, $password, $email, $additional_data);

                if ($new_user) {
                    $data = array(
                        'registered'=>'1',
                        'token' => ''
                    );
                    $dataUser = array(
                        'eventProjectSocket' => 'all_user,user_'.$new_user
                    );
                    $this->User->editUser($new_user, $dataUser);
                    $this->Subscription->update($register->id, $data);
                    $dataUser = $this->User->getUserId($new_user);
                    if ($dataUser) {
                        $kunci = $this->config->item('thekey');
                        $user_groups = $this->ion_auth->get_users_groups($new_user)->row();
                        $newtoken['id'] = $new_user;  //From here$g
                        $newtoken['role'] = $user_groups->id;  //From here
                        $newtoken['username'] = $decoded->email;
                        $date = new DateTime();
                        $newtoken['iat'] = $date->getTimestamp();
                        $newtoken['exp'] = $date->getTimestamp() + 60*60*5; //To here is to generate newtoken
                        $output['token'] = JWT::encode($newtoken, $kunci); //This is the output token
                        $output['exp'] = $date->getTimestamp() + 60*60*5;
                        $output['_base'] = $user_groups->name;
                        $this->response($output, REST_Controller::HTTP_OK);
                    }
                } else {
                    $invalid = array(
                  'status' => 'エラーがありましたので、ユーザーの作成ができませんでした。再度作成してください。'
              );
                    $this->response($invalid, REST_Controller::HTTP_BAD_REQUEST);
                }
            } else {
                $invalid = array(
              'status' => 'このユーザー名が存在しました。'
          );
                $this->response($invalid, REST_Controller::HTTP_BAD_REQUEST);
            }
        } else {
            $invalid = array(
            'status' => 'このメールアドレスは既に存在します!'
        );
            $this->response($invalid, REST_Controller::HTTP_BAD_REQUEST);
        }
    }

    public function sendmail_post()
    {
        $this->load->library('email');
        $config = $this->load->config('email');
        $this->email->initialize($config);


        $this->email->from('admin@kakuseida.conoha.io', 'KAKUSEIDA');
        $this->email->to('hodacquyenpx@gmail.com');
        //   $this->email->reply_to('admin@kakuseida.conoha.io', 'KAKUSEIDA');
        $this->email->subject('canoha test');
        // $message = $this->load->view('email_template/beefree-ppeh5b7pdl.php',$data,TRUE);
        $message = 'dsadsadsa';
        $this->email->message($message);
        $this->email->send();
    }

    public function Forgot_pass_post()
    {
        $email = $this->post('email');

        if (!valid_email($email)) {
            $invalid = array(
              'status' => 'メールアドレスが正しくないので、確認してください'
          );
            $this->response($invalid, REST_Controller::HTTP_BAD_REQUEST);
        }
        $code = $this->ion_auth->forgotten_password($email);
        if ($code) {
            // config email
            $this->load->library('email');

            $this->email->from('admin@kakuseida.conoha.io', 'KAKUSEIDA');
            $this->email->to($email);
            // $this->email->reply_to('admin@kakuseida.conoha.io', 'KAKUSEIDA');
            $this->email->subject('【KAKUSEIDA】パスワード再設定用URLのお知らせ');
            $tokenForgot = $code["forgotten_password_code"];
            $this->email->message(
              'クラウドファンディングKAKUSEIDAをご利用いただき、誠にありがとうございます。<br />
              <a href="'.app_url().'reset_pass/'.$tokenForgot.'">こちら</a>のURLをクリックしてパスワードを再設定してください。<br />
              ※以上のURLは1回のみ使えますので、ご注意ください。'
            );
            if ($this->email->send()) {
                $data = array('status'=>'Send Mail Thành Công');
                $this->response($data, 200);
            } else {
                $data = array('status'=>'Send Mail Thất Bại');
                $this->response($data, REST_Controller::HTTP_BAD_REQUEST);
            }
        } else {
            $data = array('status'=>'This User not exits');
            $this->set_response($data, REST_Controller::HTTP_BAD_REQUEST);
        }
    }

    public function check_code_exists_post()
    {
        $code = $this->post('code');
        $user = $this->ion_auth->forgotten_password_check($code);
        if ($user) {
            $data = array(
            'status'=> 'This code is exists!'
        );
            $this->response($data, 200);
        } else {
            $data = array(
            'status'=> 'This code is not exists!'
        );
            $this->response($data, REST_Controller::HTTP_BAD_REQUEST);
        }
    }

    public function checkTokenForgot_post()
    {
        $code = $this->post('tokenForgot');
        $user = $this->ion_auth->forgotten_password_check($code);
        if ($user) {
            $success = array(
                'status' => 'Token exists!'
            );
            $this->response($success, 200);
        } else {
            $error = array(
                'status' => 'Token not exists!'
            );
            $this->response($error, 404);
        }
    }

    public function Reset_pass_post()
    {
        $code = $this->post('code');
        $user = $this->ion_auth->forgotten_password_check($code);
        if ($user) {
            $new = $this->post('new_password');
            $change = $this->ion_auth->reset_password($user->email, $new);

            if ($change) {
                $a = $this->ion_auth->forgotten_password_complete($code);
                $data = array(
                'status'=> 'change password success!'
            );
                $this->response($data, 200);
            } else {
                $data = array(
                'status'=> 'change password failed!'
            );
                $this->response($data, REST_Controller::HTTP_BAD_REQUEST);
            }
        } else {
            $data = array(
            'status'=> 'Your Request not found!'
        );
            $this->response($data, REST_Controller::HTTP_BAD_REQUEST);
        }
    }
}
