<?php if(!defined('BASEPATH')) exit('No direct script allowed');

class BackingLevel extends BD_Model{
	var $table = 'backing_levels';
	// Get All Level Donate
	function getBackingLevel(){
		$this->db->from($this->table);
		$a = $this->db->get();
		return $a->result();
	}
	// Get All Level Donate with  project_id
  function getBackingLevelId($id){
		$this->db->from($this->table);
		$this->db->where('project_id',$id);
		$a = $this->db->get();
		return $a->result();
	}
	function getLevelId($id){
		  $this->db->from($this->table);
		  $this->db->where('id',$id);
		  $a = $this->db->get();
		  return $a->row();
	  }
	// Get Level Donate with id
	function getBackingId($id){
		$this->db->select('backing_levels.*,projects.user_id');
		$this->db->from('backing_levels');
		$this->db->where('backing_levels.id',$id);
		$this->db->join('projects', 'projects.id = backing_levels.project_id');
		$a = $this->db->get();
		return $a->row();
	}

	function getProjectBackingId($project_id,$backing_level_id){
		$this->db->select('*');
		$this->db->select('backing_levels.id as `id`');
		$this->db->from($this->table);
		$this->db->where('backing_levels.id',$backing_level_id);
		$this->db->where('projects.id',$project_id);
		$this->db->join('projects', 'projects.id = backing_levels.project_id');
		$a = $this->db->get();
		return $a->row();
	}

	// Deletes Level Donate with id (Xóa nhiều, khi xóa 1 project thì xóa luôn các level donate)
	function deleteProjectReturn($id){
		if(isset($id)){
			$this->db->where("id in ($id)");
			// Delete talbe project with id
			$del = $this->db->delete($this->table);
			if($del){
				return true;
			}else{
				return false;
			}
		}else{
			return false;
		}
	}
}
