<?php if(!defined('BASEPATH')) exit('No direct script allowed');

class WishList extends BD_Model{
	var $table = 'wish_list';

    public function checkAddWishList($user_id,$project_id) {
        $this->db->from($this->table);
        $this->db->where('user_id',$user_id);
        $this->db->where('project_id',$project_id);
        $param = $this->db->get();
        if(isset($param)){
            return $param->row();
        }else{
            return false;
        }
    }

	public function getWishList($user_id,$input = null){
		$date = new DateTime();
		//fomat date
		$date = $date->format('Y-m-d H:i:s');
		//total data
		$this->db->select('wish_list.id,projects.project_name,projects.thumbnail,projects.active,projects.status_fanclub');
		$this->db->from($this->table);
		$this->db->where('wish_list.user_id',$user_id);
		$this->db->where('projects.active','yes');
		$this->db->where('projects.status_fanclub','progress');
		$this->db->order_by('wish_list.created','DESC');
		$this->db->join('projects', ' projects.id = wish_list.project_id');
		$total = $this->db->count_all_results();


		//Use Limit of CI
		$this->db->select('wish_list.*,projects.project_name,projects.thumbnail,projects.active,projects.status_fanclub');
		$this->db->from($this->table);
		$this->db->where('wish_list.user_id',$user_id);
		$this->db->where('projects.active','yes');
		$this->db->where('projects.status_fanclub','progress');
		$this->db->order_by('wish_list.created','DESC');
		$this->db->join('projects', ' projects.id = wish_list.project_id');
		// $this->db->where('opened','yes');
		// if($input){
		// 	$this->db->limit($input['limit'][0], $input['limit'][1]);
		// }
		$query = $this->db->get();
		$project = $query->result();
		return ["total" => $total, "project" => $project];
	}
}
