<?php if(!defined('BASEPATH')) exit('No direct script allowed');

class Setting extends BD_Model{
	var $table = 'setting';

	function getSetting(){
		$this->db->from($this->table);
		$a = $this->db->get();
		return $a->row();
	}
}
?>
