<?php if(!defined('BASEPATH')) exit('No direct script allowed');

class BlackList extends BD_Model{
	var $table = 'black_list';

    public function getCard($last4){

        if(isset($last4) && $last4){
            $this->db->from($this->table);
            $this->db->where('last4',$last4);
            $this->db->where('status',1);
            $a = $this->db->get();
            return $a->result();
        }

    }

}
