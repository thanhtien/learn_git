<?php if(!defined('BASEPATH')) exit('No direct script allowed');

class Project extends BD_Model{
	var $table = 'projects';
	// search Project vs project name with admin public
	function searchProject($search){
		$this->db->select('*');
		$this->db->from($this->table);
		$this->db->like('project_name', $search);
		$this->db->where('opened', 'yes');
		$this->db->where('active', 'yes');
		$a = $this->db->get();
		return $a->result();
	}

	function getProjectCls(){
		$this->db->select('*');
		$this->db->from($this->table);
		$this->db->where('opened', 'yes');
		$this->db->where('active', 'cls');
		$a = $this->db->get();
		return $a->result();
	}
	//
	function getProjectUserId($id){
		$this->db->select('*');
		$this->db->from($this->table);
		$this->db->where('user_id', $id);
		$project = $this->db->get();
		return $project->result();
  }

	function getProjectId($id){
		$this->db->select('*');
		$this->db->from($this->table);
		$this->db->where('id', $id);
		$a = $this->db->get();
		return $a->row();
	}

	function getProjectAndUserById($id){
		$this->db->select($this->table.'.*,users.email,users.username');
		$this->db->from($this->table);
		$this->db->join('users', 'projects.user_id = users.id');
		$this->db->where('projects.id', $id);
		$a = $this->db->get();
		return $a->row();
	}

	function getProjectNormalId($id){
		$this->db->select('*');
		$this->db->from($this->table);
		$this->db->where('id', $id);
		$this->db->where('project_type','0');
		$a = $this->db->get();
		return $a->row();
	}

	public function getProjectIdJoin($id){
		$this->db->select('projects.*,categories.*,users.username,users.address,users.job,users.profileImageURL');
		$this->db->select('projects.id as `id`');
		$this->db->from($this->table);
		$this->db->join('categories', 'projects.category_id = categories.id' , 'RIGHT');
		$this->db->join('users', 'projects.user_id = users.id');
		// $this->db->join('backing_levels', 'projects.id = backing_levels.project_id','LEFT');
		$this->db->where('projects.id', $id);
		$this->db->group_by('projects.id');
		$this->db->order_by('projects.modified', 'desc');
		$a = $this->db->get();
		return $a->row();
	}


	function getProjectUserIdSubsidized($id,$input = null){
		$this->db->select('projects.*');
		$this->db->from('projects');
		$this->db->join('backing_levels', ' projects.id = backing_levels.project_id ');
		if($input){
			$this->db->limit($input['limit'][0], $input['limit'][1]);
		}
		$this->db->where('user_id', $id);
		$this->db->where('opened', 'yes');
		$this->db->where('active', 'yes');
		$this->db->where('backing_levels.now_count >= ', '1');
		$this->db->distinct();
		$a = $this->db->get();
		return $a->result();
  }

	function getProjectUserIdPost($id,$input = null){
		$this->db->select('projects.*');
		$this->db->from('projects');
		$this->db->join('backing_levels', ' projects.id = backing_levels.project_id ');
		if($input){
			$this->db->limit($input['limit'][0], $input['limit'][1]);
		}
		$this->db->where('user_id', $id);
		$this->db->where('active', 'no');
		$this->db->or_where('active', 'yes');
		$this->db->where('backing_levels.now_count = ', '0');
		$this->db->distinct();
		$a = $this->db->get();
		return $a->result();
	}

	function getAllProject($id,$input = null){
		$this->db->select('*');
		$this->db->from($this->table);
		if($input){
			$this->db->limit($input['limit'][0], $input['limit'][1]);
		}
		$this->db->order_by('id', 'desc');
		$this->db->where('user_id', $id);
		$a = $this->db->get();
		return $a->result();

	}

	function getAllProjectJoinCat($id,$input = null){
		$this->db->select('projects.*,categories.*,users.username');
		$this->db->select('projects.id as `id`');
		$this->db->from($this->table);
		if($input){
			$this->db->limit($input['limit'][0], $input['limit'][1]);
		}
		$this->db->join('categories', 'projects.category_id = categories.id' , 'RIGHT');
		$this->db->join('users', 'projects.user_id = users.id');
		// $this->db->join('backing_levels', 'projects.id = backing_levels.project_id','LEFT');
		$this->db->where('projects.user_id', $id);
		$this->db->group_by('projects.id');
		$this->db->order_by('projects.modified', 'DESC');
		$a = $this->db->get();
		return $a->result();
	}

	function getProjectSearch($search,$input = null){
		$this->db->select('projects.*,categories.name,categories.slug,users.username');
		$this->db->from($this->table);
		$this->db->join('categories', 'categories.id = projects.category_id');
		$this->db->join('users', 'users.id = projects.user_id');
		if($input){
			$this->db->limit($input['limit'][0], $input['limit'][1]);
		}
		$this->db->order_by('projects.id', 'desc');
		$this->db->like('projects.project_name',$search);
		$this->db->or_like('categories.name',$search);
		$this->db->or_like('categories.slug',$search);
		$this->db->or_like('users.username',$search);
		$a = $this->db->get();
		return $a->result();

	}

  	function ProjectId($id){
		$this->db->select('*');
		$this->db->from($this->table);
		$this->db->where('id', $id);
		$a = $this->db->get();
		return $a->row();
	}

	function getProjectCateId($id,$input = null){
		$this->db->select('projects.*,categories.*,users.username');
		$this->db->select('projects.id as `id`');
		$this->db->from($this->table);
		$this->db->join('categories', 'projects.category_id = categories.id' , 'RIGHT');
		$this->db->join('users', 'projects.user_id = users.id');
		$this->db->where('projects.category_id', $id);
		$this->db->where('projects.opened', 'yes');
		$this->db->where('projects.active', 'yes');
		if($input){
			$this->db->limit($input['limit'][0], $input['limit'][1]);
		}
		$a = $this->db->get();
		return $a->result();
	}
	// nvtrong
    function getProjectFavo($input = null){
       $this->db->select('projects.*,categories.*,users.username');
        $this->db->from($this->table);
        $this->db->join('categories', 'projects.category_id = categories.id' );
        $this->db->join('users', 'projects.user_id = users.id');
        $this->db->where('projects.opened', 'yes');
        $this->db->where('projects.active', 'yes');
        $this->db->where_in('projects.id', $input['array_project_id']);
        if($input){
            if(key_exists("limit",$input)){
                $this->db->limit($input['limit'][0], $input['limit'][1]);
            }

        }
        $a = $this->db->get();
        return $a->result();

    }

	function getProjectEnoughExpired($date,$datePlusFive){
		$this->db->select('projects.*,categories.*,users.username');
		$this->db->select('projects.id as `id`');
		$this->db->from($this->table);
		$this->db->join('categories', 'projects.category_id = categories.id' , 'RIGHT');
		$this->db->join('users', 'projects.user_id = users.id');
		$this->db->where('projects.collection_end_date >=', $date);
		$this->db->where('projects.collection_end_date <=', $datePlusFive);
		$this->db->where('projects.opened', 'yes');
		$this->db->where('projects.active', 'yes');
		$this->db->where('projects.project_type', '0');
		$this->db->limit(4,0);
		$a = $this->db->get();
		return $a->result();
	}

	function getAllProjectEnoughExpired($date,$datePlusFive){
		$this->db->select('projects.*,categories.*,users.username');
		$this->db->select('projects.id as `id`');
		$this->db->from($this->table);
		$this->db->join('categories', 'projects.category_id = categories.id' , 'RIGHT');
		$this->db->join('users', 'projects.user_id = users.id');
		$this->db->where('projects.collection_end_date >=', $date);
		$this->db->where('projects.collection_end_date <=', $datePlusFive);
		$this->db->where('projects.opened', 'yes');
		$this->db->where('projects.active', 'yes');
		$this->db->where('projects.project_type', '0');
		$a = $this->db->get();
		return $a->result();
	}

	function getProjectEnoughExpiredPagination($date,$datePlusFive,$input){
		$this->db->select('projects.*,categories.*,users.username');
		$this->db->select('projects.id as `id`');
		$this->db->from($this->table);
		$this->db->join('categories', 'projects.category_id = categories.id' , 'RIGHT');
		$this->db->join('users', 'projects.user_id = users.id');
		$this->db->where('projects.collection_end_date >=', $date);
		$this->db->where('projects.collection_end_date <=', $datePlusFive);
		$this->db->where('projects.opened', 'yes');
		$this->db->where('projects.active', 'yes');
		$this->db->where('projects.project_type', '0');
		if($input){
				$this->db->limit($input['limit']['0'],$input['limit']['1']);
		}
		$a = $this->db->get();
		return $a->result();
	}

	function countProjectEnoughExpired($date,$datePlusFive){
		$this->db->where('collection_end_date >=', $date);
		$this->db->where('collection_end_date <=', $datePlusFive);
		$this->db->where('opened', 'yes');
		$this->db->where('active', 'yes');
    	$total = $this->db->count_all_results($this->table);
		return $total;
	}

	function getEnoughAmount($date){
		$this->db->select('projects.*,categories.*,users.username');
		$this->db->select('projects.id as `id`');
		$this->db->from($this->table);
		$this->db->join('categories', 'projects.category_id = categories.id' , 'RIGHT');
		$this->db->join('users', 'projects.user_id = users.id');
		$this->db->where('(projects.collected_amount/projects.goal_amount)*100 >=', 90);
		$this->db->where('(projects.collected_amount/projects.goal_amount)*100 <', 100);
		$this->db->where('projects.collection_end_date >=', $date);
		$this->db->where('projects.opened', 'yes');
		$this->db->where('projects.active', 'yes');
		$this->db->where('projects.project_type', '0');
		$this->db->limit(4,0);
		$a = $this->db->get();
		return $a->result();
	}

	function getEnoughAmountPagination($date,$input = null){
		$this->db->select('projects.*,categories.*,users.username');
		$this->db->select('projects.id as `id`');
		$this->db->from($this->table);
		$this->db->join('categories', 'projects.category_id = categories.id' , 'RIGHT');
		$this->db->join('users', 'projects.user_id = users.id');
		$this->db->where('(projects.collected_amount/projects.goal_amount)*100 >=', 90);
		$this->db->where('(projects.collected_amount/projects.goal_amount)*100 <', 100);
		$this->db->where('projects.collection_end_date >=', $date);
		$this->db->where('projects.opened', 'yes');
		$this->db->where('projects.active', 'yes');
		$this->db->where('projects.project_type', '0');
		if($input){
			$this->db->limit($input['limit']['0'],$input['limit']['1']);
		}
		$a = $this->db->get();
		return $a->result();
	}

	function countEnoughAmount($date){
		$this->db->where('(collected_amount/goal_amount)*100 >=', 90);
		$this->db->where('(collected_amount/goal_amount)*100 <', 100);
		$this->db->where('collection_end_date >=', $date);
		$this->db->where('opened', 'yes');
		$this->db->where('active', 'yes');
		$total = $this->db->count_all_results($this->table);
		return $total;
	}

	public function addProject($data){
		$a = $this->db->insert($this->table, $data);
		return $this->db->insert_id();
	}

	public function checkProjectId($id){
		if(isset($id)){
			$query = $this->db->get_where($this->table, array('id' => $id))->row();
			if($query != null && $query->active == 'yes'){
				return $query;
			}else{
				return false;
			}
		}else{
			return false;
		}
	}
	public function deleteProject($id,$user_id){
		if(isset($id)){
			$query = $this->db->get_where($this->table, array('id' => $id))->row();
			if($query != null && $query->user_id == $user_id && $query->active === 'no' && $query->opened === 'no'){
				$projectId = $query->id;
				$projectReturn = $this->db->get_where('backing_levels', array('project_id' => $projectId))->result();

				if(count($projectReturn) > 0){
					$this->db->where("project_id in ($projectId)");
					$delProjectReturn = $this->db->delete('backing_levels');
				}

				$this->db->where('id', $id);
				$this->db->where('user_id', $user_id);
				$del = $this->db->delete($this->table);
				if($del){
					return true;
				}else{
					return false;
				}
			}else{
				return false;
			}
		}else{
			return false;
		}
	}

	public function checkProject($id,$user_id){
		if(isset($id)){
			$query = $this->db->get_where($this->table, array('id' => $id))->row();
			if($query != null && $query->user_id == $user_id && $query->active === 'no'){
				return true;
			}else{
				return false;
			}
		}else{
			return false;
		}
	}

	public function limitRefunds($date){
		$date = new DateTime();
		$date = $date->format('Y-m-d');
		$firtDate = date("Y-m-01", strtotime($date));
		$lastDate = date("Y-m-t", strtotime($date));
		$this->db->select('id');
		$this->db->from($this->table);
		$this->db->where('(collected_amount/goal_amount)*100 <', 100);
		$this->db->where('collection_end_date >=', $firtDate);
		$this->db->where('collection_end_date <=', $lastDate);
		$this->db->where('collection_end_date <', $date);
		$this->db->where('opened', 'yes');
		$this->db->where('active', 'yes');
		$a = $this->db->get();
		return $a->result();
	}

	public function getFavouriteProject(){
		$this->db->from('favourite_projects');
		$query = $this->db->get();
		$query = $query->row();
		return $query;
	}

	public function getPickupProject(){
		$this->db->from('pickup_projects');
		$query = $this->db->get();
		$query = $query->row();
		return $query;
	}

	public function getRecommendProject(){
		$this->db->from('recommends_projects');
		$query = $this->db->get();
		$query = $query->row();
		return $query;
	}

	public function getExpiredProject(){
		$this->db->from('expired_projects');
		$query = $this->db->get();
		$query = $query->row();
		return $query;
	}

	public function getFanClubs(){
		$this->db->from($this->table);
		$this->db->where('project_type','1');
		$this->db->where('active','yes');
		$this->db->where('opened','yes');
		$query = $this->db->get();
		return $query->result();
	}

	public function getFanClubsById($id){
		$this->db->from($this->table);
		$this->db->where('project_type','1');
		$this->db->where('id',$id);
		$query = $this->db->get();
		return $query->row();
	}

	public function getFanClubPickup(){
		$this->db->from('fanclub_projects');
		$query = $this->db->get();
		$query = $query->row();
		return $query;
	}

	public function getFanClubsStop(){
		$this->db->from($this->table);
		$this->db->where('project_type','1');
		$this->db->where('status_fanclub','stop');
		$this->db->where('active','yes');
		$this->db->where('opened','yes');
		$query = $this->db->get();
		return $query->result();
	}

	public function getAllProjectCurrent(){
		$this->db->from($this->table);
		$query = $this->db->get();
		return $query->result();
	}

	function getEmailOwnerByProjectId($id){
		$this->db->select($this->table.'.*,users.email');
		$this->db->from($this->table);
		$this->db->where('projects.id', $id);
		$this->db->join('users', 'projects.user_id = users.id');
		$project = $this->db->get();
		return $project->row();
	}

	function getOwnerByProjectId($id){
		$this->db->select('users.id,users.username,users.profileImageURL');
		$this->db->from($this->table);
		$this->db->where('projects.id', $id);
		$this->db->join('users', 'projects.user_id = users.id');
		$project = $this->db->get();
		return $project->row();
	}

	function getFullAmount($date,$input = null){
		$this->db->select('id');
		$this->db->from($this->table);
		$this->db->join('categories', 'projects.category_id = categories.id' , 'RIGHT');
		$this->db->join('users', 'projects.user_id = users.id');
		$this->db->where('(projects.collected_amount/projects.goal_amount)*100 >=', 100);
		$this->db->where('projects.collection_end_date >=', $date);
		$this->db->where('projects.opened', 'yes');
		$this->db->where('projects.active', 'yes');
		$this->db->where('projects.project_type', '0');
		$total = $this->db->count_all_results();

		$this->db->select('projects.*,categories.*,users.username');
		$this->db->select('projects.id as `id`');
		$this->db->from($this->table);
		$this->db->join('categories', 'projects.category_id = categories.id' , 'RIGHT');
		$this->db->join('users', 'projects.user_id = users.id');
		$this->db->where('(projects.collected_amount/projects.goal_amount)*100 >=', 100);
		$this->db->where('projects.collection_end_date >=', $date);
		$this->db->where('projects.opened', 'yes');
		$this->db->where('projects.active', 'yes');
		$this->db->where('projects.project_type', '0');
		if(isset($input) && $input){
			$this->db->limit($input['limit']['0'],$input['limit']['1']);
		}else {
			$this->db->limit(4,0);
		}
		$project = $this->db->get();
		$project = $project->result();
      	return ["total" => $total, "project" => $project];
	}


	function getEnoughAmountById($id,$date){
		$this->db->select('projects.*,categories.*,users.username');
		$this->db->select('projects.id as `id`');
		$this->db->from($this->table);
		$this->db->join('categories', 'projects.category_id = categories.id' , 'RIGHT');
		$this->db->join('users', 'projects.user_id = users.id');
		$this->db->where('(projects.collected_amount/projects.goal_amount)*100 >=', 90);
		$this->db->where('(projects.collected_amount/projects.goal_amount)*100 <', 100);
		$this->db->where('projects.id', $id);
		$this->db->where('projects.collection_end_date >=', $date);
		$this->db->where('projects.opened', 'yes');
		$this->db->where('projects.active', 'yes');
		$this->db->where('projects.project_type', '0');
		$a = $this->db->get();
		return $a->row();
	}

	public function getProjectExpiredAllin() {
		$date = new DateTime();
		$date = $date->format('Y-m-d');
		$this->db->select('projects.*,categories.*,users.username,users.email,users.token');
		$this->db->select('projects.id as `id`');
		$this->db->from($this->table);
		$this->db->join('categories', 'projects.category_id = categories.id' , 'RIGHT');
		$this->db->join('users', 'projects.user_id = users.id');
		$this->db->where('projects.collection_end_date <', $date);
		$this->db->where('projects.opened', 'yes');
		$this->db->where("(projects.active='yes' OR projects.active='cls')");
		$this->db->where('projects.status_finish_all_in', '0');
		$this->db->where('projects.project_type', '0');
		$a = $this->db->get();
		return $a->result();
	}


	public function getProjectBlockPublic($id){
		$this->db->select('projects.*,users.username,users.email');
		$this->db->from($this->table);
		$this->db->join('users', 'projects.user_id = users.id');
		$this->db->where('projects.opened', 'yes');
		$this->db->where('projects.user_id', $id);
		$this->db->where("(projects.active='yes' OR projects.active='blk')");
		$this->db->where('projects.status_finish_block_user', '0');
		$a = $this->db->get();
		return $a->result();
	}


    function countEnoughAmount80($date){
        $this->db->where('(collected_amount/goal_amount)*100 >=', 80);
        $this->db->where('(collected_amount/goal_amount)*100 <', 100);
        $this->db->where('collection_end_date >=', $date);
        $this->db->where('opened', 'yes');
        $this->db->where('active', 'yes');
        $total = $this->db->count_all_results($this->table);
        return $total;
    }
    function getEnoughAmountPagination80($date){
        $this->db->select('projects.*,categories.*,users.username');
        $this->db->select('projects.id as `id`');
        $this->db->from($this->table);
        $this->db->join('categories', 'projects.category_id = categories.id' , 'RIGHT');
        $this->db->join('users', 'projects.user_id = users.id');
        $this->db->where('(projects.collected_amount/projects.goal_amount)*100 >=', 80);
        $this->db->where('(projects.collected_amount/projects.goal_amount)*100 <', 100);
        $this->db->where('projects.collection_end_date >=', $date);
        $this->db->where('projects.opened', 'yes');
        $this->db->where('projects.active', 'yes');
        $this->db->where('projects.project_type', '0');
        $this->db->limit(4,0);
        $a = $this->db->get();
        return $a->result();
    }



}
