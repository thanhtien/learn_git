<?php if(!defined('BASEPATH')) exit('No direct script allowed');

class Report extends BD_Model{
  var $table = 'report_daily';

    public function listReport($project_id, $limit = null, $page = null){
        if(isset($project_id) && $project_id){
            $this->db->select('*');
                $this->db->from($this->table);
            $this->db->where('project_id',$project_id);
            $this->db->order_by('updated','DESC');
            if(isset($limit) && $limit && isset($page) && $page){
                $this->db->limit($limit,$page);
            }else{
                $this->db->limit(5, 0);
            }
            $project = $this->db->get();
            return $project->result();
        }else{
                return false;
        }
    }


    public function listReport2($project_id, $limit = null, $page = null){
        if(isset($project_id) && $project_id){
            $this->db->select('*');
                $this->db->from($this->table);
            $this->db->where('project_id',$project_id);
            $this->db->where('status',1);
            $this->db->order_by('updated','DESC');
            if(isset($limit) && $limit && isset($page) && $page){
                $this->db->limit($limit,$page);
            }else{
                $this->db->limit(5, 0);
            }
            $project = $this->db->get();
            return $project->result();
        }else{
                return false;
        }
    }

    function countReportDaily($project_id){
        $this->db->where('project_id',$project_id);
        $this->db->order_by('updated','DESC');
        $total = $this->db->count_all_results($this->table);
        return $total;
    }

    function countReportDaily2($project_id){
        $this->db->where('project_id',$project_id);
        $this->db->where('status',1);
        $this->db->order_by('updated','DESC');
        $total = $this->db->count_all_results($this->table);
        return $total;
    }

    public function getReportById($reportId) {

        if(isset($reportId) && $reportId){

    		$this->db->from($this->table);
    		$this->db->where('id', $reportId);
    		$report = $this->db->get();

            if(isset($report) && $report) {
                return $report->row();
            }else{
                return false;
            }

        }else{
            return false;
        }
    }

    public function getReportByDetailId($reportId) {

        if(isset($reportId) && $reportId){

            $this->db->from($this->table);
            $this->db->where('id', $reportId);
            // $this->db->where('status', 1);
            $report = $this->db->get();

            if(isset($report) && $report) {
                return $report->row();
            }else{
                return false;
            }

        }else{
            return false;
        }
    }

    public function delReportById($id){
        if(isset($id) && $id){
            $this->db->where('id',$id);
            $del = $this->db->delete($this->table);
            if($del){
                return true;
            }else{
                return false;
            }
        }else{
            return false;
        }
    }
}
