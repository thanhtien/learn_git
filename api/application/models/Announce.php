<?php if(!defined('BASEPATH')) exit('No direct script allowed');

class Announce extends BD_Model{
	var $table = 'announce';

    public function getInfoAnnounce(){
        $this->db->from($this->table);
        $this->db->where('status','1');
        $this->db->limit(2,0);
        $this->db->order_by('updated','DESC');
        $a = $this->db->get();
        return $a->result();
    }

	public function getAnnounceById($id){
		$this->db->from($this->table);
		$this->db->where('status','1');
		$this->db->where('id',$id);
		$a = $this->db->get();
		return $a->row();
	}
}
