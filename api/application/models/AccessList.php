<?php if(!defined('BASEPATH')) exit('No direct script allowed');

class AccessList extends BD_Model{
	var $table = 'access_list';

    public function checkAddAccessList($user_id,$project_id) {
        $this->db->from($this->table);
        $this->db->where('user_id',$user_id);
        $this->db->where('project_id',$project_id);
        $param = $this->db->get();
        if(isset($param)){
            return $param->row();
        }else{
            return false;
        }
    }

    public function deleteProjectUser($userId,$projectId){

        $array= array("project_id"=>$projectId,"user_id"=>$userId);
        $this->db->where($array);
        $this->db->delete('access_list');

    }
    public function getTopTop($user_id){
        $date = new DateTime();
        //fomat date
        $date = $date->format('Y-m-d H:i:s');
        //total data
        $this->db->select('projects.id,projects.project_name,projects.thumbnail,projects.active,projects.status_fanclub');
        $this->db->from($this->table);
        $this->db->join('projects', ' projects.id = access_list.project_id');
        $this->db->where('access_list.user_id',$user_id);
        $this->db->where('projects.active','yes');
        $this->db->order_by('access_list.created','DESC');
        $this->db->limit(1);
        $query = $this->db->get();
        $project = $query->row();
        return $project;
    }
    public function get3RecentAccess($user_id){
        $date = new DateTime();
        //fomat date
        $date = $date->format('Y-m-d H:i:s');
        //total data
        $this->db->select('projects.id,projects.project_name,projects.thumbnail,projects.active,projects.status_fanclub');
        $this->db->from($this->table);
        $this->db->join('projects', ' projects.id = access_list.project_id');
        $this->db->where('access_list.user_id',$user_id);
        $this->db->where('projects.active','yes');
        $this->db->order_by('access_list.created','DESC');
        $this->db->limit(3);
        $query = $this->db->get();

        $project = $query->result_array();
        return $project;


    }
    public function get3OneLastPercentProject(){


        $date = new DateTime();
        $date = $date->format('Y-m-d H:i:s');
        $this->db->select('projects.id,projects.project_name,projects.thumbnail,projects.active,projects.status_fanclub');
        $this->db->from('projects');
        $this->db->where('projects.active','yes');
        $this->db->where('(projects.collected_amount/projects.goal_amount)*100 >=', 80);
        $this->db->where('(projects.collected_amount/projects.goal_amount)*100 <', 100);
        $this->db->where('projects.collection_end_date >=', $date);
        $this->db->order_by('projects.modified','DESC');
        $this->db->limit(3);
        $query = $this->db->get();

        $project = $query->result_array();
        return $project;
    }
    public function getOneLastPercentProject(){


        $date = new DateTime();
        $date = $date->format('Y-m-d H:i:s');
        $this->db->select('projects.id,projects.project_name,projects.thumbnail,projects.active,projects.status_fanclub');
        $this->db->from('projects');
        $this->db->where('projects.active','yes');
        $this->db->where('(projects.collected_amount/projects.goal_amount)*100 >=', 80);
        $this->db->where('(projects.collected_amount/projects.goal_amount)*100 <', 100);
        $this->db->where('projects.collection_end_date >=', $date);
        $this->db->order_by('projects.modified','DESC');
        $this->db->limit(1);
        $query = $this->db->get();

        $project = $query->row();
        return $project;
    }

    public function getLimit3LastNew(){



        $date = new DateTime();
        $date = $date->format('Y-m-d H:i:s');
        $this->db->select('projects.id,projects.project_name,projects.thumbnail,projects.active,projects.status_fanclub');
        $this->db->from('projects');
        $this->db->where('projects.active','yes');
        $this->db->where('projects.collection_end_date >=', $date);
        $this->db->order_by('projects.modified','DESC');
        $this->db->limit(3);
        $query = $this->db->get();



        $project = $query->result_array();
        return $project;

    }
    public function getLimit3LastProject(){



        $date = new DateTime();
        $date = $date->format('Y-m-d H:i:s');
        $this->db->select('projects.id,projects.project_name,projects.thumbnail,projects.active,projects.status_fanclub');
        $this->db->from('projects');
        $this->db->where('projects.active','yes');
        $this->db->where('projects.collection_end_date >=', $date);
        $this->db->order_by('projects.modified','DESC');
        $this->db->limit(3);
        $query = $this->db->get();



        $project = $query->result_array();
        return $project;
        /*

        $date = new DateTime();
        $date = $date->format('Y-m-d H:i:s');
        $input['order'] = array('projects.modified','DESC');
        // $input['where'] = array('collection_start_date >=' => $date);
        $input['where'] = array('projects.active'=>'yes');
        $input['where2'] = array('projects.collection_end_date >=' => $date);
        $input['where3'] = array('projects.project_type'=>'0');
        // $input['where'] = array('opened'=>'yes');
        $input['limit'] = array(3,0);
        $toppage = $this->getDataProject($input);

        foreach ($toppage as $key => $value) {
            $value->now_count=array(
                'now_count'=>$value->now_count
            );

            $value->category=array(
                'id'=>$value->category_id,
                'name'=>$value->name,
                'slug'=>$value->slug
            );

            $value->user = array(
                'id'=>$value->user_id,
                'name'=>$value->username,
            );
            $timeProject = getTimeProject($value->collection_end_date);
            $value->format_collection_end_date = $timeProject;
        }
        return $toppage;
 */
    }

    public function getOneLasNews(){

        $this->db->select('news.id,news.title as project_name,news.thumnail as thumbnail');
        $this->db->from('news');
        $this->db->order_by('news.updated','DESC');
        $this->db->limit(1);
        $query = $this->db->get();
        $project = $query->row();
        return $project;
    }
    public function getAccessList($user_id,$input = null){
		$date = new DateTime();
		//fomat date
		$date = $date->format('Y-m-d H:i:s');
		//total data
		$this->db->select('access_list.id,projects.project_name,projects.thumbnail,projects.active,projects.status_fanclub');
		$this->db->from($this->table);
		$this->db->where('access_list.user_id',$user_id);
		$this->db->where('projects.active','yes');
		$this->db->where('projects.status_fanclub','progress');
		$this->db->order_by('access_list.created','DESC');
		$this->db->join('projects', ' projects.id = access_list.project_id');
		$total = $this->db->count_all_results();


		//Use Limit of CI
		$this->db->select('access_list.*,projects.project_name,projects.thumbnail,projects.active,projects.status_fanclub');
		$this->db->from($this->table);
		$this->db->where('access_list.user_id',$user_id);
		$this->db->where('projects.active','yes');
		$this->db->where('projects.status_fanclub','progress');
		$this->db->order_by('access_list.created','DESC');
		$this->db->join('projects', ' projects.id = access_list.project_id');

		$query = $this->db->get();
		$project = $query->result();
		return ["total" => $total, "project" => $project];
	}
}
