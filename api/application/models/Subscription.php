<?php if(!defined('BASEPATH')) exit('No direct script allowed');

class Subscription extends BD_Model{
	var $table = 'subscription';
	function getDataEmail($email) {
			if($email){
					$query = $this->db->where('email',$email)->get($this->table);
					return $query->row();
			}else{
				return false;
			}
	}

	function unSubscribeMail($email,$data) {
		if(isset($email) && $email) {
		$update = $this->db->update($this->table, $data, array('email' => $email));
			if(isset($update) && $update){
				return true;
			}else{
				return false;
			}
		}else{
			return false;
		}
	}
}
