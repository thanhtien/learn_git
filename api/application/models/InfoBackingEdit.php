<?php if(!defined('BASEPATH')) exit('No direct script allowed');

class InfoBackingEdit extends BD_Model{
	var $table = 'info_backing_edit';

    public function getBackingByProjectId($project_id){
        if(isset($project_id) && $project_id){
            $this->db->from($this->table);
            $this->db->where('project_id',$project_id);
			$this->db->order_by("created", "desc");
            $a = $this->db->get();
            return $a->result();
        }
    }

	public function checkStatusByProjectId($project_id){
		if(isset($project_id) && $project_id){
			$this->db->from($this->table);
			$this->db->where('project_id',$project_id);
			$this->db->where('status','0');
			$a = $this->db->get();
			return $a->row();
		}
	}

	public function getBackingByProjectId2($project_id,$input = null){
		if(isset($project_id) && $project_id){
			//total data
			$this->db->select('id');
			$this->db->from($this->table);
			$this->db->order_by('id','DESC');
			$this->db->where('project_id', $project_id);
			$total = $this->db->count_all_results();

			$this->db->from($this->table);
			$this->db->where('project_id',$project_id);
			$this->db->order_by("created", "desc");
			if(isset($input) && $input){
				$this->db->limit($input['limit'][0], $input['limit'][1]);
			}
			$data = $this->db->get();
			return ["total" => $total, "data" => $data->result()];
		}
	}

	public function checkBackingByProjectId($project_id){
		if(isset($project_id) && $project_id){
			$this->db->select_max('list_status');
			$this->db->from($this->table);
			$this->db->where('project_id',$project_id);
			$a = $this->db->get();
			return $a->row();
		}
	}

	public function getExistsBackingByProjectId($project_id){
		if(isset($project_id) && $project_id){
			$this->db->from($this->table);
			$this->db->where('project_id',$project_id);
			$this->db->where('status','0');
			$a = $this->db->get();
			return $a->result();
		}
	}

	public function getListBacking($project_id,$list_status){
        if(isset($project_id) && $project_id){
            $this->db->from($this->table);
            $this->db->where('project_id',$project_id);
            $this->db->where('list_status',$list_status);
            $a = $this->db->get();
            return $a->row();
        }
    }

	public function getListBacking2($project_id,$list_status) {
		if(isset($project_id) && $project_id) {
			$this->db->select('id,project_id,max_count,thumnail,now_count,invest_amount,return_amount,created,schedule');
			$this->db->from($this->table);
			$this->db->where('project_id',$project_id);
			$this->db->where('list_status',$list_status);
			$a = $this->db->get();
			return $a->result_array();
		}
	}


	function deleteProjectReturn($id){
		if(isset($id)){
			$this->db->where("id in ($id)");
			// Delete talbe project with id
			$del = $this->db->delete($this->table);
			if($del){
				return true;
			}else{
				return false;
			}
		}else{
			return false;
		}
	}
}
