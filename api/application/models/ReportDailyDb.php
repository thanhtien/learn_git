<?php if(!defined('BASEPATH')) exit('No direct script allowed');

class ReportDailyDb extends BD_Model{
	var $table = 'report_daily';

    public function getTopThree(){

        $this->db->select('report_daily.id,report_daily.project_id,report_daily.thumbnail,report_daily.title,report_daily.updated');
        $this->db->from($this->table);
       // $this->db->where('report_daily.project_id',8888);
        $this->db->order_by('report_daily.updated','DESC');
        $this->db->limit(4);
        $query = $this->db->get();
        $project = $query->result();
        return $project;
    }

}
