<?php if(!defined('BASEPATH')) exit('No direct script allowed');

class News extends BD_Model{
	var $table = 'news';
	// Get News with id
  function getNewId($id){
      $this->db->from($this->table);
      $this->db->where('id',$id);
	  $this->db->where('status','1');
      $param = $this->db->get();
	  if(isset($param)){
		  return $param->row();
	  }else{
		  return false;
	  }
  }
    function getNew2NextPre($id){
      $result['pre'] = 1;
      $result['next'] = 1;
        $result['next_id'] ='';
        $result['pre_id'] ='';
        $this->db->from($this->table);
        $this->db->where('id<',$id);
        $this->db->where('status','1');
        $this->db->order_by("created", 'desc');
        $this->db->limit(1,0);
        $param = $this->db->get();
        $data =[];
        $myData = $param->result();
        $this->db->reset_query();
        // kiem tra neu Pre,neu ko co pre thi lay 2 cai next
        if(count($myData)>0){
            $data[] = $myData[0];
            $result['pre_id'] = $myData[0]->id;
        }else{
            $result['pre'] = 0;
            $this->db->from($this->table);
            $this->db->where('id>',$id);
            $this->db->where('status','1');
            $this->db->order_by("created", 'asc');
            $this->db->limit(2, 0);
            $param = $this->db->get();
            $myData = $param->result();
            $this->db->reset_query();
            if(count($myData)>0){
                $result['next_id'] = $myData[0]->id;
                $data = $myData;
            }
             if(count($data) == 0){
                 $result['next'] = 0;
             }
        }

        if($result['pre'] ==1){
            $this->db->from($this->table);
            $this->db->where('id>',$id);
            $this->db->where('status','1');
            $this->db->order_by("created", 'asc');
            $this->db->limit(1,0);
            $param = $this->db->get();
            $myData = $param->result();
            if(count($myData)>0){
                $data[] = $myData[0];
                $result['next_id'] = $myData[0]->id;
            }else{
                $result['next'] = 0;
            }
        }
        $result['list']  =$data;
        return $result;


    }
  public function getNewsYearMonth(){
    $this->db->select("DATE_FORMAT(created,'%Y/%m/%d') as CreatedMonth,DATE_FORMAT(created,'%Y-%m-%d') as YearMonth ");
    $this->db->from($this->table);
    $this->db->where('status','1');
    $this->db->group_by("DATE_FORMAT(created,'%Y-%m-%d')");
    $this->db->order_by("created", 'desc');
    $a = $this->db->get();
    return $a->result();
   }
    public function getNewsYearMonthInMonth($YearMonth){
        $this->db->select(" `id`,  `title`,  LEFT(`des`, 256),  `thumnail`");
        $this->db->from($this->table);
        $this->db->where('status','1');
        $this->db->where("DATE_FORMAT(created,'%Y-%m-%d')",$YearMonth);
        $this->db->order_by("created", 'desc');
        $a = $this->db->get();
        return $a->result();
    }
}
