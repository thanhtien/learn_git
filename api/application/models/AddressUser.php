<?php if(!defined('BASEPATH')) exit('No direct script allowed');

class AddressUser extends BD_Model{
	var $table = 'address_user';

	public function getAddressUserId($user_id,$id = null){
		if(isset($user_id) && $user_id){
			$this->db->from($this->table);
			$this->db->where('user_id',$user_id);
			$this->db->order_by('updated','desc');
			if(isset($id) && $id){
				$this->db->where('id',$id);
				$address = $this->db->get();
				if(isset($address) && $address){
					return $address->row();
				}else{
					return false;
				}
			}else{
				$address = $this->db->get();
				if(isset($address) && $address){
					return $address->result();
				}else{
					return false;
				}
			}
		}else{
			return false;
		}
	}

	public function getAddressUserDefault($user_id,$id = null){
		if(isset($user_id) && $user_id){
			$this->db->from($this->table);
			$this->db->where('user_id',$user_id);
			if(isset($id) && $id){
				$this->db->where('id',$id);
			}
			$this->db->where('chosen_default','1');
			$address = $this->db->get();
			if(isset($address) && $address){
				return $address->row();
			}else{
				return false;
			}
		}else{
			return false;
		}
	}

	public function createAddressUser($data){
		$query = $this->db->insert($this->table, $data);
		if($query)
		{
			 return $this->db->insert_id();
		}else{
			return FALSE;
		}
	}

	public function getAddressId($id){
		if(isset($id) && $id){
			$this->db->from($this->table);
			$this->db->where('id',$id);
			$address = $this->db->get();
			if(isset($address) && $address){
				return $address->row();
			}else{
				return false;
			}
		}else{
			return false;
		}
	}


	public function editAddressUser($id,$data){
		if(isset($id) && $id){
    	$update = $this->db->update($this->table, $data, array('id' => $id));
			if(isset($update) && $update){
				return true;
			}else{
				return false;
			}
		}else{
			return false;
		}
	}

	public function deleteAddressUser($id){
		if(isset($id) && $id){
			$this->db->where('id',$id);
			$delete = $this->db->delete($this->table);
			if($delete){
				return true;
			}else{
				return false;
			}
		}else{
			return false;
		}
	}
}
