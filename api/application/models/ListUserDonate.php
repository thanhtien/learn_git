<?php if(!defined('BASEPATH')) exit('No direct script allowed');

class ListUserDonate extends BD_Model{
	var $table = 'listuserdonate';

	public function getListUserDonateUserId($user_id){
		if(isset($user_id) && $user_id){
			$this->db->from($this->table);
			$this->db->where('user_id',$user_id);
			$address = $this->db->get();
			if(isset($address) && $address){
				return $address->result();
			}else{
				return false;
			}
		}else{
			return false;
		}
	}

	public function createListUserDonate($data){
		$query = $this->db->insert($this->table, $data);
		if($query)
		{
			 return $this->db->insert_id();
		}else{
			return FALSE;
		}
	}

	public function checkIsDonate($user_id,$project_id){
		if(isset($user_id) && $user_id){
			$this->db->from($this->table);
			$this->db->where('user_id',$user_id);
			$this->db->where('project_id',$project_id);
			$address = $this->db->get();
			if(isset($address) && $address){
				return $address->row();
			}else{
				return false;
			}
		}else{
			return false;
		}
	}

}
