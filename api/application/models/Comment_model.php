<?php if(!defined('BASEPATH')) exit('No direct script allowed');

class Comment_model extends BD_Model{
    var $table = 'comment';

    public function getComment($projectId,$input = null){
        if(isset($projectId) && $projectId){
            $this->db->where('project_id',$projectId);
            if($input){
                $this->db->limit($input['limit'][0], $input['limit'][1]);
            }
            $this->db->order_by('created','DESC');

            $para = $this->db->get($this->table)->result();
            if($para){
                return $para;
            }else{
                return [];
            }
        }else{
            return [];
        }
    }

    public function getAllComment($projectId){
        if(isset($projectId) && $projectId){
            $this->db->select('id');
            $this->db->where('project_id',$projectId);
            $this->db->from($this->table);
            $this->db->order_by('created','DESC');
            $para = $this->db->count_all_results();
            if($para){
                return $para;
            }else{
                return false;
            }
        }else{
            return false;
        }
    }

    public function getCommentId($comment_id){
        if(isset($comment_id) && $comment_id){
            $this->db->where('id',$comment_id);
            $param = $this->db->get($this->table)->row();
            if(isset($param)){
                return $param;
            }else{
                return false;
            }
        }else{
            return false;
        }
    }

    public function deleteComment($id){
        if(isset($id) && $id){
            $this->db->where('id',$id);
            $delete = $this->db->delete($this->table);
            if($delete){
                return true;
            }else{
                return false;
            }
        }else{
            return false;
        }
    }
}
