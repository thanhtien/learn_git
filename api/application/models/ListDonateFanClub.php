<?php if(!defined('BASEPATH')) exit('No direct script allowed');

class ListDonateFanClub extends BD_Model{
	var $table = 'listuserdonate_fanclub';

    public function getAllDonate($project_id,$month){

		if(isset($project_id) && $project_id){
			$this->db->select($this->table.'.*,projects.status_fanclub');
			$this->db->from($this->table);
			$this->db->where('project_id',$project_id);
			$this->db->where('month',$month);
			$this->db->where('status_join_fanclub','0');
			$this->db->where('projects.status_fanclub','progress');
			$this->db->join('projects','projects.id = listuserdonate_fanclub.project_id');
			$this->db->limit(10,0);
			$address = $this->db->get();
			if(isset($address) && $address){
				return $address->result();
			}else{
				return false;
			}
		}else{
			return false;
		}
	}


	public function getAllDonateClose($project_id){

		if(isset($project_id) && $project_id){
			$this->db->select($this->table.'.*,users.email,users.username,projects.project_name');
			$this->db->from($this->table);
			$this->db->where('project_id',$project_id);
			$this->db->where('status_join_fanclub','0');
			$this->db->where('status','0');
			$this->db->join('users','users.id = listuserdonate_fanclub.user_id');
			$this->db->join('projects','projects.id = listuserdonate_fanclub.project_id');
			$this->db->limit(50,0);
			$address = $this->db->get();
			if(isset($address) && $address){
				return $address->result();
			}else{
				return false;
			}
		}else{
			return false;
		}
	}



	public function getAllDonateFanClub($user_id,$input = null){
		if(isset($user_id) && $user_id){

			$this->db->select('id');
			$this->db->from($this->table);
			$this->db->where('user_id',$user_id);
			$total = $this->db->count_all_results();


			$this->db->select($this->table.'.*,
				projects.status_fanclub,
				projects.project_type,
				projects.number_month,
				projects.project_name,
				projects.thumbnail'
			);
			$this->db->from($this->table);
			$this->db->where('listuserdonate_fanclub.user_id',$user_id);
			$this->db->join('projects','projects.id = listuserdonate_fanclub.project_id');
			if($input){
				$this->db->limit($input['limit'][0], $input['limit'][1]);
			}
			$address = $this->db->get();
			$data = $address->result();
			return ["total" => $total, "data" => $data];
		}else{
			return false;
		}
	}

	public function countBackedProject($project_id,$month,$backing_levels_id){
		$this->db->select('id');
		$this->db->from($this->table);
		$this->db->where('project_id',$project_id);
		$this->db->where('month',$month);
		$this->db->where('backing_levels_id',$backing_levels_id);
		$total = $this->db->count_all_results();
		return $total;
	}

	public function createListUserDonate($data){
		$query = $this->db->insert($this->table, $data);
		if($query)
		{
			 return $this->db->insert_id();
		}else{
			return FALSE;
		}
	}

	public function checkIsDonate($user_id,$project_id,$backing_levels_id,$month = null){
			$this->db->from($this->table);
			$this->db->where('user_id',$user_id);
			$this->db->where('project_id',$project_id);
			$this->db->where('backing_levels_id',$backing_levels_id);
			if(isset($month) && $month) {
				$this->db->where('month',$month);
			}
			$address = $this->db->get();
			if(isset($address) && $address){
				return $address->row();
			}else{
				return false;
			}
	}

	public function checkIsDonateSendMail($user_id,$project_id,$month){
		$this->db->from($this->table);
		$this->db->where('user_id',$user_id);
		$this->db->where('project_id',$project_id);
		$this->db->where('month',$month);
		$address = $this->db->get();
		if(isset($address) && $address){
			return $address->row();
		}else{
			return false;
		}
	}

	public function AllDonateClose($user_id,$project_id,$input = null){
		$this->db->from($this->table);
		$this->db->where('user_id',$user_id);
		$this->db->where('project_id',$project_id);
		if(isset($input) && $input){
			$this->db->where('status_join_fanclub','0');
		}
		$address = $this->db->get();
		return $address->result();
	}


	public function getInfoFanClubDonate($user_id,$project_id,$backing_levels_id){
		$this->db->from($this->table);
		$this->db->where('user_id',$user_id);
		$this->db->where('project_id',$project_id);
		$this->db->where('backing_levels_id',$backing_levels_id);
		$address = $this->db->get();
		if(isset($address) && $address){
			return $address->row();
		}else{
			return false;
		}
	}


	public function updateMoreFanClub($id,$data){
		if(isset($id) && isset($data)){
			$this->db->from($this->table);
		    $this->db->where("id in ($id)");
			$user = $this->db->update($this->table,$data);
			if($user){
				return $id;
			}else{
				return false;
			}
		}else{
			return false;
		}
	}
}
