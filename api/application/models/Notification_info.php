<?php if(!defined('BASEPATH')) exit('No direct script allowed');

class Notification_info extends BD_Model{
	var $table = 'notification_info';

	// get all notification
	public function getAllNotification($user_id){
		$this->db->select('id');
		$this->db->from($this->table);
		$this->db->where("(user_id='".$user_id."' OR user_id='0')");
		$this->db->or_where("user_id",'0');
		$amount = $this->db->count_all_results();

	   	$this->db->select('*');
		$this->db->from($this->table);
		$this->db->where("(user_id='".$user_id."' OR user_id='0')");
		$this->db->limit(10, 0);
	   	$this->db->order_by('created','DESC');
	   	// $this->db->group_by("users.id");
	   	$query = $this->db->get();
	   	$notification = $query->result();

		return ["amount" => $amount, "notification" => $notification];
	}

	public function getNumberNotification($user_id){
		$this->db->select('id');
		$this->db->from($this->table);
		$this->db->where("check_status",'0');
		$this->db->where("(user_id='".$user_id."' OR user_id='0')");
		$amount = $this->db->count_all_results();
		return $amount;
	}

	public function getAllNewNotification($user_id){
		$this->db->select('*');
		$this->db->from($this->table);
		$this->db->where("(user_id='".$user_id."' OR user_id='0')");
		$this->db->or_where("user_id",'0');
		$this->db->order_by('created','DESC');
		// $this->db->group_by("users.id");
		$query = $this->db->get();
		$notification = $query->result();

		return $notification;
	}

	public function CountAllNotificationById($user_id){
		$this->db->select('id');
		$this->db->from($this->table);
		$this->db->where("(user_id='".$user_id."' OR user_id='0')");
		$amount = $this->db->count_all_results();
		return $amount;
	}

	public function LoadMoreNotification($user_id,$input){

		$this->db->select('*');
		$this->db->from($this->table);
		if($input){
			$this->db->limit($input['limit'][0], $input['limit'][1]);
		}else{
			$this->db->limit(5, 0);
		}
		$this->db->where("(user_id='".$user_id."' OR user_id='0')");
		$this->db->order_by('created','DESC');
		// $this->db->group_by("users.id");
		$query = $this->db->get();
		$notification = $query->result();
		return $notification;
	}

	public function getNotificationStatus($user_id){
		$this->db->select('*');
		$this->db->from($this->table);
		$this->db->where("(user_id='".$user_id."' OR user_id='0')");
		$this->db->where("check_status",0);
	   	$this->db->order_by('created','DESC');
	   	// $this->db->group_by("users.id");
	   	$query = $this->db->get();
	   	$notification = $query->result();
		return $notification;
	}

	public function getNotificationProject($project_id,$input){
		$this->db->select('id');
		$this->db->from($this->table);
		$this->db->like('link', $project_id);
		$this->db->order_by('created','DESC');
		$total = $this->db->count_all_results();

		$this->db->select('*');
		$this->db->from($this->table);
		$this->db->limit($input['limit'][0], $input['limit'][1]);
		$this->db->like('link', $project_id);
		$this->db->order_by('created','DESC');
		// $this->db->group_by("users.id");
		$query = $this->db->get();
		$notification = $query->result();
		return ["total" => $total, "project" => $notification];
	}
}
