<?php if(!defined('BASEPATH')) exit('No direct script allowed');

class Pickup extends BD_Model{
	var $table = 'pickup_projects';
	// 
  function Pickup_data_projects(){
    $this->db->select('*');
    $this->db->from($this->table);
    // $this->db->join('users','users.id = projects.user_id');
    // $this->db->join('categories','categories.id = projects.category_id');
    $this->db->order_by('created', 'DESC');
    $this->db->limit(8, 0);
    $query = $this->db->get();
    return $query->result();
  }

}
