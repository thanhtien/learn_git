<?php if(!defined('BASEPATH')) exit('No direct script allowed');

class MailBox extends BD_Model{
	var $table = 'mail_box';

	public function getMailSend($user_id,$input = null){
		$this->db->from($this->table);
		$this->db->where('user_id_send',$user_id);
        $total = $this->db->count_all_results();

		$this->db->select($this->table.'.*,users.username,users.profileImageURL,projects.project_name');
		$this->db->from($this->table);
		$this->db->where('mail_box.user_id_send',$user_id);
		$this->db->join('users', 'mail_box.user_id_receive = users.id');
		$this->db->join('projects', 'mail_box.project_id = projects.id');
		$this->db->order_by('mail_box.created', 'desc');
		if($input){
			$this->db->limit($input['limit'][0], $input['limit'][1]);
		}
		$mail = $this->db->get();
		$mail = $mail->result();
		return ["total" => $total, "mail" => $mail];
	}

	public function getMailReceive($user_id,$input = null){
		$this->db->from($this->table);
		$this->db->where('user_id_receive',$user_id);
        $total = $this->db->count_all_results();

		$this->db->select($this->table.'.*,users.username,users.profileImageURL,projects.project_name');
		$this->db->from($this->table);
		$this->db->where('mail_box.user_id_receive',$user_id);
		$this->db->join('users', 'mail_box.user_id_send = users.id');
		$this->db->join('projects', 'mail_box.project_id = projects.id');
		$this->db->order_by('mail_box.created', 'desc');
		if($input){
			$this->db->limit($input['limit'][0], $input['limit'][1]);
		}
		$mail = $this->db->get();
		$mail = $mail->result();
		return ["total" => $total, "mail" => $mail];
	}

	public function getDetailEmailSend($id){
		$this->db->select($this->table.'.*,users.username,users.profileImageURL');
		$this->db->from($this->table);
		$this->db->where('mail_box.id',$id);
		$this->db->join('users', 'mail_box.user_id_receive = users.id');
		$mail = $this->db->get();
		$mail = $mail->row();
		return $mail;
	}

	public function getDetailEmailReceive($id){
		$this->db->select($this->table.'.*,users.username,users.profileImageURL');
		$this->db->from($this->table);
		$this->db->where('mail_box.id',$id);
		$this->db->join('users', 'mail_box.user_id_send = users.id');
		$mail = $this->db->get();
		$mail = $mail->row();
		return $mail;
	}

	public function checkMailBox($id){
		$this->db->from($this->table);
		$this->db->where('id',$id);
		$mail = $this->db->get();
		$mail = $mail->row();
		return $mail;
	}

}
