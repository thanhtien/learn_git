<?php if(!defined('BASEPATH')) exit('No direct script allowed');

class Reply_Comment_model extends BD_Model{
  var $table = 'reply_comment';


    public function getReplyComment($comment_id,$more = null){
        if(isset($comment_id) && $comment_id){
            $this->db->where('comment_id',$comment_id);
            $this->db->order_by('created','DESC');
            if(isset($more) && $more){
            }else{
                $this->db->limit(3);
            }
            $para = $this->db->get($this->table)->result();
            if($para){
                return $para;
            }else{
                return [];
            }
        }else{
            return [];
      }
    }

    public function getReplyCommentId($comment_id){
        if(isset($comment_id) && $comment_id){
            $this->db->where('id',$comment_id);
            $param = $this->db->get($this->table)->row();
            if(isset($param)){
                return $param;
            }else{
                return false;
            }
        }else{
            return false;
        }
    }

    public function getAllReply($projectId){
        if(isset($projectId) && $projectId){
            $this->db->select('id');
            $this->db->where('project_id',$projectId);
            $this->db->from($this->table);
            $this->db->order_by('created','DESC');
            $para = $this->db->count_all_results();
            if($para){
                return $para;
            }else{
                return false;
            }
        }else{
            return false;
        }
    }

    public function RemoveReplyComments($comment_id){
        if(isset($comment_id) && $comment_id){
            $this->db->where("comment_id",$comment_id);
            $query = $this->db->delete($this->table);
            if($query){
                return true;
            }else{
                return false;
            }
        }else{
            return false;
        }
    }

    public function deleteReplyComment($id){
        if(isset($id) && $id){
            $this->db->where('id',$id);
            $delete = $this->db->delete($this->table);
            if($delete){
                return true;
            }else{
                return false;
            }
        }else{
            return false;
        }
    }
}
