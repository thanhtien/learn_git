<?php if(!defined('BASEPATH')) exit('No direct script allowed');

class User extends BD_Model{
	var $table = 'users';
	function getUser(){
		$this->db->from($this->table);
		$data = $this->db->get();
		return $data->result();
	}
	function getUserId($id){
		$this->db->select('users.*,subscription.unsubscribe');
		$this->db->select('users.id as `id`');
		$this->db->from($this->table);
		$this->db->where('users.id',$id);
		$this->db->join('subscription','subscription.email = users.email');
		$user = $this->db->get();
		return $user->row();
	}

	function checkSocical($id){
		$this->db->from($this->table);
		$user = $this->db->get();
		return $user->row();
	}

	function getUserSocialId($id) {
		$this->db->from($this->table);
		$this->db->where('id',$id);
		$user = $this->db->get();
		return $user->row();
	}

	function activeSocial($token_active,$data) {
		$this->db->select('*');
		$this->db->from($this->table);
		$this->db->where('activation_code',$token_active);
		$user = $this->db->update($this->table,$data);
		if($user){
			return $id;
		}else{
			return false;
		}
		$user = $this->db->get();
		return $user->row();
	}

	function getAllUserBlock() {
		$this->db->select('*');
		$this->db->from($this->table);
		$this->db->where('active','0');
		$user = $this->db->get();
		return $user->result();
	}

	function getUsersBankNumber($bank_number){
		$this->db->select('*');
		$this->db->from($this->table);
		$this->db->where('bank_number', $bank_number);
		$data = $this->db->get();
		return $data->row();
	}
	function getUsersStripecvc($last4){
		$this->db->select('*');
		$this->db->from($this->table);
		$this->db->where('last4', $last4);
		$data = $this->db->get();
		return $data->result();
	}


	function getDataEmail($email) {
		if($email){
			$query = $this->db->where('email',$email)->get($this->table);
			return $query->result();
		}else{
			return false;
		}
	}

	function getDataUser($name) {
		if($name){
			$query = $this->db->where('username',$name)->get($this->table);
			return $query->row();
		}else{
			return false;
		}
	}
	function editUser($id,$data){
		if(isset($id) && isset($data)){
			$this->db->from($this->table);
			$this->db->where('id',$id);
			$user = $this->db->update($this->table,$data);
			if($user){
				return $id;
			}else{
				return false;
			}
		}else{
			return false;
		}
	}

	function editUserUnSubscribe($email,$data){
		if(isset($email) && isset($data)){
			$this->db->from($this->table);
			$this->db->where('email',$email);
			$user = $this->db->update($this->table,$data);
			if($user){
				return true;
			}else{
				return false;
			}
		}else{
			return false;
		}
	}

	function checkIdFacebook($facebookId){
		if($facebookId){
			$query = $this->db->where('facebook_id',$facebookId)->get($this->table);
			if(isset($query) && $query){
				return $query->row();
			}else{
				return false;
			}
		}else{
			return false;
		}
	}

	function checkIdTwitter($Twitter){
		if($Twitter){
			$query = $this->db->where('twitter_id',$Twitter)->get($this->table);
			if(isset($query) && $query){
				return $query->row();
			}else{
				return false;
			}
		}else{
			return false;
		}
	}

	public function checkUsername($username){
		if($username){
			$query = $this->db->where('username',$username)->get($this->table);
			if(isset($query) && $query){
				return $query->row();
			}else{
				return false;
			}
		}else{
			return false;
		}
	}

    /**
     * @param $activeCode
     * @return 1:OK,0 active code not found,2 deadline active days
     * @throws Exception
     */
    public function checkActivePassword($activeCode){
        $returnStatus = 0;
        $myData = null;
        if(( $activeCode==null )|| trim($activeCode)==''  ){
            return 0;
        }
        $dateTime = new DateTime();
        $ymd = $dateTime->format('Y-m-d');
        if($activeCode){
            $query = $this->db->where('active_new_password_code',$activeCode)->get($this->table);



            if(isset($query) && $query){
                if($query->num_rows()!=1){
                    $returnStatus= 0;
                }else{
                    $myData = $query->row();
                    $myExpiredDate = DateTime::createFromFormat('Y-m-d H:i:s', $myData->expired_date)->format('Y-m-d');
                    if( $ymd > $myExpiredDate){
                        $returnStatus= 2;
                    }else{
                        $returnStatus= 1;
                    }

                }


            }else{
                $returnStatus= 0;
            }
        }else{
            $returnStatus= 0;
        }
        if($returnStatus==1){
            if($myData!=null){
                $new_pasword = $myData->new_password;

                $this->db->select('*');
                $this->db->from($this->table);
                $this->db->where('id',$myData->id);
                $data['password'] = $new_pasword;
                $data['modified'] =  'NOW()';
                $data['new_password'] =  '';
                $data['active_new_password_code']='';
                $data['expired_date'] = '1900-01-01 11:11:11';
                $this->db->update($this->table,$data);

            }

        }
        return $returnStatus;
    }

	public function getUserJoinAllUser(){
		$this->db->from($this->table);
		$this->db->where('status_all_user','0');
		$this->db->where('token IS NOT NULL', null, false);
		$this->db->limit(10,0);
		$data = $this->db->get();
		return $data->result();
	}

	public function checkUser($email){
		$this->db->select('users.email,users.active');
		$this->db->from($this->table);
		$this->db->where('email',$email);
		$data = $this->db->get();
		return $data->row();
	}
}
