<?php
//for FILE URL
function base_url() {
  return 'https://static.kakuseida.com/';
}
//for APP URL
function app_url(){
    return 'https://kakuseida.com/';
}
function admin_url(){
    return 'https://admin.kakuseida.com/';
}
if (!function_exists('replaceName')) {

   function replaceName($string) {
       $string = trim($string);
       $search = array(
           '#(à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ)#',
           '#(è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ)#',
           '#(ì|í|ị|ỉ|ĩ)#',
           '#(ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ)#',
           '#(ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ)#',
           '#(ỳ|ý|ỵ|ỷ|ỹ)#',
           '#(đ)#',
           '#(À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ)#',
           '#(È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ)#',
           '#(Ì|Í|Ị|Ỉ|Ĩ)#',
           '#(Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ)#',
           '#(Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ)#',
           '#(Ỳ|Ý|Ỵ|Ỷ|Ỹ)#',
           '#(Đ)#',
           "/[^a-zA-Z0-9\-\_]/",
       );
       $replace = array(
           'a',
           'e',
           'i',
           'o',
           'u',
           'y',
           'd',
           'A',
           'E',
           'I',
           'O',
           'U',
           'Y',
           'D',
           '-',
       );
       $string = preg_replace($search, $replace, $string);
       $string = preg_replace('/(-)+/', '-', $string);
       $string = strtolower($string);
       return $string;
   }

}

if(!function_exists('Hash_Data')){
    function Hash_Data($data){
        $stringData = base64_decode($data);
        $ArrayData = json_decode($stringData);
        $myArray = json_decode(json_encode($ArrayData), true);
        return $myArray;
    }
}


if (!function_exists('getTimeProject')) {

    /**
     * [getTimeProject_get description]
     * @param  [type] $time [collection_end_date]
     * @return [type]       [TIME TO APP]
     */
    function getTimeProject($collection_end_date) {

        if(isset($collection_end_date) && $collection_end_date){
            $date1 = new DateTime($collection_end_date);
            $date2 = new DateTime(date('Y-m-d H:i:s'));
            if($date1 > $date2){
                $time = $date1->diff($date2);

                $date = date_diff($date1,$date2);
                $date = $date->format("%a");
                $format_collection_end_date = array(
                    'date' =>$date,
                    'hour' => $time->h,
                    'minutes' => $time->i,
                );
            }else{
                $format_collection_end_date = array(
                    'status'=>'終了'
                );
            }
            return $format_collection_end_date;

        }

    }
}



if (!function_exists('getTimeComment')) {

    /**
     * [getTimeProject_get description]
     * @param  [type] $time [collection_end_date]
     * @return [type]       [TIME TO APP]
     */
    function getTimeComment($collection_end_date) {

        if(isset($collection_end_date) && $collection_end_date){
            $date1 = new DateTime($collection_end_date);
            $date2 = new DateTime(date('Y-m-d H:i:s'));
                $time = $date1->diff($date2);

                $date = date_diff($date1,$date2);
                $date = $date->format("%a");
                $time = array(
                    'date' =>$date,
                    'hour' => $time->h,
                    'minutes' => $time->i,
                    'second' => $time->s,
                );
            return $time;

        }

    }
}


if (!function_exists('getMonthProject')) {

    /**
     * [getMonthProject_get description]
     * @param  [type] $time [collection_end_date]
     * @return [type]       [TIME TO APP]
     */
    function getMonthProject($collection_start_date,$number_month) {

        if(isset($collection_start_date) && $collection_start_date && isset($number_month) && $number_month){
            $date1 = new DateTime($value->collection_start_date);
            $date1 = $date1->format('Y-m-01');
            $date2 = new DateTime();
            $date2 = $date2->format('Y-m-01');

            $ts1 = strtotime($date1);
            $ts2 = strtotime($date2);

            $year1 = date('Y', $ts1);
            $year2 = date('Y', $ts2);

            $month1 = date('m', $ts1);
            $month2 = date('m', $ts2);

            $diff = (($year2 - $year1) * 12) + ($month2 - $month1);

            // create month = ?month / month of project
            if((int)$diff >= (int)$number_month){
                $newmonth = ($diff/$number_month);
                $checkMonth = ceil($diff/$number_month);
                if($newmonth === (int)$checkMonth){
                    $newmonth = $checkMonth;
                }else {
                    $newmonth = $checkMonth - 1;
                }
            }else {
                $newmonth = 0;
            }
            return $month;
        }

    }
}


if (!function_exists('getImage')) {

    /**
     * [getTimeProject_get description]
     * @param  [type] $time [collection_end_date]
     * @return [type]       [TIME TO APP]
     */
    function getImage($linkImage) {
        // get location of last / in image
        $location = strripos($linkImage, '/');
        // replcace last / -> /medium_
        $medium = substr_replace($linkImage,'/medium_',$location,1);
        // replcace last / -> /small
        $small = substr_replace($linkImage,'/small_',$location,1);
        // create array
        $data = array(
            'larger' => $linkImage,
            'medium' => $medium,
            'small' => $small
        );
        // return
        return $data;
    }

}

?>
