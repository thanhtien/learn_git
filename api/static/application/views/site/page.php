
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title></title>
	<link rel="stylesheet" href="">
	<meta charset="utf-8">
	<title>elFinder 2.1.x source version with PHP connector</title>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=2" />

	<!-- Section CSS -->
	<!-- jQuery UI (REQUIRED) -->
	<link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">

	<!-- elFinder CSS (REQUIRED) -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/plugins/elFinder/') ?>css/elfinder.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/plugins/elFinder/') ?>css/theme.css">
	<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<!--<![endif]-->
	<script src="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>

	<!-- elFinder JS (REQUIRED) -->
	<script src="<?php echo base_url('assets/plugins/elFinder/') ?>js/elfinder.min.js"></script>

	<!-- Extra contents editors (OPTIONAL) -->
	<script src="<?php echo base_url('assets/plugins/elFinder/') ?>js/extras/editors.default.js"></script>

	<script src="<?php echo base_url('assets/plugins/elFinder/') ?>/js/i18n/elfinder.ca.js"></script>

	<!-- GoogleDocs Quicklook plugin for GoogleDrive Volume (OPTIONAL) -->
	<!--<script src="js/extras/quicklook.googledocs.js"></script>-->

	<!-- elFinder initialization (REQUIRED) -->
	<script type="text/javascript" charset="utf-8">
			// Documentation for client options:
			// https://github.com/Studio-42/elFinder/wiki/Client-configuration-options
			$(document).ready(function() {
				$('#elfinder').elfinder(
					// 1st Arg - options
					{
						cssAutoLoad : false,               // Disable CSS auto loading
						url : '<?php echo base_url(); ?>/home/elfinder_init'  // connector URL (REQUIRED)
						// , lang: 'ru'                    // language (OPTIONAL)
					},
					// 2nd Arg - before boot up function
					function(fm, extraObj) {
						// `init` event callback function
						fm.bind('init', function() {
							// Optional for Japanese decoder "extras/encoding-japanese.min"
							delete fm.options.rawStringDecoder;
							if (fm.lang === 'jp') {
								fm.loadScript(
									[ fm.baseUrl + 'js/extras/encoding-japanese.min.js' ],
									function() {
										if (window.Encoding && Encoding.convert) {
											fm.options.rawStringDecoder = function(s) {
												return Encoding.convert(s,{to:'UNICODE',type:'string'});
											};
										}
									},
									{ loadType: 'tag' }
								);
							}
						});
						// Optional for set document.title dynamically.
						var title = document.title;
						fm.bind('open', function() {
							var path = '',
								cwd  = fm.cwd();
							if (cwd) {
								path = fm.path(cwd.hash) || null;
							}
							document.title = path? path + ':' + title : title;
						}).bind('destroy', function() {
							document.title = title;
						});
					}
				);
			});
	</script>
</head>
<body>
	<div id="elfinder">

	</div>
</body>
</html>
