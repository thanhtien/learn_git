

// Change Tab of Project Edit
$("#tabs").tabs({
    active:$('#tab').val(),
    activate: function (event, ui) {
        var active = $('#tabs').tabs('option', 'active');
    }
});

// Change Tab of Project Edit
$("#Child_tabs").tabs({
    active:0,
    activate: function (event, ui) {
        var active = $('#Child_tabs').tabs('option', 'active');
    }
});
// PreView Project edit/add (Change Tab)
$("#tab_pr1").click(function(){
    $( "#tab_pr1" ).addClass( "active" );
    $( "#tab_pr2" ).removeClass( "active" );
    $( "#tabPre-1" ).addClass( "active" );
    $( "#tabPre-2" ).removeClass( "active" );
});
// PreView Project edit/add (Change Tab)
$("#tab_pr2").click(function(){
  $( "#tab_pr2" ).addClass( "active" );
  $( "#tab_pr1" ).removeClass( "active" );
  $( "#tabPre-2" ).addClass( "active" );
  $( "#tabPre-1" ).removeClass( "active" );
});
// Do table dot dot dot
$(".comment").shorten({
    "showChars" : 20,
});
// Do table dot dot dot
$(".comment_return").shorten({
    "showChars" : 42,
});
// Do table dot dot dot
$(".title_shorten").shorten({
    "showChars" : 20,
});
// Do table dot dot dot
$(".content_shorten").shorten({
    "showChars" : 120,
});
// datepicker format
$('.input-group.date').datepicker({format: "yyyy-mm-dd"});
// If click out calendar
$('.input-group.date').on('changeDate', function(ev){
    $(this).datepicker('hide');
});
// Page Load Success
$(document).ready(function() {


    $("#common_loading").delay(600).fadeOut();
    // Page Load Success remove common_loading
    if ($('#common_loading').hasClass('show')) {

        $('#common_loading').removeClass('show');
    }
    // Select username when create/edit project
    $('#itemName').select2({
        // 3 character then run load username
        minimumInputLength: 3,
        placeholder: '--- ユーザー名を選択する ---',
        language: {
            // setting announce when not found
            noResults: function (params) {
                return "該当な結果がありません.";
            },
            // setting announce when not input 3 character
            inputTooShort: function(params){
                if(params['input'].length == 0){
                    return "3文字以上を入力すると、自動的に出ます.";
                }
                if(params['input'].length == 1){
                    return "2文字以上を入力すると、自動的に出ます.";
                }
                if(params['input'].length == 2){
                    return "1文字以上を入力すると、自動的に出ます.";
                }
            }
        },
        // ajax call
        ajax: {
            url: '/search',
            dataType: 'json',
            delay: 500,
            processResults: function (data) {
                data.forEach(function(element) {
                });
                return {
                    results: data
                };
            },
            cache: true
        }
    });


    // Call Popup of return project hide
    $(".pop_upReturn").hide();

        // Socket

        // var socket = io.connect( 'http://localhost:8080' );
        // socket.on('message',function(data){
        //   console.log(data);
        // });
        // console.log('123123');
        // var message = {
        //   id: '1',
        //   content : 'content',
        // };

        // $( "#accept" ).click(function() {
        //     if($("#accept").val() === '1'){
        //         var status = 'public'
        //     }else{
        //         status = 'unpublic'
        //     }
        //     var projectname = $("#project_name").val();
        //     var idUser = $("#itemName").val();
        //     var message = 'Your Project "'+projectname+'" is '+status+'';
        //     var data = {
        //         id:idUser,
        //         message: message
        //     };
        //     // socket.emit("helloworld",data);
        // });
        // socket.emit("helloworld",message);


        // Socket
    // When Click td -> turn off event show modal in project return
    $("#table_pop_up td .editDonate").click(function(event){
        event.stopPropagation();
    });
    $(".trTableProjectReturn #inputchecked").click(function(event){
        event.stopPropagation();
    });
        event.stopPropagation();
    $('#sendmailPreview').click(function(){
        $loading.show();
    });
    // Sendmail template
    $("#preViewEmailTemplate td .sendmail").click(function(event){
        // get id in tr(.sendmail -> td -> tr)
        $loading.show();
        var id = $(this).parent().parent().attr('id');
        // // .ajaxStart(function () {
        // //     $loading.show();
        // // })
        // // .ajaxStop(function () {
        // //     $loading.hide();
        // // });
        // // stop event popup show preview
        event.stopPropagation();
        // // Call ajax
        $.ajax({
            url: base_url+'EmailTemplate/sendMailTemplate',
            type: 'post',
            dataType: 'json',
            data: {'template': id},
            success: function(result){
                console.log(result);
                alert('成功！メール送信ができました。');
                $loading.hide();
            },
            error: function(err){
                console.log(err);
                $loading.hide();
            }
        });
    });
    // Check Search = '' not click search
    if($('#srch-term').val() === ''){
        document.getElementById("btn-srch").disabled = true;
    }
    // Check Search = '' not click search
    $('#srch-term').bind('input', function() {
        // Search if input = '' then disable and else
        if($(this).val() === ''){
            document.getElementById("btn-srch").disabled = true;
        }else{
            document.getElementById("btn-srch").disabled = false;
        }
    });
    // if click un_schedule -> disable calendar
    var checkBox = document.getElementById("un_schedule");
    // If checkBox exists
    if(checkBox){
        // Check checkBox
        if(checkBox.checked === true){
            // disable
            document.getElementById("schedule").disabled = true;
        }else{
            document.getElementById("schedule").disabled = false;
        }
        // check
        if(document.getElementById('un_schedule')){
            // change checkBox flow un_schedule
            document.getElementById('un_schedule').onchange = function() {
                if(checkBox.checked === true){
                    document.getElementById("schedule").disabled = true;
                    $(".input-group.date").datepicker({ enableOnReadonly: false });
                }else{
                    document.getElementById("schedule").disabled = false;
                }
            };
        }
    }

    // Upload image of Profile User
    $("#profile-img-user").on("change", function() {
        // data
        var data = new FormData();
        // file image
        var file = $('form input[type=file]')[0].files[0];
        // assign file
        data.append('files',file);
        // ajax call
        $.ajax({
            url: API_URL+'api/CreateUpload/',
            processData: false,
            contentType: false,
            data: data,
            type: 'POST'
        }).done(function(result) {
            // success
            data = result['base_url']+result['forder']+result['name_image']+result['mime_type'];
            $('#hiden_image').val(data);
            $("#announce").text('');
        }).fail(function(error) {
            // failed
            if(error['responseText'] == '"a"'){
            }else if(error['responseText'] == '"The filetype you are attempting to upload is not allowed."'){
                $("#announce").text('画像ファイルの形式はJPG、JPEG、PNGだけでお願いします。');
            }else{
                $("#announce").text('アップロードされた画像の容量は2MB以上ですので、2MB以下の画像を再度アップロードしてください。');
            }
        });
    });

    // Upload image for news
    $("#new-img").on("change", function() {
        // get user_id
        var user = $("#idNew").val();
        // create FormData
        var data = new FormData();
        // get file image
        var file = $('form input[type=file]')[0].files[0];
        // assign file + user_id
        data.append('files',file);
        data.append('new',user);
        // ajax call
        $.ajax({
            url: API_URL+'api/CreateUpload/new',
            processData: false,
            contentType: false,
            data: data,
            type: 'POST'
        }).done(function(result) {
            // sucess
            data = result['base_url']+result['forder']+result['name_image']+result['mime_type'];
            $('#hiden_image').val(data);
            $("#announce").text('');
        }).fail(function(error) {
            // failed
            if(error['responseText'] == '"a"'){
            }else if(error['responseText'] == '"The filetype you are attempting to upload is not allowed."'){
                $("#announce").text('画像ファイルの形式はJPG、JPEG、PNGだけでお願いします。');
            }else{
                $("#announce").text('アップロードされた画像の容量は2MB以上ですので、2MB以下の画像を再度アップロードしてください。');
            }
        });
    });

    // upload image
    // $("#new-img-tag").on("change", function() {
    //     console.log('123');
    //     // var data = new FormData();
    //     // var file = $('form input[type=file]')[0].files[0];
    //     // data.append('files',file);
    //     // $.ajax({
    //     //     url: API_URL+'api/CreateUpload/',
    //     //     processData: false,
    //     //     contentType: false,
    //     //     data: data,
    //     //     type: 'POST'
    //     // }).done(function(result) {
    //     //     data = result['base_url']+result['forder']+result['name_image']+result['mime_type'];
    //     //     $('#hiden_image').val(data);
    //     //     $("#announce").text('');
    //     // }).fail(function(error) {
    //     //     if(error['responseText'] == '"a"'){
    //     //     }else if(error['responseText'] == '"The filetype you are attempting to upload is not allowed."'){
    //     //         $("#announce").text('画像ファイルの形式はJPG、JPEG、PNGだけでお願いします。');
    //     //     }else{
    //     //         $("#announce").text('アップロードされた画像の容量は2MB以上ですので、2MB以下の画像を再度アップロードしてください。');
    //     //     }
    //     // });
    // });


    // Upload image
    $("#profile-img-return").on("change", function() {
        var user = $("#user_id").val();
        if(user == null){
            user = '1';
        }
        var data = new FormData();
        var file = $('form input[type=file]')[0].files[0];
        data.append('files',file);
        data.append('username',user);
        $.ajax({
            url: API_URL+'api/CreateUpload/thumbnailReturn',
            processData: false,
            contentType: false,
            data: data,
            type: 'POST'
        }).done(function(result) {
            data = result['base_url']+result['forder']+result['name_image']+result['mime_type'];
            $('#hiden_image').val(data);
            $("#announce").text('');
        }).fail(function(error) {
            if(error['responseText'] == '"a"'){
            }else if(error['responseText'] == '"The filetype you are attempting to upload is not allowed."'){
                $("#announce").text('画像ファイルの形式はJPG、JPEG、PNGだけでお願いします。');
            }else{
                $("#announce").text('アップロードされた画像の容量は2MB以上ですので、2MB以下の画像を再度アップロードしてください。');
            }
        });
    });

    $("#uploadfile").on("click", function() {
        var data = new FormData();
        var file = $('form input[type=file]')[0].files[0];
        data.append('files',file);
        $.ajax({
            url: API_URL+'api/CreateUpload/uploadFile',
            processData: false,
            contentType: false,
            data: data,
            type: 'POST'
        }).done(function(result) {
            console.log(result);
        }).fail(function(error) {
            console.log(error);
        });
    });

    $("#profile-img").on("change", function() {
        var user = $("#itemName").val();
        if(user == null){
            user = '1';
        }
        var data = new FormData();
        var file = $('form input[type=file]')[0].files[0];
        data.append('files',file);
        data.append('username',user);
        $.ajax({
            url: API_URL+'api/CreateUpload/uploads',
            processData: false,
            contentType: false,
            data: data,
            type: 'POST'
        }).done(function(result) {
            data = result['base_url']+result['forder']+result['name_image']+result['mime_type'];
            $('#hiden_image').val(data);
            $("#announce").text('');
        }).fail(function(error) {
            if(error['responseText'] == '"a"'){
            }else if(error['responseText'] == '"The filetype you are attempting to upload is not allowed."'){
                $("#announce").text('画像ファイルの形式はJPG、JPEG、PNGだけでお願いします。');
            }else{
                $("#announce").text('アップロードされた画像の容量は2MB以上ですので、2MB以下の画像を再度アップロードしてください。');
            }
        });
    });

    $("#profile_thumnail1").on("change", function() {
        var user = $("#itemName").val();
        if(user == null){
            user = '1';
        }
        var data = new FormData();
        var file = $('form input[type=file]')[1].files[0];
        data.append('files',file);
        data.append('username',user);
        $.ajax({
            url: API_URL+'api/CreateUpload/thumbnailDes',
            processData: false,
            contentType: false,
            data: data,
            type: 'POST'
        }).done(function(result) {
            data = result['base_url']+result['forder']+result['name_image']+result['mime_type'];
            $('#hiden_image1').val(data);
            $("#announce1").text('');
        }).fail(function(error) {
            if(error['responseText'] == '"a"'){
            }else if(error['responseText'] == '"The filetype you are attempting to upload is not allowed."'){
                $("#announce1").text('画像ファイルの形式はJPG、JPEG、PNGだけでお願いします。');
            }else{
                $("#announce1").text('アップロードされた画像の容量は2MB以上ですので、2MB以下の画像を再度アップロードしてください。');
            }
        });
    });

    $("#profile_thumnail2").on("change", function() {
        var user = $("#itemName").val();
        if(user == null){
            user = '1';
        }
        var data = new FormData();
        var file = $('form input[type=file]')[2].files[0];
        data.append('files',file);
        data.append('username',user);
        $.ajax({
            url: API_URL+'api/CreateUpload/thumbnailDes',
            processData: false,
            contentType: false,
            data: data,
            type: 'POST'
        }).done(function(result) {
            data = result['base_url']+result['forder']+result['name_image']+result['mime_type'];
            $('#hiden_image2').val(data);
            $("#announce2").text('');
        }).fail(function(error) {
            if(error['responseText'] == '"a"'){
            }else if(error['responseText'] == '"The filetype you are attempting to upload is not allowed."'){
                $("#announce2").text('画像ファイルの形式はJPG、JPEG、PNGだけでお願いします。');
            }else{
                $("#announce2").text('アップロードされた画像の容量は2MB以上ですので、2MB以下の画像を再度アップロードしてください。');
            }
        });
    });

    $("#profile_thumnail3").on("change", function() {
        var user = $("#itemName").val();
        if(user == null){
            user = '1';
        }
        var data = new FormData();
        var file = $('form input[type=file]')[3].files[0];
        data.append('files',file);
        data.append('username',user);
        $.ajax({
            url: API_URL+'api/CreateUpload/thumbnailDes',
            processData: false,
            contentType: false,
            data: data,
            type: 'POST'
        }).done(function(result) {
            data = result['base_url']+result['forder']+result['name_image']+result['mime_type'];
            $('#hiden_image3').val(data);
            $("#announce3").text('');
        }).fail(function(error) {
            if(error['responseText'] == '"a"'){
            }else if(error['responseText'] == '"The filetype you are attempting to upload is not allowed."'){
                $("#announce3").text('画像ファイルの形式はJPG、JPEG、PNGだけでお願いします。');
            }else{
                $("#announce3").text('アップロードされた画像の容量は2MB以上ですので、2MB以下の画像を再度アップロードしてください。');
            }
        });
    });

    // SubMenu add icon
    if ($('#submenu').hasClass('active')) {
        $("#menu_sub").html(`<i class="fa fa-minus"></i>`);
    }else{
        $("#menu_sub").html(`<i class="fa fa-plus"></i>`);
    }
    // use validate of jQuery
    $("#demoForm").validate({
        // create rules
	    rules: {
			"project_name": {
			    required: true,
                maxlength: 150
		    },
            "summary":{
                maxlength:1000
            },
            "video": {
                url: true,
            },
            "itemName":{
                required: true,
            },
            "goal_amount": {
                required: true,
                number:true
            },
            "date":{
                required: true,
            }
	    },
        // show message if error
        messages: {
            "project_name": {
                required: "プロジェクト名を入力してください。",
                maxlength:"プロジェクト名は最大150文字までです。",
            },
            "summary":{
                maxlength:"「はじめにご挨拶」の文書は最大1000文字までです。",
            },
            "video": {
                url: "動画のURLを入力してください",
            },
            "itemName": {
                required: "ユーザー名を入力してください",
            },
            "goal_amount": {
                required: "目標金額を入力してください",
                number: "数字を入力してください"
            },
            "date": {
                required: "募集終了日を入力してください",
            }
        },
        // show error & hidden error
        highlight: function(element) {
            $(element).addClass('error');
        }, unhighlight: function(element) {
            $(element).removeClass('error');
        }
	});

    // use validate of jQuery

    $("#uploadEmailTemplate").validate({
        // create rules
        rules: {
            "file": {
                required: true,
            },
            "title": {
                required: true,
            },
            "name":{
                required: true,
                noSpace: true,
            }
        },
        // show message if error
        messages: {
            "file": {
                required: "HTMLファイルを選択してください",
            },
            "title": {
                required: "サブジェクトを入力してください。",
            },
            "name":{
                required: "メールテンプレート名を入力してください。",
                noSpace: "ローマ字・数字だけで入力してください",
            }
        },
        // show error & hidden error
        highlight: function(element) {
            $(element).addClass('error');
        }, unhighlight: function(element) {
            $(element).removeClass('error');
        }
    });

    // use validate of jQuery
    $("#sendMailTemplateGroup").validate({
        // create rules
        rules: {
            "project_id": { valueNotEquals: "default"}
        },
        // show message if error
        messages: {
            "project_id": {valueNotEquals: "default"}
        },
        // show error & hidden error
        highlight: function(element) {
            $(element).addClass('error');
        }, unhighlight: function(element) {
            $(element).removeClass('error');
        }
    });    // user validate jQuery
    $("#ProjectReturn").validate({
        rules: {
            "amount": {
                required: true,
                number:true
            },
            "content":{
                required: true,
            }
        },
        messages: {
            "amount": {
                required: "支援額を入力してください",
                number:"数字を入力してください"
            },
            "content":{
                required:"リターン品・説明文を入力してください"
            }
        },highlight: function(element) {
            $(element).addClass('error');
        }, unhighlight: function(element) {
            $(element).removeClass('error');
        }
    });
    jQuery.validator.addMethod("noSpace", function(value, element) {
        return value.indexOf(" ") < 0 && value != "";
    }, "No space please and don't leave it empty");

    // user validate jQuery
    $("#newUser").validate({
        rules: {
            "username": {
                required: true,
                noSpace: true,
            },
            "email":{
                required: true,
                email:true
            },
            "password":{
                required: true,
                minlength: 6,
                maxlength: 32
            },
            "re_pass":{
                required: true,
                equalTo:"#password"
            }
        },
        messages: {
            "username": {
                required: "ユーザー名を入力してください",
                noSpace: "ユーザー名には間隔を入れないでください"
            },
            "email":{
                required: "メールアドレスを入力してください",
                email:"メールアドレスが正しくないので、確認してください"
            },
            "password":{
                required: "パスワードを入力してください",
                minlength: "パスワードは6術～32術まで入力してください",
                maxlength: "パスワードは6術～32術まで入力してください"
            },
            "re_pass":{
                required: "パスワード確認を入力してください",
                equalTo:"パスワード確認は以上のパスワードと合っていません。確認してください。"
            }
        },
        highlight: function(element) {
            $(element).addClass('error');
        }, unhighlight: function(element) {
            $(element).removeClass('error');
        },
        submitHandler : function(form) {
            form.submit();
            $(".loading").css("display", "block");
        }
    });
});

    // create function load image
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#profile-img-tag').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
    // create function load image
    function readURL1(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#profile_thumnail1-tag').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
    // create function load image
    function readURL2(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#profile_thumnail2-tag').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
    // create function load image
    function readURL3(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#profile_thumnail3-tag').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
    // create function load image
    function readURL4(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#thumbnail_movie-tag').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
    // create function load image
    function readURL5(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#profile-img-return-tag').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
    // create function load image
    function readURL6(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#new-img-tag').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
    // if upload image success then show image -> tag
    $("#profile-img").change(function(){
        readURL(this);
    });
    // if upload image success then show image -> tag
    $("#profile-img-user").change(function(){
        readURL(this);
    });
    // if upload image success then show image -> tag
    $("#profile-img-return").change(function(){
        readURL5(this);
    });
    // if upload image success then show image -> tag
    $("#profile_thumnail1").change(function(){
        readURL1(this);
    });
    // if upload image success then show image -> tag
    $("#profile_thumnail2").change(function(){
        readURL2(this);
    });
    // if upload image success then show image -> tag
    $("#profile_thumnail3").change(function(){
        readURL3(this);
    });
    // if upload image success then show image -> tag
    $("#new-img").change(function(){
        readURL6(this);
    });


    // popup of block user hidden
    $(".popUpBlockUser").hide();

    // chosen tr of table and redirect project edit
    $('#tableProject .trTableProject').click(function() {
        var id = $(this).attr('id');
        window.location.replace(BASE_URL+'project/edit/'+id);
    });
    $('#MailGroup').click(function() {
        $loading.show();
    });
    // chosen tr of table and redirect category edit
    $('#tableCategory .trTableCategory').click(function() {
        var id = $(this).attr('id');
        window.location.replace(BASE_URL+'category/edit/'+id);
    });
    // chosen tr of table and redirect projectreturn edit
    $('#tableProjectReturn .trTableProjectReturn').click(function() {
        var id = $(this).attr('id');
        window.location.replace(BASE_URL+'projectreturn/edit/'+id);
    });
    $('#tableProjectReturnActive .trTableProjectReturnActive').click(function() {
        var id = $(this).attr('id');
        window.location.replace(BASE_URL+'projectreturn/editActive/'+id);
    });    // chosen tr of table and redirect dashboard edit
    $('#tableDashBoard .trTableDashBoard').click(function() {
        var id = $(this).attr('id');
        window.location.replace(BASE_URL+'dashboard/edit/'+id);
    });
    // blockUser when click in icon blockUser
    $('#test tr p').click(function() {
        // get id
        var id = $(this).parent().parent().attr('id');
        // hidden all popup
        $(".popUpBlockUser").hide();
        // show popup current(flow id)
        $(".popUpBlockUser"+id).show();
        // if click yes in popup
        $(".yes_blockuser"+id).click(function(){
            // hidden popUP
            $(".popUpBlockUser").hide();
            // call ajax
            $loading.show();
            $.ajax({
                url: base_url+'users/blockUser',
                type: 'post',
                dataType: 'json',
                data: {'id': id, 'active':'0'},
                success: function(result){
                    // success empty line(tr td current) and use append create tr td new
                    $("#"+id).empty();
                    $("#"+id).append(`
                        <td><div class="icheckbox_minimal-blue" style="position: relative;"><input type="checkbox" class="minimal checkitem" name="val[]" value="77" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div></td>
                        <td>
                            <a href="/users/edit/${id}" style="color:black">${result.username}</a>
                        </td>
                        <td>
                            <a href="/users/edit/${id}" style="color:black">${result.email}</a>
                        </td>
                        <td>
                            <a href="/users/edit/${id}" style="color:black">${result.created}</a>
                        </td>
                        <td>
                            <a href="/users/edit/${id}" style="color:black">${result.role}</a>
                        </td>
                        <td>
                            <span style="color:red">ブロック</span>
                        </td>
                        <td style="text-align:center">
                            <a id="block_user" class="btn btn-xs btn-danger"><i class="fa fa-ban"></i></a>
                            <a href="/users/edit/${id}" class="btn btn-xs btn-primary"><i class="fa fa-edit"></i></a>
                        </td>`
                    );
                    $loading.hide();
                },
                // show error
                error: function(err){
                    console.log(err);
                    $loading.hide();
                }
            });
        })
        // if click no then hidden all popup block user
        $(".no_blockuser").click(function(){
            $(".popUpBlockUser").hide();
        });
    });
    // preview for create project
    $("#setPreview").click(function(){
        // get value of project
        var goal_amount = $("#goal_amount").val();
        var description = CKEDITOR.instances.description.getData();
        var project_name = $("#project_name").val();
        var itemName = $("#itemName").val();
        var summary = $("#summary").val();
        var category = $("#category").val();
        var hiden_image = $("#hiden_image").val();
        var newyoutube = $(".youtubevideo").val();
        var date = $("#date").val();
        var d = new Date();

        var month = d.getMonth()+1;
        var day = d.getDate();

        var output = d.getFullYear() + '-' +
        (month<10 ? '0' : '') + month + '-' +
        (day<10 ? '0' : '') + day;

        // check date
        if(date){
            // use date_diff show how many day
            var date_diff_indays = function(date1, date2) {
                dt1 = new Date(date1);
                dt2 = new Date(date2);
                return Math.floor((Date.UTC(dt2.getFullYear(), dt2.getMonth(), dt2.getDate()) - Date.UTC(dt1.getFullYear(), dt1.getMonth(), dt1.getDate()) ) /(1000 * 60 * 60 * 24));
            }
            var end_date = date_diff_indays(output , date);
            if(end_date >= 0){
                $(".row-3 .row-number").text(end_date+'日');
            }else{
                $(".row-3 .row-number").text('終了');
            }
        }else{
            $(".row-3 .row-number").text('0日');
        }
        // check goal_amount
        if(goal_amount){
            $(".row-1 .row-note").text('目標金額は'+goal_amount+'円');
        }else{
            $(".row-1 .row-note").text('目標金額は0円');
        }
        // check summary
        if(summary){
            $(".pd-box-content-intro .pd-box-text").text(summary);
        }else{
            $(".pd-box-content-intro .pd-box-text").text(summary);
        }
        // check project_name
        if(project_name){
            $(".pannel-head").text(project_name);
        }else{
            $(".pannel-head").text("Preview");
        }
        // check hiden_image
        if(hiden_image){
            $('#slider_img').attr({
                'src': hiden_image,
                'alt': 'Thumbnail'
            });
        }else{
            $('#slider_img').attr({
                'src': IMAGE_URL+"images/2018/default/noimage-01.png",
                'alt': 'Thumbnail'
            });
        }

        var ID = '';
        newyoutube = newyoutube.replace(/(>|<)/gi,'').split(/(vi\/|v=|\/v\/|youtu\.be\/|\/embed\/)/);
        if(newyoutube[2] !== undefined) {
            ID = newyoutube[2].split(/[^0-9a-z_\-]/i);
            ID = ID[0];
        } else {
            newyoutube = newyoutube[0].replace(/(>|<)/gi,'').split(/(vi\/|v=|\/v\/|y2u\.be\/|\/embed\/)/);
            if(newyoutube[2] !== undefined) {
                ID = newyoutube[2].split(/[^0-9a-z_\-]/i);
                ID = ID[0];
            }
        }

        // call ajax
        $.ajax({
            url: base_url+'project/preView',
            type: 'post',
            dataType: 'json',
            data: {'user_id': itemName},
            success: function(result){
                // success show data
                $('#img_user').attr({
                    'src': result.profileImageURL,
                    'alt': 'Thumbnail'
                });
                $('.playvideo').attr({
                    'src': '//www.youtube.com/embed/'+ID+'?rel=0',
                });
                $(".playvideo").css("z-index", "-1");
                if(ID){
                    $('#clickYoutube').on('click', function(ev) {
                        console.log($(".playvideo")[0].src);
                        $(".playvideo").css("z-index", "11111111");
                        $(".playvideo")[0].src += "&autoplay=1";
                        ev.preventDefault();
                    });
                }else{
                    $('#clickYoutube').on('click', function(ev) {
                        $(".playvideo").css("z-index", "-1");
                    });
                }
                if(result.username){
                    $(".pd-box-text-name").text('氏名 ：'+result.username);
                }else{
                    $(".pd-box-text-name").text('氏名 ：');
                }
                if(result.address){
                    $(".pd-box-text-address").text('現在地 ：'+result.address);
                }else{
                    $(".pd-box-text-address").text('現在地 :');
                }
                if(result.job){
                    $(".pd-box-text-job").text('職業 ：'+result.job);
                }else{
                    $(".pd-box-text-job").text('職業 ：');
                }

            },
            // show error
            error: function(err){
                console.log(err);
            }
        });
        // check description
        if(description){
            $("#tabPre-1").html(description);
        }else{
            $("#tabPre-1").html('');
        }
    });



    // preview for create project
    $("#setPreviewNotification").click(function(){
        // get value of project
        var title = $("#title").val();
        var message = CKEDITOR.instances.description.getData();

        if(title){
            $("#title_preview").text(title);
        }else{
            $("#title_preview").text("Preview");
        }

        if(message){
            $("#tabPre-1").html(message);
        }else{
            $("#tabPre-1").html('<p>Preview</p>');
        }
    });

    // preview of create News
    $("#previewNew").click(function(){
        // get data
        var title = $("#title").val();
        var content = CKEDITOR.instances.description.getData();
        var image = $("#hiden_image").val();
        // check title_preivew
        if(title) {
            $("#title_preivew").html(title);
        }
        // check content
        if(content) {
            $(".content_preview").html(content);
        }
        // check image
        if(image) {
            $('.img_thumnail').attr({
                'src': image,
                'alt': 'Thumbnail'
            });
        }
    });
    // hide loading
    var $loading = $('.loading').hide();
    // use ajax loading show and else
    // $(document)
    // .ajaxStart(function () {
    //     $loading.show();
    // })
    // .ajaxStop(function () {
    //     $loading.hide();
    // });
    //
    $( "#table_pop_up td #showPopUp" ).click(function() {
        var id = $(this).parent().attr('id');
        $(".pop_upReturn"+id).toggle();
    });

    // check video checked in (project add & edit)
    if($('#Video').is(':checked')) {
        // show video and hide image
        document.getElementById("video").style.display = "block";
        document.getElementById("image_thumnail").style.display = "none";
    }
    // check iamge checked in (project add & edit)
    if($('#Image').is(':checked')) {
        // show image and hide video
        document.getElementById("video").style.display = "none";
        document.getElementById("image_thumnail").style.display = "block";
    }
    // show or hidden flow image
    $("#Image").click(function(){
        document.getElementById("video").style.display = "none";
        document.getElementById("image_thumnail").style.display = "block";
    });
    // show or hidden flow Video
    $("#Video").click(function(){
        document.getElementById("video").style.display = "block";
        document.getElementById("image_thumnail").style.display = "none";
    });
    // disable or not disable password + re_pass in edit user ( flow checkme)
    if(document.getElementById('checkme')){
        document.getElementById('checkme').onchange = function() {
            document.getElementById('password').disabled = !this.checked;
            document.getElementById('re_pass').disabled = !this.checked;
        };
    }
    // format money (text (number))
    $('.money').simpleMoneyFormat();
    function validate(evt) {
        var theEvent = evt || window.event;

        // Handle paste
        if (theEvent.type === 'paste') {
            key = event.clipboardData.getData('text/plain');
        } else {
            // Handle key press
            var key = theEvent.keyCode || theEvent.which;
            key = String.fromCharCode(key);
        }
        var regex = /[0-9]|\./;
        if( !regex.test(key) ) {
            theEvent.returnValue = false;
            if(theEvent.preventDefault) theEvent.preventDefault();
        }

    }
    // check exists goal_amount
    if(document.getElementById('goal_amount')){
        window.onload = function() {
            const myInput = document.getElementById('goal_amount');
            myInput.onpaste = function(e) {
                e.preventDefault();
            }
        }
    }
    // check exists amount
    if(document.getElementById('amount')){
        window.onload = function() {
            const myInput = document.getElementById('amount');
            myInput.onpaste = function(e) {
                e.preventDefault();
            }
        }
    }
    // click id refunds_load show loading(refunds money when project not success)
    $("#refunds_load").click(function(){
        $('#common_loading2').addClass('show');
    });
    // click submenu in slidebar
    $('#submenu').click(function(){
        if ($('#submenu').hasClass('active')) {
            $("#menu_sub").html(`<i class="fa fa-plus"></i>`);
        }else{
            $("#menu_sub").html(`<i class="fa fa-minus"></i>`);
        }
    });

    function filterMailTemPlate(name) {
        var regex = /^[a-z0-9_-\s]+$/;
        return regex.test(name);
    }

    $("#name_mailtemplate").on("change", function() {
        var name = $("#name_mailtemplate").val();
        // console.log(name.test(/^[a-z0-9_-\s]+$/gi));
        // filterEmail(name);
        if(filterMailTemPlate(name)){
            document.getElementById("name_mailtemplate_error_js").style.display = "none";
        }else{
            document.getElementById("name_mailtemplate_error_js").style.display = "block";
        }
    });

    // last add
    $('#btnrefuse').click(function() {
        // $loading.show();

        if($("#SendMailRefuse").validate()['invalid']['message'] === false && $("#SendMailRefuse").validate()['invalid']['subject'] === false){
            $loading.show();
        }
    });



    $("#SendMailRefuse").validate({
        // create rules
        rules: {
            "subject": {
                required: true,
            },
            "message": {
                required: true,
            }
        },
        // show message if error
        messages: {
            "subject": {
                required: "HTMLファイルを選択してください",
            },
            "message": {
                required: "サブジェクトを入力してください。",
            }
        },
        // show error & hidden error
        highlight: function(element) {
            $(element).addClass('error');
        }, unhighlight: function(element) {
            $(element).removeClass('error');
        }
    });


    $(".list_editActive").shorten({
        "showChars" : 39,
    });    $('#tableProjectReturnActive .trTableEditProjectActive').click(function() {
        var id = $(this).attr('id');
        var length = id.length;
        var location = id.lastIndexOf('_');
        var project_id = id.slice(0, location);
        var list_status = id.slice(location+1, length);
        window.open(BASE_URL+'projectreturn/listActive?project_id='+project_id+'&status='+list_status);
    });

    $("#tableProjectReturnActive tr td .stopPopUpTarget").click(function(event){
        event.stopPropagation();
    });

    autosize(document.getElementById("previewArea"));

    // check video checked in (project add & edit)
    if($('#Project_Normal').is(':checked')) {
        // show video and hide image
        $("#info_project_normal").css({"display":"block"});
        $("#info_project_fanclub").css({"display":"none"});
    }
    // check iamge checked in (project add & edit)
    if($('#Project_FanClub').is(':checked')) {
        // show image and hide video
        $("#info_project_normal").css({"display":"none"});
        $("#info_project_fanclub").css({"display":"block"});
    }
    // show or hidden flow image
    $("#Project_FanClub").click(function(){
        $("#info_project_normal").css({"display":"none"});
        $("#info_project_fanclub").css({"display":"block"});
    });
    // show or hidden flow Video
    $("#Project_Normal").click(function(){
        $("#info_project_normal").css({"display":"block"});
        $("#info_project_fanclub").css({"display":"none"});
    });
