<?php

defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Page
 *
 * @author TNM Group
 */
class News extends MY_Controller {

    // public $menu = 'category';
    // public $page_title = 'Sản phẩm';

    public function __construct() {
        parent::__construct();
        $this->_is_admin();
        $this->menu = 'news';
        // $this->page_title = 'プロジェクトのカテゴリー';
        $this->load->helper(array('form', 'html'));
        $this->load->library('form_validation');
        $this->lang->load('category_lang','japanese');
        $this->load->model('admin/news_model');
        $this->load->model('admin/category_model');
    }
    /**
     *  show news and pagination
     */
    public function index() {

        if ($this->uri->segment(5) === FALSE) {
            $page = 0;
        } else {
            $page = $this->uri->segment(5);
        }
        $data['data'] = $this->news_model->getNews($page);
        //var_dump($data);die();
        $this->_renderAdminLayout('admin/news/index', $data);
    }
    /**
     * add new
     */
    public function add() {
        // ckeditor
        $this->carabiner->js('ckeditor/ckeditor.js');
        $this->carabiner->js('js/ckeditor-loader.js');
        // get laster id for upload image
        $data['id'] = $this->news_model->getLastId();
        if(isset($data['id']) && $data['id']){
            $data['id']  = (float) $data['id']->id + 1;
        }else{
            $data['id'] = 1;
        }
        // if click save or public
        if ($this->input->post('save') || $this->input->post('public')) {
            // form_validation
            $this->load->library('form_validation');
            $this->form_validation->set_rules('title', 'Name', 'trim|required');
            $this->form_validation->set_rules('content', 'Content', 'trim|required');
            // if validation true
            if ($this->form_validation->run() !== false) {
                // insert new
                $id = $this->news_model->insert();
                // if click save show messagea
                if($this->input->post('save') === '1'){
                    $this->session->set_flashdata('msg', 'ニュースの保存ができました。');
                // if click public show messagea
                }else if($this->input->post('public') === '1'){
                    $this->session->set_flashdata('msg', 'ニュースの公開ができました。');
                }
                // redirect
                redirect('/news/');
            } else {
                // message + redirect
                $this->session->set_flashdata('error', 'ニュースの投稿がまだできません');
                redirect('/news/add');
            }
        } else {
            // load view
            $this->_renderAdminLayout('admin/news/add', $data);
        }
    }
    // edit new
    public function edit($id) {
        // ckeditor
        $this->carabiner->js('ckeditor/ckeditor.js');
        $this->carabiner->js('js/ckeditor-loader.js');
        // get new for $id
        $data['news'] = $this->news_model->getNewById($id);
        // get pid
        $data['pid'] = $this->input->post('pid'); // $id;
        // click save or public
        if ($this->input->post('save') || $this->input->post('public')) {
            // check public
            $public = $this->input->post('public');
            $this->news_model->update();
            // check click save and show message
            if($this->input->post('save') === '1'){
                $this->session->set_flashdata('msg', 'ニュースの更新ができました');
            // check message public = 1(public) and show message
            }else if($this->input->post('public') === '1'){
                $this->session->set_flashdata('msg', 'ニュースの公開ができました。');
            // check click public = 2(un_public) and show message
            }else if($this->input->post('public') === '2'){
                $this->session->set_flashdata('msg', 'ニュースの非公開ができました。');
            }
            redirect('/news/');
        } else {
            // load view
            $this->_renderAdminLayout('admin/news/edit', $data);
        }
    }
    // delete new
    public function delete($id) {
        // check $id
        if ((int) $id > 0) {
            // get new
            $new = $this->news_model->getNewById($id);
            // delete id
            $del = $this->news_model->delete($id);
            // check del
            if($del){
                // use curl delete image
                $url = api_url().'api/DeleteImage/delImage';
                $params = array(
                    'linkImage' => $new->thumnail,
                    'id' => $new->id,
                );

                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($params));
                curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 60);
                curl_setopt($ch, CURLOPT_TIMEOUT, 60);

                // This should be the default Content-type for POST requests
                //curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-type: application/x-www-form-urlencoded"));

                $result = curl_exec($ch);
                $this->session->set_flashdata('msg', '削除ができました');
                redirect('/news');

            }else{
                // show error
                $this->session->set_flashdata('error', 'ニュースが削除されました。');
                redirect('/news');
            }
        // show error
        }else{
            $this->session->set_flashdata('error', 'ニュースが削除されました。');
            redirect('/news');
        }
    }
    // delete news
    public function action() {
        // get id flow array
        $val = $this->input->post('val');
        // action
        $action = $this->input->post('hidAction');
        // check action
        if ($action == 'delete') {
            // delete news
            $in = implode(',', $val);
            $this->db->where("id in ($in)");
            $this->db->delete('news');
        $this->session->set_flashdata('msg', 'ニュースが削除されました。');
        }
        redirect('/news');
    }
}
