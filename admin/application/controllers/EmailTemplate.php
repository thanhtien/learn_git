<?php

defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Categories Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Page
 *
 * @author TNM Group
 */
class EmailTemplate extends MY_Controller {

    // public $menu = 'category';
    // public $page_title = 'Sản phẩm';

    // __construct
    public function __construct() {
        parent::__construct();
        $this->_is_admin();
        $this->menu = 'email_template';
        $this->page_title = 'プロジェクトのカテゴリー';
        $this->load->helper(array('form', 'html'));
        $this->load->model('admin/subscription_model');
        $this->load->model('admin/project_model');
        $this->load->model('admin/users_model');
        $this->load->model('admin/backed_model');
        $this->lang->load('category_lang','japanese');
    }
    /**
     *  use curl call api get email template
     */
    function index(){

        $this->load->helper('directory');
        $map = directory_map('./TeampleatEmail',1);
        // var_dump($map);
        $countMap = count($map);

        $assignMap = [];
        $newMap = [];
        foreach ($map as $key => $value) {
            // var_dump($value);
            $newDate = date("Y-m-d H:i:s", filemtime('./TeampleatEmail/'.$value));
            $assignMap['time'] = $newDate;
            $assignMap['name'] = $value;
            // var_dump($assignMap);
            array_push($newMap,$assignMap);
        }

        function date_compare($a, $b)
        {
            $t1 = strtotime($a['time']);
            $t2 = strtotime($b['time']);
            return $t2 - $t1;
        }
        usort($newMap, 'date_compare');

        if(isset($newMap) && $newMap){
            $data['total'] = $countMap;
            $data['email_template'] = $newMap;
        }else{
            $data['total'] = 0;
            $data['email_template'] = [];
        }

        $this->_renderAdminLayout('admin/email_template/index', $data);
    }

    // send mail
    public function sendMailTemplate() {

        $template = $this->input->post('template');
        $email_template = $this->subscription_model->getAllEmailSubscription();
        $email = [];

        foreach ($email_template as $key => $value) {
            if($value->unsubscribe === '0' && $value->email !== ''){
                if (!in_array($value->email, $email)) {
                    array_push($email,$value->email);
                }
            }
        }

        $this->load->helper('directory');
        $map = directory_map('./TeampleatEmail/'.$template.'');
        // get subject + message
        $subject = file_get_contents('./TeampleatEmail/'.$template.'/'.$map[1].'');
        $newfile = file_get_contents('./TeampleatEmail/'.$template.'/'.$map[0].'');

        $searchVal = array('src="images', '</body>');
        $replaceVal = array('src="'.base_url().'TeampleatEmail/'.$template.'/images', '<hr style="margin-top: 25px;"><p style="text-align:center;margin-top:28px;color:#808080;">本メールが届かないように、<a style="color:#808080;" href="'.app_url().'my-page/subcriber/&subcriber=true" >こちら</a>に行って下さい。</p></body>');

        $res = str_replace($searchVal, $replaceVal, $newfile);
        $message = $res;

        $status = SendMail($email,$subject,$message);
        $myObj['status']   = $status;
        echo json_encode($myObj);
    }

    // preview mail
    public function previewEmailTemplate($template){
        $this->load->helper('directory');
        $map = directory_map('./TeampleatEmail/'.$template.'');
        // var_dump($map);
        $countMap = count($map);
        // get title
        $title = file_get_contents('./TeampleatEmail/'.$template.'/'.$map[1].'');
        // exit;
        $newfile  = str_replace('src="images','src="'.base_url().'TeampleatEmail/'.$template.'/images',file_get_contents('./TeampleatEmail/'.$template.'/'.$map[0].''));

        if($this->input->post('save')){
            $email_template = $this->subscription_model->getAllEmailSubscription();
            $email = [];
            foreach ($email_template as $key => $value) {
                if($value->unsubscribe === '0' && $value->email !== ''){
                    if (!in_array($value->email, $email)) {
                        array_push($email,$value->email);
                    }
                }
            }

            $subject = file_get_contents('./TeampleatEmail/'.$template.'/'.$map[1].'');
            $newfile = file_get_contents('./TeampleatEmail/'.$template.'/'.$map[0].'');
            $searchVal = array('src="images', '</body>');
            $replaceVal = array('src="'.base_url().'TeampleatEmail/'.$template.'/images', '<hr style="margin-top: 25px;"><p style="text-align:center;margin-top:28px;color:#808080;">本メールが届かないように、<a style="color:#808080;" href="'.app_url().'my-page/subcriber/&subcriber=true" >こちら</a>に行って下さい。</p></body>');
            // str_replace('src="images','src="'.base_url().'TeampleatEmail/'.$template.'/images',file_get_contents('./TeampleatEmail/'.$template.'/'.$map[0].''));
            // str_replace('</body>','<p style="text-align:center;margin-top:30px;">本メールが届かないように、<a href="'.app_url().'" >こちら</a>に行って下さい。</p></body>',file_get_contents('./TeampleatEmail/'.$template.'/'.$map[0].''));
            $res = str_replace($searchVal, $replaceVal, $newfile);
            $message = $res;

            $sendmail = SendMail($email,$subject,$message);
            if($sendmail){
                $this->session->set_flashdata('msg', 'メール送信ができました。');
                redirect('/EmailTemplate/');
            }else{
                $this->session->set_flashdata('error', 'このメールテンプレートが存在していません。');
                redirect('/EmailTemplate/');
            }

        }else{
            // $this->_renderAdminLayout('admin/email_template/index', $data);
            $data['title'] = $title;
            $data['template'] = $newfile;
            $this->_renderAdminLayout('admin/email_template/preview', $data);
        }

    }

    // Chosen Group send Email Template
    public function EmailTemplateGroup($template) {
        $this->load->helper('directory');
        $map = directory_map('./TeampleatEmail/'.$template.'');
        // var_dump($map);
        $countMap = count($map);
        if(isset($map) & $map){

            $title = file_get_contents('./TeampleatEmail/'.$template.'/'.$map[1].'');
            // exit;
            $newfile  = str_replace('src="images','src="'.base_url().'TeampleatEmail/'.$template.'/images',file_get_contents('./TeampleatEmail/'.$template.'/'.$map[0].''));

            if($this->input->post('save')){
                $projectId = $this->input->post('project_id');

                $listBacked = $this->backed_model->getBackedAll($projectId);
                $listIdEmail = array();
                foreach ($listBacked as $key => $value) {
                    if (in_array($value->user_id,$listIdEmail) !== true) {
                        array_push($listIdEmail,$value->user_id);
                    }
                }

                $listEmail = array();
                foreach ($listIdEmail as $key => $value) {

                    $user = $this->users_model->getUserId($value);
                    if($user->unsubscribeProject === '0' && $user->email !== ''){
                        array_push($listEmail,$user->email);
                    }
                }
                $subject = file_get_contents('./TeampleatEmail/'.$template.'/'.$map[1].'');
                $newfile = file_get_contents('./TeampleatEmail/'.$template.'/'.$map[0].'');
                $searchVal = array('src="images', '</body>');
                $replaceVal = array('src="'.base_url().'TeampleatEmail/'.$template.'/images', '<hr style="margin-top: 25px;"><p style="text-align:center;margin-top:28px;color:#808080;">本メールが届かないように、<a style="color:#808080;" href="'.app_url().'my-page/subcriber/&subcriber=true" >こちら</a>に行って下さい。</p></body>');
                $res = str_replace($searchVal, $replaceVal, $newfile);
                $message = $res;
                $status = SendMail($listEmail,$subject,$message);

                if($status){
                    $this->session->set_flashdata('msg', 'メール送信ができました。');
                    redirect('/EmailTemplate/');
                }else{
                    $this->session->set_flashdata('error', 'メールテンプレート送信ができていません。');
                    redirect('/EmailTemplate/');
                }
            }else{
                // $this->_renderAdminLayout('admin/email_template/index', $data);
                $data['template'] = $newfile;
                $data['project'] = $this->project_model->getAllProjectActive();
                $this->_renderAdminLayout('admin/email_template/sendmailgroup', $data);
            }
        }else{
            $this->session->set_flashdata('error', 'このメールテンプレートが存在していません。');
            redirect('/EmailTemplate/');
        }
    }

    public function UploadEmailTemplate(){
        $this->load->helper('file');
        if($this->input->post('save')){
            $nameFolder = $this->input->post('name');
            if(preg_match("/^[a-zA-Z0-9_-]+$/", $nameFolder) == 1) {
                $titleSubject = $this->input->post('title');
                $time = time();
                $date = new Datetime();
                $month = date('m');
                $year = date('Y');
                if(isset($_FILES["file"])){
                    $new_name = $_FILES["file"]['name'];
                    if($nameFolder === ''){
                        $nameFolder = time().$new_name;
                    }
                }else{
                    $error = 'a';
                }
                $config['upload_path']          = './images/TeampleatEmail2';
                $config['allowed_types']        = '*';
                $config['file_name'] = $new_name;
                $config['encrypt_name'] = TRUE;

                //load thư viện vả sử dụng thư viện upload
                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                // kiểm tra file đã upload chưa
                if ( ! $this->upload->do_upload('file')) {
                    // Đá lỗi
                    $error = strip_tags($this->upload->display_errors());
                    $this->session->set_flashdata('error', $error);
                } else {
                    $data = array('upload_data' => $this->upload->data());
                    $zip = new ZipArchive;
                    $file = $data['upload_data']['full_path'];
                    if ($zip->open($file) === TRUE) {
                        $zip->extractTo('./TeampleatEmail/'.$nameFolder.'');
                        $zip->close();
                        write_file('./TeampleatEmail/'.$nameFolder.'/title.html', $titleSubject);
                        $this->session->set_flashdata('msg', 'HTMLファイルをアップロードできました。');
                    } else {
                        $this->session->set_flashdata('error', 'HTMLファイルをアップロードできました。');
                    }
                }
                redirect('/EmailTemplate/');
            }else{
                $this->session->set_flashdata('error', 'ローマ字・数字だけで入力してください');
                redirect('/EmailTemplate/');
            }
        }else{
            $this->_renderAdminLayout('admin/email_template/upload');
        }
    }

    // delete email template
    public function delete($template) {
        // check $template
        if(isset($template) && $template){
        $this->load->helper('directory');
        $map = directory_map('./TeampleatEmail',1);

            if (file_exists('./TeampleatEmail/'.$template)) {
                $files = glob('./TeampleatEmail/'.$template.'/images/*'); // get all file names
                foreach($files as $file){ // iterate files
                  if(is_file($file))
                    unlink($file); // delete file
                }
                $files = glob('./TeampleatEmail/'.$template.'/*'); // get all file names
                foreach($files as $file){ // iterate files
                  if(is_file($file))
                    unlink($file); // delete file
                }
                rmdir ('./TeampleatEmail/'.$template.'/images');
                rmdir ('./TeampleatEmail/'.$template);
            }
            // show message and redirect
            $this->session->set_flashdata('msg', '削除ができました');
            redirect('/EmailTemplate');
        }else{
            // show error + redirect
            $this->session->set_flashdata('error', 'ニュースが削除されました。');
            redirect('/EmailTemplate');
        }

    }
}
