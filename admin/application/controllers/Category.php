<?php

defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Categories Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Page
 *
 * @author TNM Group
 */
class Category extends MY_Controller {

    // __construct
    public function __construct() {
        parent::__construct();
        $this->_is_admin();
        $this->menu = 'category';
        $this->page_title = 'プロジェクトのカテゴリー';
        $this->load->helper(array('form', 'html'));
        $this->load->library('form_validation');
        $this->lang->load('category_lang','japanese');
        $this->load->model('admin/project_model');
        $this->load->model('admin/category_model');
    }
    // Show Category vs Pagination
    public function index($sorting="id",$by="DESC") {
        if ($this->uri->segment(6) === FALSE) {
            $page = 0;
        } else {
            $page = $this->uri->segment(6);
        }
        // Get Data
        $data['data'] = $this->category_model->getCategories($sorting,$by,$page);
        $data['sort'] =$sorting;
        $data['by'] = $by;
        // Render Layout(Load View)
        $this->_renderAdminLayout('admin/category/index', $data);
    }
    // Search category
    public function searchCategories($sorting="id",$by="DESC") {
        // get pagiantion flow this
        if ($this->uri->segment(6) === FALSE) {
            $page = 0;
        } else {
            $page = $this->uri->segment(6);
        }
        // get search
        $search = $this->input->get('srch-term');
        // search
        $data['search'] = $search;
        $data['data'] = $this->category_model->searchCategories($sorting,$by,$search,$page);
        $data['sort'] =$sorting;
        $data['by'] = $by;
        // load view
        $this->_renderAdminLayout('admin/category/index', $data);
    }
    // Create Category
    public function add() {
        // Click 'save'
        if ($this->input->post('save')) {
            // Form Validation
            $this->form_validation->set_rules('name', 'Name', 'trim|required|is_unique[categories.name]');
              $this->form_validation->set_rules('slug', 'Slug', 'trim|required');
            // If Form Validation = true
            if ($this->form_validation->run() !== false) {
                // Redirect Function _save()
                $this->_save();
            // If Form Validation = flase
            } else {
                // Redirect Category/add and Session error
                $this->session->set_flashdata('error', '【プロジェクトのカテゴリー名】はすでに存在します。');
                redirect('/category/add');
            }
        // Not Click 'save'(Load View)
        } else {
            // Render Layout(Load View)
            $this->_renderAdminLayout('admin/category/add', null);
        }
    }
    // Finish Create Category

    // Edit Category with Id
    public function edit($id) {
        // Get Category with id
        $data['category'] = $this->category_model->getCategoryById($id);
        if(isset($data['category']) && $data['category']){

            // Get id with pid(Bottom view of edit category)
            $data['pid'] = $this->input->post('pid'); // $id;
            // Click 'Save'
            if ($this->input->post('save')) {
                // Redirect function _save()
                $this->_save();
            // Not click 'save'(Load View)
            } else {
                $this->_renderAdminLayout('admin/category/edit', $data);
            }
        }else{
            $this->session->set_flashdata('error', 'プロジェクトのカテゴリー');
            redirect('category');
        }

    }
    // Finish Edit Category

    // Function _save() Create and Edit use this function
    public function _save() {
        // Gan $data = $this->input->post()
        $data = $this->input->post();
        // Get $id vs $this->input->post('pid');
        $id = (int) $data["pid"];
        //If have page id parameter then update page, else add new page
        if ($id > 0) {
            // use category model update
            $this->category_model->update();
            // Update done redirect and session msg
            $this->session->set_flashdata('msg', 'プロジェクトのカテゴリーが更新できました。');
            redirect('/category/');
        // Not $id
        } else {
            // use category model insert
            $id = $this->category_model->insert();
            // insert done redirect and session msg
            if($id){
              $this->session->set_flashdata('msg', 'プロジェクトのカテゴリ作成ができました。');
              redirect('/category/');
            }else{
              $this->session->set_flashdata('error', 'アルファベットとハイフンだけを入力してください。');
              redirect('/category/add');
            }

        }
    }
    // finish function _save()

    // Delete Category
    public function delete($id) {
        // Check Id exists


        if ((int) $id > 0) {
            $project = $this->category_model->getProjectByCategoryId($id);
            // Delete Category with Id
            if(isset($project) && $project) {
                $this->session->set_flashdata('error', 'このカテゴリーにはプロジェクトが入っていますので、削除することができないことです。');
                redirect('/category');
            }else {
                $this->category_model->delete($id);
            }
        }
        // Redirect and session msg
        $this->session->set_flashdata('msg', 'プロジェクトのカテゴリーが削除されました。');
        redirect('/category');
    }
    // Finish Delete Category

    // Function acction()
    public function action() {
        // Get Id($val in checkbox of view (category.index))
        $val = $this->input->post('val');

        // Get Action
        $action = $this->input->post('hidAction');
        // If action = delete
        if ($action == 'delete') {
            $in = implode(',', $val);
            $project = $this->category_model->getProjectByCategoryIds($in);
            if(isset($project) && $project) {
                $this->session->set_flashdata('error', 'このカテゴリーにはプロジェクトが入っていますので、削除することができないことです。');
            }else {
                $this->db->where("id in ($in)");
                $this->db->delete('categories');
                $this->session->set_flashdata('msg', 'プロジェクトのカテゴリーが削除されました。');
            }
        }
        redirect('/category');
    }
    // Finish Action()
}
