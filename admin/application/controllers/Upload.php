<?php

defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Page
 *
 * @author TNM Group
 */
class Upload extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper(array('form', 'html', 'file', 'path'));
        $this->load->library('form_validation');
    }

    public function index() {
        $config['upload_path'] = './media/project/';
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['max_size'] = '1024';
        $config['encrypt_name'] = TRUE;
        $this->load->library('upload', $config);
        if (!$this->upload->do_upload('file')) {
            $error = array('error' => $this->upload->display_errors('<div class="alert alert-danger">', '</div>'));
            die(json_encode($error));
        } else {
            $upload_data = $this->upload->data();
            die(json_encode($upload_data['full_path']));
        }
    }
    
    public function project_image() {
        $config['upload_path'] = './media/project/';
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        //$config['max_size'] = '1024';
        $config['encrypt_name'] = TRUE;
        $this->load->library('upload', $config);
        if (!$this->upload->do_upload('image')) {
            $error = array('error' => $this->upload->display_errors('<div class="alert alert-danger">', '</div>'));
            die(json_encode($error));
        } else {
            $upload_data = $this->upload->data();
            die('/media/project/'.$upload_data['file_name']);
        }
    }

    public function delete_image() {
        $file = FCPATH . $this->input->post('uri');
        if(file_exists($file) && $this->input->post('uri') != '') {
            unlink($file);
        }
    }

    public function getBase64Image($image) {
        $config['image_library'] = 'gd2';
        $config['source_image'] = set_realpath('media/' . $image);
        $imageData = base64_encode(file_get_contents(set_realpath('media/' . $image)));
        $this->load->library('image_lib', $config);
        $src = 'data: ' . $this->image_lib->mime_type . ';base64,' . $imageData;
        return $src;
    }

}
