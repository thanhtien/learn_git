<?php

defined('BASEPATH') OR exit('No direct script access allowed');
require("assets/socket/socket.io.php");
/*
 * To change this license header, choose License Headers in Categories Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Page
 *
 * @author TNM Group
 */
class Announce extends MY_Controller {

    // __construct
    public function __construct() {
        parent::__construct();
        $this->_is_admin();
        $this->menu = 'announce';
        $this->page_title = 'プロジェクトのカテゴリー';
        $this->load->helper(array('form', 'html'));
        $this->load->library('form_validation');
        $this->lang->load('category_lang','japanese');
        $this->load->model('admin/announce_model');
        $this->load->model('admin/notification_info');
    }
    // Show Category vs Pagination
    public function index($sorting="id",$by="DESC") {
        if ($this->uri->segment(6) === FALSE) {
            $page = 0;
        } else {
            $page = $this->uri->segment(6);
        }
        // Get Data
        $data['data'] = $this->announce_model->getAnnounce($sorting,$by,$page);
        $data['sort'] =$sorting;
        $data['by'] = $by;
        // Render Layout(Load View)
        $this->_renderAdminLayout('admin/announce/index', $data);
    }
    // Search announce
    public function searchCategories($sorting="id",$by="DESC") {
        // get pagiantion flow this
        if ($this->uri->segment(6) === FALSE) {
            $page = 0;
        } else {
            $page = $this->uri->segment(6);
        }
        // get search
        $search = $this->input->get('srch-term');
        // search
        $data['search'] = $search;
        $data['data'] = $this->announce_model->searchCategories($sorting,$by,$search,$page);
        $data['sort'] =$sorting;
        $data['by'] = $by;
        // load view
        $this->_renderAdminLayout('admin/announce/index', $data);
    }
    // Create Category
    public function add() {
        // key
        $key_firebase  = $this->config->item('key_firebase');
        $roomSocket = $this->config->item('roomSocket');
        $portRoomSocket = $this->config->item('portRoomSocket');

        //Load ckeditor
        $this->carabiner->js('ckeditor/ckeditor.js');
        $this->carabiner->js('js/ckeditor-loader.js');

        // Click 'save'
        if ($this->input->post('save') || $this->input->post('public')) {

            // Form Validation
            $this->form_validation->set_rules('title', 'title', 'trim|required');
            $this->form_validation->set_rules('message', 'message', 'trim|required');

            // If Form Validation = true
            if ($this->form_validation->run() !== false) {

                $id = $this->announce_model->insert();
                // insert done redirect and session msg
                if($id){
                    if($this->input->post('public')) {

                        $title = $this->input->post('title');
                        // create Datetime
                        $datetime = new DateTime();
                        $datetime = $datetime->format('Y-m-d H:i:s');
                        // get strtotime + change time zone
                        $asia_timestamp = strtotime($datetime);
                        date_default_timezone_set('UTC');
                        // get Date vs time zone
                        $date = date("Y-m-d H:i:s", $asia_timestamp);

                        // array create notification info
                        $dataNotification = array(
                            'user_id' => 0,
                            'title' => 'カクセイだクラウドファンディングからの通知が来ました',
                            'user_action' => 'Admin',
                            'message' => $title,
                            'link'=> "my-page/admin-notification/".$id,
                            'status' => 0,
                            'thumbnail' => image_url().'/images/2018/default/logo-icon.png',
                            'created' => $date
                        );
                        // create notification info
                        $createNotification = $this->notification_info->insert($dataNotification);
                        // check exists
                        if($createNotification){
                            // use SocketIO annouce for user
                            $socketio = new SocketIO();
                            // array
                            $data = array(
                                'room' => 'all_user',
                                'title' => 'カクセイだクラウドファンディングからの通知が来ました',
                                'message' => $title,
                                'url'=> app_url()."my-page/admin-notification/".$id,
                                'ava' => image_url().'/images/2018/default/logo-icon.png'
                            );

                            $socketio->send($roomSocket, $portRoomSocket, 'public_project',json_encode($data));
                            //
                            $curl = curl_init();

                            curl_setopt_array($curl, array(
                              CURLOPT_URL => "https://fcm.googleapis.com/fcm/send",
                              CURLOPT_RETURNTRANSFER => true,
                              CURLOPT_ENCODING => "",
                              CURLOPT_MAXREDIRS => 10,
                              CURLOPT_TIMEOUT => 30,
                              CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                              CURLOPT_CUSTOMREQUEST => "POST",
                              CURLOPT_POSTFIELDS => "{   \"notification\": {\r\n        \"title\": \"カクセイだクラウドファンディングからの通知が来ました\",\r\n        \"body\": \"".$title."\",\r\n        \"click_action\": \"".app_url()."my-page/admin-notification/".$id."\",\r\n        \"icon\": \"".image_url()."images/2018/default/logo-icon.png\"\r\n    },\r\n  \"to\" : \"/topics/all_user\"\r\n}",
                              CURLOPT_HTTPHEADER => array(
                                "Authorization: key=".$key_firebase."",
                                "Content-Type: application/json",
                              ),
                            ));
                            $response = curl_exec($curl);
                            $err = curl_error($curl);

                            $this->session->set_flashdata('msg', '公開ができました。');
                            redirect('/announce');
                        }
                    }else{
                        $this->session->set_flashdata('msg', '下書き保存ができました。');
                        redirect('/announce');
                    }
                }else{
                  $this->session->set_flashdata('error', '通知がまだ送付できません。');
                  redirect('/announce/add');
                }
            // If Form Validation = flase
            } else {
                // Redirect Category/add and Session error
                $this->session->set_flashdata('error', '通知がまだ送付できません。');
                redirect('/announce/add');
            }
        // Not Click 'save'(Load View)
        } else {
            // Render Layout(Load View)
            $this->_renderAdminLayout('admin/announce/add', null);
        }
    }

    // Edit Category with Id
    public function edit($id) {
        $key_firebase  = $this->config->item('key_firebase');

        $roomSocket = $this->config->item('roomSocket');

        $portRoomSocket = $this->config->item('portRoomSocket');
        //Load ckeditor
        $this->carabiner->js('ckeditor/ckeditor.js');
        $this->carabiner->js('js/ckeditor-loader.js');
        // Get Category with id
        $data['announce'] = $this->announce_model->getCategoryById($id);

        if(isset($data['announce']) && $data['announce']){

            // Get id with pid(Bottom view of edit announce)
            $data['pid'] = $this->input->post('pid'); // $id;
            // Click 'Save'
            if ($this->input->post('save') || $this->input->post('public')) {

                // Redirect function _save()
                $updated = $this->announce_model->update();
                if($updated) {
                    if($this->input->post('public') && $data['announce']->status === '0') {
                        $title = $this->input->post('title');
                        // create Datetime
                        $datetime = new DateTime();
                        $datetime = $datetime->format('Y-m-d H:i:s');
                        // get strtotime + change time zone
                        $asia_timestamp = strtotime($datetime);
                        date_default_timezone_set('UTC');
                        // get Date vs time zone
                        $date = date("Y-m-d H:i:s", $asia_timestamp);

                        // array create notification info
                        $dataNotification = array(
                            'user_id' => 0,
                            'title' => 'カクセイだクラウドファンディングからの通知が来ました',
                            'user_action' => 'Admin',
                            'message' => $title,
                            'link'=> "my-page/admin-notification/".$updated,
                            'status' => 0,
                            'thumbnail' => image_url().'/images/2018/default/logo-icon.png',
                            'created' => $date
                        );
                        // create notification info
                        $createNotification = $this->notification_info->insert($dataNotification);
                        // check exists
                        if($createNotification){
                            // use SocketIO annouce for user
                            $socketio = new SocketIO();
                            // array
                            $data = array(
                                'room' => 'all_user',
                                'title' => 'カクセイだクラウドファンディングからの通知が来ました',
                                'message' => $title,
                                'url'=> app_url()."my-page/admin-notification/".$updated,
                                'ava' => image_url().'/images/2018/default/logo-icon.png'
                            );

                            $socketio->send($roomSocket, $portRoomSocket, 'public_project',json_encode($data));
                            //
                            $curl = curl_init();

                            curl_setopt_array($curl, array(
                              CURLOPT_URL => "https://fcm.googleapis.com/fcm/send",
                              CURLOPT_RETURNTRANSFER => true,
                              CURLOPT_ENCODING => "",
                              CURLOPT_MAXREDIRS => 10,
                              CURLOPT_TIMEOUT => 30,
                              CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                              CURLOPT_CUSTOMREQUEST => "POST",
                              CURLOPT_POSTFIELDS => "{   \"notification\": {\r\n        \"title\": \"カクセイだクラウドファンディングからの通知が来ました\",\r\n        \"body\": \"".$title."\",\r\n        \"click_action\": \"".app_url()."my-page/admin-notification/".$updated."\",\r\n        \"icon\": \"".image_url()."images/2018/default/logo-icon.png\"\r\n    },\r\n  \"to\" : \"/topics/all_user\"\r\n}",
                              CURLOPT_HTTPHEADER => array(
                                "Authorization: key=".$key_firebase."",
                                "Content-Type: application/json",
                              ),
                            ));
                            $response = curl_exec($curl);
                            $err = curl_error($curl);

                            curl_close($curl);

                            $this->session->set_flashdata('msg', '公開ができました。');

                            redirect('/announce');
                        }
                    }else if($this->input->post('public') && $data['announce']->status === '1'){
                        $this->session->set_flashdata('msg', '非公開できました。');
                        redirect('/announce');
                    }else if($this->input->post('save')){
                        $this->session->set_flashdata('msg', '下書き保存ができました。');
                        redirect('/announce');
                    }
                }else {
                    $this->session->set_flashdata('error', '通知がまだ送付できません。');
                    redirect('/announce');
                }

            // Not click 'save'(Load View)
            } else {
                $this->_renderAdminLayout('admin/announce/edit', $data);
            }
        }else{
            $this->session->set_flashdata('error', 'このURLが存在しません。');
            redirect('announce');
        }

    }
    // Finish Edit Category

    // Function _save() Create and Edit use this function
    public function _save() {
        // Gan $data = $this->input->post()
        $data = $this->input->post();
        // Get $id vs $this->input->post('pid');
        $id = (int) $data["pid"];
        //If have page id parameter then update page, else add new page
        if ($id > 0) {
            // use announce model update
            $this->announce_model->update();
            // Update done redirect and session msg
            $this->session->set_flashdata('msg', 'プロジェクトのカテゴリーが更新できました。');
            redirect('/announce/');
        // Not $id
        } else {
            // use announce model insert
            $id = $this->announce_model->insert();
            // insert done redirect and session msg
            if($id){
              $this->session->set_flashdata('msg', 'プロジェクトのカテゴリ作成ができました。');
              redirect('/announce/');
            }else{
              $this->session->set_flashdata('error', 'アルファベットとハイフンだけを入力してください。');
              redirect('/announce/add');
            }

        }
    }
    // finish function _save()

    // Delete Category
    public function delete($id) {
        // Check Id exists
        if ((int) $id > 0) {
            $this->announce_model->delete($id);
            // Redirect and session msg
            $this->session->set_flashdata('msg', 'プロジェクトのカテゴリーが削除されました。');
            redirect('/announce');
        }
    }

    // Function acction()
    public function action() {
        // Get Id($val in checkbox of view (announce.index))
        $val = $this->input->post('val');

        // Get Action
        $action = $this->input->post('hidAction');
        // If action = delete
        if ($action == 'delete') {
            $in = implode(',', $val);
            $this->db->where("id in ($in)");
            $this->db->delete('announce');
            $this->session->set_flashdata('msg', 'プロジェクトのカテゴリーが削除されました。');
        }
        redirect('/announce');
    }
    // Finish Action()
}
