<?php

defined('BASEPATH') OR exit('No direct script access allowed');
require("assets/socket/socket.io.php");
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Page
 *
 * @author TNM Group
 */
class Projectreturn extends MY_Controller {

    // public $menu = 'category';
    // public $page_title = 'Sản phẩm';
    // Load Construct
    public function __construct() {
        parent::__construct();
        $this->_is_admin();
        $this->menu = 'projectreturn';
        $this->page_title = 'プロジェクトのカテゴリー';
        $this->load->helper(array('form', 'html'));
        $this->load->library('form_validation');
        $this->lang->load('category_lang','japanese');
        $this->lang->load('project_lang','japanese');
        $this->load->model('admin/projectreturn_model');
        $this->load->model('admin/project_model');
        $this->load->model('admin/backing_model');
        $this->load->model('admin/notification_info');
        $this->load->model('admin/projectreturn_model');
        $this->load->model('admin/users_model');
        $this->load->model('admin/setting_model');
    }
    // Add Project Return with project_id(id of project)
    public function add($project_id) {
      $data['project_id'] = $project_id;
      $data['project'] = $this->project_model->getProjectById($project_id);
      $data['project_type'] = $data['project']->project_type;
      $data['number_month'] = $data['project']->number_month;
      $data['product_fanclub_id'] = $data['project']->product_fanclub_id;
      $data['user_id'] = $data['project']->user_id;

      //set api key
      $key = $this->setting_model->getSettings();
      $stripe = array(
          "secret_key"      => $key->secret_key,
          "publishable_key" => $key->public_key
      );
      $data['settingKey'] = $stripe['secret_key'];

      if ($this->input->post('save')) {
          // use form_validation
          $this->form_validation->set_rules('content', 'projectreturn_name', 'trim');
          $this->form_validation->set_rules('amount', 'projectreturn_name', 'trim');
          // If true
          if ($this->form_validation->run() !== false) {
                // Insert
                $id = $this->projectreturn_model->insert($project_id);
                $this->session->set_flashdata('msg', $this->lang->line('create_project_return_success'));
                $this->session->set_flashdata('tab', '1');
                redirect('/project/edit/' . $project_id);
          } else {
              // show error and redirect
              $this->session->set_flashdata('error', $this->lang->line('create_project_return_failure'));
              redirect('/project/edit/' . $project_id);
          }
      } else {
          // load layout use MY_Controller
          $this->_renderAdminLayout('admin/project_return/add', $data);
      }
    }
    // Edit Project Return
    public function edit($id) {
      if(isset($id) && $id){
        // $this->carabiner->js('ckeditor/ckeditor.js');
        // $this->carabiner->js('js/ckeditor-loader.js');
        $data['projectreturn'] = $this->projectreturn_model->getProjectReturnById($id);
        $data['project'] = $this->project_model->getProjectById($data['projectreturn']->project_id);
        $data['project_type'] = $data['project']->project_type;

        if(isset($data['projectreturn']) && $data['projectreturn']){

          $data['tab'] = '0';
          if ($this->input->post('save')) {
            $this->projectreturn_model->update($id);
            $this->session->set_flashdata('msg', '更新ができました。');
            $this->session->set_flashdata('tab', '1');
            redirect('/project/edit/' . $data['projectreturn']->project_id);
          } else {
              // load layout use MY_Controller
              $this->_renderAdminLayout('admin/project_return/edit', $data);
          }
        }else{
          $this->session->set_flashdata('error', 'このリターンは存在しません');
          redirect('/project/');
        }
      }else{
        $this->session->set_flashdata('error', 'このリターンは存在しません');
        redirect('/project/');
      }
    }

    public function delete($id) {

        if ((int) $id > 0) {
            $project =  $this->projectreturn_model->getProjectReturnById($id);
            $this->projectreturn_model->delete($id);
        }
        $this->session->set_flashdata('msg', $this->lang->line('edit_project_return_success'));
        $this->session->set_flashdata('tab', '1');
        redirect('/project/edit/'.$project->project_id);
    }

    public function listActive(){
        $project_id = $this->input->get('project_id');
        $status = $this->input->get('status');
        $data['project'] = $this->project_model->getProjectById($project_id);
        $data['project_type'] = $data['project']->project_type;
        $data['status']  = $status;
        if(isset($data['project']) && $data['project']){
            $infoBacked = $this->backing_model->getListBacking($project_id,$status);

            if(isset($infoBacked) && $infoBacked){
                $listReturnActive = $this->projectreturn_model->getProjectReturnActiveAll($project_id,$status);
                $data['title'] = $infoBacked->title;
                $data['title'] = $infoBacked->title;
                $data['message'] = $infoBacked->message;
                $data['current_status'] = $infoBacked->status;
                $data['goal_amount'] = $infoBacked->goal_amount;
                $data['listReturnActive'] = $listReturnActive;
                $this->_renderAdminLayout('admin/project_return/editActive', $data);
            }else{
                $this->session->set_flashdata('error', 'このリターンは存在しません');
                redirect('/project/edit/'.$project_id.'?tab=2');
            }
        }else{
            $this->session->set_flashdata('error', 'このリターンは存在しません');
            redirect('/project/');
        }
    }

    public function refuse(){
        $key_firebase  = $this->config->item('key_firebase');
        $roomSocket  = $this->config->item('roomSocket');
        $portRoomSocket  = $this->config->item('portRoomSocket');

        $projecId = $this->input->get('project_id');
        $list_status = $this->input->get('status');

        $subject = $this->input->post('subject');
        $message = $this->input->post('message');
        $project = $this->project_model->getProjectById($projecId);

        $infoBacked = $this->backing_model->getListBacking($projecId,$list_status);

        $data = array(
            'status'=>'1',
        );


        if($infoBacked->status === '1' || $infoBacked->status === '2'){
            $this->session->set_flashdata('error', 'プロジェクトへの更新内容が却下されました。');
            redirect('/project/edit/'.$projecId.'?tab=2');
        }else{
            $updateInfo = $this->backing_model->updateBackingProjectActive($infoBacked->id,$data);

            // create Datetime
            $datetime = new DateTime();
            $datetime = $datetime->format('Y-m-d H:i:s');
            // get strtotime + change time zone
            $asia_timestamp = strtotime($datetime);
            date_default_timezone_set('UTC');
            // get Date vs time zone
            $date = date("Y-m-d H:i:s", $asia_timestamp);

            $projectName = $project->project_name;
            // array create notification info
            $dataNotification = array(
                'user_id' => $project->user_id,
                'title' => '公開のプロジェクト・更新内容',
                'user_action' => 'Admin',
                'message' => '「'.$projectName.'」プロジェクトへの更新内容が却下されました。',
                'link'=> "my-page/edit-project-public/".$project->id."/page=1",
                'status' => 0,
                'thumbnail' => image_url().'/images/2018/default/logo-icon.png',
                'created' => $date
            );
            // create notification info
            $createNotification = $this->notification_info->insert($dataNotification);
            // check exists
            if($createNotification) {

                $user = $this->users_model->getUserId($project->user_id);
                $email = $user->email;
                $subject = "【".$project->project_name."】の更新申請が却下されました。";
                $message = '【'.$user->username.'】さん、<br>
                【'.$project->project_name.'】の更新申請が却下されました。<br>
                管理者からのメッセージは以下の通りです。ご確認ください。<br>
                ------------------------------------------------<br>
                <pre>'.$message.'</pre>';
                // use SocketIO annouce for user
                $socketio = new SocketIO();
                // array
                $dataSocket = array(
                    'room' => 'project_'.$projecId,
                    'title' => '公開のプロジェクト・更新内容',
                    'message' => '「'.$projectName.'」プロジェクトへの更新内容が却下されました。',
                    'url'=> app_url()."my-page/edit-project-public/".$projecId."/page=1",
                    'ava' => image_url().'/images/2018/default/logo-icon.png'
                );

                $socketio->send($roomSocket, $portRoomSocket, 'public_project',json_encode($dataSocket));


                $curl = curl_init();

                curl_setopt_array($curl, array(
                  CURLOPT_URL => "https://fcm.googleapis.com/fcm/send",
                  CURLOPT_RETURNTRANSFER => true,
                  CURLOPT_ENCODING => "",
                  CURLOPT_MAXREDIRS => 10,
                  CURLOPT_TIMEOUT => 30,
                  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                  CURLOPT_CUSTOMREQUEST => "POST",
                  CURLOPT_POSTFIELDS => "{   \"notification\": {\r\n        \"title\": \"公開のプロジェクト・更新内容\",\r\n        \"body\": \"「".$projectName."」プロジェクトへの更新内容が却下されました。\",\r\n        \"click_action\": \"".app_url()."my-page/edit-project-public/".$projecId."/page=1\",\r\n        \"icon\": \"".image_url()."images/2018/default/logo-icon.png\"\r\n    },\r\n  \"to\" : \"/topics/project_".$projecId."\"\r\n}",
                  CURLOPT_HTTPHEADER => array(
                    "Authorization: key=".$key_firebase."",
                    "Content-Type: application/json",
                  ),
                ));
                $response = curl_exec($curl);
                $err = curl_error($curl);

                curl_close($curl);

                SendMail($email,$subject,$message);

            }


            $this->session->set_flashdata('msg', '却下メール送信ができました。');
            redirect('/project/edit/'.$projecId.'?tab=2');
        }
    }
}
