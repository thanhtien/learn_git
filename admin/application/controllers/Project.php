<?php

defined('BASEPATH') OR exit('No direct script access allowed');
require("assets/socket/socket.io.php");

class Project extends MY_Controller {


    public function __construct() {
        parent::__construct();


        $this->_is_admin();
        $this->menu = 'project';
        $this->page_title = 'プロジェクトのカテゴリー';
        $this->load->helper(array('form', 'html'));
        $this->load->library('form_validation');
        $this->lang->load('category_lang','japanese');
        $this->load->model('admin/project_model');
        $this->load->model('admin/category_model');
        $this->load->model('admin/backing_model');
        $this->load->model('admin/backed_model');
        $this->load->model('admin/projectreturn_model');
        $this->load->model('admin/users_model');
        $this->load->model('admin/favourite_model');
        $this->load->model('admin/pickup_model');
        $this->load->model('admin/notification_info');
        $this->load->model('admin/setting_model');
        $this->load->model('admin/recommends_model');
        $this->load->model('admin/listdonatefanclub_model');
        $this->lang->load('project_lang','japanese');
        $this->lang->load('user_lang','japanese');
    }
    // Get User use select2
    function autocompleteData() {
  		$json = [];
  		$this->load->database();
  		if(!empty($this->input->get("q"))){
  			$this->db->like('username', $this->input->get("q"));
  			$query = $this->db->select('id,username as text')
  						->limit(5)
  						->get("users");
  			$json = $query->result();
  		}
  		echo json_encode($json);
    }

    function sort_objects_by_total($a, $b) {
        if($a->now_count == $b->now_count){ return 0 ; }
        return ($a->now_count < $b->now_count) ? -1 : 1;
    }

    // Get all project and pagination project
    public function index($sorting="modified",$by="DESC") {

        $date = new DateTime();
        $date = $date->format('Y-m-d H:i:s');
        if ($this->uri->segment(6) === FALSE) {
            $page = 0;
        } else {
            $page = $this->uri->segment(6);
        }
        // Get Project
        if ($sorting === "id"  || $sorting ===  'project_name' || $sorting ===  'goal_amount'|| $sorting ===  'collected_amount' || $sorting ===  'username' || $sorting ===  'active' || $sorting ===  'collection_start_date'|| $sorting ===  'collection_end_date' || $sorting ===  'project_type') {
          $data['data'] = $this->project_model->getProject($sorting,$by,$page);
          $data['sort'] =$sorting;
          $data['by'] = $by;
        } else {
          $sort = 'modified';
          $data['data'] = $this->project_model->getProject($sort,$by,$page);
          $data['sort'] =$sorting;
          $data['by'] = $by;
        }
        // Foreach project
        foreach ($data['data']['project'] as $key => $value) {
          // Get $backing_level(Get now_count)
            $sum = $this->backing_model->abc($value->id);
            if (isset($sum->project_id) && isset($sum->now_count)) {
                if($value->id == $sum->project_id){
                    $value->now_count = $sum->now_count;
                }
            } else {
                $value->now_count = 0;
            }
            if($value->project_type === '0') {
                if($value->collection_end_date < $date){
                    $value->public = true;
                }else{
                    $value->public = false;
                }
            }else if($value->project_type === '1')  {
                $value->public = false;
            }

        }
        function method1($a,$b) {
            return ($a->now_count <= $b->now_count) ? -1 : 1;
        }
        function method2($a,$b) {
            return ($a->now_count <= $b->now_count) ? 1 : -1;
        }
        if(isset($sort) && $sort === 'id' &&  $by === 'ASC'){
            usort($data['data']['project'], "method2");
        }else if(isset($sort) && $sort === 'id' &&  $by === 'DESC'){
            usort($data['data']['project'], "method1");
        }
        $this->_renderAdminLayout('admin/project/index', $data);
    }


    public function searchProject($sorting="id",$by="DESC") {
        $date = new DateTime();
        $date = $date->format('Y-m-d H:i:s');
        if ($this->uri->segment(6) === FALSE) {
            $page = 0;
        } else {
            $page = $this->uri->segment(6);
        }

        // Get Project
        $search = $this->input->get('srch-term');
        $data['search'] = $search;
        $data['sort'] =$sorting;
        $data['by'] = $by;

        if ($sorting ==  'project_name'
            || $sorting ==  'goal_amount'
            || $sorting ==  'collected_amount'
            || $sorting ==  'username'
            || $sorting ==  'active'
            || $sorting ==  'collection_start_date'
            || $sorting ==  'collection_end_date'
            || $sorting ===  'project_type') {

            $data['data'] = $this->project_model->searchProject($sorting,$by,$search,$page);
            $data['sort'] =$sorting;
            $data['by'] = $by;

        } else {

            $sort = 'id';
            $data['data'] = $this->project_model->searchProject($sort,$by,$search,$page);
            $data['sort'] =$sorting;
            $data['by'] = $by;

        }

        // Foreach project
        foreach ($data['data']['project'] as $key => $value) {
            // Get $backing_level(Get now_count)
            $backing_level = $this->backing_model->getBackingLevelId($value->id);
            // Create now count
            $now_count = 0;
            // Foreach $backing_level
            foreach ($backing_level as $key => $value2) {
                $now_count = $now_count + $value2->now_count;
                // Gan" now count  = total now count
                $value->now_count=$now_count;
            }
            // if backing level not found now count =0
            $value->now_count=$now_count;
            // Get User Id -> get user name
            $user = $this->users_model->getUserId($value->user_id);
            $value->username=$user->username;

            $sum = $this->backing_model->abc($value->id);
            if (isset($sum->project_id) && isset($sum->now_count)) {
                if($value->id == $sum->project_id){
                    $value->now_count = $sum->now_count;
                }
            } else {
                $value->now_count = 0;
            }
            if($value->collection_end_date < $date){
                $value->public = true;
            }else{
                $value->public = false;
            }

        }
        function method1($a,$b) {
            return ($a->now_count <= $b->now_count) ? -1 : 1;
        }
        function method2($a,$b) {
            return ($a->now_count <= $b->now_count) ? 1 : -1;
        }

        if(isset($sort) && $sort === 'id' &&  $by === 'ASC'){

            usort($data['data']['project'], "method2");

        }else if(isset($sort) && $sort === 'id' &&  $by === 'DESC'){

            usort($data['data']['project'], "method1");

        }
        //load view
        $this->_renderAdminLayout('admin/project/index', $data);
    }
    //Add Project
    public function add() {
        //Load ckeditor
        $this->carabiner->js('ckeditor/ckeditor.js');
        $this->carabiner->js('js/ckeditor-loader.js');
        //Get Category
        $data['category'] = $this->category_model->getAllCategories();
        $project_type = $this->input->post('project_type');
        //set api key
        $key = $this->setting_model->getSettings();
        $stripe = array(
            "secret_key"      => $key->secret_key,
            "publishable_key" => $key->public_key
        );
        $data['settingKey'] = $stripe['secret_key'];

        //click save
        if ($this->input->post('save') || $this->input->post('public')) {

            //form_validation
            $this->form_validation->set_rules(
                'project_name', 'プロジェクト名',
                'required|max_length[150]',
                array(
                    'required'=>'プロジェクト名を入力してください。',
                    'max_length'=>'プロジェクト名は最大150文字までです。'
                )
            );
            $this->form_validation->set_rules(
                'summary', 'はじめにご挨拶',
                'max_length[210]',
                array(
                  'max_length'=>'「はじめにご挨拶」の文書は最大255文字までです。'
                )
            );
            $this->form_validation->set_rules(
                'video', '動画URL',
                'valid_url',
                array(
                  'valid_url'=>'動画のURLを入力してください'
                )
            );
            $this->form_validation->set_rules(
                'itemName', 'ユーザー名',
                'required',
                array(
                  'required'=>'ユーザー名を入力してください'
                )
            );

            if($project_type === '0') {
                $this->form_validation->set_rules(
                    'goal_amount', '目標金額',
                    'required|min_length[6]',
                    array(
                      'required'=>'目標金額を入力してください',
                      'min_length'=>'目標金額は、10,000円以上の金額で入力してください。'
                    )
                );
                $this->form_validation->set_rules(
                    'date', '募集終了日',
                    'required',
                    array(
                      'required'=>'募集終了日を入力してください',
                    )
                );
            }

            // If True
            if ($this->form_validation->run() !== false) {
                // User Model project_model insert
                $id = $this->project_model->insert();
                if($this->input->post('save')) {
                    //Use FlashDate
                    $this->session->set_flashdata('msg', '保存されました。');
                    //redirect
                    $this->session->set_flashdata('tab', '1');
                }else if($this->input->post('public')) {
                    //Use FlashDate
                    $this->session->set_flashdata('msg', '公開されました。');
                    //redirect
                    $this->session->set_flashdata('tab', '1');
                }

                redirect('/project/edit/'.$id,$newdata);
            } else {
                $this->session->set_flashdata('error', strip_tags(validation_errors()));
                redirect('/project/add');
            }
        } else {
            //redirect
            $this->_renderAdminLayout('admin/project/add', $data);
        }
    }

    // Edit Project

    public function updateshoworder( $backing_levels_id ) {

        $is_up = $_REQUEST['is_up'];
        $currentB= $this->backing_model->getBacking( $backing_levels_id);
        $arrReturn["is_ok"] = 1;

        $show_order_current = $currentB->show_order;
        if($is_up ==1){
            $minid = $this->db->query('SELECT MIN(show_order) AS `minid` FROM `backing_levels` where project_id='.$currentB->project_id)->row()->minid;
            if($show_order_current<=$minid){
                $arrReturn["is_ok"] = 0;
                $arrReturn["msg"] = "MIN ERROR!";
                return $this->output
                    ->set_content_type('application/json')
                    ->set_status_header(500)
                    ->set_output(json_encode($arrReturn));
            }
            $show_order = $show_order_current -1;
//            $updateOld = $this->backing_model->getBackingFromProjectIdAndShowOrder($currentB->project_id,$show_order);
//            $dataUpdateBackedOld['show_order'] = $show_order_current;
//            $updateBackedOld = $this->backing_model->updateBacking($updateOld->id,$dataUpdateBackedOld);

        }else{
            // is down
            $show_order  = $show_order_current +1;
            $maxid = $this->db->query('SELECT MAX(show_order) AS `maxid` FROM `backing_levels` where project_id='.$currentB->project_id)->row()->maxid;
            if( $show_order_current >=$maxid){
                $arrReturn["is_ok"] = 0;
                $arrReturn["msg"] = "MAX ERROR!";
                return $this->output
                    ->set_content_type('application/json')
                    ->set_status_header(500)
                    ->set_output(json_encode($arrReturn));
            }

        }
        $updateOld = $this->backing_model->getBackingFromProjectIdAndShowOrder($currentB->project_id,$show_order);
        $dataUpdateBackedOld['show_order'] = $show_order_current;
        $updateBackedOld = $this->backing_model->updateBacking($updateOld->id,$dataUpdateBackedOld);
        $dataUpdateBacked['show_order'] = $show_order;
        $updateBacked = $this->backing_model->updateBacking($backing_levels_id,$dataUpdateBacked);
        $arrReturn['project_id']=$currentB->project_id;


        return $this->output
            ->set_content_type('application/json')
            ->set_status_header(200)
            ->set_output(json_encode($arrReturn));


    }
    public function get_edit_data($id) {
        $key_firebase  = $this->config->item('key_firebase');

        $roomSocket = $this->config->item('roomSocket');

        $portRoomSocket = $this->config->item('portRoomSocket');
        if ($this->uri->segment(5) === FALSE) {
            $page = 0;
        } else {
            $page = $this->uri->segment(5);
            if($page >= 1){
                $this->session->set_flashdata('tab', '3');
            }
        }
        // change tab
        $tab = $this->input->get('tab');
        // check $tab
        if(isset($tab) && $tab){
            // set session
            $this->session->set_flashdata('tab', $tab);
        }
        //Load ckeditor
        $this->carabiner->js('ckeditor/ckeditor.js');
        $this->carabiner->js('js/ckeditor-loader.js');
        //Get Category
        $data['category'] = $this->category_model->getAllCategories();
        // Get Project folow Id
        $data['project'] = $this->project_model->getProjectById($id);
        $data['users'] = $this->users_model->getUserId($data['project']->user_id);
        $data['address_user'] = $this->users_model->getAddressUserByUserId($data['project']->user_id);
        $data['project_type'] = $data['project']->project_type;

        $data['user_name'] = $this->users_model->getUserId($data['project']->user_id);

        $emailOnwer = $data['user_name']->email;
        $usernameOnwer =  $data['user_name']->username;
        // Check id is exists
        if(!$data['project']){
            // Show Error and redirect Project
            $this->session->set_flashdata('error', 'このURLが存在しません。');
            redirect('/project/');
        }
        //Get Return (backed_level)
        $data['project_return'] = $this->projectreturn_model->getProjectReturnByProjectId($id);

        // Get $backing_level(Get now_count)
        $backing_level = $this->backing_model->getBackingLevelId($data['project']->id);

        // Create now count
        $now_count = 0;

        // Foreach $backing_level
        foreach ($backing_level as $key => $value2) {

            $now_count = $now_count + $value2->now_count;
            // Gan" now count  = total now count
            $data['project']->now_count=$now_count;

        }
        // if backing level not found now count =0
        $data['project']->now_count=$now_count;

        $date1 = new DateTime($data['project']->collection_end_date);
        $date2 = new DateTime(date('Y-m-d H:i:s'));
        if($date1 > $date2){
            $time = $date1->diff($date2);

            $date = date_diff($date1,$date2);
            $date = $date->format("%a");
            $data['project']->end_date = array(
                'date' =>$date,
                'hour' => $time->h,
                'minutes' => $time->i,
            );
        } else {
            $data['project']->end_date = array(
                'status'=>'終了'
            );
        }

        $data['favourite_project'] = $this->favourite_model->checkProject($id);
        $data['pickup_project'] = $this->pickup_model->checkProject($id);
        $data['recommends_project'] = $this->recommends_model->checkProject($id);
        // Get User Id -> get user name
        $data['backedProject'] = $this->backed_model->getBackedProjectId($id,$page);

        $data['ListBackedProjectEdit'] = $this->backing_model->getBackingByProjectId($id);

        if(isset($data['backedProject']['backed'])
            && $data['backedProject']['backed']) {

            foreach ($data['backedProject']['backed'] as $key => $backed) {

                $backing = $this->backing_model->getBacking($backed->backing_level_id);
                if(isset($backing) && $backing){
                    $backed->backingReturn = $backing->return_amount;
                    $backed->invest_amount = $backing->invest_amount;
                    $backed->now_count = $backing->now_count;
                    $backed->thumnail = $backing->thumnail;
                    $backed->schedule = $backing->schedule;
                    $backed->show_order = $backing->show_order;
                    if($backed->status === 'refunded'){
                        $data['project']->status = 'refunded';
                    }
                }
            }
            foreach ($data['backedProject']['backed'] as $key => $value) {
                if(isset($value->user_id) && $value->user_id) {
                    $user = $this->users_model->getUserId($value->user_id);
                    $value->username = $user->username;
                } else {
                    $value->username = $value->name_return;
                }
            }
        } else {
            $data['project']->status = 'refunded';
        }

//        $data['idProject'] = $id;
//        $data['goal_amount'] = $data['project']->goal_amount;
//        $data['total'] = 0;
//
//        $backedall = $this->backed_model->getBackedAll($id);
//
//        foreach ($backedall as $key => $value) {
//            if($value->type === 'Credit' && $value->status === 'succeeded'){
//                $data['total'] = $data['total'] + $value->invest_amount;
//            }
//            if($value->type === 'Bank' && $value->status === 'succeeded'){
//                $data['total'] = $data['total'] + $value->invest_amount;
//            }
//        }
        usort($data['project_return'], function($a, $b)
        {
            return strcmp($a->show_order, $b->show_order);
        });
        $this->_renderBakingAjax('admin/project/ajax_backing', $data);
//        return $this->output
//            ->set_content_type('application/json')
//            ->set_status_header(200)
//            ->set_output(json_encode($data['project_return']));

    }
    public function edit($id) {
        $key_firebase  = $this->config->item('key_firebase');

        $roomSocket = $this->config->item('roomSocket');

        $portRoomSocket = $this->config->item('portRoomSocket');
        if ($this->uri->segment(5) === FALSE) {
            $page = 0;
        } else {
            $page = $this->uri->segment(5);
            if($page >= 1){
                $this->session->set_flashdata('tab', '3');
            }
        }
        // change tab
        $tab = $this->input->get('tab');
        // check $tab
        if(isset($tab) && $tab){
            // set session
            $this->session->set_flashdata('tab', $tab);
        }
        //Load ckeditor
        $this->carabiner->js('ckeditor/ckeditor.js');
        $this->carabiner->js('js/ckeditor-loader.js');
        //Get Category
        $data['category'] = $this->category_model->getAllCategories();
        // Get Project folow Id
        $data['project'] = $this->project_model->getProjectById($id);
        $data['users'] = $this->users_model->getUserId($data['project']->user_id);
        $data['address_user'] = $this->users_model->getAddressUserByUserId($data['project']->user_id);
        $data['project_type'] = $data['project']->project_type;

        $data['user_name'] = $this->users_model->getUserId($data['project']->user_id);

        $emailOnwer = $data['user_name']->email;
        $usernameOnwer =  $data['user_name']->username;
        // Check id is exists
        if(!$data['project']){
            // Show Error and redirect Project
            $this->session->set_flashdata('error', 'このURLが存在しません。');
            redirect('/project/');
        }
        //Get Return (backed_level)
        $data['project_return'] = $this->projectreturn_model->getProjectReturnByProjectId($id);

        // Get $backing_level(Get now_count)
        $backing_level = $this->backing_model->getBackingLevelId($data['project']->id);

        // Create now count
        $now_count = 0;

        // Foreach $backing_level
        foreach ($backing_level as $key => $value2) {

            $now_count = $now_count + $value2->now_count;
            // Gan" now count  = total now count
            $data['project']->now_count=$now_count;

        }
        // if backing level not found now count =0
        $data['project']->now_count=$now_count;

        $date1 = new DateTime($data['project']->collection_end_date);
        $date2 = new DateTime(date('Y-m-d H:i:s'));
        if($date1 > $date2){
            $time = $date1->diff($date2);

            $date = date_diff($date1,$date2);
            $date = $date->format("%a");
            $data['project']->end_date = array(
                'date' =>$date,
                'hour' => $time->h,
                'minutes' => $time->i,
            );
        } else {
            $data['project']->end_date = array(
                'status'=>'終了'
            );
        }

        $data['favourite_project'] = $this->favourite_model->checkProject($id);
        $data['pickup_project'] = $this->pickup_model->checkProject($id);
        $data['recommends_project'] = $this->recommends_model->checkProject($id);
        // Get User Id -> get user name
        $data['backedProject'] = $this->backed_model->getBackedProjectId($id,$page);

        $data['ListBackedProjectEdit'] = $this->backing_model->getBackingByProjectId($id);

        if(isset($data['backedProject']['backed'])
            && $data['backedProject']['backed']) {

            foreach ($data['backedProject']['backed'] as $key => $backed) {

                $backing = $this->backing_model->getBacking($backed->backing_level_id);
                if(isset($backing) && $backing){
                    $backed->backingReturn = $backing->return_amount;
                    $backed->invest_amount = $backing->invest_amount;
                    $backed->now_count = $backing->now_count;
                    $backed->thumnail = $backing->thumnail;
                    $backed->schedule = $backing->schedule;
                    $backed->show_order = $backing->show_order;
                    if($backed->status === 'refunded'){
                        $data['project']->status = 'refunded';
                    }
                }
            }
            foreach ($data['backedProject']['backed'] as $key => $value) {
                if(isset($value->user_id) && $value->user_id) {
                    $user = $this->users_model->getUserId($value->user_id);
                    $value->username = $user->username;
                } else {
                    $value->username = $value->name_return;
                }
            }
        } else {
            $data['project']->status = 'refunded';
        }

        $data['idProject'] = $id;
        $data['goal_amount'] = $data['project']->goal_amount;
        $data['total'] = 0;

        $backedall = $this->backed_model->getBackedAll($id);

        foreach ($backedall as $key => $value) {
            if($value->type === 'Credit' && $value->status === 'succeeded'){
                $data['total'] = $data['total'] + $value->invest_amount;
            }
            if($value->type === 'Bank' && $value->status === 'succeeded'){
                $data['total'] = $data['total'] + $value->invest_amount;
            }
        }

        //If click save
        if ($this->input->post('save') || $this->input->post('public')) {

            //Use Model Function update with id

            $this->project_model->update($id);

            // After update redirect project edit with id
                if($this->input->post('save')){
                    $this->session->set_flashdata('msg', '保存されました。');
                    // Submit button public and public =2 ( close project)
                }else if($this->input->post('public')
                        && $this->input->post('public') ==='2') {
                    // Get Backed User Donate
                    $backed = $this->backed_model->getBackedAll($id);

                    // Create List Email
                    $listUserEmail = [];
                    // use foreach push email in array $listUserEmail
                    foreach ($backed as $key => $value) {
                        // Get User Id
                        $userEmail = $this->users_model->getUserId($value->user_id);
                        // Push Array
                        if (!in_array($userEmail->email, $listUserEmail)) {
                            array_push($listUserEmail,$userEmail->email);
                        }
                    }

                    if($data['project']->project_type === '1') {

                        $listDonateFanClub = $this->listdonatefanclub_model->getInfoDonateFanClub($id);

                        require_once APPPATH."third_party/stripe/init.php";

                        $key = $this->setting_model->getSettings();

                        $stripe = array(
                            "secret_key"      => $key->secret_key,
                            "publishable_key" => $key->public_key
                        );

                        \Stripe\Stripe::setApiKey($stripe['secret_key']);
                        if(isset($listDonateFanClub) && $listDonateFanClub) {

                            foreach ($listDonateFanClub as $key => $value) {

                                $subscription = \Stripe\Subscription::retrieve($value->sub_fanclub_id);
                                var_dump($subscription->status);
                                if(isset($subscription) && $subscription->status !== 'canceled') {
                                    $cancelDonate = $subscription->cancel();
                                }

                            }
                        }
                    }
                    exit;

                    // $dataProject = array(
                    //     'email_close' => $listUserEmail
                    // );
                    // echo "<pre>";
                    // var_dump($dataProject);
                    // exit;
                    // // Check $listUserEmail > 1
                    if(isset($listUserEmail) && $listUserEmail){
                        // Send Mail For User
                        // Get User
                        $user = $this->users_model->getUserId($this->input->post('itemName'));
                        // Get Email
                        $email = $listUserEmail;
                        // Subject
                        $subject = '【KAKUSEIDA】本人確認のお知らせ';
                        // Message
                        $message = '
                        支援者の皆様<br />
                        KAKUSEIDA運営事務局です<br /><br />
                        支援していただいた以下のプロジェクトは途中で終了になりました。<br />
                        プロジェクト名：'.$this->input->post('project_name').' <br />
                        企画者： '.$user->username.' <br />
                        企画したプロジェクトはキャンセルとなり、支援していただいたお金は全て支援者の方に返金されます。<br />
                        返金スケジュールなど、詳細については追って御連絡させていただきます。<br />
                        ご不便をかけて大変申し訳ありません。<br />

                        今後ともKAKUSEIDAをよろしくお願い致します。';
                        // Send Mail
                        SendMail($email,$subject,$message);
                    }

                    // // Send Mail For Owner
                    // // // Subject
                    $subject = '【KAKUSEIDA】本人確認のお知らせ';
                    // Message
                    $messageOnwer =
                    ''.$usernameOnwer.'様<br />
                    KAKUSEIDA運営事務局です<br /><br />

                    お世話になっております。<br />
                    総合的な理由により'.$usernameOnwer.'様が企画していただいているプロジェクトは途中で終了になります。<br />
                    支援していただいたお金は全て支援者の方に返金されます。<br />
                    返金スケジュールなど、詳細については追って御連絡させていただきます。<br />
                    ご不便をかけて大変申し訳ありません。<br />
                    よろしくお願い致します。<br />';
                    // Send Mail
                    SendMail($emailOnwer,$subject,$messageOnwer);
                    // Session send Message for Admin
                    $this->session->set_flashdata('msg', '終了されました。');

                } else {

                    if($data['project']->project_type === '1') {
                        //include Stripe PHP library
                        require_once APPPATH."third_party/stripe/init.php";

                        //set api key
                        $key = $this->setting_model->getSettings();
                        $stripe = array(
                            "secret_key"      => $key->secret_key,
                            "publishable_key" => $key->public_key
                        );
                        \Stripe\Stripe::setApiKey($stripe['secret_key']);
                        if(isset($data['project_return'])
                            && $data['project_return']) {
                            foreach ($data['project_return'] as $key => $value) {
                                if($value->plan_fanclub_id === null){
                                    $plan = \Stripe\Plan::create([
                                        "nickname" => "Donate_".$value->invest_amount,
                                        "product" => $data['project']->product_fanclub_id,
                                        "amount" => $value->invest_amount,
                                        "currency" => "JPY",
                                        "interval" => "day",
                                        "interval_count" => 30*$data['project']->number_month,
                                        "usage_type" => "licensed",
                                    ]);
                                    $dataupdateFan = array(
                                        'plan_fanclub_id' => $plan->id
                                    );
                                    $update = $this->backing_model->updateBacking($value->id,$dataupdateFan);
                                }
                            }
                        }
                    }
                    // create Datetime
                    $datetime = new DateTime();
                    $datetime = $datetime->format('Y-m-d H:i:s');
                    // get strtotime + change time zone
                    $asia_timestamp = strtotime($datetime);
                    date_default_timezone_set('UTC');
                    // get Date vs time zone
                    $date = date("Y-m-d H:i:s", $asia_timestamp);

                    $projectName = $data['project']->project_name;
                    // array create notification info
                    $dataNotification = array(
                        'user_id' => $data['project']->user_id,
                        'title' => 'プロジェクトが公開されました ',
                        'user_action' => 'Admin',
                        'message' => '「'.$projectName.'」プロジェクトが公開されました。',
                        'link'=> "project-detail/".$data['project']->id,
                        'status' => 0,
                        'thumbnail' => image_url().'/images/2018/default/logo-icon.png',
                        'created' => $date
                    );
                    // create notification info
                    $createNotification = $this->notification_info->insert($dataNotification);
                    // check exists
                    if($createNotification){
                        $curentId = $this->ion_auth->get_users_groups()->row()->id;
                        $currentUser = $this->users_model->getUserId($curentId);
                        $currentImage = $currentUser->profileImageURL;
                        // use SocketIO annouce for user
                        $socketio = new SocketIO();
                        // array
                        $data = array(
                            'room' => 'project_'.$id,
                            'title' => 'プロジェクトが公開されました',
                            'message' => '「'.$projectName.'」プロジェクトが公開されました。',
                            'url'=> app_url()."project-detail/".$id,
                            'ava' => image_url().'/images/2018/default/logo-icon.png'
                        );

                        $socketio->send($roomSocket, $portRoomSocket, 'public_project',json_encode($data));

                        $curl = curl_init();

                        curl_setopt_array($curl, array(
                            CURLOPT_URL => "https://fcm.googleapis.com/fcm/send",
                            CURLOPT_RETURNTRANSFER => true,
                            CURLOPT_ENCODING => "",
                            CURLOPT_MAXREDIRS => 10,
                            CURLOPT_TIMEOUT => 30,
                            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                            CURLOPT_CUSTOMREQUEST => "POST",
                            CURLOPT_POSTFIELDS => "{   \"notification\": {\r\n        \"title\": \"プロジェクトが公開されました\",\r\n        \"body\": \"「".$projectName."」プロジェクトが公開されました。\",\r\n        \"click_action\": \"".app_url()."project-detail/".$id."\",\r\n        \"icon\": \"".image_url()."images/2018/default/logo-icon.png\"\r\n    },\r\n  \"to\" : \"/topics/project_".$id."\"\r\n}",
                            CURLOPT_HTTPHEADER => array(
                                "Authorization: key=".$key_firebase."",
                                "Content-Type: application/json",
                            ),
                        ));
                        $response = curl_exec($curl);
                        $err = curl_error($curl);

                        curl_close($curl);
                        // Session send message for admin
                    }
                    // Session send message for admin
                    $this->session->set_flashdata('msg', '公開されました。');
                }
                // Redirect project
                redirect('/project/');
                // If not submit button
        } else {
            // load layout
            usort($data['project_return'], function($a, $b)
            {
                return strcmp($a->show_order, $b->show_order);
            });
            $this->_renderAdminLayout('admin/project/edit', $data);
        }
    }


    //Edit Donate when Admin confirm User Donate success Bank!
    public function editDonate($id){
        $key_firebase  = $this->config->item('key_firebase');
        $roomSocket = $this->config->item('roomSocket');
        $portRoomSocket = $this->config->item('portRoomSocket');
        // Check Id exists!
        if(isset($id) && $id){
            // Get Info Donate Bank with id
            $data['backed'] = $this->backed_model->getBackedId($id);
            // Check Info is exists!
            if(isset($data['backed']) && $data['backed']){
                // Get Info Project, ProjectReturn current, All Project Return, User
                $data['project'] = $this->project_model->getProjectById($data['backed']->project_id);
                $data['project_return'] = $this->backing_model->getBacking($data['backed']->backing_level_id);
                $data['allProejctReturn'] = $this->backing_model->getBackingLevelId($data['backed']->project_id);
                $data['user'] = $this->users_model->getUserId($data['backed']->user_id);

                $number_month = $data['project']->number_month;
                $timestart = $data['project']->collection_start_date;
                $project_type = $data['project']->project_type;
                $user_id = $data['user']->id;
                $backing_level_id = $data['project_return']->id;
                // Click Button
                if ($this->input->post('save')) {
                    // Get Info
                    $money_donate =$this->input->post('money_donate');
                    // Check Info Exists

                    if(!isset($money_donate)){
                        // If info don't exists redirect about current Page and show error!
                        $this->session->set_flashdata('error', '支援のデータは存在しません');
                        redirect('/project/editDonate/'.$data['backed']->id);
                    }
                    // Get Info Status
                    $status = $this->input->post('status');
                    $status_refunded = $this->input->post('status_refunded');

                    // Check Info === '1' Assign a variable $status = 'succeeded';
                    // And Else $status = 'unsuccess';
                    if($status === '1'){
                        $status = 'succeeded';
                    }else{
                        $status = 'unsuccess';
                    }
                    if($status_refunded === '1'){
                        $status = 'refunded';
                    }

                    // Create Array and Assign Data
                    $dataUpdate = array(
                        'backing_level_id'=>$money_donate,
                        'status'=>$status
                    );

                    // Use function updateBacked with Id and $dataUpdate
                    // for update data of info Donate Bank
                    // (confirm Users donate success ?)
                    $updateBacked = $this->backed_model->updateBacked($id,$dataUpdate);


                    if($updateBacked) {
                        if($data['backed']->status === 'unsuccess'
                            && $status === 'succeeded') {
                            $now_count = $data['project_return']->now_count;
                            $dataUpdateBacked = array (
                                'now_count'=>$now_count + 1
                            );

                            // perform $updateBacked success
                            if($project_type === '1') {

                                $time = getMonthProject($timestart);
                                $time = ceil($time/$number_month);
                                $dateTime = new DateTime();
                                $dateTime = $dateTime->format('Y-m-d H:i:s');
                                $dataListDonate = array(
                                    'user_id' => $user_id,
                                    'project_id' => $data['project']->id,
                                    'backing_levels_id' => $backing_level_id,
                                    'month' => $time,
                                    'created' => $dateTime
                                );

                                $this->db->insert('listuserdonate_fanclub', $dataListDonate);
                            }

                            $updateBacked = $this->backing_model->updateBacking($data['project_return']->id,$dataUpdateBacked);

                            $collected_amount = $data['project']->collected_amount;
                            $dataUpdateProject = array(
                                'collected_amount' => $collected_amount + $data['backed']->invest_amount
                            );
                            $updateProject = $this->project_model->updateProject($data['project_return']->project_id,$dataUpdateProject);

                            // create Datetime
                            $datetime = new DateTime();
                            $datetime = $datetime->format('Y-m-d H:i:s');
                            // get strtotime + change time zone
                            $asia_timestamp = strtotime($datetime);
                            date_default_timezone_set('UTC');
                            // get Date vs time zone
                            $date = date("Y-m-d H:i:s", $asia_timestamp);

                            // get data for socket + create notification_info + firebase
                            $userDonate = $data['user']->username;
                            $investAmountDonate = $data['backed']->invest_amount;
                            $imageUserDonate = $data['user']->profileImageURL;
                            $projectId = $data['backed']->project_id;
                            $projectName = $data['project']->project_name;
                            // array create notification info
                            $dataNotification = array(
                                'user_id' => $data['project']->user_id,
                                'user_action' => $userDonate,
                                'title' => '支援があります',
                                'message' => ''.$userDonate.'さんが 「'.$projectName.'」に'.number_format($investAmountDonate).'円支援しました',
                                'link'=> "my-page/statistic/".$projectId,
                                'status' => 0,
                                'thumbnail' => $imageUserDonate,
                                'created' => $date
                            );
                            // create notification info
                            $createNotification = $this->notification_info->insert($dataNotification);
                            // check exists
                            if($createNotification) {
                                // use SocketIO annouce for user
                                $curl = curl_init();

                                curl_setopt_array($curl, array(
                                    CURLOPT_URL => "https://fcm.googleapis.com/fcm/send",
                                    CURLOPT_RETURNTRANSFER => true,
                                    CURLOPT_ENCODING => "",
                                    CURLOPT_MAXREDIRS => 10,
                                    CURLOPT_TIMEOUT => 30,
                                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                                    CURLOPT_CUSTOMREQUEST => "POST",
                                    CURLOPT_POSTFIELDS => "{   \"notification\": {\r\n        \"title\": \"支援があります\",\r\n        \"body\": \"".$userDonate." さんが「".$projectName."」に".number_format($investAmountDonate)."円支援しました\",\r\n        \"click_action\": \"".app_url()."my-page/statistic/".$projectId."\",\r\n        \"icon\": \"".image_url()."images/2018/default/logo-icon.png\"\r\n    },\r\n  \"to\" : \"/topics/project_".$projectId."\"\r\n}",
                                    CURLOPT_HTTPHEADER => array(
                                        "Authorization: key=".$key_firebase."",
                                        "Content-Type: application/json",
                                    ),
                                ));

                                $response = curl_exec($curl);
                                $err = curl_error($curl);

                                curl_close($curl);

                                $socketio = new SocketIO();
                                // // array
                                $data = array(
                                    'room' => 'project_'.$projectId,
                                    'title' => '支援があります',
                                    'message' => ''.$userDonate.'さんが 「'.$projectName.'」に'.number_format($investAmountDonate).'円支援しました',
                                    'url'=> app_url()."my-page/statistic/".$projectId,
                                    'ava' => $imageUserDonate
                                );

                                $socketio->send('socket.kakuseida-test.tk', 4000, 'donate_project',json_encode($data));
                                // Session send message for admin
                            }
                            $this->session->set_flashdata('msg', '更新ができました。');
                            $this->session->set_flashdata('tab', '3');
                            redirect('/project/edit/'.$projectId);

                        }else if($data['backed']->status === 'succeeded'
                                && $status === 'unsuccess') {
                            $now_count = $data['project_return']->now_count;
                            $dataUpdateBacked = array(
                                'now_count'=>$now_count - 1
                            );
                            $updateBacked = $this->backing_model->updateBacking($data['project_return']->id,$dataUpdateBacked);

                            $collected_amount = $data['project']->collected_amount;
                            $dataUpdateProject = array(
                                'collected_amount' => $collected_amount - $data['backed']->invest_amount
                            );
                            $updateProject = $this->project_model->updateProject($data['project_return']->project_id,$dataUpdateProject);
                        }else{
                            $now_count = $data['project_return']->now_count;
                        }
                        // Redirect about Project Edit with ProjectId and msg success
                        $projectId = $data['backed']->project_id;
                        $this->session->set_flashdata('msg', '更新ができました。');
                            $this->session->set_flashdata('tab', '3');
                            redirect('/project/edit/'.$projectId);
                            // perform $updateBacked Fail
                    } else {
                        // Redirect about Project Edit with ProjectId and error
                        $projectId = $data['backed']->project_id;
                        $this->session->set_flashdata('error', '更新ができました。');
                        $this->session->set_flashdata('tab', '3');
                        redirect('/project/edit/'.$projectId);
                    }
                    // If don't click button save then show view
                } else {
                    // Use function show view(MY_Controller)
                    $this->_renderAdminLayout('admin/project/editDonate',$data);
                }
                // If don't data of $data['backed'] redirect and show error
            } else {
                $this->session->set_flashdata('error', '支援のデータは存在しません');
                redirect('/project/');
            }
            // If don't data of $id redirect and show error
        } else {
            $this->session->set_flashdata('error', '支援のデータは存在しません');
            redirect('/project/');
        }
    }


    // Delete with id
    public function delete($id) {
        //Check id is exists
        if ((int) $id > 0) {
            //Delete project with id
            $project = $this->project_model->getProjectById($id);

            $date = new DateTime();
            $date = $date->format('Y-m-d H:i:s');

            if($project->active === 'yes' && $project->collection_end_date > $date){
                $this->session->set_flashdata('error', 'ーが出ましたので、プロジェクトを削除できません');
                redirect('/project');
            }else{
                $backed = $this->backed_model->getBackedAll($id);
                $ArrayBacked = [];
                foreach ($backed as $key => $value) {
                    array_push($ArrayBacked,$value->id);
                }
                $inbacked = implode(',', $ArrayBacked);
                // Use where check project with id
                $this->db->where("id in ($inbacked)");
                // Delete talbe project with id
                $this->db->delete('backed_projects');

                $backing_level = $this->backing_model->getBackingLevelId($id);
                $ArrayBackingLevel = [];
                foreach ($backing_level as $key => $value) {
                    array_push($ArrayBackingLevel,$value->id);
                }

                $inbacking = implode(',', $ArrayBackingLevel);
                // Use where check project with id
                $this->db->where("id in ($inbacking)");
                // Delete talbe project with id
                $this->db->delete('backing_levels');

                $this->project_model->delete($id);

                $this->session->set_flashdata('msg', 'プロジェクトのカテゴリーが削除されました。');
                redirect('/project');
            }
        }
        //redirect Project

    }
    //Action is many Delete project
    public function action() {
        //Get id
        $val = $this->input->post('val');
        // Get Action
        $action = $this->input->post('hidAction');
        // Get id

        $date = new DateTime();
        $date = $date->format('Y-m-d H:i:s');
        $arrayProject = array();
        foreach ($val as $key => $value) {
            $project = $this->project_model->getProjectById($value);

            if($project->active === 'yes' && $project->collection_end_date > $date){
            }else{
                array_push($arrayProject,$value);

                $backed = $this->backed_model->getBackedAll($value);
                $ArrayBacked = [];
                foreach ($backed as $key => $value) {
                    array_push($ArrayBacked,$value->id);
                }
                $inbacked = implode(',', $ArrayBacked);
                // Use where check project with id
                $this->db->where("id in ($inbacked)");
                // Delete talbe project with id
                $this->db->delete('backed_projects');

                $backing_level = $this->backing_model->getBackingLevelId($value);
                $ArrayBackingLevel = [];
                foreach ($backing_level as $key => $value) {
                    array_push($ArrayBackingLevel,$value->id);
                }

                $inbacking = implode(',', $ArrayBackingLevel);
                // Use where check project with id
                $this->db->where("id in ($inbacking)");
                // Delete talbe project with id
                $this->db->delete('backing_levels');

                $this->project_model->delete($value);
            }
        }
        $this->session->set_flashdata('msg', 'プロジェクトのカテゴリーが削除されました。');
        redirect('/project');
    }

    private function guidv4()
    {
        if (function_exists('com_create_guid') === true)
            return trim(com_create_guid(), '{}');

        $data = openssl_random_pseudo_bytes(16);
        $data[6] = chr(ord($data[6]) & 0x0f | 0x40); // set version to 0100
        $data[8] = chr(ord($data[8]) & 0x3f | 0x80); // set bits 6-7 to 10
        return vsprintf('%s%s-%s-%s-%s-%s%s%s', str_split(bin2hex($data), 4));
    }
    private function getBakingNeedChange($project_id,$arGuid){


        $this->db->select('id,project_id,show_order,guid_id');
        $this->db->from('`kaku_tk`.`backing_levels`');
        $this->db->where('project_id', $project_id);

        $this->db->where_in('IFNULL(guid_id, \'\')', $arGuid);
        $this->db->order_by('show_order', 'ASC');
        $query = $this->db->get();
        $data = array();
        if($query !== FALSE && $query->num_rows() > 0){
            $data = $query->result_array();
        }
        return $data;
    }
    private function updateShowOrderPlus1($show_order_need,$project_id,$arGuid){
       // Lay nhung project co sap xep theo show_order.tru nhung Guid moi them vao
        $this->db->select('id,project_id,show_order');
        $this->db->from('`kaku_tk`.`backing_levels`');
        //
        $this->db->where('project_id', $project_id);
        $this->db->where('show_order >=', $show_order_need);
        $this->db->where_not_in('IFNULL(guid_id, \'\')', $arGuid);
        $this->db->order_by('show_order', 'ASC');
        $query = $this->db->get();
        $data = array();
        if($query !== FALSE && $query->num_rows() > 0){
            $data = $query->result_array();
        }
        //updateShowOrderPlus1
        foreach($data as $obj){
            $id = $obj['id'];
            $this->db->where('id',$id);
            $dataUpdate = $obj;
            $dataUpdate['show_order'] = $obj['show_order'] +1;
            $data = $this->db->update('`kaku_tk`.`backing_levels`',$dataUpdate);

        }
        return $data;
    }
    private function showOrderReChange($arrNeed,$project_id){
        //1.lay du lieu trong bang backing_levels ngoai tru guid
        //2.Chen vao kiem tra vi tri hop le
//       $newvalueShowOrder = array(
//           'project_id' => $value->project_id,
//           'show_order' => $value->show_order,
//           'guid' => $myGUid,
//       );

        $arGuid = [];
        foreach($arrNeed as $obj){
            $arGuid[] = $obj['guid'];
        }

        $this->db->select('id,project_id,show_order');
        $this->db->from('`kaku_tk`.`backing_levels`');
       //
        $this->db->where('project_id', $project_id);
        $this->db->where_not_in('IFNULL(guid_id, \'\')', $arGuid);
        $this->db->order_by('show_order', 'ASC');
        $query = $this->db->get();

        $data = array();
        if($query !== FALSE && $query->num_rows() > 0){
            $data = $query->result_array();
        }
        $myNumMax = $query->num_rows();

        $arrNeed1 = $this->getBakingNeedChange($project_id,$arGuid);

        foreach($arrNeed1 as $obj){
            $arGuid[] = $obj['guid_id'];
            $show_order = $obj['show_order'];
            if( $show_order <=0){
                $show_order = 1;
            }
            if( $show_order >$myNumMax ){
                $show_order = $myNumMax+1;
            }
            $this->updateShowOrderPlus1($show_order,$project_id,$arGuid);


        }


    }
    public function updateShowOrderAll($project_id){

        $calCulateOrder = 1;
        $SQL = "
                       
                        SELECT 
                            id,project_id,show_order
                        FROM
                            `kaku_tk`.`backing_levels`
                        
                         WHERE  `project_id`=".$project_id." order by show_order";
        $query = $this->db->query($SQL);

        $myData = $query->result_array();
        $numberUpdate =1;
        foreach ($myData as $data){
            //num,show_order,project_id
            $project_id = $data['project_id'];
            $dataBaking =$data;
            $dataBaking['show_order'] = $numberUpdate;
            $this->db->where('id',$data['id']);
            $data = $this->db->update('`kaku_tk`.`backing_levels`',$dataBaking);
            $numberUpdate ++;
        }
//        $maxShowOrder = $numberUpdate-1;
//        if( $clientShowOrder>$clientShowOrder){
//            $calCulateOrder = $numberUpdate;
//        }
//        if( $clientShowOrder<1){
//            $calCulateOrder = 1;
//        }
//        $_REQUEST['is_up'] = 1;
//        $this->updateShowOrder($backing_levels_id);

    }
    public function actionActive($projecId){
        $myGUid = $this->guidv4();
        $key_firebase  = $this->config->item('key_firebase');
        $roomSocket  = $this->config->item('roomSocket');
        $portRoomSocket  = $this->config->item('portRoomSocket');

        $data['project'] = $this->project_model->getProjectById($projecId);
        $data['project_type'] = $data['project']->project_type;
        $list_status = $this->input->get('status');

        $BackedProjectEdit = $this->projectreturn_model->getProjectReturnActiveAll($projecId,$list_status);

        $dataInfo = $this->backing_model->getListBacking($projecId,$list_status);

        if((int)$data['project']->goal_amount > (int)$dataInfo->goal_amount){
            $this->session->set_flashdata('error', '更新の目標金額は現在のより高くしてください。');
            $this->session->set_flashdata('tab', '2');
            redirect('/project/edit/'.$projecId);
        }
        if($dataInfo->status === '2'){
            $this->session->set_flashdata('error', 'この申請が承認されました。');
            $this->session->set_flashdata('tab', '2');
            redirect('/project/edit/'.$projecId);
        }else if($dataInfo->status === '1'){
            $this->session->set_flashdata('error', 'この申請が却下されました。');
            $this->session->set_flashdata('tab', '2');
            redirect('/project/edit/'.$projecId);
        }

        $idEdit = $dataInfo->id;
        $goal_amount = $dataInfo->goal_amount;

        $dataValue = array();
        $dataValueShowOrder = array();
        if($data['project']->project_type === '1'){
            //include Stripe PHP library
            require_once APPPATH."third_party/stripe/init.php";

            //set api key
            $key = $this->setting_model->getSettings();
            $stripe = array(
                "secret_key"      => $key->secret_key,
                "publishable_key" => $key->public_key
            );
            \Stripe\Stripe::setApiKey($stripe['secret_key']);
            if(isset($BackedProjectEdit) && $BackedProjectEdit) {
                foreach ($BackedProjectEdit as $key => $value) {

                    $plan = \Stripe\Plan::create([
                      "nickname" => "Donate_".$value->invest_amount,
                      "product" => $data['project']->product_fanclub_id,
                      "amount" => $value->invest_amount,
                      "currency" => "JPY",
                      "interval" => "day",
                      "interval_count" => 30*$data['project']->number_month,
                      "usage_type" => "licensed",
                    ]);


                    $newvalue = array(
                        'name' => $value->name,
                        'project_id' => $value->project_id,
                        'max_count' => $value->max_count,
                        'thumnail' => $value->thumnail,
                        'now_count' => $value->now_count,
                        'invest_amount' => $value->invest_amount,
                        'return_amount' => $value->return_amount,
                        'created' => $value->created,
                        'schedule' => $value->schedule,
                        'plan_fanclub_id' => $plan->id,
                         'guid_id' => $myGUid,
                        'show_order' => $value->show_order
                    );

                    //nvtrong change order
                    $newvalueShowOrder = array(
                        'project_id' => $value->project_id,
                        'show_order' => $value->show_order,
                        'guid' => $myGUid,
                    );
                    array_push($dataValueShowOrder,$newvalueShowOrder);
                    array_push($dataValue,$newvalue);
                }
            }
        }else {
            foreach ($BackedProjectEdit as $key => $value) {
                $newvalue = array(
                    'name' => $value->name,
                    'project_id' => $value->project_id,
                    'max_count' => $value->max_count,
                    'thumnail' => $value->thumnail,
                    'now_count' => $value->now_count,
                    'invest_amount' => $value->invest_amount,
                    'return_amount' => $value->return_amount,
                    'created' => $value->created,
                    'schedule' => $value->schedule,
                    'plan_fanclub_id' => $plan->id

                );
                array_push($dataValue,$newvalue);
            }
        }
        $this->updateShowOrderAll($projecId);
        $this->db->insert_batch('backing_levels', $dataValue);
        //order show order nvtrong
        $this->showOrderReChange($dataValueShowOrder,$projecId);

        // create array edit goal amount project
        if($data['project_type'] === '0'){
            $dataProject = array(
                'goal_amount' => $goal_amount
            );
            $this->project_model->updateProject($data['project']->id,$dataProject);
        }
        // create array edit listBackingInfo (success)
        $dataEditBacking = array(
            'status' => '2'
        );
        $this->backing_model->updateBackingProjectActive($idEdit,$dataEditBacking);

        // create Datetime
        $datetime = new DateTime();
        $datetime = $datetime->format('Y-m-d H:i:s');
        // get strtotime + change time zone
        $asia_timestamp = strtotime($datetime);
        date_default_timezone_set('UTC');
        // get Date vs time zone
        $date = date("Y-m-d H:i:s", $asia_timestamp);

        $projectName = $data['project']->project_name;
        // array create notification info
        $dataNotification = array(
            'user_id' => $data['project']->user_id,
            'title' => '公開のプロジェクト・更新内容',
            'user_action' => 'Admin',
            'message' => '「'.$projectName.'」プロジェクトへの更新内容が承認されました。',
            'link'=> "my-page/edit-project-public/".$data['project']->id."/page=1",
            'status' => 0,
            'thumbnail' => image_url().'/images/2018/default/logo-icon.png',
            'created' => $date
        );
        // create notification info
        $createNotification = $this->notification_info->insert($dataNotification);
        // check exists
        if($createNotification) {

            $user = $this->users_model->getUserId($data['project']->user_id);

            $email = $user->email;
            $subject = "【".$projectName."】プロジェクトの更新の申請が承認されました。";
            $message = $user->username.'さん、<br>
            <br>
            KAKUSEIDA運営事務局です。<br>
            お世話になっております。<br>
            <br>
            公開申請をされておりました【'.$projectName.'】プロジェクトの更新の申請が承認されました。<br>
            ご確認ください。<br>
            <a href="'.app_url().'project-detail/'.$projecId.'">'.app_url().'project-detail/'.$projecId.'</a><br>
            よろしくお願いいたします。<br>';
            SendMail($email,$subject,$message);

            // use SocketIO annouce for user
            $socketio = new SocketIO();
            // array
            $dataSocket = array(
                'room' => 'project_'.$projecId,
                'title' => '公開のプロジェクト・更新内容',
                'message' => '「'.$projectName.'」プロジェクトへの更新内容が承認されました。',
                'url'=> app_url()."my-page/edit-project-public/".$projecId."/page=1",
                'ava' => image_url().'/images/2018/default/logo-icon.png'
            );

            $socketio->send($roomSocket, $portRoomSocket, 'public_project',json_encode($dataSocket));


            $curl = curl_init();

            curl_setopt_array($curl, array(
              CURLOPT_URL => "https://fcm.googleapis.com/fcm/send",
              CURLOPT_RETURNTRANSFER => true,
              CURLOPT_ENCODING => "",
              CURLOPT_MAXREDIRS => 10,
              CURLOPT_TIMEOUT => 30,
              CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
              CURLOPT_CUSTOMREQUEST => "POST",
              CURLOPT_POSTFIELDS => "{   \"notification\": {\r\n        \"title\": \"公開のプロジェクト・更新内容\",\r\n        \"body\": \"「".$projectName."」プロジェクトへの更新内容が承認されました。\",\r\n        \"click_action\": \"".app_url()."my-page/edit-project-public/".$projecId."/page=1\",\r\n        \"icon\": \"".image_url()."images/2018/default/logo-icon.png\"\r\n    },\r\n  \"to\" : \"/topics/project_".$projecId."\"\r\n}",
              CURLOPT_HTTPHEADER => array(
                "Authorization: key=".$key_firebase."",
                "Content-Type: application/json",
              ),
            ));
            $response = curl_exec($curl);
            $err = curl_error($curl);

            curl_close($curl);
            // Session send message for admin
        }

        $this->session->set_flashdata('msg', 'プロジェクトへの更新内容が承認されました。');
        $this->session->set_flashdata('tab', '2');
        redirect('/project/edit/'.$projecId);
    }

    /**
     * [Preview description]
     */
    public function preView(){
      $user_id = $this->input->post('user_id');
      if(isset($user_id) && $user_id){
        $user = $this->users_model->getUserId($user_id);
        if(isset($user) && $user){
          $myObj['username']          = $user->username;
          $myObj['job']               = $user->job;
          $myObj['address']           = $user->address;
          $myObj['profileImageURL']   = $user->profileImageURL;
          echo json_encode($myObj);
        }else{
          $myObj['username']          = '氏名';
          $myObj['job']               = '職業';
          $myObj['address']           = '現在地';
          $myObj['profileImageURL']   = ''.image_url().'images/2018/default/default-profile_01.png';
          echo json_encode($myObj);
        }
      }else{
        $myObj['username']            = '氏名';
        $myObj['job']                 = '職業';
        $myObj['address']             = '現在地';
        $myObj['profileImageURL']     = ''.image_url().'images/2018/default/default-profile_01.png';
        echo json_encode($myObj);
      }
    }

    public function refunds(){
      $this->load->model('admin/setting_model');
      $dataRefunds = [];
      $id = $this->input->post('pid');
      $project = $this->project_model->getProjectById($id);
      if($project->active === 'yes'){
        redirect('/project/edit/' . $id);
      }
      $dataProject = $this->backing_model->getBackingLevelId($id);
      foreach ($dataProject as $key => $value) {
        $refunds = $this->backed_model->getBackingAll($value->id);
        foreach ($refunds as $key => $data) {
          $newdata = array(
            'id' => $data->id,
            'stripe_charge_id'=>$data->stripe_charge_id
          );
          if($data->status === 'succeeded'){
            array_push($dataRefunds,$newdata);
          }
        }
      }
      require_once APPPATH."third_party/stripe/init.php";
      $key = $this->setting_model->getSettings();
      \Stripe\Stripe::setApiKey($key->secret_key);

      try {
        foreach ($dataRefunds as $key => $value) {
          $refund = \Stripe\Refund::create(array(
              "charge" => $value['stripe_charge_id']
          ));
          //
          $dataBacked = array(
            'status'=>'refunded'
          );
          $backed = $this->backed_model->updateBacked($value['id'],$dataBacked);
          $dataBacking = array(
              'now_count'=>'0'
          );
          $backing = $this->backing_model->getBackingLevelId($id);
          if(isset($backing) && $backing){
              foreach ($backing as $key => $value) {
                  $updatebacking = $this->backing_model->updateBacking($value->id,$dataBacking);
              }
          }
          $dataProject = array(
            'collected_amount' => 0
          );
          $project = $this->project_model->updateProject($id,$dataProject);
        }
        $this->session->set_flashdata('tab', '3');
        $this->session->set_flashdata('msg', '返金ができました。');
        redirect('/project/edit/'.$id);
      } catch(Stripe_CardError $e) {
        $error1 = $e->getMessage();
        $this->session->set_flashdata('error', $error1);
      } catch (Stripe_InvalidRequestError $e) {
        // Invalid parameters were supplied to Stripe's API
        $error2 = $e->getMessage();
        $this->session->set_flashdata('error', $error2);
      } catch (Stripe_AuthenticationError $e) {
        // Authentication with Stripe's API failed
        $error3 = $e->getMessage();
        $this->session->set_flashdata('error', $error3);
      } catch (Stripe_ApiConnectionError $e) {
        // Network communication with Stripe failed
        $error4 = $e->getMessage();
        $this->session->set_flashdata('error', $error4);
      } catch (Stripe_Error $e) {
        // Display a very generic error to the user, and maybe send
        // yourself an email
        $error5 = $e->getMessage();
        $this->session->set_flashdata('error', $error5);
      } catch (Exception $e) {
        // Something else happened, completely unrelated to Stripe
        $error6 = $e->getMessage();
        $this->session->set_flashdata('error', $error6);
      }
      redirect('/project/edit/'.$id);
    }

    public function acceptCloseFanClub($id){

        $key_firebase  = $this->config->item('key_firebase');
        $roomSocket  = $this->config->item('roomSocket');
        $portRoomSocket  = $this->config->item('portRoomSocket');

        $project = $this->project_model->getProjectById($id);

        if(isset($project) && $project->status_fanclub === 'wait') {
            $user = $this->users_model->getUserId($project->user_id);
            $projectName = $project->project_name;

            $fanclub = $this->listdonatefanclub_model->getAllDonateFanClub($id);

            $startDay = new DateTime($project->collection_start_date);
            $date = $startDay->format('Y-m-01');
            // current day
            $curentDay = new DateTime();
            $date2 = $curentDay->format('Y-m-01');

            $ts1 = strtotime($date);
            $ts2 = strtotime($date2);

            $year1 = date('Y', $ts1);
            $year2 = date('Y', $ts2);

            $month1 = date('m', $ts1);
            $month2 = date('m', $ts2);

            $diff = (($year2 - $year1) * 12) + ($month2 - $month1);

            // create month = ?month / month of project
            if($diff >= (int)$project->number_month){
                $newmonth = ($diff/$project->number_month);
                $checkMonth = ceil($diff/$project->number_month);
                if($newmonth === (int)$checkMonth){
                    $newmonth = $checkMonth;
                }else {
                    $newmonth = $checkMonth - 1;
                }
            }else {
                $newmonth = 0;
            }

            // use explode format firstday
            $parts = explode('-', $date);
            $dayCanClose = date(
                'Y-m-t 23:59:59',
                mktime(0, 0, 0, ($parts[1] + ($newmonth*($project->number_month)) + ($project->number_month - 1)), $parts[2] + 1, $parts[0])
            );

            $curentDay = $curentDay->format('Y-m-d');
            $parts2 = explode('-', $curentDay);
            $dateCurrent = date(
                'Y-m-d 23:59:59',
                mktime(0, 0, 0, ($parts2[1] + 1), $parts2[2], $parts2[0])
            );

            if($dateCurrent >= $dayCanClose){
                $dayClose = date(
                    'Y-m-t 23:59:59',
                    mktime(0, 0, 0, ($parts[1] + ($newmonth*($project->number_month))  + ($project->number_month)), $parts[2] + 1, $parts[0])
                );
            }else{
                $dayClose = $dayCanClose;
            }


            $data = array(
                'status_fanclub' => 'stop',
                'date_fanclub_close' => $dayClose
            );
            $updateProject = $this->project_model->updateProject($id,$data);

            if($updateProject) {
                // create Datetime
                $datetime = new DateTime();
                $datetime = $datetime->format('Y-m-d H:i:s');
                // get strtotime + change time zone
                $asia_timestamp = strtotime($datetime);
                date_default_timezone_set('UTC');
                // get Date vs time zone
                $date = date("Y-m-d H:i:s", $asia_timestamp);

                // array create notification info
                $dataNotification = array(
                    'user_id' => $project->user_id,
                    'title' => '「'.$projectName.'」プロジェクトの解散が承認されました。',
                    'user_action' => 'Admin',
                    'message' => '「'.$projectName.'」プロジェクトの解散が承認されました。',
                    'link'=> "my-page/statistic/".$id,
                    'status' => 0,
                    'thumbnail' => image_url().'/images/2018/default/logo-icon.png',
                    'created' => $date
                );
                // create notification info
                $createNotification = $this->notification_info->insert($dataNotification);
                // check exists
                if($createNotification) {

                    // use SocketIO annouce for user
                    $socketio = new SocketIO();
                    // array
                    $dataSocket = array(
                        'room' => 'project_'.$id,
                        'title' => '「'.$projectName.'」プロジェクトの解散が承認されました。',
                        'message' => '「'.$projectName.'」プロジェクトの解散が承認されました。',
                        'url'=> app_url()."my-page/statistic/".$id,
                        'ava' => image_url().'/images/2018/default/logo-icon.png'
                    );

                    $socketio->send($roomSocket, $portRoomSocket, 'public_project',json_encode($dataSocket));


                    $curl = curl_init();

                    curl_setopt_array($curl, array(
                      CURLOPT_URL => "https://fcm.googleapis.com/fcm/send",
                      CURLOPT_RETURNTRANSFER => true,
                      CURLOPT_ENCODING => "",
                      CURLOPT_MAXREDIRS => 10,
                      CURLOPT_TIMEOUT => 30,
                      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                      CURLOPT_CUSTOMREQUEST => "POST",
                      CURLOPT_POSTFIELDS => "{   \"notification\": {\r\n        \"title\": \"「".$projectName."」プロジェクトの解散が承認されました。\",\r\n        \"body\": \"「".$projectName."」プロジェクトの解散が承認されました。\",\r\n        \"click_action\": \"".app_url()."my-page/statistic/".$id."\",\r\n        \"icon\": \"".image_url()."images/2018/default/logo-icon.png\"\r\n    },\r\n  \"to\" : \"/topics/project_".$id."\"\r\n}",
                      CURLOPT_HTTPHEADER => array(
                        "Authorization: key=".$key_firebase."",
                        "Content-Type: application/json",
                      ),
                    ));
                    $response = curl_exec($curl);
                    $err = curl_error($curl);

                    curl_close($curl);
                }

            }

            $email = $user->email;
            $subject = "「".$projectName."」プロジェクトの解散が承認されました。";
            $message = $user->username.'様<br>
            KAKUSEIDA運営事務局です。<br>
            <br>
            「'.$projectName.'」プロジェクトの解散が承認されました。<br>
            「'.$projectName.'」プロジェクトは本日から一ヵ月以上経過後に正式に解散されます。<br>
            支援者の方にも「'.$projectName.'」プロジェクトの解散については通知されました。<br>
            <br>
            '.$user->username.'様からは、今回の「'.$projectName.'」プロジェクトの解散にいたる経緯、詳細、解散日時について必ず支援者様に周知してください。<br>
            支援者様への通知は、プロジェクト管理画面から行うことができます。<br>
            操作方法などが分からない場合は、KAKUSEIDA運営事務局までお問い合わせください。<br>
            よろしくお願い致します。<br>
            <br>
            KAKUSEIDA運営事務局';
            SendMail($email,$subject,$message);

            $this->session->set_flashdata('msg', 'success');
            redirect('/project/');
        }else {
            $this->session->set_flashdata('error', 'This Project Not Exists');
            redirect('/project/');
        }
    }

    public function refuseFanClub($projecId){
        $key_firebase  = $this->config->item('key_firebase');
        $roomSocket  = $this->config->item('roomSocket');
        $portRoomSocket  = $this->config->item('portRoomSocket');

        $message = $this->input->post('message');
        $project = $this->project_model->getProjectById($projecId);

        $data = array(
            'status_fanclub'=>'progress',
        );


        if($project->project_type !== '1' && $project->status_fanclub !== 'wait'){
            $this->session->set_flashdata('error', 'This FanClub It not stop');
            redirect('/project');
        }else{
            $updateInfo = $this->project_model->updateProject($project->id,$data);

            // create Datetime
            $datetime = new DateTime();
            $datetime = $datetime->format('Y-m-d H:i:s');
            // get strtotime + change time zone
            $asia_timestamp = strtotime($datetime);
            date_default_timezone_set('UTC');
            // get Date vs time zone
            $date = date("Y-m-d H:i:s", $asia_timestamp);

            $projectName = $project->project_name;
            // array create notification info
            $dataNotification = array(
                'user_id' => $project->user_id,
                'title' => '「'.$projectName.'」プロジェクトの解散申請がKAKUSEIDA運営事務局により却下されました。',
                'user_action' => 'Admin',
                'message' => '「'.$projectName.'」プロジェクトの解散申請がKAKUSEIDA運営事務局により却下されました。',
                'link'=> "notification/page=1",
                'status' => 0,
                'thumbnail' => image_url().'/images/2018/default/logo-icon.png',
                'created' => $date
            );
            // create notification info
            $createNotification = $this->notification_info->insert($dataNotification);
            // check exists
            if($createNotification) {

                $user = $this->users_model->getUserId($project->user_id);
                $email = $user->email;

                $subject = "「".$project->project_name."」プロジェクト解散申請が却下されました";
                $message = $user->username.'様、<br>
                【'.$project->project_name.'】プロジェクトの解散申請がKAKUSEIDA運営事務局により却下されました。<br>
                以下の却下理由をお読みになり、質問や意見等がある場合はKAKUSEIDA運営事務局にご連絡ください。<br>
                ------------------------------------------------<br>
                <pre>'.$message.'</pre>';

                // use SocketIO annouce for user
                $socketio = new SocketIO();
                // array
                $dataSocket = array(
                    'room' => 'project_'.$projecId,
                    'title' => '「'.$projectName.'」プロジェクトの解散申請がKAKUSEIDA運営事務局により却下されました。',
                    'message' => '「'.$projectName.'」プロジェクトの解散申請がKAKUSEIDA運営事務局により却下されました。',
                    'url'=> app_url()."notification/page=1",
                    'ava' => image_url().'/images/2018/default/logo-icon.png'
                );

                $socketio->send($roomSocket, $portRoomSocket, 'public_project',json_encode($dataSocket));


                $curl = curl_init();

                curl_setopt_array($curl, array(
                  CURLOPT_URL => "https://fcm.googleapis.com/fcm/send",
                  CURLOPT_RETURNTRANSFER => true,
                  CURLOPT_ENCODING => "",
                  CURLOPT_MAXREDIRS => 10,
                  CURLOPT_TIMEOUT => 30,
                  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                  CURLOPT_CUSTOMREQUEST => "POST",
                  CURLOPT_POSTFIELDS => "{   \"notification\": {\r\n        \"title\": \"「".$projectName."」プロジェクトの解散申請がKAKUSEIDA運営事務局により却下されました。\",\r\n        \"body\": \"「".$projectName."」プロジェクトの解散申請がKAKUSEIDA運営事務局により却下されました。\",\r\n        \"click_action\": \"".app_url()."notification/page=1\",\r\n        \"icon\": \"".image_url()."images/2018/default/logo-icon.png\"\r\n    },\r\n  \"to\" : \"/topics/project_".$projecId."\"\r\n}",
                  CURLOPT_HTTPHEADER => array(
                    "Authorization: key=".$key_firebase."",
                    "Content-Type: application/json",
                  ),
                ));
                $response = curl_exec($curl);
                $err = curl_error($curl);

                curl_close($curl);

                SendMail($email,$subject,$message);
                $this->session->set_flashdata('msg', '却下メール送信ができました。');
                redirect('/project');
            }else{
                $this->session->set_flashdata('error', 'Error');
                redirect('/project');
            }

        }
    }

    public function SendMoneyOwnerProject($projecId){
        $data['project'] = $this->project_model->getProjectById($projecId);
        if(isset($data['project']) && $data['project']) {
            $user = $this->users_model->getUserId($data['project']->user_id);
            $email = $user->email;
            $subject = '支援金振込のご連絡';
            $message =
            $user->username.'様<br>
            <br>
            お世話になっております。<br>
            KAKUSEIDA運営事務局です。<br>
            <br>
            KAKUSEIDAにて「'.$data['project']->project_name.'」プロジェクトを企画していただきありがとうございました。<br>
            '.$user->username.'様が企画されましたプロジェクトが無事終了致しました。<br>
            お疲れ様でございました。<br>
            <br>
            集まりました支援金全額を登録されました口座に振り込みましたことを連絡させていただきます。<br>
            支援総額から振込に関わる手数料を差し引いた額が振り込まれていることをご確認ください。<br>
            質問等ありましたら遠慮なくお問い合わせください。<br>
            <br>
            今後とも、KAKUSEIDAを宜しくお願い申し上げます。<br>
            <br>
            メールにて恐縮ではございますが、<br>
            取り急ぎお礼を申し上げます<br>';
            SendMail($email,$subject,$message);
            $this->session->set_flashdata('msg', '却下メール送信ができました。');
            redirect('/project');
        }else {
            $this->session->set_flashdata('error', 'Error');
            redirect('/project');
        }
    }

}
