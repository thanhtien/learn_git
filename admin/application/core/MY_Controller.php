<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller {

    public $active_menu = '';
    public $menu = 'dashboard';
    public $page_title = 'Kakuseida';
    public $page_description = '';
    public $category_lineage;
    public $current_category;
    public $brand_lineage;
    public $current_brand;
    public $products;
    public $canonical_url = NULL;
    public $page_seo;
    public $theme = 'default';
    public $theme_path = '';

    // protected $data = [];
    public function __construct() {
        parent::__construct();
        $this->_homepage = FALSE;
        $this->theme_path = 'frontend/themes/' . $this->theme . '/';
        $this->load->library('carabiner');
        $carabiner_config = array(
            'script_dir' => 'assets/',
            'style_dir' => 'assets/',
            'cache_dir' => 'assets/cache/',
            'base_uri' => base_url(),
            'combine' => FALSE,
            'dev' => TRUE,
        );
        $this->page_seo = new stdClass;

        $this->carabiner->config($carabiner_config);
        // $_SESSION['userdata'] = $this->session->userdata();
    }

    public function _is_admin() {
      if (!$this->session->userdata['is_admin']) {
          redirect(site_url('login'));
      }
    }

    public function _renderAdminLayout($view, $data = null) {
        //$data['page_title'] = $this->page_title;
        $this->load->vars($data);
        $this->load->view('admin/_part/header');
        $this->load->view('admin/_part/sidebar', ['menu' => $this->menu]);
        $this->load->view($view);
        $this->load->view('admin/_part/footer');
    }
    public function _renderBakingAjax($view, $data = null) {
        //$data['page_title'] = $this->page_title;
        $this->load->vars($data);//ajax_backing.php
        $this->load->view($view);

    }
}
