<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Users_model extends CI_Model
{

    private $table = TB_USERS;

    function __construct()
    {
        parent::__construct();
        $this->load->database();
        //$this->user_id = $this->session->userdata['user_info']['id'];
    }
    function showAllUser(){
      $query = $this->db->get('users');
      return $query->result();
    }
    public function getUserId($id = 0) {
        if ((int) $id > 0) {
            $query = $this->db->get_where($this->table, array('id' => $id));
            return $query->row();
        } else {
            return NULL;
        }
    }

    public function getAddressUserByUserId($id){
        if ((int) $id > 0) {
            $query = $this->db->get_where('address_user', array('user_id' => $id,'chosen_default' => '1'));
            return $query->row();
        } else {
            return NULL;
        }
    }
    public function getUsers($sorting, $by, $page = 0) {
        $this->db->select('id');
        $this->db->from($this->table);
        $this->db->join('users_groups', 'users_groups.user_id = users.id');
        $this->db->join('groups', 'users_groups.group_id = groups.id');
        $this->db->join('subscription','subscription.email = users.email');
        $total = $this->db->count_all_results();
        $limit = $this->config->item('admin_per_page');
        $start = ($page <= 1) ?  0 : ($page - 1)  * $limit;

         // $this->db->select('DISTINCT users.* users_groups.group_id, ',false);
         $select =   array(
                'users.*',
                'users_groups.user_id',
                'users_groups.group_id',
                'groups.name',
                'subscription.created'
        );
        $this->db->select($select);
        $this->db->limit($limit, $start);
        $this->db->from($this->table);
        $this->db->join('users_groups', 'users_groups.user_id = users.id');
        $this->db->join('groups', 'users_groups.group_id = groups.id');
        $this->db->join('subscription','subscription.email = users.email');
        $this->db->order_by($sorting,$by);
        $query = $this->db->get();
        $users = $query->result();
        return ["total" => $total, "users" => $users];
    }

    function searchUsers($sorting, $by, $search, $page = 0){
      $select =   array(
        'users.*',
        'users_groups.user_id',
        'users_groups.group_id',
        'groups.name'
      );
      $this->db->select($select);
      $this->db->from($this->table);
      $this->db->join('users_groups', 'users_groups.user_id = users.id');
      $this->db->join('groups', 'users_groups.group_id = groups.id');
      $this->db->like('username', $search);
      $this->db->or_like('email', $search);
      $total = $this->db->count_all_results();

      $limit = $this->config->item('admin_per_page');
      $start = ($page <= 1) ?  0 : ($page - 1)  * $limit;
      $select =   array(
             'users.*',
             'users_groups.user_id',
             'users_groups.group_id',
             'groups.name'
     );
      $this->db->select($select);
      $this->db->from($this->table);
      $this->db->limit($limit, $start);
      $this->db->join('users_groups', 'users_groups.user_id = users.id');
      $this->db->join('groups', 'users_groups.group_id = groups.id');
      $this->db->like('username', $search);
      $this->db->or_like('email', $search);
      // $this->db->group_by('id');
      // $array = array('name' => 'admin', 'name' => 'members');

      // $this->db->where($array);
      $this->db->order_by($sorting,$by);
      $data = $this->db->get();

      $users = $data->result();
      return ["total" => $total, "users" => $users];
    }

    // Delete New with id
    public function delete($id) {
        $this->db->delete($this->table, array('id' => $id));
    }

    public function updateUser($id,$data){
        if(isset($id) && isset($data)){
            $this->db->where('id',$id);
            $data = $this->db->update($this->table,$data);
            if($data){
                return true;
            }else{
                return false;
            }
        }else{
            return false;
        }
    }
}
