<?php if(!defined('BASEPATH')) exit('No direct script allowed');

class Backing_model extends CI_Model{
	var $table = 'backing_levels';
	function getBackingLevel(){
		$this->db->from($this->table);
		$a = $this->db->get();
		return $a->result();
	}
  	function getBackingLevelId($id){
		$this->db->from($this->table);
		$this->db->where('project_id',$id);
		$a = $this->db->get();
		return $a->result();
	}
    function getBackingFromProjectIdAndShowOrder($id,$showOrder){
        $this->db->from($this->table);
        $this->db->where('project_id',$id)->where('show_order',$showOrder);
        $a = $this->db->get();
        return $a->row();
    }
	function getBacking($id){
		$this->db->from($this->table);
		$this->db->where('id',$id);
		$a = $this->db->get();
		return $a->row();
	}
	public function abc($id = null) {
		$this->db->select('project_id');
		$this->db->select_sum('now_count');
		$this->db->where('project_id', $id);
		$result = $this->db->get($this->table)->row();
		return $result;
	}

	public function updateBacking($id,$data){
		if(isset($id) && isset($data)){
			$this->db->where('id',$id);
			$data = $this->db->update($this->table,$data);
			if($data){
				return true;
			}else{
				return false;
			}
		}else{
			return false;
		}
	}


	public function getBackedProjectActive($id){
		if(isset($id) && $id){
			$this->db->from('backing_levels_edit');
			$this->db->where('project_id',$id);
			$a = $this->db->get();
			return $a->result();
		}else{
			return false;
		}
	}

	public function getBackedProjectActive2($id){
		if(isset($id) && $id){
			$this->db->select('project_id,max_count,thumnail,now_count,invest_amount,return_amount,created,modified,schedule');
			$this->db->from('backing_levels_edit');
			$this->db->where("id in ($id)");
			$a = $this->db->get();
			return $a->result();
		}else{
			return false;
		}
	}

	function getBackingProjectActiveId($id){
		$this->db->from('backing_levels_edit');
		$this->db->where('id',$id);
		$a = $this->db->get();
		return $a->row();
	}


	public function updateBackingProjectActive($id,$data){
		if(isset($id) && isset($data)){
			$this->db->where("id",$id);
			$data = $this->db->update('info_backing_edit',$data);
			if($data){
				return true;
			}else{
				return false;
			}
		}else{
			return false;
		}
	}

	public function checkBackingByProjectId($project_id){
		if(isset($project_id) && $project_id){
			$this->db->select_max('list_status');
			$this->db->from('backing_levels_edit');
			$this->db->where('project_id',$project_id);
			$a = $this->db->get();
			return $a->row();
		}
	}

	public function getBackingByProjectId($project_id){
        if(isset($project_id) && $project_id){
            $this->db->from('info_backing_edit');
            $this->db->where('project_id',$project_id);
			$this->db->order_by("created", "desc");
            $a = $this->db->get();
            return $a->result_array();
        }
    }

	public function getListBacking($project_id,$list_status){
        if(isset($project_id) && $project_id){
            $this->db->from('info_backing_edit');
            $this->db->where('project_id',$project_id);
            $this->db->where('list_status',$list_status);
            $a = $this->db->get();
            return $a->row();
        }
    }
}
