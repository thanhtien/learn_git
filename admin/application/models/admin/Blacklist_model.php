<?php if(!defined('BASEPATH')) exit('No direct script allowed');

class Blacklist_model extends CI_Model{
	var $table = 'black_list';

	public function createBlackList($data){
		if(isset($data) && $data){
			$this->db->insert($this->table, $data);
			return $this->db->insert_id();
		}
	}


	public function editBlackList($email,$data){
		if(isset($email) && isset($data)){
			$this->db->where("email",$email);
			$data = $this->db->update($this->table,$data);
			if($data){
				return true;
			}else{
				return false;
			}
		}else{
			return false;
		}
	}

	public function checkBackList($email){
		$this->db->where("email",$email);
        $this->db->from($this->table);
		$query = $this->db->get();
		return $query->row();

	}
}
