<?php if(!defined('BASEPATH')) exit('No direct script allowed');

class Backed_model extends CI_Model{
	var $table = 'backed_projects';

  public function getBackedProjectId($id,$page = 0) {
      //total data
      $this->db->select('id');
      $this->db->from($this->table);
      $this->db->order_by('id','DESC');
      $this->db->where('project_id', $id);
      $total = $this->db->count_all_results();

      $limit = $this->config->item('admin_per_page');
      $start = ($page <= 1) ?  0 : ($page - 1)  * $limit;
      //Use Limit of CI
      $this->db->limit($limit, $start);
      //OrderBy
      $this->db->where('project_id', $id);
      $this->db->order_by('id','DESC');

      $this->db->from($this->table .' c1');
      $query = $this->db->get();
      $backed = $query->result();
      return ["total" => $total, "backed" => $backed];

  }


	public function getBackedAll($id) {
			//OrderBy
			$this->db->where('project_id', $id);
			$this->db->from($this->table );
			$query = $this->db->get();
			$backed = $query->result();
			return $backed;

	}

	public function getBackingAll($id) {
			//OrderBy
			$this->db->where('backing_level_id', $id);
			$this->db->from($this->table );
			$query = $this->db->get();
			$backed = $query->result();
			return $backed;

	}

	public function getBackedId($id) {
			//OrderBy
			$this->db->where('id', $id);
			$this->db->from($this->table );
			$query = $this->db->get();
			$backed = $query->row();
			return $backed;

	}

	public function updateBacked($id,$data){
		if(isset($id) && $id){
			$this->db->where('id',$id);
			$para = $this->db->update($this->table, $data);
			if($para){
				return true;
			}else{
				return false;
			}

		}else{

			return false;

		}
	}
}
