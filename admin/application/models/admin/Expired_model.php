<?php if(!defined('BASEPATH')) exit('No direct script allowed');

class Expired_model extends CI_Model{
	var $table = 'expired_projects';

    public function getAllExpired(){
        $this->db->from($this->table);
        $query = $this->db->get();
        $query = $query->row();
        return $query;
    }

    public function delete($id) {
        $this->db->delete($this->table, array('id' => $id));
    }

    public function checkProject($id) {
        $query = $this->db->get_where($this->table, array('project_id' => $id))->row();
        if(isset($query) && $query){
            return $query;
        }else{
            return false;
        }
    }

}
