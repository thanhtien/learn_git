<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Subscription_model extends CI_Model
{

    private $table = 'subscription';

    function __construct()
    {
        parent::__construct();
        $this->load->database();
        //$this->user_id = $this->session->userdata['user_info']['id'];
    }
    function getAllEmailSubscription(){
        $this->db->select('*');
        $query = $this->db->get($this->table);
        return $query->result();
    }
    public function getSubId($email) {
        if (isset($email) && $email) {
            $query = $this->db->get_where($this->table, array('email' => $email));
            return $query->row();
        } else {
            return false;
        }
    }

    function update($id, $data) {
        if (!$id)
        {
            return FALSE;
        }
        $this->db->where('id',$id);
        $query = $this->db->update($this->table, $data);
        if($query){
            return TRUE;
        }else{
            return FALSE;
        }

    }
    function create($data){
        if(isset($data) && $data){
            $query = $this->db->insert($this->table, $data);
            if($query){
                return TRUE;
            }else{
                return FALSE;
            }
        }
    }

}
