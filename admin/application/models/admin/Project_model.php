<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Projects_model
 *
 * @author TNM Group
 */
class Project_model extends CI_Model {
    private $table = TB_PROJECTS;

    public function __construct() {
        parent::__construct();
    }
    // Get All Project
    public function getAllProject($order_by = 'id', $order_direction = 'ASC') {
        $this->db->order_by($order_by,$order_direction);
        $query = $this->db->get($this->table);
        return $query->result();
    }

    public function getAllProjectActive($order_by = 'id', $order_direction = 'ASC') {
        $date = new DateTime();
        $date = $date->format('Y-m-d H:i:s');
        $this->db->order_by($order_by,$order_direction);
        $this->db->where('active','yes');
        $this->db->where('project_type','0');
        $this->db->where('collection_end_date >', $date);
        $query = $this->db->get($this->table);
        return $query->result();
    }

    public function getProjectExpired($order_by = 'id', $order_direction = 'ASC'){
        $date = new DateTime();
        $date = $date->format('Y-m-d H:i:s');
        $this->db->order_by($order_by,$order_direction);
        $this->db->where('active','yes');
        $this->db->where('project_type','0');
        $this->db->where('(collected_amount/goal_amount)*100 >=','100');
        $this->db->where('collection_end_date <=', $date);
        $query = $this->db->get($this->table);
        return $query->result();
    }

    public function getFanClubs($order_by = 'id', $order_direction = 'ASC'){
        $date = new DateTime();
        $date = $date->format('Y-m-d H:i:s');
        $this->db->order_by($order_by,$order_direction);
        $this->db->where('opened','yes');
        $this->db->where('active','yes');
        $this->db->where('project_type',1);
        // $this->db->where('collection_end_date <=', $date);
        $query = $this->db->get($this->table);
        return $query->result();
    }


    // Get Project has pagination
    public function getProject($sorting,$by,$page = 0) {
        //total data
        $this->db->select('id');
        $this->db->from($this->table);
        $this->db->where('opened', 'yes');
        $total = $this->db->count_all_results();

        $limit = $this->config->item('admin_per_page');
        $start = ($page <= 1) ?  0 : ($page - 1)  * $limit;
        //Use Limit of CI
        $this->db->select($this->table.'.*, users.username');
        $this->db->where('opened', 'yes');
        $this->db->limit($limit, $start);
        $this->db->join('users','users.id = projects.user_id');
        $this->db->order_by($sorting,$by);
        $this->db->from($this->table);
        $query = $this->db->get();
        $project = $query->result();
        return ["total" => $total, "project" => $project];
    }
    public function getProjectNearExpres($page = 0) {
        $date = new DateTime();
        //fomat date
        $date = $date->format('Y-m-d H:i:s');
        //total data
        $this->db->select('id');
        $this->db->from($this->table);
        $this->db->join('users','users.id = projects.user_id');
        $this->db->where('opened', 'yes');
        $this->db->where('pay_owner', 0);
        $this->db->where('collected_amount >', 0);
        $this->db->where('collection_end_date <=', $date);
        $this->db->order_by('collection_end_date','ASC');
        $this->db->where('projects.active','yes');
        $total = $this->db->count_all_results();

        $limit = $this->config->item('admin_per_page');
        $start = ($page <= 1) ?  0 : ($page - 1)  * $limit;

        //Use Limit of CI
        $this->db->select($this->table.'.*, users.username');
        $this->db->limit($limit, $start);
        $this->db->join('users','users.id = projects.user_id');
        $this->db->where('pay_owner', 0);
        $this->db->where('collected_amount >', 0);
        $this->db->where('collection_end_date <=', $date);
        $this->db->order_by('collection_end_date','ASC');
        $this->db->where('projects.active','yes');
        // $this->db->where('opened','yes');

        $this->db->from($this->table);
        $query = $this->db->get();
        $project = $query->result();
        return ["total" => $total, "project" => $project];
    }
    function searchProject($sorting,$by,$search,$page = 0){
      $this->db->select($this->table.'.*, users.username');
      $this->db->from($this->table);
      $this->db->join('users','users.id = projects.user_id');
      $this->db->like('project_name',$search);
      $this->db->or_like('username',$search);
      $this->db->where('opened', 'yes');
      $total = $this->db->count_all_results();

      $limit = $this->config->item('admin_per_page');
      $start = ($page <= 1) ?  0 : ($page - 1)  * $limit;
      $this->db->select($this->table.'.*, users.username');
      $this->db->from($this->table);
      $this->db->where('opened', 'yes');
      $this->db->join('users','users.id = projects.user_id');
      $this->db->limit($limit, $start);
      $this->db->like('project_name',$search);
      $this->db->or_like('username',$search);
      $this->db->order_by($sorting,$by);
      $data = $this->db->get();
      $project = $data->result();

      return ["total" => $total, "project" => $project];
    }
    //Get Project with Id
    public function getProjectById($id = 0) {
        //Check Id is exists
        if ((int) $id > 0) {
            //Use get where Check Id in database
            $query = $this->db->get_where($this->table, array('id' => $id));
            //Return Data
            return $query->row();
        } else {
            // Return Null
            return NULL;
        }
    }

    public function getProjectByCategoryId($id = 0) {
        //Check Id is exists
        if ((int) $id > 0) {
            //Use get where Check Id in database
            $query = $this->db->get_where($this->table, array('category_id' => $id));
            //Return Data
            return $query->result();
        } else {
            // Return Null
            return NULL;
        }
    }

    public function getProjectByCategoryIds($id) {
        //Check Id is exists
        if ($id) {
            //Use get where Check Id in database
            $this->db->from($this->table);
            $this->db->where("category_id in ($id)");
            //Return Data
            $query = $this->db->get();
            return $query->result();
        } else {
            // Return Null
            return NULL;
        }
    }

    public function getProjectByUserId($id = 0) {
        //Check Id is exists
        if ((int) $id > 0) {
            //Use get where Check Id in database
            $query = $this->db->get_where($this->table, array('user_id' => $id));
            //Return Data
            return $query->result();
        } else {
            // Return Null
            return NULL;
        }
    }
    //Insert
    public function insert() {
        //Get Date current
        $date = new DateTime();
        //fomat date
        $date = $date->format('Y-m-d H:i:s');
        // listed data with $this->input->post
        $project_name = $this->input->post('project_name');
        $project_type = $this->input->post('project_type');
        $category = $this->input->post('category');
        $summary = $this->input->post('summary');
        $radio = $this->input->post('Radios');
        $user_id = $this->input->post('itemName');
        // If radio == 1 is Video
        // Add $video
        $video = $this->input->post('video');
        if($video = ''){
          $video = null;
        }
        $thumbnail1 = $this->input->post('hiden_image1');
        $thumbnail2 = $this->input->post('hiden_image2');
        $thumbnail3 = $this->input->post('hiden_image3');
        if($thumbnail1 == ''){
          $thumbnail1 = ''.image_url().'images/2018/default/noimage-01.png';
        }
        if($thumbnail2 == ''){
          $thumbnail2 = ''.image_url().'images/2018/default/noimage-01.png';
        }
        if($thumbnail3 == ''){
          $thumbnail3 = ''.image_url().'images/2018/default/noimage-01.png';
        }

        if($project_type === '0') {
            $status_all_in =  $this->input->post('status_all_in');
            $dateclose = $this->input->post('date');
            $dateclose = date("Y-m-d 23:59:59", strtotime($dateclose));

            $goal_amount = $this->input->post('goal_amount');
            $goal_amount = preg_replace('/,/', '', $goal_amount);
            $number_month = null;
            $product_fanclub_id = null;

        }else{
            $status_all_in = '1';
            $settingKey =$this->input->post('settingKey');
            //include Stripe PHP library
            require_once APPPATH."third_party/stripe/init.php";

            \Stripe\Stripe::setApiKey($settingKey);

            $product_fanclub = \Stripe\Product::create([
                "name" => $project_name,
                "type" => "service",
            ]);
            $product_fanclub_id = $product_fanclub->id;

            $number_month = $this->input->post('number_month');
            $goal_amount = null;
            $dateclose   = null;
        }


        $project_content = $this->input->post('description');
        $thumbnail = $this->input->post('hiden_image');
        if($thumbnail == ''){
          $thumbnail = ''.image_url().'images/2018/default/noimage-01.png';
        }
        $favourite_projects = $this->input->post('favorite');
        $recommends_projects = $this->input->post('recommends');
        $pickup_projects = $this->input->post('pickup');
        if($favourite_projects == null){
          $favourite_projects = 0;
        }
        if($recommends_projects == null){
          $recommends_projects = 0;
        }
        if($pickup_projects == null){
          $pickup_projects = 0;
        }
        $public = $this->input->post('public');
        if($public === '1'){
          $active = 'yes';
        }else{
          $active = 'no';
        }

        // Add Data listed on $data
        $data = array(
            'project_name' => $project_name,
            'category_id' => $category,
            'description' => $summary,
            'goal_amount' => $goal_amount,
            'thumbnail_descrip1'=> $thumbnail1,
            'thumbnail_descrip2'=> $thumbnail2,
            'thumbnail_descrip3'=> $thumbnail3,
            'thumbnail_type'=>$radio,
            'thumbnail_movie_code'=>$video,
            'collection_start_date'=>$date,
            'collection_end_date'=>$dateclose,
            'favourite_projects'=>$favourite_projects,
            'recommends_projects'=>$recommends_projects,
            'pickup_projects'=>$pickup_projects,
            'user_id' => $user_id,
            'active' => $active,
            'status_all_in' => $status_all_in,
            'opened' => 'yes',
            'created' => $date,
            'modified'=> $date,
            'thumbnail'=>$thumbnail,
            'project_content'=>$project_content,
            'project_type' =>$project_type,
            'number_month' =>$number_month,
            'product_fanclub_id' =>$product_fanclub_id,
        );


        $a = $this->db->insert($this->table, $data);
        $id = $this->db->insert_id();
        if($this->input->post('favorite') == 1){
          $favorite = array(
            'project_id'=>$id,
            'created'=>$date
          );
          $this->db->insert('favourite_projects', $favorite);
        }
        if($this->input->post('recommends') == 1){
          $recommends = array(
            'project_id'=>$id,
            'created'=>$date
          );
          $this->db->insert('recommends_projects', $recommends);
        }
        if($this->input->post('pickup') == 1){
          $pickup = array(
            'project_id'=>$id,
            'created'=>$date
          );
          $this->db->insert('pickup_projects', $pickup);
        }
        return $id;
    }
    // Update project
    public function update($id) {
        //Get Date current
        $date = new DateTime();
        //fomat date
        $date = $date->format('Y-m-d H:i:s');
        // listed data with $this->input->post
        $project_name = $this->input->post('project_name');
        $category = $this->input->post('category');
        $summary = $this->input->post('summary');
        $radio = $this->input->post('Radios');
        $user_id = $this->input->post('itemName');
        // If radio == 1 is Video
        $video = $this->input->post('video');
        $thumbnail1 = $this->input->post('hiden_image1');
        $thumbnail2 = $this->input->post('hiden_image2');
        $thumbnail3 = $this->input->post('hiden_image3');

        $project_content = $this->input->post('description');
        $thumbnail = $this->input->post('hiden_image');
        $favourite_projects = $this->input->post('favorite');
        $recommends_projects = $this->input->post('recommends');
        $pickup_projects = $this->input->post('pickup');
        $project_type = $this->input->post('project_type');

        if($project_type === '0') {
            $status_all_in = $this->input->post('status_all_in');
            $dateclose = $this->input->post('date');

            $dateclose = date("Y-m-d 23:59:59", strtotime($dateclose));

            $goal_amount = $this->input->post('goal_amount');
            $goal_amount = preg_replace('/,/', '', $goal_amount);
            $number_month = null;
        }else{
            $status_all_in = '1';
            $number_month = $this->input->post('number_month');
            $goal_amount = null;
            $dateclose   = null;
        }

        $public = $this->input->post('public');
        $status_fanclub = $this->input->post('status_fanclub');
        if($this->input->post('save') != '1'){
            if($public == '1'){
                $active = 'yes';
            }else {
                // $yesterday  = mktime(0, 0, 0, date("m"),   date("d")-1,   date("Y"));
                // $yesterday = date("Y-m-d", $yesterday);
                // $dateclose = $yesterday;
                $active = 'cls';
                $status_fanclub = 'stop';
            }
        }else{
            $active = $this->input->post('active');
        }

        if($favourite_projects == null){
            $favourite_projects = 0;
        }
        if($recommends_projects == null){
            $recommends_projects = 0;
        }
        if($pickup_projects == null){
            $pickup_projects = 0;
        }


        // Add Data listed on $data
        $data = array(
            'project_name' => $project_name,
            'category_id' => $category,
            'description' => $summary,
            'goal_amount' => $goal_amount,
            'thumbnail_descrip1'=> $thumbnail1,
            'thumbnail_descrip2'=> $thumbnail2,
            'thumbnail_descrip3'=> $thumbnail3,
            'thumbnail_type'=>$radio,
            'thumbnail_movie_code'=>$video,
            'collection_start_date'=>$date,
            'collection_end_date'=>$dateclose,
            'user_id' => $user_id,
            'active' => $active,
            'status_all_in' => $status_all_in,
            'created' => $date,
            'modified' => $date,
            'thumbnail'=>$thumbnail,
            'project_content'=>$project_content,
            'project_type' =>$project_type,
            'number_month' =>$number_month,
            'status_fanclub' => $status_fanclub
        );

        //Update project with id and return this
        $this->db->where('id',$id);
        $data = $this->db->update($this->table,$data);
        return $data;
    }
    public function refunds_owner($id){
      $data = array(
          'pay_owner' => '1',
      );
      $a = $this->db->update($this->table, $data, array('id' => $id));
    }

    public function updateProject($id,$data){
      if(isset($id) && isset($data)){
        $this->db->where('id',$id);
        $data = $this->db->update($this->table,$data);
        if($data){
          return true;
        }else{
          return false;
        }
      }else{
        return false;
      }
    }


    public function uploadUserNoActive($id,$data){
      $a = $this->db->update($this->table, $data, array('id' => $id));
      if($a){
        return true;
      }else{
        return false;
      }
    }
    // Delete project with id
    public function delete($id) {
        $this->db->delete($this->table, array('id' => $id));
    }
}
