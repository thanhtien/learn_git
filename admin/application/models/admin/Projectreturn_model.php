<?php

/*
 * To change this license header, choose License Headers in ProjectReturn Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ProjectReturns_model
 *
 * @author TNM Group
 */
class Projectreturn_model extends CI_Model {
    private $table = TB_PROJECT_RETURNS;

    public function __construct() {
        parent::__construct();
    }
    // Get All Project Return(backed_level)
    public function getAllProjectReturn($order_by = 'id', $order_direction = 'ASC') {
        $this->db->order_by($order_by,$order_direction);
        $query = $this->db->get($this->table);
        return $query->result();
    }
    // Get Project return (backed_level) with pagination
    public function getProjectReturn($page = 0) {
        $total = $this->db->count_all_results($this->table);
        $limit = $this->config->item('admin_per_page');
        $start = ($page <= 1) ?  0 : ($page - 1)  * $limit;
        $this->db->limit($limit, $start);
        $this->db->order_by('id','DESC');
        $this->db->from($this->table .' c1');
        $query = $this->db->get();
        $project = $query->result();
        // debug_sql();
        return ["total" => $total, "project" => $project];
    }

    // Get Project Return(backed_level) with Id
    public function getProjectReturnById($id = 0) {
        if ((int) $id > 0) {
            $query = $this->db->get_where($this->table, array('id' => $id));
            return $query->row();
        } else {
            return NULL;
        }
    }

    // Get Project Return(backed_level) with Id
    public function getProjectReturnActiveById($id = 0) {
        if ((int) $id > 0) {
            $query = $this->db->get_where('backing_levels_edit', array('id' => $id));
            return $query->row();
        } else {
            return NULL;
        }
    }

    public function getProjectReturnActiveAll($project_id,$status) {
        if(isset($project_id) && $project_id && isset($status) && $status) {
            $this->db->from('backing_levels_edit');
            $this->db->where('project_id',$project_id);
            $this->db->where('list_status',$status);
            $param = $this->db->get();
            return $param->result();
        }else {
            return false;
        }
    }


    public function updateRefuse($id,$data){
        if(isset($id) && $id && isset($data) && $data){

            return $this->db->update('backing_levels_edit', $data, array('id' => $id));
        }else{
            return false;
        }
    }

    //Edit Project Return
    public function updateActive($id) {
        $date = new DateTime();
        $date = $date->format('Y-m-d H:i:s');
        $amount = $this->input->post('amount');
        $amount = preg_replace('/,/', '', $amount);
        $max_count = $this->input->post('max_count');
        if(isset($max_count) && $max_count){
          $max_count = preg_replace('/,/', '', $max_count);
        }else{
          $max_count = null;
        }
        $return_amount  = $this->input->post('content');
        $thumbnail = $this->input->post('hiden_image');
        $schedule = $this->input->post('schedule');
        if($schedule == ''){
          $schedule = null;
        }
        $data = array(
            'invest_amount' => $amount,
            'return_amount' => $return_amount,
            'max_count' => $max_count,
            'modified'=>$date,
            'schedule'=>$schedule,
            'thumnail'=>$thumbnail
        );
        return $this->db->update('backing_levels_edit', $data, array('id' => $id));
    }

    //Get Project Return(backed_level) with Projectid in project
    // Use it in controller project in edit
    public function getProjectReturnByProjectId($id = 0) {
        if ((int) $id > 0) {
            $query = $this->db->get_where($this->table, array('project_id' => $id));
            return $query->result();
        } else {
            return NULL;
        }
    }
    // Add Project Return
    public function insert($project_id) {
        $date = new DateTime();
        $date = $date->format('Y-m-d H:i:s');
        $amount = $this->input->post('amount');
        $amount = preg_replace('/,/', '', $amount);
        $max_count = $this->input->post('max_count');
        if(isset($max_count) && $max_count){
          $max_count = preg_replace('/,/', '', $max_count);
        }else{
          $max_count = null;
        }
        $project_type =$this->input->post('project_type');
        $return_amount  = $this->input->post('content');
        $thumbnail = $this->input->post('hiden_image');
        $schedule = $this->input->post('schedule');
        $name = $this->input->post('name');

        if($project_type === '1'){
            $number_month =$this->input->post('number_month');
            $product_fanclub_id =$this->input->post('product_fanclub_id');
            $settingKey =$this->input->post('settingKey');
            //include Stripe PHP library
            require_once APPPATH."third_party/stripe/init.php";

            \Stripe\Stripe::setApiKey($settingKey);

                $plan = \Stripe\Plan::create([
                  "nickname" => "Donate_".$amount,
                  "product" => $product_fanclub_id,
                  "amount" => $amount,
                  "currency" => "JPY",
                  "interval" => "day",
                  "interval_count" => 30*$number_month,
                  "usage_type" => "licensed",
                ]);
                $planId = $plan->id;
        }else {
            $planId = null;
        }

        if($schedule == ''){
          $schedule = null;
        }
        if($thumbnail == ''){
          $thumbnail = ''.image_url().'images/2018/default/noimage-01.png';
        }
        $data = array(
            'name' => $name,
            'project_id' => $project_id,
            'invest_amount' => $amount,
            'return_amount' => $return_amount,
            'max_count' => $max_count,
            'now_count'=>0,
            'created'=>$date,
            'schedule'=>$schedule,
            'thumnail'=>$thumbnail,
            'plan_fanclub_id'=>$planId,
        );
        $a = $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }

    //Edit Project Return
    public function update($id) {
        $date = new DateTime();
        $date = $date->format('Y-m-d H:i:s');
        $amount = $this->input->post('amount');
        $amount = preg_replace('/,/', '', $amount);
        $max_count = $this->input->post('max_count');
        if(isset($max_count) && $max_count){
          $max_count = preg_replace('/,/', '', $max_count);
        }else{
          $max_count = null;
        }
        $return_amount  = $this->input->post('content');
        $name  = $this->input->post('name');
        $thumbnail = $this->input->post('hiden_image');
        $schedule = $this->input->post('schedule');
        if($schedule == ''){
          $schedule = null;
        }
        $data = array(
            'name' => $name,
            'invest_amount' => $amount,
            'return_amount' => $return_amount,
            'max_count' => $max_count,
            'modified'=>$date,
            'schedule'=>$schedule,
            'thumnail'=>$thumbnail
        );
        return $this->db->update($this->table, $data, array('id' => $id));
    }
    // Delete Project Return
    public function delete($id) {
        $this->db->delete($this->table, array('id' => $id));
    }
}
