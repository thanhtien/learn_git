<?php

/*
 * To change this license header, choose License Headers in Category Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Categorys_model
 *
 * @author TNM Group
 */
class Category_model extends CI_Model {
    private $table = TB_CATEGORIES;

    public function __construct() {
        parent::__construct();
    }
    // Get all category
    public function getAllCategories($order_by = 'id', $order_direction = 'ASC') {
        $this->db->order_by($order_by,$order_direction);
        $query = $this->db->get($this->table);
        return $query->result();
    }
    //get category with pagination
    public function getCategories($sorting,$by,$page = 0) {
        $total = $this->db->count_all_results($this->table);
        $limit = $this->config->item('admin_per_page');
        $start = ($page <= 1) ?  0 : ($page - 1)  * $limit;
        $this->db->limit($limit, $start);
        $this->db->order_by('id','DESC');
        $this->db->from($this->table .' c1');
        $query = $this->db->get();
        $categories = $query->result();
        // debug_sql();
        return ["total" => $total, "categories" => $categories];
    }

    function searchCategories($sorting,$by,$search,$page = 0){
      $this->db->select('id');
      $this->db->from($this->table);
      $this->db->like('name', $search);
      $total = $this->db->count_all_results();

      $limit = $this->config->item('admin_per_page');
      $start = ($page <= 1) ?  0 : ($page - 1)  * $limit;
      $this->db->select('*');
      $this->db->from($this->table);
      $this->db->like('name', $search);
      $this->db->limit($limit, $start);
      $data = $this->db->get();

      $categories = $data->result();

      return ["total" => $total, "categories" => $categories];
    }

    //Get category with id
    public function getCategoryById($id = 0) {
        if ((int) $id > 0) {
            $query = $this->db->get_where($this->table, array('id' => $id));
            return $query->row();
        } else {
            return NULL;
        }
    }
    // // get category by slug
    // public function getCategoryBySlug($slug = '') {
    //     if (!empty($slug)) {
    //         $query = $this->db->get_where($this->table, array('slug' => $slug));
    //         return $query->row();
    //     } else {
    //         return NULL;
    //     }
    // }
    // Add Category
    public function insert() {
        // Use create_slug in helper Slug_helper
        $slug = create_slug($this->input->post('slug'));
        if($slug === '-'){
          return false;
        }
        $name = $this->input->post('name');
        $data = array(
            'name' => $name,
            'slug' => $slug,
        );
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }
    // Update Category
    public function update() {
        $id = $this->input->post('pid');
        $slug = create_slug($this->input->post('slug'));
        $name = $this->input->post('name');
        $uniq_name = createUniqueSlug($name, 'categories', 'name', $id);
        $uniq_slug = createUniqueSlug($slug, 'categories', 'slug', $id);
        $data = array(
            'name' => $uniq_name,
            'slug' => $uniq_slug
        );
        return $this->db->update($this->table, $data, array('id' => $id));
    }
    // Delete Category with id
    public function delete($id) {
        $this->db->delete($this->table, array('id' => $id));
    }

    public function getProjectByCategoryId($id = 0) {
        //Check Id is exists
        if ((int) $id > 0) {
            //Use get where Check Id in database
            $query = $this->db->get_where('projects', array('category_id' => $id));
            //Return Data
            return $query->result();
        } else {
            // Return Null
            return NULL;
        }
    }

    public function getProjectByCategoryIds($id) {
        //Check Id is exists
        if ($id) {
            //Use get where Check Id in database
            $this->db->from('projects');
            $this->db->where("category_id in ($id)");
            //Return Data
            $query = $this->db->get();
            return $query->result();
        } else {
            // Return Null
            return NULL;
        }
    }
}
