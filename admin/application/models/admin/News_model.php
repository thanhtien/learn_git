<?php

/*
 * To change this license header, choose License Headers in New Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of News_model
 *
 * @author TNM Group
 */
class News_model extends CI_Model {
    private $table = TB_NEWS;

    public function __construct() {
        parent::__construct();
    }
    // Get all category
    public function getAllNews($order_by = 'id', $order_direction = 'ASC') {
        $this->db->order_by($order_by,$order_direction);
        $query = $this->db->get($this->table);
        return $query->result();
    }
    //get category with pagination
    public function getNews($page = 0) {
        $total = $this->db->count_all_results($this->table);
        $limit = $this->config->item('admin_per_page');
        $start = ($page <= 1) ?  0 : ($page - 1)  * $limit;
        $this->db->limit($limit, $start);
        $this->db->order_by('updated','DESC');
        $this->db->from($this->table .' c1');
        $query = $this->db->get();
        $news = $query->result();
        // debug_sql();
        return ["total" => $total, "news" => $news];
    }
    //Get category with id
    public function getNewById($id = 0) {
        if ((int) $id > 0) {
            $query = $this->db->get_where($this->table, array('id' => $id));
            return $query->row();
        } else {
            return NULL;
        }
    }
    public function insert() {
        $date = new DateTime();
        $date = $date->format('Y-m-d H:i:s');
        $title = $this->input->post('title');
        $image = $this->input->post('hiden_image');
        $save = $this->input->post('save');
        $public = $this->input->post('public');
        if(isset($save) && $save ) {
            $status = 0;
        }else{
            $status = 1;
        }

        if($image == ''){
          $image = ''.image_url().'images/2018/default/noimage-01.png';
        }
        $content = $this->input->post('content');
        $data = array(
            'title' => $title,
            'thumnail' => $image,
            'content' => $content,
            'status'  => $status,
            'created' => $date,
            'updated' => $date,
        );

        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }
    // Update New
    public function update() {
        $date = new DateTime();
        $date = $date->format('Y-m-d H:i:s');
        $id = $this->input->post('pid');
        $title = $this->input->post('title');
        $image = $this->input->post('hiden_image');
        $content = $this->input->post('content');

        $save = $this->input->post('save');
        $public = $this->input->post('public');
        if(!isset($save)){
            if($public === '1'){
                $status = 1;
            }else{
                $status = 0;
            }
        }else{
            $status = $this->input->post('status');
        }
        $data = array(
            'title' => $title,
            'thumnail' => $image,
            'content' => $content,
            'status'  => $status,
            'updated' => $date,
        );
        return $this->db->update($this->table, $data, array('id' => $id));
    }
    // Delete New with id
    public function delete($id) {
        $del = $this->db->delete($this->table, array('id' => $id));
        if($del) {
            return true;
        }else{
            return false;
        }
    }

    public function getLastId(){
        $this->db->select('id');
        $this->db->order_by('id','DESC');
        $this->db->limit(1);
        $this->db->from($this->table .' c1');
        $query = $this->db->get();
        return $query->row();
    }
}
