<?php

/*
 * To change this license header, choose License Headers in Setting Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Settings_model
 *
 * @author TNM Group
 */
class Setting_model extends CI_Model {

    private $table = 'setting';

    public function __construct() {
        parent::__construct();
    }

    public function getSettings() {
        $this->db->limit(1);
        $query = $this->db->get($this->table);
        $settings = $query->row();
        return $settings;
    }

    public function getSettingById($id = 0) {
        if ((int) $id > 0) {
            $query = $this->db->get_where($this->table, array('id' => $id));
            return $query->row_array();
        } else {
            return NULL;
        }
    }

    public function updateSetting($data) {
        if(isset($data) && $data){
            $update = $this->db->update($this->table, $data, ['id' => '1']);
            return $update;
        }else{
            return false;
        }
    }

    public function delete($id) {
        $this->db->delete($this->table, array('id' => $id));
    }

}
