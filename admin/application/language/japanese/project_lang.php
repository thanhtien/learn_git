<?php
  //Project Index
  $lang['project_name'] = 'プロジェクト名';
  $lang['goal_amount'] = '目標金額';
  $lang['collection_amount'] = '現在の支援総額';
  $lang['master_project'] = 'ファンディングの企画者';
  $lang['number_people_donate'] = '支援者数';
  $lang['active'] = '公開・非公開';
  $lang['active_yes'] = '公開';
  $lang['active_no'] = '非公開';
  $lang['start_date'] = '開始';
  $lang['end_date'] = '完了';

  //Message
  $lang['create_project_success'] = 'プロジェクト作成ができました';
  $lang['create_project_failure'] = 'エラーが出ましたので、プロジェクト新規作成がまだできません';
  $lang['edit_project_success'] = '更新が保存されました。';
  $lang['edit_project_failure'] = 'エラーが出ましたので、更新が保存できません。';
  $lang['create_project_return_success'] = 'リターン作成ができました';
  $lang['create_project_return_failure'] = 'リターン作成ができませんでした。';
  $lang['edit_project_return_success'] = '更新ができました。';
  $lang['edit_project_return_failure'] = 'エラーが出ています。更新が保存できません。';

  //Create Project
  $lang['user_name'] = 'ユーザー名';
  $lang['image_thumnail'] = '画像（サムネイル表示）';
  $lang['note_image'] = '※画像のサイズは600px＊400pxで最高です。';
  $lang['summary'] = 'はじめにご挨拶 （最大100文字）';
  $lang['note_upload'] = '※「動画のURL入力」か「画像アップロード」を選択して、該当のURLか、画像をアップロードしてください。';
  $lang['upload_video'] = '動画のURL入力';
  $lang['upload_image'] = '画像アップロード';
  $lang['note_upload_image'] = '※2～３枚の画像をアップロードしたら、スライダーで表示されるようになります。';
  $lang['video'] = '動画URL';
  $lang['image1'] = '画像 1';
  $lang['image2'] = '画像 2';
  $lang['image3'] = '画像 3';
  $lang['category'] = 'カテゴリ';
  $lang['collection_end_date'] = '募集終了日';
  $lang['project_content'] = 'プロジェクト概要';

  //page_title
  $lang['create_project'] = 'プロジェクト概要・詳細';
  $lang['return_project'] = 'リターン作成';

  //List Project Return
  $lang['amount'] = '支援額';
  $lang['content'] = 'リターン品・説明文';
  $lang['date_send'] = 'お届け予定';
  $lang['add_new_return'] = 'リターン追加';

  //Craete Project return
  $lang['invert_amount'] = '支援額';
  $lang['return_amout'] = 'リターン品・説明文';
  $lang['image'] = '画像';
  $lang['schedule'] = 'お届け予定';


?>
