<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <?php echo $this->lang->line('return_project'); ?>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active"><a href="<?php echo site_url('project/edit/'.$projectreturn->project_id)?>"><?php echo $this->lang->line('return_project'); ?></a></li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <?php if ($this->session->flashdata('error')) { ?>
          <div class="alert alert-danger" id="success-alert">
              <button type="button" class="close" data-dismiss="alert"><i class="fa fa-times"></i></button>
              <strong><?php echo $this->lang->line('error'); ?></strong>
              <?php echo $this->session->flashdata('error'); ?>
          </div>
        <?php } ?>
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title"><?php echo $this->lang->line('return_project'); ?></h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" id="ProjectReturn" action="" method="POST" enctype="multipart/form-data">
                <div class="nav-tabs-custom">
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab_1">
                          <div class="box-body">


                              <?php if($project_type === '0'){ ?>
                                  <div class="form-group ">
                                    <div class="col-sm-6">
                                      <label for="name"><?php echo $this->lang->line('invert_amount'); ?></label>
                                      <input type="text" autocomplete="off" class="form-control money" autocomplete="off" value="<?php echo $projectreturn->invest_amount ?>" required="" id="amount" name="amount" onkeypress='validate(event)'>
                                    </div>

                                    <div class="form-group col-sm-6">
                                      <label for="name"><?php echo $this->lang->line('schedule'); ?></label>
                                      <input autocomplete="off"  maxlength="10" type="text" class="form-control" name="schedule" value="<?php echo $projectreturn->schedule; ?>">
                                    </div>
                                  </div>
                                  <div class="form-group col-sm-12">
                                      <label for="name">リターン品名</label>
                                      <input type="text" autocomplete="off" class="form-control" autocomplete="off" required="" value="<?php echo $projectreturn->name ?>" id="name" name="name">
                                  </div>
                              <?php }else { ?>
                                  <div class="form-group col-sm-12">
                                      <label for="name"><?php echo $this->lang->line('invert_amount'); ?></label>
                                      <input type="text" autocomplete="off" class="form-control money" autocomplete="off" value="<?php echo $projectreturn->invest_amount ?>" required="" id="amount" name="amount" onkeypress='validate(event)'>
                                  </div>
                                  <div class="form-group col-sm-12">
                                      <label for="name">Name</label>
                                      <input type="text" autocomplete="off" class="form-control" autocomplete="off" required="" value="<?php echo $projectreturn->name ?>" id="name" name="name">
                                  </div>
                              <?php } ?>



                              <div class="form-group col-sm-12">
                                  <label for="name">件数</label>
                                  <input type="text" autocomplete="off" class="form-control money" autocomplete="off"  id="" value="<?php echo $projectreturn->max_count ?>" name="max_count" onkeypress='validate(event)'>
                              </div>
                              <div class="form-group col-sm-12">
                                  <label for="name"><?php echo $this->lang->line('return_amout'); ?></label>
                                  <textarea class="form-control ckeditor" name="content" id="content"><?php echo $projectreturn->return_amount ?></textarea>
                              </div>

                              <div class="form-group col-sm-12">
                                  <label for="name"><?php echo $this->lang->line('image'); ?></label>
                                  <p>※画像のサイズは600px＊400pxで最高です。容量 2MBまで 。</p>
                                  <input type="file" name="file" id="profile-img">
                                  <input type="hidden" name="hiden_image" id="hiden_image"  value="<?php echo $projectreturn->thumnail ?>">
                                  <p id="announce" style="color:red" ></p>
                                  <p style="text-align:center"><img src="<?php echo $projectreturn->thumnail ?>" id="profile-img-tag" width="300px"  /></p>
                              </div>
                          </div>
                        </div>
                    </div>
                </div>


                <div class="box-footer">
                    <button type="submit" name="save" class="btn btn-primary" value="1"><?php echo $this->lang->line('save'); ?></button>
                    <a <?php $this->session->set_flashdata('tab', '1'); ?> href="<?php echo site_url('project/edit/'.$projectreturn->project_id)?>" class="btn btn-default"><?php echo $this->lang->line('cancel'); ?></a>
                    <input type="hidden" name="pid" value="0" />
                    <input type="hidden" name="pid" value="<?php echo $this->ion_auth->user()->row()->id; ?>" />
                </div>
            </form>
        </div>

    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
