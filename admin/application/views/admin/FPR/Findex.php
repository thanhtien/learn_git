<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            人気のプロジェクト
        </h1>
        <ol class="breadcrumb">
            <li><a href="/"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active"><a href="<?php echo site_url('/FPR_Project/Findex')?>">人気のプロジェクト</a></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <?php if ($this->session->flashdata('msg')) { ?>
            <div class="alert alert-success" id="success-alert">
                <button type="button" class="close" data-dismiss="alert"><i class="fa fa-times"></i></button>
                <strong><?php echo $this->lang->line('success'); ?></strong>
                <?php echo $this->session->flashdata('msg'); ?>
            </div>
        <?php } ?>
        <?php if ($this->session->flashdata('error')) { ?>
          <div class="alert alert-danger" id="success-alert">
              <button type="button" class="close" data-dismiss="alert"><i class="fa fa-times"></i></button>
              <strong><?php echo $this->lang->line('error'); ?></strong>
              <?php echo $this->session->flashdata('error'); ?>
          </div>
        <?php } ?>
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">人気のプロジェクト</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" action="" method="POST" enctype="multipart/form-data">
                <div class="nav-tabs-custom">
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab_1">
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="content">1番目</label>
                                    <select class="form-control selectPickup" name="pickup[]">
                                        <option selected value="">プロジェクトを選択する</option>
                                        <?php foreach ($project as $key => $value) {?>
                                            <?php if(isset($show['0']) && $show['0'] === $value->id){ ?>
                                                <option selected value="<?php echo $value->id ?>"><?php echo $value->id ?>:<?php echo $value->project_name; ?></option>
                                            <?php }else if(isset($show['0']) && $show['0'] !== $value->id){ ?>
                                                <option value="<?php echo $value->id ?>" class="<?php echo $value->id ?>"><?php echo $value->id ?>:<?php echo $value->project_name; ?></option>
                                            <?php }?>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="content">2番目</label>
                                    <select class="form-control selectPickup" name="pickup[]">
                                        <option selected value="">プロジェクトを選択する</option>
                                        <?php foreach ($project as $key => $value) {?>
                                            <?php if(isset($show['1']) && $show['1'] === $value->id){ ?>
                                                <option selected value="<?php echo $value->id ?>"><?php echo $value->id ?>:<?php echo $value->project_name; ?></option>
                                            <?php }else if(isset($show['1']) && $show['1'] !== $value->id){ ?>
                                                <option value="<?php echo $value->id ?>" class="<?php echo $value->id ?>"><?php echo $value->id ?>:<?php echo $value->project_name; ?></option>
                                            <?php }?>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="content">3番目</label>
                                    <select class="form-control" name="pickup[]">
                                        <option selected value="">プロジェクトを選択する</option>
                                        <?php foreach ($project as $key => $value) {?>
                                            <?php if(isset($show['2']) && $show['2'] === $value->id){ ?>
                                                <option selected value="<?php echo $value->id ?>"><?php echo $value->id ?>:<?php echo $value->project_name; ?></option>
                                            <?php }else if(isset($show['2']) && $show['2'] !== $value->id){ ?>
                                                <option value="<?php echo $value->id ?>"><?php echo $value->id ?>:<?php echo $value->project_name; ?></option>
                                            <?php }?>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="content">4番目</label>
                                    <select class="form-control" name="pickup[]">
                                        <option selected value="">プロジェクトを選択する</option>
                                        <?php foreach ($project as $key => $value) {?>
                                            <?php if(isset($show['3']) && $show['3'] === $value->id){ ?>
                                                <option selected value="<?php echo $value->id ?>"><?php echo $value->id ?>:<?php echo $value->project_name; ?></option>
                                            <?php }else if(isset($show['3']) && $show['3'] !== $value->id){ ?>
                                                <option value="<?php echo $value->id ?>"><?php echo $value->id ?>:<?php echo $value->project_name; ?></option>
                                            <?php }?>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="content">5番目</label>
                                    <select class="form-control" name="pickup[]">
                                        <option selected value="">プロジェクトを選択する</option>
                                        <?php foreach ($project as $key => $value) {?>
                                            <?php if(isset($show['4']) && $show['4'] === $value->id){ ?>
                                                <option selected value="<?php echo $value->id ?>"><?php echo $value->id ?>:<?php echo $value->project_name; ?></option>
                                            <?php }else if(isset($show['4']) && $show['4'] !== $value->id){ ?>
                                                <option value="<?php echo $value->id ?>"><?php echo $value->id ?>:<?php echo $value->project_name; ?></option>
                                            <?php }?>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="content">6番目</label>
                                    <select class="form-control" name="pickup[]">
                                        <option selected value="">プロジェクトを選択する</option>
                                        <?php foreach ($project as $key => $value) {?>
                                            <?php if(isset($show['5']) && $show['5'] === $value->id){ ?>
                                                <option selected value="<?php echo $value->id ?>"><?php echo $value->id ?>:<?php echo $value->project_name; ?></option>
                                            <?php }else if(isset($show['5']) && $show['5'] !== $value->id){ ?>
                                                <option value="<?php echo $value->id ?>"><?php echo $value->id ?>:<?php echo $value->project_name; ?></option>
                                            <?php }?>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="content">7番目</label>
                                    <select class="form-control" name="pickup[]">
                                        <option selected value="">プロジェクトを選択する</option>
                                        <?php foreach ($project as $key => $value) {?>
                                            <?php if(isset($show['6']) && $show['6'] === $value->id){ ?>
                                                <option selected value="<?php echo $value->id ?>"><?php echo $value->id ?>:<?php echo $value->project_name; ?></option>
                                            <?php }else if(isset($show['6']) && $show['6'] !== $value->id){ ?>
                                                <option value="<?php echo $value->id ?>"><?php echo $value->id ?>:<?php echo $value->project_name; ?></option>
                                            <?php }?>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="content">8番目</label>
                                    <select class="form-control" name="pickup[]">
                                        <option selected value="">プロジェクトを選択する</option>
                                        <?php foreach ($project as $key => $value) {?>
                                            <?php if(isset($show['7']) && $show['7'] === $value->id){ ?>
                                                <option selected value="<?php echo $value->id ?>"><?php echo $value->id ?>:<?php echo $value->project_name; ?></option>
                                            <?php }else if(isset($show['7']) && $show['7'] !== $value->id){ ?>
                                                <option value="<?php echo $value->id ?>"><?php echo $value->id ?>:<?php echo $value->project_name; ?></option>
                                            <?php }?>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <!-- /.box-body -->
                        </div>
                    </div>
                    <!-- /.tab-content -->
                </div>


                <div class="box-footer">
                    <button type="submit" name="save" class="btn btn-primary" value="1"><?php echo $this->lang->line('save'); ?></button>
                    <a href="<?php echo site_url('/FPR_Project/Findex/') ?>" class="btn btn-default"><?php echo $this->lang->line('cancel'); ?></a>
                </div>
            </form>
        </div>
    </section>
</section>
<!-- /.content -->
</div>
<!-- /.content-wrapper -->
