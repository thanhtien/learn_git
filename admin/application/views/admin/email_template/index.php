<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            メールテンプレート
        </h1>
        <ol class="breadcrumb">
            <li><a href="/"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active"><a href="<?php echo site_url('/EmailTemplate')?>">メールテンプレート</a></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <?php if ($this->session->flashdata('msg')) { ?>
            <div class="alert alert-success" id="success-alert">
                <button type="button" class="close" data-dismiss="alert"><i class="fa fa-times"></i></button>
                <strong><?php echo $this->lang->line('success'); ?></strong>
                <?php echo $this->session->flashdata('msg'); ?>
            </div>
        <?php } ?>
        <?php if ($this->session->flashdata('error')) { ?>
          <div class="alert alert-danger" id="success-alert">
              <button type="button" class="close" data-dismiss="alert"><i class="fa fa-times"></i></button>
              <strong><?php echo $this->lang->line('error'); ?></strong>
              <?php echo $this->session->flashdata('error'); ?>
          </div>
        <?php } ?>
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <form id="frmMain" method="POST" action="/news/action">
                        <div class="box-header">
                            <h3 class="box-title">&nbsp;</h3>

                            <div class="box-tools">
                                <div class="btn-group pull-right">
                                    <a target="_blank" class="btn btn-sm btn-primary " href="https://beefree.io/editor"><i class="fa fa-plus"></i> メールテンプレート新規作成</a>
                                    <a  class="btn btn-sm btn-primary " href="<?php echo site_url('/EmailTemplate/UploadEmailTemplate') ?>"><i class="fa fa-plus"></i> メールテンプレートアップロード</a>
                                    <input type="hidden" id="hidAction" name="hidAction" value="" />
                                </div>
                            </div>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body no-padding">
                            <table class="table table-bordered table-hover" id="preViewEmailTemplate">
                                <tr>
                                    <th >タイトル</th>
                                    <th style="width: 80px"></th>
                                    <th style="width: 80px"></th>
                                    <th style="width: 80px"></th>
                                    <th style="width: 80px"></th>
                                </tr>
                                <?php if ($total > 0) { ?>
                                    <?php for ($i=0; $i < $total ; $i++) {
                                            // $template[$i] = str_replace('/', '', )
                                            $template[$i] = substr($email_template[$i]['name'], 0, -1);
                                    ?>
                                    <?php  ?>
                                        <tr data-toggle="modal" data-target="#exampleModal" style="cursor:pointer;"  id="<?php echo $email_template[$i]['name'];?>">

                                            <td ><?php echo $template[$i]; ?></td>

                                            <td  style="text-align:center">
                                                <a target="_blank" href="/EmailTemplate/previewEmailTemplate/<?php echo $template[$i]; ?>">
                                                    <i class="fa fa-eye"></i>
                                                </a>
                                            </td>
                                            <td  style="text-align:center">
                                                <a href="/EmailTemplate/EmailTemplateGroup/<?php echo $template[$i]; ?>">
                                                    <i class="fa fa-users"></i>
                                                </a>
                                            </td>
                                            <td style="text-align:center">

                                                <a class='sendmail'>
                                                    <i class="fa fa-envelope-o" aria-hidden="true"></i>
                                                </a>
                                            </td>
                                            <td style="text-align:center;">
                                                <a href="/EmailTemplate/delete/<?php echo $template[$i]; ?>" class="btn btn-xs btn-danger" data-title="このメールテンプレートを削除しますか。" data-btn-ok-label="はい"  data-btn-cancel-label="いいえ" data-toggle="confirmation2" data-placement="left" data-singleton="true"><i class="fa fa-trash-o"></i></a>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                <?php }else{ ?>
                                    <tr>
                                        <td colspan="4" style="text-align:center;"><?php echo $this->config->item('no_data')?></td>
                                    </tr>
                                <?php } ?>
                            </table>

                        </div>
                    </form>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->

            </div>
        </div>
    </section>
</section>
<!-- /.content -->
</div>
<!-- /.content-wrapper -->
