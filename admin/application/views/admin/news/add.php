<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            ニュース
        </h1>
        <ol class="breadcrumb">
            <li><a href="/"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active"><a href="<?php echo site_url('/news')?>">ニュース</a></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <?php if ($this->session->flashdata('msg')) { ?>
          <div class="alert alert-success" id="success-alert">
              <button type="button" class="close" data-dismiss="alert"><i class="fa fa-times"></i></button>
              <strong><?php echo $this->lang->line('success'); ?></strong>
              <?php echo $this->session->flashdata('msg'); ?>

          </div>
        <?php } ?>
        <?php if ($this->session->flashdata('error')) { ?>
          <div class="alert alert-danger" id="success-alert">
              <button type="button" class="close" data-dismiss="alert"><i class="fa fa-times"></i></button>
              <strong><?php echo $this->lang->line('error'); ?></strong>
              <?php echo $this->session->flashdata('error'); ?>
          </div>
        <?php } ?>
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">ニュース・新規作成</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" action="" method="POST" enctype="multipart/form-data">
                <div class="nav-tabs-custom">
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab_1">
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="title">タイトル</label>
                                    <input type="text" class="form-control" required="" id="title" name="title"  placeholder="">
                                </div>
                                <div class="form-group" id="form_img">
                                  <label for="new-img">画像（サムネイル表示）</label>
                                  <p>※画像のサイズは600px＊400pxで最高です。容量 2MBまで 。</p>
                                  <input type="file" name="file" id="new-img">
                                  <input type="hidden" name="hiden_image" id="hiden_image" >
                                  <p id="announce" style="color:red" ></p>
                                  <p style="text-align: center;"><img src="" id="new-img-tag"/ width="600px" alt=""></p>
                                </div>
                                <div class="form-group">
                                    <label for="content">コンテンツ</label>
                                    <textarea class="form-control ckeditor" name="content" id="description"></textarea>
                                </div>
                            </div>
                            <!-- /.box-body -->
                        </div>
                    </div>
                    <!-- /.tab-content -->
                </div>

                <div id="myModal" class="modal fade" role="dialog">
                  <div class="modal-dialog modal-lg">
                      <style media="screen">
                        .title-green {
                            border-left: 5px solid #01a662;
                            padding: 10px;
                        }
                        .green-time {
                            color: #01a662;
                            margin-bottom: 15px;
                        }
                        .title-page {
                            font-weight: 700;
                            font-size: 20px;
                        }
                        .modal-body{
                            margin-left: 15px;
                        }
                      </style>
                    <!-- Modal content-->
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                      </div>
                      <div class="modal-body">
                            <?php $newDate = new DateTime(); $newDate = date_format($newDate,'Y-m-d');?>
                            <?php $date=date_create($newDate); ?>
                            <?php $year = (date_format($date,"Y")); ?>
                            <?php $month = (date_format($date,"m")); ?>
                            <?php $days = (date_format($date,"d")); ?>
                        <div class="title-green">
                            <p class="green-time"><?php echo $year.'年'.$month.'月'.$days.'日' ?></p>
                            <p class="title-page" id="title_preivew">プレビュー</p>
                        </div>
                        <br>
                        <img class="img_thumnail" src="<?php echo image_url().'images/2018/default/noimage-01.png'; ?>" alt="">
                        <br>
                        <br>
                        <div class="content_preview">
                            プレビュー
                        </div>
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">閉じる</button>
                      </div>
                    </div>

                  </div>
                </div>

                <div class="box-footer">
                    <button type="submit" name="save" class="btn btn-primary" value="1"><?php echo $this->lang->line('save'); ?></button>
                    <button type="submit" name="public" class="btn btn-danger" value="1">公開</button>
                    <button id="previewNew" type="button" class="btn btn-warning" data-toggle="modal" data-target="#myModal">プレビュー</button>
                    <a href="<?php echo site_url('/news/') ?>" class="btn btn-default"><?php echo $this->lang->line('cancel'); ?></a>
                    <input type="hidden" name="id" id="idNew" value="<?php echo $id ?>">
                </div>
            </form>
        </div>

    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
