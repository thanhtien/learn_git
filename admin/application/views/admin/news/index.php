<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            ニュース
        </h1>
        <ol class="breadcrumb">
            <li><a href="/"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active"><a href="<?php echo site_url('/news')?>">ニュース</a></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <?php if ($this->session->flashdata('msg')) { ?>
            <div class="alert alert-success" id="success-alert">
                <button type="button" class="close" data-dismiss="alert"><i class="fa fa-times"></i></button>
                <strong><?php echo $this->lang->line('success'); ?></strong>
                <?php echo $this->session->flashdata('msg'); ?>
            </div>
        <?php } ?>
        <?php if ($this->session->flashdata('error')) { ?>
          <div class="alert alert-danger" id="success-alert">
              <button type="button" class="close" data-dismiss="alert"><i class="fa fa-times"></i></button>
              <strong><?php echo $this->lang->line('error'); ?></strong>
              <?php echo $this->session->flashdata('error'); ?>
          </div>
        <?php } ?>
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <form id="frmMain" method="POST" action="/news/action">
                        <div class="box-header">
                            <h3 class="box-title">&nbsp;</h3>

                            <div class="box-tools">
                                <div class="btn-group pull-right">
                                    <a class="btn btn-sm btn-primary " href="<?php echo site_url('/news/add') ?>"><i class="fa fa-plus"></i> 新規作成</a>
                                    <a id="bulk-delete" class="btn btn-sm btn-danger " data-title="選択されたものを削除したいですか。" data-btn-ok-label="<?php echo $this->lang->line('yes'); ?>"  data-btn-cancel-label="<?php echo $this->lang->line('no'); ?>"  data-toggle="confirmation" data-placement="left" data-singleton="true"><i class="fa fa-trash-o"></i> 削除</a>
                                    <input type="hidden" id="hidAction" name="hidAction" value="" />
                                </div>
                            </div>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body no-padding">
                            <table class="table table-bordered table-hover">
                                <tr>
                                    <th style="width: 20px"><input type="checkbox" class="minimal checkth" ></th>
                                    <th style="width: 300px">タイトル</th>
                                    <th >コンテンツ</th>
                                    <th style="width: 95px;">ステータス</th>
                                    <th style="width: 85px;">更新日付</th>
                                    <th style="width: 80px"></th>
                                </tr>
                                <?php if ($data['total'] > 0) { ?>
                                    <?php foreach ($data['news'] as $key => $new) { ?>
                                        <tr>
                                            <td><input type="checkbox" class="minimal checkitem" name="val[]" value="<?php echo $new->id ?>" ></td>
                                            <td>
                                              <a style="display:block;color:black;" class="title_shorten" href="/news/edit/<?php echo $new->id; ?>" style="color:black" ><?php echo $new->title ?></a>
                                            <td>
                                                <a style="display:block;color:black;" class="content_shorten" href="/news/edit/<?php echo $new->id; ?>" style="color:black" ><?php echo strip_tags($new->content); ?></a>
                                            </td>
                                            <td>
                                                <a style="display:block;color:black;" href="/news/edit/<?php echo $new->id; ?>" style="color:black" ><?php if($new->status === '1'){ echo "公開中";}else{ echo "非公開";} ?></a>
                                            </td>
                                            <td>
                                                <a style="display:block;color:black;" href="/news/edit/<?php echo $new->id; ?>" style="color:black" ><?php echo ($new->updated); ?></a>
                                            </td>
                                            <td>
                                                <a href="/news/edit/<?php echo $new->id; ?>" class="btn btn-xs btn-primary"><i class="fa fa-edit"></i></a>
                                                <a href="/news/delete/<?php echo $new->id; ?>" class="btn btn-xs btn-danger" data-title="<?php echo $this->lang->line('are_you_sure'); ?>" data-btn-ok-label="<?php echo $this->lang->line('yes'); ?>"  data-btn-cancel-label="<?php echo $this->lang->line('no'); ?>" data-toggle="confirmation2" data-placement="left" data-singleton="true"><i class="fa fa-trash-o"></i></a>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                <?php } else { ?>
                                        <tr>
                                            <td colspan="3" class="text-center"><?php echo $this->config->item('no_data')?></td>
                                        </tr>
                                <?php }?>
                            </table>

                        </div>
                        <div class="box-footer clearfix">
                            <?php echo custom_pagination('/news/index/', $data['total'],10);?>
                        </div>
                    </form>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->

            </div>
        </div>
    </section>
</section>
<!-- /.content -->
</div>
<!-- /.content-wrapper -->
