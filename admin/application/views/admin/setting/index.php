<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            設定
        </h1>
        <ol class="breadcrumb">
            <li><a href="/"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active"><a href="<?php echo site_url('/setting')?>">設定</a></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <?php if ($this->session->flashdata('msg')) { ?>
            <div class="alert alert-success" id="success-alert">
                <button type="button" class="close" data-dismiss="alert"><i class="fa fa-times"></i></button>
                <strong><?php echo $this->lang->line('success'); ?></strong>
                <?php echo $this->session->flashdata('msg'); ?>
            </div>
        <?php } ?>
        <?php if ($this->session->flashdata('error')) { ?>
          <div class="alert alert-danger" id="success-alert">
              <button type="button" class="close" data-dismiss="alert"><i class="fa fa-times"></i></button>
              <strong><?php echo $this->lang->line('error'); ?></strong>
              <?php echo $this->session->flashdata('error'); ?>
          </div>
        <?php } ?>
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">&nbsp;</h3>

                            <div class="box-tools">
                                <div class="btn-group pull-right">
                                    <a class="btn btn-sm btn-primary " href="<?php echo site_url('/setting/add') ?>"><i class="fa fa-plus"></i> STRIPE設定へのテスト</a>
                                </div>
                            </div>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body no-padding">
                            <table class="table table-bordered table-hover">
                                <tr>
                                    <th style="width: 20px"><input type="checkbox" class="minimal checkth" ></th>
                                    <th>公開キー</th>
                                    <th>シークレットキー</th>
                                    <!-- <th style="width: 80px"></th> -->
                                </tr>

                                <?php if(isset($settings) && $settings){ ?>
                                    <tr>
                                        <td><input type="checkbox" class="minimal checkitem" name="val[]" value="<?php echo $settings->id ?>" ></td>
                                        <td>
                                          <a href="/setting/edit" style="display:block;color:black" ><?php echo $settings->public_key; ?></a></td>
                                        <td>
                                            <a  href="/setting/edit" style="color:black;display:block;" ><?php echo $settings->secret_key; ?></a></td>
                                        </td>
                                        <!-- <td style="text-align:center">
                                            <a href="/setting/edit" class="btn btn-xs btn-primary" style="d"><i class="fa fa-edit"></i></a>
                                        </td> -->
                                    </tr>
                                <?php  }else {?>
                                        <tr>
                                            <td colspan="3" class="text-center"><?php echo $this->config->item('no_data')?></td>
                                        </tr>
                                <?php } ?>
                            </table>
                        </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->

            </div>
        </div>
    </section>
</section>
<!-- /.content -->
</div>
<!-- /.content-wrapper -->
