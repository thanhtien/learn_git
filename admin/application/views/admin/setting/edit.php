<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            設定
        </h1>
        <ol class="breadcrumb">
            <li><a href="/"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active"><a href="<?php echo site_url('/setting')?>">設定</a></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <?php if ($this->session->flashdata('msg')) { ?>
          <div class="alert alert-success" id="success-alert">
              <button type="button" class="close" data-dismiss="alert"><i class="fa fa-times"></i></button>
              <strong><?php echo $this->lang->line('success'); ?></strong>
              <?php echo $this->session->flashdata('msg'); ?>

          </div>
        <?php } ?>
        <?php if ($this->session->flashdata('error')) { ?>
          <div class="alert alert-danger" id="success-alert">
              <button type="button" class="close" data-dismiss="alert"><i class="fa fa-times"></i></button>
              <strong><?php echo $this->lang->line('error'); ?></strong>
              <?php echo $this->session->flashdata('error'); ?>
          </div>
        <?php } ?>
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">STRIPEカード編集</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" action="" method="POST" enctype="multipart/form-data">
                <div class="nav-tabs-custom">
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab_1">
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="public_key">公開キー</label>
                                    <input type="text" class="form-control" required="" id="public_key" name="public_key" value="<?php echo $setting->public_key ?>" placeholder="">
                                </div>
                                <div class="form-group">
                                    <label for="secret_key">シークレットキー</label>
                                    <input type="text" class="form-control" required="" id="secret_key" name="secret_key" value="<?php echo $setting->secret_key ?>" placeholder="">
                                </div>
                            </div>
                            <!-- /.box-body -->
                        </div>
                    </div>
                    <!-- /.tab-content -->
                </div>
                <div class="box-footer">
                    <button type="submit" name="save" class="btn btn-primary" value="1"><?php echo $this->lang->line('save'); ?></button>
                    <a href="<?php echo site_url('/setting/') ?>" class="btn btn-default"><?php echo $this->lang->line('cancel'); ?></a>
                </div>
            </form>
        </div>

    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
