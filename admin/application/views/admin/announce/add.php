<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            管理者からの通知一覧
        </h1>
        <ol class="breadcrumb">
            <li><a href="/"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active"><a href="<?php echo site_url('/announce')?>">管理者からの通知一覧</a></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <?php if ($this->session->flashdata('error')) { ?>
          <div class="alert alert-danger" id="success-alert">
              <button type="button" class="close" data-dismiss="alert"><i class="fa fa-times"></i></button>
              <strong><?php echo $this->lang->line('error'); ?></strong>
              <?php echo $this->session->flashdata('error'); ?>
          </div>
        <?php } ?>
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">管理者からの通知一覧 </h3>
            </div>

            <div class="modal fade " id="ModalPreView" role="dialog">
              <div class="modal-dialog modal-lg" style="width:1100px;">
                <!-- Modal content-->
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" style="color:red"><i class="fa fa-times-circle"></i></button>
                    <h4 class="modal-title"></h4>
                  </div>
                  <div class="modal-body">
                    <div class="preview">
                      <div class="pd-banner">
                          <div class="wraper">
                              <div class="pannel">
                                  <p class="pannel-head" id="title_preview"></p>
                              </div>
                          </div>
                      </div>
                      <!-- BANNER -->
                      <div id="content">
                          <div class="" id="tabPre-1">

                          </div>
                      </div>
                      <!-- CONTENT -->
                    </div>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">閉じる</button>
                  </div>
                </div>

              </div>
            </div>

            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" action="" method="POST" enctype="multipart/form-data">
                <div class="nav-tabs-custom">
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab_1">
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="name">タイトル</label>
                                    <input type="text" class="form-control" required="" id="title" name="title" placeholder="" maxlength="255">
                                </div>
                                <div class="form-group">
                                    <label for="name">本文</label>
                                    <textarea class="form-control ckeditor" name="message" id="description"></textarea>
                                </div>

                            </div>
                            <!-- /.box-body -->
                        </div>
                    </div>
                </div>


                <div class="box-footer">

                    <button type="submit" name="save" class="btn btn-primary" value="1" id="upload">下書き</button>
                    <button type="submit" name="public" class="btn btn-danger" value="1" id="upload">公開</button>
                    <p id="setPreviewNotification" class="btn btn-danger" data-toggle="modal" data-target="#ModalPreView" >プレビュー</p>
                    <a href="<?php echo site_url('/announce/') ?>" class="btn btn-default"><?php echo $this->lang->line('cancel'); ?></a>
                    <input type="hidden" name="pid" value="0" />
                </div>
            </form>
        </div>

    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
