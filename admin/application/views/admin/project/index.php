<!-- Content Wrapper. Contains page content -->

<div class="content-wrapper">
  <section class="content-header">
    <h1>
      全てのプロジェクト一覧
    </h1>
    <ol class="breadcrumb">
      <li><a href="/"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active"><a href="<?php echo site_url('/project')?>">全てのプロジェクト一覧</a></li>
    </ol>
  </section>
  <section class="content">
    <?php if ($this->session->flashdata('msg')) { ?>
        <div class="alert alert-success" id="success-alert">
            <button type="button" class="close" data-dismiss="alert"><i class="fa fa-times"></i></button>
            <strong><?php echo $this->lang->line('success'); ?></strong>
            <?php echo $this->session->flashdata('msg'); ?>
        </div>
    <?php } ?>
    <?php if ($this->session->flashdata('error')) { ?>
      <div class="alert alert-danger" id="success-alert">
          <button type="button" class="close" data-dismiss="alert"><i class="fa fa-times"></i></button>
          <strong><?php echo $this->lang->line('error'); ?></strong>
          <?php echo $this->session->flashdata('error'); ?>
      </div>
    <?php } ?>
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="FrmSearch">
            <form class="navbar-form" role="search" method="GET" action="/project/searchProject">
              <div class="input-group add-on">
                <input class="form-control" placeholder="Search" value="<?php if(isset($search)){ echo $search; } ?>" name="srch-term" id="srch-term" type="text">
                <div class="input-group-btn">
                  <button class="btn btn-default" id="btn-srch" type="submit"><i class="glyphicon glyphicon-search"></i></button>
                </div>
              </div>
            </form>
          </div>
          <form id="frmMain" method="POST" action="/project/action">
            <div class="box-header">
              <h3 class="box-title">&nbsp;</h3>
              <div class="box-tools">
                  <div class="btn-group pull-right">
                    <a class="btn btn-sm btn-primary " href="<?php echo site_url('/project/add') ?>"><i class="fa fa-plus"></i> 新規作成</a>
                    <a id="bulk-delete" class="btn btn-sm btn-danger " data-title="選択されたものを削除しますか。" data-btn-ok-label="<?php echo $this->lang->line('yes'); ?>"  data-btn-cancel-label="<?php echo $this->lang->line('no'); ?>"  data-toggle="confirmation" data-placement="left" data-singleton="true"><i class="fa fa-trash-o"></i> 削除</a>
                    <input type="hidden" id="hidAction" name="hidAction" value="" />
                  </div>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
              <?php
                if(isset($by) && $by === 'DESC'){
                  $order = 'ASC';
                }else{
                  $order = 'DESC';
                }
              ?>
              <table class="table table-bordered table-hover" id="tableProject">
                <tr>
                  <th style="width: 20px"><input type="checkbox" class="minimal checkth" ></th>
                  <th style="width: 200px">
                    <?php if(isset($search)){ ?>
                      <a href="/project/searchProject/project_name/<?php echo $order; ?>/?srch-term=<?php echo $search; ?>">プロジェクト名</a></th>
                    <?php }else{ ?>
                      <a href="/project/index/project_name/<?php echo $order; ?>">プロジェクト名</a></th>
                    <?php } ?>
                    <th style="width: 150px">
                      <?php if(isset($search)){ ?>
                        <a href="/project/searchProject/project_type/<?php echo $order; ?>/?srch-term=<?php echo $search; ?>">タイプ</a></th>
                      <?php }else{ ?>
                        <a href="/project/index/project_type/<?php echo $order; ?>">タイプ</a></th>
                      <?php } ?>
                  <th style="width: 120px;">
                    <?php if(isset($search)){ ?>
                      <a href="/project/searchProject/goal_amount/<?php echo $order; ?>/?srch-term=<?php echo $search; ?>">目標金額</a></th>
                    <?php }else{ ?>
                      <a href="/project/index/goal_amount/<?php echo $order; ?>">目標金額</a></th>
                    <?php } ?>
                  <th style="width: 120px;">
                    <?php if(isset($search)){ ?>
                      <a href="/project/searchProject/collected_amount/<?php echo $order; ?>/?srch-term=<?php echo $search; ?>">現在の支援総額</a></th>
                    <?php }else{ ?>
                      <a href="/project/index/collected_amount/<?php echo $order; ?>">現在の支援総額</a></th>
                    <?php } ?>
                  <th style="width: 120px; text-align: center;">
                    <?php if(isset($search)){ ?>
                      <a href="/project/searchProject/username/<?php echo $order; ?>/?srch-term=<?php echo $search; ?>">ファンディングの企画者</a></th>
                    <?php }else{ ?>
                      <a href="/project/index/username/<?php echo $order; ?>">ファンディングの企画者</a></th>
                    <?php } ?>
                  <th>
                    <?php if(isset($search)){ ?>
                      <a href="/project/searchProject/now_count/<?php echo $order; ?>/?srch-term=<?php echo $search; ?>">支援者数</a></th>
                    <?php }else{ ?>
                      <a href="/project/index/now_count/<?php echo $order; ?>">支援者数</a></th>
                    <?php } ?>
                  <th>
                    <?php if(isset($search)){ ?>
                      <a href="/project/searchProject/active/<?php echo $order; ?>/?srch-term=<?php echo $search; ?>">ステータス </a></th>
                    <?php }else{ ?>
                      <a href="/project/index/active/<?php echo $order; ?>">ステータス </a></th>
                    <?php } ?>
                    <th style="width: 90px; text-align: center;">
                      <?php if(isset($search)){ ?>
                        <a href="/project/searchProject/status_all_in/<?php echo $order; ?>/?srch-term=<?php echo $search; ?>">募集方式 </a></th>
                      <?php }else{ ?>
                        <a href="/project/index/status_all_in/<?php echo $order; ?>">ステータス </a></th>
                      <?php } ?>
                  <th>
                    <?php if(isset($search)){ ?>
                      <a href="/project/searchProject/collection_start_date/<?php echo $order; ?>/?srch-term=<?php echo $search; ?>">開始</a></th>
                    <?php }else{ ?>
                      <a href="/project/index/collection_start_date/<?php echo $order; ?>">開始</a></th>
                    <?php } ?>
                    <a href="/project/index//<?php echo $order; ?>"></a></th>
                  <th>
                    <?php if(isset($search)){ ?>
                      <a href="/project/searchProject/collection_end_date/<?php echo $order; ?>/?srch-term=<?php echo $search; ?>">完了</a></th>
                    <?php }else{ ?>
                      <a href="/project/index/collection_end_date/<?php echo $order; ?>">完了</a></th>
                    <?php } ?>
                  <th style="width: 80px"></th>
                </tr>
                  <?php if ($data['total'] > 0) { ?>
                    <?php foreach ($data['project'] as $key => $project) { ?>
                      <tr style="cursor: pointer;" >
                        <a href="/project/edit/<?php echo $project->id ?>"></a>
                        <td><input type="checkbox" class="minimal checkitem" name="val[]" value="<?php echo $project->id ?>" ></td>
                        <td>
                          <a class="comment" style="color:black" href="/project/edit/<?php echo $project->id ?>"><?php
                              echo $project->project_name;
                          ?></a>
                        </td>
                        <td>
                            <a style="color:black" href="/project/edit/<?php echo $project->id ?>">
                                <?php if($project->project_type === '0') { echo "普通のプロジェクト"; } else { echo "ファンクラブ"; } ?>
                            </a>
                        </td>
                        <td style="text-align:center;">
                            <a style="color:black" href="/project/edit/<?php echo $project->id ?>">
                                <?php if($project->project_type === '1'){ echo "--------------"; } else { echo number_format($project->goal_amount)."円"; }?>
                            </a>
                        </td>
                        <td style="text-align:center;">
                            <a style="color:black" href="/project/edit/<?php echo $project->id ?>">
                                <?php if($project->project_type === '1'){ echo "--------------"; } else { echo number_format($project->collected_amount)."円"; }?>
                            </a>
                        </td>
                        <td>
                            <a style="color:black" href="/project/edit/<?php echo $project->id ?>">
                                <?php echo $project->username  ?>
                            </a>
                        </td>
                        <td>
                            <a style="color:black" href="/project/edit/<?php echo $project->id ?>">
                                <?php echo $project->now_count ?>
                            </a>
                        </td>

                        <td>
                            <a style="color:black" href="/project/edit/<?php echo $project->id ?>">
                                <?php
                                    if($project->project_type === '0'){
                                        if($project->active === 'yes'){
                                            if($project->public === false){
                                                echo '<a class="text-success">公開中</a>';
                                            }else{
                                                echo '<a class="text-danger">終了</a>';
                                            }
                                            // echo '公開';
                                        }else if($project->active =='no'){
                                            echo '<a class="text-warning">申請中</a>';
                                        }else if($project->active =='blk'){
                                            echo '<a class="text-danger">ブロック済</a>';
                                        }else if($project->active =='cls'){
                                            echo '<a class="text-danger">途中終了</a>';
                                        }
                                    }else{
                                        if($project->active === 'yes'){
                                            if($project->status_fanclub === 'progress'){
                                                echo '<a class="text-success">公開中</a>';
                                            }else if($project->status_fanclub === 'wait'){
                                                echo '<a class="text-warning">解散承認待ち</a>';
                                            }else if($project->status_fanclub === 'stop'){
                                                echo '<a class="text-danger">解散済み</a>';
                                            }
                                            // echo '公開';
                                        }else if($project->active =='no'){
                                            echo '<a class="text-warning">申請中</a>';
                                        }else if($project->active =='blk'){
                                            echo '<a class="text-danger">解散済み</a>';
                                        }else if($project->active =='cls'){
                                            echo '<a class="text-danger">解散済み</a>';
                                        }
                                    }
                                ?>
                            </a>
                        </td>
                        <td>
                            <a style="color:black" href="/project/edit/<?php echo $project->id ?>">
                                <?php if($project->project_type === '1'){ echo "-------"; } else { if($project->status_all_in === '1'){ echo "All-in"; }else {echo "All-or-nothing";} }?>
                            </a>
                        </td>
                        <td>
                            <a style="color:black" href="/project/edit/<?php echo $project->id ?>">
                                <?php echo $project->collection_start_date?>
                            </a>
                        </td>
                        <td style="text-align:center;">
                            <a style="color:black" href="/project/edit/<?php echo $project->id ?>">
                                <?php if($project->project_type === '1'){ echo "--------------"; } else { echo $project->collection_end_date; }?>
                            </a>
                        </td>
                        <!-- <td><input type="number" name="sort[]" min="0" oninput="validity.valid||(value='');" value="<?php// echo $project->order ?>" class="sorting-input"/></td> -->
                        <td>
                            <a href="/project/edit/<?php echo $project->id; ?>" class="btn btn-xs btn-primary"><i class="fa fa-edit"></i></a>
                            <a href="/project/delete/<?php echo $project->id; ?>" class="btn btn-xs btn-danger" data-title="このプロジェクトを削除しますか。" data-btn-ok-label="<?php echo $this->lang->line('yes'); ?>"  data-btn-cancel-label="<?php echo $this->lang->line('no'); ?>" data-toggle="confirmation2" data-placement="left" data-singleton="true"><i class="fa fa-trash-o"></i></a>
                        </td>
                      </tr>
                    <?php } ?>
                  <?php } else { ?>
                      <tr>
                        <td colspan="10" class="text-center"><?php echo $this->config->item('no_data')?></td>
                      </tr>
                  <?php }?>
              </table>
            </div>
            <div class="box-footer clearfix">
                <?php
                  if(isset($search)){
                    echo custom_paginationGET('/project/searchProject/'.$sort.'/'.$by.'/', $search, $data['total'],10);
                  }else{
                    echo custom_pagination('/project/index/'.$sort.'/'.$by.'/', $data['total'],10);
                  }
                ?>
            </div>
          </form>
            <!-- /.box-body -->
        </div>
      </div>
    </div>
  </section>
<!-- /.content -->
</div>
<!-- /.content-wrapper -->
