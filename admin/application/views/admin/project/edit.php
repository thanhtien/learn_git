<!-- Content Wrapper. Contains page content -->
<style >
  .pd-banner .pannel .pannel-body .pannel-content .row .row-process-bar:after{
    <?php if(ceil(($project->collected_amount/$project->goal_amount)*100) === floatval(0)){ ?>
        content: "<?php echo ceil(($project->collected_amount/$project->goal_amount)*100); ?>%";
        width: calc(<?php echo ceil(($project->collected_amount/$project->goal_amount)*100); ?>% - 15px);
        background: #ededed;
    <?php }else if(ceil(($project->collected_amount/$project->goal_amount)*100) >= floatval(100)){ ?>
        content: "<?php echo ceil(($project->collected_amount/$project->goal_amount)*100); ?>%";
        width: calc(103% - 15px);
    <?php }else{ ?>
        content: "<?php echo ceil(($project->collected_amount/$project->goal_amount)*100); ?>%";
        width: calc(<?php echo ceil(($project->collected_amount/$project->goal_amount)*100); ?>% - 15px);
    <?php } ?>
  }
</style>
<script>
    function loadGrid(projectId) {

        $.ajax({
            url: '/project/get_edit_data/'+projectId+'',
            type: 'GET'
        }).done(function(result) {
            // console.log(result);
            $('#tabs-2').html("...");
            $('#tabs-2').html(result);
            $(".comment_return").shorten({
                "showChars" : 42,
            });
            $loading.hide();
        }).fail(function(error) {
            $loading.hide();
        });
    }
    function onClickDown( projectIdMore){
        $loading.show();
       console.log(projectIdMore);
       ///project/updateshoworder/20?is_up=1
        $.ajax({
            url: '/project/updateshoworder/'+projectIdMore+'?is_up=0',
            type: 'GET'
        }).done(function(result) {

            if(result.is_ok ==1){
                projectId = result.project_id;
                loadGrid(projectId);
            }
            $loading.hide();

        }).fail(function(error) {
            $loading.hide();
        });
    }
    function onClickUp(projectIdMore){
        $loading.show();
        $.ajax({
            url: '/project/updateshoworder/'+projectIdMore+'?is_up=1',
            type: 'GET'
        }).done(function(result) {
            if(result.is_ok ==1){
                projectId = result.project_id;
                loadGrid(projectId);

            }
            $loading.hide();
        }).fail(function(error) {
            $loading.hide();
        });
    }
</script>

<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
        プロジェクト
    </h1>
    <ol class="breadcrumb">
      <li>
        <a href="/">
          <i class="fa fa-dashboard"></i>
            Home
        </a>
      </li>
      <li class="active"><a href="<?php echo site_url('/project')?>">プロジェクト</a></li>
    </ol>
  </section>
    <!-- Main content -->
    <section class="content">
        <?php if ($this->session->flashdata('msg')) { ?>
          <div
            class="alert alert-success"
            id="success-alert">
              <button
                type="button"
                class="close"
                data-dismiss="alert">
                <i class="fa fa-times"></i>
              </button>
              <strong>
                <?php echo $this->lang->line('success'); ?>
              </strong>
              <?php echo $this->session->flashdata('msg'); ?>
          </div>
        <?php } ?>
        <?php if ($this->session->flashdata('error')) { ?>
        <div
          class="alert alert-danger"
          id="success-alert">
            <button
              type="button"
              class="close"
              data-dismiss="alert">
              <i class="fa fa-times"></i>
            </button>
            <strong><?php echo $this->lang->line('error'); ?></strong>
            <?php echo $this->session->flashdata('error'); ?>
        </div>
        <?php } ?>

      <div class="box box-primary">
          <div class="modal fade " id="ModalPreView" role="dialog">
            <div class="modal-dialog modal-lg" style="width:1100px;">
              <!-- Modal content-->
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" style="color:red"><i class="fa fa-times-circle"></i></button>
                  <h4 class="modal-title"></h4>
                </div>
                <div class="modal-body">
                  <div class="preview">
                    <div class="pd-banner">
                        <div class="wraper">
                            <div class="pannel">
                                <p class="pannel-head"></p>
                                <div class="pannel-body">
                                    <div class="pannel-slider">
                                        <div class="cover_image_preview">
                                            <img width="100%" id="slider_img" src="<?php echo image_url(); ?>images/2018/default/noimage-01.png" alt="">
                                            <a class="click_image_preview" id="clickYoutube" href="#"></a>
                                            <iframe id="video" class="playvideo"  style=""width="420" height="315" src="" frameborder="0" allowfullscreen></iframe>
                                        </div>
                                    </div>
                                    <?php if($project_type === '0') {?>
                                        <div class="pannel-content">
                                            <div class="row row-1">
                                                <p class="row-label">現在の支援総額</p>
                                                <p class="row-number"><?php echo number_format($project->collected_amount); ?>円</p>
                                                <p class="row-process-bar">0%</p>
                                                <p class="row-note">目標金額は0円</p>
                                            </div>
                                            <div class="row row-2">
                                                <p class="row-label">支援者数</p>
                                                <p class="row-number"><?php echo $project->now_count; ?>人</p>
                                            </div>
                                            <div class="row row-3">
                                                <p class="row-label">募集終了まで残り</p>
                                                <p class="row-number">10日</p>
                                            </div>
                                            <p class="pannel-btn"><a style="cursor: pointer;">プロジェクトを支援する</a></p>
                                        </div>
                                    <?php }else { ?>
                                        <div class="pannel-content">
                                            <div class="row row-2">
                                                <p class="row-label">支援者数</p>
                                                <p class="row-number"><?php echo $project->now_count; ?>人</p>
                                            </div>
                                            <p class="pannel-btn"><a style="cursor: pointer;">プロジェクトを支援する</a></p>
                                            <p>このプロジェクトは、ファンクラブ方式(定額課金制)です。<br>
                                                課金間隔はプロジェクト毎に異なり、1ヵ月～12ヵ月となります。<br>
                                                課金期間はプロジェクトが中断、もしくはお客様がプロジェクトから退会されるまでとなります。 <br>
                                                選択した金額でパトロンになり、課金間隔毎に集まった金額がプロジェクトにファンディングされます。<br></p>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- BANNER -->
                    <div id="content">
                        <div class="wraper">
                            <div class="tab-area">
                                <ul style="margin-left: -40px;">
                                    <li id="tab_pr2" class="active">プロジェクト詳細</li>
                                    <li id="tab_pr1" >プロジェクト概要</li>
                                </ul>
                            </div>
                            <!-- .tab-area -->

                            <div class="pd-area  " id="tabPre-1">

                            </div>
                            <!-- #tab-1 -->
                            <div class="pd-area active" id="tabPre-2">
                                <div class="pd-box pd-box-1">
                                    <h4 class="pd-box-title">ファンディングの企画者について</h4>
                                    <div class="pd-box-content">
                                        <div class="pd-box-content-panel">
                                            <p class="pd-box-content-panel-avatar"><img id="img_user" width="100%" src="<?php echo image_url(); ?>images/2018/default/default-profile_01.png" alt=""></p>
                                            <div class="pd-box-content-panel-text-wraper">
                                                <p class="pd-box-text pd-box-text-name">氏名 ：氏名</p>
                                                <p class="pd-box-text pd-box-text-address">現在地  ：現在地 　</p>
                                                <p class="pd-box-text pd-box-text-job">職業 ：　職業</p>
                                            </div>
                                        </div>
                                        <div class="pd-box-content-intro">
                                            <p class="pd-box-content-intro-title">はじめにご挨拶：</p>
                                            <p class="pd-box-text"></p>
                                            <p class="pd-box-btn"><a style="cursor: pointer;">プロジェクト概要 ＞</a></p>
                                        </div>
                                    </div>
                                </div>
                                <div class="pd-box pd-box-2">
                                    <h4 class="pd-box-title">リターンを選ぶ</h4>

                                    <div class="return-list">
                                        <ul>
                                            <?php if(isset($project_return) && $project_return){
                                                foreach ($project_return as $key => $value) {
                                            ?>
                                            <li>
                                                <p class="return-list-img"><a href="#"><img style="width:100%;" src="<?php echo $value->thumnail; ?>" alt=""></a></p>
                                                <div class="return-list-content cover_relative">
                                                    <?php if($project_type === '0') { ?>
                                                        <p class="return-list-content-number"><?php echo number_format($value->invest_amount); ?>円</p>
                                                    <?php } else {?>
                                                        <p class="return-list-content-number"><?php echo number_format($value->invest_amount); ?>円/<?php if($project->number_month === '1') { echo "月"; }else { echo $project->number_month."月";} ?></p>
                                                    <?php } ?>
                                                    <p class="return-list-text"><?php echo $value->return_amount; ?></p>
                                                    <div class="return-list-status" style="    width: 470px;">
                                                        <p class="return-list-s return-list-s-1">支援者数：　<span><?php echo $value->now_count; ?></span></p>
                                                        <?php if(isset($value->schedule) && $value->schedule){ ?>
                                                            <p class="return-list-s return-list-s-2">お届け予定：<span><?php echo $value->schedule; ?></span></p>
                                                        <?php } ?>
                                                    </div>
                                                    <?php if(isset($value->max_count) && $value->max_count){ ?>
                                                        <p class="max_count">残り件数:<span><?php echo $value->max_count.'件'; ?></span></p>
                                                    <?php } ?>

                                                    <p class="return-list-btn"><a style="cursor:pointer;">このリターンを選択する</a></p>
                                                </div>
                                            </li>
                                            <?php
                                                }
                                            } ?>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <!-- #tab-2 -->
                        </div>
                    </div>
                    <!-- CONTENT -->
                  </div>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">閉じる</button>
                </div>
              </div>

            </div>
          </div>


        <div id="tabs">
          <input type="hidden" id="page_tab" name="" value="0">
          <ul>
            <li>
              <a href="#tabs-1">
                プロジェクト概要・詳細
              </a>
            </li>
            <li>
              <a href="#tabs-2">
                リターン作成
              </a>
            </li>
            <?php if($project->active === 'yes') {?>
                <li>
                  <a href="#tabs-3">
                    プロジェクト内容更新、リターン追加依頼
                  </a>
                </li>
            <?php } ?>
            <li>
              <a href="#tabs-4">
                支援一覧
              </a>
            </li>
            <?php if($project->project_type === '1' && $project->status_fanclub !== 'progress') {?>
                <li>
                  <a href="#tabs-5">
                    解散リクエスト
                  </a>
                </li>
            <?php } ?>
            <li>
              <a href="#tabs-6">
                企画者の情報
              </a>
            </li>
          </ul>
          <div id="tabs-1">
            <form role="form" action=""  id="demoForm" method="POST" enctype="multipart/form-data">
                <div class="nav-tabs-custom">
                  <div class="tab-content">
                    <div class="tab-pane active" id="tab_1">
                      <div class="box-body">
                        <div class="form-group col-sm-12">
                          <label for="name">
                            プロジェクト名（最大150文字）
                          </label>
                          <input
                            type="text"
                            class="form-control"
                            required
                            id="project_name"
                            value="<?php echo $project->project_name ?>"
                            name="project_name"
                            placeholder=""
                            maxlength="150">
                        </div>
                        <div class="form-group col-sm-12">
                          <label for="name">ユーザー名</label>
                          <br>
                          <select class="form-control col-sm-12" id="itemName" name="itemName" style="width:100%;">
                            <option class="form-control col-sm-12" value="<?php echo $project->user_id ?>"><?php echo $user_name->username ?></option>
                          </select>
                        </div>
                          <div class="form-group col-sm-12" id="form_img">
                            <label for="name">画像（サムネイル表示）</label>
                            <p>※画像のサイズは600px＊400pxで最高です。容量 2MBまで 。</p>
                            <input
                            type="file"
                            name="file"
                            id="profile-img">
                            <input
                              type="hidden"
                              name="hiden_image"
                              id="hiden_image"
                              value="<?php echo $project->thumbnail ?>">
                            <p id="announce" style="color:red" ></p>
                            <p style="text-align:center">
                              <img
                                src="<?php echo $project->thumbnail ?>"
                                width="600px"
                                id="profile-img-tag" />
                            </p>
                          </div>
                          <div class="form-group col-sm-12">
                            <label for="name">
                              はじめにご挨拶 （最大1000文字）
                            </label>
                            <textarea
                              class="form-control"
                              name="summary"
                              id="summary" maxlength="1000"><?php echo $project->description ?></textarea>
                          </div>
                          <div class="form-group">
                            <p class="form-group col-sm-12">※「動画のURL入力」か「画像アップロード」を選択して、該当のURLか、画像をアップロードしてください。</p>
                            <div class="form-check col-sm-2">
                              <input
                                class="form-check-input"
                                type="radio"
                                name="Radios"
                                id="Video"
                                <?php
                                  if($project->thumbnail_type == 1){
                                ?>
                                checked=""
                                <?php
                                  }
                                ?>
                                value="1"
                                >
                              <label
                                class="form-check-label"
                                for="Video">
                                動画のURL入力
                              </label>
                            </div>
                            <div class="form-check col-sm-2">
                              <input
                                class="form-check-input"
                                type="radio"
                                name="Radios"
                                id="Image"
                                <?php
                                  if($project->thumbnail_type ==0){
                                ?>
                                checked=""
                                <?php
                                  }
                                ?>
                                value="0">
                              <label class="form-check-label" for="Image">
                                画像アップロード
                              </label>
                            </div>
                            <div class="col-sm-12">
                              <div id="video">
                                <div class="col-sm-12">
                                  <label for="name">動画URL</label>
                                  <input type="text" class="form-control youtubevideo" id="video"  name="video" value="<?php echo $project->thumbnail_movie_code; ?>">
                                </div>
                              </div>
                              <div id="image_thumnail">
                                <p class="col-sm-12">※2～３枚の画像をアップロードしたら、スライダーで表示されるようになります。</p>
                                <p class="col-sm-12">※画像のサイズは600px＊400pxで最高です。容量 2MBまで 。</p>
                                <div class="form-group col-sm-4" id="form_img">
                                  <label for="name">画像 1</label>
                                  <input type="file" name="file1" id="profile_thumnail1">
                                  <input type="hidden" value="<?php echo $project->thumbnail_descrip1 ?>" name="hiden_image1" id="hiden_image1" >
                                  <p id="announce1" style="color:red" ></p>
                                  <p><img src="<?php echo $project->thumbnail_descrip1 ?>" width="300px"   id="profile_thumnail1-tag"/></p>
                                </div>
                                <div class="form-group col-sm-4" id="form_img">
                                  <label for="name">画像 2</label>
                                  <input type="file" name="file2" id="profile_thumnail2">
                                  <input type="hidden"value="<?php echo $project->thumbnail_descrip2 ?>" name="hiden_image2" id="hiden_image2" >
                                  <p id="announce2" style="color:red" ></p>
                                  <p><img src="<?php echo $project->thumbnail_descrip2 ?>" width="300px"   id="profile_thumnail2-tag"/></p>
                                </div>
                                <div class="form-group col-sm-4" id="form_img">
                                  <label for="name">画像 3</label>
                                  <input type="file" name="file3" id="profile_thumnail3">
                                  <input type="hidden" value="<?php echo $project->thumbnail_descrip3 ?>" name="hiden_image3" id="hiden_image3" >
                                  <p id="announce3" style="color:red" ></p>
                                  <p><img src="<?php echo $project->thumbnail_descrip3 ?>" width="300px"   id="profile_thumnail3-tag"/></p>
                                </div>
                              </div>
                            </div>
                          </div>
                          <input type="hidden" value="<?php echo $project->project_type; ?>" name="project_type" >
                          <div class="form-group ">
                            <div class="col-sm-6">
                              <Br />
                              <label for="name">プロジェクトのカテゴリ</label>
                              <select class="form-control form-control-sm" name="category" id="category">
                                <?php foreach ($category as $key => $value) { ?>
                                  <option
                                    value="<?php echo $value->id?>"
                                    <?php if($value->id == $project->category_id){ ?>
                                    selected
                                    <?php }?> >
                                      <?php echo $value->name; ?>
                                  </option>
                                  <?php } ?>
                              </select>
                            </div>
                            <div class="col-sm-6">
                                <?php if($project->project_type === '1') { ?>
                                    <label for="name">
                                      <br />
                                      定期課金間隔
                                    </label>
                                    <select class="form-control" id="number_month" name="number_month" >
                                        <?php for ($i=1; $i <=12 ; $i++) { ?>
                                            <option value="<?php echo $i; ?>" <?php if($i === (int)$project->number_month) { echo "selected";} ?>><?php echo $i; ?>カ月</option>
                                        <?php } ?>
                                    </select>
                                <?php }else { ?>
                                    <label for="name">
                                      <br />
                                      募集終了日
                                    </label>
                                    <div
                                      class="input-group date"
                                      data-date-format="datepicker-mm-yyyy">
                                      <input
                                        type="text"
                                        autocomplete="off"
                                        class="form-control"
                                        <?php
                                          $date=strtotime($project->collection_end_date);
                                        ?>
                                        placeholder="yyyy-mm-dd"
                                        name="date"
                                        id="date"
                                        value="<?php echo date("Y-m-d",$date); ?>"
                                        >
                                      <div class="input-group-addon" >
                                        <span class="glyphicon glyphicon-th"></span>
                                      </div>
                                    </div>
                                <?php } ?>
                            </div>
                          </div>
                                <?php if($project->project_type === '0'){ ?>
                                    <div class="form-group">
                                      <div class="form-check-status col-sm-3">
                                        <input class="form-check-input" type="radio" id="all_in" name="status_all_in"
                                            <?php
                                                if($project->status_all_in === '1'){
                                            ?>
                                            checked=""
                                            <?php
                                                }
                                            ?>
                                            value="1"
                                        >
                                        <label class="form-check-label" for="all_in">
                                            All-in
                                        </label>
                                      </div>
                                      <div class="form-check-status col-sm-3">
                                        <input class="form-check-input" type="radio" id="allornothing" name="status_all_in"
                                        <?php
                                            if($project->status_all_in === '0'){
                                        ?>
                                        checked=""
                                        <?php
                                            }
                                        ?>
                                        value="0">
                                        <label class="form-check-label" for="allornothing">
                                            All-or-nothing
                                        </label>
                                      </div>
                                    </div>
                                    <div class="form-group col-sm-12">
                              <br />
                              <label for="name">
                                目標金額
                              </label>
                              <input
                                type="text"
                                class="form-control money"
                                required=""
                                id="goal_amount"
                                autocomplete="off"
                                value="<?php echo $project->goal_amount; ?>"
                                name="goal_amount"
                                onkeypress='validate(event)'>
                            </div>

                            <?php } ?>
                            <div class="form-group col-sm-12">
                              <label for="name">
                                プロジェクト概要
                              </label>
                              <textarea
                                class="form-control ckeditor"
                                name="description"
                                id="description"><?php echo $project->project_content ?></textarea>
                            </div>
                        </div>
                    </div>
                  </div>
                </div>
                <input type="hidden" name="active" value="<?php echo $project->active; ?>">
                <input type="hidden" name="status_fanclub" value="<?php echo $project->status_fanclub; ?>">

                <div class="box-footer">
                  <button
                    type="submit"
                    name="save"
                    class="btn btn-primary"
                    value="1">
                      <?php echo $this->lang->line('save'); ?>
                  </button>

                  <?php if($project->active == 'yes'){ ?>
                    <p class="btn btn-danger" data-toggle="modal" data-target="#CloseProject">終了</p>
                    <?php }else if($project->active == 'no'){?>
                      <button type="submit" name="public" class="btn btn-danger" id="accept" value="1" >
                          公開
                      </button>
                  <?php }else if($project->active == 'blk' || $project->active == 'cls'){?>
                      <button type="submit" name="public" disabled class="btn btn-danger" id="accept" value="1" >
                          公開
                      </button>
                  <?php } ?>
                  <div class="modal fade" id="CloseProject" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          </button>
                        </div>
                        <div class="modal-body">
                            <p style="text-align:center">このプロジェクトを終了しますか。<br />終了した後で、復帰することができませんので、ご注意ください。</p>
                        </div>
                        <div class="modal-footer">
                            <button
                              type="submit"
                              name="public"
                              class="btn btn-danger"
                              id="accept"
                              value="2"
                            >
                                はい
                            </button>

                          <button type="button" class="btn btn-secondary" data-dismiss="modal">いいえ </button>
                        </div>
                      </div>
                    </div>
                  </div>
                  <p id="setPreview" class="btn btn-danger" data-toggle="modal" data-target="#ModalPreView" >プレビュー</p>
                  <?php if($project_type === '0') { ?>
                      <input type="hidden" id="hiddenPreview" value="<?php echo ceil(($project->collected_amount/$project->goal_amount)*100); ?>%">
                  <?php } ?>
                  <input type="hidden" id="pid" value="<?php echo $project->id; ?>">
                  <a
                    href="<?php echo site_url('/project/') ?>"
                    class="btn btn-default">
                      <?php echo $this->lang->line('cancel'); ?>
                  </a>
                  <?php
                    if($this->session->flashdata('tab')){ ?>
                      <input type="hidden" name="tab" id="tab" value="<?php echo $this->session->flashdata('tab');?>" />
                  <?php }else{ ?>
                      <input type="hidden" name="tab" id="tab" value="0" />
                  <?php } ?>
                <!-- <?php echo $this->session->flashdata('tab');?> -->
                </div>
            </form>
          </div>
          <div id="tabs-2">
           <?php require_once "ajax_backing.php"?>
          </div>
          <?php if($project->active === 'yes') {?>
            <div id="tabs-3">
                <div class="box-body no-padding">
                  <table class="table table-bordered table-hover" id="tableProjectReturnActive">
                      <tr>
                          <th >依頼名</th>
                          <th >内容（理由などについて）</th>
                          <th >ステータス</th>
                          <th style="width: 80px"></th>
                      </tr>
                      <?php if (count($ListBackedProjectEdit) >= 1) {   ?>
                        <?php foreach ($ListBackedProjectEdit as $key => $value) { ?>
                            <tr class="trTableEditProjectActive"  style="cursor:pointer" id="<?php echo $project->id."_".$value['list_status'] ?>">
                                <td >
                                    <p  class="comment"><?php echo $value['title']; ?></p>
                                </td>
                                <td >
                                    <p  class="list_editActive comment_return"><?php echo $value['message']; ?></p>
                                </td>
                                <td>
                                    <?php if($value['status'] === '0') {
                                        echo "<p class='text-warning'>承認待ち</p>";
                                    }else if($value['status'] === '1') {
                                        echo "<p class='text-danger'>承認却下</p>";
                                    }else if($value['status'] === '2') {
                                        echo "<p class='text-success'>承認済み</p>";
                                    } ?>
                                </td>

                                <td style="text-align:center;">
                                    <a
                                    target="_blank"
                                    href="/projectreturn/listActive/?project_id=<?php echo $project->id; ?>&status=<?php echo $value['list_status']; ?>"
                                    style="color:white"
                                    class="btn btn-xs btn-primary stopPopUpTarget">
                                    <i class="fa fa-edit"></i>
                                </a>
                            </td>
                            </tr>
                          <?php } ?>
                        <?php } else { ?>
                            <tr>
                                <td
                                colspan="5"
                                class="text-center">
                                    <?php echo $this->config->item('no_data')?>
                                </td>
                            </tr>
                        <?php }?>
                    </table>
                </div>
            </div>
        <?php } ?>
        <div id="tabs-4">
            <div class="box-body no-padding">
              <!-- Button trigger modal -->


              <table class="table table-bordered table-hover">
                <tr>
                  <th>支援者名</th>
                  <th>支援日</th>
                  <th style="width:170px;">支援形式</th>
                  <th style="width:180px;">金額</th>
                  <th style="width:100px;">ステータス</th>
                  <!-- <th style="width:50px;"></th> -->
                </tr>
                <?php if ($backedProject['total'] > 0) { ?>
                  <?php foreach ($backedProject['backed'] as $key => $backed) { ?>
                    <tr id="table_pop_up" style="cursor:pointer"  data-toggle="modal" data-target="#exampleModalCenter<?php echo $backed->id; ?>">
                      <div class="modal fade" id="exampleModalCenter<?php echo $backed->id; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                        <div class="modal-dialog modal-lg" role="document">
                          <div class="modal-content">
                            <div class="modal-header">
                              <h2 class="modal-title" id="exampleModalCenterTitle<?php echo $backed->id; ?>"></h2>
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              </button>
                            </div>
                            <div class="modal-body">
                              <div class="return-list">
                                  <p class="return-list-img"><a href="#"><img width="100%" src="<?php echo $backed->thumnail; ?>" alt=""></a></p>
                                  <div class="return-list-content">
                                      <p class="return-list-content-number"><?php echo number_format($backed->invest_amount); ?>円</p>
                                      <p class="return-list-content-text"><?php echo $backed->backingReturn; ?></p>
                                      <div class="return-list-status">
                                          <p class="return-list-s return-list-s-1">支援者数：　<span><?php echo $backed->now_count; ?>人</span></p>
                                          <p class="return-list-s return-list-s-2">お届け予定：<span><span><?php if(isset($backed->schedule) && $backed->schedule) { echo $backed->schedule;} else {echo "";}?></span></p>
                                      </div>
                                  </div>
                              </div>
                            </div>
                            <div class="modal-footer">
                              <button type="button" class="btn btn-secondary" data-dismiss="modal">閉じる</button>
                            </div>
                          </div>
                        </div>
                      </div>
                      <td><?php echo $backed->username; ?></td>
                      <td><?php echo ($backed->created)  ?></td>
                      <td><?php if($backed->type === 'Bank'){
                        echo "銀行口座";
                      }else if($backed->type === 'Credit'){
                        echo "クレジットカード";
                      }   ?>
                      </td>

                      <td id="<?php echo $backed->id ?>">
                         <a  id="showPopUp"><?php echo number_format($backed->invest_amount)  ?> 円</a>
                      </td>
                      <td style="text-align:center;"><?php if($backed->status === 'succeeded' && $backed->type ==='Credit') {
                        echo "<p style='color:green;'>支援済み</p>";
                      } else if($backed->status === 'succeeded' && $backed->type ==='Bank'){
                        echo "<p style='color:green;'>振込済み</p>";
                      } else if($backed->status === 'unsuccess') {
                        echo "<p style='color:red;'>銀行振込の承認待ち</p>";
                      }else{
                        echo "返金済み";
                      }  ?> </td>
                      <td style="text-align:center">
                            <?php if($backed->type === 'Bank'){?>
                                <a
                                    href="/project/editDonate/<?php echo $backed->id; ?>"
                                    style="color:white"
                                    class="btn btn-xs editDonate btn-primary">
                                    <i class="fa fa-edit"></i>
                                </a>
                            <?php }?>
                      </td>
                    </tr>
                  <?php } ?>
                <?php } else { ?>
                    <tr>
                      <td colspan="5" class="text-center"><?php echo $this->config->item('no_data')?></td>
                    </tr>
                <?php }?>


              </table>
            </div>
            <div class="box-footer clearfix">

                <?php
                    echo customPaginationProjectReturn('/project/edit/'.$idProject.'/', $backedProject['total'],10);
                ?>
            </div>
            <div class="modal fade" id="refunds" role="dialog">
              <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content" style="margin-top:200px;">
                  <div class="modal-header">
                    <!-- <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Thông Báo</h4> -->
                  </div>
                  <div class="modal-body">
                    <p>すみませんが、返金を行う前に、本プロジェクトを非公開することが必要です。</p>
                  </div>
                  <div class="modal-footer">
                    <a href="<?php echo base_url('/project/edit/'.$project->id); ?>" class="btn btn-default" >はい </a>
                    <button type="button" class="btn btn-default" data-dismiss="modal">キャンセル</button>
                  </div>
                </div>

              </div>
            </div>


            <div style="overflow:hidden;">
                <div id="Child_tabs" style="width:500px;float:left">
                    <input type="hidden" id="page_tab" name="" value="0">
                    <ul>
                        <li>
                            <a href="#Child_tabs-1">
                                返金
                            </a>
                        </li>
                        <li>
                            <a href="#Child_tabs-2">
                                振込
                            </a>
                        </li>
                    </ul>
                    <div id="Child_tabs-1">
                        <form role="form" action="/project/refunds"  method="POST" enctype="multipart/form-data">
                          <p >支援者へ返金を行いますか。</p>

                          <?php if($project->active === 'yes'){?>
                            <p style="cursor:pointer;float:right" class="btn btn-primary" id="refunds" data-toggle="modal" data-target="#refunds">返金</p>
                          <?php }else{ ?>
                            <button
                              type="submit"
                              name="refunds"
                              class="btn btn-primary"
                              style="float:right"
                              value="1"
                              <?php if(isset($project->status) && $project->status === 'refunded'){?>
                                <?php echo "disabled=''"; ?>
                              <?php } ?>
                              id="refunds_load"
                              >
                                返金
                            </button>
                          <?php }?>
                          <input type="hidden" name="pid" value="<?php echo $project->id; ?>">
                        </form>
                    </div>
                    <div id="Child_tabs-2">
                        <form role="form" action="/project/SendMoneyOwnerProject/<?php echo $project->id; ?>"  method="POST" enctype="multipart/form-data">
                          <p >企画者に振込を行いましたか。</p>

                              <button
                                type="submit"
                                 style="float:right"
                                name="refunds"
                                class="btn btn-primary"
                                value="1"
                                <?php if(isset($project->status) && $project->status === 'refunded'){?>
                                  <?php echo "disabled=''"; ?>
                                <?php } ?>
                                >
                                  はい
                              </button>
                          <input type="hidden" name="pid" value="<?php echo $project->id; ?>">
                        </form>
                    </div>
                </div>


              <div style="width:350px;padding: 10px 5px;float:right; text-align:right; border:1px solid; padding">
                <p style="width:340px;float:left; text-align:left">合計<span style="margin-left: 75px;">:</span>  <span style="color:green;margin-left: 15px;"><?php echo number_format($total)  ?>円</span></p>
                <p style="width:340px;float:left; text-align:left">目標金額<span style="margin-left: 44px;">:</span> <span style="margin-left: 15px;"><?php echo number_format($goal_amount)  ?>円</span> </p>
                <p style="width:340px;float:left; text-align:left">残り<span style="margin-left: 74px;">:</span>
                  <?php

                    if($goal_amount - $total > 0){
                      echo '<span style="color:red;margin-left: 15px;">-'. number_format($goal_amount - $total). '円</span>';
                    }else{
                      echo '<span style="color:#00FFFF;margin-left: 15px">+'. number_format($total - $goal_amount).'円</span>';
                    }
                  ?>

                </p>
                <p style="clear: both;display:none;"></p>
              </div>
            </div>

          </div>
          <?php if($project->project_type === '1' && $project->status_fanclub !== 'progress') {?>
              <div id="tabs-5">
                  <div class="modal fade " id="ModalMailRefuse" role="dialog">
                      <div class="modal-dialog" >
                          <!-- Modal content-->
                          <div class="modal-content">
                              <form id="SendMailRefuse" action="/project/refuseFanClub/<?php echo $project->id; ?>" method="post">
                                  <div class="modal-header">
                                      却下の場合へのメール送信
                                  </div>
                                  <div class="modal-body">

                                      <div class="preview">
                                          <!-- BANNER -->
                                          <div id="content">
                                              <div class="form-group">
                                                  <p>【<?php echo $project->project_name; ?>】を解散する申請が却下されました。</p>
                                              </div>
                                              <div class="form-group">
                                                  <p>却下理由</p>
                                                  <textarea name="message" class="form-control" style="resize:none;" rows="8" cols="80"></textarea>
                                              </div>
                                          </div>
                                          <!-- CONTENT -->
                                      </div>
                                  </div>
                                  <div class="modal-footer">
                                      <button type="submit" id="btnrefuse" class="btn btn-primary" form="SendMailRefuse" value="Submit">送信</button>
                                      <button type="button" class="btn btn-default" data-dismiss="modal">キャンセル</button>
                                  </div>
                              </form>

                          </div>

                      </div>
                  </div>
                  <form id="frmMain" method="POST" action="/project/acceptCloseFanClub/<?php echo $project->id ?>">

                  <div class="box-body no-padding">
                      <div id="content">
                          <div class="wraper">
                              <div class="pd-area active" id="tabPre-2">
                                  <div class="pd-box pd-box-2">
                                      <h4 class="pd-box-title fix_space">依頼名</h4>
                                      <p class="message_project"><?php echo $project->title_fanclub; ?></p>
                                      <h4 class="pd-box-title fix_space">内容（理由などについて）</h4>
                                      <!-- <pre class="message_project premessage" >'ads'</pre> -->
                                      <textarea disabled name="name" id="previewArea" class="premessage" ><?php echo $project->message_fanclub; ?></textarea>

                                  </div>
                              </div>
                              <!-- #tab-2 -->
                          </div>
                      </div>
                  </div>
                  <div class="box-footer">
                      <button type="submit" <?php if($project->status_fanclub !== 'wait'){ echo "disabled";} ?>  name="save" class="btn btn-primary" value="1">承認</button>
                      <p id="setPreview"   class="btn btn-danger" data-toggle="modal" <?php if($project->status_fanclub === 'wait'){?> data-target="#ModalMailRefuse" <?php }else { echo "disabled";} ?> > 却下</p>
                      <a <?php $this->session->set_flashdata('tab', '2'); ?> href="<?php echo site_url('project/')?>" class="btn btn-default">戻る</a>
                      <input type="hidden" name="ActiveProjectReturn" value="<?php echo $project->id ?>" />
                  </div>
                  </form>
              </div>
          <?php } ?>
          <div id="tabs-6">
              <div class="nav-tabs-custom">
                  <div class="tab-content" style="overflow:hidden">
                      <div class="tab-pane active col-sm-6" id="tab_1">
                          <div class="box-body">
                              <div class="form-group">
                                  <label for="username"><?php echo $this->lang->line('username') ?></label>
                                  <input type="text" class="form-control"  id="username" name="username" disabled="" value="<?php echo $users->username ?>">
                              </div>
                              <div class="form-group">
                                  <label for="email"><?php echo $this->lang->line('email') ?> </label>
                                  <input type="text" class="form-control"  id="email" name="email" disabled="" value="<?php echo $users->email ?>">
                              </div>
                              <div class="form-group">
                                  <label for="username">名前</label>
                                  <input type="text" class="form-control"  id="username" name="username" disabled="" value="<?php if(isset($address_user->name_return) && $address_user->name_return) {echo $address_user->name_return;} else { echo "";}  ?>">
                              </div>
                              <div class="form-group">
                                  <label for="email">電話番号</label>
                                  <input type="text" class="form-control"  id="username" name="username" disabled="" value="<?php if(isset($address_user->phone) && $address_user->phone) {echo $address_user->phone;} else { echo "";}  ?>">
                              </div>
                              <div class="form-group">
                                  <label for="email">郵便番号</label>
                                  <input type="text" class="form-control"  id="username" name="username" disabled="" value="<?php if(isset($address_user->postcode) && $address_user->postcode) {echo $address_user->postcode;} else { echo "";}  ?>">
                              </div>
                              <div class="form-group">
                                  <label for="email">住所</label>
                                  <input type="text" class="form-control"  id="username" name="username" disabled="" value="<?php if(isset($address_user->address_return) && $address_user->address_return) {echo $address_user->address_return;} else { echo "";}  ?>">
                              </div>
                              <div class="form-group" >
                                <label >画像</label>
                                <p style="text-align: left;"><img src="<?php echo $users->profileImageURL ?>" id="profile-img-tag"/ width="150px" alt=""></p>
                              </div>
                          </div>
                          <!-- /.box-body -->
                      </div>
                      <div class="tab-pane active col-sm-6">

                          <div class="box-body">
                              <div class="form-group">
                                  <label for="username">銀行名</label>
                                  <input type="text" class="form-control"  id="bank_name" name="bank_name" disabled="" value="<?php echo $users->bank_name ?>">
                              </div>
                              <div class="form-group">
                                  <label for="email">支店名</label>
                                  <input type="text" class="form-control"  id="bank_branch" name="bank_branch" disabled="" value="<?php echo $users->bank_branch ?>">
                              </div>
                              <div class="form-group">
                                  <label for="username">口座種類</label>
                                  <input type="text" class="form-control"  id="bank_type" name="bank_type" disabled="" value="<?php if(isset($users->bank_type) && $users->bank_type) {echo $users->bank_type;} else { echo "";}  ?>">
                              </div>
                              <div class="form-group">
                                  <label for="email">口座番号</label>
                                  <input type="text" class="form-control"  id="bank_number" name="bank_number" disabled="" value="<?php if(isset($users->bank_number) && $users->bank_number) {echo $users->bank_number;} else { echo "";}  ?>">
                              </div>
                              <div class="form-group">
                                  <label for="email">口座カナ名義</label>
                                  <input type="text" class="form-control"  id="bank_owner" name="bank_owner" disabled="" value="<?php if(isset($users->bank_owner) && $users->bank_owner) {echo $users->bank_owner;} else { echo "";}  ?>">
                              </div>

                          </div>
                          <!-- /.box-body -->
                      </div>
                  </div>
                  <!-- /.tab-content -->
              </div>

          </div>
        </div>
      </div>
    </section>
</div>
