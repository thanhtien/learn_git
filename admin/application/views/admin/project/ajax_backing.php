<div class="box-body no-padding trTableProjectReturn">
    <table class="table table-bordered table-hover" id="tableProjectReturn">
        <tr>
            <th style="width: 200px">Name</th>
            <th style="width: 150px">支援額</th>
            <th>リターン品・説明文</th>
            <th style="width: 200px">お届け予定</th>
            <th style="width: 80px">表示順</th>
            <th style="width: 80px"></th>
        </tr>

        <?php if (isset($project_return)) { ?>
            <?php foreach ($project_return as $key => $value) { ?>
                <tr class="trTableProjectReturn111" style="cursor:pointer" id="<?php echo $value->id ?>">
                    <td><a class="comment" style="color:black" href="/projectreturn/edit/<?php echo $value->id ?>"><?php
                            echo $value->name;
                            ?></a></td>
                    <td>
                        <?php echo number_format($value->invest_amount) ?>
                    </td>
                    <td>
                        <a class="comment_return" style="color:black" href="/projectreturn/edit/<?php echo $value->id ?>"><?php
                            echo $value->return_amount;
                            ?></a>
                    </td>
                    <td>
                        <?php
                        if($project_type === '0'){
                            echo $value->schedule;
                        }else {
                            echo "-------------------------";
                        }
                        ?>
                    </td>
                    <td><?php echo $value->show_order; ?>

                        <a href="javascript:;" onclick="onClickDown(<?php echo $value->id; ?>)" > <i class="fa fa-angle-down"></i>


                        </a>
                        <a href="javascript:;" onclick="onClickUp(<?php echo $value->id; ?>)" ><i class="fa fa-angle-up"></i>


                        </a>

                    </td>
                    <td>
                        <a
                                href="/projectreturn/edit/<?php echo $value->id; ?>"
                                style="color:white"
                                class="btn btn-xs btn-primary">
                            <i class="fa fa-edit"></i>
                        </a>
                        <a
                                href="/projectreturn/delete/<?php echo $value->id; ?>"
                                style="color:white"
                                class="btn btn-xs btn-danger"
                                data-title="<?php echo $this->lang->line('are_you_sure'); ?>"
                                data-btn-ok-label="<?php echo $this->lang->line('yes'); ?>"
                                data-btn-cancel-label="<?php echo $this->lang->line('no'); ?>"
                                data-toggle="confirmation2" data-placement="left"
                                data-singleton="true">
                            <i class="fa fa-trash-o"></i>
                        </a>
                    </td>
                </tr>
            <?php } ?>
        <?php } else { ?>
            <tr>
                <td
                        colspan="5"
                        class="text-center">
                    <?php echo $this->config->item('no_data')?>
                </td>
            </tr>
        <?php }?>
    </table>
    <a
            style=" display: inline-block;text-align: center;margin:25px 46% 0 46%;
                color:white"
            href="/projectreturn/add/<?php echo $project->id; ?>"
            class="btn btn-xs btn-primary">
        リターン追加
    </a>
</div>