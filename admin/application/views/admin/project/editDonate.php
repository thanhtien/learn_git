 <!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            支援情報画面
        </h1>
        <ol class="breadcrumb">
            <li><a href="/"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active"><a href="<?php echo site_url('/project/edit/'.$project->id)?>">支援情報画面</a></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <?php if ($this->session->flashdata('msg')) { ?>
          <div class="alert alert-success" id="success-alert">
              <button type="button" class="close" data-dismiss="alert"><i class="fa fa-times"></i></button>
              <strong><?php echo $this->lang->line('success'); ?></strong>
              <?php echo $this->session->flashdata('msg'); ?>

          </div>
        <?php } ?>
        <?php if ($this->session->flashdata('error')) { ?>
          <div class="alert alert-danger" id="success-alert">
              <button type="button" class="close" data-dismiss="alert"><i class="fa fa-times"></i></button>
              <strong><?php echo $this->lang->line('error'); ?></strong>
              <?php echo $this->session->flashdata('error'); ?>
          </div>
        <?php } ?>
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">支援情報画面</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" action="" method="POST" enctype="multipart/form-data">
                <div class="nav-tabs-custom">
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab_1">
                              <div class="box-body">
                                <div class="form-group">
                                    <label for="name">プロジェクト名</label>
                                    <input type="text" class="form-control" required="" id="name" name="name" value="<?php echo $project->project_name ?>" disabled placeholder="">
                                </div>
                                <div class="form-group">
                                    <label for="name">支援者名</label>
                                    <input type="text" class="form-control" required="" id="name" name="name" value="<?php echo $user->username ?>" disabled placeholder="">
                                </div>
                                <div class="form-group">
                                    <label for="name">支援金額</label>
                                    <input type="text" class="form-control" required="" id="name" name="name" value="<?php echo number_format($backed->invest_amount); ?> ¥" disabled placeholder="">
                                </div>
                                <label for="name">リターン品</label>
                                <div class="form-group">
                                  <?php foreach ($allProejctReturn as $key => $value) { ?>
                                    <label for="Donate<?php echo $value->id ?>" class="cover_data" style="margin-bottom: 45px;">
                                      <div style="min-height:390px;">
                                        <p style="padding: 0 10px;margin-top: 15px;"><?php echo number_format($value->invest_amount); ?> ¥ <input type="radio" style="float:right;" id="Donate<?php echo $value->id ?>" name="money_donate" <?php if($backed->backing_level_id == $value->id){ echo "checked";} ?> value="<?php echo $value->id; ?>"></p>
                                        <p style="padding: 13px;line-height: 2;"><?php echo $value->return_amount; ?></p>
                                      </div>
                                    </label>
                                  <?php } ?>
                                  <div style="clear:both;"></div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-3">
                                        <label class="container" style="margin-left:0;">振込済み
                                          <input type="checkbox"
                                          <?php if($backed->status === 'succeeded' ){ ?>
                                            checked
                                          <?php } ?>
                                           name="status" value="1">
                                          <span class="checkmark"></span>
                                        </label>
                                    </div>
                                    <?php if($backed->status === 'succeeded' || $backed->status === 'refunded'){ ?>
                                        <div class="col-sm-3">
                                            <label class="container" style="margin-left:0;">銀行口座へ返金済み
                                              <input type="checkbox"
                                              <?php if($backed->status === 'refunded' ){ ?>
                                                checked
                                              <?php } ?>
                                               name="status_refunded" value="1">
                                              <span class="checkmark"></span>
                                            </label>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                            <!-- /.box-body -->
                        </div>
                    </div>
                    <!-- /.tab-content -->
                </div>


                <div class="box-footer">
                    <button type="submit" name="save" class="btn btn-primary" value="1"><?php echo $this->lang->line('save'); ?></button>
                    <a href="<?php echo site_url('/project/edit/'.$project->id) ?>" class="btn btn-default"><?php echo $this->lang->line('cancel'); ?></a>
                    <input type="hidden" name="pid" value="<?php echo $backed->id?>" />
                </div>
            </form>
        </div>

    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
