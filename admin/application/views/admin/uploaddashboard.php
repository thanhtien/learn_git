<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
        プロジェクト
    </h1>
    <ol class="breadcrumb">
      <li>
        <a href="/">
          <i class="fa fa-dashboard"></i>
            Home
        </a>
      </li>
      <li class="active"><a href="<?php echo site_url('/project')?>">プロジェクト</a></li>
    </ol>
  </section>
    <!-- Main content -->
    <section class="content">
        <?php if ($this->session->flashdata('msg')) { ?>
          <div
            class="alert alert-success"
            id="success-alert">
              <button
                type="button"
                class="close"
                data-dismiss="alert">
                <i class="fa fa-times"></i>
              </button>
              <strong>
                <?php echo $this->lang->line('success'); ?>
              </strong>
              <?php echo $this->session->flashdata('msg'); ?>
          </div>
        <?php } ?>
        <?php if ($this->session->flashdata('error')) { ?>
        <div
          class="alert alert-danger"
          id="success-alert">
            <button
              type="button"
              class="close"
              data-dismiss="alert">
              <i class="fa fa-times"></i>
            </button>
            <strong><?php echo $this->lang->line('error'); ?></strong>
            <?php echo $this->session->flashdata('error'); ?>
        </div>
        <?php } ?>
      <div class="box box-primary">
        <div id="tabs">
          <input type="hidden" id="page_tab" name="" value="0">
          <ul>
            <li>
              <a href="#tabs-1">
                プロジェクト概要・詳細
              </a>
            </li>
            <li>
              <a href="#tabs-2">
                リターン作成
              </a>
            </li>
            <li>
              <a href="#tabs-3">
                支援一覧
              </a>
            </li>
          </ul>
          <div id="tabs-1">
            <form role="form" action=""  id="demoForm" method="POST" enctype="multipart/form-data">
                <div class="nav-tabs-custom">
                  <div class="tab-content">
                    <div class="tab-pane active" id="tab_1">
                      <div class="box-body">
                        <div class="form-group col-sm-12">
                          <label for="name">
                            プロジェクト名（最大150文字）
                          </label>
                          <input
                            type="text"
                            class="form-control"
                            required
                            id="project_name"
                            value="<?php echo $project->project_name ?>"
                            name="project_name"
                            placeholder=""
                            maxlength="150">
                        </div>
                        <div class="form-group col-sm-12">
                          <label for="name">ユーザー名</label>
                          <br>
                          <select class="form-control col-sm-12" id="itemName" name="itemName">
                            <option class="form-control col-sm-12" value="<?php echo $project->user_id ?>"><?php echo $user_name->username ?></option>
                          </select>
                        </div>
                          <div class="form-group col-sm-12" id="form_img">
                            <label for="name">画像（サムネイル表示）</label>
                            <p>※画像のサイズは600px＊400pxで最高です。容量 2MBまで 。</p>
                            <input
                            type="file"
                            name="file"
                            id="profile-img">
                            <input
                              type="hidden"
                              name="hiden_image"
                              id="hiden_image"
                              value="<?php echo $project->thumbnail ?>">
                            <p id="announce" style="color:red" ></p>
                            <p style="text-align:center">
                              <img
                                src="<?php echo $project->thumbnail ?>"
                                width="600px"
                                id="profile-img-tag" />
                            </p>
                          </div>
                          <div class="form-group col-sm-12">
                            <label for="name">
                              はじめにご挨拶 （最大210文字）
                            </label>
                            <textarea
                              class="form-control"
                              name="summary"
                              id="summary" maxlength="210"><?php echo $project->description ?></textarea>
                          </div>
                          <div class="form-group">
                            <p class="form-group col-sm-12">※「動画のURL入力」か「画像アップロード」を選択して、該当のURLか、画像をアップロードしてください。</p>
                            <div class="form-check col-sm-2">
                              <input
                                class="form-check-input"
                                type="radio"
                                name="Radios"
                                id="Video"
                                <?php
                                  if($project->thumbnail_type == 1){
                                ?>
                                checked=""
                                <?php
                                  }
                                ?>
                                value="1"
                                >
                              <label
                                class="form-check-label"
                                for="Video">
                                動画のURL入力
                              </label>
                            </div>
                            <div class="form-check col-sm-2">
                              <input
                                class="form-check-input"
                                type="radio"
                                name="Radios"
                                id="Image"
                                <?php
                                  if($project->thumbnail_type ==0){
                                ?>
                                checked=""
                                <?php
                                  }
                                ?>
                                value="0">
                              <label class="form-check-label" for="Image">
                                画像アップロード
                              </label>
                            </div>
                            <div class="col-sm-12">
                              <div id="video">
                                <div class="col-sm-12">
                                  <label for="name">動画URL</label>
                                  <input type="text" class="form-control" id="video" name="video" value="<?php echo $project->thumbnail_movie_code; ?>">
                                </div>
                              </div>
                              <div id="image_thumnail">
                                <p class="col-sm-12">※2～３枚の画像をアップロードしたら、スライダーで表示されるようになります。</p>
                                <p class="col-sm-12">※画像のサイズは600px＊400pxで最高です。容量 2MBまで 。</p>
                                <div class="form-group col-sm-4" id="form_img">
                                  <label for="name">画像 1</label>
                                  <input type="file" name="file1" id="profile_thumnail1">
                                  <input type="hidden" value="<?php echo $project->thumbnail_descrip1 ?>" name="hiden_image1" id="hiden_image1" >
                                  <p id="announce1" style="color:red" ></p>
                                  <p><img src="<?php echo $project->thumbnail_descrip1 ?>" width="300px"   id="profile_thumnail1-tag"/></p>
                                </div>
                                <div class="form-group col-sm-4" id="form_img">
                                  <label for="name">画像 2</label>
                                  <input type="file" name="file2" id="profile_thumnail2">
                                  <input type="hidden"value="<?php echo $project->thumbnail_descrip2 ?>" name="hiden_image2" id="hiden_image2" >
                                  <p id="announce2" style="color:red" ></p>
                                  <p><img src="<?php echo $project->thumbnail_descrip2 ?>" width="300px"   id="profile_thumnail2-tag"/></p>
                                </div>
                                <div class="form-group col-sm-4" id="form_img">
                                  <label for="name">画像 3</label>
                                  <input type="file" name="file3" id="profile_thumnail3">
                                  <input type="hidden" value="<?php echo $project->thumbnail_descrip3 ?>" name="hiden_image3" id="hiden_image3" >
                                  <p id="announce3" style="color:red" ></p>
                                  <p><img src="<?php echo $project->thumbnail_descrip3 ?>" width="300px"   id="profile_thumnail3-tag"/></p>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="form-group ">
                            <div class="col-sm-6">
                              <Br />
                              <label for="name">プロジェクトのカテゴリ</label>
                              <select class="form-control form-control-sm" name="category">
                                <?php foreach ($category as $key => $value) { ?>
                                  <option
                                    value="<?php echo $value->id?>"
                                    <?php if($value->id == $project->category_id){ ?>
                                    selected
                                    <?php }?> >
                                      <?php echo $value->name; ?>
                                  </option>
                                  <?php } ?>
                              </select>
                            </div>
                            <div class="col-sm-6">
                              <label for="name">
                                <br />
                                募集終了日
                              </label>
                              <div
                                class="input-group date"
                                data-date-format="datepicker-mm-yyyy">
                                <input
                                  type="text"
                                  autocomplete="off"
                                  class="form-control"
                                  <?php
                                    $date=strtotime($project->collection_end_date);
                                  ?>
                                  placeholder="yyyy-mm-dd"
                                  name="date"
                                  value="<?php echo date("Y-m-d",$date); ?>"
                                  >
                                <div class="input-group-addon" >
                                  <span class="glyphicon glyphicon-th"></span>
                                </div>
                              </div>
                            </div>
                          </div>
                            <div class="form-group col-sm-12">
                              <br />
                              <label for="name">
                                目標金額
                              </label>
                              <input
                                type="text"
                                class="form-control money"
                                required=""
                                id="goal_amount"
                                autocomplete="off"
                                value="<?php echo $project->goal_amount; ?>"
                                name="goal_amount"
                                onkeypress='validate(event)'>
                            </div>
                            <div class="form-group col-sm-12">
                              <label for="name">
                                プロジェクト概要
                              </label>
                              <textarea
                                class="form-control ckeditor"
                                name="description"
                                id="description"><?php echo $project->project_content ?></textarea>
                            </div>
                        </div>
                    </div>
                  </div>
                </div>
                <input type="hidden" name="active" value="<?php echo $project->active; ?>">

                <div class="box-footer">
                  <?php
                    if($this->session->flashdata('tab')){ ?>
                      <input type="hidden" name="tab" id="tab" value="<?php echo $this->session->flashdata('tab');?>" />
                  <?php }else{ ?>
                      <input type="hidden" name="tab" id="tab" value="0" />
                  <?php } ?>

                <!-- <?php echo $this->session->flashdata('tab');?> -->
                </div>
            </form>
          </div>
          <div id="tabs-2">
            <div class="box-body no-padding">
              <table class="table table-bordered table-hover">
                <tr>
                  <th>支援額</th>
                  <th>リターン品・説明文</th>
                  <th>お届け予定</th>
                  <th style="width: 80px"></th>
                </tr>

                  <?php if (isset($project_return)) { ?>
                    <?php foreach ($project_return as $key => $value) { ?>
                      <tr>
                        <td>
                          <?php echo number_format($value->invest_amount) ?>
                        </td>
                        <td>
                          <a class="comment_return" style="color:black" href="/projectreturn/edit/<?php echo $value->id ?>"><?php
                              echo $value->return_amount;
                          ?></a>
                        </td>
                        <td>
                          <?php
                            if($value->schedule == ''){
                              echo '';
                            }else{
                              $time = strtotime($value->schedule);
                              echo date('Y-m-d',$time);
                            }
                          ?>
                        </td>
                        <td>
                          <a
                            href="/projectreturn/edit/<?php echo $value->id; ?>"
                            style="color:white"
                            class="btn btn-xs btn-primary">
                            <i class="fa fa-edit"></i>
                          </a>
                          <a
                            href="/projectreturn/delete/<?php echo $value->id; ?>"
                            style="color:white"
                            class="btn btn-xs btn-danger"
                            data-title="<?php echo $this->lang->line('are_you_sure'); ?>"
                            data-btn-ok-label="<?php echo $this->lang->line('yes'); ?>"
                            data-btn-cancel-label="<?php echo $this->lang->line('no'); ?>"
                            data-toggle="confirmation2" data-placement="left"
                            data-singleton="true">
                              <i class="fa fa-trash-o"></i>
                          </a>
                        </td>
                      </tr>
                    <?php } ?>
                    <?php } else { ?>
                        <tr>
                          <td
                            colspan="5"
                            class="text-center">
                              <?php echo $this->config->item('no_data')?>
                          </td>
                        </tr>
                    <?php }?>
              </table>
            </div>
          </div>
          <div id="tabs-3">
            <div class="box-body no-padding">
              <table class="table table-bordered table-hover">
                <tr>
                  <th>支援者名</th>
                  <th>支援日</th>
                  <th style="width:170px;">支援形式</th>
                  <th style="width:180px;">金額</th>
                </tr>
                <?php if ($backedProject['total'] > 0) { ?>
                  <?php foreach ($backedProject['backed'] as $key => $backed) { ?>
                    <tr>
                      <td><?php echo $backed->username; ?></td>
                      <td><?php echo ($backed->created)  ?></td>
                      <td><?php echo $backed->type  ?></td>
                      <td><?php echo number_format($backed->invest_amount)  ?> 円</td>
                    </tr>
                  <?php } ?>
                <?php } else { ?>
                    <tr>
                      <td colspan="5" class="text-center"><?php echo $this->config->item('no_data')?></td>
                    </tr>
                <?php }?>


              </table>
            </div>
            <div class="box-footer clearfix">
                <?php
                    echo customPaginationProjectReturn('/project/edit/'.$idProject.'/', $backedProject['total'],10);
                ?>

            </div>
            <div style="overflow:hidden;">
              <?php  if(isset($project->show_btn) && $project->show_btn){ ?>
                  <form role="form" action="" style="float:left;" id="demoForm" method="POST" enctype="multipart/form-data">
                    <p   style="margin-top: 49px;">本プロジェクトの企画者の口座に振り込みましたか。</p>
                    <button
                      type="submit"
                      name="save"
                      class="btn btn-primary"
                      value="1"

                      >
                      はい
                    </button>
                  </form>
              <?php  }  ?>

              <div style="width:350px;padding: 10px 5px;float:right; text-align:right; border:1px solid; padding">
                <p style="width:340px;float:left; text-align:left">合計<span style="margin-left: 75px;">:</span>  <span style="float:right; color:green;margin-right: 115px;"><?php echo number_format($total)  ?>円</span></p>
                <p style="width:340px;float:left; text-align:left">目標金額<span style="margin-left: 44px;">:</span> <span style="float:right;margin-right: 115px;"><?php echo number_format($goal_amount)  ?>円</span> </p>
                <p style="width:340px;float:left; text-align:left">残り<span style="margin-left: 74px;">:</span>
                  <?php

                    if($goal_amount - $total > 0){
                      echo '<span style="float:right; color:red;margin-right: 115px;">-'. number_format($goal_amount - $total). '円</span>';
                    }else{
                      echo '<span style="float:right; color:#00FFFF;margin-right: 115px">+'. number_format($total - $goal_amount).'円</span>';
                    }
                  ?>

                </p>
                <p style="clear: both;display:none;"></p>
              </div>
            </div>

          </div>
        </div>
      </div>
    </section>
</div>
