<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            ダッシュボード
        </h1>
        <ol class="breadcrumb">
            <li><a href="/"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active"><a href="/"><i class="fa fa-dashboard"></i>ダッシュボード</a></li>
        </ol>
    </section>
    <section class="content">
      <?php if ($this->session->flashdata('msg')) { ?>
          <div class="alert alert-success" id="success-alert">
              <button type="button" class="close" data-dismiss="alert"><i class="fa fa-times"></i></button>
              <strong><?php echo $this->lang->line('success'); ?></strong>
              <?php echo $this->session->flashdata('msg'); ?>
          </div>
      <?php } ?>
      <?php if ($this->session->flashdata('error')) { ?>
        <div class="alert alert-danger" id="success-alert">
            <button type="button" class="close" data-dismiss="alert"><i class="fa fa-times"></i></button>
            <strong><?php echo $this->lang->line('error'); ?></strong>
            <?php echo $this->session->flashdata('error'); ?>
        </div>
      <?php } ?>
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <p style="font-weight: bold;font-size: 20px;margin-left: 10px; padding: 10px 10px 10px 15px;">終了・終了間際のプロジェクト一覧</p>
            <p style="margin-left: 10px; padding: 0 0 10px 10px;">※本画面には、終了のプロジェクトで、管理者はプロジェクトの企画者の口座にまだ振り込まない場合に、赤字で表示されるが、振り込みが行われたら、非表示になること。</p>
            <form id="frmMain" method="POST" action="/project/action">
              <!-- /.box-header -->
              <div class="box-body no-padding">
                <?php
                  if(isset($by) && $by === 'DESC'){
                    $order = 'ASC';
                  }else{
                    $order = 'DESC';
                  }
                ?>
                <table class="table table-bordered table-hover" id="tableDashBoard">
                  <tr>
                    <th style="width: 250px">プロジェクト名</th>
                    <th style="width: 120px;">目標金額</th>
                    <th style="width: 120px;">現在の支援総額</th>
                    <th style="width: 120px; text-align: center;">ファンディングの企画者</th>
                    <th>支援者数</th>
                    <th>振り込みの状態</th>
                    <th>開始</th>
                    <th>完了</th>
                    <th style="width: 80px"></th>
                  </tr>
                    <?php if ($data['total'] > 0) { ?>
                      <?php foreach ($data['project'] as $key => $project) { ?>
                        <tr class="trTableDashBoard"  style="cursor:pointer;" id="<?php echo $project->id ?>">
                          <td>
                            <a  class="comment" style="color: <?php echo $project->color;?>" href="/dashboard/edit/<?php echo $project->id ?>"><?php
                                echo $project->project_name;
                            ?></a>
                          </td>
                          <td style="color: <?php echo $project->color;?>"><?php echo number_format($project->goal_amount)  ?> 円</td>
                          <td style="color: <?php echo $project->color;?>"><?php echo number_format($project->collected_amount)  ?> 円</td>
                          <td style="color: <?php echo $project->color;?>"><?php echo $project->username  ?></td>
                          <td style="color: <?php echo $project->color;?>"><?php echo $project->now_count ?></td>
                          <td style="color: <?php echo $project->color;?>"><?php
                            if($project->pay_owner =='1'){
                              echo '決済済み';
                            }else{
                              echo '未決済';
                            }
                          ?></td>
                          <td style="color: <?php echo $project->color;?>"><?php echo $project->collection_start_date?></td>
                          <td style="color: <?php echo $project->color;?>"><?php echo $project->collection_end_date?></td>
                          <!-- <td><input type="number" name="sort[]" min="0" oninput="validity.valid||(value='');" value="<?php// echo $project->order ?>" class="sorting-input"/></td> -->
                          <td style="text-align:center">
                              <a href="/dashboard/edit/<?php echo $project->id; ?>" class="btn btn-xs btn-primary"><i class="fa fa-edit"></i></a>
                          </td>
                        </tr>
                      <?php } ?>
                    <?php } else { ?>
                        <tr>
                          <td colspan="10" class="text-center"><?php echo $this->config->item('no_data')?></td>
                        </tr>
                    <?php }?>
                </table>
              </div>
              <div class="box-footer clearfix">
                  <?php
                    if(isset($search)){
                      echo custom_paginationGET('/dashboard/searchProject/', $search, $data['total'],10);
                    }else{
                      echo custom_pagination('/dashboard/index/', $data['total'],10);
                    }
                  ?>
              </div>
            </form>
              <!-- /.box-body -->
          </div>
        </div>
      </div>
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
