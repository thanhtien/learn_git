  <!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <?php echo $this->lang->line('user') ?>
        </h1>
        <ol class="breadcrumb">
            <li><a href="/"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active"><a href="<?php echo site_url('/users')?>"><?php echo $this->lang->line('user') ?></a></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <?php if ($this->session->flashdata('msg')) { ?>
          <div class="alert alert-success" id="success-alert">
              <button type="button" class="close" data-dismiss="alert"><i class="fa fa-times"></i></button>
              <strong><?php echo $this->lang->line('success'); ?></strong>
              <?php echo $this->session->flashdata('msg'); ?>

          </div>
        <?php } ?>
        <?php if ($this->session->flashdata('error')) { ?>
          <div class="alert alert-danger" id="success-alert">
              <button type="button" class="close" data-dismiss="alert"><i class="fa fa-times"></i></button>
              <strong><?php echo $this->lang->line('error'); ?></strong>
              <?php echo $this->session->flashdata('error'); ?>
          </div>
        <?php } ?>
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title"><?php echo $this->lang->line('edit_user') ?></h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form"  id="newUser" action="" method="POST" enctype="multipart/form-data">
                <div class="nav-tabs-custom">
                    <div class="tab-content" style="overflow:hidden">
                        <div class="tab-pane active col-sm-6" id="tab_1">
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="username"><?php echo $this->lang->line('username') ?></label>
                                    <input type="text" class="form-control"  id="username" name="username" disabled="" value="<?php echo $users->username ?>">
                                </div>
                                <div class="form-group">
                                    <label for="email"><?php echo $this->lang->line('email') ?> </label>
                                    <input type="text" class="form-control"  id="email" name="email" disabled="" value="<?php echo $users->email ?>">
                                </div>
                                <div class="form-group">
                                    <label for="username">名前</label>
                                    <input type="text" class="form-control"  id="username" name="username" disabled="" value="<?php if(isset($address_user->name_return) && $address_user->name_return) {echo $address_user->name_return;} else { echo "";}  ?>">
                                </div>
                                <div class="form-group">
                                    <label for="email">電話番号</label>
                                    <input type="text" class="form-control"  id="username" name="username" disabled="" value="<?php if(isset($address_user->phone) && $address_user->phone) {echo $address_user->phone;} else { echo "";}  ?>">
                                </div>
                                <div class="form-group">
                                    <label for="email">郵便番号</label>
                                    <input type="text" class="form-control"  id="username" name="username" disabled="" value="<?php if(isset($address_user->postcode) && $address_user->postcode) {echo $address_user->postcode;} else { echo "";}  ?>">
                                </div>
                                <div class="form-group">
                                    <label for="email">住所</label>
                                    <input type="text" class="form-control"  id="username" name="username" disabled="" value="<?php if(isset($address_user->address_return) && $address_user->address_return) {echo $address_user->address_return;} else { echo "";}  ?>">
                                </div>
                            </div>
                            <!-- /.box-body -->
                        </div>
                        <div class="tab-pane active col-sm-6">
                            <div class="box-body">

                                <div class="form-group " style=" margin-top: 26px;">
                                  <label class="container" style="margin-left:0; padding-top: 5px;font-size:14px;"><?php echo $this->lang->line('change_pass') ?>
                                    <input type="checkbox"  id="checkme" name="change_pass" value="1">
                                    <span class="checkmark"></span>
                                  </label>
                                </div>

                                <div class="form-group">

                                  <div class="On_Off">
                                    <div class="form-group">
                                        <label for="password"><?php echo $this->lang->line('password') ?></label>
                                        <input type="password" disabled class="form-control"  id="password" name="password"  placeholder="">
                                    </div>
                                    <div class="form-group">
                                        <label for="re_pass"><?php echo $this->lang->line('re_password') ?></label>
                                        <input type="password" disabled class="form-control"  id="re_pass" name="re_pass"  placeholder="">
                                    </div>
                                  </div>

                                </div>

                                <div class="form-group">
                                    <label for="active"><?php echo $this->lang->line('active') ?></label>
                                    <select class="form-control" name="active">
                                      <option
                                        <?php if($users->active == 1){ ?>
                                          selected
                                        <?php } ?>
                                        value="1" >公開
                                      </option>
                                      <option
                                        <?php if($users->active == 0){ ?>
                                          selected
                                        <?php } ?>
                                        value="0">ブロック
                                      </option>
                                    </select>
                                </div>
                                <div class="form-group" id="form_img">
                                  <label for="profile-img">画像（サムネイル表示）</label>
                                  <p>※画像のサイズは300px＊300pxで最高です。容量 2MBまで 。</p>
                                  <input type="file" name="file" id="profile-img-user">
                                  <input type="hidden" name="hiden_image" id="hiden_image" value="<?php echo $users->profileImageURL ?>">
                                  <p id="announce" style="color:red" ></p>
                                  <p style="text-align: left;margin-top:40px;"><img src="<?php echo $users->profileImageURL ?>" id="profile-img-tag"/ width="150px" alt=""></p>
                                </div>
                            </div>
                            <!-- /.box-body -->
                        </div>
                    </div>
                    <!-- /.tab-content -->
                </div>


                <div class="box-footer">
                    <button type="submit" name="save" class="btn btn-primary" value="1"><?php echo $this->lang->line('save'); ?></button>
                    <a href="<?php echo site_url('/users/') ?>" class="btn btn-default"><?php echo $this->lang->line('cancel'); ?></a>
                    <input type="hidden" name="pid" value="<?php echo $users->id?>" />
                </div>
            </form>
        </div>

    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
