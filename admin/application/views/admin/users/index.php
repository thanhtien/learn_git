<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <?php echo $this->lang->line('user'); ?>
        </h1>
        <ol class="breadcrumb">
            <li><a href="/"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active"><a href="<?php echo site_url('/users/')?>"><?php echo $this->lang->line('user'); ?></a></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <?php if ($this->session->flashdata('msg')) { ?>
            <div class="alert alert-success" id="success-alert">
                <button type="button" class="close" data-dismiss="alert"><i class="fa fa-times"></i></button>
                <strong><?php echo $this->lang->line('success'); ?></strong>
                <?php echo $this->session->flashdata('msg'); ?>
            </div>
        <?php } ?>
        <?php if ($this->session->flashdata('error')) { ?>
          <div class="alert alert-danger" id="success-alert">
              <button type="button" class="close" data-dismiss="alert"><i class="fa fa-times"></i></button>
              <strong><?php echo $this->lang->line('error'); ?></strong>
              <?php echo $this->session->flashdata('error'); ?>
          </div>
        <?php } ?>
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="FrmSearch">
                      <form class="navbar-form" role="search" method="GET" action="/users/searchUsers">
                        <div class="input-group add-on">
                          <input class="form-control" placeholder="Search" value="<?php if(isset($search)){ echo $search; }?>" name="srch-term" id="srch-term" type="text">
                          <div class="input-group-btn">
                            <button class="btn btn-default" id="btn-srch" type="submit"><i class="glyphicon glyphicon-search"></i></button>
                          </div>
                        </div>
                      </form>
                    </div>
                    <p class="total_user">    登録済の全ユーザー数  : <?php echo $total; ?></p>
                    <form id="frmMain" method="POST" action="/users/action">
                        <div class="box-header">
                            <h3 class="box-title">&nbsp;</h3>

                            <div class="box-tools">
                                <div class="btn-group pull-right">
                                    <a class="btn btn-sm btn-primary " href="<?php echo site_url('/users/add') ?>"><i class="fa fa-plus"></i> 新規作成</a>
                                    <!-- <a id="bulk-delete" class="btn btn-sm btn-danger " data-title="選択されたものを削除しますか。" data-btn-ok-label="<?php echo $this->lang->line('yes'); ?>"  data-btn-cancel-label="<?php echo $this->lang->line('no'); ?>"  data-toggle="confirmation" data-placement="left" data-singleton="true"><i class="fa fa-trash-o"></i> 削除</a> -->
                                    <input type="hidden" id="hidAction" name="hidAction" value="" />
                                </div>
                            </div>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body no-padding">
                          <?php
                            if(isset($by) && $by === 'DESC'){
                              $order = 'ASC';
                            }else{
                              $order = 'DESC';
                            }
                          ?>
                            <table id="test" class="table table-bordered table-hover" id="tableUser">
                                <tr>
                                    <th style="width: 20px"><input type="checkbox" class="minimal checkth" ></th>
                                    <th >
                                      <?php if(isset($search)){ ?>
                                        <a href="/users/searchUsers/username/<?php echo $order; ?>/?srch-term=<?php echo $search; ?>"><?php echo $this->lang->line('username'); ?></a></th>
                                      <?php }else{ ?>
                                        <a href="/users/index/username/<?php echo $order; ?>"><?php echo $this->lang->line('username'); ?></a></th>
                                      <?php } ?>
                                    <th >
                                      <?php if(isset($search)){ ?>
                                        <a href="/users/searchUsers/email/<?php echo $order; ?>/?srch-term=<?php echo $search; ?>">メールアドレス</a></th>
                                      <?php }else{ ?>
                                        <a href="/users/index/email/<?php echo $order; ?>">メールアドレス</a></th>
                                      <?php } ?>
                                    <th >
                                        <?php if(isset($search)){ ?>
                                            <a href="/users/searchUsers/created/<?php echo $order; ?>/?srch-term=<?php echo $search; ?>">メールアドレス</a></th>
                                        <?php }else{ ?>
                                            <a href="/users/index/created/<?php echo $order; ?>">登録日時</a></th>
                                        <?php } ?>
                                    <th style="width: 150px">
                                      <?php if(isset($search)){ ?>
                                        <a href="/users/searchUsers/group_id/<?php echo $order; ?>/?srch-term=<?php echo $search; ?>">権限</a></th>
                                      <?php }else{ ?>
                                        <a href="/users/index/group_id/<?php echo $order; ?>">権限</a></th>
                                      <?php } ?>
                                    <th style="width: 150px">
                                      <?php if(isset($search)){ ?>
                                        <a href="/users/searchUsers/active/<?php echo $order; ?>/?srch-term=<?php echo $search; ?>"><?php echo $this->lang->line('active'); ?></a></th>
                                      <?php }else{ ?>
                                        <a href="/users/index/active/<?php echo $order; ?>"><?php echo $this->lang->line('active'); ?></a></th>
                                      <?php } ?>
                                    <th style="width: 100px"></th>
                                </tr>
                                <?php if ($data['total'] > 0) { ?>
                                    <?php foreach ($data['users'] as $key => $user) { ?>
                                        <tr style="cursor:pointer;"  id="<?php echo $user->id; ?>">
                                            <td><input type="checkbox" class="minimal checkitem" name="val[]" value="<?php echo $user->id ?>" ></td>
                                            <td>
                                              <a href="/users/edit/<?php echo $user->id; ?>" style="color:black;display:block" ><?php echo $user->username ?></a>
                                            </td>
                                            <td>
                                                <a href="/users/edit/<?php echo $user->id; ?>" style="color:black;display:block" ><?php echo $user->email ?></a>
                                            </td>
                                            <td>
                                                <a href="/users/edit/<?php echo $user->id; ?>" style="color:black;display:block" ><?php echo date("Y-m-d", strtotime($user->created)); ?></a>
                                            </td>
                                            <td>
                                                <a href="/users/edit/<?php echo $user->id; ?>" style="color:black;display:block" ><?php echo $user->name ?></a>
                                            </td>
                                            <td>
                                                <a href="/users/edit/<?php echo $user->id; ?>" style="color:black;display:block" ><?php
                                                  if($user->active == 1 ){
                                                    echo $this->lang->line('active_user');
                                                  }else{
                                                    echo "<span style='color:red'>ブロック</span>";
                                                  }
                                                ?>
                                                </a>
                                            </td>
                                            <td  style="text-align: center;">
                                                <div class=" popUpBlockUser popUpBlockUser<?php echo $user->id; ?>">
                                                  <p class="text_popUp">このユーザーをブロックしますか。</p>
                                                  <p class="text_popUp">注：「保存」ボタンをクリックすると、公開中のプロジェクトへの支援者に通知メールがすぐに自動的に送付されるようになります。</p>
                                                  <div class="" style="background:#fff;padding: 10px 0;">
                                                    <button style="background-color: #3c8dbc;    margin-right: 20px; width: 55px;border-color: #367fa9;color:#fff;border:none;    margin-left: 32%;" type="button" name="button" class="yes_blockuser<?php echo $user->id; ?>">はい </button>
                                                    <button style="background-color: #f4f4f4;width: 55px;border-color: #ddd;color:#000;border:none;" type="button" name="button" class="no_blockuser">いいえ</button>
                                                  </div>
                                                </div>
                                                <p id="block_user" class="btn btn-xs btn-danger"><i class="fa fa-ban"></i></p>
                                                <a href="/users/edit/<?php echo $user->id; ?>" class="btn btn-xs btn-primary"><i class="fa fa-edit"></i></a>
                                                <!-- <a href="/users/delete/<?php echo $user->id; ?>" class="btn btn-xs btn-danger" data-title="このユーザを削除しますか。" data-btn-ok-label="<?php echo $this->lang->line('yes'); ?>"  data-btn-cancel-label="<?php echo $this->lang->line('no'); ?>" data-toggle="confirmation2" data-placement="left" data-singleton="true"><i class="fa fa-trash-o"></i></a> -->
                                            </td>
                                        </tr>
                                    <?php } ?>
                                <?php } else { ?>
                                        <tr>
                                            <td colspan="3" class="text-center"><?php echo $this->config->item('no_data')?></td>
                                        </tr>
                                <?php }?>
                            </table>

                        </div>
                        <div class="box-footer clearfix">
                            <?php
                            if(isset($search)){
                              echo custom_paginationGET('/users/searchUsers/'.$sort.'/'.$by.'/', $search, $data['total'],10);
                            }else{
                              echo custom_pagination('/users/index/'.$sort.'/'.$by.'/', $data['total'],10);
                            }
                            ?>
                        </div>

                    </form>

                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
        </div>
    </section>
</section>
<!-- /.content -->
</div>

<!-- /.content-wrapper -->
