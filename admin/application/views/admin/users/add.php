<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <?php echo $this->lang->line('user') ?>
        </h1>
        <ol class="breadcrumb">
            <li><a href="/"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active"><a href="<?php echo site_url('/users')?>"><?php echo $this->lang->line('user') ?></a></li>
        </ol>
    </section>
    <div class="loading">
      <div class="loader">
        <span></span>
        <span></span>
        <span></span>
        <span></span>
        <span></span>
        <span></span>
        <span></span>
        <span></span>
        <span></span>
        <span></span>
        <span></span>
        <span></span>
        <span></span>
        <span></span>
        <span></span>
      </div>
    </div>
    <!-- Main content -->
    <section class="content">
        <?php if ($this->session->flashdata('msg')) { ?>
          <div class="alert alert-success" id="success-alert">
              <button type="button" class="close" data-dismiss="alert"><i class="fa fa-times"></i></button>
              <strong><?php echo $this->lang->line('success'); ?></strong>
              <?php echo $this->session->flashdata('msg'); ?>

          </div>
        <?php } ?>
        <?php if ($this->session->flashdata('error')) { ?>
          <div class="alert alert-danger" id="success-alert">
              <button type="button" class="close" data-dismiss="alert"><i class="fa fa-times"></i></button>
              <strong><?php echo $this->lang->line('error'); ?></strong>
              <?php echo $this->session->flashdata('error'); ?>
          </div>
        <?php } ?>
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title"><?php echo $this->lang->line('create_user') ?></h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form"  id="newUser" action="" method="POST" enctype="multipart/form-data">
                <div class="nav-tabs-custom">
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab_1">
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="username"><?php echo $this->lang->line('username') ?></label>
                                    <input type="text" class="form-control"  id="username" name="username"  placeholder="">
                                </div>
                                <div class="form-group">
                                    <label for="email"><?php echo $this->lang->line('email') ?></label>
                                    <input type="text" class="form-control"  id="email" name="email"  placeholder="">
                                </div>
                                <div class="form-group">
                                    <label for="password"><?php echo $this->lang->line('password') ?></label>
                                    <input type="password" class="form-control"  id="password" name="password"  placeholder="">
                                </div>
                                <div class="form-group">
                                    <label for="re_pass"><?php echo $this->lang->line('re_password') ?></label>
                                    <input type="password" class="form-control"  id="re_pass" name="re_pass"  placeholder="">
                                </div>
                                <div class="form-group" id="form_img">
                                  <label for="profile-img">画像（サムネイル表示）</label>
                                  <p>※画像のサイズは300px＊300pxで最高です。容量 2MBまで 。</p>
                                  <input type="file" name="file" id="profile-img-user">
                                  <input type="hidden" name="hiden_image" id="hiden_image" >
                                  <p id="announce" style="color:red" ></p>
                                  <p style="text-align: center;"><img src="" id="profile-img-tag"/ width="300px" alt=""></p>
                                </div>
                            </div>
                            <!-- /.box-body -->
                        </div>
                    </div>
                    <!-- /.tab-content -->
                </div>


                <div class="box-footer">
                    <button type="submit" name="save" class="btn btn-primary new_user" value="1"><?php echo $this->lang->line('save'); ?></button>
                    <a href="<?php echo site_url('/news/') ?>" class="btn btn-default"><?php echo $this->lang->line('cancel'); ?></a>
                </div>
            </form>
        </div>

    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
