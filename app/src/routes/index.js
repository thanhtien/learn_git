import React from 'react';
//React Router
import { Route , Switch } from 'react-router-dom';
import App from '../components/app';
import ErrorPage from '../components/error';
import Signout from '../components/auth/signout';
import RequireAuth from '../components/auth/require_auth';
import Loading from '../components/common/loading';
import ScrollToTop from './ScrollToTop';
import SettingUser from '../components/my-profile/SettingUser/';
import { TransitionGroup, CSSTransition } from "react-transition-group";


function asyncComponent(getComponent) {
  return class AsyncComponent extends React.Component {
    static Component = null;
    state = { Component: AsyncComponent.Component };

    componentWillMount() {
      if (!this.state.Component) {
        getComponent().then(Component => {
          AsyncComponent.Component = Component;
          this.setState({ Component });
        })
      }
    }
    render() {
      const { Component } = this.state
      if (!Component) {
        return <Loading/>
      }
      if (Component) {
        return <Component {...this.props} />
      }
      return null
    }
  }
}
const MailBox = asyncComponent(() =>
    import('../components/my-profile/MailBox/').then(module => module.default)
)
const project100List = asyncComponent(() =>
    import('../components/project100List').then(module => module.default)
)
const EmailSended = asyncComponent(() =>
    import('../components/my-profile/MailBox/EmailSended').then(module => module.default)
)

const CreateEmail = asyncComponent(() =>
    import('../components/my-profile/MailBox/CreateEmail/').then(module => module.default)
)

const CreateEmailToUser = asyncComponent(() =>
    import('../components/my-profile/MailBox/CreateEmailToUser/').then(module => module.default)
)

const ComposerMail = asyncComponent(() =>
    import('../components/my-profile/MailBox/ComposerMail').then(module => module.default)
)

const DetailAdminNotification = asyncComponent(() =>
    import('../components/my-profile/detailAdminNotification/').then(module => module.default)
)
const NotificationList = asyncComponent(() =>
    import('../components/notificationList').then(module => module.default)
)
const WishList = asyncComponent(() =>
    import('../components/WishList').then(module => module.default)
)
const DonateNonLogin = asyncComponent(() =>
    import('../components/StripePayNonLogin').then(module => module.default)
)

const FanClubList = asyncComponent(() =>
    import('../components/project-fanclub-list').then(module => module.default)
)

const FirstUser = asyncComponent(() =>
    import('../components/first-user').then(module => module.default)
)

const Page404 = asyncComponent(() =>
    import('../components/staticPage/404Page/').then(module => module.default)
)
const wifiError = asyncComponent(() =>
    import('../components/staticPage/networkPage/').then(module => module.default)
)
const Page401 = asyncComponent(() =>
    import('../components/staticPage/401Page/').then(module => module.default)
)

const Welcome = asyncComponent(() =>
    import('../components/welcome/').then(module => module.default)
)

const StripePay = asyncComponent(() =>
    import('../components/StripePay').then(module => module.default)
)
const PrivacyPolicy = asyncComponent(() =>
    import('../components/staticPage/PrivacyPolicy/').then(module => module.default)
)

const AllGui = asyncComponent(() =>
    import('../components/staticPage/AllGui/').then(module => module.default)
)
const Legal = asyncComponent(() =>
    import('../components/staticPage/Legal/').then(module => module.default)
)
const Guideline = asyncComponent(() =>
    import('../components/staticPage/Guideline/').then(module => module.default)
)
const Term = asyncComponent(() =>
    import('../components/staticPage/Term/').then(module => module.default)
)

const NewDetail = asyncComponent(() =>
    import('../components/news/detail').then(module => module.default)
)

const ThreeGirl = asyncComponent(() =>
    import('../components/staticPage/ThreeGirl/ThreeGirlMain').then(module => module.default)
)
const NewsList = asyncComponent(() =>
    import('../components/news/').then(module => module.default)
)

const searchResult = asyncComponent(() =>
    import('../components/SearchResult/').then(module => module.default)
)
const enoughGoalAmount = asyncComponent(() =>
    import('../components/enoughGoalAmount/').then(module => module.default)
)

const EditProject = asyncComponent(() =>
    import('../components/my-profile/edit-project/').then(module => module.default)
)
const NewProject = asyncComponent(() =>
    import('../components/new-project/').then(module => module.default)
)
const ProjectEnoughExpired = asyncComponent(() =>
    import('../components/ProjectEnoughExpired/').then(module => module.default)
)

const projectHistory = asyncComponent(() =>
    import('../components/project-expired-list/').then(module => module.default)
)
const AddNewProject = asyncComponent(() =>
    import('../components/my-profile/new-project/').then(module => module.default)
)
const StatisticProject = asyncComponent(() =>
    import('../components/my-profile/StatisticProject/').then(module => module.default)
)
const FinishSocial = asyncComponent(() =>
    import('../components/auth/finishSocial').then(module => module.default)
)
const Signin = asyncComponent(() =>
    import('../components/auth/signin').then(module => module.default)
)
const forgot = asyncComponent(() =>
    import('../components/auth/forgotPassword').then(module => module.default)
)
const resetPassword = asyncComponent(() =>
    import('../components/auth/resetPassword').then(module => module.default)
)
const Signup = asyncComponent(() =>
    import('../components/auth/signup').then(module => module.default)
)
const CreateUser = asyncComponent(() =>
    import('../components/auth/createUser').then(module => module.default)
)
const ProjectDetail = asyncComponent(() =>
    //import('../components/project-detail/').then(module => module.default)
    import('../components/project-detail/ProjectType').then(module => module.default)
)
const projectDetailMyPage = asyncComponent(() =>
    import('../components/my-profile/project-detail/').then(module => module.default)
)
const TowaKakuseida = asyncComponent(() =>
    import('../components/staticPage/TowaKakuseida/').then(module => module.default)
)
const Contact = asyncComponent(() =>
    import('../components/staticPage/contact/').then(module => module.default)
)
const projectListCatSlug = asyncComponent(() =>
    import('../components/ProjectsListCatSlug/').then(module => module.default)
)
const projectListFromType = asyncComponent(() =>
    import('../components/ProjectsListFromType/').then(module => module.default)
)
const ProjectSupported = asyncComponent(() =>
    import('../components/my-profile/supported-project').then(module => module.default)
)
const ProjectSupportedFan = asyncComponent(() =>
    import('../components/my-profile/supported-project-fan').then(module => module.default)
)
const PostProject = asyncComponent(() =>
    import('../components/my-profile/post-project/').then(module => module.default)
)
const EditUser = asyncComponent(() =>
    import('../components/my-profile/SettingUser/edit-profile').then(module => module.default)
)
const AddressReturn = asyncComponent(() =>
    import('../components/my-profile/SettingUser/address-return').then(module => module.default)
)
const bankAccout = asyncComponent(() =>
    import('../components/my-profile/SettingUser/bankAccout').then(module => module.default)
)
const bankAccoutATM = asyncComponent(() =>
    import('../components/my-profile/SettingUser/AtmAccout').then(module => module.default)
)
const ChangePassword = asyncComponent(() =>
    import('../components/my-profile/SettingUser/change-password').then(module => module.default)
)
const ReportList = asyncComponent(() =>
    import('../components/my-profile/StatisticProject/newsProject').then(module => module.default)
)
const ReportNew = asyncComponent(() =>
    import('../components/my-profile/StatisticProject/newsProject/add').then(module => module.default)
)

const ReportEdit = asyncComponent(() =>
    import('../components/my-profile/StatisticProject/newsProject/edit').then(module => module.default)
)

const ReportNewPublic= asyncComponent(() =>
    import('../components/project-detail/newsProject/publicDetail').then(module => module.default)
)
const EditProjectPublic= asyncComponent(() =>
    import('../components/my-profile/edit-project-public').then(module => module.default)
)
const EditProjectPublicAdd= asyncComponent(() =>
    import('../components/my-profile/edit-project-public/add').then(module => module.default)
)


const kakuseitowa= asyncComponent(() =>
    import('../components/staticPage/kakuseitowa').then(module => module.default)
)
const daihyouAisatsu= asyncComponent(() =>
    import('../components/staticPage/daihyouAisatsu').then(module => module.default)
)
const activePassword= asyncComponent(() =>
    import('../components/staticPage/activePassword').then(module => module.default)
)
const company= asyncComponent(() =>
    import('../components/staticPage/company').then(module => module.default)
)
const access= asyncComponent(() =>
    import('../components/staticPage/access').then(module => module.default)
)
const SubFAQBtn1= asyncComponent(() =>
    import('../components/staticPage/SubFAQBtn/SubFAQBtn1').then(module => module.default)
)
const FirstUserFanClub= asyncComponent(() =>
    import('../components/staticPage/FirstUserMore/FirstUserFanClub').then(module => module.default)
)





const AppRoute = ({ socket ,component: Component, layout: Layout, ...rest }) => (
    <Route {...rest} render={props => (
        <Layout socket={socket}>
          <Component {...props} socket={socket} />
        </Layout>
    )} />
)

const AppRouteError = ({ socket ,component: Component, layout: Layout, ...rest }) => (
    <Route {...rest} render={props => (
        <Layout>
          <Component {...props}/>
        </Layout>
    )} />

)


const Routes = (location) => {

  return (
      <ScrollToTop>
        <TransitionGroup className="transition-group">
          <CSSTransition
              key={location.location.key}
              timeout={{ enter: 300, exit: 300 }}
              classNames={'fade'}
          >
            <Switch>

              {/*Auth Router*/}
              <AppRoute exact path="/signin" socket={location.socket} component={Signin} layout={App} />
              <Route exact path="/signout" socket={location.socket} component={Signout} />
              <Route exact path="/signup" socket={location.socket} component={Signup} />
              <AppRoute exact path="/forgot-password/" layout={App} socket={location.socket} component={forgot} />
              <AppRoute exact path="/reset_pass/:code" socket={location.socket} component={resetPassword} layout={App}/>




              <AppRoute exact path="/create-user/:token" socket={location.socket} component={CreateUser} layout={App}/>

              <AppRoute exact path="/create-social/:token" socket={location.socket} component={FinishSocial} layout={App}/>


              {/*Auth Router*/}






              {/*User Router*/}
              <AppRoute exact path="/" layout={App} socket={location.socket} component={Welcome} />
              <AppRoute exact path="/fanclub-list/:page" layout={App} socket={location.socket} component={FanClubList} />
              {/*<AppRoute exact path="/project-detail/:id" layout={App} socket={location.socket} component={ProjectDetail} />*/}
                <AppRoute exact path="/project-detail/:id" layout={App} socket={location.socket} component={ProjectDetail} />
              <AppRoute exact path="/news-list/:page" layout={App} socket={location.socket} component={NewsList} />
              <AppRoute exact path="/news-list/new-detail/:id"  layout={App} socket={location.socket} component={NewDetail} />

              <AppRoute exact path="/static/ThreeGirl" layout={App} socket={location.socket} component={ThreeGirl} />

              <AppRoute exact path="/project-enough-expired-list/:page" layout={App} socket={location.socket} component={ProjectEnoughExpired} />
              <AppRoute exact path="/project-expired-list/:page" layout={App} socket={location.socket} component={projectHistory} />
              <AppRoute exact path="/project-enough-goal-amount-list/:page" layout={App} socket={location.socket} component={enoughGoalAmount} />
              <AppRoute exact path="/categories/:slug/:page" layout={App} socket={location.socket} component={projectListCatSlug} />
              <AppRoute exact path="/project-from-type/:slug/:page" layout={App} socket={location.socket} component={projectListFromType} />


              <AppRoute exact path="/notification/:page" layout={App} socket={location.socket}  component={RequireAuth(NotificationList)} />

              <AppRoute exact path="/my-page/emailBox/:page" layout={App} socket={location.socket}  component={RequireAuth(MailBox)} />


              <AppRoute exact path="/my-page/email-box/new/:project_id" layout={App} socket={location.socket}  component={RequireAuth(CreateEmail)} />


              <AppRoute exact path="/my-page/email-box/new-user/:user_id/project/:project_id" layout={App} socket={location.socket}  component={RequireAuth(CreateEmailToUser)} />



              <AppRoute exact path="/my-page/mail-compose/:page" layout={App} socket={location.socket}  component={RequireAuth(ComposerMail)} />

              <AppRoute exact path="/my-page/mail-sended/:page" layout={App} socket={location.socket}  component={RequireAuth(EmailSended)} />




              <AppRoute exact path="/my-page/admin-notification/:id" layout={App} socket={location.socket}  component={RequireAuth(DetailAdminNotification)} />

              <AppRoute exact path="/wishlist/:page" layout={App} socket={location.socket}  component={RequireAuth(WishList)} />
              <AppRoute exact path="/project100-list/:page" layout={App} socket={location.socket}  component={project100List} />




              <AppRoute exact path="/kakuseida/" layout={App} socket={location.socket} component={Term} />
              <AppRoute exact path="/kakuseida/privacy-policy" layout={App} socket={location.socket} component={PrivacyPolicy} />
              <AppRoute exact path="/kakuseida/common/:tabactive" layout={App} socket={location.socket} component={PrivacyPolicy} />
              <AppRoute exact path="/kakuseida/AllGui/:tabactive" layout={App} socket={location.socket} component={AllGui} />
              <AppRoute exact path="/kakuseida/SubFAQBtn1/" layout={App} socket={location.socket} component={SubFAQBtn1} />
              <AppRoute exact path="/first-user/first-user-fan-club/" layout={App} socket={location.socket} component={FirstUserFanClub} />
              <AppRoute exact path="/first-user/first-user-project/" layout={App} socket={location.socket} component={FirstUserFanClub} />


              <AppRoute exact path="/kakuseida/legal/" layout={App} socket={location.socket} component={Legal} />
              <AppRoute exact path="/first-user/guideline/" layout={App} socket={location.socket} component={AllGui} />
              <AppRoute exact path="/first-user/how-to-create-project" layout={App} socket={location.socket} component={AllGui} />
              <AppRoute exact path="/first-user/faq/" layout={App} socket={location.socket} component={AllGui} />
              <AppRoute exact path="/kakuseida/Term/" layout={App} socket={location.socket} component={Term} />
              <AppRoute exact path="/kakuseida/Contact/" layout={App} socket={location.socket} component={Contact} />

              <AppRoute exact path="/search-result/" layout={App} socket={location.socket} component={searchResult} />
              <AppRoute exact path="/my-page/project-supported/:page" layout={App} socket={location.socket} component={RequireAuth(ProjectSupported)} />
              <AppRoute exact path="/my-page/project-supported-fan/:page" layout={App} socket={location.socket} component={RequireAuth(ProjectSupportedFan)} />


              <AppRoute exact path="/my-page/post-project/:page" layout={App} socket={location.socket} component={RequireAuth(PostProject)} />

              <AppRoute exact  path="/my-page/setting-user/" layout={App} socket={location.socket} component={RequireAuth(SettingUser)} />


              <AppRoute exact  path="/my-page/setting-user/edit-profile" layout={App} socket={location.socket} component={RequireAuth(EditUser)} />
              <AppRoute exact  path="/my-page/subcriber/:subcriber" layout={App} socket={location.socket} component={RequireAuth(EditUser)} />

              <AppRoute exact path="/my-page/setting-user/address-return" layout={App} socket={location.socket} component={RequireAuth(AddressReturn)} />
              <AppRoute exact path="/my-page/setting-user/credit-accounts" layout={App} socket={location.socket} component={RequireAuth(bankAccout)} />
              <AppRoute exact path="/my-page/setting-user/bank-accounts-project-success" layout={App} socket={location.socket} component={RequireAuth(bankAccoutATM)} />
              <AppRoute exact path="/my-page/setting-user/change-password" layout={App} socket={location.socket} component={RequireAuth(ChangePassword)} />

              <AppRoute exact path="/my-page/project-detail/:id" layout={App} socket={location.socket} component={RequireAuth(projectDetailMyPage)} />

              <AppRoute exact path="/my-page/edit-project-public/:id/:page" layout={App} socket={location.socket} component={RequireAuth(EditProjectPublic)} />

              <AppRoute exact path="/my-page/project-public/add-new-request/:id" layout={App} socket={location.socket} component={RequireAuth(EditProjectPublicAdd)} />



              <AppRoute exact path="/my-page/add-new-project" layout={App} socket={location.socket} component={RequireAuth(AddNewProject)} />


              <AppRoute exact path="/my-page/edit-project/:id" layout={App} socket={location.socket} component={RequireAuth(EditProject)} />
              <AppRoute exact path="/my-page/statistic/:id" layout={App} socket={location.socket} component={RequireAuth(StatisticProject)} />
              <AppRoute exact path="/my-page/statistic/:id/report-list/:page" layout={App} socket={location.socket} component={RequireAuth(ReportList)} />
              <AppRoute exact path="/my-page/statistic/:id/report-new" layout={App} socket={location.socket} component={RequireAuth(ReportNew)} />
              <AppRoute exact path="/my-page/statistic/:id/report-edit/:new_id" layout={App} socket={location.socket} component={RequireAuth(ReportEdit)} />
              <AppRoute exact path="/new-project-list/:page" layout={App} socket={location.socket} component={NewProject} />
              <AppRoute exact path="/projects/:projectID/backers/:backerId" layout={App} socket={location.socket} component={RequireAuth(StripePay)} />
              <AppRoute exact path="/projects-incognito/:projectID/backers/:backerId" layout={App} socket={location.socket} component={DonateNonLogin} />

              <AppRoute exact path="/project-detail/:id/news-detail/:idNew" layout={App} socket={location.socket} component={ReportNewPublic} />


              <AppRoute exact path="/operating-company/kakuseitowa" layout={App} socket={location.socket} component={kakuseitowa} />
              <AppRoute exact path="/operating-company/daihyou-aisatsu" layout={App} socket={location.socket} component={daihyouAisatsu} />
              <AppRoute exact path="/active-password/:slug" layout={App} socket={location.socket} component={activePassword} />
              <AppRoute exact path="/operating-company/company" layout={App} socket={location.socket} component={company} />
              <AppRoute exact path="/operating-company/access" layout={App} socket={location.socket} component={access} />
              <AppRoute exact path="/kakuseida/kakuseida-towa/" layout={App} socket={location.socket} component={TowaKakuseida} />
              {/*User Router*/}



              <AppRoute exact path="/first-user" layout={App} socket={location.socket} component={FirstUser} />

              <AppRouteError exact path="/unauthorized" layout={ErrorPage}  component={Page401} />
              <AppRouteError exact path="/connect-error" layout={ErrorPage}  component={wifiError} />
              <AppRouteError layout={ErrorPage}  component={Page404} />



            </Switch>
          </CSSTransition>
        </TransitionGroup>
      </ScrollToTop>
  );
};

export default Routes;
