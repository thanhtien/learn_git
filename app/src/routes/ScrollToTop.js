import { withRouter } from 'react-router'
import  {Component} from 'react';
class ScrollToTop extends Component {
  componentDidUpdate(prevProps) {


    if (this.props.location !== prevProps.location) {

      setTimeout(function () {
        window.scrollTo(0, 0)
      }, 300);
    }
  }

  render() {
    return this.props.children
  }
}

export default withRouter(ScrollToTop)
