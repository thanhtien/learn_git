import 'babel-polyfill';
import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware, compose } from 'redux';
import reduxThunk from 'redux-thunk';
import { BrowserRouter, Router, Switch } from 'react-router-dom';
import History from './history.js';
import Routes from './routes';
import { AUTH_USER } from './actions/types';
import { initializeFirebase } from './config/firebase';
import io from 'socket.io-client/dist/socket.io';
// import logger from 'redux-logger';
import rootReducer from './reducers';



var CONFIG = require('./config/common');
if (process.env.NODE_ENV === "development") {
  CONFIG = CONFIG.CONFIG.DEV;
} else {
  CONFIG = CONFIG.CONFIG.PRODUCT;
}
// Connect Socket IO

const socket = io(CONFIG.SOCKET_URL);


function getInternetExplorerVersion() {
  var rv = -1;
  var ua = '';
  var re = '';
  if (navigator.appName === 'Microsoft Internet Explorer')
  {
    ua = navigator.userAgent;
    re  = new RegExp(`MSIE ([0-9]{1,}[.0-9]{0,})`);
    if (re.exec(ua) !== null)
      rv = parseFloat( RegExp.$1 );
  }
  else if (navigator.appName === 'Netscape')
  {
    ua = navigator.userAgent;
    re  = new RegExp(`Trident/.*rv:([0-9]{1,}[.0-9]{0,})`);
    if (re.exec(ua) !== null)
      rv = parseFloat( RegExp.$1 );
  }


  if (rv !== -1) {
    localStorage.setItem('ie', 'true');
  }else {
    localStorage.setItem('ie', 'false');
  }

}
getInternetExplorerVersion();

const store = createStore(
  rootReducer,
    //, window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
     compose(applyMiddleware(reduxThunk))
);

//Token Local Storage
const token = localStorage.getItem('token');

// if we have a token, consiger the user to be signed in
if (token) {
  store.dispatch({ type: AUTH_USER });
}



ReactDOM.render(


  <Provider store={store}>

    <BrowserRouter>
      <Router history={History} >
        <Switch>
          <Routes socket={socket}/>
        </Switch>
      </Router>
    </BrowserRouter>
  </Provider>, document.getElementById('root')

);

if (token) {
  initializeFirebase();
}
// we need to update application state
getInternetExplorerVersion();
