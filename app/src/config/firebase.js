
import firebase from 'firebase/app';
import 'firebase/app';
import 'firebase/messaging';
import axios from 'axios';
var CONFIG = require('./common');

if (process.env.NODE_ENV === "development") {
  CONFIG = CONFIG.CONFIG.DEV;
}else {
  CONFIG = CONFIG.CONFIG.PRODUCT;
}
const ROOT_URL = CONFIG.ROOT_URL;
const tokenAuth = 'Bearer '+ localStorage.getItem('token');

var config = {
  apiKey: "AIzaSyBMA7D0phhzgIFrOWLRsS7zpc_zJMdkWyM",
   authDomain: "kakuseida-4c9ff.firebaseapp.com",
   databaseURL: "https://kakuseida-4c9ff.firebaseio.com",
   projectId: "kakuseida-4c9ff",
   storageBucket: "kakuseida-4c9ff.appspot.com",
   messagingSenderId: "74226131022"
};



export const initializeFirebase = () => {
  firebase.initializeApp(config);

  // Let's check if the browser supports notifications
  if (!("Notification" in window)) {
    console.log("This browser does not support desktop notification");
    return false;
  }
  if (Notification.permission !== 'denied' || Notification.permission === "default") {
    //Send Token Here
    askForPermissioToReceiveNotifications();
  }


}

export const askForPermissioToReceiveNotifications = async () => {
  try {
    const messaging = firebase.messaging();
    await messaging.requestPermission();
    const token = await messaging.getToken();
    if (token) {
      var config = {
        headers: {
          "Authorization" : tokenAuth
        }
      };
      var data = {
        token:token
      }
      //Save Token Firebase To server
      axios.post(`${ROOT_URL}v2/firebase/sendTokenToServer`, data , config)
        .then(response => {

        }).catch((error) => {
          console.log(error.data);
      });
    }
    return token;
  } catch (error) {
    console.error(error);
  }
}
