/* @flow */

import React, { Component } from 'react';


export default class MyComponent extends Component {
  render() {
    return (
      <div className="blank inner">
        <p>{this.props.data}</p>
      </div>
    );
  }
}
