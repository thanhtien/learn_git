/* @flow */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import Loading from '../../common/loading';
import * as actions from '../../../actions/projectDetail';
import Banner from './banner';
import InforProject from './inforProject';
import ProjectReturn from './projectReturn';
import smoothScroll from '../../../components/common/smoothScroll';

class ProjectDetail extends Component {

  constructor(props) {
    super(props);
    this.state = {
      tab:1
    };
  }

  tagChange(tab) {
    this.setState({
      tab:tab
    })
  }

  tagChangeBanerScroll(){
    this.setState({
      tab:1
    }, () => {
      smoothScroll.scrollTo('project_return');

    })
  }

  componentDidMount() {
    const {id} = this.props.match.params;
    this.props.ProjectDetailMypageGet(id);
  }

  htmlReturn(data) {
    return{
      __html:data
    }
  }

  render() {
    const {data,loading,profile,sendProjectUnActive} = this.props;
    const {tab} = this.state;

    if (loading) {
      return(
        <Loading></Loading>
      )
    }

    if (data) {
      document.title = data.project.project_name+"｜KAKUSEIDA";
    }



      return (
        <div>
          {
            data  ? <div>

              <div className="pd-banner">
                {<Banner sendProjectUnActive={sendProjectUnActive} tagChange={ () => this.tagChangeBanerScroll() } tab={tab} data={data}></Banner>}
              </div>
              <div className="wraper">
                  <div className="tab-area">
                      <ul>
                          <li onClick={ () => this.tagChange(1) } className={ tab === 1 ? "active":""}>プロジェクト詳細</li>
                          <li onClick={ () => this.tagChange(2) } className={ tab === 2 ? "active":""}>リターン</li>
                      </ul>
                  </div>
                  {/* .tab-area */}
                  <div className={ tab === 1 ? "active pd-area animated fadeIn":"pd-area"}  id="tab-2">
                    {
                      data.project.project_content ?
                      <div className="div-content" dangerouslySetInnerHTML={this.htmlReturn(data.project.project_content)} ></div>
                      : <p style={{textAlign:'center', border: "3px dotted #0071BA",padding:"10px"}}>コンテンツがまだありません。</p>
                    }


                    {/* .WYSIWYG */}
                  </div>

                  {/* #tab-1 */}
                  <div className={ tab === 2 ? "active pd-area animated fadeIn":"pd-area"} id="tab-1">
                    <InforProject tagChange={() => this.tagChange(1)} data={data.project}></InforProject>
                    <div className="pd-box pd-box-2">
                      {
                        data.project_return.length !== 0 ?
                        <h4 id="project_return" className="pd-box-title">リターンを選ぶ</h4>:
                        null
                      }

                        <div className="return-list">
                          {
                            profile ?
                            <ProjectReturn
                              owner={data.project.user_id}
                              number_month={data.project.number_month}
                              currentUser={profile.id}
                              data={data.project_return}></ProjectReturn> :
                            null
                          }

                        </div>
                    </div>
                  </div>
                  {/* #tab-2 */}
              </div>
            </div> : <Loading></Loading>
          }

        </div>
      );


  }
}

const mapStateToProps = (state) => {
  return {
    data: state.ProjectDetail.projectDetailMypage,
    loading:state.common.loading,
    profile:state.common.profile,
  }
}

export default connect(mapStateToProps, actions)(ProjectDetail);
