/* @flow */

import React, { Component } from 'react';
import ReactPaginate from 'react-paginate';

import LoadingScroll from '../../common/loadingScroll';
import FormatFunc from '../../common/FormatFunc';
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faChevronLeft, faChevronRight} from "@fortawesome/free-solid-svg-icons";


export default class Table extends Component {

  handlePageClick = (data) => {
    let selected = data.selected;
    const {id} = this.props;
    this.props.notificationList(id,selected);
  };



  render() {
    const {NotificationData} = this.props;
    return (
      <div className="static-noti">
        <h1 className="title-noti">お知らせ一覧</h1>
        <ul className="personal-panel-width personal-panel notification-list" style={{display:"block",overflow: "hidden" , height:'auto'}}>
          {
            NotificationData && NotificationData.data.length > 0 ?
            NotificationData.data.map((item,i)=>{


              return (
                <li className="static-noti-mobi" key={i} style={{background: '#f8f8f8'}}>
                  <span   className="notification">
                    <div className="notification-image">
                      <img src={item.thumbnail} alt="thum" />
                    </div>
                    <div className="notification-text">
                      <span className="text-name"> </span> {item.message}
                      <p className="calendar-time">
                        <span className="time-text">
                          <FormatFunc date={item.created}></FormatFunc>
                        </span>
                      </p>
                    </div>
                  </span>
                </li>
              )

            }):
            <li className="noti-list">
              <p>通知がまだありません。</p>
            </li>



          }
        </ul>
        <div className="cover-paginate noti">
          {
            this.props.loading?<div className="loading-io"><LoadingScroll></LoadingScroll></div>:null
          }
          {
            NotificationData && NotificationData.data.length > 0 ?
            <ReactPaginate
              previousLabel={<FontAwesomeIcon icon={faChevronLeft}/>}
              nextLabel={<FontAwesomeIcon icon={faChevronRight}/>}
              breakLabel={<span>...</span>}
              breakClassName={"break-me"}
              pageCount={NotificationData.page_count}
              marginPagesDisplayed={2}
              pageRangeDisplayed={2}
              onPageChange={this.handlePageClick}
              containerClassName={"pagination"}
              subContainerClassName={"pages pagination"}
              activeClassName={"active"} /> : null
          }
        </div>
        <div className="clear-fix"></div>
      </div>
    );
  }
}
