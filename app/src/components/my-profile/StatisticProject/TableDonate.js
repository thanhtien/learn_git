/* @flow */

import React, { Component } from 'react';
import ReactPaginate from 'react-paginate';
import NumberFormat from 'react-number-format';
import Rodal from 'rodal';
import ProjectReturn from './projectReturn';
import Loading from '../../common/loading';
import FormatFunc from '../../common/FormatFunc';
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faChevronLeft, faChevronRight} from "@fortawesome/free-solid-svg-icons";


export default class Table extends Component {
  constructor(props) {
    super(props);
    this.state = {
      backingData:null,
      showPopUp:false,
      loadingMobile:false

    };
  }
  _renderStatus(status) {
    if (status === 'succeeded') {
      return(
        <span style={{color:'#0e6eb8'}}>支援済み</span>
      )
    }else if(status === 'unsuccess') {
      return (
        <span style={{color:'red'}}>振込待ち</span>
      )
    }else if( status === 'refunded' ) {
      return (
        <span>返金済み</span>
      )
    }
  }
  /**
   * [handlePageClick page number table]
   * @param  {[type]} data [data number page]
   * @return {[type]}      [data for table]
   */
  handlePageClick(data){
    let selected = data.selected;
    this.props.getprojectStatisticProps(this.props.id,selected);
  }
  /**
   * [hideModal hidemodal Project Return detail]
   * @return {[type]} [Hide Modal]
   */
  hideModal(){
    this.setState({
      showPopUp:false
    })
    document.body.className = '';

  }
  /**
   * [setDataModal Set data for modal Show]
   */
  setDataModal(data,type,id,sendPackage,number_month) {

    this.setState({
      backingData:data,
      UserType:type,
      id_donate:id,
      sendPackage:sendPackage,
      number_month:number_month,
      id:data.id


    },function(){
      this.setState({
        showPopUp:true
      })
    })
    document.body.className = 'block-body';
  }

  /**
   * [_renderTotalProject ForRender Project Total]
   * @param  {[type]} data [data]
   * @return {[type]}      [layout project total]
   */
  _renderTotalProject(data) {

    return(
      <div className="box-total">
        <p>
          <span className="label-box">合計:</span>
          <span className="green text">
            <NumberFormat value={data.collected_amount} displayType={'text'} thousandSeparator={true} suffix={'円'} />

          </span>
        </p>
        {
          data.goal_amount ?
          <p>
            <span className="label-box" >目標金額:</span>
            <span className="text">

              <NumberFormat value={data.goal_amount} displayType={'text'} thousandSeparator={true} suffix={'円'} />

            </span>
          </p>
          : null
        }
        {
          this.props.typeProject !== 'fanclub' ?
          <p>
            <span className="label-box">残り:</span>
            <span className="red text">
              <NumberFormat value={data.total} displayType={'text'} thousandSeparator={true} suffix={'円'} />
            </span>
          </p> : null
        }


        <p></p>
      </div>
    )
  }
  _renderTypeCard(item) {
    if (item.toLowerCase() === 'credit') {
      return "クレジットカード";
    }else if( item.toLowerCase() === 'bank' ) {
      return "銀行振込";
    }

  }

  ConfirmReturnEmail(id_donate,email){
    var dataSendAPI = {
      email:email,
      id:id_donate
    };


    this.props.ConfirmReturnEmail(dataSendAPI,()=>this.LoadingSet(),()=>this.hideModal());
  }
  LoadingSet(){
    this.setState({
      loadingMobile:!this.state.loadingMobile
    })
  }
  render() {
    const {statistic_project,typeProject} = this.props;
    var DetectedMotile = false;
    if (window.innerWidth <= 768) {
      DetectedMotile=true;
    }




    return (
      <div className="cover-static">
      <table className="static table-fix-width">
        <caption className="static-cap">支援一覧</caption>
        <thead>
          <tr>
            <th scope="col">ユーザー名</th>
            <th scope="col">支援者氏名</th>
            <th scope="col">メールアドレス</th>
            <th scope="col">
              {
                typeProject === 'fanclub' ? '定期課金間隔' : '支援日'
              }
            </th>
            <th scope="col">支払い方法</th>
            <th scope="col">支援金額</th>
            <th scope="col">入金確認</th>
            <th scope="col">リターン配送</th>
          </tr>
        </thead>
        <tbody>
          {

            statistic_project ?
            statistic_project.backed.map( (item , i ) => {



              return(
                <tr style={{position: 'relative'}} key={i}
                  onClick={ !DetectedMotile? () => this.setDataModal(item.backing,item.status_user,item.id,item.sendPackage,item.number_month) : null }>
                  {
                    this.state.loadingMobile ?
                    <td  className="sp tdloading">
                      <div>
                        <Loading></Loading>
                      </div>

                    </td> : null
                  }


                  <td style={item.blockUser === 1 ? {color:'#ddd'} : null} data-label="ユーザー名" title={item.username}>
                    {
                      item.username
                    }
                    {
                      typeProject !== 'fanclub' && item.status_user === 'not_user' ? '(会員未登録)' : null
                    }
                    {
                      item.blockUser === 1 ? " (退会済み)" : null
                    }
                  </td>

                  <td data-label="支援者氏名" title={item.name_return}>
                    {
                      item.name_return
                    }

                  </td>

                  <td data-label="メールアドレス" title={item.email}>
                    {
                      item.email
                    }

                  </td>


                  <td data-label={typeProject === 'fanclub' ? '定期課金間隔' : '支援日'}>
                    {
                      typeProject === 'fanclub' ? `${item.number_month}カ月` :  <FormatFunc date={item.created}></FormatFunc>
                    }
                  </td>
                  <td data-label="支払い方法">
                    {
                      this._renderTypeCard(item.type)
                    }
                  </td>
                  <td data-label="支援金額">
                    {
                      <NumberFormat value={item.invest_amount} displayType={'text'} thousandSeparator={true} suffix={'円'} />
                    }
                  </td>
                  <td data-label="入金確認">
                    {
                      this._renderStatus(item.status)
                    }
                  </td>
                  <td data-label="リターン配送">
                    {
                      item.sendPackage === '1' ? '配送済み' : '未配送'
                    }
                  </td>
                  <td className="sp">
                    {
                      item.sendPackage !== '1' ?
                      <button onClick={()=>this.ConfirmReturnEmail(item.id,item.backing.email)} className="link-to-project-detail button-donor" >リターン品を送信しました</button> : <span className="link-to-project-detail button-donor" style={{backgroundColor: '#ddd'}}>リターン品を送信しました</span>
                    }
                  </td>
                </tr>
              )
            }) : null

          }

        </tbody>
      </table>

      <br/>
      {
        statistic_project ?
        <ReactPaginate
          previousLabel={<FontAwesomeIcon icon={faChevronLeft}/>}
          nextLabel={<FontAwesomeIcon icon={faChevronRight}/>}
          breakLabel={<span>...</span>}
          breakClassName={"break-me"}
          pageCount={statistic_project.page_count}
          marginPagesDisplayed={2}
          pageRangeDisplayed={2}
          onPageChange={(data)=>this.handlePageClick(data)}
          containerClassName={"pagination"}
          subContainerClassName={"pages pagination"}
          activeClassName={"active"}
        /> : null
      }
      <div className="">
        {
          this.state.backingData ?
          <Rodal className="rodal-custom" measure="%" width={70} height={60} visible={this.state.showPopUp} onClose={()=>this.hideModal()}>
            <div className="scroll-modal">
              <ProjectReturn
                number_month={this.state.number_month}
                sendPackage={this.state.sendPackage}
                onClose={()=>this.hideModal()}
                id_donate={this.state.id_donate}
                UserType={this.state.UserType}
                data={this.state.backingData} />
            </div>
          </Rodal>:
          null
        }
      </div>

      {
        statistic_project ?
        this._renderTotalProject(statistic_project.totalProject) :
        null
      }

      </div>
    );
  }
}
