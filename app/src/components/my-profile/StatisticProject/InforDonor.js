/* @flow */

import React, { Component } from 'react';
import { connect } from 'react-redux'
import * as actions from '../../../actions/editSystem';
import { reduxForm } from 'redux-form';
import Loading from '../../common/loading';


class InforDonor extends Component {


  constructor(props) {
    super(props);
    this.state = {
      loading:false
    };
  }
  handleFormSubmit(){
    const {data,id_donate,onClose} = this.props;

    var dataSendAPI = {
      email:data.email,
      id:id_donate
    };

    this.props.ConfirmReturnEmail(dataSendAPI,()=>this.LoadingSet(),onClose);
  }

  LoadingSet() {
    this.setState({
      loading:!this.state.loading
    })
  }






  render() {
    const {handleSubmit,data,UserType,sendPackage} = this.props;

    const {loading} = this.state;
    if(loading){
      return (<Loading></Loading>);
    }

    return (
      <div className="address_return_module infor-donor">
        <div className="page-tab-form">
          <div id="default-address">
            {
              UserType !== 'not_user' ?
              <form onSubmit={handleSubmit(this.handleFormSubmit.bind(this))}>
                <div className="form-area form-area-3">
                  <span className="page-tab-label">お届け先情報</span>
                  <div className="form-box">
                      <div className="form-field form-field-1">
                          <span className="form-field-name">支援者氏名</span>
                          <div className="form-field-content">
                            {
                              <span>
                                <span>{data.name_return}</span>
                              </span>
                            }

                          </div>
                      </div>
                      <div className="form-field form-field-2">
                          <span className="form-field-name">電話番号</span>
                          <div className="form-field-content">
                            {
                              <span>
                                <span>{data.phone}</span>
                              </span>
                            }

                          </div>
                      </div>

                      <div className="form-field form-field-4">
                          <span className="form-field-name">郵便番号</span>
                          <div className="form-field-content">

                            {
                              <span>
                                <span>{data.postcode}</span>
                              </span>
                            }

                          </div>
                      </div>
                      <div className="form-field form-field-5">
                          <span className="form-field-name">住所</span>
                          <div className="form-field-content">

                            {
                              <span>
                                <span>{data.address_return}</span>
                              </span>
                            }

                          </div>
                      </div>

                      <div className="form-field form-field-5">
                          <span className="form-field-name">メールアドレス</span>
                          <div className="form-field-content">

                            {
                              <span>
                                <span>{data.email}</span>
                              </span>
                            }

                          </div>
                      </div>


                  </div>
              </div>
                {
                  sendPackage !== '1' ?
                  <button className="link-to-project-detail button-donor" >リターン品を送信しました</button> : null
                }

              </form> :
              <form onSubmit={handleSubmit(this.handleFormSubmit.bind(this))}>
                <div className="form-area form-area-3">
                  <span className="page-tab-label">お届け先情報</span>
                  <div className="form-box">
                    <div className="form-field form-field-1">
                        <span className="form-field-name">支援者氏名</span>
                        <div className="form-field-content">
                          {
                            <span>
                              <span>{data.name_return}</span>
                            </span>
                          }

                        </div>
                    </div>

                    <div className="form-field form-field-5">
                        <span className="form-field-name">メールアドレス</span>
                        <div className="form-field-content">

                          {
                            <span>
                              <span>{data.email}</span>
                            </span>
                          }

                        </div>
                    </div>

                  </div>
              </div>
              {
                sendPackage !== '1' ? <button className="link-to-project-detail button-donor" >リターン品を送信しました</button> : <span className="link-to-project-detail button-donor" style={{backgroundColor: '#ddd'}}>リターン品を送信しました</span>
              }

              </form>
            }

          </div>
        </div>
      </div>

    );
  }
}

InforDonor = reduxForm({
  form: 'Send-Donate',
  destroyOnUnmount: false,
})(InforDonor)


// You have to connect() to any reducers that you wish to connect to yourself
InforDonor = connect(null, actions )(InforDonor);


export default InforDonor;
