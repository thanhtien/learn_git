/* @flow */

import React, { Component } from 'react';
import {  button } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {faChartLine,faEdit } from '@fortawesome/free-solid-svg-icons';


export default class Nav extends Component {
  setTab(tab) {
    this.props.setTab(tab)
  }
  render() {
    const {tab} = this.props;
    return (
      <ul className="navjs">
        <li>
          <button onClick={ () => this.setTab(1) }  className={ tab === 1 ? "active" : null }>
            <FontAwesomeIcon style={{fontSize:"16px"}}  icon={faChartLine} />
            <span>
              レポート
            </span>
          </button>
        </li>
        <li>
          <button onClick={ () => this.setTab(2) }  className={ tab === 2 ? "post-row active" : "post-row" }>
            <FontAwesomeIcon style={{fontSize:"16px"}}  icon={faEdit} />
            <span>
              プロジェクトの情報更新
            </span>
          </button>
        </li>


      </ul>
    )
  }
}
