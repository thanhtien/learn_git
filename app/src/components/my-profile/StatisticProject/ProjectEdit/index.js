/* @flow */
import React, { Component } from 'react';
import Tab from './tab';
import EditProjectForm from './Form';
import {  Link } from 'react-router-dom';
import Rodal from 'rodal';
import moment from 'moment';
import StopProjectForm from './StopProjectForm';

class EditProject extends Component {

  constructor(props) {
    super(props);
    this.state = {
      id:null,
      showHideStopProject:false
    };
  }

  StopProject() {
    const {id} = this.props;
    this.setState({
      id:id,
      showHideStopProject:true
    }, () => {
      document.body.className = 'block-body';
      document.getElementById("header").style.zIndex = -1;
    })

  }
  hidePopup() {
    this.setState({
      showHideStopProject:false

    },function () {
      document.body.className = '';
      document.getElementById("header").style.zIndex = 1;
    })

  }

  _renderStatusFanClub(item){
    const {id} = this.props;

    if(item.active === 'yes'){
      if(item.status_fanclub === 'progress'){
        return(
          <p className="blank-text-report">
            プロジェクトの情報が編集したい場合に、こちらの
            <Link to={`/my-page/edit-project-public/${id}/page=1`}>フォーム</Link>
            に入力してください。
          </p>
        );
      }else if(item.status_fanclub === 'wait'){
        return (null)
      }else if(item.status_fanclub === 'stop'){
        return (null)
      }
    }
  }

  _renderStatusNormal(item) {
    const {id} = this.props;
    if (item) {
      var day = moment(item.collection_end_date).diff(new Date(), 'days');

      if (day < 0) {
        return (null)
      }else {
        return (
          <p className="blank-text-report">
            プロジェクトの情報が編集したい場合に、こちらの
            <Link to={`/my-page/edit-project-public/${id}/page=1`}>フォーム</Link>
            に入力してください。
          </p>
        )
      }

    }
  }


  render() {
    const {id,project_default_edit,fanclubOrNormal,loading} = this.props;

    const {showHideStopProject} = this.state;
    var width;

    if (window.innerWidth > 767) {
      width = window.innerWidth * 60 / 100;
    }else {
      width = 300;
    }




    return (

      <div className="my-page">

        <Rodal width={width} height={520} visible={showHideStopProject} onClose={this.hidePopup.bind(this)}>

          <StopProjectForm
            number_month={project_default_edit.number_month}
            id={id}
            loading={loading}/>

        </Rodal>

        <div className="wraper">
          {
            fanclubOrNormal && fanclubOrNormal === 'fanclub' ? this._renderStatusFanClub(project_default_edit) : this._renderStatusNormal(project_default_edit)}


          {/* NAV Detail */}
          <div className="clear-fix"></div>
        </div>
        <div className="white-page">
          <div className="wraper">

            <div className="write-nav static-page-here">
              <Tab></Tab>
              {
                project_default_edit.project_type === '1' && project_default_edit.active === 'yes' && project_default_edit.status_fanclub === 'progress' ?
                <button onClick={ () => this.StopProject() } className="link-to-project-detail stop-project">定期課金のプロジェクトを解散する</button> : null
                /*Stop Project*/
              }
            </div>
            <div className="cover-no-blank new-project-here">
              <div className="wraper">
                <EditProjectForm></EditProjectForm>
                <div className="clear-fix"></div>
              </div>
            </div>
          </div>
        </div>


      </div>
    );
  }
}

export default EditProject;
