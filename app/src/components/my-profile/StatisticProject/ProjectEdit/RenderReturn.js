import React, { Component } from 'react';


export default class RenderReturn extends Component {
  constructor(props) {
    super(props)
    this.state = {
      stateDate:false,
      toogle:false
    }
    this.inputElement = React.createRef();
  }



  render() {
    const {data} = this.props;
    return (
      <div  className="render-feild">

        {data.map((project_return, index) => (
          <div key={index}>

            <table  className="my-table show">
              <tbody>
                <tr className="my-tr pc">
                  <th className="my-th">
                      <label className="integer optional" >支援額 </label>
                  </th>
                  <td className="my-td">
                      <div className="selectbox">
                        <span>{project_return.invest_amount}</span>
                      </div>
                  </td>


                </tr>
                {
                  this.props.typeProject !== '1' ?
                  <tr className="my-tr pc">
                    <th className="my-th">
                        <label className="date optional" >お届け予定 </label>
                    </th>
                    <td className="my-td" >

                      <div className="selectbox">
                        <span>{project_return.schedule}</span>

                      </div>

                    </td>
                  </tr> : null
                }

                <tr className="my-tr pc">
                  <th className="my-th">
                      <label className="integer optional" >件数 </label>
                  </th>
                  <td className="my-td" colSpan="3">
                      <div className="selectbox">
                        <span>{project_return.max_count}</span>
                      </div>
                  </td>

                </tr>
                <tr className="my-tr sp">
                    <th className="my-th">
                        <label className="integer optional" >支援額 <span className="must-icon"></span></label>
                    </th>
                    <td className="my-td" colSpan="3">
                        <div className="selectbox">
                          <span>{project_return.invest_amount}</span>
                        </div>
                    </td>
                  </tr>
                  <tr className="my-tr sp">
                    <th className="my-th">
                        <label className="date optional" >お届け予定 </label>
                    </th>
                    <td className="my-td" colSpan="3">
                      <span>{project_return.schedule}</span>
                    </td>

                  </tr>
                  <tr className="my-tr">
                      <th className="my-th">
                          <label className="string required">リターン品・説明文</label>
                      </th>
                      <td className="my-td" colSpan="3">
                        <span>{project_return.return_amount}</span>

                      </td>
                  </tr>
                  <tr className="my-tr">
                      <th className="my-th">
                          <label className="file optional">画像 </label>
                      </th>
                      <td className="my-td" colSpan="3">
                        <div className="upload-box just-one">
                            <div className="input-file-outer">
                              <div className="dropzone">
                                {/*DropZone*/}
                                <img src={project_return.thumnail} alt=""/>






                                {/*DropZone*/}


                              </div>
                            </div>
                        </div>

                      </td>
                  </tr>
              </tbody>
            </table>

        </div>
        ))}


      </div>
    );
  }
}
