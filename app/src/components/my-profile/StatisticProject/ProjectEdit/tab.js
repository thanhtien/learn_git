/* @flow */

import React, { Component } from 'react';
import { connect } from 'react-redux';

class Tab extends Component {
  changeTab(tab,tabName) {
    const {dispatch} = this.props;
    dispatch({
      type:'ChangeTab',
      payload:tab,
      tabName:tabName
    });
  }
  render() {
    const {tab} = this.props;
    return (
      <ul>
        <li onClick={()=>this.changeTab(1,'リターン作成')}   className={ tab === 1 ? "active" : null }>
          <span style={{cursor: "pointer"}}>
            プロジェクト詳細
          </span>
        </li>
        <li onClick={()=>this.changeTab(2,'プロジェクト詳細')} className={ tab === 2 ? "active" : null }>
          <span style={{cursor: "pointer"}}>
            リターン作成
          </span>
        </li>
      </ul>
    );
  }
}

const mapStateToProps = (state) => {
    return {
      tab:state.common.tab
    }
}

// You have to connect() to any reducers that you wish to connect to yourself
Tab = connect(mapStateToProps)(Tab);


export default Tab;
