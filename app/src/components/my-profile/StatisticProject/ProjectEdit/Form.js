/* @flow */
import React, { Component } from 'react';
import { connect } from 'react-redux';
import {  reduxForm ,FieldArray } from 'redux-form';


import RenderReturn from './RenderReturn';







class TableEdit extends Component {
  constructor(props) {
    super(props)
    this.state = {
      videoOrImage:'video',
    }

  }

  onVideoImage(data) {
    this.setState({
      videoOrImage:data
    })
  }


  //Change Tab Name
  goProjectReturn() {
    const {dispatch,tab} = this.props;
    if (tab === 1) {
      dispatch({
        type:'ChangeTab',
        payload:2,
        tabName:'プロジェクト詳細'
      });

    }
    else {
      dispatch({
        type:'ChangeTab',
        payload:1,
        tabName:'リターン作成'
      });
    }
  }

  componentDidMount() {
    const {initialValues} = this.props;
    if (initialValues.thumbnail_movie_code === "") {
      this.setState({
        videoOrImage:"image"
      })
    }
  }
  htmlReturn(data) {
    return{
      __html:data
    }
  }
  YouTubeGetID(url){
     var ID = '';
     url = url.replace(/(>|<)/gi,'').split(/(vi\/|v=|\/v\/|youtu\.be\/|\/embed\/)/);
     if(url[2] !== undefined) {

       ID = url[2];
     }
     else {
       url = url[0].replace(/(>|<)/gi,'').split(/(vi\/|v=|\/v\/|y2u\.be\/|\/embed\/)/);
       if(url[2] !== undefined) {
         // ID = url[2].split(/[^0-9a-z_\-]/i);
         ID = url[2];
       }

     }
     return ID;
   }
  render() {
    const { initialValues,tab,Categories } = this.props;
    if (!initialValues) {
      return null
    }

    return (
      <div className="main main-write">

          <h3 className="h3">{tab === 1 ? 'プロジェクト詳細' : 'リターン'}</h3>

          <div className={ tab === 1 ? "active cover-table animated fadeIn":"cover-table"}>
            <table id="my-table-edit" className={"my-table show table-80-per"} >
              <tbody>
                <tr className="my-tr">
                    <th className="my-th">
                        <label className="string required">タイトル </label>
                    </th>
                    <td className="my-td">
                      <span>{initialValues.project_name}</span>
                    </td>
                </tr>
                <tr className="my-tr">
                    <th className="my-th">
                      <label className="file optional">
                        画像
                      </label>
                    </th>
                    <td className="my-td" >
                      <div className="upload-box just-one">
                          <div className="input-file-outer">
                            <div className="dropzone">
                              {
                                initialValues ?
                                <div style={{marginBottom:"20px", textAlign: 'center'}}>
                                  <img src={initialValues.thumbnail} alt="avartar"/>
                                </div>
                                : null
                              }

                            </div>
                          </div>
                      </div>
                    </td>
                </tr>

                <tr className="my-tr">
                    <th className="my-th">
                        <label className="integer optional">
                          ご挨拶
                        </label>
                    </th>
                    <td className="my-td" >
                      <span>{initialValues.description}</span>
                    </td>
                </tr>



                {
                  this.state.videoOrImage === "video" && initialValues.thumbnail_movie_code ?
                  <tr className="my-tr">
                      <th className="my-th">
                          <label className="integer optional" >動画</label>
                      </th>
                      <td className="my-td" >
                        <div className="selectbox">
                          <iframe
                            title="iframe"
                            allowFullScreen
                            src={"https://www.youtube.com/embed/"+this.YouTubeGetID(initialValues.thumbnail_movie_code)+"?rel=0"}
                            width="560" height="315">
                          </iframe>
                        </div>
                      </td>
                  </tr>:null
                }

                {

                  this.state.videoOrImage === "image"?
                  <tr className="my-tr">
                    <th className="my-th">
                        <label className="integer optional">概要文</label>
                    </th>
                    <th colSpan='3'>


                        <div className="upload-box">
                            <div className="input-file-outer">
                              <div className="dropzone">
                                {
                                  initialValues ?
                                  <div style={{marginBottom:"20px", textAlign: 'center'}}>
                                    <img src={initialValues.thumbnail_descrip1} alt="avartar"/>
                                  </div>
                                  : null
                                }


                              </div>
                            </div>

                        </div>

                        <div className="upload-box">
                            <div className="input-file-outer">
                              <div className="dropzone">
                                {
                                  initialValues ?
                                  <div style={{marginBottom:"20px", textAlign: 'center'}}>
                                    <img src={initialValues.thumbnail_descrip2} alt="avartar"/>
                                  </div>
                                  : null
                                }


                              </div>
                            </div>

                        </div>


                        <div className="upload-box">
                            <div className="input-file-outer">
                              <div className="dropzone">
                                {
                                  initialValues ?
                                  <div style={{marginBottom:"20px", textAlign: 'center'}}>
                                    <img src={initialValues.thumbnail_descrip3} alt="avartar"/>
                                  </div>
                                  : null
                                }


                              </div>
                            </div>

                        </div>

                    </th>
                  </tr>
                  :null
                }


                <tr className="my-tr pc">
                    <th className="my-th">
                        <label className="integer optional" >カテゴリ </label>
                    </th>
                    <td className="my-td">
                      <span>
                        {
                          Categories.map( (cat, i) => {
                            if (cat.id === initialValues.category_id) {
                              return cat.name
                            }
                            return null;
                          })
                        }
                      </span>
                    </td>

                </tr>
                {
                  initialValues.project_type !== '1' ?
                  <tr className="my-tr pc">
                    <th className="my-th">
                        <label className="date optional" >募集終了日 </label>
                    </th>
                    <td className="my-td" >
                      {
                        <span>
                          {
                            initialValues.collection_end_date
                          }
                        </span>
                      }

                    </td>

                  </tr>

                  : null
                }

                <tr className="my-tr sp">
                  <th className="my-th">
                      <label className="integer optional" >カテゴリ </label>
                  </th>
                  <td className="my-td" >
                    <span>
                      {
                        Categories.map( (cat, i) => {
                          if (cat.id === initialValues.category_id) {
                            return cat.name
                          }
                          return null;
                        })
                      }
                    </span>
                  </td>
                </tr>
                {
                  initialValues.project_type !== '1' ?
                  <tr className="my-tr sp">
                    <th className="my-th">
                        <label className="date optional" >募集終了日 </label>
                    </th>
                    <td className="my-td"  >
                      <span>
                        {
                          initialValues.collection_end_date
                        }
                      </span>
                    </td>
                  </tr> :null
                }
                {
                  initialValues.project_type !== '1' ?
                  <tr className="my-tr">
                      <th className="my-th">
                          <label className="integer optional" >目標金額 </label>
                      </th>
                      <td className="my-td" >
                          <div className="selectbox">
                            <span>{initialValues.goal_amount}</span>
                          </div>

                      </td>
                  </tr>
                  : null
                }

                {
                  initialValues.project_type === '1' ?
                  <tr className="my-tr">
                      <th className="my-th">
                          <label className="integer optional" >定期課金間隔 </label>
                      </th>
                      <td className="my-td" >
                          <div className="selectbox">
                            <span>{initialValues.number_month}</span>
                          </div>

                      </td>
                  </tr>
                  : null
                }



                <tr className="my-tr">
                    <th className="my-th">
                        <label className="integer optional" >本文</label>
                    </th>
                    <td className="my-td" >
                      <div>
                        <div className="div-content-edit"
                          dangerouslySetInnerHTML={this.htmlReturn(initialValues.project_content)} >
                        </div>
                      </div>
                    </td>
                </tr>
            </tbody>
            </table>


          </div>
          {/*TAB 1*/}
          <div className={ tab === 2 ? "active cover-table animated fadeIn":"cover-table"}>
            <FieldArray
              typeProject={initialValues.project_type}
              data={initialValues.project_return}
              name="project_return"
              component={RenderReturn}
            />
          </div>
          {/*TAB 2*/}


      </div>
    );
  }
}




TableEdit = reduxForm({
  form: 'edit-project-user',
})(TableEdit)
const mapStateToProps = (state) => {
    return {
      tabName:state.common.tabName,
      initialValues: state.common.project_default_edit,
      tab:state.common.tab,
      Categories:state.common.Categories,
    }
}

// You have to connect() to any reducers that you wish to connect to yourself
TableEdit = connect(mapStateToProps, null , null , { withRef: true } )(TableEdit);



export default TableEdit
