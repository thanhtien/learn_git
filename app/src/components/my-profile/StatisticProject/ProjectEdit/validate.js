const validate = values => {
  const errors = {};
  if (!values.title_fanclub) {
    errors.title_fanclub = "タイトルを入力してください" ;
  }
  if (!values.message_fanclub) {
    errors.message_fanclub = "解散したい理由を入力してください" ;
  }
  return errors;
};

export default validate;
