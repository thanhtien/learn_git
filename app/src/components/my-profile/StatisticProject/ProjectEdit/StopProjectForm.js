/* @flow */

import React, { Component } from 'react';
import { Field, reduxForm  , reset  } from 'redux-form';
import { connect } from 'react-redux'
import * as actions from '../../../../actions/editSystem';
import renderFieldInput from '../../../common/renderFieldInput';
import renderFieldTextArea from '../../../common/renderFieldTextArea';
import validate from './validate';
import Loading from '../../../common/loading';

class StopProjectForm extends Component {



  handleFormSubmit(data) {
    const {id,StopProjectFanClub} = this.props;
    data.id = id;

    document.body.className = '';
    StopProjectFanClub(data);

  }



  render() {
    const {handleSubmit,loading} = this.props;
    if (loading) {
      return(
        <Loading></Loading>
      )
    }
    return (
      <div className="white-page">

        <div className="cover-no-blank inbox">

          <div className="">

            <div className="main" style={{width:'100%'}}>
              <form onSubmit={handleSubmit(this.handleFormSubmit.bind(this))}>
                <table className="static">
                  <caption className="static-cap">解散したい理由</caption>
                </table>
                <pre className="text-stop">
                  <span style={{fontSize: 'bold',color: 'red'}} >※解散は解散の一か月以上前に支援者に告知する必要があります。</span><br/>
                  以下のフォームに解散する理由を記入してください。「送信する」ボタンをクリックすると、解散の申請がKAKUSEIDA管理者に送信されます。<br/>
                  解散が承認されると、支援者に解散の通知が送付されます。解散通知発行の一ヵ月後に解散が有効となります。
                </pre>




                <table className="show my-table">
                  <tbody>
                    <tr className="my-tr">
                      <th className="my-th">
                          <label className="string required">タイトル <span className="must-icon"></span></label>
                      </th>
                      <td className="my-td">
                        <Field
                          name="title_fanclub"
                          component={renderFieldInput}
                          type="text"
                          placeholder="タイトルを記入してください"
                        />
                      </td>
                    </tr>

                    <tr className="my-tr">
                      <th className="my-th">
                          <label className="string required">解散したい理由 <span className="must-icon"></span></label>
                      </th>
                      <td className="my-td">
                        <Field
                          name="message_fanclub"
                          component={renderFieldTextArea}
                          type="text"
                          placeholder="解散したい理由を記入してください"
                        />
                      </td>
                    </tr>
                  </tbody>
                </table>
                <div className="submit-box">
                  <input type="submit" value="送信する" />
                </div>
              </form>
            </div>




            <div className="clear-fix"></div>
          </div>
        </div>
      </div>
    );
  }
}

const afterSubmit = (result, dispatch) => {
  return (
    dispatch(reset('Stop-Fanclub'))
  )
};
StopProjectForm = reduxForm({
  form: 'Address-Return',
  onSubmitSuccess:afterSubmit,
  validate,
  destroyOnUnmount: false,

})(StopProjectForm)
const mapStateToProps = (state) => {
  return {
    data: state.uploadImage.data,
    loading:state.common.loading,
    statusAPI:state.common.statusAPI

  }
}

// You have to connect() to any reducers that you wish to connect to yourself
StopProjectForm = connect(mapStateToProps, actions )(StopProjectForm);


export default StopProjectForm;
