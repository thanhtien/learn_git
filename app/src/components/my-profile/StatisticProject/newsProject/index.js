/* @flow */

import React, { Component } from 'react';

import Loading from '../../../common/loading';
import LoadingScroll from '../../../common/loadingScroll';
import * as actions from '../../../../actions/editSystem';
import { connect } from 'react-redux';
import ReactPaginate from 'react-paginate';
import { Link } from 'react-router-dom';
import { LazyLoadImage } from 'react-lazy-load-image-component';
import Breadcrumbs from '../../../Breadcrumbs';
import FormatFunc from '../../../common/FormatFunc';
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faChevronLeft, faChevronRight} from "@fortawesome/free-solid-svg-icons";

class ListProject extends Component {


  componentDidMount() {
    const {id,page} = this.props.match.params;
    var PageString = page.replace('page=', '');
    this.props.NewProjectListGet(id,Number(PageString)-1);
    document.title = "最新の活動報告｜KAKUSEIDA";
  }

  handlePageClick = (data) => {
    const {id} = this.props.match.params;
    let selected = data.selected;
    this.props.NewProjectListGet(id,selected);
  };

  renderDes(content) {
    return content.replace(/<\/?[^>]+(>|$)/g, "").substring(0, 200);
  }

  renderStatus(status) {
    if (status === '0') {
      return(
        <span  className="label-news-project" style={{background:'#ddd'}}>下書き</span>
      )
    }
    else {
      return (
        <span className="label-news-project" style={{background:'#0e6eb8'}}>公開中</span>
      )
    }
  }

  render() {
    const {loading,newsList,pageload} = this.props;
    const {id} = this.props.match.params;
    var linkBreacrm = [
      {last:false,label:'最新の活動報告',link:`/my-page/statistic/${id}`},
      {last:true,label:'最新の活動報告',link:null}
    ]


    return (
      <div className="about_box bg">
        <div className="banner-css news-list-banner">
          <div className="wraper">
            <p className="title-white">
              最新の活動報告
            </p>
          </div>
        </div>
        <div>
          <div className="wraper">
            <div className="cover-Breadcrumbs project-detail-bread">
              <Breadcrumbs linkBreacrm={linkBreacrm}></Breadcrumbs>
            </div>
          </div>

          {
            newsList.length !== 0 ?

              <div style={{background:'#fff'}}  className="wraper">


                {
                  <div className="cover-list-new">
                    {
                      newsList.data.map( (item , i) => {



                        return (
                          <div className="each-item" key={i} >
                            <Link className="new-detail" style={{color:'initial'}} to={`/my-page/statistic/${id}/report-edit/${item.id}`}>
                              <div className="thumnail">
                                {
                                  this.renderStatus(item.status)
                                }
                                <LazyLoadImage src={item.thumbnail} effect="blur" alt="thum"></LazyLoadImage>
                              </div>
                              <div className="text-right-only">
                                <p className="time">{<FormatFunc date={item.created}></FormatFunc>}</p>
                                <p className="title">{item.title}</p>
                                <p className="des">
                                  {
                                    this.renderDes(item.content)
                                  }

                                </p>

                              </div>
                            </Link>
                            {
                              /*<Link className="go-more" to={`new-detail/${item.id}`}>もっと見る</Link>
                              <Link className="go-more mr" to={`/my-page/statistic/${id}/report-edit/${item.id}`}>もっと見る</Link>*/
                            }

                          </div>
                        )
                      } )
                    }
                  </div>
                }

                {
                  newsList.data.length > 0 ?
                  <div className="cover-paginate">
                    {
                      loading?<div className="loading-io"><LoadingScroll></LoadingScroll></div>:null
                    }
                    <ReactPaginate
                      previousLabel={<FontAwesomeIcon icon={faChevronLeft}/>}
                      nextLabel={<FontAwesomeIcon icon={faChevronRight}/>}
                      breakLabel={<span>...</span>}
                      breakClassName={"break-me"}
                      pageCount={newsList.page_count}
                      forcePage={Number(pageload)}
                      marginPagesDisplayed={2}
                      pageRangeDisplayed={2}
                      onPageChange={this.handlePageClick}
                      containerClassName={"pagination"}
                      subContainerClassName={"pages pagination"}
                      activeClassName={"active"} />
                  </div> :
                  <div className="mar-center">
                    <p
                      id="notFound"
                      className="blank slug-blank blank-new"
                    >
                      <span>最新の活動報告はまだありません。</span>
                    </p>
                  </div>
                }




                <div className="clear-fix"></div>
              </div>
            : <Loading></Loading>
          }

        </div>
      </div>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    newsList:state.ProjectDetail.newsProjectList.data,
    pageload:state.ProjectDetail.newsProjectList.page,
    loading:state.common.loading
  }
}

export default connect(mapStateToProps, actions)(ListProject);
