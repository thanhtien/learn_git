/**
 * [validate For Validate Form New Project]
 * @param  {[type]} values [values Form]
 * @return {[type]} Error  [Error For Validate match]
 */


const validate = values => {
    const errors = {};

    if (!values.title) {
      errors.title = 'タイトル を入力してください';
    }

    return errors;
};

export default validate;
