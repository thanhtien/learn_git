import { scroller } from 'react-scroll';


function getErrorFieldNames(obj, name = '') {
  const errorArr = [];
  errorArr.push(Object.keys(obj).map((key) => {
    const next = obj[key];
    if (next) {
      if (typeof next === 'string') {
        return name + key;
      }
      // Keep looking
      if (next.map) {
        errorArr.push(next.map((item, index) => getErrorFieldNames(item, `${name}${key}[${index}].`)).filter(o => o));
      }
    }
    return null;
  }).filter(o => o));
  return flatten(errorArr);
}

function flatten(arr) {
  return arr.reduce((flat, toFlatten) => flat.concat(Array.isArray(toFlatten) ? flatten(toFlatten) : toFlatten), []);
}

function isEmpty(obj) {
    for(var prop in obj) {
        if(obj.hasOwnProperty(prop))
            return false;
    }

    return JSON.stringify(obj) === JSON.stringify({});
}
export default function scrollToFirstErrorPreview(errors,dispatch) {

    if (isEmpty(errors) === false) {

      const errorFields = getErrorFieldNames(errors);
      if (errorFields) {
        dispatch({
          type:'ChangeTab',
          payload:1,
          tabName:'リターン作成'
        });
      }
      if (errors.project_return) {
        dispatch({
          type:'ChangeTab',
          payload:2,
          tabName:'プロジェクト詳細'
        });
      }
      setTimeout(function () {
        // Using breakable for loop
        for (let i = 0; i < errorFields.length; i++) {
          const fieldName = `position-${errorFields[i]}`;

          // Checking if the marker exists in DOM
          if (document.querySelectorAll(`[name="${fieldName}"]`).length) {
            scroller.scrollTo(fieldName, { offset: -200, smooth: true });
            break;
          }
        }
        document.getElementsByName(errorFields[0])[0].focus();
        document.getElementsByName(errorFields[0])[0].blur();
      }, 300);
      return true
    }else {
      return false;
    }


}
