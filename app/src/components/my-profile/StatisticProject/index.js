
/* @flow */
import React, { Component } from 'react';
import HeaderProfile from '../header';
import { connect } from 'react-redux'
import {  Link } from 'react-router-dom';
import * as actions from '../../../actions/editSystem';
import TableDonate from './TableDonate';
import TableNotification from './TableNotification';
import ReportPageView from './reportPageView';
import ReportDonate from './reportDonate';
import ReportDonateTotal from './reportTotalDonate';
import Nav from './nav';
import ProjectEdit from './ProjectEdit';
import Breadcrumbs from '../../Breadcrumbs';
import moment from 'moment';
import Loading from '../../common/loading';

class StatisticProject extends Component {

  constructor(props) {
    super(props);
    this.state = {
      loadingChart:false,
      loadingChartDonate:false,
      loadingChartTotalDonate:false,
      tab:1
    }
  }
  componentDidMount() {
    const {id} = this.props.match.params;
    var today = new Date();
    var month = today.getMonth() + 1;
    var date = today.getDate();
    if (month < 10) {
      month = '0' + month;
    }
    if (date < 10) {
      date = '0' + date;
    }
    var last_day = `${today.getFullYear()}-${month}-${date}`;
    var first_day = `${today.getFullYear()}-${month}-01`;

    this.props.notificationListStatic(id,0);
    this.props.getProjectDefaultEdit(id);
    this.props.getprojectStatistic(id,0);
    this.props.getGooglePageView(id,()=>this.setLoadingChartGoogle(),first_day,last_day);
    this.props.getDonateChart(id,()=>this.setLoadingChartDonate(),first_day,last_day);
    this.props.getDonateTotalChart(id,()=>this.setLoadingChartTotalDonate(),last_day);
    document.title = "支援一覧｜KAKUSEIDA";
  }

  setLoadingChartGoogle(){
    this.setState({
      loadingChart:!this.state.loadingChart
    })
  }

  setLoadingChartDonate(){
    this.setState({
      loadingChartDonate:!this.state.loadingChartDonate
    })
  }
  setLoadingChartTotalDonate(){
    this.setState({
      loadingChartTotalDonate:!this.state.loadingChartTotalDonate
    })
  }


  /**
   * [_renderTableDonate Render Table Or Text]
   * @param  {[type]} data
   * @return {[type]}      [Template Table]
   */
  _renderTableDonate(data,type) {
    const {id} = this.props.match.params;

    if (data) {
      if (data.backed.length === 0) {
        return(
          <div>
            <table className="static">
              <caption>支援一覧</caption>
            </table>
            <p>
              支援情報がまだありません。
            </p>

          </div>
        )
      }else {
        return(
          <TableDonate
            typeProject={type}
            id={id}
             getprojectStatisticProps={(id,start)=>this.props.getprojectStatistic(id,start)} statistic_project={data}
             ConfirmReturnEmail={this.props.ConfirmReturnEmail}
          />
        )
      }
    }
  }


  /**
   * [_renderTableDonate Render Table Or Text]
   * @param  {[type]} data
   * @return {[type]}      [Template Table]
   */
  _renderTableNotification(NotificationData) {
    const {id} = this.props.match.params;

    if (NotificationData) {
      if (NotificationData.data.length === 0) {
        return(
          <div>
            <table className="static">
              <caption>支援一覧</caption>
            </table>
            <p>
              支援情報がまだありません。
            </p>

          </div>
        )
      }else {
        return(
          <TableNotification
            id={id}
            notificationList={this.props.notificationListStatic} NotificationData={NotificationData}
          />
        )
      }
    }
  }

  setTab(tab) {
    this.setState({
      tab:tab
    })
  }


  _renderStatusFanClub(item){
    if(item.active === 'yes'){
      if(item.status_fanclub === 'progress'){
        return(
          "公開中"
        );
      }else if(item.status_fanclub === 'wait'){
        return ("解散承認待ち")
      }else if(item.status_fanclub === 'stop'){
        return ("解散済み")
      }
    }

    else if(item.active ==='blk'){
      return (
        "解散済み"
      )
    }else if(item.active ==='cls'){
      return (
        "解散済み"
      )
    }

  }

  _renderStatusNormal(item) {

    if (item) {
      var day = moment(item.collection_end_date).diff(new Date(), 'days');

      if (day < 0) {
        return ("終了")
      }

      if (item.active === 'no' && item.opened === 'no') {
        return("下書き")
      } else if((item.active === 'no' && item.opened === 'yes')) {
        return("申請中")
      }
      else if((item.active === 'cls' && item.opened === 'yes')) {
        return("途中終了")
      } else {
        return ("公開中")
      }
    }
  }

  /**
   * [_renderLinkNews For Render Link News]
   * @return {[type]} [Button]
   */
  _renderLinkNews(type,item) {
    const {id} = this.props.match.params;
    //Check Fan Or Not Fan
    if (type === 'fanclub') {

      if(item.active === 'yes'){
        if(item.status_fanclub === 'progress'){
          return (
            <Link className="link-to-project-detail report-add" to={`/my-page/statistic/${id}/report-new/`}>報告・新規作成</Link>
          )
        }else if(item.status_fanclub === 'wait'){
          return (
            <Link className="link-to-project-detail report-add" to={`/my-page/statistic/${id}/report-new/`}>報告・新規作成</Link>
          )
        }
      }


    }
    else {
      if ((item.active === 'yes' && item.opened === 'yes')) {
        return (
          <Link className="link-to-project-detail report-add" to={`/my-page/statistic/${id}/report-new/`}>報告・新規作成</Link>
        )
      }

    }

  }


  render() {
    const {
      loading,
      statistic_project,
      pageView,
      donateChart,
      getGooglePageView,
      getDonateChart,
      getDonateTotalChart,
      donateChartTotal,
      loadingChartTotalDonate,
      listNotification,
      project_default_edit} = this.props;

    const {loadingChart,loadingChartDonate,tab} = this.state;
    const {id} = this.props.match.params;
    var fanclubOrNormal = null;
    if (project_default_edit && statistic_project) {
      var linkBreacrm = [
        {last:false,label:"自分のプロジェクト",link:'/my-page/post-project/page=1'},
        {last:true,label:project_default_edit.project_name,link:null}
      ]
      if (project_default_edit.project_type === '1') {
        fanclubOrNormal = 'fanclub';
      }else {
        fanclubOrNormal = 'normal';
      }
    }else {
      return(
        <Loading></Loading>
      )
    }





    return (
      <div className="c-area my-page">
        <div className="wraper">

          <HeaderProfile ></HeaderProfile>
          {/* Profile Detail */}

          <div className="clear-fix"></div>
        </div>


        {/* Nav Tab */}
        <div className="wraper">
          <div className="cover-Breadcrumbs"><Breadcrumbs linkBreacrm={linkBreacrm}></Breadcrumbs></div>

          <h1 className="new-project-title">
            <span>
              {project_default_edit ? project_default_edit.project_name : null}
              {' '}
              ({ fanclubOrNormal && fanclubOrNormal === 'fanclub' ? this._renderStatusFanClub(project_default_edit) : this._renderStatusNormal(project_default_edit)})
            </span>
          </h1>
          {/* Breadcrumbs */}
          <div className="nav-here">
            <Nav tab={tab} setTab={ (tab) => this.setTab(tab) } ></Nav>
          </div>
        </div>
        {/* Nav Tab */}


        <div className="white-page">


          <div className={ tab === 1 ? "active tab-div animated fadeIn":"tab-div"}>
            <div className="cover-no-blank">

              <div className="wraper statistic">

                <div className="cover-link-here">
                  <a target='_blank' rel="noopener noreferrer" className="link-to-project-detail" href={'/project-detail/'+this.props.match.params.id}>プロジェクト詳細</a>
                  <Link className="link-to-project-detail report-list" to={`/my-page/statistic/${id}/report-list/page=0`}>報告一覧</Link>

                  {
                    this._renderLinkNews(fanclubOrNormal,project_default_edit)
                  }

                </div>


                {
                  this._renderTableDonate(statistic_project,fanclubOrNormal)
                }
                <div className="clear-fix"></div>

                {
                  this._renderTableNotification(listNotification)
                }

                <div className="clear-fix"></div>
              </div>
              <ReportPageView
                loadingChart={loadingChart}
                pageView={pageView}
                projectID={id}
                getGooglePageView={getGooglePageView}
                setLoadingChartGoogle={()=>this.setLoadingChartGoogle()}/>
              <ReportDonate
                loadingChartDonate={loadingChartDonate}
                donateChart={donateChart}
                projectID={id}
                getDonateChart={getDonateChart}
                setLoadingChartDonate={()=>this.setLoadingChartDonate()}
                />
              <ReportDonateTotal
                loadingChartTotalDonate={loadingChartTotalDonate}
                donateChartTotal={donateChartTotal}
                projectID={id}
                getDonateTotalChart={getDonateTotalChart}
                setLoadingChartTotalDonate={()=>this.setLoadingChartTotalDonate()}
                />
            </div>
          </div>
          {/*  Tab  One Report */}

          <div className={ tab === 2 ? "active tab-div animated fadeIn":"tab-div"}>
            {
              project_default_edit ?
                <ProjectEdit
                  loading={loading}
                  fanclubOrNormal={fanclubOrNormal}
                  project_default_edit={project_default_edit}
                  id={id} />
              : null
            }
          </div>
          {/*  Tab  Two Edit */}
        </div>
      </div>
    );
  }
}
const mapStateToProps = (state) => {
    return {
      profile:state.common.profile,
      loading:state.common.loading,
      statistic_project:state.common.statistic_project,
      listNotification:state.common.listNotification,
      donateChart:state.common.donateChart,
      donateChartTotal:state.common.donateChartTotal,
      pageView:state.common.pageView,
      project_default_edit: state.common.project_default_edit,

    }
}

export default connect(mapStateToProps,actions,null, { withRef: true })(StatisticProject);
