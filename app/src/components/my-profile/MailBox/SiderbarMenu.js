/* @flow */

import React, { Component } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {faPaperPlane , faInbox , faUpload , faAngleRight} from '@fortawesome/free-solid-svg-icons';
import {  NavLink,Link } from 'react-router-dom';
export default class MyComponent extends Component {
  render() {
    const {active} = this.props;

    return (
      <aside className="sidebar">
        <div className="aside-action">
          <div className="compose">
            <Link to={'/my-page/mail-compose/page=0'} className="button-compose">
              <FontAwesomeIcon style={{marginRight: '5px'}} icon={faPaperPlane} />
              メッセージ新規作成
            </Link>
          </div>
        </div>
        {/* Composer */}
        <ul className="menu clearfix">
          <li >
            <NavLink className={active==='emailBox' ? 'selected' : null } to={"/my-page/emailBox/page=1"}>
              <FontAwesomeIcon className="icon-sidebar" icon={faInbox} />
              受信
              <span><FontAwesomeIcon icon={faAngleRight} /></span>
            </NavLink>
          </li>

          <li >
            <NavLink className={active==='emailSended' ? 'selected' : null } to={"/my-page/mail-sended/page=0"}>
              <FontAwesomeIcon className="icon-sidebar" icon={faUpload} />
              送信済
              <span><FontAwesomeIcon icon={faAngleRight} /></span>
            </NavLink>
          </li>
        </ul>
        {/* Menu */}
      </aside>
    );
  }
}
