/* @flow */

import React, { Component } from 'react';


export default class Title extends Component {
  render() {
    return (
      <p className="title-mail">{this.props.title}</p>
    );
  }
}
