/* @flow */

import React, { Component } from 'react';
import ReactPaginate from 'react-paginate';
import LinesEllipsis from 'react-lines-ellipsis';
import { Link  } from 'react-router-dom';

export default class ListProject extends Component {


  handlePageClick = (data) => {
    let selected = data.selected;
    this.props.handlePageClick(selected);
  };

  render() {
    const {data , pageLoad} = this.props;

    return (
      <div>
        {
          data.email.length > 0 ?
          <table className="composer-table">
            <tbody>
              <tr>
                <th>プロジェクト</th>
                <th>メッセージ</th>
              </tr>
              {

                data.email.map( (item,i) => {

                  return(
                    <tr key={i}>
                      <td>
                        <pre>
                          <LinesEllipsis
                            text={item.project_name}
                            maxLine={1}
                            ellipsis={'...'}
                            trimRight
                            basedOn='letters'
                          />
                        </pre>

                      </td>
                      <td>
                        {
                          <Link to={`/my-page/email-box/new/${item.project_id}`} className="link-to-project-detail" >選択</Link>
                        }
                      </td>
                    </tr>
                  )
                })

              }
            </tbody>
          </table> :
          <table className="composer-table">
            <tbody>
              <tr>
                <th>プロジェクト</th>
                <th>メッセージ</th>
              </tr>

              <tr>
                <td colSpan="2">
                  <div className="blank"><p>メッセージを送信できるプロジェクトはありません。</p></div>
                </td>
              </tr>

            </tbody>
          </table>
        }

        <div className="cover-paginate custom-suport">
          {
            data.email.length > 0 ? <ReactPaginate
              previousLabel={"«"}
              nextLabel={"»"}
              breakLabel={<span>...</span>}
              breakClassName={"break-me"}
              forcePage={Number(pageLoad)}
              pageCount={data.totalPages}
              marginPagesDisplayed={2}
              pageRangeDisplayed={2}
              onPageChange={this.handlePageClick}
              containerClassName={"pagination"}
              subContainerClassName={"pages pagination"}
              activeClassName={"active"}
            /> :null
          }

        </div>
      </div>
    );
  }
}
