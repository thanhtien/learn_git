/* @flow */

import React, { Component } from 'react';
import { connect } from 'react-redux'
import * as actions from '../../../../actions/EmailBox';
import Loading from '../../../common/loading';
import SiderbarMenu from '../SiderbarMenu';
import Title from '../Title';
import ComposerInput from './ComposerInput';




class CreateEmail extends Component {

  constructor(props) {
    super(props);
    this.state = {
      loading:false
    };
  }

  componentDidMount() {
    const {user_id} = this.props.match.params;
    this.props.getProfileOwnerUserID(user_id);
    document.title = "メッセージ新規作成｜KAKUSEIDA";

  }

  ToogleLoading() {
    this.setState({
      loading:!this.state.loading
    })
  }

  render() {
    const {ownerProject} = this.props;
    const {project_id,user_id} = this.props.match.params;
    const {loading} = this.state;

    if (loading) {
      return ( <Loading></Loading> )
    }

    return (
      <div className="area cover-send-mail">
        <Title title={'メッセージ新規作成'}></Title>
        {/* Title */}

        <div className="wraper">
          <div className="cover-no-blank">

            <SiderbarMenu />
            {/* SiderBar */}

            <div className="right-content-mail">
              {
                /* Composer */
                ownerProject ?
                <ComposerInput
                  ToogleLoading={()=>this.ToogleLoading()}
                  SendEmailToOwnner={this.props.SendEmailToOwnner}
                  project_id={project_id}
                  user_id={user_id}
                  ownerProject={ownerProject}/> :
                  <div style={{textAlign: 'center'}}><Loading></Loading></div>
              }
            </div>

            {/* Mail List */}

            <div className="clear-fix"></div>
          </div>

        </div>
        {/* Wraper */}

      </div>
    );
  }
}

const mapStateToProps = (state) => {
    return {
      ownerProject:state.MailBox.ownerData
    }
}
export default connect(mapStateToProps, actions )(CreateEmail);
