/* @flow */

import React, { Component } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {faPencilAlt} from '@fortawesome/free-solid-svg-icons';




 class HeaderNewProject extends Component {

  render() {
    const {typeProject} = this.props;
    return (
      <div className="cover-all-here">

        <h1 className="new-project-title">
          <i><FontAwesomeIcon icon={faPencilAlt} /></i>
          <span>{ typeProject === 'normal' ? '普通のプロジェクト' : '定期課金のプロジェクト' }新規作成</span>
        </h1>
        <div className="clear-fix"></div>
      </div>
    );
  }
}


export default HeaderNewProject;
