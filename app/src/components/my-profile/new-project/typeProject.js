/* @flow */

import React, { Component } from 'react';


export default class TypeProject extends Component {


  constructor(props) {
    super(props)
    this.state = {
      typeProject:'normal'
    }
  }
  /**
   * [onVideoImage switch Video Or Image]
   * @param  {[type]} data [data swift]
   */
  onTypeProject(data) {

    this.props.typeProjectChange(data)
    
  }
 componentDidMount() {
   this.is_project =  new URL(window.location.href).searchParams.get("is_project");
   if(this.is_project != null){
     if(this.is_project == 0){
       // typeProject = "normal";
       this.onTypeProject("normal");
     }else{
       //  typeProject = "fanclub";
       this.onTypeProject("fanclub");
     }

   }
 }

  render() {
    let {typeProject} = this.props;


    return (
      <div className="cover-no-blank">
        <div className="main main-write" style={{padding:0}}>
          <table className={"my-table show"} style={{margin:"15px 0"}}>
            <tbody>
              <tr className="my-tr">
                <th className="my-th">
                  <label className="string required">プロジェクト種類</label>
                </th>
                <td className="my-td" colSpan="3">
                  <div className="form-check ">
                    <input
                      value="normal_project"
                      onChange={()=>this.onTypeProject("normal")}
                      className="form-check-input"
                      id="Radios3"
                      type="radio" name="Radios" checked={typeProject === "normal" ? true : false}/>
                    <label style={{"cursor":"pointer"}} htmlFor="Radios3" className="form-check-label">
                      普通のプロジェクト
                    </label>
                  </div>

                  <div className="form-check ">
                    <input
                      value="fanclub_project"
                      onChange={()=>this.onTypeProject("fanclub")} className="form-check-input"
                      type="radio" name="Radios"
                      id="Radios4"
                      checked={typeProject === "fanclub" ? true : false}
                    />
                    <label style={{"cursor":"pointer"}} htmlFor="Radios4" className="form-check-label">
                      定期課金のプロジェクト
                    </label>
                  </div>
                </td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>


    );
  }
}
