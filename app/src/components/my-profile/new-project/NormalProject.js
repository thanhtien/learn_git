/* @flow */
import React, { Component } from 'react';
import { connect } from 'react-redux';
import EditorField from '../../common/EditorField';
import NumberFormat from '../componentCommon/NumberFormat';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {faExclamationCircle , faCheck } from '@fortawesome/free-solid-svg-icons';
import * as actions from '../../../actions/editSystem';
import { Field, reduxForm , FieldArray , reset , getFormValues } from 'redux-form'
import renderFieldInput from '../../common/renderFieldInput';
import renderFieldTextArea from '../../common/renderFieldTextArea';
import RenderDropzone from '../../common/renderDropzone';
import renderDropdownList from '../../common/renderDropdownList';
import renderDateTimePicker from '../../common/renderDateTimePicker';
import RenderReturn from '../componentCommon/RenderReturn';
import validate from '../componentCommon/validate';
import scrollToFirstError from '../componentCommon/scrollToFirstError';
import scrollToFirstErrorPreview from '../componentCommon/scrollToFirstPreview';
import History from '../../../history.js';
import Rodal from 'rodal';
import Loading from '../../common/loading';
class NormalProject extends Component {
  constructor(props) {
    super(props)
    this.state = {
      stateDate:false,
      preview_status:false,
      videoOrImage:'video',
      dataPreview:null,
      previewYesOrNo:false,
      status_all_in:'1'

    }
  }

  /**
   * [handleFormSubmit Submit Form Create Project]
   * @param  {[type]} data [Data TO Server]
   */

  handleFormSubmit(data){
    data.goal_amount = data.goal_amount.replace(/,/g, "");

    if (this.state.videoOrImage === 'video') {
      data.thumbnail_type = 1;
    }else {
      data.thumbnail_type = 0;
    }

    if (this.state.status_all_in) {
      data.status_all_in = this.state.status_all_in;
    }
    if (data.project_return) {
      for (var i = 0; i < data.project_return.length; i++) {

        if (data.project_return[i].invest_amount) {
          data.project_return[i].invest_amount = data.project_return[i].invest_amount.replace(/,/g, "");
        }
        if (data.project_return[i].max_count) {
          data.project_return[i].max_count = data.project_return[i].max_count.replace(/,/g, "");
        }
      }
    }

    this.props.addNewProject(data);
  }



  /**
   * [onVideoImage switch Video Or Image]
   * @param  {[type]} data [data swift]
   */

  onVideoImage(data) {
    this.setState({
      videoOrImage:data
    })
  }

  /**
   * [onVideoImage switch Video Or Image]
   * @param  {[type]} data [data swift]
   */

  onDonateType(data) {
    this.setState({
      status_all_in:data
    })
  }

  hideModalAndConfirm() {
    const {projectInfor,profile} = this.props;

    if (profile) {

      if (profile.bank_branch === "" || profile.bank_name === "" || profile.bank_owner === "" || profile.bank_number === "" || profile.bank_type === "" ) {
        var CheckProfile = window.confirm('申請する前に、銀行口座情報をきちんと記入してください');
        if (CheckProfile === true) {
          History.push('/my-page/setting-user/bank-accounts-project-success');
        }
      } else {
        this.props.confirmProject(projectInfor.id);
      }
    }
  }
  hideModal() {
    const {dispatch} = this.props;
    dispatch({
      type:"CONFIRM_OK_CANCEL"
    });
    window.location.href = "/my-page/post-project/page=1";
  }
  //Change Tab Name
  goProjectReturn() {
    const {dispatch,tab} = this.props;
    if (tab === 1) {
      dispatch({
        type:'ChangeTab',
        payload:2,
        tabName:'プロジェクト詳細'
      });

    }
    else {
      dispatch({
        type:'ChangeTab',
        payload:1,
        tabName:'リターン作成'
      });
    }
  }

  //Change Tab Name In Tab.js
  tabNameChange(tab) {
    if (tab === 2) {
      this.setState({
        tabName:'プロジェクト詳細'
      })
    }else {
      this.setState({
        tabName:'リターン作成'
      })
    }
  }

  clearForm() {
    this.props.reset();
  }

  /**
   * [preview Generate HTML AND DATA FOR PREVIEW PROJECT]
   * @return {[type]} [LAYOUT PROJECT]
   */
  previewYesOrNo(){
    this.setState({
      previewYesOrNo:true
    })
  }
  preview(){
    //check Validate Form same as submit and Scrollto error feild
    var check = scrollToFirstErrorPreview(validate(this.props.dataPreview),this.props.dispatch, ()=>this.previewYesOrNo());

    if (check !== true) {

      //initial value preview
      var data = this.props.dataPreview;

      data.goal_amount = data.goal_amount.replace(/,/g, "");
      //Thumnail Type
      if (this.state.videoOrImage === 'video') {
        data.thumbnail_type = 1;
      }else {
        data.thumbnail_type = 0;
      }

      //Data Project Return Replace
      if (data.project_return) {
        for (var i = 0; i < data.project_return.length; i++) {

          if (data.project_return[i].invest_amount) {
            data.project_return[i].invest_amount = data.project_return[i].invest_amount.replace(/,/g, "");
          }
          if (data.project_return[i].max_count) {
            data.project_return[i].max_count = data.project_return[i].max_count.replace(/,/g, "");
          }

        }
      }


      //IF Data Thumnail = 1 but it just defalt

      if (data.thumbnail_type === 1) {
        if (!data.thumbnail_movie_code) {
          //generate default slider
          data.thumbnail_img = [
            {
              thumbnail_descrip: "https://static.kakuseida.com/images/2018/default/noimage-01.png",
              type: "image"
            },
            {
              thumbnail_descrip: "https://static.kakuseida.com/images/2018/default/noimage-01.png",
              type: "image"
            },
            {
              thumbnail_descrip: "https://static.kakuseida.com/images/2018/default/noimage-01.png",
              type: "image"
            }
          ]
        }else {

          // //parse youtube code
          // var video_id = data.thumbnail_movie_code.split('v=')[1];
          // var ampersandPosition = video_id.indexOf('&');
          // if(ampersandPosition !== -1) {
          //   video_id = video_id.substring(0, ampersandPosition);
          // }
          // // data.thumbnail_movie_code = video_id;
        }
      }else {
        //if thumnail type = 0
        if (!data.thumbnail_descrip2 && !data.thumbnail_descrip3 && !data.thumbnail_descrip1) {
          //But data is empty
          data.thumbnail_img = [
            {
              thumbnail_descrip: "https://static.kakuseida.com/images/2018/default/noimage-01.png",
              type: "image"
            },
            {
              thumbnail_descrip: "https://static.kakuseida.com/images/2018/default/noimage-01.png",
              type: "image"
            },
            {
              thumbnail_descrip: "https://static.kakuseida.com/images/2018/default/noimage-01.png",
              type: "image"
            }
          ]
        }else {
          //Data not empty
          data.thumbnail_img = [];
          //push 1
          if (data.thumbnail_descrip1) {
            var thumbnail_descrip1 = {
              thumbnail_descrip:data.thumbnail_descrip1,
              type:"image"
            }
            data.thumbnail_img.push(thumbnail_descrip1);
          }
          //push 2
          if (data.thumbnail_descrip2) {
            var thumbnail_descrip2 = {
              thumbnail_descrip:data.thumbnail_descrip2,
              type:"image"
            }
            data.thumbnail_img.push(thumbnail_descrip2);

          }
          //push 3
          if (data.thumbnail_descrip3) {
            var thumbnail_descrip3 = {
              thumbnail_descrip:data.thumbnail_descrip3,
              type:"image"
            }
            data.thumbnail_img.push(thumbnail_descrip3);
          }
        }
      }

      //Set Default Project Return
      if (!data.project_return) {
        data.project_return = [];
      }

      //SetDefault thumbnail
      if (!data.thumbnail) {
        data.thumbnail = "https://static.kakuseida.com/images/2018/default/noimage-01.png"
      }


      var today = new Date();
      var endDate = data.collection_end_date;
      var diffMs = (endDate - today);
      var diffDays = Math.floor(diffMs / 86400000);



      if (diffDays === -1 ) {
        data.format_collection_end_date = {
          hour:23,
          minutes:59
        }

      }else if(diffDays === 0) {
        data.format_collection_end_date = {
          date:1
        }
      }else {
        data.format_collection_end_date = {
          date:diffDays + 1
        }
      }

      this.props.previewOpen(data,this.props.profile);
    }

  }

  render() {
    const { handleSubmit  , Categories   , add_new_status  , tab , loading  } = this.props;
    const {previewYesOrNo} = this.state;


    if (loading) {
      return(
        <Loading></Loading>
      )
    }

    return (
      <div>
        <Rodal width={300} height={240} visible={add_new_status} onClose={this.hideModal.bind(this)}>
          <div className="icon-sucess">
            <FontAwesomeIcon icon={faCheck} />
          </div>
          <p className="text-sucess">プロジェクトを申請しますか。</p>
          <button onClick={() => this.hideModalAndConfirm()} className="confirm-button">はい</button>
          <button style={{"background":"#ddd"}} onClick={() => this.hideModal()} className="confirm-button cancel">いいえ</button>

        </Rodal>

        <form onSubmit={handleSubmit(this.handleFormSubmit.bind(this))}>


          <h3 className="h3">{tab === 1 ? 'プロジェクト詳細' : 'リターン作成'}</h3>


          <div className={ tab === 1 ? "active cover-table animated fadeIn":"cover-table"}>
            <table className={"my-table show"} >
              <tbody>
                <tr className="my-tr">
                    <th className="my-th">
                        <label className="string required">タイトル <span className="must-icon"></span></label>
                    </th>
                    <td className="my-td" colSpan="3">

                      <Field
                        name="project_name"
                        component={renderFieldInput}
                        type="text"
                        placeholder="プロジェクト名（最大150文字）"
                        nameError="project_name"
                        maxlength={150}
                      />
                    <span className="warning">
                      ※最大150文字
                    </span>

                    </td>
                </tr>
                <tr className="my-tr">
                    <th className="my-th">
                      <label className="file optional">
                        画像
                      </label>
                    </th>
                    <td className="my-td" colSpan="3">
                      <div className="upload-box just-one">
                          <div className="input-file-outer">
                            <div className="dropzone">
                              {/*DropZone*/}
                                <RenderDropzone
                                  path="thumbnail"
                                  formName="new_project"
                                  feild="thumbnail">
                                </RenderDropzone>
                                <p className="note">アップロードできる画像形式は <span style={{color: 'red'}}>*.jpeg と *.png</span>のみです</p>

                              {/*DropZone*/}
                              <Field style={{width:"100%"}} name="thumbnail" component="input" type="hidden" />

                            </div>
                          </div>
                      </div>
                      <span className="warning">
                        ※画像のサイズは600px＊400pxで最高です。容量 2MBまで 。
                      </span>
                      <span className="warning">※サムネイル表示</span>
                    </td>
                </tr>

                <tr className="my-tr">
                    <th className="my-th">
                        <label className="integer optional">
                          ご挨拶
                        </label>
                    </th>
                    <td className="my-td" colSpan="3">
                      <Field
                        name="description"
                        component={renderFieldTextArea}
                        type="text"
                        maxlength={1000}
                        placeholder="ご挨拶 （最大1000文字）"
                        />
                      <span className="warning">※最大1000文字</span>

                    </td>
                </tr>


                <tr className="my-tr">

                  <td className="my-td" colSpan="4">
                    <div className="form-check ">
                      <input
                        value="video"
                        onChange={()=>this.onVideoImage("video")}
                        className="form-check-input"
                        id="Radios1"
                        type="radio" name="Radios" checked={this.state.videoOrImage === "video" ? true : false}/>
                      <label style={{"cursor":"pointer"}} htmlFor="Radios1" className="form-check-label">
                        動画のURL入力
                      </label>
                    </div>

                    <div className="form-check ">
                      <input
                        value="image"
                        onChange={()=>this.onVideoImage("image")} className="form-check-input"
                        type="radio" name="Radios"
                        id="Radios2"
                        checked={this.state.videoOrImage === "image" ? true : false}
                      />
                    <label style={{"cursor":"pointer"}} htmlFor="Radios2" className="form-check-label">
                        画像アップロード
                      </label>
                    </div>
                  </td>
                </tr>


                  <tr className={this.state.videoOrImage === "video" ? "my-tr" : "hidden-tr my-tr" } >
                      <th className="my-th">
                          <label className="integer optional" >動画</label>
                      </th>
                      <td className="my-td" colSpan="3">
                        <div className="selectbox">
                          <Field
                            name="thumbnail_movie_code"
                            component={renderFieldInput}
                            type="text"
                            placeholder="https://www.youtube.com/watch?v=xxxxxxxxxx"
                            />
                        </div>
                        <span className="warning">※ YouTube のみ対応しています。</span>
                      </td>
                  </tr>




                  <tr className={ this.state.videoOrImage === "image" ? "my-tr" : 'hidden-tr my-tr'}>
                    <th className="my-th">
                        <label className="integer optional">概要文</label>
                    </th>
                    <td className="my-td" colSpan='3'>


                        <div className="upload-box">
                            <div className="input-file-outer">
                              <div className="dropzone">
                                {/*DropZone*/}
                                  <RenderDropzone
                                    path="thumbnailDes"
                                    formName="new_project"
                                    feild="thumbnail_descrip1">
                                  </RenderDropzone>
                                  <p className="note">アップロードできる画像形式は <span style={{color: 'red'}}>*.jpeg と *.png</span>のみです</p>
                                {/*DropZone*/}
                                <Field style={{width:"100%"}} name="thumbnail_descrip1" component="input" type="hidden" />

                              </div>
                            </div>

                        </div>

                        <div className="upload-box">
                            <div className="input-file-outer">
                              <div className="dropzone">
                                {/*DropZone*/}
                                  <RenderDropzone path="thumbnailDes" formName="new_project" feild="thumbnail_descrip2"></RenderDropzone>
                                  <p className="note">アップロードできる画像形式は <span style={{color: "red"}}>*.jpeg と *.png</span>のみです</p>
                                {/*DropZone*/}
                                <Field style={{width:"100%"}} name="thumbnail_descrip2" component="input" type="hidden" />

                              </div>
                            </div>

                        </div>


                        <div className="upload-box">
                            <div className="input-file-outer">
                              <div className="dropzone">
                                {/*DropZone*/}
                                  <RenderDropzone
                                    path="thumbnailDes"
                                    formName="new_project"
                                    feild="thumbnail_descrip3">
                                  </RenderDropzone>
                                  <p className="note">アップロードできる画像形式は <span style={{color: "red"}}>*.jpeg と *.png</span>のみです</p>
                                {/*DropZone*/}
                                <Field style={{width:"100%"}} name="thumbnail_descrip3" component="input" type="hidden" />

                              </div>
                            </div>

                        </div>


                        <span className="warning w100">
                        ※画像のサイズは600px＊400pxで最高です。容量 2MBまで 。
                        </span>


                        <span className="warning w100">
                          {"※2～３枚の画像をアップロードしたら、スライダーで表示されるようになります。"}
                        </span>

                    </td>
                  </tr>



                <tr className="my-tr pc">
                    <th className="my-th">
                        <label className="integer optional" >カテゴリ <span className="must-icon"></span></label>
                    </th>
                    <td className="my-td">
                        <div className="selectbox">
                          <Field
                            name="category_id"
                            component={renderDropdownList}
                            data={Categories}
                            valueField="id"
                            textField="name"
                            placeholder="カテゴリ"
                          />

                        </div>
                        <span className="warning">※カテゴリの決定に関しましては、弊社の基準が適用されることがあります。</span>
                    </td>
                    <th className="my-th">
                        <label className="date optional" >募集終了日 <span className="must-icon"></span></label>
                    </th>
                    <td className="my-td" >

                      <Field
                        name="collection_end_date"
                        previewYesOrNo={previewYesOrNo}
                        showTime={false}
                        component={renderDateTimePicker}
                        stateDate={this.state.stateDate}
                        onToggle={()=>this._OpenDate()}
                        placeholder="募集終了日"
                        nameError="collection_end_date"

                      />
                    <span className="warning">※公開可能期間は2日〜1年間までとなります。</span>
                      <span className="sub">(例) 2018-01-01 を選択した場合、2018-01-01 23:59 に終了します。</span>
                      <div className="error-message"></div>
                    </td>
                </tr>
                <tr className="my-tr sp">
                  <th className="my-th">
                      <label className="integer optional" >カテゴリ <span className="must-icon"></span></label>
                  </th>
                  <td className="my-td" colSpan="3">
                      <div className="selectbox">
                        <Field
                          name="category_id"
                          component={renderDropdownList}
                          data={Categories}
                          valueField="id"
                          textField="name"
                          placeholder="カテゴリ"
                        />
                      </div>
                      <span className="warning">※カテゴリの決定に関しましては、弊社の基準が適用されることがあります。</span>
                  </td>
                </tr>
                <tr className="my-tr sp">
                  <th className="my-th">
                      <label className="date optional" >募集終了日 <span className="must-icon"></span></label>
                  </th>
                  <td className="my-td" colSpan="3" >
                    <div>
                      <Field
                        name="collection_end_date"
                        previewYesOrNo={previewYesOrNo}
                        showTime={false}
                        component={renderDateTimePicker}
                        stateDate={this.state.stateDate}
                        onToggle={()=>this._OpenDate()}
                      />
                    </div>
                  <span className="sub warning"><i><FontAwesomeIcon icon={faExclamationCircle} /></i> 公開可能期間は2日〜80日となります。</span>
                    <span className="warning">(例) 2018-01-01 を選択した場合、2018-01-01 23:59 に終了します。</span>
                    <div className="error-message"></div>
                  </td>
                </tr>
                <tr className="my-tr">
                    <th className="my-th">
                        <label className="integer optional" >目標金額 <span className="must-icon"></span></label>
                    </th>
                    <td className="my-td" colSpan="3">
                        <div className="selectbox">
                          <Field
                            name="goal_amount"
                            component={renderFieldInput}
                            type="text"
                            placeholder="例）100000"
                            normalize={NumberFormat}
                          />
                        </div>
                        <span className="sub warning">【目標金額の鉄則！】</span>
                        <span className="intro">また、<span>プロジェクトの実施に必要な最低金額</span>での設定がおすすめです。 <br/>募集期間中は目標金額を達成後も資金を集め続けることが可能ですので、まずは最小限からはじめてみましょう。</span>
                    </td>
                </tr>


                <tr className="my-tr">
                  <th className="my-th">
                      <label className="integer optional" >募集方式 <span className="must-icon"></span></label>
                  </th>

                  <td className="my-td" colSpan="4">
                    <div className="form-check ">
                      <input
                        value="1"
                        onChange={()=>this.onDonateType("1")}
                        className="form-check-input"
                        id="Radios6"
                        type="radio"
                        name="Radios6" checked={this.state.status_all_in === "1" ? true : false}/>
                      <label style={{"cursor":"pointer"}} htmlFor="Radios6" className="form-check-label check-fix-font-size">
                        All-in
                      </label>
                    </div>

                    <div className="form-check ">
                      <input
                        value="0"
                        onChange={()=>this.onDonateType("0")} className="form-check-input"
                        type="radio"
                        name="Radios6"
                        id="Radios5"
                        checked={this.state.status_all_in === "0" ? true : false}
                      />
                    <label style={{"cursor":"pointer"}} htmlFor="Radios5" className="form-check-label check-fix-font-size">
                        All-or-nothing
                      </label>
                    </div>
                    <div className="clear-fix"></div>
                    <span className="sub warning">【2つの方式の違いとは？】</span>
                    <span className="intro">
                      <b style={{fontWeight: 'bold'}}>・All-or-Nothing 方式：</b> <br/> 目標金額を達成すると、資金を受け取れる方式 <br/>
                    </span>
                    <span className="intro">
                      <b style={{fontWeight: 'bold'}}>・All-in 方式：</b> <br/> 目標金額を達成しなくても、資金を受け取れる方方式 <br/> <span className="intro">プロジェクト実施が確約できる方のみ</span> ご利用いただけます。
                    </span>
                  </td>
                </tr>



                <tr className="my-tr">
                    <th className="my-th">
                        <label className="integer optional" >本文</label>
                    </th>
                    <td className="my-td" colSpan="3">
                      <div>
                        <span style={{marginBottom:6}} className="sub warning">※画像の容量 2MBまで 。</span>
                        <EditorField
                          key="field"
                          name="project_content"
                          id="inputEditorText"
                          disabled={false}
                        />
                      </div>
                    </td>
                </tr>
            </tbody>
            </table>


          </div>
          {/*TAB 1*/}
          <div className={ tab === 2 ? "active cover-table animated fadeIn":"cover-table"}>
            <FieldArray name="project_return" formName="new_project" component={RenderReturn} />
          </div>
          {/*TAB 2*/}
          <div className="submit-box submit-box-create-project">
            <input type="submit" name="commit" value="保存" data-disable-with="保存する"/>
            <span onClick={() => this.goProjectReturn()} className="btn-preview "> {this.props.tabName} </span>
            <span className='btn-preview' onClick={()=> this.preview()} >プレビュー</span>
          </div>
        </form>

      </div>

    );
  }
}

const afterSubmit = (result, dispatch) => {
  return (
    dispatch(reset('new_project'))
  )
};

NormalProject = reduxForm({
  form: 'new_project',
  destroyOnUnmount: false,
  onSubmitFail: (errors , dispatch) => scrollToFirstError(errors , dispatch),
  onSubmitSuccess:afterSubmit,
  validate

})(NormalProject);

const mapStateToProps = (state) => {
  const dataPreview = getFormValues('new_project')(state);
  return {
    dataPreview:dataPreview,
    data: state.uploadImage.data,
    percent: state.uploadImage.progess,
    disable: state.uploadImage.disable,
    loading:state.common.loading,
    tabName:state.common.tabName,
    Categories:state.common.Categories,
    error:state.form._error,
    add_new_status:state.common.add_new_status,
    projectInfor:state.common.projectInfor,
    profile: state.common.profile ,
    tab:state.common.tab
  }
}

// You have to connect() to any reducers that you wish to connect to yourself
NormalProject = connect(mapStateToProps, actions , null , { withRef: true })(NormalProject);

export default NormalProject;
