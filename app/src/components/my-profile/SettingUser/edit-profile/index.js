/* @flow */
import React, { Component } from 'react';
import NavProfile from '../../nav';


import HeaderProfile from '../../header';
import TableEdit from './TableEdit';
import { connect } from 'react-redux'
import Loading from '../../../common/loading';

class EditUser extends Component {
  componentDidMount() {

    document.title = "プロフィール編集｜KAKUSEIDA";


  }

  render() {
    const {profile} = this.props;
    const {subcriber} = this.props.match.params;

    return (
      <div className="c-area my-page">
        <div className="wraper">
          <HeaderProfile subcriber={subcriber} ></HeaderProfile>
          {/* Profile Detail */}
          <div className="nav-here">
            {
              profile ?
              <NavProfile
                patron={profile.patron} patronfanclub={profile.patronfanclub}
                numberProject={profile.numberProject}/>
              : <Loading></Loading>
            }
          </div>
          {/* NAV Detail */}
          <div className="clear-fix"></div>
        </div>
        <div className="white-page">
          <div className="cover-no-blank">

            <div className="wraper">
              <TableEdit></TableEdit>
              <div className="clear-fix"></div>
            </div>
          </div>
        </div>


      </div>
    );
  }
}
const mapStateToProps = (state) => {
    return {
      profile:state.common.profile
    }
}
export default connect(mapStateToProps)(EditUser);
