/* @flow */

import React, { Component } from 'react';
import { connect } from 'react-redux'
import { Field, reduxForm  } from 'redux-form'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {faExclamationCircle} from '@fortawesome/free-solid-svg-icons';
import stateJapan from './stateJapan';
import gender from './stateGender';
import * as actions from '../../../../actions/editSystem';

import AsideLeft from '../aside';
import Loading from '../../../common/loading';

import RenderDropzone from '../../../common/renderDropzone';
import renderFieldInput from '../../../common/renderFieldInput';
import renderFieldTextArea from '../../../common/renderFieldTextArea';
import renderDropdownList from '../../../common/renderDropdownList';
import renderDateTimePicker from '../../../common/renderDateTimePickerBirth';
import FlashMassage from 'react-flash-message';

class TableEdit extends Component {
  //constructor
  constructor(props) {
    super(props)
    this.state = {
      stateDate:false,
      toogle:false
    }
  }
  //Submit
  handleFormSubmit(data){
    if (data.birthday && typeof data.birthday.getMonth === 'function') {
      var month = Number(data.birthday.getMonth()) + 1 ;
      data.birthday = data.birthday.getFullYear()+"/"+month.toString()+"/"+data.birthday.getDate();
    }


    this.props.editUser(data);
  }

  //Loading Data Redux
  componentDidMount() {
    if (this.props.initialValues) {
      this.props.InitialData(this.props.initialValues)
    }
  }

  _renderStatusAPI() {
    setTimeout( () => {
      this.props.Dis_Status_Api(false);
    }, 2000);
    return(
      <div id="push-notification">
        <FlashMassage duration={2000}>
          <div className="alert">
            成功！変更が保存されました。
          </div>
        </FlashMassage>
      </div>
    )
  }

  render() {
    const { handleSubmit,initialValues , statusAPI } = this.props;

    if (this.props.loading) {
      return(
        <Loading></Loading>
      )
    }

    return (
      <div>
        <AsideLeft/>

        <div className="main">
          {
            statusAPI ? this._renderStatusAPI(): null
          }
          <h3>プロフィール編集</h3>
          <span className="check-it-out">
            <FontAwesomeIcon icon={faExclamationCircle} style={{marginRight: "6px"}}/>
              プロフィール情報は、プロジェクトの支援や企画をする際に必要となります。<br/>
              「性別」と「生年月日」は基本的に公開されません。プロジェクトを支援した場合にのみ、全体の集計データとしてプロジェクト企画者に提供されます。また、一度登録されるとご自分で変更ができません。プロフィール変更の際には、お問い合わせより管理者までご連絡ください。
          </span>


          <form  onSubmit={handleSubmit(this.handleFormSubmit.bind(this))}>

            <table className="show my-table">
              <tbody>
                  <tr className="my-tr">
                      <th className="my-th">
                          <label className="string required">ユーザー名</label>
                      </th>
                      <td className="my-td">



                        <span>
                          {
                            initialValues ? initialValues.username : null
                          }
                        </span>



                      </td>
                  </tr>
                  <tr className="my-tr">
                      <th className="my-th">
                          <label className="string required">職業</label>
                      </th>
                      <td className="my-td">
                        <Field
                          name="job"
                          component={renderFieldTextArea}
                          type="textarea"
                          placeholder="自分の職業を入力してください"
                          maxlength={80}
                        />
                      </td>
                  </tr>
                  <tr className="my-tr">
                      <th className="my-th">
                          <label className="file optional">アイコン</label>
                      </th>
                      <td className="my-td">
                          <div className="upload-box just-one">
                              <div className="input-file-outer">
                                <div className="dropzone">



                                  {/*DropZone*/}
                                  {

                                    initialValues ? <RenderDropzone
                                      imagePreview={initialValues.profileImageURL}
                                      path="profile"
                                      formName="edit-setting-user"
                                      feild="profileImageURL">
                                    </RenderDropzone> :null
                                  }

                                    <p className="note">アップロードできる画像形式は <span style={{color:'red'}}>*.jpeg と *.png</span>のみです</p>
                                  {/*DropZone*/}
                                  <Field style={{width:"100%"}} name="profileImageURL" component="input" type="hidden" />
                                </div>
                              </div>
                          </div>
                      </td>
                  </tr>
                  <tr className="my-tr">
                      <th className="my-th">
                          <label className="integer optional" >現在地</label>
                      </th>
                      <td className="my-td">
                          <div className="selectbox">
                            <Field
                              name="address"
                              component={renderDropdownList}
                              data={stateJapan}
                              valueField="id"
                              textField="state"/>
                          </div>
                      </td>
                  </tr>
                  <tr className="my-tr">
                      <th className="my-th">
                          <label className="integer optional" >出身地</label>
                      </th>
                      <td className="my-td">
                          <div className="selectbox">

                            <Field
                              name="country"
                              component={renderDropdownList}
                              data={stateJapan}
                              valueField="id"
                              textField="state"/>
                          </div>
                      </td>
                  </tr>
                  <tr className="my-tr">
                      <th className="my-th">
                          <label className="integer optional" >性別</label>
                      </th>
                      <td className="my-td">
                          <div className="selectbox">
                            <Field
                              name="sex"
                              component={renderDropdownList}
                              data={gender}
                              valueField="id"
                              textField="display"/>
                          </div>
                      </td>
                  </tr>
                  <tr className="my-tr">
                      <th className="my-th">
                          <label className="date optional" >生年月日</label>
                      </th>
                      <td className="my-td">
                        <Field
                          name="birthday"
                          showTime={false}
                          component={renderDateTimePicker}
                          onToggle={()=>this._OpenDate()}
                        />
                        <div className="error-message"></div>
                      </td>
                  </tr>

                  <tr className="my-tr">
                      <th className="my-th">
                          <label className="text optional" >ウェブサイト</label>
                      </th>
                      <td className="my-td">
                        <Field
                          name="url1"
                          component={renderFieldInput}
                          type="text"
                          placeholder="ウェブサイト"
                        />
                      </td>
                  </tr>
                  <tr className="my-tr">
                      <th className="my-th">
                          <label className="text optional" >ウェブサイト</label>
                      </th>
                      <td className="my-td">
                        <Field
                          name="url2"
                          component={renderFieldInput}
                          type="text"
                          placeholder="ウェブサイト"
                        />
                      </td>
                  </tr>
                  <tr className="my-tr">
                      <th className="my-th">
                          <label className="text optional" >ウェブサイト</label>
                      </th>
                      <td className="my-td">
                        <Field
                          name="url3"
                          component={renderFieldInput}
                          type="text"
                          placeholder="ウェブサイト"
                        />
                      </td>
                  </tr>

              </tbody>
            </table>

            <div className="submit-box">
              <input type="submit" name="commit" value="変更" data-disable-with="変更"/>
            </div>
          </form>
        </div>
      </div>
    );
  }
}

const validate = values => {
    const errors = {};

    if (!values.job) {
    }else if(values.job.length > 80 || values.job.length < 10) {
      errors.job = '最低10文字 、最大80文字';
    }




    if (!values.username) {
        errors.username = 'ユーザー名を入力してください';
    }else if(values.username){
      if (values.username.length<=2 || values.username.length>= 32) {
        errors.username = 'ユーザー名（英数字3-32文字）';
      }
    }
    return errors;
};

TableEdit = reduxForm({
  form: 'edit-setting-user',
  destroyOnUnmount: false,
  validate
})(TableEdit)
const mapStateToProps = (state) => {
    return {
      data: state.uploadImage.data,
      percent: state.uploadImage.progess,
      disable: state.uploadImage.disable,
      initialValues: state.common.profile,
      statusAPI:state.common.statusAPI,
      loading:state.common.loading
    }
}

TableEdit = connect(mapStateToProps, actions )(TableEdit);





export default TableEdit
