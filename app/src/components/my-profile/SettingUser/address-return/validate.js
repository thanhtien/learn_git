const validate = values => {
    const errors = {};

    if (!values.name_return) {
      errors.name_return = "名前を入力してください" ;
    }
    if (!values.postcode) {
      errors.postcode = "郵便番号を入力してください" ;
    }
    if (!values.address_return) {
      errors.address_return = "住所を入力してください" ;
    }
    if (!values.phone) {
      errors.phone = "電話番号を入力してください" ;
    }
    if (!values.email) {
      errors.email = "メールアドレスを入力してください" ;
    }else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
          errors.email = 'メールアドレスを正しくしてください';
    }

    return errors;
};

export default validate;
