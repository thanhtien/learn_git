/* @flow */

import React, { Component } from 'react';
import postal_code  from 'japan-postal-code';
import { connect } from 'react-redux'
import * as actions from '../../../../actions/editSystem';
import { Field, reduxForm , change , reset  } from 'redux-form';
import Loading from '../../../common/loading';
import renderFieldInput from '../../../common/renderFieldInput';
import validate from './validate';

class Form extends Component {

  constructor(props) {
    super(props);
    this.state = {
      loadingGoogle:false,
      checkbox:false
    };
  }

  handleFormSubmit(data){
    const {close , closeForm} = this.props;
    this.setState({checkbox:false})
    this.props.AddressReturnEdit(data,close , closeForm);
  }

  onChangePostCode(code){
    postal_code.get(code, (address) => {
      var addressReturn = address.prefecture+" "+address.city+" "+address.area+" "+address.street;
      this.props.dispatch(change('Address-Return-Edit', 'address_return', addressReturn));
    });

  }
  setDefaultForm() {
    this.setState({
      checkbox:!this.state.checkbox
    },function () {
      this.props.dispatch(change('Address-Return-Edit', 'setDefault', this.state.checkbox.toString()));
    })
  }

  componentDidMount() {
    const {setChoseDefault} = this.props;
    if (setChoseDefault) {
      this.setState({
        checkbox:true
      });
      this.props.dispatch(change('Address-Return-Edit', 'setDefault', this.state.checkbox.toString()));
    }
  }




  render() {
    const {handleSubmit , loading} = this.props;

    return (
      <form onSubmit={handleSubmit(this.handleFormSubmit.bind(this))}>
        <table className="show my-table">
          <tbody>
              <tr className="my-tr">
                  <th className="my-th">
                      <label className="string required">名前 <span className="must-icon"></span></label>
                  </th>
                  <td className="my-td">
                    <Field
                      name="name_return"
                      component={renderFieldInput}
                      type="text"
                      placeholder="ユーザー名（英数字3-12文字）"
                    />
                  </td>
              </tr>
              <tr className="my-tr">
                  <th className="my-th">
                      <label className="string required">電話番号<span className="must-icon"></span> </label>
                  </th>
                  <td className="my-td">
                    <Field
                      name="phone"
                      component={renderFieldInput}
                      type="text"
                      placeholder="電話番号"
                    />
                  </td>
              </tr>
              <tr className="my-tr">
                  <th className="my-th">
                      <label className="string required">郵便番号<span className="must-icon"></span> </label>
                  </th>
                  <td className="my-td">
                    <Field
                      name="postcode"
                      component={renderFieldInput}
                      type="text"
                      placeholder="郵便番号"
                      disable={this.state.loadingGoogle}
                      onBlur={ (event) => this.onChangePostCode(event.target.value) }
                    />
                  </td>
              </tr>
              <tr className="my-tr">
                  <th className="my-th">
                      <label className="string required">住所<span className="must-icon"></span></label>
                  </th>
                  <td className="my-td">
                    <Field
                      name="address_return"
                      component={renderFieldInput}
                      type="text"
                      placeholder="住所"
                    />
                  </td>
              </tr>

              <tr className="my-tr">
                  <th className="my-th">
                      <label className="string required">メールアドレス<span className="must-icon"></span></label>
                  </th>
                  <td className="my-td">
                    <Field
                      name="email"
                      component={renderFieldInput}
                      type="text"
                      placeholder="住所"
                    />
                  </td>
              </tr>


              <tr className="my-tr">
                  <th className="my-th">
                      <label className="string required">デフォルトにする</label>
                  </th>
                  <td className="my-td">
                    <span  onClick={ ( ) => this.setDefaultForm() } className={ this.state.checkbox ? 'fake-chekcbox active' : 'fake-chekcbox' }>
                      デフォルトにする
                    </span>
                    <Field
                      name="setDefault"
                      component={renderFieldInput}
                      type="hidden"
                    />
                  </td>
              </tr>

          </tbody>
        </table>

        <div className="submit-box">
          <span className="button-submit form-edit" onClick={ () => this.props.closeForm() }>一覧へ</span>
          <input disabled={this.state.loadingGoogle} type="submit" name="commit" value="更新" data-disable-with="更新"/>
        </div>
        {
          loading ? <Loading></Loading> : null
        }
    </form>
    );
  }
}
const afterSubmit = (result, dispatch) => {
  return (
    dispatch(reset('Address-Return-Edit'))
  )
};
Form = reduxForm({
  form: 'Address-Return-Edit',
  onSubmitSuccess:afterSubmit,
  validate,
  destroyOnUnmount: false,

})(Form)
const mapStateToProps = (state) => {
  return {
    data: state.uploadImage.data,
    loading:state.common.loading,
    statusAPI:state.common.statusAPI

  }
}

// You have to connect() to any reducers that you wish to connect to yourself
Form = connect(mapStateToProps, actions )(Form);


export default Form;
