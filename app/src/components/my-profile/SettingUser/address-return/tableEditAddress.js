/* @flow */

import React, { Component } from 'react';
import { connect } from 'react-redux'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {faExclamationCircle} from '@fortawesome/free-solid-svg-icons';
import AsideLeft from '../aside';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
import FlashMassage from 'react-flash-message';
import Form from './form';
import Default from './defaultAddress';
import * as actions from '../../../../actions/editSystem';


class AddressReturnEdit extends Component {



  constructor(props) {
    super(props);
    this.state = {
      defaultData:null
    };
  }


  _renderStatusAPI() {
    setTimeout( () => {
      this.props.Dis_Status_Api(false);
    }, 2000);
    return(
      <div id="push-notification">
        <FlashMassage duration={2000}>
          <div className="alert">
            成功！変更が保存されました。
          </div>
        </FlashMassage>
      </div>
    )
  }






  openList() {
    this.props.openList();
  }
  openAdd() {
    this.props.openAdd();
  }

  _renderFormOrDefault(addressList) {
    if (addressList.length === 0) {
      return <Form></Form>
    }else {

      for (var i = 0; i < addressList.length; i++) {
        if (addressList[i].chosen_default === "1") {
          return <Default data={addressList[i]}></Default>
        }
      }

    }
  }

  render() {
    const {  statusAPI , addressList } = this.props;

    return (
      <div>
        <AsideLeft/>
        <div className="main">
          {
            statusAPI ? this._renderStatusAPI(): null
          }
          <h3>お届け先情報</h3>
          <span className="check-it-out">
            <FontAwesomeIcon icon={faExclamationCircle} style={{marginRight: "6px"}}/>支援したプロジェクトからのリターン（返礼品）は、以下に記入された住所にお届けされます。
          </span>

          <ul className="list-button">
            <li onClick={ () => this.openAdd() }>
              <span>
                <i className="icono-plusCircle"></i>
                <span>住所を追加する</span>
              </span>
            </li>
            <li onClick={ () => this.openList() } >
              <span>
                <i className="icono-list"></i>
                <span>住所録</span>
              </span>
            </li>

          </ul>

          {
            addressList?
            this._renderFormOrDefault(addressList) : null

          }


        </div>

      </div>
    );
  }
}



const mapStateToProps = (state) => {
  return {
    statusAPI:state.common.statusAPI
  }
}

// You have to connect() to any reducers that you wish to connect to yourself
AddressReturnEdit = connect(mapStateToProps , actions )(AddressReturnEdit);


export default AddressReturnEdit
