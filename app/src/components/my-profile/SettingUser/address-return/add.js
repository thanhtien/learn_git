/* @flow */

import React, { Component } from 'react';
import Rodal from 'rodal';
import { connect } from 'react-redux'
import { reduxForm , change  } from 'redux-form'
import * as actions from '../../../../actions/editSystem';

import FlashMassage from 'react-flash-message';
import postal_code  from 'japan-postal-code';
import validate from './validate';
import Form from './form';

class Add extends Component {

  constructor(props) {
    super(props);
    this.state = {
      loadingGoogle:false
    };
  }

  handleFormSubmit(data){
    this.props.AddressReturn(data);
  }

  _renderStatusAPI() {
    setTimeout( () => {
      this.props.Dis_Status_Api(false);
    }, 2000);
    return(
      <div id="push-notification">
        <FlashMassage duration={2000}>
          <div className="alert">
            成功！変更が保存されました。
          </div>
        </FlashMassage>
      </div>
    )
  }

  hideModal() {
    this.props.close();
  }
  onChangePostCode(code){
    postal_code.get(code, (address) => {
      var addressReturn = address.prefecture+" "+address.city+" "+address.area+" "+address.street;
      this.props.dispatch(change('Address-Return', 'address_return', addressReturn));
    });

  }
  render() {
    const { status } = this.props;

    return (
      <Rodal customStyles={{overflow:'auto'}} measure="%" width={80} height={60} visible={status} onClose={this.hideModal.bind(this)}>
        <div className="cover-no-blank inbox">
          <div style={{width:'100%'}} className="main">
            <Form
              typeAction="add"
              close={this.props.close}></Form>
          </div>
        </div>

      </Rodal>
    );
  }
}





Add = reduxForm({
  form: 'Address-Return',
  validate,
  destroyOnUnmount: false
})(Add)
const mapStateToProps = (state) => {
  return {
    loading:state.common.loading,
    statusAPI:state.common.statusAPI

  }
}

// You have to connect() to any reducers that you wish to connect to yourself
Add = connect(mapStateToProps, actions )(Add);



export default Add
