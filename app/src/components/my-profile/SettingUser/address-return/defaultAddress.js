/* @flow */

import React, { Component } from 'react';

export default class Default extends Component {



  render() {
    const {data} = this.props;

    return (
      <table className="show my-table">
        <tbody>
            <tr className="my-tr">
                <th className="my-th">
                    <label className="string required">名前 </label>
                </th>
                <td className="my-td">
                  <span>
                    {
                      data.name_return
                    }

                  </span>
                </td>
            </tr>
            <tr className="my-tr">
                <th className="my-th">
                    <label className="string required">電話番号 </label>
                </th>
                <td className="my-td">
                  <span>{data.phone}</span>
                </td>
            </tr>
            <tr className="my-tr">
                <th className="my-th">
                    <label className="string required">郵便番号 </label>
                </th>
                <td className="my-td">
                  <span>{data.postcode}</span>
                </td>
            </tr>
            <tr className="my-tr">
                <th className="my-th">
                    <label className="string required">住所</label>
                </th>
                <td className="my-td">
                  <span>{data.address_return}</span>
                </td>
            </tr>
            <tr className="my-tr">
                <th className="my-th">
                    <label className="string required">メールアドレス</label>
                </th>
                <td className="my-td">
                  <span>{data.email}</span>
                </td>
            </tr>
        </tbody>
      </table>
    );
  }
}
