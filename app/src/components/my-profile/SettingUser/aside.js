/* @flow */

import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faAngleRight} from '@fortawesome/free-solid-svg-icons';
export default class Aside extends Component {
  render() {
    return (
      <aside className="sidebar">
        <ul className="menu clearfix">
          <li className="">
            <NavLink activeClassName="selected" to={"/my-page/setting-user/edit-profile"}>プロフィール編集<span><FontAwesomeIcon icon={faAngleRight} /></span></NavLink>
          </li>

          <li>
            <NavLink
              activeClassName="selected"
              to={"/my-page/setting-user/address-return"}>
              お届け先情報
              <span><FontAwesomeIcon icon={faAngleRight} /></span>

            </NavLink>
          </li>
          <li>
            <NavLink activeClassName="selected" to={"/my-page/setting-user/credit-accounts"}>クレジットカード情報<span><FontAwesomeIcon icon={faAngleRight} /></span></NavLink>
          </li>
          <li>
            <NavLink activeClassName="selected" to={"/my-page/setting-user/bank-accounts-project-success"}>銀行口座情報<span><FontAwesomeIcon icon={faAngleRight} /></span></NavLink>
          </li>
          <li>
            <NavLink activeClassName="selected" to={"/my-page/setting-user/change-password"}>パスワード変更<span><FontAwesomeIcon icon={faAngleRight} /></span></NavLink>
          </li>
          
        </ul>
      </aside>
    );
  }
}
