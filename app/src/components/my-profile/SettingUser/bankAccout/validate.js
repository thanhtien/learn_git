import {
    CREDIT_MONTH_YEAR
} from '../../../../actions/types';
import valid from 'card-validator';
import creditCardType from 'credit-card-type';



const validate = (values , dispatch) => {
  const errors = {};


  if (!values.cvc) {
    errors.cvc = "セキュリティーコードが正しくありません";
  }


  if (!values.number_card) {
    errors.number_card = "クレジットカード情報は正しくありません。" ;
  }else if (values.number_card.length <= 13) {
    errors.number_card = "クレジットカード情報は正しくありません。" ;
  }
  var visaCards = creditCardType(values.number_card);
  var numberValidation = valid.number(values.number_card);

  if (visaCards.length === 0) {
    errors.number_card = "クレジットカード情報は正しくありません。" ;
  }

 if (!numberValidation.isPotentiallyValid) {
    errors.number_card = "クレジットカード情報は正しくありません。" ;
  }


	var minMonth = new Date().getMonth() + 1;
	var minYear = new Date().getFullYear();
	var month = parseInt(values.exp_month, 10);
	var year = parseInt(values.exp_year, 10);
	if (year.toString().length === 2) {
		year = year + 2000;
	}

  if ((!values.exp_month)|| ( month < 1) || ( month > 12)) {
    dispatch.dispatch({
      type:CREDIT_MONTH_YEAR,
      payload:"有効期限の月を入力してください"
    })
  }else if ((!values.exp_year)||( year < minYear)) {
    dispatch.dispatch({
      type:CREDIT_MONTH_YEAR,
      payload:"有効期限の年を入力してください"
    })
  }else if (( year > minYear && month < 13 && month > 0)|| (year === minYear && month >= minMonth && month < 13)) {
      dispatch.dispatch({
        type:CREDIT_MONTH_YEAR,
        payload:null
      })
	}else {
	  dispatch.dispatch({
	    type:CREDIT_MONTH_YEAR,
	    payload:"有効期限が正しくありません"
	  })
	}

  return errors;
};
export default validate;
