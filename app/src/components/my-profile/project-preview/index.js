/* @flow */

import React, { Component } from 'react';

import Banner from './banner';
import InforProject from './inforProject';
import ProjectReturn from './projectReturn';
import smoothScroll from '../../../components/common/smoothScroll';

class ProjectDetail extends Component {

  constructor(props) {
    super(props);
    this.state = {
      tab:1
    };
  }

  tagChange(tab) {
    this.setState({
      tab:tab
    })
  }

  tagChangeBanerScroll(){
    this.setState({
      tab:1
    }, () => {
      smoothScroll.scrollTo('project_return');

    })
  }

  htmlReturn(data) {
    return{
      __html:data
    }
  }

  render() {
    const {data,profile} = this.props;
    const {tab} = this.state;

      return (
        <div>

          <div className="pd-banner">
            {<Banner tagChange={ () => this.tagChangeBanerScroll() } tab={tab} data={data}></Banner>}
          </div>
          <div className="wraper">
              <div className="tab-area">
                  <ul>
                      <li onClick={ () => this.tagChange(1) } className={ tab === 1 ? "active":""}>プロジェクト詳細</li>
                      <li onClick={ () => this.tagChange(2) } className={ tab === 2 ? "active":""} >リターン</li>
                  </ul>
              </div>
              {/* .tab-area */}
              <div className={ tab === 1 ? "active pd-area animated fadeIn":"pd-area"}  id="tab-2">
                {
                  data.project_content ?
                  <div className="div-content" dangerouslySetInnerHTML={this.htmlReturn(data.project_content)} ></div> :
                    <p style={{textAlign:'center', border: "3px dotted #0071BA",padding:"10px"}}>
                      コンテンツがまだありません。
                    </p>
                }


                {/* .WYSIWYG */}
              </div>

              <div className="sns-share"></div>

              {/* #tab-1 */}
              <div className={ tab === 2 ? "active pd-area animated fadeIn":"pd-area"} id="tab-1">
                <InforProject tagChange={() => this.tagChange(2)} profile={profile} data={data}></InforProject>
                <div className="pd-box pd-box-2">
                  <h4 id="project_return" className="pd-box-title">リターンを選ぶ</h4>

                    <div className="return-list">
                      {
                        <ProjectReturn currentUser={profile.id} data={data.project_return}></ProjectReturn>
                      }

                    </div>
                </div>
              </div>
              {/* #tab-2 */}
          </div>
        </div>
      );


  }
}



export default ProjectDetail;
