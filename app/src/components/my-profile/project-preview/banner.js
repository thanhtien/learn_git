/* @flow */

import React, { Component } from 'react';

import NumberFormat from 'react-number-format';
import Slider from "react-slick";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {faPlay} from '@fortawesome/free-solid-svg-icons';
import smoothScroll from '../../../components/common/smoothScroll';
import { LazyLoadImage } from 'react-lazy-load-image-component';

export default class Banner extends Component {



  constructor(props) {
    super(props);
    this.state = {
      showVideo:true,
      hiddenPercent:false
    };
  }


  _renderProjectEndDate(data) {

    if (data) {

      if (data.format_collection_end_date.date) {
        return <span>{data.format_collection_end_date.date} 日</span>;
      }else {
        return <span>{data.format_collection_end_date.hour}時{data.format_collection_end_date.minutes}分</span>;
      }
    }
  }
  showVideo(){
    this.setState({
      showVideo:!this.state.showVideo
    })
  }
  YouTubeGetID(url){
     var ID = '';
     url = url.replace(/(>|<)/gi,'').split(/(vi\/|v=|\/v\/|youtu\.be\/|\/embed\/)/);
     if(url[2] !== undefined) {
       ID = url[2].split(/[^0-9a-z_\\-]/i);
       ID = ID[0];
     }
     else {
       url = url[0].replace(/(>|<)/gi,'').split(/(vi\/|v=|\/v\/|y2u\.be\/|\/embed\/)/);
       if(url[2] !== undefined) {
         ID = url[2].split(/[^0-9a-z_\\-]/i);
         ID = ID[0];
       }

     }
     return ID;
   }
  _renderMovie(data) {
    return(
      <div onClick={()=>this.showVideo()}>
        {
          this.state.showVideo ?
          <div className="cover-video-thum">
            <button className="button-play"><FontAwesomeIcon className="play-icon" icon={faPlay} /></button>
            <img src={data.thumbnail} alt="thum"/>
          </div>
          :<iframe title="iframe" allow="autoplay" allowFullScreen src={"https://www.youtube.com/embed/"+this.YouTubeGetID(data.thumbnail_movie_code)+"?rel=0&autoplay=1"}  width="560" height="315"></iframe>
        }
      </div>

    );
  }
  _renderSlider(data ,settings) {

    var image = [];
    var imageWillRender = [];

    for (var i = 0; i < data.thumbnail_img.length; i++) {
      var parser = document.createElement('a');
      parser.href = data.thumbnail_img[i].thumbnail_descrip;
      var imageArray = parser.pathname.split("/");

      if (imageArray[imageArray.length - 1] !== 'noimage-01.png') {
        var itemPush = {
          thumbnail_descrip:data.thumbnail_img[i].thumbnail_descrip,
          type:'image'
        }
        image.push(itemPush);
      }
    }
    if (image.length > 0) {
      imageWillRender = image;
    }else {
      imageWillRender = data.thumbnail_img;
    }

    return(
      <Slider {...settings}>
        {
          imageWillRender.map((item,i)=>{
            return(
              item.type === 'image' ?
              <div key={i}>
                  <LazyLoadImage
                    alt={"thumbnail"}
                    effect="blur"
                    src={item.thumbnail_descrip} />
              </div> :
              <div key={i}><iframe title="video" src={"https://www.youtube.com/embed/"+item.thumbnail_movie_code}></iframe></div>
            )
          })
        }
      </Slider>
    );
  }

  SetStyle(){
    const numberWidth = 0;
    if (numberWidth === 0) {
      return {width:"0%"};
    }
  }
  scrollToReturn() {

    const {data,tab} = this.props;
    if (data.project_return.length !== 0) {
      if (tab === 2) {
        smoothScroll.scrollTo('project_return');        
      }else {
        this.props.tagChange();
      }

    }else {
      alert('本プロジェクトはリターン品がありません。');
    }
  }
  render() {
    const {data} = this.props;
    const numberWidth = 0;

    let now_count = 0;

    const settings = {
      dots: true,
      dotsClass: "slick-dots slick-thumb",
      infinite: true,
      slidesToShow: 1,
      slidesToScroll: 1,
      autoplay: true,
      autoplaySpeed: 3000,
      arrows: false,
    };


    return (

      <div className="wraper">
          <div className="pannel">
              <p className="pannel-head">{data.project_name}</p>
              <div className="pannel-body">
                  <div onClick={ () => this.showVideo() }>

                  </div>
                  <div className="pannel-slider detail-page-project">

                    {
                      data.thumbnail_movie_code?
                      this._renderMovie(data):
                      this._renderSlider(data,settings)
                    }
                  </div>
                  <div className="pannel-content">
                      <h4 className="row-title sp">{data.project_name}</h4>
                      {
                        data.typeProject !== "fanclub" ?
                        <div className="row row-1">
                            <p className="row-label">現在の支援総額</p>
                            <p className="row-number">
                              <NumberFormat value={data.collected_amount} displayType={'text'} thousandSeparator={true} suffix={'円'} />
                            </p>
                            <p className="row-process-bar">
                              <i>{numberWidth}%</i>

                                <span
                                   style={this.SetStyle()}
                                >
                                </span>
                            </p>
                            <p className="row-note">目標金額は<NumberFormat value={data.goal_amount} displayType={'text'} thousandSeparator={true} suffix={'円'} /></p>
                        </div> : null
                      }
                      <div className="row row-2">
                          <p className="row-label">支援者数</p>
                          <p className="row-number">{now_count}人</p>
                      </div>
                      {
                        data.typeProject !== "fanclub" ?
                        <div className="row row-3">
                            <p className="row-label">募集終了まで残り</p>
                            <p className="row-number">{this._renderProjectEndDate(data)}</p>
                        </div> : null
                      }

                      <p className="pannel-btn"><span onClick={ () => this.scrollToReturn() } style={{cursor:'pointer'}}>プロジェクトを支援する</span></p>

                        {
                          data.typeProject === "fanclub" ?
                          <div className="row">
                            <p className="fanclub-info">
                              このプロジェクトは、定期課金のプロジェクト方式(定額課金制)です。<br/>
                              課金間隔はプロジェクト毎に異なり、1ヵ月～12ヵ月となります。<br/>
                              課金期間はプロジェクトが中断、もしくはお客様がプロジェクトから退会されるまでとなります。
                              選択した金額で支援者になり、課金間隔毎に集まった金額がプロジェクトにファンディングされます。
                            </p>

                          </div> : null
                        }
                  </div>
              </div>
          </div>
      </div>
    );
  }
}
