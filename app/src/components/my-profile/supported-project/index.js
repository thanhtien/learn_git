/* @flow */


import React, { Component } from 'react';
import NavProfile from '../nav';
import HeaderProfile from '../header';
import { connect } from 'react-redux'
import * as actions from '../../../actions/editSystem';
import Loading from '../../common/loading';
import LoadingScroll from '../../common/loadingScroll';
import ReactPaginate from 'react-paginate';
import NumberFormat from 'react-number-format';
import LinesEllipsis from 'react-lines-ellipsis';
import History from '../../../history';
import Blank from '../blank';
import FormatFunc from '../../common/FormatFunc';
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faChevronLeft, faChevronRight} from "@fortawesome/free-solid-svg-icons";


class subsidized_project extends Component {




  componentDidMount() {
    const {page} = this.props.match.params;
    const {subsidized_project} = this.props;
    var PageString = page.replace('page=', '');
    if (!subsidized_project) {
      this.props.getProjectdonated(Number(PageString)-1);
    }
    document.title = "支援したプロジェクト｜KAKUSEIDA";

  }


  handlePageClick = (data) => {
    let selected = data.selected;
    this.props.getProjectdonated(selected);
  };


  _renderTypeCard(item) {
    if (item.toLowerCase() === 'credit') {
      return "クレジットカード";
    }else if( item.toLowerCase() === 'bank' ) {
      return "銀行振込";
    }

  }

  goLink(id,blockLogic,projectname) {
    if (blockLogic === 'blk') {
      //opem modal
      alert(`プロジェクト「${projectname}」は、企画者による総合的な理由により中止となりました。`);
    }else {
      History.push(`/project-detail/${id}`);
    }
  }




  _renderStatus(status) {
    if (status === 'succeeded') {
      return(
        <span style={{color:'#0e6eb8'}}>支援済み</span>
      )
    }else if(status === 'unsuccess') {
      return (
        <span style={{color:'red'}}>振込待ち</span>
      )
    }else if( status === 'refunded' ) {
      return (
        <span>返金済み</span>
      )
    }
  }

  renderContentOrBlanK(){
    const {subsidized_project,loading , pageLoad} = this.props;


    if (subsidized_project) {
      if (subsidized_project.data.length !== 0) {
        return(
          <div className="cover-static cover_subsidized_project">

            <table className="static suported">
              <caption>支援したプロジェクト一覧</caption>
              <thead>
                <tr>
                  <th scope="col">プロジェクト名</th>


                  <th scope="col">支援日時</th>
                  <th scope="col">決済金額</th>
                  <th scope="col">選択したリターン／お届け予定</th>
                  <th scope="col">お届け先</th>
                  <th scope="col">支払方法</th>

                  <th scope="col">決済状況</th>
                </tr>
              </thead>

              <tbody>
                {
                  subsidized_project.data.map( (item,i) => {

                    return(
                      <tr key={i} onClick={()=>this.goLink(item.project_id,item.active,item.project_name)}>
                        <td title={item.project_name} data-label="プロジェクト名">
                          <div className="cover-mobi-m">
                            <div  className="project-name">
                              <pre>
                                <LinesEllipsis
                                  text={item.project_name}
                                  maxLine='2'
                                  ellipsis='...'
                                  trimRight
                                  basedOn='letters'
                                />
                              </pre>
                          </div>
                            <p className="image-project">
                              <img src={item.thumbnail} alt="thumbnail"/>
                            </p>
                          </div>
                        </td>



                        <td data-label="支援日時">
                          <div className="cover-mobi-m">
                            <FormatFunc date={item.created}></FormatFunc>
                          </div>

                        </td>
                        <td data-label="決済金額">
                          <div className="cover-mobi-m">
                            <NumberFormat value={item.invest_amount} displayType={'text'} thousandSeparator={true} suffix={'円'} />
                          </div>

                        </td>
                        <td title={item.return_amount} data-label="選択したリターン／お届け予定">
                          <div className="cover-mobi-m">
                            <pre>
                              <LinesEllipsis
                                text={item.return_amount}
                                maxLine='3'
                                ellipsis='...'
                                trimRight
                                basedOn='letters'
                              />
                            </pre>
                          </div>


                        </td>
                        <td data-label="お届け先">
                          <div className="cover-mobi-m">
                            {
                              item.address_return
                            }

                          </div>

                        </td>
                        <td data-label="支払方法">
                          <div className="cover-mobi-m">
                            {
                              this._renderTypeCard(item.type)
                            }

                          </div>

                        </td>
                        <td data-label="決済状況">
                          <div className="cover-mobi-m">
                            {
                              this._renderStatus(item.status)
                            }
                          </div>

                        </td>

                      </tr>
                    )
                  })
                }



              </tbody>
            </table>



              <div className="cover-paginate custom-suport">
                {
                  loading?<div className="loading-io"><LoadingScroll></LoadingScroll></div>:null
                }
                <ReactPaginate
                  previousLabel={<FontAwesomeIcon icon={faChevronLeft}/>}
                  nextLabel={<FontAwesomeIcon icon={faChevronRight}/>}
                  breakLabel={<span>...</span>}
                  breakClassName={"break-me"}
                  pageCount={subsidized_project.page_count}
                  marginPagesDisplayed={2}
                  pageRangeDisplayed={2}
                  forcePage={Number(pageLoad)}
                  onPageChange={this.handlePageClick}
                  containerClassName={"pagination"}
                  subContainerClassName={"pages pagination"}
                  activeClassName={"active"} />
              </div>




            <div className="clear-fix"></div>
          </div>
        )
      }else {
        return (
          <Blank data="支援したプロジェクトがまだありません。"></Blank>
        )
      }
    }else {
      return (
        <Loading></Loading>
      )
    }
  }
// {this.renderContentOrBlanK()}
  render() {
    const {profile} = this.props;
    return (
      <div className="c-area my-page">
        <div className="wraper">
          <HeaderProfile></HeaderProfile>
          {/* Profile Detail */}
          <div className="nav-here">
            {
              profile ? <NavProfile patron={profile.patron} patronfanclub={profile.patronfanclub} numberProject={profile.numberProject}/> : null
            }
          </div>
          {/* NAV Detail */}
          <div className="clear-fix"></div>
        </div>
        <div className="">
          <div className="wraper">
            {this.renderContentOrBlanK()}
          </div>
        </div>

      </div>
    );
  }
}

const mapStateToProps = (state) => {
    return {
      subsidized_project: state.common.subsidized_project.data,
      pageLoad:state.common.subsidized_project.page,

      loading:state.common.loading,
      profile:state.common.profile,
    }
}
export default connect(mapStateToProps, actions )(subsidized_project);
