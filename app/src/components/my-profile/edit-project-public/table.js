/* @flow */

import React, { Component } from 'react';
import {  Link } from 'react-router-dom';

import ReactPaginate from 'react-paginate';
import LoadingScroll from '../../common/loadingScroll';
import LinesEllipsis from 'react-lines-ellipsis';
import ModalDetail from './detail/index';

import FormTable from './add/form';
import FormTableClub from './add/form_fanclub';
import FormatFunc from '../../common/FormatFunc';
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faChevronLeft, faChevronRight} from "@fortawesome/free-solid-svg-icons";

export default class Table extends Component {
  constructor(props) {
    super(props);
    this.state = {
      toogleModal:false
    }
  }



  /**
   * [_renderStatus Render Status for request edit]
   * @param  {[type]} status
   * @return {[type]}        [status Layout]
   */
  _renderStatus(status) {
    if (status === '2') {
      return(
        <span style={{color:'#0e6eb8'}}>承認済み</span>
      )
    }else if(status === '1') {
      return (
        <span style={{color:'red'}}>承認却下</span>
      )
    }else if( status === '0' ) {
      return (
        <span>承認待ち</span>
      )
    }
  }


  handlePageClick = (data) => {
    this.props.handlePageClick(data);
  };

  openPopup(dataItem){
    this.setState({
      showHideDetail:true,
      dataItem:dataItem
    })
    document.body.className = 'block-body';
  }

  hidePopup() {
    this.setState({
      showHideDetail:false
    })
    document.body.className = '';
  }
  _renderForm(type) {

    const {id} = this.props;
    if (type === '1') {
      return (
        <FormTableClub id={id}></FormTableClub>
      )
    }
    else {
      return(
        <FormTable id={id}></FormTable>
      )
    }
  }

  render() {
    const {showHideDetail,dataItem} = this.state;
    const {id,data,project_default_edit} = this.props;

    return (
      <div className="cover-static">
        {
          data.data.length === 0 ? this._renderForm(project_default_edit.project_type) : null
        }
        {
          data ? <ModalDetail data={dataItem} showHideDetail={showHideDetail} hidePopup={()=>this.hidePopup()} ></ModalDetail> :null
        }
        <div className="cover-link-here">
          {
            data.status !== 0 && data.data.length > 0 ?
            <Link to={`/my-page/project-public/add-new-request/${id}`} className="link-to-project-detail" >
              情報更新の申請
            </Link> : null
          }


        </div>
        {/* Button */}
        {
          data && data.data.length > 0 ?
          <table className="static">
            <caption className="static-cap">  プロジェクトの情報更新一覧</caption>
            <thead>
              <tr>
                <th scope="col">タイトル</th>
                <th scope="col">メッセージ</th>
                <th scope="col">ステータス</th>
                <th scope="col">日付</th>
              </tr>
            </thead>
            <tbody>
            {
              data.data.map((item,i)=>{
                return(
                  <tr key={i} onClick={ () => this.openPopup(item) }>

                    <td data-label="タイトル">
                      {
                        item.title
                      }
                    </td>
                    <td data-label="メッセージ">
                      {

                        <pre>
                          <LinesEllipsis
                            text={item.message}
                            maxLine={2}
                            ellipsis={'...'}
                            trimRight
                            basedOn='letters'
                          />
                        </pre>
                      }
                    </td>

                    <td data-label="ステータス">
                      {
                        this._renderStatus(item.status)
                      }
                    </td>

                    <td data-label="日付">
                      {
                        <FormatFunc date={item.created} ></FormatFunc>
                      }
                    </td>

                  </tr>
                )
              })
            }

          </tbody>
        </table> : null
        }
        {

        data && data.length > 0 ?
          <div className="cover-paginate edit-public">
          {
            this.props.loading ? <div className="loading-io"><LoadingScroll></LoadingScroll></div>:null
          }


            <ReactPaginate
              previousLabel={<FontAwesomeIcon icon={faChevronLeft}/>}
              nextLabel={<FontAwesomeIcon icon={faChevronRight}/>}
              breakLabel={<span>...</span>}
              breakClassName={"break-me"}
              forcePage={Number(this.props.pageload)}
              pageCount={data.page_count}
              marginPagesDisplayed={2}
              pageRangeDisplayed={2}
              onPageChange={this.handlePageClick}
              containerClassName={"pagination"}
              subContainerClassName={"pages pagination"}
              activeClassName={"active"}
            />



        </div>: null
        }
      <div className="clear-fix"></div>
    </div>
    );
  }
}
