import React, { Component } from 'react';
import { Field } from 'redux-form';
import renderFieldInput from '../../../common/renderFieldInput';
import renderDateTimePicker from '../../../common/renderDateTimePickerArray';
import renderFieldTextArea from '../../../common/renderFieldTextArea';
import RenderDropzone from '../../../common/renderDropzone';
import renderFieldCheckbox from '../../../common/renderFieldCheckbox';

import NumberFormat from './NumberFormat';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPlusSquare , faTrashAlt , faExclamationCircle } from '@fortawesome/free-solid-svg-icons';



export default class RenderReturn extends Component {
  constructor(props) {
    super(props)
    this.state = {
      stateDate:false,
      toogle:false
    }
    this.inputElement = React.createRef();

  }

  appenForm() {
    this.props.fields.push({});
  }

  render() {
    const {fields , meta: { touched, error, submitFailed }} = this.props;

    return (
      <div className="render-feild">

        {fields.map((project_return, index) => (
          <div key={index}>

            <div className="cover-header">
              <button
                className="button-remove"
                type="button"
                title="Remove Member"
                onClick={() => fields.remove(index)}
              >
                <FontAwesomeIcon icon={faTrashAlt} />
              </button>
            </div>

            <table className="my-table show">
              <tbody>
                <tr className="my-tr pc">
                  <th className="my-th">
                      <label className="integer optional" >リターン品の名前</label>
                  </th>
                  <td className="my-td" colSpan="3">
                      <div className="selectbox">
                        <Field
                          name={`${project_return}.name`}
                          component={renderFieldInput}
                          valueField="id"
                          type="text"
                          textField="name"
                          placeholder="リターン品の名前"
                        />

                      </div>
                  </td>


                </tr>
                <tr className="my-tr pc">
                  <th className="my-th">
                      <label className="integer optional" >支援額</label>
                  </th>
                  <td className="my-td">
                      <div className="selectbox">

                        <Field
                          name={`${project_return}.invest_amount`}
                          component={renderFieldInput}
                          valueField="id"
                          type="text"
                          textField="name"
                          placeholder="支援額"
                          normalize={NumberFormat}
                        />

                      </div>
                  </td>
                  <th className="my-th">
                      <label className="date optional" >お届け予定 </label>
                  </th>
                  <td className="my-td" >

                    <div className="selectbox">
                      <Field
                        name={`${project_return}.schedule`}
                        component={renderFieldInput}
                        type="text"
                        placeholder="お届け予定"
                        maxlength="10"
                      />
                    </div>

                  </td>
                </tr>

                <tr className="my-tr pc">
                  <th className="my-th">
                      <label className="integer optional" >件数 </label>
                  </th>
                  <td className="my-td" colSpan="3">
                      <div className="selectbox">
                        <Field
                          name={`${project_return}.max_count`}
                          component={renderFieldInput}
                          valueField="id"
                          type="text"
                          textField="name"
                          placeholder="件数"
                          maxlength="16"
                          normalize={NumberFormat}
                        />

                      </div>
                  </td>

                </tr>




                <tr className="my-tr sp">
                    <th className="my-th">
                        <label className="integer optional" >支援額 <span className="must-icon"></span></label>
                    </th>
                    <td className="my-td" colSpan="3">
                        <div className="selectbox">
                          <Field
                            name={`${project_return}.invest_amount`}
                            component={renderFieldInput}
                            valueField="id"
                            type="text"
                            textField="name"
                            placeholder="支援額"
                            normalize={NumberFormat}
                          />
                        </div>
                    </td>
                  </tr>

                  <tr className="my-tr sp">
                    <th className="my-th">
                        <label className="date optional" >お届け予定<span className="must-icon"></span></label>
                    </th>
                    <td className="my-td" colSpan="3" >
                      <input
                        style={{height:0,border:'none',padding:0}}
                        type="text"
                        ref={'input'+index}
                      />

                      <Field
                        valueRender={fields}
                        indexRender={index}
                        name={`${project_return}.schedule`}
                        showTime={false}
                        component={renderDateTimePicker}
                        placeholder="お届け予定"

                      />

                      <Field
                        valueRender={fields}
                        onChange={() => { this.FocusFix(index); }}
                        name={`${project_return}.un_schedule`}
                        id={`${project_return}.un_schedule`}
                        component={renderFieldCheckbox}
                        type="checkbox"/>

                    </td>
                  </tr>


                  <tr className="my-tr">
                      <th className="my-th">
                          <label className="string required">リターン品・説明文</label>
                      </th>
                      <td className="my-td" colSpan="3">

                        <Field
                          name={`${project_return}.return_amount`}
                          component={renderFieldTextArea}
                          type="text"
                          placeholder="リターン品・説明文"
                          maxlength={300}
                        />
                      <span className="warning">※最大300文字</span>

                      </td>
                  </tr>
                  <tr className="my-tr">
                      <th className="my-th">
                          <label className="file optional">画像 </label>
                      </th>
                      <td className="my-td" colSpan="3">
                        <div className="upload-box just-one">
                            <div className="input-file-outer">
                              <div className="dropzone">
                                {/*DropZone*/}

                                  <RenderDropzone
                                    path="thumbnailReturn"
                                    formName={`new_project_public_request`}
                                    feild={`${project_return}.thumnail`}></RenderDropzone>
                                  <p className="note">アップロードできる画像形式は <span style={{color: 'red'}}>*.jpeg と *.png</span>のみです</p>
                                {/*DropZone*/}
                                <Field style={{width:"100%"}} name={`${project_return}.thumnail`} component="input" type="hidden" />

                              </div>
                            </div>
                        </div>
                        <span className="sub warning"><i><FontAwesomeIcon icon={faExclamationCircle} /></i> {"※画像のサイズは600px＊400pxで最高です。容量 2MBまで 。"}</span>

                      </td>
                  </tr>
              </tbody>
            </table>

        </div>
        ))}

        <div style={{cursor:"pointer"}} className="m-add-return-form blank" onClick={() => this.appenForm()}>
          <p>
            <span className="plus-form">
              <FontAwesomeIcon icon={faPlusSquare} /> 新しいリターンを追加する
            </span>
          </p>
          {(touched || submitFailed) && error && <span>{error}</span>}
        </div>
      </div>
    );
  }
}
