function centsToDollaString(x){
  return x.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
}
export const NumberFormat = value => {
  var ValueFormat = value.replace(/[^0-9]+/g, "");
  return centsToDollaString(ValueFormat)
}



const minValue = min => value =>{
  if( value){
    var value = value.replace(/[^0-9]+/g, "");
    if( value < min ){
      return `Must be at least ${min}`;
    }
    if( value > 1000 ){
      return `Must be at <= 999`;
    }
  }

  if(value==''){
    return `Must be at least ${min}`;
  }

  return '';

}

export const minValue1 = minValue(1,1000);
export  default minValue1

