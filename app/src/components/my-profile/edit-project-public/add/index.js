/* @flow */

import React, { Component } from 'react';
import HeaderProfile from '../../header';
import { connect } from 'react-redux'
import * as actions from '../../../../actions/editSystem';
import Breadcrumbs from '../../../Breadcrumbs';
import FormTable from './form';
import FormTableClub from './form_fanclub';
import Loading from '../../../common/loading';

class Add extends Component {

  componentDidMount() {
    const {id} = this.props.match.params;
    this.props.getProjectDefaultEdit(id);
    document.title = "プロジェクト内容更新、リターン追加依頼｜KAKUSEIDA";
  }
  _renderForm(type) {

    const {id} = this.props.match.params;
    if (type === '1') {
      return (
        <FormTableClub id={id}></FormTableClub>
      )
    }
    else {
      return(
        <FormTable id={id}></FormTable>
      )
    }
  }
  render() {

    if (this.props.loading) {
      return(
        <Loading></Loading>
      )
    }


    const {data} = this.props;
    const {id} = this.props.match.params;
    var linkBreacrm = [
      {last:false,label:"自分のプロジェクト",link:'/my-page/post-project/page=1'},
      {last:false,label:"プロジェクトの情報更新一覧",link:`/my-page/edit-project-public/${id}/page=1`},
      {last:true,label:"プロジェクト内容更新、リターン追加依頼",link:null}
    ]
  


    return (
      <div className="c-area my-page">
        <div className="wraper">
          <HeaderProfile ></HeaderProfile>
          {/* Profile Detail */}
          <div className="clear-fix"></div>
        </div>

        {/* Nav Tab */}
        <div className="wraper">
          <div className="cover-Breadcrumbs"><Breadcrumbs linkBreacrm={linkBreacrm}></Breadcrumbs></div>
          {/* Breadcrumbs */}
        </div>
        {/* Nav Tab */}

        <div className="white-page">
          <div className="cover-no-blank">
            <div className="wraper">

              {
                data ? this._renderForm(data.project_type) : null
              }

              {/* Add New Form */}
              <div className="clear-fix"></div>
            </div>
          </div>
        </div>
        {/* White Page */}

      </div>
    );
  }
}



const mapStateToProps = (state) => {
  return {
    data: state.common.project_default_edit,
    loading:state.common.loading,

  }
}
export default connect(mapStateToProps, actions)(Add);
