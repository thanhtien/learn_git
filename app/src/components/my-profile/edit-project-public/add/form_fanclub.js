/* @flow */
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Field, reduxForm , FieldArray , reset  } from 'redux-form'

import * as actions from '../../../../actions/editSystem';

import Loading from '../../../common/loading';


import renderFieldInput from '../../../common/renderFieldInput';
import renderFieldTextArea from '../../../common/renderFieldTextArea';

import RenderReturn from './RenderReturn_fan';
import validate from './validate_fan';

import scrollToFirstError from './scrollToFirstError';


class TableEdit extends Component {


  /**
   * [handleFormSubmit Submit Form Create Project]
   * @param  {[type]} data [Data TO Server]
   */

  handleFormSubmit(data){

    const {id} = this.props;


    if (data.project_return) {
      for (var i = 0; i < data.project_return.length; i++) {

        if (data.project_return[i].invest_amount) {
          data.project_return[i].invest_amount = data.project_return[i].invest_amount.replace(/,/g, "");
        }
        if (data.project_return[i].max_count) {
          data.project_return[i].max_count = data.project_return[i].max_count.replace(/,/g, "");
        }
      }
    }

    data.id = id;

    this.props.AddNewEditPublic(data);
  }





  render() {
    const { handleSubmit } = this.props;

    if (this.props.loading) {
      return(
        <Loading></Loading>
      )
    }







    return (
      <div className="main main-write">



        <form onSubmit={handleSubmit(this.handleFormSubmit.bind(this))}>
          <h3 className="h3">{`「${this.props.project_name}」`}プロジェクト内容更新、リターン追加依頼</h3>


          <div>
            <table className={"my-table show"} >
              <tbody>
                <tr className="my-tr">
                    <th className="my-th">
                        <label className="string required">依頼名 <span className="must-icon"></span></label>
                    </th>
                    <td className="my-td" colSpan="3">

                      <Field
                        name="title"
                        component={renderFieldInput}
                        type="text"
                        placeholder="プロジェクト名（最大150文字）"
                        nameError="title"
                        maxlength={150}
                      />
                    <span className="warning">
                      ※最大150文字
                    </span>

                    </td>
                </tr>


                <tr className="my-tr">
                    <th className="my-th">
                        <label className="integer optional">
                          内容
                        </label>
                    </th>
                    <td className="my-td" colSpan="3">
                      <Field
                        name="message"
                        component={renderFieldTextArea}
                        type="text"
                        maxlength={1000}
                        placeholder="メッセージ （最大1000文字）"
                        />
                      <span className="warning">※最大1000文字</span>

                    </td>
                </tr>


            </tbody>
            </table>


          </div>
          {/*TAB 1*/}
          <div >
            <h2 style={{marginBottom:'7px'}} >リターン追加</h2>
            <FieldArray name="project_return" component={RenderReturn} />
          </div>
          {/*TAB 2*/}
          <div className="submit-box submit-box-create-project">
            <input type="submit" name="commit" value="送信" data-disable-with="保存する"/>

          </div>

        </form>




      </div>
    );
  }
}


const afterSubmit = (result, dispatch) => {
  return (
    dispatch(reset('new_project_public_request'))
  )
};

TableEdit = reduxForm({
  form: 'new_project_public_request',
  destroyOnUnmount: false,
  onSubmitSuccess:afterSubmit,
  onSubmitFail: (errors , dispatch) => scrollToFirstError(errors , dispatch),
  validate

})(TableEdit);

const mapStateToProps = (state) => {
  var dataEdit = {
    goal_amount:state.common.project_default_edit.goal_amount
  }

  return {
    initialValues: dataEdit,
    project_name:state.common.project_default_edit.project_name,
    data: state.uploadImage.data,
    percent: state.uploadImage.progess,
    disable: state.uploadImage.disable,
    loading:state.common.loading,
  }
}

// You have to connect() to any reducers that you wish to connect to yourself
TableEdit = connect(mapStateToProps, actions , null , { withRef: true })(TableEdit);


export default TableEdit
