/**
 * [validate For Validate Form New Project]
 * @param  {[type]} values [values Form]
 * @return {[type]} Error  [Error For Validate match]
 */


const validate = values => {
    const errors = {};


    if (!values.title) {
      errors.title = 'タイトルを入力してください';

    }
    if (!values.message) {
      errors.message = 'メッセージを入力してください';

    }


    const project_returnErrors = []
    if (values.project_return) {
      values.project_return.forEach((projectReturn, memberIndex) => {
        const memberErrors = {}
          if (!projectReturn || !projectReturn.show_order) {
              memberErrors.show_order = '表示順を入力してください';
              project_returnErrors[memberIndex] = memberErrors;
          }
        if (!projectReturn || !projectReturn.invest_amount) {
          memberErrors.invest_amount = '支援額を入力してください';
          project_returnErrors[memberIndex] = memberErrors;
        }
        if (!projectReturn || !projectReturn.name ) {
          memberErrors.name = 'リターン品の名前を入力してください';
          project_returnErrors[memberIndex] = memberErrors;
        }
      })
      if (project_returnErrors.length) {
        errors.project_return = project_returnErrors
      }
    }

    return errors;
};

export default validate;
