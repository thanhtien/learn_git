/* @flow */

import React, { Component } from 'react';
import Rodal from 'rodal';
import ProjectReturn from './projectReturn';
import ProjectPublic from './ProjectPublic';
export default class ModalDetail extends Component {
  hidePopup() {
    this.props.hidePopup();
  }
  render() {
    const {showHideDetail,data} = this.props;

    return (
      <div>
        <Rodal measure={'%'} width={80} height={80} visible={showHideDetail} onClose={this.hidePopup.bind(this)}>
          <div style={{overflow:'auto',height:'100%'}}>
            <table className="static padding">
              <caption>公開のプロジェクト・更新内容</caption>
            </table>



            {
              data ?  <ProjectPublic data={data}></ProjectPublic> : null
            }

            <div className="pd-area" style={{display:'block'}}>
              <div className="return-list">
                {
                  data ?  <ProjectReturn data={data.project_return} /> : null
                }
              </div>
            </div>
            {/* Return List */}



          </div>


        </Rodal>
      </div>
    );
  }
}
