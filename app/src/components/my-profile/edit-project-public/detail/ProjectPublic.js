/* @flow */

import React, { Component } from 'react';
import NumberFormat from 'react-number-format';

export default class ProjectPublic extends Component {

  render() {
    const {data} = this.props;

    return (
      <div className="cover-no-blank">
         <div className="main-public main">
           <div className="cover-table active">
             <table id="my-table-edit" className={"my-table show"} >
               <tbody>
                 <tr className="my-tr">
                     <th className="my-th">
                         <label className="string required">サブジェクト </label>
                     </th>
                     <td className="my-td" colSpan="3">
                       <span>{data.title}</span>
                     </td>
                 </tr>

                 <tr className="my-tr">
                     <th className="my-th">
                         <label className="string required">メッセージ <br/>（理由などについて） </label>
                     </th>
                     <td className="my-td" colSpan="3">
                       <span>{data.message}</span>
                     </td>
                 </tr>

                 <tr className="my-tr">
                     <th className="my-th">
                         <label className="string required">目標金額</label>
                     </th>
                     <td className="my-td" colSpan="3">
                       <span><NumberFormat value={data.goal_amount} displayType={'text'} thousandSeparator={true} suffix={'円'} /></span>
                     </td>
                 </tr>

             </tbody>
             </table>


           </div>
         </div>
      </div>


    );
  }
}
