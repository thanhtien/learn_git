/**
 * [validate For Validate Form New Project]
 * @param  {[type]} values [values Form]
 * @return {[type]} Error  [Error For Validate match]
 */

 function validateUrl(value) {
   return /https?:\/\/(?:(youtu|y2u)\.be\/|(?:[a-z]{2,3}\.)?youtube\.com\/watch(?:\?|#\t)v=)([\w-]{11}).*/ig.test(value);
 }

const validate = values => {
    const errors = {};

    if (!values.project_name) {
      errors.project_name = 'プロジェクト名を入力してください。';
    }
    if (!values.collection_end_date) {
        errors.collection_end_date = 'collection_end_datexxx';
    }


    if (!values.goal_amount) {
      errors.goal_amount = '目標金額を入力してください';

    }

    if (values.goal_amount) {

      if( Number(values.goal_amount.replace(/,/g, "")) < 10000) {
        errors.goal_amount = '目標金額は、10,000円以上の金額で入力してください。';
      }
    }




    if (values.thumbnail_movie_code) {
      if (!validateUrl(values.thumbnail_movie_code)) {
        errors.thumbnail_movie_code = '動画のURLを入力してください';
      }
    }

    if (!values.category_id) {
      errors.category_id = 'カテゴリを選択してください';
    }

    if (!values.collection_end_date) {
      errors.collection_end_date = '募集終了日を記入してください';

    }else{

        Date.prototype.addDays = function(days) {
            var date = new Date(this.valueOf());
            date.setDate(date.getDate() + days);
            return date;
        }
        Date.prototype.addYear = function(year) {
            var date = new Date(this.valueOf());
            date.setFullYear(date.getFullYear() + year);

            return date;
        }
        var dayChoose = values.collection_end_date;

        let dateChange2 = dayChoose;
        let today = new Date();
        today = today.addDays(2);
        let todayCom= new Date(today.getFullYear(),today.getMonth(),today.getDate());
        let dayChoose2= new Date(dateChange2.getFullYear(),dateChange2.getMonth(),dateChange2.getDate());
        const diffTime = dayChoose2.getTime() - todayCom.getTime();
        const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24));
        console.log(diffDays);
        if(diffDays<0){
            errors.collection_end_date = '下記に指定されている範囲内に記入してください';
        }
        let todayYear = new Date();
        todayYear = todayYear.addDays(2);
        todayYear = todayYear.addYear(1);
        let todayYCom= new Date(todayYear.getFullYear(),todayYear.getMonth(),todayYear.getDate());
        const diffTime1 = dayChoose2.getTime() - todayCom.getTime();
        const diffDays1 = Math.ceil(diffTime / (1000 * 60 * 60 * 24));

        if (dayChoose2.getTime()> todayYCom.getTime()){
            errors.collection_end_date = '下記に指定されている範囲内に記入してください';
        }
        console.log(diffDays1);
    }



    const project_returnErrors = []
    if (values.project_return) {
      values.project_return.forEach((projectReturn, memberIndex) => {
        const memberErrors = {}
        if (!projectReturn || !projectReturn.invest_amount) {
          memberErrors.invest_amount = '支援額を入力してください';
          project_returnErrors[memberIndex] = memberErrors;
        }
        if (!projectReturn || !projectReturn.name ) {
          memberErrors.name = 'リターン品の名前を入力してください';
          project_returnErrors[memberIndex] = memberErrors;
        }
      })
      if (project_returnErrors.length) {
        errors.project_return = project_returnErrors
      }
    }





    return errors;
};

export default validate;
