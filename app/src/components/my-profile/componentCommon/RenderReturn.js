import React, { Component } from 'react';
import { Field } from 'redux-form';
import renderFieldInput from '../../common/renderFieldInput';

import renderFieldTextArea from '../../common/renderFieldTextArea';
import RenderDropzone from '../../common/renderDropzoneProjectReturn';
import RenderDropzoneNew from '../../common/renderDropzone';

import Loading from '../../../components/common/loading';

import NumberFormat from './NumberFormat';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPlusSquare , faTrashAlt , faExclamationCircle } from '@fortawesome/free-solid-svg-icons';
import smoothScroll from '../../../components/common/smoothScroll';


import axios from 'axios';
const token = 'Bearer '+ localStorage.getItem('token');
const config = {
  headers: {
    "Authorization" : token
  }
};
var CONFIG = require('../../../config/common');

if (process.env.NODE_ENV === "development") {
  CONFIG = CONFIG.CONFIG.DEV;
}else {
  CONFIG = CONFIG.CONFIG.PRODUCT;
}
const ROOT_URL = CONFIG.ROOT_URL;


export default class RenderReturn extends Component {
  constructor(props) {
    super(props)
    this.state = {
      stateDate:false,
      toogle:false,
      itemFeild:0,
      loadingDelete:false
    }
    this.inputElement = React.createRef();

  }



  appenForm() {
    const {fields} = this.props;
    fields.push({});
    this.setState({
      itemFeild:this.state.itemFeild + 1
    },function () {
      smoothScroll.scrollTo(`project_return_index_${this.state.itemFeild}`);
    })

  }


  appenFormEdit() {
    this.props.fields.push({});
    const {fields} = this.props;
    if (fields.getAll()) {
      this.setState({
        itemFeild:fields.getAll().length + 1
      },function () {
        smoothScroll.scrollTo(`project_return_index_${this.state.itemFeild}`);
      })
    }else {
      this.setState({
        itemFeild:this.state.itemFeild + 1
      },function () {
        smoothScroll.scrollTo(`project_return_index_${this.state.itemFeild}`);
      })
    }

  }

  loadingDelete(){
   this.setState({
     loadingDelete:!this.state.loadingDelete
   })
 }






  removeForm(index){
    this.props.fields.remove(index);
    this.setState({
      itemFeild:this.state.itemFeild - 1
    })
  }

  removeFormEdit(index,project_return){
   const {idProject} = this.props;

   if (JSON.stringify(project_return[index]) !== '{}') {
     this.loadingDelete();

     axios.delete(ROOT_URL+`v2/user/Projects/delProjectReturnEdit/${idProject}/${project_return[index].id}`,config)

     .then(response =>{

       this.loadingDelete();
       this.props.fields.remove(index);
       this.setState({
         itemFeild:this.state.itemFeild - 1
       })

     })
     .catch( (error) => {
       this.loadingDelete();
       alert(error.response.data.status);
     });

   }else {
     this.props.fields.remove(index);
     this.setState({
       itemFeild:this.state.itemFeild - 1
     })
   }

 }


  render() {
    const {fields , meta: { touched, error, submitFailed },formName} = this.props;
    const {loadingDelete} = this.state;

    return (
      <div className="render-feild">

        {
          loadingDelete ? <Loading></Loading> : null
        }


        {fields.map((project_return, index) => (
          <div key={index} id={`project_return_index_${index+1}`}>

            <div className="cover-header">
              <button
                className="button-remove"
                type="button"
                title="Remove Member"
                onClick={ formName === 'edit-project-user' ? () => this.removeFormEdit(index,fields.getAll()) : () => this.removeForm(index)}
              >
                <FontAwesomeIcon icon={faTrashAlt} />
              </button>
            </div>

            <table className="my-table show">
              <tbody>
                <tr className="my-tr">
                  <th className="my-th">
                      <label className="integer optional" >
                        リターン品の名前
                        <span className="must-icon"></span>
                      </label>
                  </th>
                  <td className="my-td" colSpan="3">
                      <div className="selectbox">
                        <Field
                          name={`${project_return}.name`}
                          component={renderFieldInput}
                          valueField="id"
                          type="text"
                          textField="name"
                          placeholder="リターン品の名前"
                        />

                      </div>
                  </td>


                </tr>


                <tr className="my-tr pc">
                  <th className="my-th">
                      <label className="integer optional" >支援額 <span className="must-icon"></span> </label>
                  </th>
                  <td className="my-td">
                      <div className="selectbox">
                        <Field
                          name={`${project_return}.invest_amount`}
                          component={renderFieldInput}
                          valueField="id"
                          type="text"
                          textField="name"
                          placeholder="支援額"
                          normalize={NumberFormat}
                        />

                      </div>
                  </td>
                  <th className="my-th">
                      <label className="date optional" >お届け予定 </label>
                  </th>
                  <td className="my-td" >

                    <div className="selectbox">
                      <Field
                        name={`${project_return}.schedule`}
                        component={renderFieldInput}
                        type="text"
                        placeholder="お届け予定"
                        maxlength="10"
                      />
                    </div>

                  </td>
                </tr>






                <tr className="my-tr sp">
                    <th className="my-th">
                        <label className="integer optional" >支援額 <span className="must-icon"></span></label>
                    </th>
                    <td className="my-td" colSpan="3">
                        <div className="selectbox">
                          <Field
                            name={`${project_return}.invest_amount`}
                            component={renderFieldInput}
                            valueField="id"
                            type="text"
                            textField="name"
                            placeholder="支援額"
                            normalize={NumberFormat}
                          />
                        </div>
                    </td>
                  </tr>

                  <tr className="my-tr sp">
                    <th className="my-th">
                        <label className="date optional" >お届け予定 </label>
                    </th>
                    <td className="my-td" >

                      <div className="selectbox">
                        <Field
                          name={`${project_return}.schedule`}
                          component={renderFieldInput}
                          type="text"
                          placeholder="お届け予定"
                          maxlength="10"
                        />
                      </div>
                      </td>
                    </tr>

                    <tr className="my-tr">
                      <th className="my-th">
                          <label className="integer optional" >件数 </label>
                      </th>
                      <td className="my-td" colSpan="3">
                          <div className="selectbox">
                            <Field
                              name={`${project_return}.max_count`}
                              component={renderFieldInput}
                              valueField="id"
                              type="text"
                              textField="name"
                              placeholder="件数"
                              maxlength="16"
                              normalize={NumberFormat}
                            />

                          </div>
                      </td>

                    </tr>







                  <tr className="my-tr">
                      <th className="my-th">
                          <label className="string required">リターン品・説明文</label>
                      </th>
                      <td className="my-td" colSpan="3">

                        <Field
                          name={`${project_return}.return_amount`}
                          component={renderFieldTextArea}
                          type="text"
                          placeholder="リターン品・説明文"
                          maxlength={300}
                        />
                      <span className="warning">※最大300文字</span>

                      </td>
                  </tr>
                  <tr className="my-tr">
                      <th className="my-th">
                          <label className="file optional">画像 </label>
                      </th>
                      <td className="my-td" colSpan="3">
                        <div className="upload-box just-one">
                            <div className="input-file-outer">
                              <div className="dropzone">
                                {/*DropZone*/}


                              {
                                // formName === 'new_project'?
                                // <Field style={{width:"100%"}} name={`${project_return}.thumnail`} component="input" type="hidden" /> :
                                // <Field
                                //   style={{width:"100%"}}
                                //   name={`${project_return}.thumnail`}
                                //   component={renderFieldInputHidden}
                                //   type="hidden"
                                // />
                              }





                                    {
                                      formName === 'new_project'?
                                      <div>
                                        <RenderDropzoneNew
                                          path="thumbnailReturn"
                                          formName={formName}
                                          feild={`${project_return}.thumnail`}></RenderDropzoneNew></div> :
                                      <Field
                                        style={{width:"100%"}}
                                        name={`${project_return}.thumnail`}
                                        formName={formName}
                                        path="thumbnailReturn"
                                        component={RenderDropzone}
                                        type="hidden"
                                      />
                                    }

                                    <Field style={{width:"100%"}} name={`${project_return}.thumnail`} component="input" type="hidden" />


                                  <p className="note">アップロードできる画像形式は <span style={{color: 'red'}}>*.jpeg と *.png</span>のみです</p>
                                {/*DropZone*/}


                              </div>
                            </div>
                        </div>
                        <span className="sub warning"><i><FontAwesomeIcon icon={faExclamationCircle} /></i> {"※画像のサイズは600px＊400pxで最高です。容量 2MBまで 。"}</span>

                      </td>
                  </tr>
              </tbody>
            </table>

        </div>
        ))}

        <div style={{cursor:"pointer"}} className="m-add-return-form blank" onClick={ formName === 'edit-project-user' ? ()=>this.appenFormEdit() : () => this.appenForm()}>
          <p>
            <span className="plus-form">
              <FontAwesomeIcon icon={faPlusSquare} /> 新しいリターンを追加する
            </span>
          </p>
          {(touched || submitFailed) && error && <span>{error}</span>}
        </div>
      </div>
    );
  }
}
