/* @flow */

import React, { Component } from 'react';


export default class TypeProject extends Component {


  constructor(props) {
    super(props)
    this.state = {
      typeProject:'normal'
    }
  }


  render() {
    const {typeProject} = this.props;
    return (
      <div className="cover-no-blank">
        <div className="main main-write" style={{padding:0}}>
          <table className={"my-table show"} style={{margin:"15px 0"}}>
            <tbody>
              <tr className="my-tr">
                <th className="my-th">
                  <label className="string required">プロジェクト種類</label>
                </th>
                <td className="my-td" colSpan="3">

                  {
                    typeProject === '1' ? '定期課金のプロジェクトを作る' : '普通のプロジェクト'
                  }


                </td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>


    );
  }
}
