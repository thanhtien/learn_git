/* @flow */
import React, { Component } from 'react';
import NormalProject from './NormalProject';
import FanClubProject from './FanClubProject';


class TableEdit extends Component {
  constructor(props) {
    super(props)
    this.state = {
      typeProject:'fanclub'
    }
  }



  render() {
    const {previewOpen , dataProject ,idProject} = this.props;

    return (
      <div className="main main-write">




        {
          dataProject.project_type === '0' ?
          <NormalProject
            idProject={idProject}
            dataProject={dataProject}
            previewOpen={previewOpen} /> : null
        }

        {
          dataProject.project_type === '1' ?
          <FanClubProject
            idProject={idProject}
            dataProject={dataProject}
            previewOpen={previewOpen} /> : null
        }


      </div>
    );
  }
}




export default TableEdit
