import React from 'react';
import {connect} from 'react-redux';
import Notifications, { info } from 'react-notification-system-redux';
import {CONFIG} from './config';
import * as actions from '../../actions/common';

class NotificationSocketIO extends React.Component {

  dispatchNotification(notificationOpts) {
    this.props.dispatch(info(notificationOpts));
  }

  GoNoti(data) {
    this.props.readNotification(data.data.id);
    window.location.assign(data.data.url);
  }

  componentDidMount() {
    const {profile} = this.props;
    var socketArray = [];
    if (profile.eventProjectSocket !== null) {
       socketArray = profile.eventProjectSocket.split(',');
    }

    socketArray.map( (item,i) => {

      if (this.props.socket.hasListeners(item) !== true) {
        return(

          this.props.socket.on(item, (data) => {
            const notificationOpts = {
              title: data.data.title,
              message: data.data.message,
              position: 'br',
              autoDismiss: 7,
              children: (
                <div className="avartar-noti">
                  <button className="click-noti" onClick={()=>this.GoNoti(data)}></button>
                  <img src={data.data.ava} alt="ava" />
                </div>
              ),

            };
            this.props.pushSocket(notificationOpts);
            this.props.reciveNotification();

          })

        )
      }

      return null;

    });

  }



	render() {
    const {notifications} = this.props;

		return (
	    <div>
        <Notifications notifications={notifications} style={CONFIG} />
      </div>
		);
	}
}


export default connect(
  state => ({
    notifications: state.notifications
  }),actions
)(NotificationSocketIO);
