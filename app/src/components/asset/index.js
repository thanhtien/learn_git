/* @flow */

import { Component } from 'react';
import 'react-widgets/dist/css/react-widgets.css';
import './style/base.css';
import './style/common.css';
import './style/pure-css-icon.css';
import 'rodal/lib/rodal.css';
import 'react-lazy-load-image-component/src/effects/blur.css';
import './style/slick.min.css';
import './style/slick-theme.min.css';
import './style/login.css';
import './style/register.css';
import './style/thank.css';
import './style/commonComponent.css';
import './style/profile.css';
import './style/news.css';
import './style/project-detail.css';
import './style/project-list.css';
import './style/StripePay.css';
import './style/staticPage.css';
import './style/error.css';
import './style/welcome.css';
import './style/comment.css';
import './style/statistic.css';
import './style/firstUser.css';
import './style/kakuseitowa.css';
import './style/daihyou-aisatsu.css';
import './style/company.css';
import './style/email.css';
import './style/access.css';
import './style/towakakuseida.css';
import './style/ribbon.css';

export default class Style extends Component {
  render() {
    return (
      null
    );
  }
}
