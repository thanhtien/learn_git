/* @flow */

import React, { Component } from 'react';
import ProjectList from '../projectList';
import Loading from '../common/loading';
import LoadingScroll from '../common/loadingScroll';
import * as actions from '../../actions/TopPage';
import { connect } from 'react-redux';
import ReactPaginate from 'react-paginate';
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faChevronLeft, faChevronRight} from "@fortawesome/free-solid-svg-icons";

class ProjectEnough extends Component {

  componentDidMount() {

    const {page} = this.props.match.params;
    var PageString = page.replace('page=', '');
    if (!this.props.ProjectEnough) {
      this.props.ProjectEnoughExpired(Number(PageString)-1);
    }


    document.title = "終了間際のプロジェクト｜KAKUSEIDA";
  }

  handlePageClick = (data) => {
    let selected = data.selected;
    this.props.ProjectEnoughExpired(selected);
  };
  _renderView(ProjectEnough) {
    const linkBreacrm = [
      {last:true,label:"終了間際のプロジェクト",link:null}
    ]
    if (ProjectEnough.data.length !== 0) {
      return(
        <div>
          <ProjectList linkBreacrm={linkBreacrm} data={ProjectEnough.data} title={"終了間際のプロジェクト"}/>
          <div className="cover-paginate">
            {
              this.props.loading?<div className="loading-io"><LoadingScroll></LoadingScroll></div>:null
            }

            <ReactPaginate
              previousLabel={<FontAwesomeIcon icon={faChevronLeft}/>}
              nextLabel={<FontAwesomeIcon icon={faChevronRight}/>}
              breakLabel={<span>...</span>}
              breakClassName={"break-me"}
              pageCount={ProjectEnough.page_count}
              marginPagesDisplayed={2}
              forcePage={Number(this.props.pageLoad)}
              pageRangeDisplayed={2}
              onPageChange={this.handlePageClick}
              containerClassName={"pagination"}
              subContainerClassName={"pages pagination"}
              activeClassName={"active"} />
          </div>
          <div className="clear-fix"></div>
        </div>
      )
    }else {
      return (
        <div>
          <ProjectList linkBreacrm={linkBreacrm} data={ProjectEnough.data} title={"終了間際のプロジェクト"}/>
        </div>
      )
    }
  }
  render() {
    const {ProjectEnough} = this.props;

    return (
      <div>
        <div>
          {
            ProjectEnough ?
              this._renderView(ProjectEnough)
            : <Loading></Loading>
          }

        </div>
      </div>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    ProjectEnough: state.topPage.ProjectEnough.data,
    pageLoad: state.topPage.ProjectEnough.page,
    loading:state.common.loading
  }
}

export default connect(mapStateToProps, actions)(ProjectEnough);
