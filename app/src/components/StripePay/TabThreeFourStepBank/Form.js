/* @flow */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actions from '../../../actions/DonateAction';
import ProjectReturn from './ProjectReturn';

 class Form extends Component {

  /**
   * [confirmToDonate Confirm Donate]
   * @return {[type]} [Donate True Or False]
   */
  confirmToDonate() {
    const {invest_amount,project_id,backing_level_id} = this.props;
    this.props.DonateBank(invest_amount,project_id,backing_level_id);
  }




  _renderAddress(addressList) {
    for (var i = 0; i < addressList.length; i++) {
      if (addressList[i].chosen_default === "1") {
        return (
          <div className="form-area form-area-3">
              <p className="page-tab-label">お届け先情報</p>
              <div className="form-box">
                  <div className="form-field form-field-1">
                      <p className="form-field-name">名前</p>
                      <div className="form-field-content">
                        <p>
                          {addressList[i].name_return}
                        </p>
                      </div>
                  </div>
                  <div className="form-field form-field-2">
                      <p className="form-field-name">郵便番号</p>
                      <div className="form-field-content">
                        <p>
                          {addressList[i].postcode}
                        </p>
                      </div>
                  </div>

                  <div className="form-field form-field-4">
                      <p className="form-field-name">住所</p>
                      <div className="form-field-content">
                        <p>
                          {addressList[i].address_return}
                        </p>
                      </div>
                  </div>
                  <div className="form-field form-field-5">
                      <p className="form-field-name">電話番号</p>
                      <div className="form-field-content">
                        <p>
                          {addressList[i].phone}
                        </p>
                      </div>
                  </div>

                  <div className="form-field form-field-5">
                      <p className="form-field-name">メールアドレス</p>
                      <div className="form-field-content">
                        <p>
                          {addressList[i].email}
                        </p>
                      </div>
                  </div>




              </div>
          </div>
        )
      }
    }
  }


  render() {
    const {initialValues,dataBacker} = this.props;
    return (
      <div>
        {
          initialValues ?
          <form>
            <div className="form-area">
                <p className="page-tab-label">クレジットカード</p>
                  <div className="form-box">
                    <div className="form-field">
                        <p className="form-field-name">銀行名<span>必須</span></p>
                        <div className="form-field-content">
                          <p>
                            {initialValues.bank_name}
                          </p>
                        </div>
                    </div>

                    <div className="form-field">
                        <p className="form-field-name">支店名<span>必須</span></p>
                        <div className="form-field-content">
                          <p>
                            {initialValues.bank_branch}
                          </p>
                        </div>
                    </div>


                    <div className="form-field">
                        <p className="form-field-name">口座種類<span>必須</span></p>
                        <div className="form-field-content">
                          <p>
                            {initialValues.bank_type}
                          </p>
                        </div>
                    </div>


                    <div className="form-field">
                        <p className="form-field-name">口座番号<span>必須</span></p>
                        <div className="form-field-content">
                          <p>
                            {initialValues.bank_number}
                          </p>
                        </div>
                    </div>


                    <div className="form-field">
                        <p className="form-field-name">口座カナ名義<span>必須</span></p>
                        <div className="form-field-content">
                          <p>
                            {initialValues.bank_owner}
                          </p>
                        </div>
                    </div>


                  </div>
            </div>

            {
              this._renderAddress(initialValues.listAddress)
            }

            <ProjectReturn data={dataBacker}></ProjectReturn>



            <span style={{cursor:"pointer"}} onClick={()=>this.confirmToDonate()} className="btn-submit">
              確認画面
            </span>
          </form> : null
        }
      </div>
    );
  }
}


const mapStateToProps = (state) => {
    return {
      initialValues: state.Donate.profileUpdate,
    }
}

// You have to connect() to any reducers that you wish to connect to yourself
export default connect(mapStateToProps, actions )(Form);
