/* @flow */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actions from '../../../actions/DonateAction';
import ProjectReturn from './ProjectReturn';
import History from '../../../history.js';
import {  Link } from 'react-router-dom';
import smoothScroll from '../../common/smoothScroll'
import CvcPop from '../../common/cvcPopup';
 class Form extends Component {

  constructor(props) {
    super(props);
    this.state = {
      showpass:false,
      valueCVC:'',
      errorCVC:''
    };
  }

  valueCVC(event){

    if (event.target.value === '') {
      this.setState({
        errorCVC:"セキュリティーコードが正しくありません"
      })
    }else {
      this.setState({
        errorCVC:""
      })
    }
    this.setState({valueCVC: event.target.value});
  }

  /**
   * [showpass Show hide CVC]
   * @return {[type]} [CVC HIDE/SHOW]
   */
   showpass(e) {
     e.stopPropagation();
     this.setState({
       showpass:!this.state.showpass
     })
   }

  /**
   * [confirmToDonate Confirm Donate]
   * @return {[type]} [Donate True Or False]
   */
  confirmToDonate() {

    const {invest_amount,project_id,backing_level_id,typePay,dataBacker} = this.props;
    if (typePay === 'stripe') {
      if (this.state.valueCVC === '') {
        this.setState({
          errorCVC:"セキュリティーコードが正しくありません"
        },function () {
          smoothScroll.scrollTo('errorCVC');
        })
      }else {

        this.props.Donate(dataBacker.project_type,this.state.valueCVC,invest_amount,project_id,backing_level_id);
      }
    }else {
      this.props.DonateBank(invest_amount,project_id,backing_level_id);
    }

  }


  gotoLink(link){
    const {backing_level_id,project_id} = this.props;
    localStorage.setItem('backLinkToDoante', `/projects/${project_id}/backers/${backing_level_id}`);
    History.push(link);
  }


  _renderBankOrStripe(initialValues) {

    const {showpass,errorCVC} = this.state;
    if (this.props.typePay === 'stripe') {
      return(
        <div className="form-area">
          <p className="page-tab-label">クレジットカード</p>
          <div className="form-box">
              <div className="form-field">
                  <p className="form-field-name">カード番号</p>
                  <div className="form-field-content">
                    <p>
                      {initialValues.number_card}
                    </p>
                  </div>
              </div>
              <div className="form-field">
                  <p className="form-field-name">有効期限</p>
                  <div className="form-field-content">
                    <p>
                      <span>{initialValues.exp_month}</span>/ <span>{initialValues.exp_year}</span>
                    </p>
                  </div>
              </div>


              <div className="form-field">
                  <p className="form-field-name form-field-name-long">セキュリティーコード<span>必須</span></p>

                  <div id="errorCVC" className="form-field-content cvc-input-stripe">
                    {
                      initialValues.cvc ?
                      <p>
                        <span>{initialValues.cvc}</span>
                      </p>
                      : <div className="" style={{width:'100%'}}>
                          <input
                            name="cvc"
                            type={ showpass ? 'text' : 'password' }
                            placeholder="（例）123"
                            style={{width:'100%'}}
                            value={this.state.valueCVC}
                            onChange={(e)=>this.valueCVC(e)}
                          />

                        <div>
                          <CvcPop></CvcPop>
                          <span style={{color:'red',position:'absolute'}} className="text-danger">{errorCVC}</span>
                          {
                            errorCVC !== ''?
                            <span style={{color:'red',position:'absolute'}} className="text-danger">{errorCVC}</span> : null
                          }

                        </div>

                        </div>

                    }

                    <span className='show-pass' onClick={ (e) => this.showpass(e) }>

                      {
                        showpass ?
                        <i className="icss-anim icss-eye"></i> :
                        <i className="icss-anim icss-eye-slash"></i>
                      }

                    </span>



                  </div>

              </div>

              <p>
                クレジットカードの情報を変更したい場合は、<span style={{color:"blue",textDecoration: 'underline',cursor:'pointer'}} onClick={ () => this.gotoLink("/my-page/setting-user/credit-accounts") }>こちら</span>をクリックしてください。
              </p>

          </div>
        </div>
      )
    }else {
      return (
        <div className="form-area">
          <p className="page-tab-label">銀行口座情報</p>
          <div className="form-box">
              <div className="form-field">
                  <p className="form-field-name">銀行名</p>
                  <div className="form-field-content">
                    <p>
                      {initialValues.bank_name}
                    </p>
                  </div>
              </div>
              <div className="form-field">
                <p className="form-field-name">支店名</p>
                  <div className="form-field-content">
                    <p>
                      {initialValues.bank_branch}
                    </p>
                  </div>

              </div>

              <div className="form-field">
                <p className="form-field-name">口座種類</p>
                  <div className="form-field-content">
                    <p>
                      {initialValues.bank_type}
                    </p>
                  </div>

              </div>

              <div className="form-field">
                <p className="form-field-name">口座番号</p>
                  <div className="form-field-content">
                    <p>
                      {initialValues.bank_number}
                    </p>
                  </div>

              </div>


              <div className="form-field">
                <p className="form-field-name">口座カナ名義</p>
                  <div className="form-field-content">
                    <p>
                      {initialValues.bank_owner}
                    </p>
                  </div>

              </div>

              <p>
                銀行口座情報の情報を変更したい場合は、<span style={{color:"blue",textDecoration: 'underline',cursor:'pointer'}} onClick={ () => this.gotoLink("/my-page/setting-user/bank-accounts-project-success") }>こちら</span>をクリックしてください。
              </p>

          </div>
        </div>
      )
    }
  }

  _renderAddress(addressList) {

    for (var i = 0; i < addressList.length; i++) {
      if (addressList[i].chosen_default === "1") {
        return (
          <div className="form-area form-area-3">
              <p className="page-tab-label">お届け先情報</p>
              <div className="form-box">
                  <div className="form-field form-field-1">
                      <p className="form-field-name">名前</p>
                      <div className="form-field-content">
                        <p>
                          {addressList[i].name_return}
                        </p>
                      </div>
                  </div>
                  <div className="form-field form-field-2">
                      <p className="form-field-name">郵便番号</p>
                      <div className="form-field-content">
                        <p>
                          {addressList[i].postcode}
                        </p>
                      </div>
                  </div>

                  <div className="form-field form-field-4">
                      <p className="form-field-name">住所</p>
                      <div className="form-field-content">
                        <p>
                          {addressList[i].address_return}
                        </p>
                      </div>
                  </div>
                  <div className="form-field form-field-5">
                      <p className="form-field-name">電話番号</p>
                      <div className="form-field-content">
                        <p>
                          {addressList[i].phone}
                        </p>
                      </div>
                  </div>

                  <div className="form-field form-field-5">
                      <p className="form-field-name">メールアドレス</p>
                      <div className="form-field-content">
                        <p>
                          {addressList[i].email}
                        </p>
                      </div>
                  </div>


                  <span>お届け先情報を変更したい場合は、<Link to="/my-page/setting-user/address-return">こちら</Link>をクリックしてください。</span>

              </div>
          </div>
        )
      }
    }
  }

  render() {
    const {data,dataBacker,typePay} = this.props;

    return (
      <div>
        <p className="page-tab-head">
          {
            typePay === 'bank' ? 'お支払い情報を入力してください （銀行振込）' : 'お支払い情報を入力してください （クレジットカード）'
          }

        </p>
        <div className="page-tab-content">
            <div className="page-tab-form">

              {
                data ?
                <form>

                  {
                    this._renderBankOrStripe(data)
                  }

                  {
                    this._renderAddress(data.listAddress)
                  }

                  {
                    <ProjectReturn data={dataBacker}></ProjectReturn>
                  }


                  {/*<input className="btn-submit" type="submit" value="確認画面"/>*/}
                  <span style={{cursor:"pointer"}} onClick={()=>this.confirmToDonate()} className="btn-submit">
                    完了
                  </span>
                </form> : null
              }



            </div>
        </div>

      </div>
    );
  }
}


const mapStateToProps = (state) => {
    return {
      typePay: state.Donate.typePay
    }
}

// You have to connect() to any reducers that you wish to connect to yourself
export default connect(mapStateToProps, actions )(Form);
