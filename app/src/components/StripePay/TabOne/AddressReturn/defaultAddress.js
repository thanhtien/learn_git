/* @flow */

import React, { Component } from 'react';

export default class Default extends Component {



  render() {
    const {data} = this.props;

    return (
      <form>

        <div className="form-area form-area-3">
            <p className="page-tab-label">お届け先情報</p>
            <div className="form-box">
                <div className="form-field form-field-1">
                    <p className="form-field-name">名前</p>
                    <div className="form-field-content">
                      {
                        <p>
                          <span>{data.name_return}</span>
                        </p>
                      }

                    </div>
                </div>
                <div className="form-field form-field-2">
                    <p className="form-field-name">電話番号</p>
                    <div className="form-field-content">
                      {
                        <p>
                          <span>{data.phone}</span>
                        </p>
                      }

                    </div>
                </div>

                <div className="form-field form-field-4">
                    <p className="form-field-name">郵便番号</p>
                    <div className="form-field-content">

                      {
                        <p>
                          <span>{data.postcode}</span>
                        </p>
                      }

                    </div>
                </div>
                <div className="form-field form-field-5">
                    <p className="form-field-name">住所</p>
                    <div className="form-field-content">

                      {
                        <p>
                          <span>{data.address_return}</span>
                        </p>
                      }

                    </div>
                </div>

                <div className="form-field form-field-5">
                    <p className="form-field-name">メールアドレス</p>
                    <div className="form-field-content">

                      {
                        <p>
                          <span>{data.email}</span>
                        </p>
                      }

                    </div>
                </div>


            </div>
        </div>
      </form>

    );
  }
}
