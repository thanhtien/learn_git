/* @flow */

import React, { Component } from 'react';
import Rodal from 'rodal';

import Form from './form';

class Add extends Component {


  hideModal() {
    this.props.close();
  }

  render() {
    const { status } = this.props;

    return (
      <Rodal  measure="%" width={80} height={80} visible={status} onClose={this.hideModal.bind(this)}>
        <div style={{width:'100%' , overflow:'auto' , height:'100%' , 'paddingBottom':'10px'}} className="add-donate-address">
          <Form></Form>
        </div>

      </Rodal>
    );
  }
}


export default Add
