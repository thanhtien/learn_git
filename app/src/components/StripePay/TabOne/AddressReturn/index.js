/* @flow */
import React, { Component } from 'react';
import TableEdit from './tableEditAddress';
import { connect } from 'react-redux'
import Loading from '../../../common/loading';
import List from './list';
import Add from './add';
class AddressReturn extends Component {

  constructor(props) {
    super(props);
    this.state = {
      statusList:false,
      statusAdd:false,
    };

  }

  /**
   * [openList Open List Address]
   * @return {[type]} [Open List]
   */

  openList() {
    this.setState({
      statusList:true
    })
    document.body.style.overflow = "hidden";

    this.refs.childList.getWrappedInstance().closeForm()

  }
  /**
   * [closeList Close List Address]
   * @return {[type]} [description]
   */
  closeList() {
    this.setState({
      statusList:false
    })
    document.body.style.overflow = "auto";
  }

  /**
   * [openList Open Add Address]
   * @return {[type]} [Open List]
   */

  openAdd() {
    this.setState({
      statusAdd:true
    })
    document.body.style.overflow = "hidden";

  }
  /**
   * [closeList Close Add Address]
   * @return {[type]} [description]
   */
  closeAdd() {
    this.setState({
      statusAdd:false
    })
    document.body.style.overflow = "auto";

  }
  render() {
    const {profile} = this.props;
    if (!profile) {
      return <Loading></Loading>
    }

    return (
      <div className="page-tab-form ">
        {
          <TableEdit
            addressList = {profile.listAddress}
            openList={() => this.openList()} closeList={() => this.closeList()}
            openAdd={() => this.openAdd()} closeAdd={() => this.closeAdd()}
          />
        }


        <List
          ref={'childList'}
          data={profile.listAddress}
          open={() => this.openList()}
          close={() => this.closeList()}
          status={this.state.statusList}>
        </List>

        {/* List Address */}

        <Add
          open={() => this.openAdd()}
          close={() => this.closeAdd()}
          status={this.state.statusAdd}>
        </Add>
        {/* List Address */}
      </div>
    );
  }
}

const mapStateToProps = (state) => {
    return {
      profile:state.common.profile
    }
}
export default connect(mapStateToProps)(AddressReturn);
