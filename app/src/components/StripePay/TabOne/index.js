/* @flow */
import React, { Component } from 'react';
import ProjectReturn from './ProjectReturn';
import History from '../../../history.js';
import { connect } from 'react-redux';
import AddressReturn from './AddressReturn';

var CONFIG = require('../../../config/common');

if (process.env.NODE_ENV === "development") {
  CONFIG = CONFIG.CONFIG.DEV;
}else {
  CONFIG = CONFIG.CONFIG.PRODUCT;
}

class TabOne extends Component {

  constructor(props) {
    super(props);
    this.state = {
      typeTab:"stripe",
    };
  }

  //Go To Back
  goBack() {
    History.goBack();
  }

  ChangeStep(step){
    if (this.props.profile) {
      if (this.props.profile.listAddress.length === 0) {
        alert('お届け先情報を入力してください');
      }else {
        this.props.ChangeStep(step);
      }

    }


  }

  changeTabType(type) {
    this.setState({
      typeTab:type
    },function() {
      this.props.dispatch({
        type: 'CHANGE_TYPE_DONATE',
        payload: type
      })
    })

  }

  _renderButtonStripe(confirmCheckPay ) {
    return  (
      confirmCheckPay === ""  ?
      <button onClick={ () => this.ChangeStep(2) } className="btn-re">次へ≫</button>
      :
      <button onClick={ () => this.ChangeStep(3) } className="btn-re">次へ≫</button>
    )
  }
  _renderButtonBank(confirmBank) {

    return  (
      confirmBank === null ?
      <button onClick={ () => this.ChangeStep(2) } className="btn-re">次へ≫</button>
      :
      <button onClick={ () => this.ChangeStep(3) } className="btn-re">次へ≫</button>
    )
  }
  _renderButton(typeTab) {
    const {profileUser} = this.props;

    if (typeTab === 'stripe') {
      if (profileUser) {
        var confirmCheckPay = profileUser.number_card;
      }
      return this._renderButtonStripe(confirmCheckPay )

    }else if( typeTab === 'bank') {
      if (profileUser) {
        var confirmBank= profileUser.bank_number;
      }
      return this._renderButtonBank(confirmBank)
    }
  }

  //Render
  render() {
    const {typeTab} = this.state;

    const {data} = this.props;
    
    return (
      <div className="page-content active" id="tab1">
          <div className="wraper">
              <p className="back-link"><span style={{cursor:'pointer'}} onClick={()=>this.goBack()}>≪　前のページへ戻る</span></p>
              <div className="BackerAddForm">


                    <ProjectReturn data={data} ></ProjectReturn>
                    {/* Project Return */}

                    <div className="address_return">
                      <h1>支払方法を選択してください。</h1>
                      <div className={data.project_type === '1' ? 'tab-change border-none' : 'tab-change'} >
                        <span
                          onClick={ () => this.changeTabType('stripe') } className={ typeTab === 'stripe' ? 'tab-name active': 'tab-name' } >
                          <p>
                            <img className="img-ticket" src={CONFIG.MAIN_URL+"/img/project-detail/ico-checked.svg"} alt="backer" />
                          </p>
                          クレジットカード
                        </span>

                        {
                          data.project_type !== '1' ?
                          <span onClick={ () => this.changeTabType('bank') } className={ typeTab === 'bank' ? 'tab-name active': 'tab-name' } >
                            <p>
                              <img className="img-ticket" src={CONFIG.MAIN_URL+"/img/project-detail/ico-checked.svg"} alt="backer" />
                            </p>
                            銀行振込</span> :null
                        }


                      </div>

                      <div className="clear-fix"></div>
                    </div>

                    <div className="cover-tab-step-one">

                      <div  className={ typeTab  === 'stripe' ?  'pannel-white fix-margin animated fadeIn' : 'pannel-white fix-margin hidden-div'}>
                        <h4 className="pannel-title">支払方法：クレジットカード</h4>
                        <div className="brand-list">
                          <ul>
                              <li><span ><img src={CONFIG.MAIN_URL+"/img/project-detail/brand-1.png"} alt=""/></span></li>
                              <li><span ><img src={CONFIG.MAIN_URL+"/img/project-detail/brand-2.png"} alt=""/></span></li>
                              <li><span ><img src={CONFIG.MAIN_URL+"/img/project-detail/brand-3.png"} alt=""/></span></li>
                              <li><span ><img src={CONFIG.MAIN_URL+"/img/project-detail/brand-5.png"} alt=""/></span></li>

                          </ul>
                        </div>
                      </div>

                      <div className={ typeTab  === 'bank' ?  'pannel-white fix-margin animated fadeIn' : 'pannel-white fix-margin hidden-div'}>
                        <p className="set-width-b">
                          銀行振り込みの方法を選択する場合は、以下のお届け先情報をご確認してください。お支払いは、48時間以内に選択したリターン品の該当の金額を以下の金融機関に振込を行ってください。振り込みが完了しましたら、<a href="mailto:info@kakuseida.com">info@kakuseida.com</a>にご連絡してください。システムの管理者が確認次第、ご支援が有効となります。<br/><br/>振込先は以下の通りです。<br/><b>銀行名：</b>西武信用金庫<br/><b>支店：</b>飯田橋支店<br/><b>店番：</b>166<br/><b>口座番号：</b>0009657<br/><b>名前：</b>ｶ)ｶｸｾｲ
                        </p>






                      </div>

                      <div className="address_return_module">
                        <AddressReturn></AddressReturn>
                      </div>
                    </div>

                    {
                      this._renderButton(typeTab)
                    }





              </div>



          </div>
      </div>
    );
  }
}




const mapStateToProps = (state) => {
  return {
    typePay: state.Donate.typePay,
    profile:state.common.profile
  }
}
export default connect(mapStateToProps)(TabOne);
