/* @flow */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actions from '../../../actions/editSystem';
import Loading from '../../common/loading';
import { Field, reduxForm , change } from 'redux-form';
import renderFieldInput from '../../common/renderFieldInput';

import validate from './validate';

import scrollToFirstError from './scrollToFirstError';


import postal_code  from 'japan-postal-code';

 class Form extends Component {


 constructor(props) {
   super(props);
   this.state = {
     loadingGoogle:false
   };

 }

  /**
   * [onChangePostCode For Generate Address]
   * @param  {[type]} code [post code]
   * @return {[type]}      [address google]
   */
   onChangePostCode(code){

    var addressReturn = '';
    postal_code.get(code, (address) => {

       addressReturn = address.prefecture+" "+address.city+" "+address.area+" "+address.street;

        if (addressReturn) {
          this.props.dispatch(change('donation-token', 'address_return', addressReturn));
        }else {
          this.props.dispatch(change('donation-token', 'address_return', ''));

        }

    });



   }

 /**
  * [handleFormSubmit Submit Form Create Project]
  * @param  {[type]} data [Data TO Server]
  */

  handleFormSubmit(data) {
    this.props.SettingBankDonateFirstTime(data , this.props.ChangeStep);
  }
 /**
  * [render description]
  * @return {[type]} [description]
  */
  render() {

    const {handleSubmit ,initialValues} = this.props;


    if (this.props.loading) {
      return(
        <Loading></Loading>
      )
    }

    

    return (
      <div>
        {
          initialValues ?
          <form onSubmit={handleSubmit(this.handleFormSubmit.bind(this))}>
              <div className="form-area">
                  <p className="page-tab-label">行口座情報</p>
                  <div className="form-box">
                    <div className="form-field">
                        <p className="form-field-name">銀行名<span>必須</span></p>
                        <div className="form-field-content">
                          {
                            initialValues.bank_name ?
                            <p>
                              {initialValues.bank_name}
                            </p>:
                            <Field
                              name="bank_name"
                              component={renderFieldInput}
                              type="text"
                              placeholder="銀行名"
                            />
                          }
                        </div>
                    </div>

                    <div className="form-field">
                        <p className="form-field-name">支店名<span>必須</span></p>
                        <div className="form-field-content">
                          {
                            initialValues.bank_branch ?
                            <p>
                              {initialValues.bank_branch}
                            </p>:
                            <Field
                              name="bank_branch"
                              component={renderFieldInput}
                              type="text"
                              placeholder="支店名"
                            />
                          }
                        </div>
                    </div>


                    <div className="form-field">
                        <p className="form-field-name">口座種類<span>必須</span></p>
                        <div className="form-field-content">
                          {
                            initialValues.bank_type ?
                            <p>
                              {initialValues.bank_type}
                            </p>:
                            <Field
                              name="bank_type"
                              component={renderFieldInput}
                              type="text"
                              placeholder="口座種類"
                            />
                          }
                        </div>
                    </div>


                    <div className="form-field">
                        <p className="form-field-name">口座番号<span>必須</span></p>
                        <div className="form-field-content">
                          {
                            initialValues.bank_number ?
                            <p>
                              {initialValues.bank_number}
                            </p>:
                            <Field
                              name="bank_number"
                              component={renderFieldInput}
                              type="text"
                              placeholder="口座番号"
                            />
                          }
                        </div>
                    </div>


                    <div className="form-field">
                        <p className="form-field-name">口座カナ名義<span>必須</span></p>
                        <div className="form-field-content">
                          {
                            initialValues.bank_owner ?
                            <p>
                              {initialValues.bank_owner}
                            </p>:
                            <Field
                              name="bank_owner"
                              component={renderFieldInput}
                              type="text"
                              placeholder="口座カナ名義"
                            />
                          }
                        </div>
                    </div>


                  </div>
              </div>



              <input className="btn-submit" type="submit" value="確認画面"/>

          </form>
          :null
        }
      </div>

    );
  }
}


Form = reduxForm({
  form: 'donation-token',
  destroyOnUnmount: false,
  onSubmitFail: (errors , dispatch) => scrollToFirstError(errors , dispatch),
  validate,
})(Form)
const mapStateToProps = (state) => {
    return {
      initialValues: state.common.profile,
      loading:state.common.loading,
    }
}

// You have to connect() to any reducers that you wish to connect to yourself
Form = connect(mapStateToProps, actions )(Form);



export default Form
