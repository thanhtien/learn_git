/* @flow */

import React, { Component } from 'react';
import { Link  } from 'react-router-dom';

import FormatFunc from '../../common/FormatFunc';


export default class NotificationMobile extends Component {

  readNotification(id){
    this.props.readNotification(id)
  }



  render() {
    const {notificationHeader,heightHeader,notification} = this.props;
    return (
      <div className="sp">
      {
        this.props.profile ?
        <ul style={{height: window.innerHeight - heightHeader,background:'#fff'}} className={notification?"menu sp active":"menu sp"} >
          <li className="after-login per-name">
            <div className="personal-panel personal-panel-width active">
              {
                notificationHeader.data.length > 0?
                <div className="dropdown-body">
                  {
                    /**
                     * [className NOTI Data]
                     * @type {String}
                     */

                    notificationHeader.data.map( (item , i)=> {
                      return (
                        <Link onClick={()=>this.readNotification(item.id)} style={item.status === '0' ? {background: 'floralwhite'} : null} key={i} to={`/${item.link}`}  className="notification">
                          <div className="notification-image">
                            <img src={item.thumbnail} alt="thum" />
                          </div>
                          <div className="notification-text">
                            {item.message}
                          </div>
                          <div className="notification-text" style={{minHeight:'auto'}}>
                            {
                              <FormatFunc date={item.created} ></FormatFunc>
                            }
                          </div>
                        </Link>
                      )
                    })

                  }

                  <Link className="to-all to-all-sp" to={'/notification/page=0'} >お知らせ一覧へ</Link>

                </div>:null
              }



              {
                notificationHeader.data.length === 0  ?
                <div className="notification no-noti">
                  <p>通知がまだありません。</p>
                </div>
                : null
              }

            </div>
          </li>
        </ul>:null
      }
      </div>
    );
  }
}
