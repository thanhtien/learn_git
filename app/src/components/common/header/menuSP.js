/* @flow */

import React, { Component } from 'react';
import { Link , NavLink } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {faDonate , faEdit , faCog , faSignOutAlt,faGem,faEnvelope} from '@fortawesome/free-solid-svg-icons';

export default class MenuSP extends Component {

  ToogleShowHideSubMenu(){
    this.props.ToogleShowHideSubMenu()
  }

  ToogleShowHideSubMenuKakuseida(){
    this.props.ToogleShowHideSubMenuKakuseida();
  }
  anchorToSlider(){
    this.props.anchorToSlider()
  }
  ToogleShowHideSubMenuCompany(){
    this.props.ToogleShowHideSubMenuCompany()
  }
  ToogleShowHideSubMenuFirstUser(){
    this.props.ToogleShowHideSubMenuFirstUser()
  }
  render() {
    const {profile,heightHeader,menuSP,SubMenuAuth,whatkaku,category,company,firstUser} = this.props;

    return (
      <div className="nav-sp">
      {
        profile ?
        <div className={menuSP?"menu active":"menu"} >
          <ul>
            <li className="after-login per-name">
              <Link to={"/my-page/project-supported/page=1"}>
                  <span className="per-avatar">
                    <img src={profile.profileImageURL} alt=""/>
                  </span>
                  <span><i>{profile.username}</i><i>マイページへ</i></span>
              </Link>
            </li>
            <li className="after-login">
              <Link to="/">
                <i className="icss-anim icss-home home-menu-mobi"></i>
                HOME
              </Link>
            </li>
            <li>
              <span
                className={SubMenuAuth ? "span-link active" : "span-link"}
                onClick={()=>this.ToogleShowHideSubMenu()} ><span className="sp">≫ </span>プロジェクトをさがす </span>

              <div className={
                  (SubMenuAuth ? "subMenu active  animated fadeIn" : "subMenu fadeOut animated")
                }>
                      <div className="col-l">
                          <ul className="menuSub menuSub-1">
                            {
                              document.getElementById('area2') ? <li><span className="link-face" onClick={()=>this.anchorToSlider()} style={{cursor:"pointer"}} >▶ 人気のプロジェクト</span></li> :null
                            }

                            <li><Link to={'/project-from-type/from-support-80/page=1'} > ▶ あと少しで目標100％になるプロジェクト</Link></li>
                            <li><Link to={'/project-from-type/from-your-expired/page=1'} >▶ 終了間近のプロジェクト</Link></li>

                          </ul>
                      </div>
                      <div className="col-r">
                          <ul className="menuSub menuSub-3">
                            {
                              category ? category.map( (item , i) => {
                                return(
                                  <li key={i}>


                                      <a className="nav-link" href={'/project-from-type/'+item.slug+'/page=1'}>{'ー '+item.name}</a>

                                  </li>
                                )
                              }) : null
                            }
                          </ul>
                      </div>
                  </div>
            </li>

            <li className="after-login al-support">
              <Link to="/my-page/project-supported/page=1"><FontAwesomeIcon icon={faDonate} />支援したプロジェクト</Link>
            </li>
            <li className="after-login al-support">


              <Link to="/my-page/project-supported-fan/page=1">  <FontAwesomeIcon icon={faGem} />定期支援中のプロジェクト</Link>
            </li>

            <li className="after-login al-post">
              <Link to="/my-page/post-project/page=1">
                <FontAwesomeIcon icon={faEdit} />
                自分のプロジェクト
              </Link>
            </li>

            <li className="after-login al-setting">
              <Link to="/my-page/setting-user/edit-profile">
                <FontAwesomeIcon icon={faCog} />
                設定
              </Link>
            </li>

            <li className="after-login al-setting">
              <Link to="/my-page/emailBox/page=0">
                <FontAwesomeIcon icon={faEnvelope} />
                メッセージ
              </Link>
            </li>



            <li>
                <span
                  onClick={()=>this.ToogleShowHideSubMenuKakuseida()}
                  className={whatkaku ? "link-here active" : "link-here"}
                >
                  <span className="sp">≫ </span>KAKUSEIDAとは？
                </span>
                  <div  className={whatkaku ? "subMenu active animated fadeIn" : "subMenu animated fadeOut"}>
                    <div className="col-r">
                        <ul className="menuSub menuSub-3">
                          <li><Link to={'/kakuseida/term'} >利用規約</Link></li>
                          <li><Link to={'/kakuseida/privacy-policy'} >プライバシーポリシー</Link></li>
                          <li><Link to={'/kakuseida/legal'} >特定商取引法に基づく表記</Link></li>
                          <li><Link to={'/kakuseida/Contact/'} >お問い合わせ</Link></li>
                        </ul>
                    </div>
                </div>

            </li>
            <li>

              <span className={company ? "span-link active" : "span-link"}
                onClick={()=>this.ToogleShowHideSubMenuCompany()} ><span className="sp">≫ </span>運営会社 </span>
                <div  className={company ? "subMenu active animated fadeIn" : "subMenu animated fadeOut"}>
                  <div className="col-r">
                      <ul className="menuSub menuSub-3">
                        <li><Link to={'/operating-company/company'} >会社概要</Link></li>
                        <li>
                            <a href="https://kakusei.com" target="_blank" rel="noopener noreferrer">
                              カクセイグループHP
                            </a>

                        </li>
                          <li><Link to={'/kakuseida/kakuseida-towa'} >KAKUSEIDAとは？</Link></li>
                          <li><Link to={'/kakuseida/guideline'} >企画者ガイドライン</Link></li>
                        <li><Link to={'/operating-company/access'} >アクセス</Link></li>
                      </ul>
                  </div>
              </div>

            </li>
            {/*<li>
              <span className={firstUser ? "span-link active" : "span-link"} onClick={()=>this.ToogleShowHideSubMenuFirstUser()}><span className="sp">≫ </span>プロジェクトを始まる</span>
              <div className={firstUser ? "subMenu active animated fadeIn" : "subMenu animated fadeOut"}>
                <div className="col-r">
                  <ul className="menuSub menuSub-3">
                    <li><Link to={'/first-user/first-user-project'} className="nav-link">プロジェクトをはじめる</Link></li>
                    <li><Link to={'/first-user/first-user-fan-club'} className="nav-link">ファンクラブをはじめる</Link></li>
                    <li><Link to={'/first-user/how-to-create-project'} className="nav-link">プロジェクトの作り方</Link></li>
                    <li><Link to={'/first-user/guideline'} className="nav-link">企画者ガイドライン</Link></li>
                    <li><Link to={'/first-user/faq'} className="nav-link">よくある質問</Link></li>
                    <li><Link to={'/first-user/first-user-project'} className="nav-link">お知らせ</Link></li>
                  </ul>
                </div>
              </div>
            </li>*/}
            <li className="after-login al-logout">

              <Link to={'/signout'} >
                <FontAwesomeIcon icon={faSignOutAlt} />
                ログアウト
              </Link>
            </li>
          </ul>
        </div> :
        <ul className={menuSP?"menu active":"menu"}>
          <li className="after-login">
            <Link to="/">
              <i className="icss-anim icss-home home-menu-mobi"></i>
              HOME
            </Link>
          </li>
          <li><Link to={'/signin'} ><span className="sp">≫ </span>ログイン </Link></li>
          <li>
            <span
              className={SubMenuAuth ? "span-link active" : "span-link"}
              onClick={()=>this.ToogleShowHideSubMenu()} ><span className="sp">≫ </span>プロジェクトをさがす </span>

              <div  className={SubMenuAuth ? "subMenu active animated fadeIn" : "subMenu animated fadeOut"}>
                      <div className="col-l">
                          <ul className="menuSub menuSub-1">
                            {
                              document.getElementById('area2') ? <li><span className="link-face" onClick={()=>this.anchorToSlider()} style={{cursor:"pointer"}} >▶ 人気のプロジェクト</span></li> :null
                            }
                            <li><Link to={'/project-enough-goal-amount-list/page=1'} > ▶ あと少しで目標100％になるプロジェクト</Link></li>
                            <li><Link to={'/project-enough-expired-list/page=1'} >▶ 終了間近のプロジェクト</Link></li>
                          </ul>
                      </div>
                      <div className="col-r">
                          <ul className="menuSub menuSub-3">
                            {
                              category ? category.map( (item , i) => {
                                return(
                                  <li key={i}>

                                    <a className="nav-link" href={'/categories/'+item.slug+'/page=1'}>{'ー '+item.name}</a>

                                  </li>
                                )
                              }) : null
                            }
                          </ul>
                      </div>
                  </div>


          </li>
          {/*<li>
            <span
              className={whatkaku ? "span-link active" : "span-link"}
              onClick={()=>this.ToogleShowHideSubMenuKakuseida()} ><em className="sp">≫ </em><img className="imgItem" src="/img/common/kakuseida_down.png" alt="KAKUSEIDA"/></span>
              <div  className={whatkaku ? "subMenu active animated fadeIn" : "subMenu animated fadeOut"}>
                <div className="col-r">
                    <ul className="menuSub menuSub-3">
                      <li><Link to={'/kakuseida/term'} >利用規約</Link></li>
                      <li><Link to={'/kakuseida/privacy-policy'} >プライバシーポリシー</Link></li>
                      <li><Link to={'/kakuseida/legal'} >特定商取引法に基づく表記</Link></li>
                      <li><Link to={'/kakuseida/guideline'} >企画者ガイドライン</Link></li>
                      <li><Link to={'/kakuseida/Contact/'} >お問い合わせ</Link></li>
                    </ul>
                </div>
            </div>
          </li>*/}
           <li>
            <span className={firstUser ? "span-link active" : "span-link"} onClick={()=>this.ToogleShowHideSubMenuFirstUser()}><span className="sp">≫ </span>プロジェクトを始まる</span>
            <div className={firstUser ? "subMenu active animated fadeIn" : "subMenu animated fadeOut"}>
              <div className="col-r">
                <ul className="menuSub menuSub-3">
                    <li><Link to={'/first-user/first-user-project'} className="nav-link">プロジェクトをはじめる</Link></li>
                    <li><Link to={'/first-user/first-user-fan-club'} className="nav-link">ファンクラブをはじめる</Link></li>
                    <li><Link to={'/first-user/how-to-create-project'} className="nav-link">プロジェクトの作り方</Link></li>
                    <li><Link to={'/first-user/guideline'} className="nav-link">企画者ガイドライン</Link></li>
                    {/*<li><Link to={'/first-user/faq'} className="nav-link">よくある質問</Link></li>*/}
                </ul>
              </div>
            </div>
          </li>
          <li>
            <span
              className={company ? "span-link active" : "span-link"}
              onClick={()=>this.ToogleShowHideSubMenuCompany()} ><span className="sp">≫ </span>運営会社 </span>
              <div  className={company ? "subMenu active animated fadeIn" : "subMenu animated fadeOut"}>
                <div className="col-r">
                    <ul className="menuSub menuSub-3">
                      <li><Link to={'/operating-company/company'} >会社概要</Link></li>
                      <li><Link to={'/kakuseida/kakuseida-towa'} >KAKUSEIDAとは？</Link></li>
                      {/*<li><Link to={'/operating-company/kakuseitowa'} >カクセイとは</Link></li>*/}
                      <li>
                          <a href="https://kakusei.com" target="_blank" rel="noopener noreferrer">
                            カクセイグループHP
                          </a>
                      </li>
                      {/*<li><Link to={'/operating-company/daihyou-aisatsu'} >代表者挨拶</Link></li>*/}
                      <li><Link to={'/operating-company/access'} >アクセス</Link></li>
                      <li><Link to={'/kakuseida/term'} >利用規約</Link></li>
                      <li><Link to={'/kakuseida/privacy-policy'} >プライバシーポリシー</Link></li>
                      <li><Link to={'/kakuseida/legal'} >特定商取引法に基づく表記</Link></li>
                      <li><Link to={'/kakuseida/guideline'} >企画者ガイドライン</Link></li>
                    </ul>
                </div>
            </div>
          </li>         
        </ul>
      }

      </div>

    );
  }
}
