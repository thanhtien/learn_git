/* @flow */

import React, { Component } from 'react';
import { Link  } from 'react-router-dom';


export default class MenuPc extends Component {

  getJsonFromUrl(url) {
    return url.split("/");
  }


  render() {
    const {categories} = this.props;
    var urlParese = this.getJsonFromUrl(window.location.href);

    return (
      <ul className="menu pc">
        {/*<li>*/}
        {/*  <Link className={ urlParese[3] === "" ? 'nav-link active' : 'nav-link' } to="/" >HOME</Link>*/}
        {/*</li>*/}
        <li>
            <div className={ urlParese[3] === "categories" ? 'top-link nav-item active' : 'top-link nav-item' }>プロジェクトをさがす
                <div className="subMenu">
                    <div className="sub-nav-wrap">
                        {/*<p className="titleSub">プロジェクトを探す</p>*/}
                        <ul className="menuSub">
                          {
                            categories ? categories.map( (item , i) => {
                              return(
                                <li key={i}>
                                  <a className="nav-link" href={'/project-from-type/'+item.slug+'/page=1'}>{'ー '+item.name}</a>
                                </li>
                              )
                            }) : null
                          }
                        </ul>
                    </div>
                    <div className="menuSub2">
                        <div className="sub-nav-wrap">
                            <ul>
                                {
                                  document.getElementById('area2') ? <li><span className="link-face" onClick={()=>this.anchorToSlider()} style={{cursor:"pointer"}} >人気のプロジェクト</span></li> :null
                                }
                                <li><Link to={'/project-from-type/from-support-80/page=1'} >あと少しで目標100％になるプロジェクト</Link></li>
                                <li><Link to={'/project-from-type/from-your-expired/page=1'} >終了間近のプロジェクト</Link></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </li>
        {/*<li>*/}
        {/*  <div*/}
        {/*    className={ urlParese[3] === "kakuseida" ? 'top-link active' : 'top-link' }    onClick={e => e.preventDefault()}>*/}
        {/*    KAKUSEIDAとは？*/}
        {/*    <div className="subMenu">*/}
        {/*      <ul className="menuSub just-link">*/}
        {/*        <li><Link to={'/kakuseida/term'} >利用規約</Link></li>*/}
        {/*        <li><Link to={'/kakuseida/privacy-policy'} >プライバシーポリシー</Link></li>*/}
        {/*        <li><Link to={'/kakuseida/legal'} >特定商取引法に基づく表記</Link></li>*/}
        {/*        <li><Link to={'/kakuseida/kakuseida-towa'} >KAKUSEIDAとは？</Link></li>*/}
        {/*        <li><Link to={'/kakuseida/guideline'} >企画者ガイドライン</Link></li>*/}
        {/*        <li><Link to={'/kakuseida/Contact/'} >お問い合わせ</Link></li>*/}
        {/*      </ul>*/}
        {/*    </div>*/}
        {/*  </div>*/}

        {/*</li>*/}
        <li>
            <div className={ urlParese[3] === "first-user"  ? 'top-link nav-item active' : 'top-link nav-item' }>プロジェクトを始める
                <div className="subMenu mySubMenu">
                    <div className="sub-nav-wrap l-tb-flex">
                        <ul className="menuSub">
                            <li><Link to={'/first-user/first-user-project'} className="nav-link">プロジェクトをはじめる</Link></li>
                            <li><Link to={'/first-user/first-user-fan-club'} className="nav-link">ファンクラブをはじめる</Link></li>
                            <li><Link to={'/first-user/how-to-create-project'} className="nav-link">プロジェクトの作り方</Link></li>
                            <li><Link to={'/first-user/guideline'} className="nav-link">企画者ガイドライン</Link></li>
                            <li><Link to={'/first-user/faq'} className="nav-link">よくある質問</Link></li>
                            {/*<li><a href="#" className="nav-link">お知らせ</a></li>*/}
                        </ul>
                    </div>
                </div>
            </div>
        </li>
        <li>
            <div className={ urlParese[3] === "operating-company" || urlParese[3] === "kakuseida"  ? 'top-link nav-item active' : 'top-link nav-item' }> 運営会社
                <div className="subMenu mySubMenu">
                    <div className="sub-nav-wrap l-tb-flex">
                        <div className={"col-sm-6"} >
                        <ul className="menuSub just-link ">
                            <li><Link to={'/operating-company/company'} >会社概要</Link></li>
                            <li><Link to={'/kakuseida/kakuseida-towa'} >KAKUSEIDAとは？</Link></li>
                            {/*<li><Link to={'/operating-company/kakuseitowa'} >カクセイとは</Link></li>*/}
                            <li>

                                <a href="https://kakusei.com" target="_blank" rel="noopener noreferrer">
                                    カクセイグループHP
                                </a>

                            </li>
                            {/*<li><Link to={'/operating-company/daihyou-aisatsu'} >代表者挨拶</Link></li>*/}
                            {/*<li><Link to={'/kakuseida/guideline'} >企画者ガイドライン</Link></li>*/}
                            <li><Link to={'/operating-company/access'} >アクセス</Link></li>
                        </ul>
                        </div>
                        <div className={"col-sm-6"} >
                            <ul className="menuSub just-link">
                                <li><Link to={'/kakuseida/term'} >利用規約</Link></li>
                                <li><Link to={'/kakuseida/privacy-policy'} >プライバシーポリシー</Link></li>
                                <li><Link to={'/kakuseida/legal'} >特定商取引法に基づく表記</Link></li>                                
                                <li><Link to={'/kakuseida/Contact/'} >お問い合わせ</Link></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </li>
      </ul>
    );
  }
}
