/* @flow */

import React, { Component } from 'react';
import { Link  } from 'react-router-dom';
import Scrollbar from 'react-scrollbars-custom';
import Loading from '../loadingScroll';
import FormatFunc from '../../common/FormatFunc';

export default class NotificationDesktop extends Component {

  readNotification(id){
    this.props.readNotification(id)
  }



  _renderNo(){
    const {notificationHeader} = this.props;
    if (notificationHeader) {
      return (
        <div className="dropdown-body">
            {
              notificationHeader.data.length > 0?
              <div>
                <Scrollbar style={ {width: '100%', height: '300px'} }>

                  {
                    /**
                     * [className NOTI Data]
                     * @type {String}
                     */

                    notificationHeader.data.map( (item , i)=> {

                      return (
                        <Link onClick={()=>this.readNotification(item.id)} style={item.status === '0' ? {background: 'floralwhite'} : null} key={i} to={`/${item.link}`}  className="notification">
                          <div className="notification-image">
                            <img src={item.thumbnail} alt="thum" />
                          </div>
                          <div className="notification-text">
                             {item.message}
                          </div>
                          <div style={{minHeight:'auto'}} className="notification-text">
                            {

                              <FormatFunc date={item.created}></FormatFunc>


                            }

                          </div>
                        </Link>
                      )
                    })
                  }

                </Scrollbar>

                <Link className="to-all" to={'/notification/page=0'} >お知らせ一覧へ</Link>

              </div>:null
            }



            {
              notificationHeader.data.length === 0  ?
              <div className="notification">
                <p>通知がまだありません。</p>
              </div>
              : null
            }

        </div>
      )
    }else {
      return (
        <div className="dropdown-body">
          <Loading></Loading>
        </div>
      )
    }
  }

  render() {
    const {loadingForHeader} = this.props;

    return (
      <div>
        <ul>
          <li className="name-row"></li>
        </ul>
        {
          loadingForHeader === true  ?
          <div className="dropdown-body">
            <Loading></Loading>
          </div> :
          this._renderNo()

        }




      </div>
    );
  }
}
