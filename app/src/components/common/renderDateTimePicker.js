import React, { Component } from 'react';

import DateTimePicker from 'react-widgets/lib/DateTimePicker';
import momentLocalizer from "react-widgets-moment";
import moment from 'moment';
momentLocalizer(moment)


export default class renderDateTimePicker extends Component {
  constructor(props) {
    super(props)
    this.state = {
      stateDate:false,
      toogle:false
    }
  }
  _OpenDate(event){

    this.setState({
      toogle:!this.state.toogle
    },function () {
      if (this.state.toogle) {
        this.setState({
          stateDate:'date'
        })
      }else {
        this.setState({
          stateDate:false
        })
      }
    })
  }
  _renderFormatFooter(date) {
    var month = Number(date.getMonth()) + 1 ;
    return (
      '今日は:' + date.getFullYear()+"/"+month.toString()+"/"+date.getDate()
    )
  }
  _renderFormatHeader(date) {
    var month = Number(date.getMonth()) + 1 ;
    if (month < 10) { month = '0' + month; }
    return (
      '登録日は:' + date.getFullYear()+"/"+month.toString()+"/"+date.getDate()
    )
  }

  render() {
    const { input: { onChange, value , name } , disable ,  placeholder , meta: { touched, error  } , previewYesOrNo } = this.props;
    
    return (
      <div name={`position-${name}`} style={{position:'relative'}}>
        <div className="w100pab" onClick= {(event)=>this._OpenDate(event)}></div>
        <DateTimePicker
          disabled={disable}
          name={name}
          open={this.state.stateDate}
          onChange={onChange}
          format="YYYY/M/D"
          autoFocus={false}
          min={new Date()}
          time={false}
          placeholder={placeholder}
          value={!value ? null : new Date(value)}
          headerFormat={date => this._renderFormatHeader(date)}
          footerFormat={date => this._renderFormatFooter(date)}
          dayFormat={day =>
            ['🎉', 'M', 'T','W','Th', 'F', '🎉'][day.getDay()]}
          onToggle={(event)=>this._OpenDate(event)}
        />

        {
          touched && error && <span style={{color:'red'}} className="text-danger">{error}</span>
        }
        {
          touched === false && previewYesOrNo && error && <span style={{color:'red'}} className="text-danger">{error}</span>
        }

      </div>
    );
  }
}
