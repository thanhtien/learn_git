import React from 'react'
const renderFieldTextArea = ({ input,placeholder,type,maxlength ,disable, meta: { touched, error } }) => (

  <div className="just-contact" name={`position-${input.name}`}>
    <textarea disabled={disable} maxLength={maxlength} style={{width:"100%",resize:'none'}} {...input} placeholder={placeholder} >
    </textarea>
    {touched && error && <span style={{color:'red'}} className="text-danger">{error}</span>}
  </div>

);
export default renderFieldTextArea;
