import React, { Component } from 'react';

import DateTimePicker from 'react-widgets/lib/DateTimePicker';
import momentLocalizer from "react-widgets-moment";
import moment from 'moment';
momentLocalizer(moment)


export default class renderDateTimePicker extends Component {
  constructor(props) {
    super(props)
    this.state = {
      stateDate:false,
      toogle:false
    }
  }
  _OpenDate(){
    this.setState({
      toogle:!this.state.toogle
    },function () {
      if (this.state.toogle) {
        this.setState({
          stateDate:'date'
        })
      }else {
        this.setState({
          stateDate:false
        })
      }
    })
  }
  _renderFormatFooter(date) {
    var month = Number(date.getMonth()) + 1 ;
    return (
      '今日は:' + date.getFullYear()+"/"+month.toString()+"/"+date.getDate()
    )
  }
  _renderFormatHeader(date) {
    var month = Number(date.getMonth()) + 1 ;
    if (month < 10) { month = '0' + month; }
    return (
      '登録日は:' + date.getFullYear()+"/"+month.toString()+"/"+date.getDate()
    )
  }
  render() {
    const { input: { onChange, value , name } , placeholder  , valueRender , indexRender  } = this.props;

    return (
      <div style={{position:'relative'}}>
        <div className="w100pab" onClick= {(event)=>this._OpenDate(event)}></div>

        {
          valueRender ?

          <DateTimePicker

            name={name}
            open={this.state.stateDate}
            onChange={onChange}
            format="YYYY/M/D"
            autoFocus={false}
            time={false}
            min={new Date()}
            placeholder={placeholder}
            value={!value ? null : new Date(value)}
            headerFormat={date => this._renderFormatHeader(date)}
            footerFormat={date => this._renderFormatFooter(date)}
            dayFormat={day =>
              ['🎉', 'M', 'T','W','Th', 'F', '🎉'][day.getDay()]}
            onToggle={()=>this._OpenDate()}
            disabled={valueRender.get(indexRender).un_schedule === true ? true : false}
          /> :
          null
        }
      <span className="warning">
        「お届け予定」
      </span>




      </div>
    );
  }
}
