/* @flow */

import React, { Component } from 'react';
import EditorController from "./EditorController";
export default class EditorFieldComponent extends Component {
  clearForm(value) {
    this.editor.clearForm(value);
  }
  componentDidMount() {
    document.body.addEventListener('click', (e) => this.bodyClickHighLight(e));
  }
  /**
   * [bodyClickHighLight HigtLight Image]
   * @param  {[type]} e [native event]
   * @return {[type]}   [css]
   */
  bodyClickHighLight(e){

      // .style.border = 'none';
    if (e.path[1].getAttribute('class') === 'rdw-image-imagewrapper') {
      var image = document.querySelectorAll('.rdw-image-imagewrapper img');

      for (var i = 0; i < image.length; i++) {
        image[i].style.border = 'none';
      }

      e.target.style.border = '2px solid #ffc95c';
    }
  }
  render() {
    const { placeholder, DefaultProjectContent ,input: { onChange, value,name }, disabled, id , disable } = this.props;

    return (
      <EditorController
        ref={instance => { this.editor = instance; }}
        DefaultProjectContent={DefaultProjectContent}
        id={id}
        disable={disable}
        name={name}
        disabled={disabled}

        placeholder={placeholder}
        onChange={onChange}
        value={value}
      />
    );
  }
}
