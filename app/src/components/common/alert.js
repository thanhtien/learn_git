/* @flow */
import React, { Component } from 'react';
import NotificationSystem from 'react-notification-system';

export default class AlertSucess extends Component {
  componentDidMount() {
    this._notificationSystem = this.refs.notificationSystem;
    this._addNotification();
  }
  _addNotification() {
    this._notificationSystem.addNotification({
      message: 'Notification message',
      level: 'success',
      autoDismiss:123123123123123
    });
  }
  render() {
    return (
      <div>
        <NotificationSystem style={{zIndex:123123123,position: 'relative'}} ref="notificationSystem" />
      </div>
    );
  }
}
