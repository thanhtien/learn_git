/*global FB*/

import React, { Component } from 'react';

import { Link } from 'react-router-dom';

var CONFIG = require('../../config/common');

if (process.env.NODE_ENV === "development") {
  CONFIG = CONFIG.CONFIG.DEV;
}else {
  CONFIG = CONFIG.CONFIG.PRODUCT;
}

class Footer extends Component {



  componentDidMount() {

    window.fbAsyncInit = function() {
      FB.init({
        appId      : CONFIG.FB_KEY,
        xfbml      : true,
        version    : 'v2.1'
      });
    };


    (function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      js = d.createElement(s); js.id = id;
      js.src = "https://connect.facebook.net/ja_JP/sdk.js#xfbml=1&version=v3.0";
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));


     var event = document.createEvent('Event');

     event.initEvent('fb_init', true, true);

     document.addEventListener('fb_init', function (e) {
       if (window.FB) {
         FB.XFBML.parse()
       }
     }, false);

     document.dispatchEvent(event);



     window.twttr = (function(d, s, id) {
     var js, fjs = d.getElementsByTagName(s)[0],  t = window.twttr || {};
     if (d.getElementById(id)) return t;
     js = d.createElement(s); js.id = id;
     js.src = "https://platform.twitter.com/widgets.js";
     fjs.parentNode.insertBefore(js, fjs);
     t._e = []; t.ready = function(f) {
       t._e.push(f);
     };
     return t;
     }(document, "script", "twitter-wjs"));

    {/*if (window.twttr.widgets) {
      window.twttr.widgets.createFollowButton(
        'kakuseitokyo',
        document.getElementById('container'),
        {
          size: 'small',
          showScreenName: "false",
          showCount: false,
          "lang":"ja"
        }
      );
    }*/}

  }

  render() {
    const {data} = this.props;
    return (
      <div id="footer">
          <div className="blkAds">
              <div className="wrap-content">
                  <div className="l-grid-flex-md">
                      <div className="l-grid-list-item">プラットフォーム手数料０%のクラウドファンディング<br/>（サポートオプションご利用の場合を除く）</div>
                      <div className="l-grid-list-item">
                          <a href={'/signIn'} className={'btn-square-soft'}>今すぐ始める</a>
                      </div>
                  </div>
              </div>
          </div>
          <div className="wrap-content">
              <div className="l-grid-flex l-grid-nav">
                  <div className="col-6 nav-block-md-3 l-grid-navi">
                      <div className="footer-nav-block">
                          <p className="footer-nav-title">さがす</p>
                          <ul className="footer-nav-list">
                              <li className="footer-nav-item"><Link to={'/project-from-type/from-your-access/page=1'} className="footer-nav-link">最近見たプロジェクト</Link></li>
                              <li className="footer-nav-item"><Link to={'/project-from-type/from-support-80/page=1'} className="footer-nav-link">あとひと押しのプロジェクト</Link></li>
                              <li className="footer-nav-item"><Link to={'/project-from-type/from-news-project/page=1'} className="footer-nav-link">新着のプロジェクト</Link></li>
                              <li className="footer-nav-item"><Link to={'/project-from-type/from-favorite/page=1'} className="footer-nav-link">注目のプロジェクト</Link></li>
                              <li className="footer-nav-item"><Link to={'/project-from-type/from-support-90/page=1'} className="footer-nav-link">終了間近のプロジェクト</Link></li>
                              <li className="footer-nav-item"><Link to={'/project-from-type/from-your-access/page=1'} className="footer-nav-link">ファンクラブ</Link></li>
                              <li className="footer-nav-item"><Link to={'/project-from-type/from-your-access/page=1'} className="footer-nav-link">過去のプロジェクト</Link></li>
                          </ul>
                      </div>
                  </div>
                  <div className="col-6 nav-block-md-2 l-grid-navi">
                      <div className="footer-nav-block">
                          <p className="footer-nav-title">はじめる</p>
                          <ul className="footer-nav-list">
                              <li className="footer-nav-item"><Link to={'/first-user/first-user-project'} className="footer-nav-link">プロジェクトをはじめる</Link></li>
                              <li className="footer-nav-item"><Link to={'/first-user/first-user-fan-club'} className="footer-nav-link">ファンクラブをはじめる</Link></li>
                              <li className="footer-nav-item"><Link to={'/first-user/how-to-create-project'} className="footer-nav-link">プロジェクトの作り方</Link></li>
                              <li className="footer-nav-item"><Link to={'/first-user/guideline'} className="footer-nav-link">企画者ガイドライン</Link></li>
                              {/*<li className="footer-nav-item"><Link to={'/first-user/faq'} className="footer-nav-link">よくある質問</Link></li>*/}
                              {/*<li className="footer-nav-item"><a href="#" className="footer-nav-link">お知らせ</a></li>*/}
                          </ul>
                      </div>
                  </div>
                  <div className="col-6 nav-block-md-3 l-grid-navi">
                      <div className="footer-nav-block">
                          <p className="footer-nav-title">カテゴリー</p>
                          <ul className="footer-nav-list">
                              {
                                data ? data.map( (item , i) => {
                                  return(
                                    <li className={"footer-nav-item"} key={i}><a className="footer-nav-link" href={'/project-from-type/'+item.slug+'/page=1'}> {item.name}</a></li>
                                  )
                                }) : null
                              }
                          </ul>
                      </div>
                  </div>
                  <div className="col-6 nav-block-md-4 l-grid-navi">
                      <div className="footer-nav-block">
                          <p className="footer-nav-title">運営情報</p>
                          <div className="nav-list-grid">
                            <ul className="footer-nav-list col-sm-6">
                                <li className="footer-nav-item"><Link to={'/operating-company/company'} className="footer-nav-link">会社概要</Link></li>
                                <li className="footer-nav-item"><Link to={'/kakuseida/kakuseida-towa'} className="footer-nav-link">KAKUSEIDAとは？</Link></li>
                                <li className="footer-nav-item"><a href="https://kakusei.com" target="_blank" rel="noopener noreferrer" className="footer-nav-link">カクセイグループHP</a> </li>
                                {/*<li className="footer-nav-item"><Link to={'/operating-company/daihyou-aisatsu'} className="footer-nav-link">代表者挨拶</Link></li>*/}
                                {/*<li className="footer-nav-item"><Link to={'/kakuseida/guideline'} className="footer-nav-link">企画者ガイドライン</Link></li>*/}
                                <li className="footer-nav-item"><Link to={'/operating-company/access'} className="footer-nav-link">アクセス</Link></li>
                            </ul>
                            <ul className="footer-nav-list col-sm-6">
                                <li className="footer-nav-item"><Link to={'/kakuseida/term'} className="footer-nav-link">利用規約</Link></li>
                                <li className="footer-nav-item"><Link to={'/kakuseida/privacy-policy'} className="footer-nav-link">プライバシーポリシー</Link></li>
                                <li className="footer-nav-item"><Link to={'/kakuseida/legal'} className="footer-nav-link">特定商取引法に基づく表記</Link></li>
                                <li className="footer-nav-item"><Link to={'/kakuseida/Contact/'} className="footer-nav-link">お問い合わせ</Link></li>
                            </ul>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
          {/* WRAPER */}
      </div>
    );
  }
}


export default Footer
