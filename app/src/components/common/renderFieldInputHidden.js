
import React, { Component } from 'react';


export default class HiddenForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      image:null
    };
  }

  render() {
    const { input } = this.props;

    return (
      <div style={{marginBottom: "20px",textAlign: "center"}}>

        {
          this.props.input.value ? <img src={this.props.input.value} alt="value"/> : false
        }
        <input {...input} type="hidden" />

      </div>
    );
  }
}
