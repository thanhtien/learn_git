
/* @flow */

import React, { Component } from 'react';
import { Field } from "redux-form";
import EditorFieldComponent from "./EditorFeildComponent";




export default class EditorField extends Component {
  clearForm(value) {
    this.foo.getRenderedComponent().clearForm(value)

  }
  render() {
    const {props} = this;

    return <Field {...props} name={props.name} disable={props.disable}  component={EditorFieldComponent}
      ref={node => this.foo = node}
        withRef />;
  }
}
