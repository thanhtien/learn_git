import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import { Link  } from 'react-router-dom';
import History from '../../history.js';
import { Field, reduxForm } from 'redux-form';
import * as actions from '../../actions/common';
import smoothScroll from './smoothScroll';
import NotificationDesktop from './header/NotificationDesktop';
import MenuPc from './header/MenuPc';
import MenuSP from './header/menuSP';
import Avatar from './header/avatar';
import NotificationMobile from './header/NotificationMobile';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {faHeart} from '@fortawesome/free-solid-svg-icons';
// import {ParseUrl} from './parseImage'
var CONFIG = require('../../config/common.js');
if (process.env.NODE_ENV === "development") {
  CONFIG = CONFIG.CONFIG.DEV;
} else {
  CONFIG = CONFIG.CONFIG.PRODUCT;
}

class Header extends PureComponent {

    constructor(props) {
      super(props);
      this.state = {
        avatar:false,
        menuSP:false,
        notification:false,
        company:false,
        wishList:false,
        heightHeader:null,
        SubMenuAuth:false,
        faded:false,
        whatkaku:false,
        firstUser:false,
        loadingForHeader:false
      };
    }

    componentDidMount() {
      if (this.header.offsetHeight) {
        this.setState({
          heightHeader:this.header.offsetHeight + 6
        })
      }

      //InitialData For Search
      if (this.props.initialValues) {
        this.props.InitialData(this.props.initialValues)
      }
      document.body.addEventListener('click', (e) => this.bodyClickHideAll(e));

    }

    componentWillUnmount(){
      this.setState = (state,callback)=>{
          return;
      };
    }
    bodyClickHideAll(e) {
      if (e.target.getAttribute('data-click') !== "img-click") {
        this.setState({
          avatar:false
        })
      }
      if (window.innerWidth > 768) {
        if (e.target.getAttribute('data-click') !== "img-bell") {
          this.setState({
            notification:false,
          })
        }

        if (e.target.getAttribute('data-click') !== "wishList") {
          this.setState({
            wishList:false,
          })
        }

      }




    }

    //Toogle Action
    ToogleAvatar(e) {
      this.setState({
        avatar:!this.state.avatar
      });
    }

    ToogleWishList(e){
      this.props.GetWishList(()=>this.loadingForHeader(),);
      this.setState({
        wishList:!this.state.wishList
      });
    }

    //Menu SP
    ToogleMenu() {
      if (this.state.menuSP === false) {
        document.body.className = 'block-body';
      }else {
        document.body.className = '';
      }
      this.setState({ menuSP:!this.state.menuSP });
    }

    //Toogle Menu SP
    ToogleShowHideSubMenu() {
      this.setState({
        SubMenuAuth:!this.state.SubMenuAuth
      },() => {
        if (this.state.SubMenuAuth === false) {
          setTimeout( () =>{
            this.setState({
              faded:true
            })
          }, 300);
        }else {
          this.setState({
            faded:false
          })
        }
      });
    }

    //Toogle Menu SP What Kakusieda
    ToogleShowHideSubMenuKakuseida() {
      this.setState({
        whatkaku:!this.state.whatkaku
      },() => {
        if (this.state.SubMenuAuth === false) {
          setTimeout( () =>{
            this.setState({
              faded:true
            })
          }, 300);
        }else {
          this.setState({
            faded:false
          })
        }
      });
    }

    loadingForHeader(){
      this.setState({
        loadingForHeader:!this.state.loadingForHeader
      })
    }

    //Call Api Delete Number
    toogleNoti() {


      this.props.DeleteNumberNotification(()=>this.loadingForHeader());

      if (window.innerWidth < 768) {
        if (this.state.notification === false) {
          document.body.className = 'block-body';
        }else {
          document.body.className = '';
        }
      }

      this.setState({
        notification:!this.state.notification,
        menuSP:false
      });
    }

    _renderNotication(notificationHeader) {

      return(
        <div className={this.state.notification ? 'personal-panel desktop-notification active fade-in personal-panel-width' : 'personal-panel fade-in personal-panel-width'}>

          <NotificationDesktop
            loadingForHeader={this.state.loadingForHeader}
            notificationHeader={notificationHeader} readNotification={(id)=>this.readNotification(id)} />

        </div>
      )
    }

    // Render Action avatar
    _renderAvatar(profile , notificationHeader , projectWishlist) {
      if (this.props.authenticated) {

        return (
          <div className="avatar pc">
            {
              profile ?
              <Link
                to={'/wishlist/page=0'}

                className="heart-list"
                title="お気に入り">


                  <FontAwesomeIcon data-click="wishList" icon={faHeart}></FontAwesomeIcon>



              </Link> : null
            }

            {
              profile !== null  ?

              <div>


                <span style={{cursor:'pointer'}} onClick={ (e) => this.ToogleAvatar(e) } className="avatar-img" ><img src={profile.profileImageURL} alt="ava" data-click="img-click"/></span>




                <button
                  onClick={ (e)=>this.toogleNoti(e) }
                  className="notification-list">
                  {
                    profile.number_notification !== 0 ?  <i className="number-no">{profile.number_notification}</i> : null
                  }
                  <span className="icss-stack"><i className="icss-bell" data-click="img-bell"></i></span>


                  {
                    /* Notifications List */
                    this._renderNotication(notificationHeader)
                    /* Notifications List */
                  }
                </button>




                  <div className={this.state.avatar ? 'personal-panel active fade-in' : 'personal-panel fade-in'}>
                    {
                      /**
                       * Avartar
                       * @type {Component}
                       */
                      <Avatar profile={profile} ></Avatar>
                    }

                  </div>
              </div>
              : null
            }


          </div>
        );
      } else {
        return (
          <Link to="/signin"  className="btn btn-large btn-blue btn-login">ログイン・新規登録</Link>
        );
      }
    }


    ToogleShowHideSubMenuCompany(){
      this.setState({
        company:!this.state.company
      })
    }

    ToogleShowHideSubMenuFirstUser(){
      this.setState({
        firstUser:!this.state.firstUser
      })
    }

    _renderMenuSP() {
      const {profile,data} = this.props;
      const {heightHeader,menuSP,SubMenuAuth,whatkaku,company,firstUser} = this.state;
      return(
        <MenuSP
          category={data}
          ToogleShowHideSubMenuKakuseida={ () => this.ToogleShowHideSubMenuKakuseida() }
          ToogleShowHideSubMenuCompany={ () => this.ToogleShowHideSubMenuCompany() }
          ToogleShowHideSubMenuFirstUser={ () => this.ToogleShowHideSubMenuFirstUser() }
          anchorToSlider={ () => this.anchorToSlider() }
          ToogleShowHideSubMenu={ () => this.ToogleShowHideSubMenu() }
          whatkaku={whatkaku}
          company={company}
          firstUser={firstUser}
          SubMenuAuth={SubMenuAuth}
          profile = {profile}
          menuSP={menuSP}
          heightHeader = {heightHeader}/>

      )
    }

    _renderNoticationSP(notificationHeader) {
      const {profile} = this.props;
      const {heightHeader,notification} = this.state;
      return(
        <NotificationMobile
          profile={profile}
          heightHeader={heightHeader}
          notification={notification}
          notificationHeader={notificationHeader}
          readNotification={(id)=>this.readNotification(id)} >
        </NotificationMobile>
      )
    }
    //Search Handeler
    handleFormSubmit(formProps) {
      this.props.searchForm(formProps)
    }

    updateOnchange(searchText) {
      this.props.updateSearch(searchText);
    }

    anchorToSlider() {
      if (window.location.pathname === '/') {
        smoothScroll.scrollTo('area2');
      }else {
        History.push('/');
        if (this.props.dataCheckTop) {
          setTimeout(function () {
            smoothScroll.scrollTo('area2');
          }, 1000);

        }
      }
    }

    readNotification(id) {
      this.props.readNotification(id)
    }


    render() {
      const {data , handleSubmit , profile , notificationHeader , wishlist } = this.props;



      return (
        <div id="header" className="l-tb-flex" ref={ (header) => this.header = header}>
          <div className="headContent wrap-content-inner">
            <div className="header-content-inner l-tb-flex">

              <a href="/" className="logo">
                <img src="/img/common/logo.png" alt="CLOUD FUNDING SYSTEM"/>
              </a>

              <div className="headInfo header-nav">

                {
                  /**
                   * [categories Menu For PC]
                   */
                  <MenuPc categories={data}/>
                }

                {
                  /*Menu SP*/
                  this._renderMenuSP()
                  /*Menu SP*/
                }

              </div>

              <div className={ profile ? '' : 'no-auth'}>

                {
                  //Notification SP
                  notificationHeader ?
                  this._renderNoticationSP(notificationHeader) :null
                  //Notification SP
                }
                <div className="pc cell-button l-tb-flex">
                  <Link to={"/project-from-type/from-news-project/page=1"} className="btn btn-large btn-blue">検索</Link>

                  {
                    /*Render Avartar*/
                    this._renderAvatar(profile,notificationHeader,wishlist)
                    /*Render Avartar*/
                  }
                </div>
                {
                  //Three Line Hamberger
                  <span className={this.state.menuSP ? "hamburger hamburger-nav open" : "hamburger hamburger-nav"} onClick={ () => this.ToogleMenu() }>
                    <span className="hamburger-bar hamburger-bar-1"></span>
                    <span className="hamburger-bar hamburger-bar-2"></span>
                    <span className="hamburger-bar hamburger-bar-3"></span>
                  </span>

                }

                {
                  this.props.profile ?

                  <div className="notification-sp">
                    <button onClick={ (e)=>this.toogleNoti(e) } className="notification-list">
                      {
                        this.props.profile.number_notification !== 0 ?
                        <i className="number-no">
                          {this.props.profile.number_notification}
                        </i> : null
                      }
                      <span className="icss-stack"><i className="icss-bell" data-click="img-bell"></i></span>



                    </button>
                  </div> : null
                }


              </div>
              {/*'HEAD INFOR'*/}
             </div>
          </div>
          {/*'HEADCONTENT'*/}

        </div>

      );
    }
}







Header = reduxForm({
  form: 'searchForm', // a unique identifier for this form
  destroyOnUnmount: false,
})(Header)

const mapStateToProps = (state) => {
    return {
      initialValues: state.common.searchText,
      authenticated: state.auth.authenticated,
      dataCheckTop: state.topPage.data,
      notificationHeader:state.notificationsList.notificationHeader,
      wishlist:state.wishlist.wishlistHeader
    }
}
// You have to connect() to any reducers that you wish to connect to yourself
Header = connect(mapStateToProps, actions)(Header);



export default Header
