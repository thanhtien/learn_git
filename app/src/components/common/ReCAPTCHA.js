/* @flow */

import React, { Component } from 'react';
import ReCAPTCHA from "react-google-recaptcha";
var CONFIG = require('../../config/common');

if (process.env.NODE_ENV === "development") {
  CONFIG = CONFIG.CONFIG.DEV;
}else {
  CONFIG = CONFIG.CONFIG.PRODUCT;
}
export default class Captcha extends Component {

  render() {
    const { meta: { touched, error  } , input: { onChange } } = this.props;
    return (
      <div style={{overflow:"hidden"}} className="input-render">
        <ReCAPTCHA
           sitekey={CONFIG.RECAP}
           onChange={onChange}
         />
        {touched && error && <span style={{color:'red'}} className="text-danger">{error}</span>}
      </div>
    );
  }
}
