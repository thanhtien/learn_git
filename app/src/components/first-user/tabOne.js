/* @flow */

import React, { Component } from 'react';
import Slider from "react-slick";
import { LazyLoadImage } from 'react-lazy-load-image-component';


export default class TabOne extends Component {

  render() {
    const settings = {
      customPaging: function(i) {
        return (
          <span className="dots-list-a"></span>
        );
      },
      dots: true,
      dotsClass: "dots-list dots-list-10",
      infinite: true,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1,
      autoplay: false,
      autoplaySpeed: 3000,
      arrows: true,
      responsive: [
      {
        breakpoint: 768,
        settings: {
          dots: true,
          customPaging: function(i) {
            return (
              <span>{''}</span>
            );
          }
        }
      },
      ]
    };
    return (
      <div>
        <div className="slides-wrapper">
            <ul className="slides">
              <Slider {...settings}>
                <li className="slides-li">
                    <p className="slides-img"><LazyLoadImage effect="blur" src="img/firstUser/slide_6.png" alt="slider-Error"/></p>
                    <p className="desc">画面右上の<span className="bold">「ログイン/新規会員登録」</span>のボタンを押下します。</p>
                </li>
                <li className="slides-li">
                    <p className="slides-img"><LazyLoadImage effect="blur" src="img/firstUser/slide_7.png" alt="slider-Error"/></p>
                    <p className="desc">新規会員登録の項目で、<span className="bold">①メールアドレスを入力して、②「確認メールを送信」</span>ボタンを押下します。</p>
                </li>

                <li className="slides-li">
                    <p className="slides-img"><LazyLoadImage effect="blur" src="img/firstUser/slide_8.png" alt="slider-Error"/></p>
                    <p className="desc">
                      メールをご確認くださいの画面に遷移します。KAKUSEIDAからのメールが登録したメールアドレスに届かない場合は、<br/> 迷惑メールボックスをご確認してください。それでもメールが届かない場合は、サポートまでご連絡ください。
                    </p>
                </li>

                <li className="slides-li">
                    <p className="slides-img"><LazyLoadImage effect="blur" src="img/firstUser/slide_9.png" alt="slider-Error"/></p>
                    <p className="desc">KAKUSEIDAからのメールを受信しましたら、本文中にある<span className="bold">「こちら」</span>をクリックしてKAKUSEIDAサイトに遷移します。</p>
                </li>

                <li className="slides-li">
                    <p className="slides-img"><LazyLoadImage effect="blur" src="img/firstUser/slide_10.png" alt="slider-Error"/></p>
                    <p className="desc">KAKUSEIDAのアカウント情報登録画面に遷移したら、ユーザー名、パスワードを入力し、<br/><span className="bold">利用規約に同意する</span>にチェックをいれたあと、<span className="bold">登録</span>ボタンを押下してください。</p>
                </li>

                <li className="slides-li">
                    <p className="slides-img"><LazyLoadImage effect="blur" src="img/firstUser/slide_11.png" alt="slider-Error"/></p>
                    <p className="desc">右上のアイコンをクリックして、<span className="bold">「設定」</span>を選択します。</p>
                </li>

                <li className="slides-li">
                    <p className="slides-img"><LazyLoadImage effect="blur" src="img/firstUser/slide_12.png" alt="slider-Error"/></p>
                    <p className="desc"><span className="bold">プロフィール編集</span>を行います。アイコンを登録したり、生年月日等を登録してください。<br/>生年月日はyyyy/mm/dd(1990年4月1日生まれでしたら、1990/04/01)で登録してください。</p>
                </li>

                <li className="slides-li">
                    <p className="slides-img"><LazyLoadImage effect="blur" src="img/firstUser/slide_13.png" alt="slider-Error"/></p>
                    <p className="desc"><span className="bold">お届け先情報</span>を登録します。こちらで登録した住所に返礼品が送付されます。<br/>（電子チケット等は、登録メールアドレスに送付されます。）</p>
                </li>

                <li className="slides-li">
                    <p className="slides-img"><LazyLoadImage effect="blur" src="img/firstUser/slide_14.png" alt="slider-Error"/></p>
                    <p className="desc">支援する際に利用する<span className="bold">クレジットカード情報</span>を登録します。</p>
                </li>

                <li className="slides-li">
                    <p className="slides-img"><LazyLoadImage effect="blur" src="img/firstUser/slide_15.png" alt="slider-Error"/></p>
                    <p className="desc"><span className="bold">銀行口座情報</span>を登録します。銀行口座情報は、プロジェクト企画者様の場合は集めた支援金の振込先として、<br/>支援者様の場合は支援プロジェクトが不成立の場合の返金に用います。</p>
                </li>

            </Slider>

            </ul>
        </div>

      </div>
    );
  }
}
