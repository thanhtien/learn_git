/* @flow */

import React, { Component } from 'react';
import TabOne from './tabOne';
import TabTwo from './tabTwo';

export default class MyComponent extends Component {

  constructor(props) {
    super(props);
    this.state = {
      tab:1,
    };
  }

  tagChange(tab) {
    this.setState({
      tab:tab
    })
  }

  componentDidMount() {
    document.title = "初めての方へ｜KAKUSEIDA";
  }


  render() {

    const {tab} = this.state;
    return (


        <div className="jumbotron">
            <h2 className="title">KAKUSEIDA<br className="sp"/>へようこそ</h2>
            <p className="subTitle">初めての方への案内</p>

            <div className="tab-list-wrapper">
                <ul className="tab-list">
                    <li  onClick={ () => this.tagChange(1) } className={ tab === 1 ? "active tab-list-li":"tab-list-li"}>
                      <button className="tab-btn" >ユーザー新規登録</button>
                    </li>
                    <li  onClick={ () => this.tagChange(2) } className={ tab === 2 ? "active tab-list-li":"tab-list-li"}><button className="tab-btn" >プロジェクト新規作成</button></li>
                </ul>
            </div>


            <div className={ tab === 1 ? "active animated fadeIn tab-firstUser":"tab-firstUser"} >
              {
                tab === 1 ? <TabOne></TabOne> : null
              }

            </div>

            <div className={ tab === 2 ? "active animated fadeIn tab-firstUser":"tab-firstUser"}>
              {
                tab === 2 ? <TabTwo></TabTwo> : null
              }

            </div>


        </div>



    );
  }
}
