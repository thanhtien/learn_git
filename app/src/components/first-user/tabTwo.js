/* @flow */

import React, { Component } from 'react';
import Slider from "react-slick";
import { LazyLoadImage } from 'react-lazy-load-image-component';


export default class TabTwo extends Component {

  render() {
    const settings = {
      customPaging: function(i) {
        return (
          <span className="dots-list-a"></span>
        );
      },
      dots: true,
      dotsClass: "dots-list dots-list-5",
      infinite: true,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1,
      autoplay: true,
      autoplaySpeed: 3000,
      arrows: true,
      responsive: [
      {
        breakpoint: 768,
        settings: {
          dots: true,
          customPaging: function(i) {
            return (
              <span>{''}</span>
            );
          }
        }
      },
      ]
    };
    return (
      <div>
        <div className="slides-wrapper">
            <ul className="slides">
              <Slider {...settings}>
                <li className="slides-li">
                    <p className="slides-img"><LazyLoadImage effect="blur" src="img/firstUser/slide_1.png" alt="slider-Error"/></p>
                    <p className="desc">プロフィールのタブから<span className="bold">自分のプロジェクト</span>を選択します。</p>
                </li>

                <li className="slides-li">
                    <p className="slides-img"><LazyLoadImage effect="blur" src="img/firstUser/slide_2.png" alt="slider-Error"/></p>
                    <p className="desc">プロフィール画面の自分のプロジェクトタブから、<span className="bold">プロジェクト新規作成</span>を押下します。</p>
                </li>

                <li className="slides-li">
                    <p className="slides-img"><LazyLoadImage effect="blur" src="img/firstUser/slide_3.png" alt="slider-Error"/></p>
                    <p className="desc"><span className="bold">プロジェクト詳細</span>タブでプロジェクトの詳細を作成していきます。</p>
                </li>

                <li className="slides-li">
                    <p className="slides-img"><LazyLoadImage effect="blur" src="img/firstUser/slide_4.png" alt="slider-Error"/></p>
                    <p className="desc"><span className="bold">リターン作成</span>タブを選択して、プロジェクトのリターンに関する詳細を作成します。</p>
                </li>

                <li className="slides-li">
                    <p className="slides-img"><LazyLoadImage effect="blur" src="img/firstUser/slide_5.png" alt="slider-Error"/></p>
                    <p className="desc"><span className="bold">保存</span>して管理者へプロジェクト公開の<span className="bold">申請</span>を行います。</p>
                </li>


            </Slider>

            </ul>
        </div>

      </div>
    );
  }
}
