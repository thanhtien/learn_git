import React, { Component } from 'react';
import Breadcrumbs from '../../Breadcrumbs';



class index extends Component {
    componentDidMount() {
        document.title = "カクセイとは｜KAKUSEIDA";

    }
    render() {
        const linkBreacrm = [
          { last: true, label: 'カクセイとは', link: null }
        ]
        return (
        <div className="kakuseitowa" >
			<div className="banner-css banner-kakuseitowa">
		        <div className="wraper">
		            <p className="title-white">
		              カクセイとは
		            </p>
		        </div>
	        </div>
	        <div className="wraper">
				<Breadcrumbs linkBreacrm={linkBreacrm}></Breadcrumbs>
				<p className="text">
					<strong>株式会社カクセイ（以下“カクセイ”）</strong>は石橋泰寛（共同創業者）と平山智浩（共同創業者/代表取締役）が2015年4月22日に創業しました。<br/>
					カクセイは“不動産”をすべての人々の生活・ビジネスにとって必要不可欠なプラットフォームととらえ、その価値の最大化を探求します。<br/>
					“不動産”はそれぞれが唯一無二のプラットフォームでありながら、そこに存在するコンテンツによって、その存在価値がいかようにも変化します。そして、その組み合わせのパターンはまさに無数です。
				</p>
				<p className="text">
					石橋（創業者）は言います、　<br/>
					「考えれば考えるだけそれだけより良いアイデアが生まれる。絶対に考えることを諦めず、考え続けなくてはいけない。」<br/>
					即ち、思考のパターンを考慮するとさらに途方もない数の解が生まれます。それを我々はスペース・イノベーションと呼びます。
				</p>
				<p className="text">
					存在価値は経済的価値だけではないです。街に調和し、そこに生きる人々と融合し、新たな価値を創造します。<br/>
					我々はテクノロジーによって、不動産を活性化させます。シェアリングエコノミーによる生活の多様化、人工知能やスマートコントラクト等の技術革新が我々を覚醒させました。我々にとっての“不動産”は土地、マンション、商業ビルなどとは限らないです。“不動産”は地球上から宇宙に広がりつつあります、いや、ヴァーチャル空間に広がる局面もあるでしょう。
				</p>
				<p className="text">
					まさにカクセイは“不動産”を覚醒させる。そこに息づく街と人々を覚醒させる。<br/>
					覚醒した世界、人類がまだ見たこともない世界を創造する。
				</p>
				<p className="text bold">スペース・イノベーション・カンパニー　カクセイです。</p>
	        </div>
      	</div>
        );
    }
}




export default index;
