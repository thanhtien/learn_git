import React, { Component } from 'react';
import '../../asset/style/FirsUserMore.css';
import History from "../../../history";


export default class FirstUserFanClub extends Component {
  constructor(props) {
    super(props);
    let pathName = this.props.location.pathname;
      this.isProject = pathName.includes('first-user-project');

  }
  componentDidMount() {
    document.title = "プロジェクトを始める｜KAKUSEIDA";
  }
  get3Row1 = () =>{
    return (
        <React.Fragment>
          <div className={'grid-box-card-item col-sm-6 col-md-4'} >
            <div className={'box-card'} >
              <div className={'box-card-icon'}>
                <img src={'/img/static/userMore/love.png'} />
              </div>
              <div className={'box-card-title'}>支援方法の選択</div>
              <div className={'box-card-content'}>
                <p>認定NPO法人など、税制優遇の対象となる団体は、寄附型のクラウドファンディングが利用可能です。支援者は「確定申告」を行うことで税制優遇（所得控除又は税額控除）が受けられます。</p>
              </div>
            </div>
          </div>
          <div className={'grid-box-card-item col-sm-6 col-md-4'} >
            <div className={'box-card'} >
              <div className={'box-card-icon'}>
                <img src={'/img/static/userMore/calculator.png'} />
              </div>
              <div className={'box-card-title'}>業界初の手数料0%</div>
              <div className={'box-card-content'}>
                <p>プロジェクトの掲載には一切費用がかかりません。最終的に達成した際の手数料も一切費用がかかりません。やりたいことを実現するため、より多くのお金を活動資金に充てることが可能です。</p>
              </div>
            </div>
          </div>
          <div className={'grid-box-card-item col-sm-6 col-md-4'} >
            <div className={'box-card'} >
              <div className={'box-card-icon'}>
                <img src={'/img/static/userMore/support.png'} />
              </div>
              <div className={'box-card-title'}>サポート</div>
              <div className={'box-card-content'}>
                <p>プロジェクトを行うにあたってどのようなことが必要なのか、またどのようにしたら目標額を達成できるのかサポートを受けながら行うことができます。</p>
              </div>
            </div>
          </div>
        </React.Fragment>
    );
  }
  get3Row2 = () =>{
    return (
        <React.Fragment>
          <div className={'grid-box-card-item col-sm-6 col-md-4'} >
            <div className={'box-card'} >
              <div className={'box-card-icon'}>
                <img src={'/img/static/userMore/monitor.png'} />
              </div>
              <div className={'box-card-title'}>簡単な操作</div>
              <div className={'box-card-content'}>
                <p>プロジェクト公開までの手順はとても簡単な操作で完成します。最短でプロジェクト作成後即日に公開可能です。</p>
              </div>
            </div>
          </div>
          <div className={'grid-box-card-item col-sm-6 col-md-4'} >
            <div className={'box-card'} >
              <div className={'box-card-icon'}>
                <img src={'/img/static/userMore/connection.png'} />
              </div>
              <div className={'box-card-title'}>安心・安全</div>
              <div className={'box-card-content'}>
                <p>申請していただいたプロジェクトについては、担当チームが弊社内で定めている基準に沿ってひとつひとつ丁寧に審査を行っております。</p>
              </div>
            </div>
          </div>
          <div className={'grid-box-card-item col-sm-6 col-md-4'} >
            <div className={'box-card'} >
              <div className={'box-card-icon'}>
                <img src={'/img/static/userMore/team.png'} />
              </div>
              <div className={'box-card-title'}>ファンクラブ</div>
              <div className={'box-card-content'}>
                <p>一度の支援で終わることなく毎月継続的に活動費の支援を募ることができます。</p>
              </div>
            </div>
          </div>
        </React.Fragment>
    );
  }
  get2Row1 = () =>{
        return (
            <React.Fragment>
                <div className={'l-grid-card-bor-item col-sm-6'}>
                    <div className="card-bor l-card-flex">
                        <div className={'card-bor-icon'}>
                            <img src={'/img/static/userMore/start-up.png'} />
                        </div>
                        <div className={'card-bor-num'}>1</div>
                        <div className={'card-bor-content'}>
                            <p>専用ページにてファンクラブの概要を書きます。</p>
                            <p>多くの人に支援して貰えるようにわかりやすく<br/>ファンクラブのイメージが伝わる写真なども用意しましょう。</p>
                        </div>
                    </div>
                </div>
                <div className={'l-grid-card-bor-item col-sm-6'} >
                    <div className="card-bor l-card-flex">
                        <div className={'card-bor-icon'}>
                            <img src={'/img/static/userMore/document.png'} />
                        </div>
                        <div className={'card-bor-num'}>2</div>
                        <div className={'card-bor-content'}>
                            <p> プロジェクトの申請をしたら審査を行います。</p>
                            <p>審査は１つずつ丁寧にスタッフがチェックを行っていきます。</p>
                        </div>
                    </div>
                </div>
            </React.Fragment>
        );
    }
  get2Row2 = () =>{
        return (
            <React.Fragment>
                <div className={'l-grid-card-bor-item col-sm-6'}>
                    <div className="card-bor l-card-flex">
                        <div className={'card-bor-icon'}>
                            <img src={'/img/static/userMore/website.png'} />
                        </div>
                        <div className={'card-bor-num'}>3</div>
                        <div className={'card-bor-content'}>
                            <p>審査が完了したらファンクラブページが公開されます。</p>
                            <p>SNSなど使い配信をして多くの人にファンクラブの内容を知って貰いましょう。</p>
                        </div>
                    </div>
                </div>
                {!this.isProject&&<div className={'l-grid-card-bor-item col-sm-6'}>
                    <div className="card-bor l-card-flex">
                        <div className={'card-bor-icon'}>
                            <img src={'/img/static/userMore/recommendation.png'} />
                        </div>
                        <div className={'card-bor-num'}>4</div>
                        <div className={'card-bor-content'}>
                            <p>ファンクラブは1人でも多くの人に長くファンでいてもらえるように、活動報告やファンとの交流を深めましょう。</p>
                        </div>
                    </div>
                </div>}
                {this.isProject&&<div className={'l-grid-card-bor-item col-sm-6'}>
                    <div className="card-bor l-card-flex">
                        <div className={'card-bor-icon'} >
                            <img src={'/img/static/userMore/goal.png'} />
                        </div>
                        <div className={'card-bor-num'}>4</div>
                        <div className={'card-bor-content'}>
                            <p>プロジェクトが成功したら支援してくれた方へリターンを送ります。また、支援してくれた方へ向けて活動報告も行いましょう。</p>
                        </div>
                    </div>
                </div>}
            </React.Fragment>
        );
    }
  get33Row1 = () =>{
        return (
            <React.Fragment>
                <div className={'l-grid-list-qa-item col-sm-6'}>
                    <div className="list-qa">
                      <div className={'list-qa-header'}>
                          1.ファンクラブの作成方法に関して
                      </div>
                      <div className={'list-qa-content'}>
                          <div className="list-qa-item">身に覚えのない「支援完了のお知らせ」メールが届いた場合について</div>
                          <div className="list-qa-item">寄付型クラウドファンディングについて</div>
                          <div className="list-qa-item">支援について</div>
                          <div className="list-qa-item">ファンクラブの会員費について</div>
                          <div className="list-qa-item">まとめて支払い</div>
                      </div>
                    </div>
                </div>
                <div className={'l-grid-list-qa-item col-sm-6'}>
                    <div className="list-qa">
                        <div className={'list-qa-header'} >
                            2.ファンクラブの申請方法に関して
                        </div>

                        <div className={'list-qa-content'} >
                            <div className="list-qa-item">身に覚えのない「支援完了のお知らせ」メールが届いた場合について</div>
                            <div className="list-qa-item">寄付型クラウドファンディングについて</div>
                            <div className="list-qa-item">支援について</div>
                            <div className="list-qa-item">ファンクラブの会員費について</div>
                            <div className="list-qa-item">まとめて支払い</div>
                        </div>
                    </div>
                </div>
            </React.Fragment>
        );
    }
  gotoLoginRight=()=>{
        let user_id = localStorage.getItem('NologIn');
        if(user_id==null){
            setTimeout(function () {
                History.push('/my-page/add-new-project/?is_project=0');
            }, 1000);
        }
        else{
            setTimeout(function () {
                History.push('/signin');
            }, 1000);
        }
  }
  gotoLogin=()=>{
        let user_id = localStorage.getItem('NologIn');
      if(user_id==null){
          setTimeout(function () {
              History.push('/my-page/add-new-project/?is_project=1');
          }, 1000);
      }
      else{
          setTimeout(function () {
              History.push('/signin');
          }, 1000);
      }
    }
  getTitleAfterHeader=()=>{
      return (<div className={'header-title'}>
          <p>カクセイだ！のクラウドファンディングの特徴は、プロジェクト支援成立時の成功報酬として発生する手数料が０%であるというところです。</p>
          <p>支援者様の投資金額がそのままプロジェクト立案者様の元に届けられるシステムとなっております。</p>
          <p>カクセイだ！は、頑張る人と応援したい人を繋げ、夢をカタチにするお手伝いをします。</p>
      </div>);

  }
  getFirstUserFanClub = () =>{
    return(
    <div id={"firsUserFanClub"} className={'wrap-main'}>
      <div className={this.isProject==true?'headerFirstProject':'headerFirst'}>
        <div className={'headerFirst2Button'}>
          <div className={'headerFirst2Buttonbtn1'} onClick={()=>{this.gotoLoginRight()}}>
            プロジェクトを作る
          </div>
          <div className={'headerFirst2Buttonbtn2'} onClick={()=>{this.gotoLogin()}}>
            ファンクラブを作る
          </div>
        </div>

      </div>
      <div className={'wrap-content-inner mt-banner'}>
        <div className={'firstMain'}>
            {this.isProject &&this.getTitleAfterHeader()}
            {!this.isProject && <div className={'header-title'} >
            <p>カクセイだ！のファンクラブの特徴は、募集期限が無く月額制で継続的に資金を調達できます。</p>
            <p>毎月提供できるファンクラブ得点を用意しファンとの交流を楽しみながら支援を募りましょう。</p>
          </div>}
          <div className={'boxKakuSeida'} >
            <div className={"imageBorder fieldset-wrapper"}>
              <img className={"fieldset-logo"} src={"/img/static/kakuseida.png"}/>
            </div>
            <div className={'txtBoxCenter txt-box-title'}>KAKUSEIDAの強み</div>
            <div className={'wrap-grid'}>
              <div className={'grid-box-card l-grid-flex'} >
                {this.get3Row1()}
                {this.get3Row2()}
              </div>
            </div>
          </div>



        </div>
      <div className={'box2FirsUser'} >

          <div className={'txtBoxCenter'}>プロジェクトの流れ</div>
          <div className={'l-grid-card-bor l-grid-flex'} >
              {this.get2Row1()}
              {this.get2Row2()}
          </div>
      </div>
          <div className={'box3FirsUser'} >
              <div className={'txtBoxCenter'}>よくある質問</div>
              <div className="wrap-grid-list-qa">
                  <div className={'l-grid-list-qa l-grid-flex'} >
                      {this.get33Row1()}
                  </div>
                  <div className="wrap-link">
                      <a className={'link-read-more'} href="#">他の質問を見る</a>
                  </div>
              </div>
          </div>
          <div className={'box4FirsUser'} >
              <div className={'headerFirst2ButtonEnd'} >
                  <div className={'headerFirst2Buttonbtn1'}  onClick={()=>{this.gotoLoginRight()}}>
                      プロジェクトを作る
                  </div>
                  <div className={'headerFirst2Buttonbtn2'}  onClick={()=>{this.gotoLogin()}} >
                      ファンクラブを作る
                  </div>
              </div>
          </div>
        </div>
    </div>);
  };

  render() {

    return (
        this.getFirstUserFanClub()
    )
  }
}

