import React, { Component } from 'react';
import '../../asset/style/AllGui.css';
import History from "../../../history";

export default class SubFAQ extends Component {
  constructor(props) {
    super(props);
    this.state ={'fqaShowTip':false}

  }


  showIt=(classShow)=>{
    this.setState({'fqaShowTip':!this.state.fqaShowTip});
  }
  GoNext = ()=>{
  };
  btn4Click(iBtn){
    if(iBtn==1){
      setTimeout(function () {
        History.push('/kakuseida/SubFAQBtn1/');
      },200);

      ///kakuseida/SubFAQBtn1/
    }

  }
  getFaq = () =>{
    return(
    <div>
      <div className={'makeProContent'}>
        <div className={'faqSearch'}>
          <div className="divSearchLeft">
            <form className="faq-form-search">

              <input  className="btnSearchLeft" type="button" onClick={()=>{this.searchKeyWord()}}  name="" value="" />
              <input type={"input"}    className={"searchLeft"} placeholder={"プロジェクトを探す"} />

            </form>
          </div>
        </div>
        {/*4 button*/}
        <div className={'faq2Bt'}>
          <div className={'faq2Bt1'} onClick={()=>this.btn4Click(1)}>
            <div className={'faq2BtInner'}>
            プロジェクト製作者向け
            </div>
          </div>
          <div className={'faq2Bt1'}>
            <div className={'faq2BtInner'}>
            支援者向け
            </div>
          </div>

        </div>
        <div className={'faq2Bt'}>
          <div className={'faq2Bt1'}>
            <div className={'faq2BtInner'}>
              ファンクラブ向け
            </div>
          </div>
          <div className={'faq2Bt1'}>
            <div className={'faq2BtInner'}>
              <p> プロジェクト製作者・支援者 </p>
              <p>共通</p>
            </div>
          </div>

        </div>
        {/*KAKUSEIDAからのニュース*/}
        <div className="faqTitle2">
          KAKUSEIDAからのニュース
        </div>
        <div className="faqDivBelowTitle">
          <div className="faqDivBelowTitleChild" onClick={()=>{this.showIt('fqaShowTip')}} >
            身に覚えのない「支援完了のお知らせ」メールが届いた場合について

            <hr className={"faqHr"}/>
            {this.state.fqaShowTip && <div className={"fqaShowTip"} >
              <p> KAKUSEIDAという機能をご用意しており、会員登録およびログインなしでメールアドレスのみの入力にてプロジェクトへの支援が可能です。</p>
              <p>そのため、身に覚えのないメールが届いた場合は速やかに削除していただき、お問い合わせフォームよりご連絡いただきますようお願いいたします。</p>
            </div>}

          </div>

          <div className="faqDivBelowTitleChild">
            寄付型クラウドファンディングについて
            <hr className={"faqHr"}/>
          </div>

          <div className="faqDivBelowTitleChild">
          支援について
          <hr className={"faqHr"}/>
        </div>
          <div className="faqDivBelowTitleChild">
            ファンクラブの会員費について
            <hr className={"faqHr"}/>
          </div>

          <div className="faqDivBelowTitleChild">
            まとめて支払い
            <hr className={"faqHr"}/>
          </div>

          <div className="faqDivBelowTitleChild">
            <p className={"faqEndChild"}> もっと見る </p>
            <hr className={"faqHr"}/>
          </div>

        </div>


      </div>
    </div>);
  };




  render() {
    return (
        this.getFaq()

    )
  }
}

