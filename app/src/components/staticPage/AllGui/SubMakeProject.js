import React, { Component } from 'react';
import '../../asset/style/AllGui.css';
import ScrollableAnchor, {configureAnchors} from 'react-scrollable-anchor'
export default class SubMakeProject extends Component {
  constructor(props) {
    super(props);
    //configureAnchors({offset: 0, scrollDuration: 1000})

  }
  componentDidUpdate(prevProps, prevState, snapshot) {

  }
  componentDidMount() {


  }

  ClickGo= (i)=>{
    this.refs[i].scrollIntoView({block: 'end', behavior: 'smooth'});


  };
  GoNext = ()=>{
window.location.href='/kakuseida/AllGui/guid-line';


  };
  getHtMakeProject = () =>{
    return <div className={'makeProContent'}>
      <div className={'floatClear'} />
      <div className={'makeProLeft'}>

        <p><a className={"aLeft"} href={'javascript:;'} onClick={e=>{this.ClickGo(1)}}> １ ： タイトル </a></p>
        <p> <a className={"aLeft"} href={'javascript:;'} onClick={e=>{this.ClickGo(2)}}>２ ： キャプション </a></p>
        <p><a className={"aLeft"} href={'javascript:;'} onClick={e=>{this.ClickGo(3)}}> ３ ： カテゴリ </a></p>
        <p><a className={"aLeft"} href={'javascript:;'} onClick={e=>{this.ClickGo(4)}}> ４ ： 目標金額 </a></p>
        <p><a className={"aLeft"} href={'javascript:;'} onClick={e=>{this.ClickGo(5)}}> ５ ： 募集期間 </a></p>
        <p><a className={"aLeft"} href={'javascript:;'} onClick={e=>{this.ClickGo(6)}}> ６ ： メイン画像、動画 </a></p>
        <p> <a className={"aLeft"} href={'javascript:;'} onClick={e=>{this.ClickGo(7)}}>７ ： プロジェクト本文 </a></p>
        <p><a className={"aLeft"} href={'javascript:;'} onClick={e=>{this.ClickGo(8)}}> ８ ： リターン </a></p>
        <p> <a className={"aLeft"} href={'javascript:;'} onClick={e=>{this.ClickGo(9)}} >９ ： プロフィール </a></p>
        <p><a className={"aLeft"} href={'javascript:;'} onClick={e=>{this.ClickGo(10)}}> １０ ： 本人確認情報</a></p>

      </div>
      <div className={'makeProRight'}>
        {/*11111111111*/}
        <div className={"contentRight"} ref={1}>
          <div className={"contentRightNumber"}>1</div>
          <div className={"contentRightText"}>
            <div className={"contentRightInner"}>
              タイトル - Title
              <div className={"newline"} />
              タイトルは、プロジェクトで1番はじめに人々の目に触れる重要な要素です。FacebookやTwitterなどのSNSで拡散する場合にも１番大きく表示されすのでとても重要です。
              多くの人はタイトルを見て、プロジェクトに興味を持ってあなたのページを訪れます。
              <div className={"newline"} />
              <p className={"correctBg"}> POINT </p>
              <div className={"divp1"}>
              <p >・文字数は40文字以内 </p>
              <p>・あなたの商品がわかる名前</p>
              <p>・プロジェクトの簡単な説明</p>
              <p>・ジャンル</p>
              </div>
              <div className={"newline"} />

              <hr className="myHr" />
            </div>

          </div>

        </div>
        {/*222222*/}
        <div className={"contentRight"} ref={2}>
          <div className={"contentRightNumber"} id={"キャプション - Caption"}>2</div>
          <div className={"contentRightText"}>
            <div className={"contentRightInner"}>
              キャプション - Caption
              <div className={"newline"} />
              キャプションは、プロジェクトの概要を3行程度で表示させる見る人が興味をそそられるような意味のある内容にしましょう。
              <div className={"newline"} />
              <p className={"correctBg"}> POINT </p>
              <div className={"divp1"}>
                <p >・文字数は150文字以内</p>

              </div>
              <div className={"newline"} />

              <hr className="myHr" />
            </div>

          </div>

        </div>
        {/*3333*/}
        <div className={"contentRight"} >
          <div className={"contentRightNumber"} ref={3}>3</div>
          <div className={"contentRightText"}>
            <div className={"contentRightInner"}>
              カテゴリ - CATEGORY
              <div className={"newline"} />
              プロジェクトの目的から適切なカテゴリを1つ選択して設定しましょう。カテゴリを設定しておくことで同じ分類のプロジェクトが提示され、ユーザーの目に止まりやすくなります
              <div className={"newline"} />


              <hr className="myHr" />
            </div>

          </div>

        </div>

        {/*4444*/}


        <div className={"contentRight"}>
          <div className={"contentRightNumber"} ref={4}  >4
            </div>

          <div className={"contentRightText"}>
            <div className={"contentRightInner moreNumber4"}>
              目標金額 - Set your Goal
              <div className={"newline"} />
              <p>「All-or-Nothing」では募集期間内に目標金額を達成できなかった場合、プロジェクトは不成立となります。</p>
              <p>支援金の決済は実行されず、プロジェクトオーナーに支援金は支払われません（不成立の場合は手数料は一切かかりません）。</p>
              <p>「All-In」では目標金額の達成・未達成に関わらず支援金が支払われますが、掲載時にプロジェクトの実施を確約する必要があります。</p>
              <p>プロジェクトの内容によっては、「All-In」がご利用できない場合がございます。</p>
              <div className={"newline"} />
              <p className={"correctBg"}> POINT </p>
              <div className={"divp1"}>
                <p >・プロジェクトの実現のために必要な金額</p>
                <p > ・現実的に集められそうな金額</p>
                <p >  ・リターンの設定で集まりそうな金額</p>

              </div>
              <div className={"newline"} />
               <div className={"divAsteris"}> ※目標金額は10000円以上に設定してください</div>
              <div className={"newline1"} />
              <div className={"div4Bg"}>
                <p >！注意事項！</p>
                <p > ・リターンに原価がかかる場合は、原価も考慮した上で目標金額を設定しましょう。</p>
                  <p > ・KAKUSEIDAの手数料は0％です。</p>
              </div>
              <hr className="myHr" />
            </div>

          </div>

        </div>


        {/*555*/}
        <div className={"contentRight"} >
          <div className={"contentRightNumber"} ref={5} >5</div>
          <div className={"contentRightText"}>
            <div className={"contentRightInner moreNumber5"}>
              募集期間 - Set your period
              <div className={"newline"} />
              目標金額を設定した後は募集期間を設定しましょう。目標金額やお金が必要な時期を考慮しながら募集期間を設定しましょう。
              <div className={"newline"} />

              <div className={"div4Bg"}>
                <p >！注意事項！</p>
                <p > ・プロジェクト終了日を土日祝日にするのはおすすめしません（休日は平日に比べ、支援してくれる人がパソコンの前にいない事が多いためです）
                  </p>
                <p > ・長ければそれだけ支援が集まるとは限りません。あなたが集中力を切らさずにプロジェクトのPRを続けられる期間を設定しましょう。</p>
              </div>
              <hr className="myHr" />
            </div>

          </div>

        </div>

        {/*666*/}
        <div className={"contentRight"}>
          <div className={"contentRightNumber"}  ref={6} >6</div>
          <div className={"contentRightText"}>
            <div className={"contentRightInner moreNumber6"}>
              メイン画像・動画 - Image , Movie
              <div className={"newline"} />

              <p> &#60;画像&#62; </p>
              <div className={"newline"} />
              <p>  メイン画像は、トップページとプロジェクト一覧ページなどで表示されるプロジェクトの顔です。FacebookやTwitterでは、タイトルと一緒にメイン画像もシェアされます。</p>
              <p>  目立つことよりも、伝わることが重要。タイトルとセットでお互いを補完・相乗し合うことが大切です。</p>
              <div className={"newline"} />
              <p> &#60;動画&#62;</p>
              <div className={"newline"} />
              <p>   海外のクラウドファンディングサイトの事例では、動画があるプロジェクトとないプロジェクトでは支援額に3倍もの違いがあります。</p>
              <p>  目標金額を達成することが第1のスタートですが、もしあなたが目標金額を越えて200%、300%…とたくさんの支援を集めるようなビッグプロジェクトを目指すのであれば、</p>
              <p>   動画は時間をかけて準備しましょう。動画は作品、プロダクトの過去の映像でも構いません。</p>
              <p>   あなたが出演して自己紹介、プロジェクトをやろうと思った理由、具体的になぜ資金が必要なのか？その資金がどう使われるのか？支援のお願いとリターンの説明などで</p>
              <p>  見ている人の心を掴むのもいいかもしれません。動画の長さとしては3分程度が平均的です。</p>

              <div className={"newline"} />


              <hr className="myHr" />
            </div>

          </div>

        </div>

        {/*77777*/}
        <div className={"contentRight"}  >
          <div className={"contentRightNumber"} ref={7} >7</div>
          <div className={"contentRightText rightNumber7"}>
            <div className={"contentRightInner"}>
              プロジェクト本文 - About your project
              <div className={"newline"} />
              <p> プロジェクト本文とは、あなたのプロジェクトページのセンターに表示される、本文のことです。</p>
               <p>  プロジェクト本文を作る時は、テキストと画像を利用して、あなたのプロジェクトを表現しましょう。</p>
<div className={"newline1"} />
                 <p>  画像とテキストの分量のバランスは大切です。</p>
                   <p>  長過ぎたり、文章ばかりが続くと、なかなか読んでもらえません。画像とテキストの分量は、6:4くらいがおすすめです。</p>
                     <p> 何を実現したくて、何のために資金が必要なのかを伝えましょう。ビジュアル付きでリターンの説明をすると、より支援されやすい傾向にあります。</p>
                       <p>  わかりやすい心を動かすような文章を心掛けましょう。</p>
              <div className={"newline"} /><div className={"newline"} />
              <p className={"correctBg"}> POINT </p>
              <div className={"divp1"}>

                  <p > ・プロジェクトをやろうと思った理由</p>
                  <p > ・プロジェクトをなぜ行いたいのか</p>
                  <p > ・具体的になぜ資金が必要なのか？その資金がどう使われるのか？</p>
                  <p > ・支援のお願いとビジュアル付きでリターンの説明</p>

              </div>
              <div className={"newline"} />

              <hr className="myHr" />
            </div>

          </div>

        </div>

        {/*88888*/}
        <div className={"contentRight"}  >
          <div className={"contentRightNumber"} ref={8}>8</div>
          <div className={"contentRightText"}>
            <div className={"contentRightInner moreNumber8"}>
              リターン - Return
              <div className={"newline"} />
              <p> 支援をしてくれた方へ、支援金に応じたお返しをすることで資金を募ります。魅力的なリターンは、プロジェクトの要にもなってきます。より魅力的なリターンを考えましょう。</p>
              <p>プロジェクトに合った内容と価格でリターンを設定しましょう。プロダクトや立体作品、グッズの場合は、サイズや大きさを記載しましょう。</p>
              <p>本文の中に、リターンのイメージ画像があると、より支援につながりやすくなります。</p>
              <div className={"newline"} />

              <div className={"div4Bg"}>
                <p >！注意事項！</p>
                <p >
                  （１）法令に違反するもの。 </p>
                <p >  （２）著作権を保有していない著作物、使用許諾を受けていない著作物その他正当な使用権を保有していないもの。 </p>
                <p >（３）銃器・凶器類、たばこ、医薬品・医療品、動物、昆虫等の生物、販売に際して法律で義務づけられている免許や資格条件を満たしていないもの、非合法商品全般。 </p>
                <p >（４）火薬・花火など危険性の高い商品、犯罪に使用されるおそれがある商品（エアガン、スタンガン、催涙スプレー）、 法令により携行を禁止された刃物、開運、 </p>
                <p >魔よけを標榜（ひょうぼう）する高額商品、金融商品（株や配当など）、一般に流通している商品券やクーポン券といった換金性の高いもの。 </p>
                <p >（５）一般に市販されている、もしくは定価がある商品やサービス。 </p>
                <p >（６）一般に市販されていないが、自らが提供する商品で定価があり、リターンの価格がその定価との間に差額が生じるもの。 </p>
                <p >（７）将来的に一般に市販を予定しており、商品に予定している定価があり、リターンの価格がその定価との間に差額が生じ、その割引率等の表記をすること。 </p>
                <p >（８）その他当社が不適切と判断したもの。 </p>
              </div>
              <hr className="myHr" />
            </div>

          </div>

        </div>

        {/*999999*/}
        <div className={"contentRight"} >
          <div className={"contentRightNumber"} name="Set-your-period" ref={9} id={"ProfileNum9"}>9</div>
          <div className={"contentRightText"}>
            <div className={"contentRightInner moreNumber9"}>
              プロフィール - Profile
              <div className={"newline"} />

              <p> プロフィールには、KAKUSIDAにユーザー登録した際のユーザー名、プロフィールが設定されます。</p>
              <p> アイコンは、支援者にプロジェクトのイメージしやすくさせる大切な要素のひとつです。なるべく、顔写真を掲載しましょう。</p>
              <p> チームの場合は、代表者やロゴでも良いですが、チームメンバーの顔が見えることで支援する人にとっては安心感に繋がります。</p>
              <div className={"newline"} />
              <hr className="myHr"  />
            </div>

          </div>

        </div>
        {/*10 10 10*/}

        <div className={"contentRight"} >
          <div className={"contentRightNumber"} ref={10} >10</div>
          <div className={"contentRightText"}>
            <div className={"contentRightInner moreNumber10"}>
              本人確認情報 - Address, Telephone Number etc.
              <div className={"newline"} />
              <p> KAKUSIDAではプロジェクトオーナーに限り、下記の情報のご提供を必須とさせていただいております（非公開）</p>
              <p>・本名</p>
              <p>・住所</p>
              <p>・電話番号</p>
              <p>・振込先口座</p>
              <div className={"newline"} />
              <hr className="myHr" />
            </div>

          </div>

        </div>

        {/*11 11 11*/}

        <div className={"contentRight"}>
          <div className={"contentRightNumber"} name="Set-your-period" id={"Telephone11"}></div>
          <div className={"contentRightText"}>
            <div className={"contentRightInner moreNumber11"}>
              プロジェクト企画者は企画者が遵守すべき利用条件もご確認ください
              <div className={"newline"} />
              <div className={"newline"} />
              <div className={"divBigButton1"} onClick={e=>{this.GoNext()}}>
                <div className={"divBigButton2"} >
                  企画者ガイドライン
                </div>
              </div>
            </div>

          </div>

        </div>

      </div>
    </div>;
  };




  render() {
    return (
        this.getHtMakeProject()

    )
  }
}

