/* @flow */

import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {faCaretRight} from '@fortawesome/free-solid-svg-icons';
import History from "../../../history";


export default class BreadCrumbsSub extends Component {
  render() {
    const {linkBreacrm} = this.props;
    return (
      <div className="bread-crums">
        <ul>
          <li>
            <Link className="breadcrumb-link" to={'/kakuseida/AllGui/faq'}>よくある質問 </Link>
            <FontAwesomeIcon className="icon-splash" icon={faCaretRight} />
          </li>
          {
                <li >
                  <span>{linkBreacrm}</span>
                </li>
          }
        </ul>
      </div>
    );
  }
}
