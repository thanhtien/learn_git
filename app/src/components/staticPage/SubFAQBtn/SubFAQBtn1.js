import React, { Component } from 'react';
import '../../asset/style/SubFaqBtn.css';
import History from "../../../history";
import BreadCrumbsSub from "./BreadCrumbsSub";

export default class SubFAQBtn1 extends Component {
  constructor(props) {
    super(props);
  }


  getFaq1 = () =>{
    return(
    <div className={'wraper'}>
      <div className={'SubFAQBtn1'} >
        <div className={'SubFAQBtn1Path'} >
          <BreadCrumbsSub linkBreacrm={"プロジェクト製作者向け"} />
        </div>
        {/*4 button*/}
        <div className={'SubFAQBtn1PathSearch'}>

          <div className="divSearchLeft">
            <form className="faq-form-search">
              <input  className="btnSearchLeft" type="button" onClick={()=>{this.searchKeyWord()}}  name="" value="" />
              <input type={"input"}   className={"searchLeft"} placeholder={"プロジェクトを探す"} />

            </form>
          </div>
        </div>
        <div className={'SubFAQBtn1Content'}>
          <div className={'SubFAQBtn1Title'}>
            プロジェクト製作者向け
          </div>
          <div className={"main8"}>
            {/*111*/}
          <div className={'SubFAQBtn1ContainerLeftRight'}>
            <div className={'SubFAQBtn1ContainerLeft'}>
              <a href={''} className={'buttonSub'} >1.プロジェクトの作成方法に関して</a>
            </div>
            <div className={'SubFAQBtn1ContainerRight'}>
              <p>  新規登録したいです </p>
              <p> 寄付型クラウドファンディングについて</p>
              <p>  支援について</p>
              <p> ファンクラブの会員費について</p>
              <p>  まとめて支払い</p>
            </div>
          </div>
            {/*222*/}
            <div className={'SubFAQBtn1ContainerLeftRight'}>
              <div className={'SubFAQBtn1ContainerLeft'}>
                <a href={''} className={'buttonSub'} >2.プロジェクトの作成方法に関して</a>
              </div>
              <div className={'SubFAQBtn1ContainerRight'}>
                <p>  新規登録したいです </p>
                <p> 寄付型クラウドファンディングについて</p>
                <p>  支援について</p>
                <p> ファンクラブの会員費について</p>
                <p>  まとめて支払い</p>
              </div>
            </div>
            {/*333*/}
            <div className={'SubFAQBtn1ContainerLeftRight'}>
              <div className={'SubFAQBtn1ContainerLeft'}>
                <a href={''} className={'buttonSub'} >3.プロジェクトの作成方法に関して</a>
              </div>
              <div className={'SubFAQBtn1ContainerRight'}>
                <p>  新規登録したいです </p>
                <p> 寄付型クラウドファンディングについて</p>
                <p>  支援について</p>
                <p> ファンクラブの会員費について</p>
                <p>  まとめて支払い</p>
              </div>
            </div>
            {/*444*/}

            <div className={'SubFAQBtn1ContainerLeftRight'}>
              <div className={'SubFAQBtn1ContainerLeft'}>
                <a href={''} className={'buttonSub'} >4.プロジェクトの作成方法に関して</a>
              </div>
              <div className={'SubFAQBtn1ContainerRight'}>
                <p>  新規登録したいです </p>
                <p> 寄付型クラウドファンディングについて</p>
                <p>  支援について</p>
                <p> ファンクラブの会員費について</p>
                <p>  まとめて支払い</p>
              </div>
            </div>
            {/*555*/}
            <div className={'SubFAQBtn1ContainerLeftRight'}>
              <div className={'SubFAQBtn1ContainerLeft'}>
                <a href={''} className={'buttonSub'} >5.プロジェクトの作成方法に関して</a>
              </div>
              <div className={'SubFAQBtn1ContainerRight'}>
                <p>  新規登録したいです </p>
                <p> 寄付型クラウドファンディングについて</p>
                <p>  支援について</p>
                <p> ファンクラブの会員費について</p>
                <p>  まとめて支払い</p>
              </div>
            </div>
            {/*66*/}
            <div className={'SubFAQBtn1ContainerLeftRight'}>
              <div className={'SubFAQBtn1ContainerLeft'}>
                <a href={''} className={'buttonSub'} >6.プロジェクトの作成方法に関して</a>
              </div>
              <div className={'SubFAQBtn1ContainerRight'}>
                <p>  新規登録したいです </p>
                <p> 寄付型クラウドファンディングについて</p>
                <p>  支援について</p>
                <p> ファンクラブの会員費について</p>
                <p>  まとめて支払い</p>
              </div>
            </div>
            {/*77*/}
            <div className={'SubFAQBtn1ContainerLeftRight'}>
              <div className={'SubFAQBtn1ContainerLeft'}>
                <a href={''} className={'buttonSub'} >7.プロジェクトの作成方法に関して</a>
              </div>
              <div className={'SubFAQBtn1ContainerRight'}>
                <p>  新規登録したいです </p>
                <p> 寄付型クラウドファンディングについて</p>
                <p>  支援について</p>
                <p> ファンクラブの会員費について</p>
                <p>  まとめて支払い</p>
              </div>
            </div>
            {/*88*/}
            <div className={'SubFAQBtn1ContainerLeftRight'}>
              <div className={'SubFAQBtn1ContainerLeft'}>
                <a href={''} className={'buttonSub'} >8.プロジェクトの作成方法に関して</a>
              </div>
              <div className={'SubFAQBtn1ContainerRight'}>
                <p>  新規登録したいです </p>
                <p> 寄付型クラウドファンディングについて</p>
                <p>  支援について</p>
                <p> ファンクラブの会員費について</p>
                <p>  まとめて支払い</p>
              </div>
            </div>
          </div>

        </div>



      </div>
    </div>);
  };




  render() {
    return (
        this.getFaq1()

    )
  }
}

