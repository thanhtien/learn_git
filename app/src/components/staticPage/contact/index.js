/* @flow */

import React, { Component } from 'react';
import { Field, reduxForm,reset } from 'redux-form';
import { connect } from 'react-redux';
import * as actions from '../../../actions/common';
import validate from './validate';
import renderFieldInput from '../../common/renderFieldInput';
import renderFieldTextArea from '../../common/renderFieldTextArea';
import Captcha from '../../common/ReCAPTCHA';
import scrollToFirstError from './scrollToFirstError';
import Loading from '../../common/loading';
import Thank from './thank';
import '../../asset/style/react-tabs.css';
import {Tab, TabList, TabPanel, Tabs} from "react-tabs";
class Contact extends Component {
  /**
   * [handleFormSubmit Send Email]
   */
  handleFormSubmit(data) {
    this.props.contactForm(data);
  }
  resetStatusContact() {
    this.props.resetStatusContact(false);
  }
  componentDidMount() {
    this.props.resetStatusContact(false);
    document.title = "お問い合わせ｜KAKUSEIDA";
  }

  getTitleContact=()=>{
    return  (<p className="title-white">
      お問い合わせ
      <br/>
      FAQ
    </p>);
  };
  getTitle=(idTab)=>{
    console.log(idTab);
    switch (idTab) {
      case 0:{
       // return this.getTitleTerm();
      }
      case 1:{
       // return this.getTitlePolicy();
      }
      case 2:{
        //return this.getTitleLega();
      }
      case 3:{
        return this.getTitleContact();
      }
      default: return  null;

    };
  };
  handleSelect=(idTab)=>{

    switch (idTab) {
      case 0:{
        window.location.href='/kakuseida/term';
      }
      case 1:{
        // return this.getTitlePolicy();
        window.location.href='/kakuseida/privacy-policy';
      }
      case 2:{
        //return this.getTitleLega();
        window.location.href='/kakuseida/legal';
      }
      case 3:{
        return   null;
      }
      default: return  null;

    };
    console.log(idTab);
  };
  getTab=(activeIndex)=>{
    return  <Tabs defaultIndex={activeIndex}  onSelect={index => this.handleSelect(index)} >
      <TabList>
        <Tab  >利用規約</Tab>
        <Tab >プライバシポリシー</Tab>
        <Tab >特定商取引法に基づく表記</Tab>
        <Tab  >お問い合わせ</Tab>
      </TabList>

      <TabPanel>
        ...
      </TabPanel>
      <TabPanel>
        ...
      </TabPanel>
      <TabPanel>
        ...
      </TabPanel>
      <TabPanel>
      </TabPanel>
    </Tabs>
  };
    render() {
    const { handleSubmit , contact_form_status , loading  } = this.props;
    const linkBreacrm = [
      {last:true,label:'お問い合わせ',link:null}
    ]

    if (loading) {
      return(
        <Loading></Loading>
      )
    }

    return (
      <div className="about_box ">

          <div className="banner-css">
            <div className="wraper titleHeader">
              {this.getTitle(3)}
              <div className={"bannerLeftStatic"}> <img  src={"/img/static/kakuseida.png"}/> </div>
            </div>
          </div>

        <div className="wraper">
          {this.getTab(3)}
          <p className="contact-information">
            受付時間：土日祝日年末年始を除く平日 <br/>
            ご質問やご意見はお気軽にお問い合わせください。<br/>
          </p>

          {
            !contact_form_status ?
            <div className="form-contact">
              <div className="form">
                <form onSubmit={handleSubmit(this.handleFormSubmit.bind(this))}>
                  <div className="form-field">
                    <p className="form-label">
                      お名前 <span>必須</span>
                    </p>
                    <Field
                      name="user_name"
                      placeholder="山田　太郎"
                      component={renderFieldInput}
                      type="text" />
                    <div className="clear-fix"></div>
                  </div>

                  <div className="form-field">
                    <p className="form-label">
                      返信先メールアドレス <span>必須</span>
                    </p>
                    <Field
                      name="email"
                      placeholder="xxx@gmail.com"
                      component={renderFieldInput}
                      type="text" />
                    <div className="clear-fix"></div>
                  </div>

                  <div className="form-field">
                    <p className="form-label">
                      お問い合わせ件名 <span>必須</span>
                    </p>
                    <Field
                      name="subject"
                      placeholder="例：支援のことについて"
                      component={renderFieldInput}
                      type="text" />
                    <div className="clear-fix"></div>
                  </div>


                  <div className="form-field textarea">
                    <p className="form-label">
                      お問い合わせ内容  <span>必須</span>
                    </p>

                    <Field
                      name="message"
                      placeholder="お問い合わせ内容を記入してください"
                      component={renderFieldTextArea}
                      type="text" />
                    <div className="clear-fix"></div>
                  </div>


                  <div className="form-field">
                    <p className="form-label">CAPTCHA<span>必須</span></p>
                    <Field name='captcharesponse' component={Captcha}/>
                    <div className="clear-fix"></div>
                  </div>

                  <div className="form-field">
                      <p className="form-label fix-sp-mb"></p>
                      <p className="form-submit"><input className="submit-contact" type="submit" value="送信"/></p>
                  </div>
                </form>
              </div>
            </div>:
            <Thank resetStatusContact={() => this.resetStatusContact()}></Thank>

          }





        </div>
    </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    loading: state.common.loading,
    contact_form_status:state.common.contact_form_status
  }
};
const afterSubmit = (result, dispatch) => {
  return (
    dispatch(reset('form-contact'))
  )
};

export default reduxForm({
    form: 'form-contact',
    onSubmitFail: (errors , dispatch) => scrollToFirstError(errors , dispatch),
    validate,
    onSubmitSuccess:afterSubmit

})(connect(mapStateToProps, actions)(Contact));
