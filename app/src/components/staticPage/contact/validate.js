const validate = values => {
  const errors = {};

  if (!values.user_name) {
    errors.user_name = 'お名前を入力してください';
  };
  if (!values.email) {
    errors.email = '返信先メールアドレスを入力してください';
  }else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
      errors.email = 'メールアドレスを正しくしてください';
  };
  if (!values.subject) {
    errors.subject = 'お問い合わせ件名を入力してください';
  };
  if (!values.message) {
    errors.message = 'お問い合わせ内容を入力してください';
  };
  if (!values.captcharesponse) {
    errors.captcharesponse = 'CAPTCHAを入力してください';
  }


  return errors;
};
export default validate;
