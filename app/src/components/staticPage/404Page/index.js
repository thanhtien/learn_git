



/* @flow */

import React, { Component } from 'react';

import { Link } from 'react-router-dom';
var CONFIG = require('../../../config/common');

if (process.env.NODE_ENV === "development") {
  CONFIG = CONFIG.CONFIG.DEV;
}else {
  CONFIG = CONFIG.CONFIG.PRODUCT;
}
export default class Page404 extends Component {
  componentDidMount() {
    document.title = "404エラー｜KAKUSEIDA";

  }
  render() {
    return (
      <div id="notfound">
    		<div className="cover-notfound">
          <p className="img-notfound">
            <img src={CONFIG.MAIN_URL+"img/common/logo.svg"} alt="CLOUD FUNDING SYSTEM"/>
          </p>
          <p className="text-bold">
            404 Not Found　
          </p>
          <p className="mess">
            ご指定のページが見つかりません <br/>ご指定のページは削除されたか、移動した可能性がございます。
          </p>
          <Link className="button-back" to="/">トップページ</Link>
        </div>
    	</div>
    );
  }
}
