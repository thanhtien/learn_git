import React, { Component } from 'react';
import Breadcrumbs from '../../Breadcrumbs';
import axios from "axios";
import {FROM_TYPE_LIST, LOADING_POST_STOP} from "../../../actions/types";
import History from "../../../history";

var CONFIG = require('../../../config/common');

if (process.env.NODE_ENV === "development") {
	CONFIG = CONFIG.CONFIG.DEV;
}else {
	CONFIG = CONFIG.CONFIG.PRODUCT;
}
const ROOT_URL = CONFIG.ROOT_URL;

class index extends Component {
    componentDidMount() {
        document.title = "Active Password｜KAKUSEIDA";
		const {slug} = this.props.match.params;

		this.checkActive(slug);

    }
    checkActive=(myKey)=>{
		axios.get(ROOT_URL+'API_Top/API/active_password/'+myKey+'/')
			.then(response =>{

				if(response.data.status==1){
					alert(response.data.message);
					window.location='/signin';
				}
				if(response.data.status==0){
					alert(response.data.message);
				}
				if(response.data.status==2){
					alert(response.data.message);
				}
				console.log( response.data);

			})
			.catch(function (error) {

				if (error) {
					if(error.response.status === 404) {
						History.push('/404');
					}else {
						alert(error.response.data.status);
					}
				}
			});
	}
    render() {
        const linkBreacrm = [
          { last: true, label: 'Active Password', link: null }
        ]
        return (
        <div className="daihyou-aisatsu" >
			<div className="banner-css banner-daihyou-aisatsu">
		        <div className="wraper">
		            <p className="title-white">
						Active Password
		            </p>
		        </div>
	        </div>

      	</div>
        );
    }
}




export default index;
