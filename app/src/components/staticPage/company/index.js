import React, { Component } from 'react';
import Breadcrumbs from '../../Breadcrumbs';



class index extends Component {
    componentDidMount() {
        document.title = "会社概要｜KAKUSEIDA";

    }
    render() {
        const linkBreacrm = [
          { last: true, label: '会社概要', link: null }
        ]
        return (
	        <div className="company" >
        		<div className="banner-center banner-company">
        	        <div className="wraper">
        	            <p className="center-title ">
        	              会社概要
        	            </p>
        	        </div>
                </div>
                <div className="wraper">
        			<Breadcrumbs linkBreacrm={linkBreacrm}></Breadcrumbs>
        			<table className="company-table">
						<tr>
							<th>会社名</th>
							<td>株式会社 カクセイ (KAKUSEI Inc.)</td>
						</tr>
						<tr>
							<th>業務内容</th>
							<td>
								不動産関連事業<br/>
								ソフトウェア開発事業<br/>
								運用コンテンツ企画・開発事業<br/>
								クラウドファンディング事業<br/>
								創業支援事業
							</td>
						</tr>
						<tr>
							<th>資本金等</th>
							<td>1億円(内、資本準備金4,200万円)</td>
						</tr>
						<tr>
							<th>代表者</th>
							<td>石橋泰寛    平山智浩</td>
						</tr>
						<tr>
							<th>従業員数</th>
							<td>12名（2019年4月）</td>
						</tr>
						<tr>
							<th>業務関連保有資格</th>
							<td>
								宅地建物取引士<br/>
								フィナンシャルプランナー<br/>
								一級建築施工管理技士<br/>
								JSHI公認ホームインスペクター<br/>
								建築仕上診断技術者<br/>
								マンションリフォームマネージャー<br/>
								既存住宅アドバイザー
							</td>
						</tr>
						<tr>
							<th>営業時間</th>
							<td>10:00～19:00</td>
						</tr>
						<tr>
							<th>定休日</th>
							<td>日曜日・年末年始</td>
						</tr>
						<tr>
							<th>電話/FAX</th>
							<td>03-6804-6204 (代表）/ 03-6804-6205（FAX)</td>
						</tr>
						<tr>
							<th>本社 / 支社</th>
							<td>
								東京本社<br/>
								〒105-0004　東京都港区新橋2丁目16番1号　ニュー新橋ビル511<br/>
								大阪支社<br/>
								〒556-0011　大阪府浪速区難波中2丁目7番7号　 ナンバFKビル4階
							</td>
						</tr>
						<tr>
							<th>免許番号</th>
							<td>宅地建物取引業 東京都知事(1)第97977号</td>
						</tr>
						<tr>
							<th>取引金融機関</th>
							<td>
								西武信用金庫<br/>
								りそな銀行<br/>
								三井住友銀行<br/>
								みずほ銀行<br/>
								東京シティ信用金庫<br/>
								西京信用金庫<br/>
								東日本銀行<br/>
								三井住友信託銀行<br/>
								きらぼし銀行<br/>
								さわやか信用金庫<br/>
								東京スター銀行<br/>
								世田谷信用金庫<br/>
								オリックス銀行<br/>
								SBJ銀行<br/>
								日本政策金融公庫<br/>
								三井住友トラストローン＆ファイナンス
							</td>
						</tr>
						<tr>
							<th>所属団体</th>
							<td>
								公益社団法人 全国宅地建物取引業協会連合会<br/>
								公益社団法人 全国宅地建物取引業保証協会
							</td>
						</tr>
        			</table>
    			</div>
	      	</div>
        );
    }
}




export default index;
