import React, { Component } from 'react';
import Breadcrumbs from '../../Breadcrumbs';
import AllKaku from "../AllKaku";
import '../../asset/style/react-tabs.css';
export default class Term extends Component {
  constructor(props) {
    super(props);

    this.state={activeIndex:0};

  }
  componentDidMount() {
    document.title = "利用規約｜KAKUSEIDA";

  }

  render() {
    var linkBreacrm;
    if (window.location.pathname.indexOf('term') > 0) {
      linkBreacrm = [
        {last:true,label:'利用規約',link:null}
      ]
    }else {
      linkBreacrm = [
        {last:true,label:'kakuseida',link:null}
      ]
    }

    return (
        <AllKaku activeIndex={this.state.activeIndex} parentThis={this}/>
    );
  }
}
