import React, { Component } from 'react';
import Breadcrumbs from '../../Breadcrumbs';
import AllKaku from "../AllKaku";
import '../../asset/style/react-tabs.css';
export default class Legal extends Component {
  constructor(props) {
    super(props);
    const {tabactive} = this.props.match.params;

    // let activeIndex  = 0;
    // if(tabactive =='term'){
    //   activeIndex  = 0;
    // }else
    // if(tabactive =='privacy-policy'){
    //   activeIndex  = 1;
    // }
    // else
    // if(tabactive =='legal'){
    //   activeIndex  = 2;
    // }
    // if(tabactive =='guideline'){
    //   activeIndex  = 3;
    // }

    this.state={activeIndex:2};

  }

  componentDidMount() {
    document.title = "特定商取引法に基づく表記｜KAKUSEIDA";

  }
  render() {
    console.log(this.state.activeIndex);
    const linkBreacrm = [
      {last:true,label:'特定商取引法に基づく表記',link:null}
    ]
      return (
        <AllKaku activeIndex={this.state.activeIndex} parentThis={this}/>
    );
  }
}
