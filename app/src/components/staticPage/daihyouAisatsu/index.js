import React, { Component } from 'react';
import Breadcrumbs from '../../Breadcrumbs';



class index extends Component {
    componentDidMount() {
        document.title = "代表者挨拶｜KAKUSEIDA";

    }
    render() {
        const linkBreacrm = [
          { last: true, label: '代表者挨拶', link: null }
        ]
        return (
        <div className="daihyou-aisatsu" >
			<div className="banner-css banner-daihyou-aisatsu">
		        <div className="wraper">
		            <p className="title-white">
		              代表者挨拶
		            </p>
		        </div>
	        </div>
	        <div className="wraper">　　　　　　　　
                
				<Breadcrumbs linkBreacrm={linkBreacrm}></Breadcrumbs>
				<div id="block_aisatsu">
                <div id="div_kaichou">
					<span><img src="/img/common/kaichou_img.png" alt="会長"/></span>
                </div>
                <div id ="div_aisatsu_con">
                 <div id="signature_div">
						<p className="signature">石橋泰寛<span>共同創業者/代表取締役会長</span></p>
                 </div>
                  <div>
						<p className="bold">
		                     人生は図らずもいただいた時間、<br/>
							 いつのまに僕たちは“持つ”ことに慣れてしまったのか<br/>

							「生命体としてすべて可能性を発見させたい！」<br/>
							この一念から我々がはスタートしました<br/>
							可能性を信じて、パッションのあるすべての仲間に実現のかのを<br/>
							そして、ともに成長する<br/>
							人間がいまだ到達しえなかった景色を観てみよう<br/>
		                    <br/>
							記憶に残る人生はおそらく一度しかない、やれるならとことんやってみよう<br/>
							そこに信念と情熱がある限り、可能性は無限大に広がり続ける<br/>
							こんなはずじゃないほどの圧倒的な自分はきと絶景だ<br/>
		                    <br/>
							遭ってみませんか、初めて逢う自分に<br/>
							半端な生き方はもうやめよう、そう、覚醒しよう<br/>
						</p>
                    </div>
                    
	             </div>
                 </div>
                <hr/>
                <div id="block_aisatsu" className="intro-right-side">
                <div id="div_kaichou">
　　　　　　　　　　<span ><img src="/img/common/shachou_img.png" alt="社長" /></span>
                </div>
				<div id ="div_aisatsu_con">
				<div id="signature_div">
				<p className="signature">平山智浩<span>共同創業者/代表取締役社長</span></p>
			</div>
			<div>
			<p className="bold">
						「より多くの人々と喜びを分かち合う“共通の志”を実現するために！」<br/>
			カクセイは、不動産に関わるサービスを通じて顧客の豊かな生活づくりを支援
			し、新たな社会や文化の構築に貢献しておきました<br/>
			<br/>
			社会全体を見渡すと、プロックチェーンやAIの技術革新、またシェアリングエ
			コノミーの成長など、多くの先進的な技術やサービスが世に出てきています。
			これからも次々にそれらが生みだされ、私たちの暮らしを革新的に変えていく
			ことでしょう<br/>
			<br/>
			カクセイは、あらゆるテクノロジーを駆使して人々の
			生活・ビジネスに必要な空間の最大価値化を実現します<br/>
			<br/>
			あらゆる空間において、
						我々はスペース・イノベーション・カンパニーとして
			世の中を牽引していきます<br/>
			</p>
			</div>
			</div>
                </div>
                
	        </div>
      	</div>
        );
    }
}




export default index;
