import React, { Component } from 'react';

import '../../asset/style/kakuseitowa.css';
import History from '../../../history.js';
export default class ThreeGirlMain extends Component {
  constructor(props) {
    super(props);

  }
  componentWillMount() {
  }
  btnGo2=()=>{
    ///
    History.push('/project-from-type/from-your-access/page=1');
  }
  btnGo1=()=>{
    ///
    History.push('/first-user/first-user-project');
  }
  componentDidMount() {
    document.title = "カクセイだとは｜KAKUSEIDA";
  }
  getThreeGirl=()=>{
    return (<div>

      <div className="imageContent"> <img  src={"/img/static/kakuseida.png"}/>
      </div>
      <div className={"textContent"}>
        <div className={"Content"}>
          <p>カクセイだ！は、株式会社カクセイが運用するクラウドファンディングサイトです。</p>
          <p>クラウドファンディングは、「新しいチャレンジに向かってお金を必要としている人」と「チャレンジする人を応援するために投資したい人」</p>
          <p>を結びつける、新しい金融の形です。近年話題となっている、金融とITが融合した「FinTech」の一つとして注目されています。</p>
          <p><br/></p>
          <p>カクセイだ！のクラウドファンディングの特徴は、プロジェクト支援成立時の成功報酬として発生する手数料が０%であるというところです。</p>
          <p>支援者様の投資金額がそのままプロジェクト立案者様の元に届けられるシステムとなっております。</p>
          <p>カクセイだ！は、頑張る人と応援したい人を繋げ、夢をカタチにするお手伝いをします。</p>
        </div>
      </div>

    </div>);
  };
  render() {

    return (
      <div className="wrap-main">
        <div className="video-here-banner">
          <img src={'/img/static/kakuseidatowa_banner.png'}/>
          <div className="twoButton wraper">
            <div className="button1" onClick={()=>{this.btnGo1()}}>
              <div className="child"><div >
              <p className="textFirst">プロジェクトを始める</p>
              <p className="textSecond">資金を調達したい方</p> </div>
              </div>
            </div>
            <div className="button2" onClick={()=>{this.btnGo2()}}>
              <div className="child"><div >
              <p className="textFirst">プロジェクトを探す</p>
              <p className="textSecond">応援したい方</p></div>
              </div>
            </div>
          </div>
        </div>
        <div className="wraper">
          {this.getThreeGirl()}
        </div>

    </div>

    );
  }
}
