/* @flow */

import React, { Component } from 'react';

export default class Nav extends Component {

  ChangeStep(step){
    const {disable} = this.props;
    if (!disable) {
      this.props.ChangeStep(step);
    }
  }
  //For Nav Stripe
  _renderNavStripe() {
    const {step,disable} = this.props;
    return(
      <ul id="topNavStripe" className="animated fadeIn">
          <li onClick={ () => this.ChangeStep(1) } className={ step === 1 && disable === false ? "active" : null }>１．リターン　選択</li>
          <li onClick={ () => this.ChangeStep(2) } className={ step === 2 && disable === false ? "active" : null }>２．お支払情報の入力</li>
          <li className={ step === 3 && disable === false ? "active" : null }>３．申込み内容の確認</li>
          <li  className={ disable === true ? "active" : null }>４．完了</li>
      </ul>
    )
  }


  render() {

    return (
      <div className="nav-tab">
        <div className="wraper">
          {
            this._renderNavStripe()
          }
        </div>
    </div>
    );
  }
}
