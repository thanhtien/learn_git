/* @flow */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actions from '../../../actions/editSystem';
import Loading from '../../common/loading';
import { Field, reduxForm  } from 'redux-form';
import renderFieldInput from '../../common/renderFieldInput';

import validate from './validate';
import CvcPop from '../../common/cvcPopup';
import scrollToFirstError from './scrollToFirstError';
import { scroller } from 'react-scroll';
import {
    CREDIT_MONTH_YEAR
} from '../../../actions/types';
import NumberFormat from './NumberFormat';


 class Form extends Component {


 constructor(props) {
   super(props);
   this.state = {
     showpass:false
   };

 }
/**
 * [showpass Show hide CVC]
 * @return {[type]} [CVC HIDE/SHOW]
 */
 showpass(e) {
   e.stopPropagation();
   this.setState({
     showpass:!this.state.showpass
   })
 }



 /**
  * [handleFormSubmit Submit Form Create Project]
  * @param  {[type]} data [Data TO Server]
  */

  handleFormSubmit(data) {
    const {projectID,backerId} = this.props;
    if (data.exp_month === "") {
      this.props.dispatch({
        type:CREDIT_MONTH_YEAR,
        payload:"有効期限の月を入力してください"
      })
    }

    if (data.exp_year === "") {
      this.props.dispatch({
        type:CREDIT_MONTH_YEAR,
        payload:"有効期限の年を入力してください"
      })
    }

    setTimeout( () => {
      if (!this.props.errorMonthYear) {
        data.project_id = projectID;
        data.backing_level_id = backerId;
        this.props.SettingViSaDonateNonLogin(data , this.props.ChangeStep);
      }
      else {
        scroller.scrollTo('errorYearMonth', { offset: -200, smooth: true });
      }
    }, 10);


  }
 /**
  * [render description]
  * @return {[type]} [description]
  */
  render() {

    const {handleSubmit,errorMonthYear} = this.props;
    const {showpass} = this.state;

    if (this.props.loading) {
      return(
        <Loading></Loading>
      )
    }


    return (
      <div>
        {
          <form onSubmit={handleSubmit(this.handleFormSubmit.bind(this))}>


            <div className="form-area">
                <p className="page-tab-label">ユーザーの基本情報</p>
                <div className="form-box">
                    <div className="form-field">
                        <p className="form-field-name">名前<span>必須</span></p>
                        <div className="form-field-content">
                          {
                            <Field
                              name="name"
                              component={renderFieldInput}
                              type="text"
                              placeholder="山田　太郎"
                            />
                          }
                        </div>
                    </div>

                    <div className="form-field">
                        <p className="form-field-name">メールアドレス<span>必須</span></p>
                        <div className="form-field-content">
                          {
                            <Field
                              name="email"
                              component={renderFieldInput}
                              type="text"
                              placeholder="xxx@gmail.com"
                            />
                          }
                        </div>
                    </div>



                </div>
            </div>

              <div className="form-area">
                  <p className="page-tab-label">クレジットカード</p>
                  <div className="form-box">
                      <div className="form-field">
                          <p className="form-field-name">カード番号<span>必須</span></p>
                          <div className="form-field-content">
                              <Field
                                name="number_card"
                                component={renderFieldInput}
                                type="text"
                                placeholder="4242-4242-4242-4242"
                                maxlength={16}
                              />
                          </div>
                      </div>
                      <div className="form-field">
                          <p className="form-field-name">有効期限<span>必須</span></p>
                          <div className="form-field-content">
                              <div>
                                <div name="errorYearMonth" className="moth-year">
                                  <Field
                                    name="exp_month"
                                    component={renderFieldInput}
                                    type="text"
                                    placeholder="月"
                                    maxlength={2}
                                    normalize={NumberFormat}
                                  />

                                  <Field
                                    name="exp_year"
                                    component={renderFieldInput}
                                    type="text"
                                    placeholder="年"
                                    maxlength={2}
                                    normalize={NumberFormat}
                                  />
                                  <span className="month-year-text">
                                    mm/yy
                                  </span>

                                </div>

                                <span  className="text-danger" style={{color: "red"}}>
                                  {
                                    errorMonthYear ? errorMonthYear : null
                                  }
                                </span>
                              </div>

                          </div>
                      </div>
                      <div className="form-field">
                          <p className="form-field-name form-field-name-long ">セキュリティーコード<span>必須</span></p>
                          <div className="form-field-content cvc-input-stripe">
                              <Field
                                name="cvc"
                                component={renderFieldInput}
                                type={ showpass ? 'text' : 'password' }
                                placeholder="（例）123"
                                maxlength={4}
                              />
                            <CvcPop></CvcPop>

                            <span className='show-pass' onClick={ (e) => this.showpass(e) }>

                              {
                                showpass ?
                                <i className="icss-anim icss-eye"></i> :
                                <i className="icss-anim icss-eye-slash"></i>
                              }

                            </span>

                          </div>
                      </div>

                  </div>
              </div>



              <input className="btn-submit" type="submit" value="確認画面"/>

          </form>
        }
      </div>

    );
  }
}


Form = reduxForm({
  form: 'donation-token',
  destroyOnUnmount: false,
  onSubmitFail: (errors , dispatch) => scrollToFirstError(errors , dispatch),
  validate,
})(Form)
const mapStateToProps = (state) => {
    return {
      loading:state.common.loading,
      errorMonthYear:state.common.errorMonthYear,
    }
}

// You have to connect() to any reducers that you wish to connect to yourself
Form = connect(mapStateToProps, actions )(Form);



export default Form
