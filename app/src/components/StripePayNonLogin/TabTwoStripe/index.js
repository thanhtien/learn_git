/* @flow */

import React, { Component } from 'react';
import Form from './Form';
export default class TabTwo extends Component {

  ChangeStep() {
    this.props.ChangeStep(3);
  }
  /**
   * [_renderHTML description]
   * @param  {[type]} profileUser [Data Default]
   * @return {[type]}             [HTML React]
   */
  _renderHTML() {
    const {projectID,backerId} = this.props;

    return (
      <div className="page-tab">
            <p className="page-tab-head">お支払い情報を入力してください （クレジットカード）</p>
            <div className="page-tab-content">
                <div className="page-tab-form">

                  <Form
                    backerId={backerId}
                    projectID={projectID}
                    ChangeStep={ () => this.ChangeStep() }/>

                </div>
            </div>
        </div>
    )
  }
  render() {


    return (
      <div className="wraper">
        {
          this._renderHTML()
        }
      </div>
    );
  }
}
