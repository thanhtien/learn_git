/* @flow */

import React, { Component } from 'react';

import NumberFormat from 'react-number-format';


var CONFIG = require('../../../config/common');

if (process.env.NODE_ENV === "development") {
  CONFIG = CONFIG.CONFIG.DEV;
}else {
  CONFIG = CONFIG.CONFIG.PRODUCT;
}
export default class ProjectReturn extends Component {

  render() {
    const {data} = this.props;

    console.log(data);

    return (
      <div className="page-content" style={{display:'block', width:'93%' , marginBottom:'20px'}}>
          <div className="pannel-white pannel-white-1">
              <p className="pw-img"><img src={data.thumnail} alt="thumnail"/></p>
              <div className="pw-content">
                  <p className="pw-content-number">
                    <img className="img-ticket" src={CONFIG.MAIN_URL+"/img/project-detail/ico-checked.svg"} alt="backer" />
                    <span><NumberFormat value={data.invest_amount} displayType={'text'} thousandSeparator={true} suffix={'円'} /></span>
                  </p>

                  <p className="pw-content-text" style={{whiteSpace:"pre-line"}}>{data.return_amount}</p>

                  <div className="pw-content-status">
                      <p className="pw-content-status-s pw-content-status-s1">支援者数：　<span>{data.now_count}人</span></p>
                        <div className="pw-content-status">

                              {data.schedule ? <p className="return-list-s return-list-s-2">
                                お届け予定：<span>{data.schedule}</span>
                              </p> :  null}
                        </div>
                  </div>
              </div>
          </div>
      </div>
    );
  }
}
