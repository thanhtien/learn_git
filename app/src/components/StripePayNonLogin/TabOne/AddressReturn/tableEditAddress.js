/* @flow */

import React, { Component } from 'react';



import FormAdd from './form';
import Default from './defaultAddress';



class AddressReturnEdit extends Component {



  constructor(props) {
    super(props);
    this.state = {
      defaultData:null
    };
  }


  openList() {
    this.props.openList();
  }
  openAdd() {
    this.props.openAdd();
  }

  _renderFormOrDefault(addressList) {
    if (addressList.length === 0) {
      return <FormAdd></FormAdd>
    }else {

      for (var i = 0; i < addressList.length; i++) {
        if (addressList[i].chosen_default === "1") {
          return <Default data={addressList[i]}></Default>
        }
      }

    }
  }

  render() {
    const {  addressList } = this.props;
    return (
      <div>

        <div className="main">
          {
            addressList.length > 0 ?
            <ul className="list-button">
              <li onClick={ () => this.openAdd() }>
                <span>
                  <i style={{color: '#E2E2E2'}} className="icono-plusCircle"></i>
                  <span>住所を追加する</span>
                </span>
              </li>
              <li onClick={ () => this.openList() } >
                <span>
                  <i style={{color: '#E2E2E2'}} className="icono-list"></i>
                  <span>住所録</span>
                </span>
              </li>

            </ul> : <ul className="list-button"><li></li></ul>
          }


          <div id="default-address" style={{paddingBottom:'10px'}}>
            {
              addressList ?
              this._renderFormOrDefault(addressList) : null
            }
          </div>




        </div>

      </div>
    );
  }
}



export default AddressReturnEdit
