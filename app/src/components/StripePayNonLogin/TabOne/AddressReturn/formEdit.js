/* @flow */

import React, { Component } from 'react';
import postal_code  from 'japan-postal-code';
import { connect } from 'react-redux'
import * as actions from '../../../../actions/editSystem';
import { Field, reduxForm , change , reset  } from 'redux-form';
import Loading from '../../../common/loading';
import renderFieldInput from '../../../common/renderFieldInput';
import validate from './validate';

class Form extends Component {

  constructor(props) {
    super(props);
    this.state = {
      loadingGoogle:false,
      checkbox:false
    };
  }

  handleFormSubmit(data){
    const {close , closeForm} = this.props;
    this.props.AddressReturnEditDonate(data,close , closeForm);
    document.body.style.overflow = "auto"
  }

  onChangePostCode(code){
    postal_code.get(code, (address) => {
      var addressReturn = address.prefecture+" "+address.city+" "+address.area+" "+address.street;
      this.props.dispatch(change('Address-Return-Edit', 'address_return', addressReturn));
    });

  }

  setDefaultForm() {
    this.setState({
      checkbox:!this.state.checkbox
    },function () {
      this.props.dispatch(change('Address-Return-Edit', 'setDefault', this.state.checkbox.toString()));
    })
  }

  componentDidMount() {
    const {setChoseDefault} = this.props;
    if (setChoseDefault) {
      this.setState({
        checkbox:true
      });
      this.props.dispatch(change('Address-Return-Edit', 'setDefault', this.state.checkbox.toString()));
    }
  }


  render() {
    const {handleSubmit , loading} = this.props;
    return (
      <form onSubmit={handleSubmit(this.handleFormSubmit.bind(this))}>
        <div className="form-area form-area-3">
            <p className="page-tab-label">お届け先情報・新規画面</p>
            <div className="form-box">
                <div className="form-field form-field-1">
                    <p className="form-field-name">名前 <span>必須</span></p>
                    <div className="form-field-content">
                      <Field
                        name="name_return"
                        component={renderFieldInput}
                        type="text"
                        placeholder="ユーザー名（英数字3-12文字）"
                      />
                    </div>
                </div>
                <div className="form-field form-field-2">
                    <p className="form-field-name">電話番号 <span>必須</span></p>
                    <div className="form-field-content">
                      <Field
                        name="phone"
                        component={renderFieldInput}
                        type="text"
                        placeholder="電話番号"
                      />

                    </div>
                </div>

                <div className="form-field form-field-4">
                    <p className="form-field-name">郵便番号 <span>必須</span></p>
                    <div className="form-field-content">

                      <Field
                        name="postcode"
                        component={renderFieldInput}
                        type="text"
                        placeholder="郵便番号"
                        disable={this.state.loadingGoogle}
                        onBlur={ (event) => this.onChangePostCode(event.target.value) }
                      />

                    </div>
                </div>
                <div className="form-field form-field-5">
                    <p className="form-field-name">住所 <span>必須</span></p>
                    <div className="form-field-content">

                      <Field
                        name="address_return"
                        component={renderFieldInput}
                        type="text"
                        placeholder="住所"
                        disable={this.state.loadingGoogle}
                        onBlur={ (event) => this.onChangePostCode(event.target.value) }
                      />

                    </div>
                </div>

                <div className="form-field form-field-5">
                    <p className="form-field-name">メールアドレス  <span>必須</span></p>
                    <div className="form-field-content">

                      <Field
                        name="email"
                        component={renderFieldInput}
                        type="text"
                        placeholder="メールアドレス"
                      />

                    </div>
                </div>

                <div className="form-field form-field-5">
                    <p className="form-field-name">デフォルトにする </p>
                    <div className="form-field-content checkbox">

                      <span  onClick={ ( ) => this.setDefaultForm() } className={ this.state.checkbox ? 'fake-chekcbox active' : 'fake-chekcbox' }>
                        デフォルトにする
                      </span>

                        <Field
                          name="setDefault"
                          component={renderFieldInput}
                          type="hidden"
                        />

                    </div>
                </div>


            </div>
        </div>

        <div className="submit-box">
          <span style={{cursor:"pointer"}} className="button-submit form-edit" onClick={ () => this.props.closeForm() }>一覧へ</span>
          <input disabled={this.state.loadingGoogle} type="submit" name="commit" value="更新" data-disable-with="更新"/>
        </div>
        {
          loading ? <Loading></Loading> : null
        }
    </form>
    );
  }
}
const afterSubmit = (result, dispatch) => {
  return (
    dispatch(reset('Address-Return-Edit'))
  )
};
Form = reduxForm({
  form: 'Address-Return-Edit',
  onSubmitSuccess:afterSubmit,
  validate,
  destroyOnUnmount: false,

})(Form)
const mapStateToProps = (state) => {
  return {
    loading:state.common.loading
  }
}

// You have to connect() to any reducers that you wish to connect to yourself
Form = connect(mapStateToProps, actions )(Form);


export default Form;
