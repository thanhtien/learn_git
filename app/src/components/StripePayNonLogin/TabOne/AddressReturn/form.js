/* @flow */

import React, { Component } from 'react';
import postal_code  from 'japan-postal-code';
import { connect } from 'react-redux'
import * as actions from '../../../../actions/editSystem';
import { Field, reduxForm , change , reset  } from 'redux-form';
import Loading from '../../../common/loading';
import renderFieldInput from '../../../common/renderFieldInput';
import validate from './validate';

class FormAdd extends Component {

  constructor(props) {
    super(props);
    this.state = {
      loadingGoogle:false,
      checkbox:false
    };
  }

  handleFormSubmit(data){
    this.setState({
      checkbox:false
    })
    this.props.AddressReturnDonate(data);
    document.body.style.overflow = "auto"
  }

  onChangePostCode(code){
    postal_code.get(code, (address) => {
      var addressReturn = address.prefecture+" "+address.city+" "+address.area+" "+address.street;
      this.props.dispatch(change('Address-Return', 'address_return', addressReturn));
    });

  }

  setDefaultForm() {
    this.setState({
      checkbox:!this.state.checkbox
    },function () {
      this.props.dispatch(change('Address-Return', 'setDefault', this.state.checkbox.toString()));
    })
  }




  render() {
    const {handleSubmit , loading} = this.props;

    return (

      <form onSubmit={handleSubmit(this.handleFormSubmit.bind(this))}>
        <div className="form-area form-area-3">
            <p className="page-tab-label">お届け先情報</p>
            <div className="form-box">
                <div className="form-field form-field-1">
                    <p className="form-field-name">名前 <span>必須</span></p>
                    <div className="form-field-content">
                      <Field
                        name="name_return"
                        component={renderFieldInput}
                        type="text"
                        placeholder="ユーザー名（英数字3-12文字）"
                      />

                    </div>
                </div>
                <div className="form-field form-field-2">
                    <p className="form-field-name">電話番号 <span>必須</span></p>
                    <div className="form-field-content">
                      <Field
                        name="phone"
                        component={renderFieldInput}
                        type="text"
                        placeholder="電話番号"
                      />

                    </div>
                </div>

                <div className="form-field form-field-4">
                    <p className="form-field-name">郵便番号 <span>必須</span></p>
                    <div className="form-field-content">

                      <Field
                        name="postcode"
                        component={renderFieldInput}
                        type="text"
                        placeholder="郵便番号"
                        disable={this.state.loadingGoogle}
                        onBlur={ (event) => this.onChangePostCode(event.target.value) }
                      />

                    </div>
                </div>
                <div className="form-field form-field-5">
                    <p className="form-field-name">住所 <span>必須</span></p>
                    <div className="form-field-content">

                      <Field
                        name="address_return"
                        component={renderFieldInput}
                        type="text"
                        placeholder="住所"
                        disable={this.state.loadingGoogle}
                        onBlur={ (event) => this.onChangePostCode(event.target.value) }
                      />

                    </div>
                </div>

                <div className="form-field form-field-5">
                    <p className="form-field-name">メールアドレス  <span>必須</span></p>
                    <div className="form-field-content">

                      <Field
                        name="email"
                        component={renderFieldInput}
                        type="text"
                        placeholder="メールアドレス"
                      />

                    </div>
                </div>

                <div className="form-field form-field-5">
                    <p className="form-field-name">デフォルトにする </p>
                    <div className="form-field-content checkbox">

                      <span  onClick={ ( ) => this.setDefaultForm() } className={ this.state.checkbox ? 'fake-chekcbox active' : 'fake-chekcbox' }>
                        デフォルトにする
                      </span>

                        <Field
                          name="setDefault"
                          component={renderFieldInput}
                          type="hidden"
                        />

                    </div>
                </div>





            </div>
        </div>



        <div className="submit-box">
          <input disabled={this.state.loadingGoogle} type="submit" name="commit" value="登録" data-disable-with="登録"/>
        </div>
        {
          loading ? <Loading></Loading> : null
        }
    </form>
    );
  }
}
const afterSubmit = (result, dispatch) => {
  return (
    dispatch(reset('Address-Return'))
  )
};
FormAdd = reduxForm({
  form: 'Address-Return',
  onSubmitSuccess:afterSubmit,
  validate,
  destroyOnUnmount: false,

})(FormAdd)

const mapStateToProps = (state) => {
  return {
    data: state.uploadImage.data,
    loading:state.common.loading,
    statusAPI:state.common.statusAPI

  }
}

// You have to connect() to any reducers that you wish to connect to yourself
FormAdd = connect(mapStateToProps, actions )(FormAdd);


export default FormAdd;
