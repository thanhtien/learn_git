/* @flow */
import React, { Component } from 'react';
import ProjectReturn from './ProjectReturn';
import History from '../../../history.js';
import { connect } from 'react-redux';

var CONFIG = require('../../../config/common');

if (process.env.NODE_ENV === "development") {
  CONFIG = CONFIG.CONFIG.DEV;
}else {
  CONFIG = CONFIG.CONFIG.PRODUCT;
}

class TabOne extends Component {

  constructor(props) {
    super(props);
    this.state = {
      typeTab:"stripe",
    };
  }

  //Go To Back
  goBack() {
    History.goBack();
  }



  ChangeStep(step){
    this.props.ChangeStep(step);
  }

  _renderButtonStripe() {
    return  (
      <button onClick={ () => this.ChangeStep(2) } className="btn-re">次へ≫</button>
    )
  }

  _renderButton(typeTab) {
    if (typeTab === 'stripe') {
      return this._renderButtonStripe()
    }
  }

  //Render
  render() {
    const {typeTab} = this.state;

    const {data} = this.props;

    return (
      <div className="page-content active" id="tab1">
          <div className="wraper">
              <p className="back-link"><span style={{cursor:'pointer'}} onClick={()=>this.goBack()}>≪　前のページへ戻る</span></p>
              <div className="BackerAddForm">


                <ProjectReturn data={data} ></ProjectReturn>
                {/* Project Return */}

                <div className="address_return">
                  <h1>支払方法を選択してください。</h1>
                  <div className={'tab-change border-none'} >
                    <span
                      className={ typeTab === 'stripe' ? 'tab-name active': 'tab-name' } >
                      <p>
                        <img className="img-ticket" src={CONFIG.MAIN_URL+"/img/project-detail/ico-checked.svg"} alt="backer" />
                      </p>
                      クレジットカード
                    </span>

                  </div>

                  <div className="clear-fix"></div>
                </div>

                <div className="cover-tab-step-one">

                  <div  className={ typeTab  === 'stripe' ?  'pannel-white fix-margin animated fadeIn' : 'pannel-white fix-margin hidden-div'}>
                    <h4 className="pannel-title">支払方法：クレジットカード</h4>
                    <div className="brand-list">
                      <ul>
                          <li><span ><img src={CONFIG.MAIN_URL+"/img/project-detail/brand-1.png"} alt=""/></span></li>
                          <li><span ><img src={CONFIG.MAIN_URL+"/img/project-detail/brand-2.png"} alt=""/></span></li>
                          <li><span ><img src={CONFIG.MAIN_URL+"/img/project-detail/brand-3.png"} alt=""/></span></li>
                          <li><span ><img src={CONFIG.MAIN_URL+"/img/project-detail/brand-5.png"} alt=""/></span></li>

                      </ul>
                    </div>
                  </div>
                </div>

                {
                  this._renderButton(typeTab)
                }
              </div>
          </div>
      </div>
    );
  }
}




const mapStateToProps = (state) => {
  return {
    typePay: state.Donate.typePay
  }
}
export default connect(mapStateToProps)(TabOne);
