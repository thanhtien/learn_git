/* @flow */

import React, { Component } from 'react';

import Loading from '../common/loading';
import LoadingScroll from '../common/loadingScroll';
import * as actions from '../../actions/TopPage';
import { connect } from 'react-redux';
import ReactPaginate from 'react-paginate';
import { Link } from 'react-router-dom';
import { LazyLoadImage } from 'react-lazy-load-image-component';
import Breadcrumbs from '../Breadcrumbs';
import FormatFunc from '../common/FormatFunc';
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faChevronLeft, faChevronRight, faClock} from "@fortawesome/free-solid-svg-icons";

class NewsList extends Component {


  componentDidMount() {

      let searchText = new URLSearchParams(this.props.location.search).get('searchText');
   // if (!this.props.newsList) {
        if(searchText){

            this.props.NewslistSearch(0,searchText);
        }else{
            this.props.Newslist(0);
        }

   // }

    document.title = "最新の活動・ニュース｜KAKUSEIDA";
  }

  handlePageClick = (data) => {
      let searchText = new URLSearchParams(this.props.location.search).get('searchText');
    let selected = data.selected;
   // this.props.Newslist(selected);
      if(searchText){

          this.props.NewslistSearch(selected,searchText);
      }else{
          this.props.Newslist(selected);
      }
  };

  renderDes(content) {
    return content.replace(/<\/?[^>]+(>|$)/g, "").substring(0, 200);
  }

  renderView(newsList) {
    if (newsList.data.length !== 0) {
      return(
        <div  className="wrap-content-inner">


          {
            <div className="l-list">
              {
                newsList.data.map( (item , i) => {

                  return (
                    <div className="each-item" key={i} >
                      <Link className="new-detail" style={{color:'initial'}} to={`/news-list/new-detail/${item.id}`}>
                        <div className="thumnail">
                          <LazyLoadImage src={item.thumnail} effect="blur" alt="thum"></LazyLoadImage>
                        </div>
                        <div className="text-right-only">
                          <p className="time"><FormatFunc date={item.created} ></FormatFunc></p>
                          <p className="title">{item.title}</p>
                          <p className="des">
                            {
                              this.renderDes(item.content)
                            }

                          </p>

                        </div>
                      </Link>
                      <Link className="go-more" to={`/news-list/new-detail/${item.id}`}>もっと見る</Link>

                    </div>
                  )
                } )
              }
            </div>
          }

          <div className="cover-paginate">
            {
              this.props.loading?<div className="loading-io"><LoadingScroll></LoadingScroll></div>:null
            }
            <ReactPaginate
              previousLabel={<FontAwesomeIcon icon={faChevronLeft}/>}
              nextLabel={<FontAwesomeIcon icon={faChevronRight}/>}
              breakLabel={<span>...</span>}
              breakClassName={"break-me"}
              pageCount={newsList.page_count}
              marginPagesDisplayed={2}
              pageRangeDisplayed={2}
              onPageChange={this.handlePageClick}
              containerClassName={"pagination"}
              subContainerClassName={"pages pagination"}
              activeClassName={"active"} />
          </div>
          <div className="clear-fix"></div>
        </div>
      )
    }else {
      return(
        <div className="mar-center">
          <p
            id="notFound"
            className="blank slug-blank"
          >
            <span>最新の活動・ニュース がまだありません。</span>
          </p>
          <Link style={{marginBottom:0}}  className="btn-effect" to={"/"}>トップページへ</Link>
          <div style={{height:"40px"}} ></div>
        </div>
      )
    }
  }

  render() {
    const {newsList} = this.props;
    var linkBreacrm = [
      {last:true,label:"最新の活動・ニュース",link:null}
    ]
    return (
      <div className="about_box">
        <div className="banner-css news-list-banner">
          <div className="wrap-content-inner">
            <p className="title-white">
              最新の活動・ニュース
            </p>
          </div>
        </div>
        <div className="wrap-content-inner"><div className="cover-Breadcrumbs"><Breadcrumbs linkBreacrm={linkBreacrm} ></Breadcrumbs></div></div>

        {
          newsList ?
            this.renderView(newsList)
          : <Loading></Loading>
        }

      </div>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    newsList: state.topPage.newsList,
    loading:state.common.loading
  }
}

export default connect(mapStateToProps, actions)(NewsList);
