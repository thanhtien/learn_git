/*global FB*/

import React, { Component } from 'react';
var CONFIG = require('../../../../config/common');

if (process.env.NODE_ENV === "development") {
  CONFIG = CONFIG.CONFIG.DEV;
}else {
  CONFIG = CONFIG.CONFIG.PRODUCT;
}
export default class Social extends Component {

  componentDidMount() {
    /**
     * [FACEBOOK INIT]
     * @return {[type]} [description]
     */
     window.fbAsyncInit = function() {
       FB.init({
         appId      : CONFIG.FB_KEY,
         xfbml      : true,
         version    : 'v2.1'
       });
     };

      var event = document.createEvent('Event');

      event.initEvent('fb_init', true, true);

      document.addEventListener('fb_init', function (e) {
        if (window.FB) {
          FB.XFBML.parse()
        }
      }, false);

      document.dispatchEvent(event);

    //Re Render Button
    if (window.twttr.widgets) {
      const {shareLink} = this.props;
      window.twttr.widgets.createShareButton(
        shareLink,
        document.getElementById('container-news'),
        {
          size: 'small',
          showScreenName: "false",
          showCount: false,
          lang:"ja",
          hashtags:"example,demo",
          text:"custom share text",
          related:"twitterapi,twitter",
          via:"twitterdev"
        }
      );
    }


  }
  render() {
    const {shareLink} = this.props;

    return (
      <ul className="sns-share">
          <li>
            <div className="fb-like"
             data-href={shareLink}
             data-layout="button"
             data-action="like"
             data-show-faces="true">
           </div>
          </li>
          <li>
            <div className="fb-share-button"
              data-href={shareLink}
              data-layout="button_count">
            </div>
          </li>
          <li id="container-news">
            <a className="twitter-share-button"
              href={shareLink}
              data-size="small"
              data-text="custom share text"
              data-url={shareLink}
              data-hashtags="example,demo"
              lang="ja"
              data-via="twitterdev"
              data-related="twitterapi,twitter">
            </a>
          </li>
      </ul>
    );
  }
}
