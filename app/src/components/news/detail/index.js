/* @flow */

import React, { Component } from 'react';
import Banner from './banner';
import * as actions from '../../../actions/TopPage';
import { connect } from 'react-redux';
import Loading from '../../common/loading';
import { Link } from 'react-router-dom';
import Social from './social';
import Breadcrumbs from '../../Breadcrumbs';
import FormatFunc from '../../common/FormatFunc';
import {LazyLoadImage} from "react-lazy-load-image-component";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faFacebookSquare, faTwitter} from "@fortawesome/fontawesome-free-brands";
import {FacebookShareButton, TwitterShareButton, LineShareButton} from 'react-share';
import History from "../../../history";
import $ from 'jquery';
import LinesEllipsis from "react-lines-ellipsis";
var CONFIG = require('../../../config/common');
if (process.env.NODE_ENV === "development") {
  CONFIG = CONFIG.CONFIG.DEV;
}else {
  CONFIG = CONFIG.CONFIG.PRODUCT;
}

class NewDetail extends Component {
    constructor(props) {
        // Required step: always call the parent class' constructor
        super(props);

        // Set the state directly. Use props if necessary.
        this.state={searchText:''};
    }
  componentDidMount() {
    const {id} = this.props.match.params;
    this.props.NewsDetail(id);
    this.props.NewsDetailMore(id);
    this.props.NewsListMonth();

  }
  htmlReturn(data) {
    return{
      __html:data
    }
  }
  backTo() {
    this.props.history.goBack();
  }
  renderDes(content) {
    return content.replace(/<\/?[^>]+(>|$)/g, "").substring(0, 200);
  }
  getNewsListMonth=(newsList,showDefault)=>{
      if(newsList)
        return(
            <ul className="l-list-normal1">
              {
                newsList.map( (item , i) => {
                  let subList = item.list;
                 console.log(item.YearMonth);
                  console.log('showDefault'+showDefault);
                  let isDefault = item.YearMonth ==showDefault;
                  let hideThis = {'display':'none'};
                  if(isDefault){
                    hideThis = {};
                  }
                  return(
                      <li key={i}>
                        {!isDefault && <a className="l-list-normal-link1" href="javascript:;"  onClick={(e)=>{this.showChild(item.YearMonth)}}><div className={"childIconaRow"}  id={item.YearMonth+'icon'}>▶</div> {item.CreatedMonth} ({item.count})  </a>}
                        {isDefault && <a className="l-list-normal-link1" href="javascript:;"  onClick={(e)=>{this.showChild(item.YearMonth)}}> <div className={"childIconaRow1"}  id={item.YearMonth+'icon'}><p  className={"childIconaRow1Child"} >▼</p></div>  {item.CreatedMonth} ({item.count})</a>}

                          <ul className={"childTitle"}   style={hideThis} id={item.YearMonth}>
                            {subList.map( (item1 , ii) => {

                            return(
                            <li key={ii}>
                            <a className={""} href={"/news-list/new-detail/"+item1.id}>
                              <LinesEllipsis
                                  text={item1.title}
                                  maxLine='1'
                                  ellipsis='...'
                                  trimRight
                                  basedOn='letters'
                                  className="card-content-title truncated"
                              />
                            </a>
                            </li>
                            )
                          })}
                          </ul>
                      </li>
                  )
                })
              }

            </ul>
        );
    return (
        <div></div>);
  }
  getPostList2=(newsList)=>{
    if(newsList)
    return (
        <div>{
        newsList.data.map( (item , i) => {
          return(
            <div className="l-list" key={i}>
              <div className="each-item l-list-item">
                <Link className="new-detail card-list" to={`/news-list/new-detail/${item.id}`}>
                  <div>
                    <div className="thumnail">
                      <LazyLoadImage src={item.thumnail} effect="blur" alt="thum"></LazyLoadImage>
                    </div>
                  </div>
                  <div className="text-right-only card-list-content">
                    <p className="title">{item.title}</p>
                    <p className="des">
                      {
                        this.renderDes(item.content)
                      }
                    </p>
                  </div>
                </Link>
              </div>
            </div>
          )
        })}
        </div>

    );
    return (
        <div></div>);
  }
  keyPressed(event) {
    if (event.key === "Enter") {
     //searchText /news-list/page=1?searchText=500
      let myK = this.state.searchText;///news-list/page=1
        History.push('/news-list/page=1?searchText='+myK+'');


    }
  }
  handleChange=(ev)=>{
    let myKey = ev.target.name;
    let myValue = ev.target.name;
    this.setState({[myKey]:ev.target.value});

  }
  showChild=(idChild)=>{
    //this.setState({[idChild]:true});
    if ($('#'+idChild).css('display') == 'none') {
      $('#'+idChild).show();
      $('#'+idChild+"icon").html('<p style="font-size:15px;margin-left: -2px;" >▼</p> '); //20190101icon


    }else{
      $('#'+idChild).hide();
      $('#'+idChild+"icon").html('<p style="font-size:15px;" >▶</p> '); //20190101icon
    }



  }
  render() {
    const {new_detail,new_detail_more,news_list_month} = this.props;

    const {id} = this.props.match.params;

    const shareLink = CONFIG.MAIN_URL+'new-detail/'+id;
    var linkBreacrm;

    if (new_detail) {

      var label;

    

      if (new_detail.title > 10) {
        label = new_detail.title.substring(0,10) + '...';
      }else {
        label = new_detail.title.substring(0,10);

      }
      document.title = new_detail.title+"｜KAKUSEIDA";
      var showDefault = new_detail.created .substring(0, 10);//2019-08-22
      linkBreacrm = [
        {last:false,label:"最新の活動・ニュース",link:'/news-list/page=1'},
        {last:true,label:label,link:null}
      ]
    }

    return (
      <div className="wrap-content-inner">
        {
          new_detail ?
          <div className="l-grid-flex gutter-30-sm gutter-40-md gutter-60-lg pt-50 pt-100-md">
            <div className="about_box post-box">
              <div className="post-heading l-grid-flex-md">
                <p className="title-page">{new_detail.title}</p>
                <p className="post-time"><FormatFunc date={new_detail.created} ></FormatFunc></p>
              </div>
              <div className="des-thumnail">
                <LazyLoadImage src={new_detail.thumnail} effect="blur" alt="thum"></LazyLoadImage>
              </div>

              {
                new_detail.content ?
                <div className="post-content">
                  <div dangerouslySetInnerHTML={this.htmlReturn(new_detail.content)} ></div>
                  <div className="post-social">
                    <FacebookShareButton url={shareLink} className="btn-social">
                      <span className="btn-share btn-share-fb"><FontAwesomeIcon icon={faFacebookSquare}/>シェア</span>
                    </FacebookShareButton>
                    <TwitterShareButton url={shareLink} className="btn-social">
                      <span className="btn-share btn-share-twitter"><FontAwesomeIcon icon={faTwitter}/>ツイート</span>
                    </TwitterShareButton>
                    <LineShareButton url={shareLink} className="btn-social">
                      <span className="btn-share btn-share-line">LINEで送る</span>
                    </LineShareButton>
                    <div className="btn-social">
                      <span className="btn-share btn-share-embedded">埋め込み</span>
                    </div>
                  </div>
                </div> : <p style={{textAlign:'center', border: "3px dotted #0071ba",padding:"10px"}}>コンテンツがまだありません。</p>
              }

              <div className="post-list">
                <p className="post-list-title">関連記事</p>

              {this.getPostList2(new_detail_more)}

              </div>
              {/* .WYSIWYG */}
              <div className="direction l-tb-flex">
                {new_detail_more && new_detail_more.isPre==1 && <a className="direction-link" href={'/news-list/new-detail/'+new_detail_more.pre_id}>前のページへ</a>}
                {new_detail_more && new_detail_more.isPre==0 && <p className="noLink" >前のページへ </p> }
                {new_detail_more && new_detail_more.isNext==1 && <a className="direction-link" href={'/news-list/new-detail/'+new_detail_more.next_id}>次のページへ</a>}
                {new_detail_more && new_detail_more.isNext==0 && <p className="noLink"  >次のページへ </p>}
              </div>
            </div>
            <div className="post-sidebar">
              <div className="box-company">
                <div className="wrap-intro"><img src={"/img/common/kakuseida.png"} alt="Kakuseida"/></div>
                <div className="box-company-text">
                  <p>プロジェクトを始めたい方、<br/>支援したい方に情報を発信中！</p>
                  <p>イベント情報やリリース情報などもお届けします。<br/>FB・TW　でも発信中！</p>
                </div>
                <div className="post-social">
                  <FacebookShareButton url={shareLink} className="btn-social">
                    <span className="btn-share btn-share-fb"><FontAwesomeIcon icon={faFacebookSquare}/>シェア</span>
                  </FacebookShareButton>
                  <TwitterShareButton url={shareLink} className="btn-social">
                    <span className="btn-share btn-share-twitter"><FontAwesomeIcon icon={faTwitter}/>ツイート</span>
                  </TwitterShareButton>
                </div>
              </div>
              <div className="box">
                <p className="box-title">キーワード検索</p>
                <div className="form-search" >
                    <input type={"input"} className={"search-box"} name={"searchText"}  onChange={(e)=>this.handleChange(e)}   onKeyPress={(e)=>this.keyPressed(e)} placeholder={"キーワードを入力"} />
                </div>
              </div>
              <div className="box">
                <p className="box-title">カテゴリ</p>
                <div className="box-tag">
                    <a className="tag" href="#">展示会</a>
                    <a className="tag" href="#">集まり</a>
                    <a className="tag" href="#">NEWS</a>
                    <a className="tag" href="#">PROJECTのアドバイス</a>
                    <a className="tag" href="#">商品</a>
                    <a className="tag" href="#">流通</a>
                    <a className="tag" href="#">PROJECTインタビュー</a>
                    <a className="tag" href="#">KAKUSEIDAのサービス</a>
                </div>
              </div>
              <div className="box">
                <p className="box-title">アーカイブ</p>
                {this.getNewsListMonth(news_list_month,showDefault)}

              </div>
            </div>
          </div> : <Loading></Loading>
        }
      </div>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    new_detail: state.common.new_detail,
    new_detail_more: state.common.new_detail_more,
    news_list_month: state.common.news_list_month,
    loading:state.common.loading
  }
}

export default connect(mapStateToProps, actions)(NewDetail);
