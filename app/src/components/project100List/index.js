/* @flow */

import React, { Component } from 'react';
import ProjectList from '../projectList';
import Loading from '../common/loading';
import LoadingScroll from '../common/loadingScroll';
import * as actions from '../../actions/TopPage';
import { connect } from 'react-redux';
import ReactPaginate from 'react-paginate';
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faChevronLeft, faChevronRight} from "@fortawesome/free-solid-svg-icons";

class ProjectHistory extends Component {

  componentDidMount() {
    window.location.href ='/project-from-type/from_project_100/page=1';
    const {page} = this.props.match.params;
    var PageString = page.replace('page=', '');
    if (!this.props.Project100) {
      this.props.project100List(Number(PageString)-1);
    }
    document.title = "達成したプロジェクト｜KAKUSEIDA";
  }

  handlePageClick = (data) => {
    let selected = data.selected;
    this.props.project100List(selected);
  };
  _renderView(Project100) {
    const linkBreacrm = [
      {last:true,label:"達成したプロジェクト",link:null}
    ]
    if (Project100.data.length !== 0) {
      return(
        <div>
          <ProjectList linkBreacrm={linkBreacrm} data={Project100.data} title={"達成したプロジェクト"}/>
          <div className="cover-paginate">
            {
              this.props.loading?<div className="loading-io"><LoadingScroll></LoadingScroll></div>:null
            }
            <ReactPaginate
              previousLabel={<FontAwesomeIcon icon={faChevronLeft}/>}
              nextLabel={<FontAwesomeIcon icon={faChevronRight}/>}
              breakLabel={<span>...</span>}
              breakClassName={"break-me"}
              pageCount={this.props.Project100.page_count}
              marginPagesDisplayed={2}
              pageRangeDisplayed={2}
              forcePage={Number(this.props.pageload)}
              onPageChange={this.handlePageClick}
              containerClassName={"pagination"}
              subContainerClassName={"pages pagination"}
              activeClassName={"active"} />
          </div>
          <div className="clear-fix"></div>
        </div>
      )
    }else {
      return (
        <div>
          <ProjectList linkBreacrm={linkBreacrm} data={this.props.Project100.data} title={"達成したプロジェクト"}/>
        </div>
      )
    }
  }
  render() {
    const {Project100} = this.props;

    return (
      <div>
        <div>
          {
            Project100 ?
              this._renderView(Project100)
            : <Loading></Loading>
          }

        </div>
      </div>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    Project100: state.topPage.Project100.data,
    loading:state.common.loading,
    pageload:state.topPage.Project100.page
  }
}

export default connect(mapStateToProps, actions)(ProjectHistory);
