/* @flow */

import React, { Component } from 'react';
import moment from 'moment';
import NumberFormat from 'react-number-format';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTag , faUser } from '@fortawesome/free-solid-svg-icons';
import { Link } from 'react-router-dom';
import LinesEllipsis from 'react-lines-ellipsis'
import { LazyLoadImage } from 'react-lazy-load-image-component';
import History from '../../history';
export default class CardProject extends Component {
  _renderProjectEndDateTitle(data) {
    if (data) {
      if (data.format_collection_end_date) {
        if ('status' in data.format_collection_end_date) {
          return '終了';
        }
        if (data.format_collection_end_date.date > 0) {
          return data.format_collection_end_date.date + "日";
        }else {
          return data.format_collection_end_date.hour+"時"+data.format_collection_end_date.minutes+"分";
        }
      }
    }
  }

  _renderProjectEndDate(data) {
    if (data) {
      if (data.format_collection_end_date) {
        if ('status' in data.format_collection_end_date) {
          return '終了';
        }
        if (data.format_collection_end_date.date > 0) {
          return <span>{data.format_collection_end_date.date} 日</span>;
        }else {
          return <span>{data.format_collection_end_date.hour}時{data.format_collection_end_date.minutes}分</span>;
        }
      }
    }
  }
  componentDidMount() {
    window.addEventListener("resize", this.updateDimensions.bind(this));
    if (this.img) {
      this.setState({
        heightImage:(Math.round((this.img.offsetWidth*2)/3)).toString()
      })
    }
  }

  updateDimensions() {
    if (this.img) {
      this.setState({
        heightImage:(Math.round((this.img.offsetWidth*2)/3)).toString()
      })
    }
  }
  /**
   * [_renderLabel Label Project]
   * @param  {[type]} item [data project]
   * @return {[type]}      [label name]
   */
  _renderLabel(item,typeProject) {

    if (typeProject === '0') {
      var day = moment(item.collection_end_date).diff(new Date(), 'days');
      if (day < 0) {
        return (
          <p style={{background:"#82D9EE"}} className="label-no-active">
            終了
          </p>
        )
      }

      if (item.active === 'no' && item.opened === 'no') {
        return(
          <div>
            <p className="label-no-active">
              下書き
            </p>
            <span onClick={ () => this.deleteProject(item) } className="trash-function">
              <i className="icono-crossCircle"></i>
            </span>
          </div>
        )
      } else if((item.active === 'no' && item.opened === 'yes')) {
        return(
          <p style={{background:'#0e6eb8'}} className="label-no-active">
            申請中
          </p>
        )
      }
      else if((item.active === 'cls' && item.opened === 'yes')) {
        return(
          <p style={{background:'#000',color:"#fff"}} className="label-no-active">
            途中終了
          </p>
        )
      } else {
        return (
          <p className="label-active">
            公開中
          </p>
        )
      }
    }
    else {

      if(item.active === 'yes'){
        if(item.status_fanclub === 'progress'){

          return (
            <p className="label-active">
              公開中
            </p>
          )
        }else if(item.status_fanclub === 'wait'){

          return (
            <p className="label-active label-wait">
              解散承認待ち
            </p>
          )
        }else if(item.status_fanclub === 'stop'){
          return (
            <p style={{background:"#82D9EE"}} className="label-no-active">
              解散済み
            </p>
          )
        }

      }
      else if(item.active ==='no' && item.opened === 'no'){
          return(
            <div>
              <p className="label-no-active">
                下書き
              </p>
              <span onClick={ () => this.deleteProject(item) } className="trash-function">
                <i className="icono-crossCircle"></i>
              </span>
            </div>
          )
      }
      else if(item.active ==='no' && item.opened === 'yes'){
        return(
          <p style={{background:'#0e6eb8'}} className="label-no-active">
            申請中
          </p>
        )
      }
      else if(item.active ==='blk'){
        return (
          <p style={{background:'#000',color:"#fff"}} className="label-no-active">
            解散済み
          </p>
        )
      }else if(item.active ==='cls'){
        return (
          <p style={{background:'#000',color:"#fff"}} className="label-no-active">
            解散済み
          </p>
        )
      }
    }


  }



  /**
   * [_renderLink Link Project]
   * @param  {[type]} item [data project]
   * @return {[type]}      [Link Url]
   */
   _renderLink(item) {

     var day = moment(item.collection_end_date).diff(new Date(), 'days');
     if (day < 0) {
       return (
         <Link to={"/my-page/statistic/"+item.id}>
           <LazyLoadImage
            alt={"thumbnail"}
            effect="blur"
            src={item.thumbnail}
            className="card-intro" />
         </Link>
       )
     }

     if (item.active === 'no' && item.opened === 'no') {
       return(
         <a href={"/my-page/edit-project/"+item.id}>
           <LazyLoadImage
            alt={"thumbnail"}
            effect="blur"
            src={item.thumbnail}
            className="card-intro" />
         </a>
       )
     } else if((item.active === 'no' && item.opened === 'yes')) {
       return(
         <Link to={"/my-page/project-detail/"+item.id}>
           <LazyLoadImage
            alt={"thumbnail"}
            effect="blur"
            src={item.thumbnail}
            className="card-intro" />
         </Link>
       )
     } else {
       return (
         <Link to={"/my-page/statistic/"+item.id}>
           <LazyLoadImage
            alt={"thumbnail"}
            effect="blur"
            src={item.thumbnail}
            className="card-intro" />
         </Link>
       )
     }
   }
  /**
   * [deleteProject Delete UnPulic]
   * @return {[type]} [description]
   */
  deleteProject(item) {

    if (window.confirm(`${item.project_name}というプロジェクトを削除しますか。`)) {
      this.props.deleteProject(item,this.props.pageLoad);
    }

  }
  _capitalizeFirstLetter(string) {
    if (string) {
      return string.charAt(0).toUpperCase() + string.slice(1);
    }
  }

  /**
   * [_renderLink Link Project]
   * @param  {[type]} item [data project]
   * @return {[type]}      [Link Url]
   */
   clickRibbon(item) {

     var day = moment(item.collection_end_date).diff(new Date(), 'days');
     if (day < 0) {
       History.push(`/my-page/statistic/${item.id}`);
     }

     if (item.active === 'no' && item.opened === 'no') {
       History.push("/my-page/edit-project/"+item.id);

     } else if((item.active === 'no' && item.opened === 'yes')) {
       History.push("/my-page/project-detail/"+item.id);
     } else {
       History.push("/my-page/statistic/"+item.id);
     }
   }

   _renderLinkTitle(item){
     var day = moment(item.collection_end_date).diff(new Date(), 'days');
     if (day < 0) {
       return (
         <Link to={'/my-page/statistic/'+item.id}>
           <pre>
             <LinesEllipsis
               text={item.project_name}
               maxLine='2'
               ellipsis='...'
               trimRight
               basedOn='letters'
               className="card-content-title truncated"
             />
           </pre>
         </Link>
       )
     }

     if (item.active === 'no' && item.opened === 'no') {
       return(
         <a href={"/my-page/edit-project/"+item.id}>
           <pre>
             <LinesEllipsis
               text={item.project_name}
               maxLine='2'
               ellipsis='...'
               trimRight
               basedOn='letters'
             />
           </pre>

         </a>
       )
     } else if((item.active === 'no' && item.opened === 'yes')) {
       return(
         <Link to={"/my-page/project-detail/"+item.id}>
           <pre>
             <LinesEllipsis
               text={item.project_name}
               maxLine='2'
               ellipsis='...'
               trimRight
               basedOn='letters'
             />
           </pre>

         </Link>
       )
     } else {
       return (
         <Link to={"/my-page/statistic/"+item.id}>
           <pre>
             <LinesEllipsis
               text={item.project_name}
               maxLine='2'
               ellipsis='...'
               trimRight
               basedOn='letters'
             />
           </pre>

         </Link>
       )
     }
   }
  _renderTemplate(item) {
    const numberWidth = Math.round((Number(item.collected_amount)/Number(item.goal_amount))*100);

    return(
      <div style={{position:"relative",overflow:'initial'}} className="col-6 col-sm-4 col-md-3 grid-card-item">
        <div className="card">
            <div style = {{height:this.img !== undefined ? this.state.heightImage+'px' : '0px' , overflow:'hidden'}} ref={ (img) => this.img = img} className="img">
              {
                this._renderLink(item)
              }
              {
                this._renderLabel(item,item.project_type)
              }
              {
                item.project_type === '1' ?
                <div style={{cursor:'pointer'}} onClick={ ()=>this.clickRibbon(item) } className="ribbon ribbon-top-right"><span>定期課金のプロジェクト</span></div>
                : null
              }

            </div>

            <div className="card-content">

                <div title={item.project_name} className="postTitle">
                  {
                    this._renderLinkTitle(item)
                  }
                </div>


                <div  className="rPeople l-tb-flex">
                    {
                      item.user ?
                      <span title={item.user.name} className="people truncated">
                        <span className="name-user" style={{cursor:'pointer'}}>
                          <img className="label-ico" src="/img/common/ico-user.png" alt="user"/>
                          {' '+ this._capitalizeFirstLetter(item.user.name) }</span></span> :null
                    }
                    <span title={item.category.name} className="note truncated">
                        <Link to={'/project-from-type/'+item.category.slug+'/page=1'}>
                            <img className="label-ico" src="/img/common/ico-tag.png" alt="tag"/>{' '+item.category.name}
                        </Link>
                    </span>
                </div>

                {
                  item.project_type === '1' ?
                  <div className="meter fan">

                  </div> :
                  <div className="meter">
                    <div className="countNum" style={numberWidth >=100 ? {width:"100%"} : {width:numberWidth+"%"}}>
                      <span className="numTr">{numberWidth}%</span>
                    </div>
                  </div>

                }
                <div className={item.project_type === '1' ? "rowBot l-tb-flex fan" : "rowBot l-tb-flex"} >
                    <div title={item.collected_amount} className="price tooltip">

                        <span className="small-title d-b-mb">支援総額</span>
                        <span className="number d-b-mb">
                            <NumberFormat value={item.collected_amount} displayType={'text'} thousandSeparator={true} suffix={'円'} />
                        </span>

                    </div>
                    <div title={item.now_count.now_count} className="pNumber">
                        <span className="small-title d-b-mb">支援者数</span>
                        <span className="number d-b-mb">{item.now_count.now_count}人</span>
                    </div>
                    <div title={this._renderProjectEndDateTitle(item)}  className="date">

                      <span className="small-title d-b-mb">残りの日数</span>
                      <span className="number d-b-mb">{this._renderProjectEndDate(item)}</span>
                    </div>
                </div>
            </div>
            {/* Card content */}
        </div>
      </div>
    );
  }
  render() {
    const {item} = this.props;

    return (
      this._renderTemplate(item)
    );
  }
}
