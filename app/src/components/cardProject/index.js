/* @flow */

import React, { Component } from 'react';
import NumberFormat from 'react-number-format';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTag , faUser } from '@fortawesome/free-solid-svg-icons';
import { Link } from 'react-router-dom';
import LinesEllipsis from 'react-lines-ellipsis';
import { LazyLoadImage } from 'react-lazy-load-image-component';
import {ParseUrl} from '../common/parseImage'

export default class CardProject extends Component {


  _renderProjectEndDateTitle(data) {


    if (data) {
      if (data.format_collection_end_date) {
        if (data.format_collection_end_date.status) {
          return '終了';
        }
        if (data.format_collection_end_date.date > 0) {
          return data.format_collection_end_date.date + "日";
        }else {
          return data.format_collection_end_date.hour+"時"+data.format_collection_end_date.minutes+"分";
        }
      }
    }
  }

  _renderProjectEndDate(data) {

    if (data) {
      if (data.format_collection_end_date) {
        if (data.format_collection_end_date.status) {
          return '終了';
        }
        if (data.format_collection_end_date.date > 0) {
          return <span>{data.format_collection_end_date.date} 日</span>;
        }else {
          return <span>{data.format_collection_end_date.hour}時{data.format_collection_end_date.minutes}分</span>;
        }
      }
      return <span>&nbsp;</span>;
    }
  }
  componentDidMount() {
    window.addEventListener("resize", this.updateDimensions.bind(this));
    if (this.img) {
      this.setState({
        heightImage:(Math.round((this.img.offsetWidth*2)/3)).toString()
      })
    }
  }

  updateDimensions() {
    if (this.img) {
      this.setState({
        heightImage:(Math.round((this.img.offsetWidth*2)/3)).toString()
      })
    }
  }

  _capitalizeFirstLetter(string) {
    if (string) {
      return string.charAt(0).toUpperCase() + string.slice(1);
    }
  }


  _renderTemplate(item) {
    const numberWidth = Math.round((Number(item.collected_amount)/Number(item.goal_amount))*100);

    return(
      <div className="col-6 col-md-4 col-lg-3 grid-item">
        <div className="card">
        <p style = {{height:this.img !== undefined ?  this.state.heightImage+'px' : '0px' , overflow:'hidden'}} ref={ (img) => this.img = img} className="img">
          <Link to={'/project-detail/'+item.id}>
            <LazyLoadImage
              alt={"thumbnail"}
              effect="blur"
              src={ParseUrl('medium',item.thumbnail)} />
          </Link>
        </p>
        <div className="card-content">

            <div title={item.project_name} className="postTitle">
              <Link to={'/project-detail/'+item.id}>
                  <LinesEllipsis
                    text={item.project_name}
                    maxLine='1'
                    ellipsis='...'
                    trimRight
                    basedOn='letters'
                    className="card-content-title truncated"
                  />

              </Link>
            </div>


            <div className="rPeople l-tb-flex">
                {
                  item.user ?
                  <span title={item.user.name} className="people truncated">
                    <span className="name-user"  style={{cursor:'pointer'}}>
                      <img className="label-ico" src="/img/common/ico-user.png" alt="user"/>
                    {' '+ this._capitalizeFirstLetter(item.user.name) }</span></span> :null
                }
                <span title={item.category.name} className="note truncated">
                    <Link to={'/project-from-type/'+item.category.slug+'/page=1'}>
                        <img className="label-ico" src="/img/common/ico-tag.png" alt="tag"/>{' '+item.category.name}
                    </Link>
                </span>
            </div>


            {
              item.project_type === '1' ?
              <div className="meter fan">

              </div> :
              <div className="meter">
                <div className="countNum" style={numberWidth >=100 ? {width:"100%"} : {width:numberWidth+"%"}}>
                  <span className="numTr">{numberWidth}%</span>
                </div>
              </div>

            }

            <div className={item.project_type === '1' ? "rowBot l-tb-flex fan" : "rowBot l-tb-flex"}>
                <div title={item.collected_amount} className="price tooltip">

                    <span className="small-title d-b-mb">支援総額</span>
                    <span className="number d-b-mb">
                        <NumberFormat value={item.collected_amount} displayType={'text'} thousandSeparator={true} suffix={'円'} />
                    </span>

                </div>
                <div title={item.now_count.now_count} className="pNumber">
                    <span className="small-title d-b-mb">支援者数</span>
                    <span className="number d-b-mb">{item.now_count.now_count}人</span>
                </div>
                <div title={this._renderProjectEndDateTitle(item)}  className="date">

                  <span className="small-title d-b-mb">残りの日数</span>
                  <span className="number d-b-mb">{this._renderProjectEndDate(item)}</span>
                </div>
            </div>
          </div>
          {/* Card content */}
        </div>
      </div>
    );
  }
  render() {
    const {item} = this.props;

    return (
      this._renderTemplate(item)
    );
  }
}
