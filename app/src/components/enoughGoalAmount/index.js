/* @flow */

import React, { Component } from 'react';
import ProjectList from '../projectList';
import Loading from '../common/loading';
import LoadingScroll from '../common/loadingScroll';
import * as actions from '../../actions/TopPage';
import { connect } from 'react-redux';
import ReactPaginate from 'react-paginate';
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faChevronLeft, faChevronRight} from "@fortawesome/free-solid-svg-icons";

class enoughGoalAmount extends Component {


  componentDidMount() {
    const {page} = this.props.match.params;
    var PageString = page.replace('page=', '');
    if (!this.props.ProjectGoal) {
      this.props.ProjectEnoughtGoalAmountList(Number(PageString)-1);
    }
    document.title = "終了間近のプロジェクト｜KAKUSEIDA";

  }


  handlePageClick = (data) => {
    let selected = data.selected;
    this.props.ProjectEnoughtGoalAmountList(selected);
  };
  _renderView(ProjectGoal) {
    const linkBreacrm = [
      {last:true,label:"終了間近のプロジェクト",link:null}
    ]
    if (ProjectGoal.data.length !== 0) {
      return(
        <div>
          <ProjectList linkBreacrm={linkBreacrm} data={this.props.ProjectGoal.data} title={"終了間近のプロジェクト"}/>
          <div className="cover-paginate">

            {
              this.props.loading?<div className="loading-io"><LoadingScroll></LoadingScroll></div>:null
            }
            <ReactPaginate
              previousLabel={<FontAwesomeIcon icon={faChevronLeft}/>}
              nextLabel={<FontAwesomeIcon icon={faChevronRight}/>}
              breakLabel={<span>...</span>}
              breakClassName={"break-me"}
              pageCount={this.props.ProjectGoal.page_count}
              forcePage={Number(this.props.pageload)}
              marginPagesDisplayed={2}
              pageRangeDisplayed={2}
              onPageChange={this.handlePageClick}
              containerClassName={"pagination"}
              subContainerClassName={"pages pagination"}
              activeClassName={"active"} />
          </div>
          <div className="clear-fix"></div>
        </div>
      )
    }else {
      return (
        <div>
          <ProjectList linkBreacrm={linkBreacrm} data={this.props.ProjectGoal.data} title={"終了間近のプロジェクト"}/>
        </div>
      );
    }

  }
  render() {
    const {ProjectGoal} = this.props;

    return (
      <div>
        <div>
          {
            ProjectGoal ?
              this._renderView(ProjectGoal)
            : <Loading></Loading>
          }

        </div>
      </div>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    ProjectGoal: state.topPage.ProjectGoal.data,
    loading:state.common.loading,
    pageload:state.topPage.ProjectGoal.page
  }
}

export default connect(mapStateToProps, actions)(enoughGoalAmount);
