/* @flow */

import React, { Component } from 'react';
import ProjectList from '../projectList';
import Loading from '../common/loading';
import LoadingScroll from '../common/loadingScroll';
import * as actions from '../../actions/TopPage';
import { connect } from 'react-redux';
import ReactPaginate from 'react-paginate';
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faChevronLeft, faChevronRight} from "@fortawesome/free-solid-svg-icons";

class ProjectHistory extends Component {

  componentDidMount() {
    const {page} = this.props.match.params;
    var PageString = page.replace('page=', '');
    if (!this.props.ProjectExpried) {
      this.props.expiredPaginationlist(Number(PageString)-1);
    }
    document.title = "過去のプロジェクト｜KAKUSEIDA";
  }

  handlePageClick = (data) => {
    let selected = data.selected;
    this.props.expiredPaginationlist(selected);
  };
  _renderView(ProjectExpried) {
    const linkBreacrm = [
      {last:true,label:"過去のプロジェクト",link:null}
    ]
    if (ProjectExpried.data.length !== 0) {
      return(
        <div>
          <ProjectList linkBreacrm={linkBreacrm} data={ProjectExpried.data} title={"過去のプロジェクト"}/>
          <div className="cover-paginate">
            {
              this.props.loading?<div className="loading-io"><LoadingScroll></LoadingScroll></div>:null
            }
            <ReactPaginate
              previousLabel={<FontAwesomeIcon icon={faChevronLeft}/>}
              nextLabel={<FontAwesomeIcon icon={faChevronRight}/>}
              breakLabel={<span>...</span>}
              breakClassName={"break-me"}
              pageCount={this.props.ProjectExpried.page_count}
              marginPagesDisplayed={2}
              pageRangeDisplayed={2}
              forcePage={Number(this.props.pageload)}
              onPageChange={this.handlePageClick}
              containerClassName={"pagination"}
              subContainerClassName={"pages pagination"}
              activeClassName={"active"} />
          </div>
          <div className="clear-fix"></div>
        </div>
      )
    }else {
      return (
        <div>
          <ProjectList linkBreacrm={linkBreacrm} data={this.props.ProjectExpried.data} title={"過去のプロジェクト"}/>
        </div>
      )
    }
  }
  render() {
    const {ProjectExpried} = this.props;

    return (
      <div>
        <div>
          {
            ProjectExpried ?
              this._renderView(ProjectExpried)
            : <Loading></Loading>
          }

        </div>
      </div>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    ProjectExpried: state.topPage.ProjectExpried.data,
    loading:state.common.loading,
    pageload:state.topPage.ProjectExpried.page
  }
}

export default connect(mapStateToProps, actions)(ProjectHistory);
