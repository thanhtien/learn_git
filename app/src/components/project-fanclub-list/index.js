/* @flow */

import React, { Component } from 'react';
import ProjectList from '../projectList';
import Loading from '../common/loading';
import LoadingScroll from '../common/loadingScroll';
import * as actions from '../../actions/TopPage';
import { connect } from 'react-redux';
import ReactPaginate from 'react-paginate';
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faChevronLeft, faChevronRight} from "@fortawesome/free-solid-svg-icons";

class FanClub extends Component {

  componentDidMount() {
    const {page} = this.props.match.params;
    var PageString = page.replace('page=', '');
    if (!this.props.ProjectExpried) {
      this.props.projectsFanClub(Number(PageString)-1);
    }
    document.title = "定期課金のプロジェクト｜KAKUSEIDA";
  }

  handlePageClick = (data) => {
    let selected = data.selected;
    this.props.projectsFanClub(selected);
  };
  _renderView(fanclub) {
    const linkBreacrm = [
      {last:true,label:"定期課金のプロジェクト",link:null}
    ]
    if (fanclub.data.length !== 0) {
      return(
        <div>
          <ProjectList linkBreacrm={linkBreacrm} data={fanclub.data} title={"定期課金のプロジェクト"}/>
          <div className="cover-paginate">
            {
              this.props.loading?<div className="loading-io"><LoadingScroll></LoadingScroll></div>:null
            }
            <ReactPaginate
              previousLabel={<FontAwesomeIcon icon={faChevronLeft}/>}
              nextLabel={<FontAwesomeIcon icon={faChevronRight}/>}
              breakLabel={<span>...</span>}
              breakClassName={"break-me"}
              pageCount={this.props.fanclub.page_count}
              marginPagesDisplayed={2}
              pageRangeDisplayed={2}
              forcePage={Number(this.props.pageload)}
              onPageChange={this.handlePageClick}
              containerClassName={"pagination"}
              subContainerClassName={"pages pagination"}
              activeClassName={"active"} />
          </div>
          <div className="clear-fix"></div>
        </div>
      )
    }else {
      return (
        <div>
          <ProjectList linkBreacrm={linkBreacrm} data={this.props.fanclub.data} title={"定期支援中のプロジェクト"}/>
        </div>
      )
    }
  }
  render() {
    const {fanclub} = this.props;

    return (
      <div>
        <div>
          {
            fanclub ?
              this._renderView(fanclub)
            : <Loading></Loading>
          }

        </div>
      </div>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    fanclub: state.topPage.fanclub.data,
    loading:state.common.loading,
    pageload:state.topPage.fanclub.page
  }
}

export default connect(mapStateToProps, actions)(FanClub);
