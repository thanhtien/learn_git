import React, { Component } from 'react';
import Header from './common/header';
import Footer from './common/footer';
import {Helmet} from "react-helmet";
import { connect } from 'react-redux';
import * as actions from './../actions/common.js'

import NotificationSocketIO from './NotificationSocketIO';
import ReactGA from 'react-ga';
import { Line } from 'rc-progress';
import StyleComponent from '../components/asset/';
var CONFIG = require('../config/common');
if (process.env.NODE_ENV === "development") {
  CONFIG = CONFIG.CONFIG.DEV;
} else {
  CONFIG = CONFIG.CONFIG.PRODUCT;
}
ReactGA.initialize(CONFIG.GA_KEY);
ReactGA.pageview(window.location.pathname + window.location.search);

class App extends Component {

  componentDidMount() {
    //Get Categories
    const {cat } = this.props;

    //Call Categories
    if (!cat) {
      this.props.fetchCat();
    }

    //Get TOKen Auth
    if (this.props.authenticated) {
      this.props.profileUser();
        localStorage.removeItem('NologIn');
    }else{
        if (localStorage.getItem('NologIn')== null) {
            localStorage.setItem('NologIn', this.uuidv4());
        }
    }
    //Remove When Change
    document.body.className = '';

  }
    uuidv4 =() =>{
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
            var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
            return v.toString(16);
        });
    }
  //Fix Warning
  componentWillUnmount(){
    this.setState = (state,callback)=>{
        return;
    };
  }




  render() {
    const {cat,profile,pushSocket,socket ,percent} = this.props;

    return (

      <div className="my-app">
        {
          percent !== 0 ?
          <Line className="lineProgess" percent={percent} strokeWidth='1' strokeColor='#2db7f5' strokeLinecap='square' /> : null
        }

        {/*  Head */}
        <Helmet>
          <style type="text/css">{`
            html,button,input,select,textarea,pre{font-family:"メイリオ","Hiragino Kaku Gothic Pro","ヒラギノ角ゴ Pro W3","ＭＳ Ｐゴシック",sans-serif}body{margin:0;font-family:"メイリオ","Hiragino Kaku Gothic Pro","ヒラギノ角ゴ Pro W3","ＭＳ Ｐゴシック",sans-serif}
           `}</style>
        </Helmet>
        {/*  Head */}

        {/* HEADER */}
        <Header data={cat} profile={profile} />
        {/* HEADER */}


        {/* Content */}
        <div id="content">

          {this.props.children}

        </div>
        {/* Content */}

        {/* Notification     */}
        {
          profile ?
          <NotificationSocketIO
            profile={profile}
            pushSocket={pushSocket}
            socket={socket}
          /> :
          null
        }
        {/* Notification */}

        {/*Footer*/}
        <Footer data={cat} />
        {/*Footer*/}



        {/*Scroll Top*/}
        <StyleComponent></StyleComponent>
      </div>
    );
  }
}
const mapStateToProps = (state) => {
    return {
      cat: state.common.Categories,
      profile:state.common.profile,
      authenticated: state.auth.authenticated,
      percent: state.uploadImage.progess,

    }
}

export default connect(mapStateToProps, actions)(App);
