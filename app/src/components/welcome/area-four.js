/* @flow */

import React, { Component } from 'react';


import CardProject from '../cardProject';
var CONFIG = require('../../config/common');

if (process.env.NODE_ENV === "development") {
  CONFIG = CONFIG.CONFIG.DEV;
}else {
  CONFIG = CONFIG.CONFIG.PRODUCT;
}
export default class AreaFour extends Component {



  render() {
    const { recommentProject } = this.props;
    return (
      <div className="area blk-area">
          <div className="wrap-content-inner">
              <h3 className="area-title l-tb-flex">
                <span className="title-area">オススメのプロジェクト</span>
              </h3>
              <div className="area-grid l-grid-group">
                  <div className="row-grid gutter-18-sm gutter-18-grid-sm">
                      {
                        recommentProject.map( (item , i) => {
                          return(
                            <CardProject key={i} item={item}/>
                          )
                        })
                      }
                  </div>
              </div>
              {/* ROW */}
          </div>
          {/* WRAPER */}
      </div>
    );
  }
}
