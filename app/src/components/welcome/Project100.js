/* @flow */
import React, { Component } from 'react';

import CardProject from '../cardProject';
import {  Link } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {faHourglassEnd} from '@fortawesome/free-solid-svg-icons';


export default class AreaSeven extends Component {


  render() {
    const {data,page} = this.props;
    var PageLink = Number(page) + 1;

    return (
      <div className="area area7">
        <div className="wraper">
            <h3 className="titleArea">
              <span className="icon">
                <FontAwesomeIcon icon={faHourglassEnd} style={{color: '#125f36'}}/>
              </span>
              <span className="title-area">達成したプロジェクト</span>
            </h3>
            <div className="row">
              {
                data.map( (item , i) => {
                  return(
                    <CardProject key={i} item={item}/>
                  )
                })
              }
            </div>
            {/* ROW */}
            <p className="linkAll"><Link to={"/project100-list/page="+PageLink}>もっと見る</Link></p>
        </div>
        {/* WRAPER */}
      </div>
    );
  }
}
