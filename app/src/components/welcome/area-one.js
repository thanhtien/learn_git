/* @flow */
import React, { Component } from 'react';
import CardProject from '../cardProject';

import {  Link } from 'react-router-dom';
var CONFIG = require('../../config/common');

if (process.env.NODE_ENV === "development") {
  CONFIG = CONFIG.CONFIG.DEV;
}else {
  CONFIG = CONFIG.CONFIG.PRODUCT;
}
export default class AreaOne extends Component {

  render() {
    const { ProjectNewest,page } = this.props;
    var PageLink = Number(page) + 1;
    return (
      <div className="area area1 blk-area">
          <div className="wrap-content-inner">
              <h3 className="area-title l-tb-flex">
                <span className="title-area">新着のプロジェクト</span>
                <a href={"/project-from-type/from-news-project/page=1"} className={"classATop"}> もっと見る</a>
              </h3>
              <div className="area-grid l-grid-group">
                  <div className="row-grid gutter-18-sm gutter-18-grid-sm">
                      {
                        ProjectNewest.map( (item , i) => {
                          return(
                            <CardProject key={i} item={item}/>
                          )
                        })
                      }
                  </div>
              </div>
              {/* ROW */}
              <p className="linkAll"><a href={"/project-from-type/from-news-project/page=1"} className={"classATop"}> もっと見る</a></p>
          </div>
          {/* WRAPER */}
      </div>
    );
  }
}
