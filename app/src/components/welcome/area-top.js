/* @flow */
import React, { Component } from 'react';

import CardProject from '../cardProject';
import {  Link } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {faHistory} from '@fortawesome/free-solid-svg-icons';
import {LazyLoadImage} from "react-lazy-load-image-component";
import {ParseUrl} from "../common/parseImage";



export default class AreaTop extends Component {
    case1Show=(projectLast)=>{
        let caseReturn = projectLast.caseReturn;

        let item1 =  projectLast.data[0];
        let item2 =  projectLast.data[1];
        let item3 =  projectLast.data[2];
        let urlGo1 = '/project-from-type/from-your-access/page=1';
        let titleGo1 = '最近見たプロジェクト';

        let urlGo2 = '/project-from-type/from-support-80/page=1';
        let titleGo2 = 'あとひと押し';

        let urlGo3 = '/project-from-type/from-news-project/page=1';
        let titleGo3 ='新着のプロジェクト';
        let showAccessRight = false;

        let item3IsEnd = true;

        return ( <div className="area area7 blk-area">
            <div className="wrap-content-inner">
                <div className="area-grid">
                    <div className="row-grid gutter-18-sm list-grid">
                        {/*LAST ACCESS*/}
                        {  item1.id >0 &&  <div className="list-grid-item col-md-4">
                            <div className="card">
                                <p className="card-title"><span>{titleGo1}</span><a href={urlGo1} className={"classATop"}> もっと見る</a></p>
                                <div className="card-intro">
                                    <Link to={'/project-detail/'+item1.id}>
                                        <LazyLoadImage src={item1.thumbnail} effect="blur" alt="thum"></LazyLoadImage>
                                    </Link>
                                </div>
                            </div>
                        </div>}
                        {/*LAST ACCESS*/}
                        {/*LAST PROJECT > 80%*/}
                        {item2.id >0 &&<div className="list-grid-item col-md-4">
                            <div className="card">
                                <p className="card-title"><span>{titleGo2}</span>{titleGo2  && <a href={urlGo2} className={"classATop"}> もっと見る</a> }

                                </p>
                                <div className="card-intro">
                                    <Link to={'/project-detail/'+item2.id}>
                                        <LazyLoadImage src={item2.thumbnail} effect="blur" alt="thum"></LazyLoadImage>
                                    </Link>
                                </div>
                            </div>
                        </div>}
                        {/*LAST PROJECT*/}
                        {/*NEWS*/}
                        {item3.id >0 && <div className="list-grid-item col-md-4">
                            <div className="card">
                                <p className="card-title"><span>{titleGo3}</span>

                                    {item3IsEnd ? <a href={"/project-from-type/from-news-project/page=1"} className={"classATop"}> もっと見る</a>:<span>&nbsp;&nbsp;</span>}

                                </p>
                                <div className="card-intro">
                                    <Link to={'/project-detail/'+item3.id}>
                                        <LazyLoadImage src={item3.thumbnail} effect="blur" alt="thum"></LazyLoadImage>
                                    </Link>
                                </div>
                            </div>
                        </div>
                        }

                        {/*NEWS*/}
                    </div>
                </div>
                {/* ROW */}

            </div>
            {/* WRAPER */}
        </div>);
    }
    case1ShowRemove=(projectLast)=>{
        let caseReturn = projectLast.caseReturn;

        let item1 =  projectLast.data[0];
        let item2 =  projectLast.data[1];
        let item3 =  projectLast.data[2];
        let urlGo1 = '/project-from-type/from-your-access/page=1';
        let titleGo1 = '最近見たプロジェクト';
        let showAccessRight = true;
        if(item1.isFromAccess==0){
            showAccessRight = false;
            urlGo1 = '/project-from-type/from-news-project/page=1';
            titleGo1 ='新着のプロジェクト';
        }
        let urlGo2 = '/project-from-type/from-support-80/page=1';
        let titleGo2 = 'あとひと押し';
        if( item2.isPercent ==0){
            urlGo2 = '/project-from-type/from-news-project/page=1';
            titleGo2 ='新着のプロジェクト';
        }
        let urlGo3 = '/project-from-type/from-news-project/page=1';
        let titleGo3 ='新着のプロジェクト';
        console.log(item1.thumbnail);

        let item3IsEnd = true;

        if(( item2.isPercent ==1)&& (showAccessRight ==false)){

            item3IsEnd =true;
        }else{
            if(showAccessRight ==false){
                if(titleGo1 == titleGo2 ){
                    titleGo1 = titleGo3 = false;
                }
                item3IsEnd =false;
                if(titleGo1 == titleGo3){
                    titleGo1 = false;
                }
            }

            if(showAccessRight ==true){
                if(titleGo2 == titleGo3){
                    titleGo3 = false;
                    item3IsEnd = true;
                }
            }
            //has percent
            if( item2.isPercent ==1){
                titleGo1 = false;
                item3IsEnd =false;

            }
        }
        return ( <div className="area area7 blk-area">
            <div className="wrap-content-inner">
                <div className="area-grid">
                    <div className="row-grid gutter-18-sm list-grid">
                        {/*LAST ACCESS*/}
                        { showAccessRight&& item1.id >0 &&  <div className="list-grid-item col-md-4">
                            <div className="card">
                                <p className="card-title"><span>{titleGo1}</span><a href={urlGo1} className={"classATop"}> もっと見る</a></p>
                                <div className="card-intro">
                                    <Link to={'/project-detail/'+item1.id}>
                                        <LazyLoadImage src={item1.thumbnail} effect="blur" alt="thum"></LazyLoadImage>
                                    </Link>
                                </div>
                            </div>
                        </div>}
                        {/*LAST ACCESS*/}
                        {/*LAST PROJECT > 80%*/}
                        {item2.id >0 &&<div className="list-grid-item col-md-4">
                            <div className="card">
                                <p className="card-title"><span>{titleGo2}</span>{titleGo2 && item2.isPercent ==1 && <a href={urlGo2} className={"classATop"}> もっと見る</a> }

                                </p>
                                <div className="card-intro">
                                    <Link to={'/project-detail/'+item2.id}>
                                        <LazyLoadImage src={item2.thumbnail} effect="blur" alt="thum"></LazyLoadImage>
                                    </Link>
                                </div>
                            </div>
                        </div>}
                        {/*LAST PROJECT*/}
                        {/*NEWS*/}
                        {item3.id >0 && <div className="list-grid-item col-md-4">
                            <div className="card">
                                <p className="card-title"><span>{titleGo3}</span>

                                    {item3IsEnd ? <a href={"/project-from-type/from-news-project/page=1"} className={"classATop"}> もっと見る</a>:<span>&nbsp;&nbsp;</span>}

                                </p>
                                <div className="card-intro">
                                    <Link to={'/project-detail/'+item3.id}>
                                        <LazyLoadImage src={item3.thumbnail} effect="blur" alt="thum"></LazyLoadImage>
                                    </Link>
                                </div>
                            </div>
                        </div>
                        }
                        {/*//no access*/}
                        { !showAccessRight&& item1.id >0 &&<div className="list-grid-item col-md-4">
                            <div className="card">
                                <p className="card-title"><span>{titleGo1}</span>
                                    {<a href={urlGo1} className={"classATop"}> もっと見る</a>}</p>
                                <div className="card-intro">
                                    <Link to={'/project-detail/'+item1.id}>
                                        <LazyLoadImage src={item1.thumbnail} effect="blur" alt="thum"></LazyLoadImage>
                                    </Link>
                                </div>
                            </div>
                        </div>}
                        {/*NEWS*/}
                    </div>
                </div>
                {/* ROW */}

            </div>
            {/* WRAPER */}
        </div>);
    }
    case2Show=(projectLast)=>{


        let item1 =  projectLast.data[0];
        let item2 =  projectLast.data[1];
        let item3 =  projectLast.data[2];
        let urlGo1 = '/project-from-type/from-your-access/page=1';
        let titleGo1 = '最近見たプロジェクト';


        let urlGo2 =  '/project-from-type/from-news-project/page=1';
        let titleGo2 ='新着のプロジェクト';

        let urlGo3 = '/project-from-type/from-news-project/page=1';
        let titleGo3 ='新着のプロジェクト';

        if(item3.id>0){
            titleGo3 = false;
        }else{

        }


        return ( <div className="area area7 blk-area">
            <div className="wrap-content-inner">
                <div className="area-grid">
                    <div className="row-grid gutter-18-sm list-grid">
                        {/*LAST ACCESS*/}
                        <div className="list-grid-item col-md-4">
                            <div className="card">
                                <p className="card-title"><span>{titleGo1}</span><a href={urlGo1} className={"classATop"}> もっと見る</a></p>
                                <div className="card-intro">
                                    <Link to={'/project-detail/'+item1.id}>
                                        <LazyLoadImage src={item1.thumbnail} effect="blur" alt="thum"></LazyLoadImage>
                                    </Link>
                                </div>
                            </div>
                        </div>
                        {/*LAST ACCESS*/}
                        {/*LAST PROJECT > 80%*/}
                        {item2.id >0 &&<div className="list-grid-item col-md-4">
                            <div className="card">
                                <p className="card-title"><span>{titleGo2}</span>{titleGo2 && item3.id ==0 && <a href={urlGo2} className={"classATop"}> もっと見る</a> }

                                </p>
                                <div className="card-intro">
                                    <Link to={'/project-detail/'+item2.id}>
                                        <LazyLoadImage src={item2.thumbnail} effect="blur" alt="thum"></LazyLoadImage>
                                    </Link>
                                </div>
                            </div>
                        </div>}
                        {/*LAST PROJECT*/}
                        {/*NEWS*/}
                        {item3.id >0 && <div className="list-grid-item col-md-4">
                            <div className="card">



                                <p className="card-title myCard">
                                    <span>{titleGo3}</span>

                                    <a href={"/project-from-type/from-news-project/page=1"} className={"classATop"}> もっと見る</a>

                                </p>
                                <div className="card-intro">
                                    <Link to={'/project-detail/'+item3.id}>
                                        <LazyLoadImage src={item3.thumbnail} effect="blur" alt="thum"></LazyLoadImage>
                                    </Link>
                                </div>
                            </div>
                        </div>
                        }


                    </div>
                </div>
                {/* ROW */}

            </div>
            {/* WRAPER */}
        </div>);
    }
    case3Show=(projectLast)=>{

        let item1 =  projectLast.data[0];
        let item2 =  projectLast.data[1];
        let item3 =  projectLast.data[2];

        let urlGo1 = '/project-from-type/from-news-project/page=1';
        let titleGo1 = '新着のプロジェクト';


        let urlGo2 =  '/project-from-type/from-news-project/page=1';
        let titleGo2 ='新着のプロジェクト';

        let urlGo3 = '/project-from-type/from-news-project/page=1';
        let titleGo3 ='新着のプロジェクト';

        if(item2.id >0){
            urlGo1 = false;
        }else{

        }
        if(item3.id >0){
            urlGo2 = false;
            titleGo2 = false;
            titleGo3 = false;
        }

        return ( <div className="area area7 blk-area">
            <div className="wrap-content-inner">
                <div className="area-grid">
                    <div className="row-grid gutter-18-sm list-grid">
                        {/*LAST ACCESS*/}
                        <div className="list-grid-item col-md-4">
                            <div className="card">
                                <p className="card-title"><span>{titleGo1}</span>
                                    {urlGo1 && <a href={urlGo1} className={"classATop"}> もっと見る</a>}
                                </p>
                                <div className="card-intro">
                                    <Link to={'/project-detail/'+item1.id}>
                                        <LazyLoadImage src={item1.thumbnail} effect="blur" alt="thum"></LazyLoadImage>
                                    </Link>
                                </div>
                            </div>
                        </div>
                        {/*LAST ACCESS*/}
                        {/*LAST PROJECT > 80%*/}
                        {item2.id >0 &&<div className="list-grid-item col-md-4">
                            <div className="card">
                                <p className="card-title">

                                    {titleGo2?<span>{titleGo2}</span>:<span>&nbsp;&nbsp;</span>  }
                                    {titleGo2 && item3.id ==0 && <a href={urlGo2} className={"classATop"}> もっと見る</a> }

                                </p>
                                <div className="card-intro">
                                    <Link to={'/project-detail/'+item2.id}>
                                        <LazyLoadImage src={item2.thumbnail} effect="blur" alt="thum"></LazyLoadImage>
                                    </Link>
                                </div>
                            </div>
                        </div>}
                        {/*LAST PROJECT*/}
                        {/*NEWS*/}
                        {item3.id >0 && <div className="list-grid-item col-md-4">
                            <div className="card">



                                <p className="card-title myCard">
                                    <span>{titleGo3}</span>

                                    <a href={"/project-from-type/from-news-project/page=1"} className={"classATop"}> もっと見る</a>

                                </p>
                                <div className="card-intro">
                                    <Link to={'/project-detail/'+item3.id}>
                                        <LazyLoadImage src={item3.thumbnail} effect="blur" alt="thum"></LazyLoadImage>
                                    </Link>
                                </div>
                            </div>
                        </div>
                        }


                    </div>
                </div>
                {/* ROW */}

            </div>
            {/* WRAPER */}
        </div>);
    }
    case4Show=(projectLast)=>{
      let caseReturn = projectLast.caseReturn;

      let item1 =  projectLast.data[0];
      let item2 =  projectLast.data[1];
      let item3 =  projectLast.data[2];
      let urlGo1 = '/project-from-type/from-support-80/page=1';
      let titleGo1 = 'あとひと押し';
      let showAccessRight = true;

      let urlGo2 =  '/project-from-type/from-news-project/page=1';
      let titleGo2 ='新着のプロジェクト';

      let urlGo3 = '/project-from-type/from-news-project/page=1';
      let titleGo3 ='新着のプロジェクト';

      if(item3.id>0){
         titleGo3 = false;
      }else{

      }


   return ( <div className="area area7 blk-area">
       <div className="wrap-content-inner">
           <div className="area-grid">
               <div className="row-grid gutter-18-sm list-grid">
                   {/*LAST ACCESS*/}
                    <div className="list-grid-item col-md-4">
                       <div className="card">
                           <p className="card-title"><span>{titleGo1}</span><a href={urlGo1} className={"classATop"}> もっと見る</a></p>
                           <div className="card-intro">
                               <Link to={'/project-detail/'+item1.id}>
                                   <LazyLoadImage src={item1.thumbnail} effect="blur" alt="thum"></LazyLoadImage>
                               </Link>
                           </div>
                       </div>
                   </div>
                   {/*LAST ACCESS*/}
                   {/*LAST PROJECT > 80%*/}
                   {item2.id >0 &&<div className="list-grid-item col-md-4">
                       <div className="card">
                           <p className="card-title"><span>{titleGo2}</span>{titleGo2 && item3.id ==0 && <a href={urlGo2} className={"classATop"}> もっと見る</a> }

                           </p>
                           <div className="card-intro">
                               <Link to={'/project-detail/'+item2.id}>
                                   <LazyLoadImage src={item2.thumbnail} effect="blur" alt="thum"></LazyLoadImage>
                               </Link>
                           </div>
                       </div>
                   </div>}
                   {/*LAST PROJECT*/}
                   {/*NEWS*/}
                   {item3.id >0 && <div className="list-grid-item col-md-4">
                       <div className="card">



                           <p className="card-title myCard">
                               <span>{titleGo3}</span>

                               <a href={"/project-from-type/from-news-project/page=1"} className={"classATop"}> もっと見る</a>

                           </p>
                           <div className="card-intro">
                               <Link to={'/project-detail/'+item3.id}>
                                   <LazyLoadImage src={item3.thumbnail} effect="blur" alt="thum"></LazyLoadImage>
                               </Link>
                           </div>
                       </div>
                   </div>
                   }


               </div>
           </div>
           {/* ROW */}

       </div>
       {/* WRAPER */}
   </div>);
   }
    case5Show=(projectLast)=>{

        let item1 =  projectLast.data[0];
        let item2 =  projectLast.data[1];
        let item3 =  projectLast.data[2];

        let urlGo1 = '/project-from-type/from-support-80/page=1';
        let titleGo1 = 'あとひと押し';


        let urlGo2 =  '/project-from-type/from-support-80/page=1';
        let titleGo2 ='あとひと押し';

        let urlGo3 = '/project-from-type/from-news-project/page=1';
        let titleGo3 ='新着のプロジェクト';
        urlGo1 = false;
        titleGo2 = false;



        return ( <div className="area area7 blk-area">
            <div className="wrap-content-inner">
                <div className="area-grid">
                    <div className="row-grid gutter-18-sm list-grid">
                        {/*LAST ACCESS*/}
                        <div className="list-grid-item col-md-4">
                            <div className="card">
                                <p className="card-title"><span>{titleGo1}</span>
                                    {urlGo1 && <a href={urlGo1} className={"classATop"}> もっと見る</a>}
                                </p>
                                <div className="card-intro">
                                    <Link to={'/project-detail/'+item1.id}>
                                        <LazyLoadImage src={item1.thumbnail} effect="blur" alt="thum"></LazyLoadImage>
                                    </Link>
                                </div>
                            </div>
                        </div>
                        {/*LAST ACCESS*/}
                        {/*LAST PROJECT > 80%*/}
                        {item2.id >0 &&<div className="list-grid-item col-md-4">
                            <div className="card">
                                <p className="card-title">

                                    {titleGo2?<span>{titleGo2}</span>:<span>&nbsp;&nbsp;</span>  }
                                    {urlGo2 &&  <a href={urlGo2} className={"classATop"}> もっと見る</a> }

                                </p>
                                <div className="card-intro">
                                    <Link to={'/project-detail/'+item2.id}>
                                        <LazyLoadImage src={item2.thumbnail} effect="blur" alt="thum"></LazyLoadImage>
                                    </Link>
                                </div>
                            </div>
                        </div>}
                        {/*LAST PROJECT*/}
                        {/*NEWS*/}
                        {item3.id >0 && <div className="list-grid-item col-md-4">
                            <div className="card">



                                <p className="card-title myCard">

                                    {titleGo3?<span>{titleGo3}</span>:<span>&nbsp;&nbsp;</span>  }

                                    {urlGo3 &&
                                    <a href={urlGo3} className={"classATop"}> もっと見る</a>
                                    }
                                </p>
                                <div className="card-intro">
                                    <Link to={'/project-detail/'+item3.id}>
                                        <LazyLoadImage src={item3.thumbnail} effect="blur" alt="thum"></LazyLoadImage>
                                    </Link>
                                </div>
                            </div>
                        </div>
                        }


                    </div>
                </div>
                {/* ROW */}

            </div>
            {/* WRAPER */}
        </div>);
    }
    case6Show=(projectLast)=>{

        let item1 =  projectLast.data[0];
        let item2 =  projectLast.data[1];
        let item3 =  projectLast.data[2];

        let urlGo1 = '/project-from-type/from-support-80/page=1';
        let titleGo1 = 'あとひと押し';


        let urlGo2 =  '/project-from-type/from-support-80/page=1';
        let titleGo2 ='あとひと押し';

        let urlGo3 =  '/project-from-type/from-support-80/page=1';
        let titleGo3 ='あとひと押し';
        if(item2.id >0){
            urlGo1 = false;
            titleGo2 = false;
        }
        if(item3.id >0){
            urlGo2 = false;
            titleGo2 = false;
            titleGo3 = false;
        }




        return ( <div className="area area7 blk-area">
            <div className="wrap-content-inner">
                <div className="area-grid">
                    <div className="row-grid gutter-18-sm list-grid">
                        {/*LAST ACCESS*/}
                        <div className="list-grid-item col-md-4">
                            <div className="card">
                                <p className="card-title"><span>{titleGo1}</span>
                                    {urlGo1 && <a href={urlGo1} className={"classATop"}> もっと見る</a>}
                                </p>
                                <div className="card-intro">
                                    <Link to={'/project-detail/'+item1.id}>
                                        <LazyLoadImage src={item1.thumbnail} effect="blur" alt="thum"></LazyLoadImage>
                                    </Link>
                                </div>
                            </div>
                        </div>
                        {/*LAST ACCESS*/}
                        {/*LAST PROJECT > 80%*/}
                        {item2.id >0 &&<div className="list-grid-item col-md-4">
                            <div className="card">
                                <p className="card-title">

                                    {titleGo2?<span>{titleGo2}</span>:<span>&nbsp;&nbsp;</span>  }
                                    {urlGo2 &&  <a href={urlGo2} className={"classATop"}> もっと見る</a> }

                                </p>
                                <div className="card-intro">
                                    <Link to={'/project-detail/'+item2.id}>
                                        <LazyLoadImage src={item2.thumbnail} effect="blur" alt="thum"></LazyLoadImage>
                                    </Link>
                                </div>
                            </div>
                        </div>}
                        {/*LAST PROJECT*/}
                        {/*NEWS*/}
                        {item3.id >0 && <div className="list-grid-item col-md-4">
                            <div className="card">



                                <p className="card-title myCard">

                                    {titleGo3?<span>{titleGo3}</span>:<span>&nbsp;&nbsp;</span>  }
                                    {urlGo3 &&
                                    <a href={urlGo3} className={"classATop"}> もっと見る</a>
                                    }


                                </p>
                                <div className="card-intro">
                                    <Link to={'/project-detail/'+item3.id}>
                                        <LazyLoadImage src={item3.thumbnail} effect="blur" alt="thum"></LazyLoadImage>
                                    </Link>
                                </div>
                            </div>
                        </div>
                        }


                    </div>
                </div>
                {/* ROW */}

            </div>
            {/* WRAPER */}
        </div>);
    }
    case7Show=(projectLast)=>{

        let item1 =  projectLast.data[0];
        let item2 =  projectLast.data[1];
        let item3 =  projectLast.data[2];
        //  item2.id = item3.id = 0;

        let urlGo1 = '/project-from-type/from-your-access/page=1';
        let titleGo1 = '最近見たプロジェクト';



        let urlGo2 = '/project-from-type/from-your-access/page=1';
        let titleGo2 = '最近見たプロジェクト';

        let urlGo3 = '/project-from-type/from-your-access/page=1';
        let titleGo3 = '最近見たプロジェクト';
        if(item2.id >0){
            urlGo1 = false;
            titleGo2 = false;
        }
        if(item3.id >0){
            urlGo2 = false;
            titleGo2 = false;
            titleGo3 = false;
        }




        return ( <div className="area area7 blk-area">
            <div className="wrap-content-inner">
                <div className="area-grid">
                    <div className="row-grid gutter-18-sm list-grid">
                        {/*LAST ACCESS*/}
                        <div className="list-grid-item col-md-4">
                            <div className="card">
                                <p className="card-title"><span>{titleGo1}</span>
                                    {urlGo1 && <a href={urlGo1} className={"classATop"}> もっと見る</a>}
                                </p>
                                <div className="card-intro">
                                    <Link to={'/project-detail/'+item1.id}>
                                        <LazyLoadImage src={item1.thumbnail} effect="blur" alt="thum"></LazyLoadImage>
                                    </Link>
                                </div>
                            </div>
                        </div>
                        {/*LAST ACCESS*/}
                        {/*LAST PROJECT > 80%*/}
                        {item2.id >0 &&<div className="list-grid-item col-md-4">
                            <div className="card">
                                <p className="card-title">

                                    {titleGo2?<span>{titleGo2}</span>:<span>&nbsp;&nbsp;</span>  }
                                    {urlGo2 &&  <a href={urlGo2} className={"classATop"}> もっと見る</a> }

                                </p>
                                <div className="card-intro">
                                    <Link to={'/project-detail/'+item2.id}>
                                        <LazyLoadImage src={item2.thumbnail} effect="blur" alt="thum"></LazyLoadImage>
                                    </Link>
                                </div>
                            </div>
                        </div>}
                        {/*LAST PROJECT*/}
                        {/*NEWS*/}
                        {item3.id >0 && <div className="list-grid-item col-md-4">
                            <div className="card">



                                <p className="card-title myCard">

                                    {titleGo3?<span>{titleGo3}</span>:<span>&nbsp;&nbsp;</span>  }
                                    {urlGo3 &&
                                    <a href={urlGo3} className={"classATop"}> もっと見る</a>
                                    }


                                </p>
                                <div className="card-intro">
                                    <Link to={'/project-detail/'+item3.id}>
                                        <LazyLoadImage src={item3.thumbnail} effect="blur" alt="thum"></LazyLoadImage>
                                    </Link>
                                </div>
                            </div>
                        </div>
                        }


                    </div>
                </div>
                {/* ROW */}

            </div>
            {/* WRAPER */}
        </div>);
    }
    case8Show=(projectLast)=>{



        let item1 =  projectLast.data[0];
        let item2 =  projectLast.data[1];
        let item3 =  projectLast.data[2];
        //item3.id = 0;
        let urlGo1 = '/project-from-type/from-your-access/page=1';
        let titleGo1 = '最近見たプロジェクト';


        let urlGo2 =  '/project-from-type/from-support-80/page=1';
        let titleGo2 ='あとひと押し';

        let urlGo3 = '/project-from-type/from-support-80/page=1';
        let titleGo3 ='あとひと押し';

        if(item3.id>0){
            urlGo2 = false;
            titleGo3 = false;
        }


        return ( <div className="area area7 blk-area">
            <div className="wrap-content-inner">
                <div className="area-grid">
                    <div className="row-grid gutter-18-sm list-grid">
                        {/*LAST ACCESS*/}
                        <div className="list-grid-item col-md-4">
                            <div className="card">
                                <p className="card-title"><span>{titleGo1}</span><a href={urlGo1} className={"classATop"}> もっと見る</a></p>
                                <div className="card-intro">
                                    <Link to={'/project-detail/'+item1.id}>
                                        <LazyLoadImage src={item1.thumbnail} effect="blur" alt="thum"></LazyLoadImage>
                                    </Link>
                                </div>
                            </div>
                        </div>
                        {/*LAST ACCESS*/}
                        {/*LAST PROJECT > 80%*/}
                        {item2.id >0 &&<div className="list-grid-item col-md-4">
                            <div className="card">
                                <p className="card-title">
                                    {titleGo2?<span>{titleGo2}</span>:<span>&nbsp;&nbsp;</span>  }
                                    {urlGo2? <a href={urlGo2} className={"classATop"}> もっと見る</a>:<span>&nbsp;&nbsp;</span>  }


                                </p>
                                <div className="card-intro">
                                    <Link to={'/project-detail/'+item2.id}>
                                        <LazyLoadImage src={item2.thumbnail} effect="blur" alt="thum"></LazyLoadImage>
                                    </Link>
                                </div>
                            </div>
                        </div>}
                        {/*LAST PROJECT*/}
                        {/*NEWS*/}
                        {item3.id >0 && <div className="list-grid-item col-md-4">
                            <div className="card">



                                <p className="card-title myCard">
                                    {titleGo3?<span>{titleGo3}</span>:<span>&nbsp;&nbsp;</span>  }

                                    {urlGo3 &&
                                    <a href={urlGo3} className={"classATop"}> もっと見る</a>
                                    }

                                </p>
                                <div className="card-intro">
                                    <Link to={'/project-detail/'+item3.id}>
                                        <LazyLoadImage src={item3.thumbnail} effect="blur" alt="thum"></LazyLoadImage>
                                    </Link>
                                </div>
                            </div>
                        </div>
                        }


                    </div>
                </div>
                {/* ROW */}

            </div>
            {/* WRAPER */}
        </div>);
    }
    case9Show=(projectLast)=>{



        let item1 =  projectLast.data[0];
        let item2 =  projectLast.data[1];
        let item3 =  projectLast.data[2];
        //item3.id = 0;
        let urlGo1 = '/project-from-type/from-your-access/page=1';
        let titleGo1 = '最近見たプロジェクト';


        let urlGo2 = '/project-from-type/from-your-access/page=1';
        let titleGo2 = '最近見たプロジェクト';

        let urlGo3 = '/project-from-type/from-support-80/page=1';
        let titleGo3 ='あとひと押し';

        if(item2.id>0){
            urlGo1 = false;
            titleGo2 = false;
        }


        return ( <div className="area area7 blk-area">
            <div className="wrap-content-inner">
                <div className="area-grid">
                    <div className="row-grid gutter-18-sm list-grid">
                        {/*LAST ACCESS*/}
                        <div className="list-grid-item col-md-4">
                            <div className="card">
                                <p className="card-title"><span>{titleGo1}</span>

                                    {urlGo1 &&
                                    <a href={urlGo1} className={"classATop"}> もっと見る</a>
                                    }
                                </p>
                                <div className="card-intro">
                                    <Link to={'/project-detail/'+item1.id}>
                                        <LazyLoadImage src={item1.thumbnail} effect="blur" alt="thum"></LazyLoadImage>
                                    </Link>
                                </div>
                            </div>
                        </div>
                        {/*LAST ACCESS*/}
                        {/*LAST PROJECT > 80%*/}
                        {item2.id >0 &&<div className="list-grid-item col-md-4">
                            <div className="card">
                                <p className="card-title">
                                    {titleGo2?<span>{titleGo2}</span>:<span>&nbsp;&nbsp;</span>  }
                                    {urlGo2? <a href={urlGo2} className={"classATop"}> もっと見る</a>:<span>&nbsp;&nbsp;</span>  }


                                </p>
                                <div className="card-intro">
                                    <Link to={'/project-detail/'+item2.id}>
                                        <LazyLoadImage src={item2.thumbnail} effect="blur" alt="thum"></LazyLoadImage>
                                    </Link>
                                </div>
                            </div>
                        </div>}
                        {/*LAST PROJECT*/}
                        {/*NEWS*/}
                        {item3.id >0 && <div className="list-grid-item col-md-4">
                            <div className="card">



                                <p className="card-title myCard">
                                    {titleGo3?<span>{titleGo3}</span>:<span>&nbsp;&nbsp;</span>  }

                                    {urlGo3 &&
                                    <a href={urlGo3} className={"classATop"}> もっと見る</a>
                                    }

                                </p>
                                <div className="card-intro">
                                    <Link to={'/project-detail/'+item3.id}>
                                        <LazyLoadImage src={item3.thumbnail} effect="blur" alt="thum"></LazyLoadImage>
                                    </Link>
                                </div>
                            </div>
                        </div>
                        }


                    </div>
                </div>
                {/* ROW */}

            </div>
            {/* WRAPER */}
        </div>);
    }
    showCase=(caseShow,projectLast)=>{

       switch(caseShow) {
           case "1":
               return this.case1Show(projectLast);
               break;
           case "2":
               return this.case2Show(projectLast);
               break;
           case "3":
               return this.case3Show(projectLast);
               break;
           case "4":
               return this.case4Show(projectLast);
               break;
           case "5":
               return this.case5Show(projectLast);
               break;
           case "6":
               return this.case6Show(projectLast);
               break;
           case "7":
               return this.case7Show(projectLast);
               break;
           case "8":
               return this.case8Show(projectLast);
               break;
           case "9":
               return this.case9Show(projectLast);
               break;
           default:
           // code block
       }
   }
  render() {
    const {projectLast,page} = this.props;

      let caseReturn = projectLast.caseReturn;
      let item1 =  projectLast.data[0];
      let item2 =  projectLast.data[1];
      let item3 =  projectLast.data[2];

      if( (item1.id ==0) && (item2.id ==0) && (item3.id ==0)){
          return (
              <div></div>
          );
      }
    return (
        <div>  {this.showCase(caseReturn,projectLast)}</div>
    );
  }
}
