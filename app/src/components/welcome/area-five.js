/* @flow */

import React, { Component } from 'react';
import CardProject from '../cardProject';

import {  Link } from 'react-router-dom';
var CONFIG = require('../../config/common');

if (process.env.NODE_ENV === "development") {
  CONFIG = CONFIG.CONFIG.DEV;
}else {
  CONFIG = CONFIG.CONFIG.PRODUCT;
}
export default class AreaFive extends Component {


  render() {
    const {ProjectEnoughExpired} = this.props;
    return (
      <div className="area area5 blk-area">
          <div className="wrap-content-inner">
              <h3 className="area-title l-tb-flex">
                  <span className="title-area">終了間際のプロジェクト</span>
                  <a href={"/project-from-type/from-your-expired/page=1"} className={"classATop"}> もっと見る</a>
              </h3>
              <div className="area-grid l-grid-group">
                <div className="row-grid gutter-18-sm gutter-18-grid-sm">
                    {
                      ProjectEnoughExpired.map( (item , i) => {
                        return(
                          <CardProject key={i} item={item}/>
                        )
                      })
                    }
                </div>
              </div>
              {/* ROW */}
              <p className="linkAll"><Link to={"/project-from-type/from-your-expired/page=1"}>もっと見る</Link></p>
          </div>
          {/* WRAPER */}
      </div>
    );
  }
}
