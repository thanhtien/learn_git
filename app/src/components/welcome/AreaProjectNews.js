/* @flow */
import React, { Component } from 'react';

import CardProject from '../cardProject';
import {  Link } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {faHistory} from '@fortawesome/free-solid-svg-icons';
import {LazyLoadImage} from "react-lazy-load-image-component";
import {ParseUrl} from "../common/parseImage";
import LinesEllipsis from "react-lines-ellipsis";


export default class AreaProjectNews extends Component {


  render() {
    const {dailyNews,page} = this.props;
    var PageLink = Number(page) + 1;

    return (
      <div className="area area7 blk-area">
        <div className="wrap-content-inner">
            <h3 className="area-title l-tb-flex">
                <span className="title-area">活動報告</span>
            </h3>

            <div className="area-grid l-grid-group">
                <div className="row-grid gutter-18-sm gutter-18-grid-sm">
                    {
                        dailyNews.map( (item1 , i) => {
                            let dateUpdate = item1.updated;
                            let titleIt = item1.title;
                            return(
                                <div className="col-6 col-md-4 col-lg-3 grid-item" key={i}>

                                    <div className="card">
                                    <div className="card-intro imgTop" >
                                        <Link to={'/project-detail/'+item1.project_id}>

                                            <LazyLoadImage src={item1.thumbnail} effect="blur" alt="thum">

                                            </LazyLoadImage>

                                        </Link>
 										                    <div className={"card-content"}>
                                               {/*<p className={"titleNewsFormat"} >*/}
                                               {/*    <Link to={'/project-detail/'+item1.project_id}>{titleIt} </Link>*/}

                                               {/*</p>*/}
                                           <div title={titleIt} className="postTitle1">
                                               <Link to={'/project-detail/'+item1.project_id}>
                                                  <pre>
                                                    <LinesEllipsis
                                                        text={titleIt}
                                                        maxLine='1'
                                                        ellipsis='...'
                                                        trimRight
                                                        basedOn='letters'
                                                        className="card-content-title mb-10 truncated"
                                                    />
                                                  </pre>
                                              </Link>
                                               </div>
                                            <p className={"post-date"} >{dateUpdate}</p>
                                        </div>
                                    </div>
                                    </div>
                                </div>
                            )
                        })
                    }

                </div>
            </div>
            {/* ROW */}

        </div>
        {/* WRAPER */}
      </div>
    );
  }
}
