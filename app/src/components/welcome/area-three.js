/* @flow */

import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { LazyLoadImage } from 'react-lazy-load-image-component';
import {ParseUrl} from '../common/parseImage'
import LinesEllipsis from 'react-lines-ellipsis';
var CONFIG = require('../../config/common');

if (process.env.NODE_ENV === "development") {
  CONFIG = CONFIG.CONFIG.DEV;
}else {
  CONFIG = CONFIG.CONFIG.PRODUCT;
}
export default class AreaThree extends Component {

  _renderTemplate(item,i) {

    return(
      <div key={i} className="col-6 col-md-4 col-lg-3">
          <div className="card card-news">
              <p className="card-news-intro img">
                <Link to={'/news-list/new-detail/'+item.id} >
                    <LazyLoadImage
                    alt={"thumbnail"}
                    effect="blur"
                    src={item.thumnail} />
                </Link>
              </p>

              <div className="boxCol">
                  <p className="postTitle">
                    <Link to={'/news-list/new-detail/'+item.id} >
					<pre>
	                  <LinesEllipsis
	                    text={item.title}
	                    maxLine='3'
	                    ellipsis='...'
	                    trimRight
	                    basedOn='letters'
	                  />
                    </pre></Link>
                  </p>
              </div>
          </div>
      </div>
    )
  }
  // <Link to={'new-detail/'+item.id} ></Link>
  render() {
    const {News} = this.props;
    return (
      <div className="area blk-area">
          <div className="wrap-content-inner">
              <h3 className="area-title l-tb-flex">
                  <span className="title-area">KAKUSEIDAからのニュース</span>
                  <a className="classATop" href="/news-list/page=1">もっと見る</a>
              </h3>
              <div className="area-grid l-grid-box">
                  <div className="row-grid gutter-18-sm grid-list-card">
                    {
                      News.map( (item , i) => {
                        return(
                          this._renderTemplate(item , i)
                        )
                      })
                    }
                  </div>
              </div>
              {/* ROW */}
          </div>
          {/* WRAPER */}
      </div>
    );
  }
}
