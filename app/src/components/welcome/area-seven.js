/* @flow */
import React, { Component } from 'react';

import CardProject from '../cardProject';
import {  Link } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {faHistory} from '@fortawesome/free-solid-svg-icons';


export default class AreaSeven extends Component {


  render() {
    const {ProjectExpired,page} = this.props;
    var PageLink = Number(page) + 1;

    return (
      <div className="area area7 blk-area">
        <div className="wrap-content-inner">
            <h3 className="area-title l-tb-flex">
                <span className="title-area">過去のプロジェクト</span>
                <Link className="classATop" to={"/project-from-type/expiredPagination/page="+PageLink}>もっと見る</Link>
            </h3>
            <div className="area-grid l-grid-group">
                <div className="row-grid gutter-18-sm gutter-18-grid-sm">
                    {
                      ProjectExpired.map( (item , i) => {
                        return(
                          <CardProject key={i} item={item}/>
                        )
                      })
                    }
                 </div>
            </div>
            {/* ROW */}
            <p className="linkAll"><Link to={"/project-from-type/expiredPagination/page="+PageLink}>もっと見る</Link></p>
        </div>
        {/* WRAPER */}
      </div>
    );
  }
}
