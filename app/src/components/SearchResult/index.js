/* @flow */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import ProjectList from '../projectList';
import SearchNotFound from './searchNotFound';


class SearchResult extends Component {
  componentDidMount() {
    document.title = "検索結果｜KAKUSEIDA";
  }
  handlePageClick = (data) => {
    let selected = data.selected;
    this.props.ProjectEnoughExpired(selected);
  };
  render() {
    const {data,searchText} = this.props;
    const linkBreacrm = [
      {last:true,label:'Search Result',link:null}
    ]
    return (
      <div>

        {
          data.length !== 0 && searchText ?
          <ProjectList linkBreacrm={linkBreacrm} data={data} title={`"${searchText.search}"の検索結果`}/> :
          <SearchNotFound linkBreacrm={linkBreacrm} title={"Search Not Found"}/>
        }
      </div>

    );
  }
}
const mapStateToProps = (state) => {
  return {
    data: state.common.searchResult,
    searchText:state.common.searchText
  }
}

export default connect(mapStateToProps)(SearchResult);
