/* @flow */

import React, { Component } from 'react';
import ProjectList from '../projectList';
import Loading from '../common/loading';
import LoadingScroll from '../common/loadingScroll';
import * as actions from '../../actions/TopPage';
import { connect } from 'react-redux';
import ReactPaginate from 'react-paginate';
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faChevronLeft, faChevronRight} from "@fortawesome/free-solid-svg-icons";


class projectListCatSlug extends Component {
  componentDidMount() {
    const {slug} = this.props.match.params;
    const {page} = this.props.match.params;
    var PageString = page.replace('page=', '');

    if (!this.props.CatSlugList) {
      this.props.catSlugPaginationlist(Number(PageString)-1,slug);
    }
  }

  handlePageClick = (data) => {
    let selected = data.selected;
    const {slug} = this.props.match.params;
    this.props.catSlugPaginationlist(selected,slug);
  };

  //Render View
  _renderView(CatSlugList) {
    const linkBreacrm = [
      {last:true,label:CatSlugList.category,link:null}
    ]
    if (CatSlugList.data.length !== 0) {
      return(
        <div>
          {
            <ProjectList linkBreacrm={linkBreacrm} data={CatSlugList.data} title={`${CatSlugList.category} のプロジェクト`}/>
          }
          <div className="cover-paginate">
            {
              this.props.loading ? <div className="loading-io"><LoadingScroll></LoadingScroll></div>:null
            }
            <ReactPaginate
              previousLabel={<FontAwesomeIcon icon={faChevronLeft}/>}
              nextLabel={<FontAwesomeIcon icon={faChevronRight}/>}
              breakLabel={<span>...</span>}
              breakClassName={"break-me"}
              forcePage={Number(this.props.pageload)}
              pageCount={CatSlugList.page_count}
              marginPagesDisplayed={2}
              pageRangeDisplayed={2}
              onPageChange={this.handlePageClick}
              containerClassName={"pagination"}
              subContainerClassName={"pages pagination"}
              activeClassName={"active"}
            />
          </div>
          <div className="clear-fix"></div>
        </div>
      )
    }else {
      return (
        <div>
          <ProjectList linkBreacrm={linkBreacrm} data={CatSlugList.data} title={`${CatSlugList.category} のプロジェクト`}/>
        </div>
      )
    }
  }
  render() {
    const {CatSlugList} = this.props;
    if (CatSlugList) {
      document.title = CatSlugList.category+"｜KAKUSEIDA";
    }

    return (
      <div>
        <div>
          {
            CatSlugList ? this._renderView(CatSlugList) : <Loading></Loading>
          }
        </div>
      </div>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    CatSlugList: state.topPage.CatSlugList.data,
    loading:state.common.loading,
    pageload:state.topPage.CatSlugList.page
  }
}

export default connect(mapStateToProps, actions)(projectListCatSlug);
