/* @flow */

import React, { Component } from 'react';
import ProjectList from '../projectList';
import Loading from '../common/loading';
import LoadingScroll from '../common/loadingScroll';
import * as actions from '../../actions/TopPage';
import { connect } from 'react-redux';
// import * as actionsCom from '../../actions/common.js'
import ReactPaginate from 'react-paginate';
import axios from 'axios';
import ProjectListAll from "../projectList/projectListAll";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faChevronLeft, faChevronRight} from '@fortawesome/free-solid-svg-icons';

var CONFIG = require('../../config/common');

if (process.env.NODE_ENV === "development") {
  CONFIG = CONFIG.CONFIG.DEV;
}else {
  CONFIG = CONFIG.CONFIG.PRODUCT;
}
const ROOT_URL = CONFIG.ROOT_URL;

class projectListFromType extends Component {
    constructor(props) {
        // Required step: always call the parent class' constructor
        super(props);

    }
  componentWillMount() {
    this.dataCate = null;
      this.setState({
          dataCate: []
      })
    axios.get(ROOT_URL+'API_Top/API/category')
        .then(response =>{
          let dataCate =  response.data;
          this.dataCate = dataCate;
          this.setState({
            dataCate: dataCate
          })
        })
  }

  componentDidMount() {
    const {slug} = this.props.match.params;
    const {page} = this.props.match.params;
    var PageString = page.replace('page=', '');

    // if (!this.props.FromTypeList) {
    //
    //   this.props.fromTypePaginationlist(Number(PageString)-1,slug);
    // }
    //Get Categories
      this.props.fromTypePaginationlist(Number(PageString)-1,slug);
    const {cat } = this.props;

    //Call Categories
    if (!cat) {
      // this.props.fetchCat();
    }

  }

  handlePageClick = (data) => {
    let selected = data.selected;
    const {slug} = this.props.match.params;
    this.props.fromTypePaginationlist(selected,slug);
  };

  getPageList=()=>{
      const {FromTypeList} = this.props;

      if (FromTypeList.data.length !== 0 && FromTypeList.page_count > 1){
          return  <ReactPaginate
              previousLabel={<FontAwesomeIcon icon={faChevronLeft}/>}
              nextLabel={<FontAwesomeIcon icon={faChevronRight}/>}
              breakLabel={<span>...</span>}
              breakClassName={"break-me"}
              forcePage={Number(this.props.pageload)}
              pageCount={FromTypeList.page_count}
              marginPagesDisplayed={2}
              pageRangeDisplayed={2}
              onPageChange={this.handlePageClick}
              containerClassName={"pagination"}
              subContainerClassName={"pages pagination"}
              activeClassName={"active"}
          />
      }
      return null;

  }
  //Render View
  _renderView(FromTypeList) {
    let letCat  = [];
      letCat = this.state.dataCate ? this.state.dataCate : [];

    const linkBreacrm = [
      {last:true,label:FromTypeList.category,link:null}
    ]
   // if (FromTypeList.data.length !== 0) {
      if (1== 1) {
      return(
          <div className={" Top100 wrap-content-inner"}>
            {
                (this.props.loading==false)&&<ProjectListAll parThis={this} cat={letCat} linkBreacrm={linkBreacrm} data={FromTypeList.data} title={`${FromTypeList.category} のプロジェクト`}/>
            }
            <div className={"clearFloat"}></div>

              {
                this.props.loading ? <div className="loading-io"><LoadingScroll></LoadingScroll></div>:null
              }




            <div className="clear-fix"></div>
          </div>
      )
    }
  }
  render() {
    const {FromTypeList} = this.props;
    if (FromTypeList) {
      document.title = FromTypeList.category+"｜KAKUSEIDA";
    }

    return (
        <div>
          <div>
            {
              FromTypeList ? this._renderView(FromTypeList) : <Loading></Loading>
            }
          </div>
        </div>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    FromTypeList: state.topPage.FromTypeList.data,
    loading:state.common.loading,
    pageload:state.topPage.FromTypeList.page,
    // cate:state.common.Categories,
  }
}

export default connect(mapStateToProps, actions)(projectListFromType);
