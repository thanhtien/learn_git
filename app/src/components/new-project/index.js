/* @flow */

import React, { Component } from 'react';
import ProjectList from '../projectList';
import Loading from '../common/loading';
import LoadingScroll from '../common/loadingScroll';
import * as actions from '../../actions/TopPage';
import { connect } from 'react-redux';
import ReactPaginate from 'react-paginate';
import History from '../../history.js';
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faChevronLeft, faChevronRight} from "@fortawesome/free-solid-svg-icons";

class newsProjectsList extends Component {

  componentDidMount() {
    const {page} = this.props.match.params;
    var PageString = page.replace('page=', '');
    if (!this.props.dataNewsProject) {
      this.props.projectsNews(Number(PageString)-1);
    }

    if (this.props.dataNewsProject) {
      if (this.props.dataNewsProject.page_count < PageString) {
        History.push('/PageNotFound');
      }
    }
    document.title = "新着のプロジェクト｜KAKUSEIDA";


  }
  // shouldComponentUpdate(nextProps, nextState){
  //   const {page} = this.props.match.params;
  //   var PageString = page.replace('page=', '');
  //   if (!this.props.dataNewsProject) {
  //     this.props.projectsNews(Number(PageString)-1);
  //   }
  //   return true;
  // }

  handlePageClick = (data) => {
    let selected = data.selected;
    this.props.projectsNews(selected);
  };


  _renderView(dataNewsProject) {
    const linkBreacrm = [
      {last:true,label:"新着のプロジェクト",link:null}
    ]
    if (dataNewsProject.data.length !== 0) {
      return(
        <div>
          <ProjectList linkBreacrm={linkBreacrm} data={dataNewsProject.data} title={"新着のプロジェクト"}/>
          <div className="cover-paginate">
            {
              this.props.loading?<div className="loading-io"><LoadingScroll></LoadingScroll></div>:null
            }
            <ReactPaginate
              previousLabel={<FontAwesomeIcon icon={faChevronLeft}/>}
              nextLabel={<FontAwesomeIcon icon={faChevronRight}/>}
              breakLabel={<span>...</span>}
              breakClassName={"break-me"}
              pageCount={dataNewsProject.page_count}
              marginPagesDisplayed={2}
              forcePage={Number(this.props.pageLoad)}
              pageRangeDisplayed={2}
              onPageChange={this.handlePageClick}
              containerClassName={"pagination"}
              subContainerClassName={"pages pagination"}
              activeClassName={"active"} />
          </div>
          <div className="clear-fix"></div>
        </div>
      )
    }else {
      return(
        <div>
          <ProjectList linkBreacrm={linkBreacrm} data={dataNewsProject.data} title={"新着のプロジェクト"}/>
        </div>
      )


    }
  }

  render() {
    const {dataNewsProject} = this.props;
    return (
      <div>
        <div>
          {
            dataNewsProject ?
              this._renderView(dataNewsProject)
            : <Loading></Loading>
          }

        </div>
      </div>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    dataNewsProject: state.topPage.newsProject.data,
    loading:state.common.loading,
    pageLoad:state.topPage.newsProject.page
  }
}

export default connect(mapStateToProps, actions)(newsProjectsList);
