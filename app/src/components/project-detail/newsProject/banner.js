/* @flow */

import React, { Component } from 'react';


export default class Banner extends Component {
  render() {
    return (
      <div className="banner-css news-list-banner">
        <div className="wraper">
          <p className="title-white">最新の活動報告</p>
        </div>
      </div>
    );
  }
}
