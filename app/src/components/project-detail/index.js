import React, { Component } from 'react';
import { connect } from 'react-redux';
import Loading from '../common/loading';
import * as actions from '../../actions/projectDetail';
import Banner from './banner';
import InforProject from './inforProject';
import ProjectReturn from './projectReturn';
import ProjectReturnNonLogin from './projectReturnNonLogin';
import smoothScroll from '../../components/common/smoothScroll';
import Comment from './comment';
import Social from './social';
import LazyLoad from 'react-lazyload';
import ListNewsProject from './newsProject';
import LoadingScroll from '../common/loadingScroll';
import Breadcrumbs from '../Breadcrumbs';
import axios from "axios";
import {ADD_WISH_LIST, UNAUTH_USER} from "../../actions/types";
import History from "../../history";

var CONFIG = require('../../config/common');
if (process.env.NODE_ENV === "development") {
  CONFIG = CONFIG.CONFIG.DEV;
}else {
  CONFIG = CONFIG.CONFIG.PRODUCT;
}
const ROOT_URL = CONFIG.ROOT_URL;
class ProjectDetail extends Component {
  isAddAccess = 0;
  constructor(props) {
    super(props);
    this.state = {
      tab:1,
      typing:false,
      pageReport:0,
      loadingProject:false,
      loadingLeave:false
    };
  }

  typing(data) {
    this.setState({
      typing:data
    })
  }

  tagChange(tab) {
    this.setState({
      tab:tab
    })
  }

  tagChangeBanerScroll(){
    this.setState({
      tab:2
    }, () => {
      smoothScroll.scrollTo('project_return');

    })
  }

  componentDidMount() {
    const {id} = this.props.match.params;
    const {socket} = this.props;
    this.props.ProjectDetailGet(id);
    if(this.isAddAccess ==0){
      const {id} = this.props.match.params;
      this.addAccessList(id);
      this.isAddAccess = 1;
    }
    socket.emit("join-room", `comment_project_${id}`);
    if (this.props.socket.hasListeners("server-chat") !== true) {
      socket.on("server-chat", (data) => {
        if (window.location.pathname.indexOf('project-detail') !== -1) {
          this.props.CommentBoxSocket(data);
        }else {
          socket.emit("leave-room", `comment_project_${id}`);
        }
      });

      socket.on("server-chat-reply", (data) => {
        if (window.location.pathname.indexOf('project-detail') !== -1) {
          this.props.ReplyBoxSocket(data);
        }else {
          socket.emit("leave-room", `comment_project_${id}`);
        }
      });

      socket.on("server-typing", (data) => {

        if (window.location.pathname.indexOf('project-detail') !== -1) {
          this.typing(data);
        }else {
          socket.emit("leave-room", `comment_project_${id}`);
        }
      });
    }
  }


  htmlReturn(data) {
    return{
      __html:data
    }
  }

  //Show Hide Loading New Project add more
  toogleLoading() {
    this.setState({
      loadingProject:!this.state.loadingProject
    })
  }
  loadMoreProject( ) {
    const {id} = this.props.match.params;

    if (Number(this.props.data.report.totalPage) - 1 !== this.state.pageReport) {
      this.setState({
        pageReport:this.state.pageReport + 1
      }, () =>{
        this.props.loadMoreNewsReport(id,this.state.pageReport,()=>this.toogleLoading());
      })

    }
  }

  loadingLeave(){
    this.setState({
      loadingLeave:!this.state.loadingLeave
    })
  }


  renderNewProject(data) {
    if (Number(data.report.totalPage) - 1 !== this.state.pageReport) {
      return(
        <div className="pd-box-btn load-more-news-project" style={{position:'relative'}}>
          {
            this.state.loadingProject ?
            <div className="cover-loading" style={{textAlign:'center'}}>
              <LoadingScroll></LoadingScroll>
            </div> : null
          }
          <span className="info-button" onClick={()=>this.loadMoreProject()} >もっと読み込む</span>
        </div>
      )
    }else {
      return null;
    }
  }

  uuidv4 =() =>{
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
      return v.toString(16);
    });
  }
  addAccessListNoLogin= (project_id) =>{
    if (localStorage.getItem('NologIn')== null) {

      localStorage.setItem('NologIn', this.uuidv4());

    }
    const token1 = localStorage.getItem('NologIn');

    const config = {

    };
    var url = ROOT_URL+'v2/user/ProjectsAddNoLogin/addAccess';

    var data = {
      project_id:project_id,
      user_id:token1
    };

    axios.post(url, data ,config)
        .then(response => {
        }).catch((error) => {
    });
  };

   addAccessList= (project_id) =>{


    if(localStorage.getItem('token')==null){
      this.addAccessListNoLogin(project_id);
      return;
    }
    const token = 'Bearer '+ localStorage.getItem('token');
    const config = {
      headers: {
        "Authorization" : token,
      }
    };
    var url = ROOT_URL+'v2/user/Projects/addAccess';

    var data = {
      project_id:project_id
    }

    axios.post(url, data ,config)
        .then(response => {
        }).catch((error) => {
    });
  };

  render() {
    //CommentBox is function add comment
    const {
      data,
      loading,
      profile,
      CommentBox,
      ReplyBox,
      LoadMoreComment,
      loadMoreReply,
      socket,
      firebaseCURL,
      deleteReply,
      deleteComment,
      CommentBoxEdit,
      ReplyBoxEdit,
      stopDonateFanClubFromProjectDetail,
      leaveToWishList,
      addToWishList
    } = this.props;
    const {tab,typing,loadingLeave} = this.state;
    const {id} = this.props.match.params;
    const shareLink = CONFIG.MAIN_URL+'project-detail/'+id;



    var linkBreacrm;


    if (loading) {
      return(
        <Loading></Loading>
      )
    }
    if(loadingLeave){
      return(
        <Loading></Loading>
      )
    }

    if (data) {

      document.title = data.project.project_name+"｜KAKUSEIDA";
      var label = "";

      if (data.project.project_name.length > 10) {
        label = data.project.project_name.substring(0,10) + '...';
      }else {
        label = data.project.project_name.substring(0,10);

      }

      linkBreacrm = [
        {last:true,label:label,link:null}
      ]



    }
    if (profile) {
      var ownerProject;
      if (data) {
        if (data.project.user_id === profile.id) {
           ownerProject = true;
        }else {
          ownerProject = false;
        }
      }

    }


    return (
      <div>

        {
          data  ?
          <div>
            <div className="pd-banner">

              {
                <Banner
                  addToWishList={addToWishList}
                  leaveToWishList={leaveToWishList}
                  stopStatus={data.project.status_fanclub}
                  tagChange={ () => this.tagChangeBanerScroll() }
                  tab={tab}
                  profile={profile}
                  data={data} />
              }
            </div>

            <div className="wraper">
              <div className="cover-Breadcrumbs project-detail-bread">
                <Breadcrumbs linkBreacrm={linkBreacrm}></Breadcrumbs>
              </div>
              <div className="tab-area">
                <ul>
                  <li onClick={ () => this.tagChange(1) } className={ tab === 1 ? "active":""}>プロジェクト詳細</li>
                  <li onClick={ () => this.tagChange(2) } className={ tab === 2 ? "active":""} >リターン</li>
                </ul>
              </div>
              {/* .Social*/}
              <Social thumbnail={data.project.thumbnail} des={data.project.description} title={data.project.project_name} shareLink={shareLink}></Social>
              {/* Social */}

                <div className={ tab === 1 ? "active pd-area animated fadeIn":"pd-area"}  id="tab-2">
                  {
                    data.project.project_content ?
                    <div className="div-content" dangerouslySetInnerHTML={this.htmlReturn(data.project.project_content)} ></div> : <p style={{textAlign:'center', border: "3px dotted #0071BA",padding:"10px"}}>コンテンツがまだありません。</p>
                  }

                  {/* .WYSIWYG */}


                  {
                    data.report.data.length !== 0 ? <ListNewsProject report={data.report} idProject={id}/> : null
                  }



                  {
                    data.report.data.length > 0 ? this.renderNewProject(data) : null
                  }

                </div>


              {/* .tab-area */}


              {/* #tab-1 */}


                <div className={ tab === 2 ? "active pd-area animated fadeIn":"pd-area"} id="tab-1">
                  <InforProject
                    profile={profile}
                    tagChange={() => this.tagChange(1)}
                    data={data.project} />
                  <div className="pd-box pd-box-2">
                    {
                      data.project_return.length !== 0 ?
                      <h4 id="project_return" className="pd-box-title">リターンを選ぶ</h4>:
                      null
                    }

                      <div className="return-list">
                        {
                          profile ?
                          <ProjectReturn
                            stopStatus={data.project.status_fanclub}
                            loadingLeave={()=>this.loadingLeave()}
                            stopDonateFanClubFromProjectDetail={stopDonateFanClubFromProjectDetail}
                            typeProject={data.project.project_type}
                            FanClubList={profile.listFanClub}
                            number_month={data.project.number_month}
                            date={data.project.format_collection_end_date} owner={data.project.user_id}
                            currentUser={profile.id}
                            data={data.project_return}></ProjectReturn> :
                          <ProjectReturnNonLogin
                            projectID={data.project.id}
                            number_month={data.project.number_month} date={data.project.format_collection_end_date} data={data.project_return}></ProjectReturnNonLogin>
                        }

                      </div>
                  </div>
                </div>
                {/* #tab-2 */}



            </div>

            {/*
              ReplyBox is function add Reply
              CommentBox is function add comment
              firebaseCURL is fucntion noti
              comment is data comment
              page_count is total page comment
              profile is profile user
              projectId is project id
              LoadMoreComment is loadmore Comment
              LoadMoreReply is loadmore Reply Comment
              socket real time object
              deleteReplyOrComment
            */}

            <LazyLoad height={200}>


              <Comment
                ReplyBoxEdit={ReplyBoxEdit}
                ownerProject={ownerProject}
                deleteReply={deleteReply}
                CommentBoxEdit={CommentBoxEdit}
                deleteComment={deleteComment}
                typing={typing}
                firebaseCURL={firebaseCURL}
                socket={socket}
                loadMoreReply={loadMoreReply}
                LoadMoreComment={LoadMoreComment}
                ReplyBox={ReplyBox}
                comment={data.comment.data}
                CommentBox={CommentBox}
                page_count={data.comment.page_count}
                profile={profile}
                projectId={id}/>
            </LazyLoad>


            {/* Comment */}
          </div>: <Loading></Loading>
        }


      </div>

    );


  }
}

const mapStateToProps = (state) => {

  return {
    data: state.ProjectDetail.projectDetail,
    loading:state.common.loading,
    profile:state.common.profile,
  }
}

export default connect(mapStateToProps, actions)(ProjectDetail);
