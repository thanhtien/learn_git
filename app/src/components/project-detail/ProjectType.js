import React, { Component } from 'react';
import { connect } from 'react-redux';
import Loading from '../common/loading';
import * as actions from '../../actions/projectDetail';
import Banner from './banner';
import InforProject from './inforProject';
import ProjectReturn from './projectReturn';
import ProjectReturnNonLogin from './projectReturnNonLogin';
import smoothScroll from '../../components/common/smoothScroll';
import Comment from './comment';
import Social from './social';
import LazyLoad from 'react-lazyload';
import ListNewsProject from './newsProject';
import LoadingScroll from '../common/loadingScroll';
import Breadcrumbs from '../Breadcrumbs';
import axios from "axios";
import '../asset/style/ProjectType.scss';
import {ADD_WISH_LIST, UNAUTH_USER} from "../../actions/types";
import History from "../../history";
import BannerProjectType from "./BannerProjectType";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faHeart} from "@fortawesome/free-solid-svg-icons";

var CONFIG = require('../../config/common');
if (process.env.NODE_ENV === "development") {
  CONFIG = CONFIG.CONFIG.DEV;
}else {
  CONFIG = CONFIG.CONFIG.PRODUCT;
}
const ROOT_URL = CONFIG.ROOT_URL;
class ProjectType extends Component {
  isAddAccess = 0;
  constructor(props) {
    super(props);
    this.state = {
      tab:1,
      typing:false,
      pageReport:0,
      loadingProject:false,
      loadingLeave:false
    };
  }

  typing(data) {
    this.setState({
      typing:data
    })
  }

  tagChange(tab) {
    this.setState({
      tab:tab
    })
  }

  tagChangeBanerScroll(){
    this.setState({
      tab:2
    }, () => {
      smoothScroll.scrollTo('project_return');

    })
  }

  componentDidMount() {
    const {id} = this.props.match.params;
    const {socket} = this.props;
    this.props.ProjectDetailGet(id);
    if(this.isAddAccess ==0){
      const {id} = this.props.match.params;
      this.addAccessList(id);
      this.isAddAccess = 1;
    }
    socket.emit("join-room", `comment_project_${id}`);
    if (this.props.socket.hasListeners("server-chat") !== true) {
      socket.on("server-chat", (data) => {
        if (window.location.pathname.indexOf('project-detail') !== -1) {
          this.props.CommentBoxSocket(data);
        }else {
          socket.emit("leave-room", `comment_project_${id}`);
        }
      });

      socket.on("server-chat-reply", (data) => {
        if (window.location.pathname.indexOf('project-detail') !== -1) {
          this.props.ReplyBoxSocket(data);
        }else {
          socket.emit("leave-room", `comment_project_${id}`);
        }
      });

      socket.on("server-typing", (data) => {

        if (window.location.pathname.indexOf('project-detail') !== -1) {
          this.typing(data);
        }else {
          socket.emit("leave-room", `comment_project_${id}`);
        }
      });
    }
  }


  htmlReturn(data) {
    return{
      __html:data
    }
  }

  //Show Hide Loading New Project add more
  toogleLoading() {
    this.setState({
      loadingProject:!this.state.loadingProject
    })
  }
  loadMoreProject( ) {
    const {id} = this.props.match.params;

    if (Number(this.props.data.report.totalPage) - 1 !== this.state.pageReport) {
      this.setState({
        pageReport:this.state.pageReport + 1
      }, () =>{
        this.props.loadMoreNewsReport(id,this.state.pageReport,()=>this.toogleLoading());
      })

    }
  }

  loadingLeave(){
    this.setState({
      loadingLeave:!this.state.loadingLeave
    })
  }


  renderNewProject(data) {
    if (Number(data.report.totalPage) - 1 !== this.state.pageReport) {
      return(
        <div className="pd-box-btn load-more-news-project" style={{position:'relative'}}>
          {
            this.state.loadingProject ?
            <div className="cover-loading" style={{textAlign:'center'}}>
              <LoadingScroll></LoadingScroll>
            </div> : null
          }
          <span className="info-button" onClick={()=>this.loadMoreProject()} >もっと読み込む</span>
        </div>
      )
    }else {
      return null;
    }
  }

  uuidv4 =() =>{
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
      return v.toString(16);
    });
  }
  addAccessListNoLogin= (project_id) =>{
    if (localStorage.getItem('NologIn')== null) {

      localStorage.setItem('NologIn', this.uuidv4());

    }
    const token1 = localStorage.getItem('NologIn');

    const config = {

    };
    var url = ROOT_URL+'v2/user/ProjectsAddNoLogin/addAccess';

    var data = {
      project_id:project_id,
      user_id:token1
    };

    axios.post(url, data ,config)
        .then(response => {
        }).catch((error) => {
    });
  };

   addAccessList= (project_id) =>{


    if(localStorage.getItem('token')==null){
      this.addAccessListNoLogin(project_id);
      return;
    }
    const token = 'Bearer '+ localStorage.getItem('token');
    const config = {
      headers: {
        "Authorization" : token,
      }
    };
    var url = ROOT_URL+'v2/user/Projects/addAccess';

    var data = {
      project_id:project_id
    }

    axios.post(url, data ,config)
        .then(response => {
        }).catch((error) => {
    });
  };

  render() {
    //CommentBox is function add comment
    const {
      data,
      loading,
      profile,
      CommentBox,
      ReplyBox,
      LoadMoreComment,
      loadMoreReply,
      socket,
      firebaseCURL,
      deleteReply,
      deleteComment,
      CommentBoxEdit,
      ReplyBoxEdit,
      stopDonateFanClubFromProjectDetail,
      leaveToWishList,
      addToWishList
    } = this.props;
    const {tab,typing,loadingLeave} = this.state;
    const {id} = this.props.match.params;
    const shareLink = CONFIG.MAIN_URL+'project-detail/'+id;



    var linkBreacrm;


    if (loading) {
      return(
        <Loading></Loading>
      )
    }
    if(loadingLeave){
      return(
        <Loading></Loading>
      )
    }

    if (data) {

      document.title = data.project.project_name+"｜KAKUSEIDA";
      var label = "";

      if (data.project.project_name.length > 10) {
        label = data.project.project_name.substring(0,10) + '...';
      }else {
        label = data.project.project_name.substring(0,10);

      }

      linkBreacrm = [
        {last:true,label:label,link:null}
      ]



    }
    if (profile) {
      var ownerProject;
      if (data) {
        if (data.project.user_id === profile.id) {
           ownerProject = true;
        }else {
          ownerProject = false;
        }
      }

    }

    var StatusWishList = false;
    if (profile) {
      if (profile.wish_list) {
        var wishList = profile.wish_list.split(",");
        if (data) {
          if (wishList.indexOf(data.project.id) > -1) {
            StatusWishList = true;
          }
        }
      }
    }
    return (
      <div>

        {
          data  ?
          <div className="ProjectType">

            <div className="wraper rowFirst">
              <div className={"firstButton"}>
                プレビュー中
              </div>
            </div>
            {/*banner*/}
            <div className="pd-banner">

              {
                <BannerProjectType
                    addToWishList={addToWishList}
                    leaveToWishList={leaveToWishList}
                    stopStatus={data.project.status_fanclub}
                    tagChange={ () => this.tagChangeBanerScroll() }
                    tab={tab}
                    profile={profile}
                    data={data} />
              }

            </div>
            {/*row child banner*/}
            <div className="wraper rowChild">
              <div className="leftChild">
            <div className="pannel-head">
              {
                data.project.project_name
              }
              {/* .Social*/}
            <div className={"divRight"}>  <Social thumbnail={data.project.thumbnail} des={data.project.description} title={data.project.project_name} shareLink={shareLink}></Social>
            </div>
              {/* Social */}

              {
                data.project.status_fanclub === 'stop' ? <span style={{color:'#b33939'}}>(解散済み)</span> : null
              }
              {
                profile && data.project.status_fanclub !== 'stop' ?
                    <button
                        onClick={
                          !StatusWishList?
                              ()=>this.addToWishList(data.project.id) :
                              ()=>this.leaveToWishList(data.project.id) }
                        className="heart-project-detail"
                        title="気に入り">
                      {
                        StatusWishList ?
                            <FontAwesomeIcon style={{color:'#ef2d2d'}} icon={faHeart}></FontAwesomeIcon>:

                            <FontAwesomeIcon icon={faHeart}></FontAwesomeIcon>
                      }

                    </button> :
                    null
              }
            </div>
              </div>
              <div className="rightChild">
                {
                  data.project.status_fanclub !== 'stop' ? <p className="pannel-btn pannel-btn-100">
                    <div className={"btnRightChild"} onClick={ () => this.scrollToReturn() } style={{cursor:'pointer'}}>
                      <div className={"p1Button"} >プロジェクトを支援する</div>
                      <div className={"p2Button"} >2,000円から支援できます</div>
                    </div>
                  </p> : null
                }
                <div className={"rightChildBottom"}>
                  このプロジェクトは、All-In方式です。
                  目標金額に関わらず、2019/05/15 23:59までに集まった金額がファンディング
                  されます。??????????
                </div>
              </div>
            </div>
            {/*row 4 button*/}
            <div className="wraper row4Button">
              <div className="btn1">
                ホーム
              </div>
              <div className="btn2">
                リターン
              </div>
              <div className="btn3">
                活動報告
              </div>
              <div className="btn4">
                コメント
              </div>
              <div className="textRight">
                ⭐️お気に入り登録する???
              </div>
            </div>
              {/*row author*/}
          <div className="wraper rowAuthor">
              <div className="auTitle">
                  プロジェクト
              </div>
              <div className="auMain">
                  <div className="auLeft">
                      <div className="imgOn">
                      <img src={"https://cungnhau.net/wp-content/uploads/2018/11/suc-manh-cua-loi-cam-on-trong-cac-tinh-huong-giao-tiep.jpg"} className={"auImage"} />
                      </div>
                      <div className="textImg">
                          <div className="texLeft">コメント
                          </div>
                          <div className="textRight">
                              もっと見る
                          </div>
                      </div>


                  </div>
                  {/*auRight*/}
                  <div className="auRight">
                      <div className="auRight1">
                          <div className="auLogo">
                              <img src={"http://tamlyhoctinhyeu.vn/wp-content/uploads/2019/01/man-head.jpg"} className={"auLogo"} />
                          </div>
                          <div className="auName">
                              YUKO
                          </div>
                          <div className="auSubIcon">
                              <img src={"/img/common/periscope-logo.png"}  /> <p>   大阪府</p>
                          </div>

                          <div className="auDesc">
                              大阪カジノディーラースクールの王子谷です。
                              当スクールは２０１２年に創立し、これまでに多くのカジノデーラー
                              を指導してきました。２０２５年の万博を目指していよいよ日本にも
                              カジノができることになりました。我々は２０２５年までに多くのカ
                              ジノディーラーを育てるべく日々精進しています。今回は門戸を広げ
                              ようとクラウドファウンディングを通して資金を募る運びとなりまし
                              た。
                          </div>
                          <div className="auBtn">
                              <div className="auBtnIn"> メッセージを送る  </div>
                          </div>
                      </div>
                      {/*faq*/}
                      <div className="auRightFaq">
                          <div className="auFaq">
                              <div>Q. 支払い方法は何がありますか？ </div>
                              <div>Q. クレジットカードの決済はいつ行われますか？</div>
                              <div>Q. プロジェクトに関する質問はどうすればいいですか？</div>
                              <div> Q. 間違って支援した場合はどうなりますか？</div>

                          </div>
                      </div>
                  </div>


              </div>
          </div>
            <div className="wraper">
              <div className="cover-Breadcrumbs project-detail-bread">
                <Breadcrumbs linkBreacrm={linkBreacrm}></Breadcrumbs>
              </div>
              <div className="tab-area">
                <ul>
                  <li onClick={ () => this.tagChange(1) } className={ tab === 1 ? "active":""}>プロジェクト詳細</li>
                  <li onClick={ () => this.tagChange(2) } className={ tab === 2 ? "active":""} >リターン</li>
                </ul>
              </div>


                <div className={ tab === 1 ? "active pd-area animated fadeIn":"pd-area"}  id="tab-2">
                  {
                    data.project.project_content ?
                    <div className="div-content" dangerouslySetInnerHTML={this.htmlReturn(data.project.project_content)} ></div> : <p style={{textAlign:'center', border: "3px dotted #0071BA",padding:"10px"}}>コンテンツがまだありません。</p>
                  }

                  {/* .WYSIWYG */}


                  {
                    data.report.data.length !== 0 ? <ListNewsProject report={data.report} idProject={id}/> : null
                  }



                  {
                    data.report.data.length > 0 ? this.renderNewProject(data) : null
                  }

                </div>


              {/* .tab-area */}


              {/* #tab-1 */}


                <div className={ tab === 2 ? "active pd-area animated fadeIn":"pd-area"} id="tab-1">
                  <InforProject
                    profile={profile}
                    tagChange={() => this.tagChange(1)}
                    data={data.project} />
                  <div className="pd-box pd-box-2">
                    {
                      data.project_return.length !== 0 ?
                      <h4 id="project_return" className="pd-box-title">リターンを選ぶ</h4>:
                      null
                    }

                      <div className="return-list">
                        {
                          profile ?
                          <ProjectReturn
                            stopStatus={data.project.status_fanclub}
                            loadingLeave={()=>this.loadingLeave()}
                            stopDonateFanClubFromProjectDetail={stopDonateFanClubFromProjectDetail}
                            typeProject={data.project.project_type}
                            FanClubList={profile.listFanClub}
                            number_month={data.project.number_month}
                            date={data.project.format_collection_end_date} owner={data.project.user_id}
                            currentUser={profile.id}
                            data={data.project_return}></ProjectReturn> :
                          <ProjectReturnNonLogin
                            projectID={data.project.id}
                            number_month={data.project.number_month} date={data.project.format_collection_end_date} data={data.project_return}></ProjectReturnNonLogin>
                        }

                      </div>
                  </div>
                </div>
                {/* #tab-2 */}



            </div>



            <LazyLoad height={200}>


              <Comment
                ReplyBoxEdit={ReplyBoxEdit}
                ownerProject={ownerProject}
                deleteReply={deleteReply}
                CommentBoxEdit={CommentBoxEdit}
                deleteComment={deleteComment}
                typing={typing}
                firebaseCURL={firebaseCURL}
                socket={socket}
                loadMoreReply={loadMoreReply}
                LoadMoreComment={LoadMoreComment}
                ReplyBox={ReplyBox}
                comment={data.comment.data}
                CommentBox={CommentBox}
                page_count={data.comment.page_count}
                profile={profile}
                projectId={id}/>
            </LazyLoad>


            {/* Comment */}
          </div>: <Loading></Loading>
        }


      </div>

    );


  }
}

const mapStateToProps = (state) => {

  return {
    data: state.ProjectDetail.projectDetail,
    loading:state.common.loading,
    profile:state.common.profile,
  }
}

export default connect(mapStateToProps, actions)(ProjectType);
