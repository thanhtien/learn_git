/* @flow */

import React, { Component } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faReply } from '@fortawesome/free-solid-svg-icons';
import InputBox from './InputBoxReply';
import InputBoxEdit from './InputBoxReplyEdit';
import CommentContent from './CommentContent';
import ReplyFunction from './ReplyFunction';

import LoadingScroll from '../../common/loadingScroll';
import FormatFunc from '../../common/FormatFunc';

export default class Reply extends Component {

  constructor(props) {
    super(props);
    this.state = {
      toogleComment:false,
      toogleCommentEdit:false,
      dataEdit:"",
      loadingReply:false
    };
  }

  ToogleComment() {

    this.setState({
      toogleComment:!this.state.toogleComment
    })
  }

  toogleCommentEdit(data) {
    this.setState({
      toogleCommentEdit:!this.state.toogleCommentEdit,
      toogleComment:false,
      dataEdit:data.comment
    })
  }

  closeEdit(){
    this.setState({
      toogleCommentEdit:false,
      toogleComment:false,
      dataEdit:""
    })
  }

  setDataEdit(data) {

    this.setState({
      dataEdit:data
    })
  }


  LoadMoreReply(){
    const {commentId,projectId,_loadingReply} = this.props;
    this.props.loadMoreReply(projectId,commentId,_loadingReply);
  }


  _renderLoadingButton() {
    const {length,current} = this.props;
    return (
      length.toString() === (current + 1).toString() ? <div onClick={()=>this.LoadMoreReply()} className="load-more-reply">もっと読み込む<FontAwesomeIcon icon={faReply} /></div> : null
    )
  }
  _renderLoading() {
    const {length,current,loadingReply} = this.props;
    if ((length - 1).toString() === current.toString()) {

      return loadingReply ? <LoadingScroll /> : null

    }
  }
  render() {
    const {profile,comment,commentId,projectId,id,ReplyBox,getMoreReply,socket,firebaseCURL,deleteReply,ReplyBoxEdit} = this.props;
    const {dataEdit} = this.state;

    return (
      <li>
         <div className="comment-avatar"><img src={comment.profileImageURL} alt=""/></div>
         <div className="comment-box">
            <div className="comment-head">
              {
                profile ?
                <h6 className={  comment.user_id === profile.id ? "comment-name by-author" : "comment-name"}>{comment.username}</h6> : <h6 className={'comment-name'}>{comment.username}</h6>

              }

               <span>
                
                 <FormatFunc date={comment.created}></FormatFunc>
               </span>
            </div>

            <CommentContent data={comment.comment}></CommentContent>

            {
              profile && comment?
              <ReplyFunction
                toogleCommentEdit={(data)=>this.toogleCommentEdit(data)}
                deleteReply={deleteReply}
                profile={profile}
                comment={comment}
                ToogleComment={()=>this.ToogleComment()} >
              </ReplyFunction> : null
            }
         </div>

         <div  className={
             (this.state.toogleComment ? "reply-reply-here active  animated fadeIn" : "reply-reply-here fadeOut animated")
           }>

           <InputBox
             firebaseCURL={firebaseCURL}
             socket={socket} id={id}
             closeAdd={() => this.ToogleComment()}
             form={projectId}
             projectId={projectId}
             commentId={commentId}
             ReplyBox={ReplyBox}
             profile={profile}
            />

         </div>

         <div  className={
             (this.state.toogleCommentEdit ? "reply-reply-here active  animated fadeIn" : "reply-reply-here fadeOut animated")
           }>

           <InputBoxEdit
             ReplyBoxEdit={ReplyBoxEdit}
             dataEdit={dataEdit}
             firebaseCURL={firebaseCURL}
             socket={socket}
             id={id}
             closeEdit={() => this.closeEdit()}
             form={projectId}
             setDataEdit={()=>this.setDataEdit()}
             projectId={projectId}
             commentId={commentId}
             replyId={comment.id}
             ReplyBox={ReplyBox}
             profile={profile}
            />

         </div>




         {
          getMoreReply ?
          this._renderLoadingButton():null
         }
         {
           this._renderLoading()
         }
      </li>
    );
  }
}
