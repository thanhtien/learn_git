/* @flow */

import React, { Component } from 'react';
import LoadingScroll from '../../common/loadingScroll';
class InputBox extends Component {

  constructor(props) {
    super(props);
    this.state = {
      value: '',
      loadingInput:false
    };

  }

  handleChange(event) {
    this.setState({value: event.target.value});
  }

  handleSubmit(event) {

    if (this.state.value !== '') {
      var data = {
        project_id:this.props.projectId,
        comment:this.state.value
      }
      var firebaseData = {
        "action":"sendProjectComment",
        "project_id":this.props.projectId,
        "comment":this.state.value
      }
      this.props.CommentBox(data,()=>this.LoadingInput(),this.props.socket);
      this.props.firebaseCURL(firebaseData);
      this.setState({
        value:''
      })
    }

    event.preventDefault();
  }

  LoadingInput() {
    this.setState({
      loadingInput:!this.state.loadingInput
    })

  }

  typing() {
    var data = {
      project_id:this.props.projectId,
      status:true
    }
    this.props.socket.emit('user-typing',data);
  }
  stopType() {
    var data = {
      project_id:this.props.projectId,
      status:false
    }
    this.props.socket.emit('user-typing',data);
  }



  render() {
    const {profile,typing} = this.props;
    const {loadingInput} = this.state;
    return (
      <div className="group">

        {
          profile ?
          <p className="avatar-comment">
            <img src={profile.profileImageURL} alt="img"/>
          </p>
          : null
        }
        <div className="group">
          {
            profile ?
            <p className="avatar-comment">
              <img src={profile.profileImageURL} alt="img"/>
            </p>
            : null
          }


            <textarea
              placeholder="コメント"
              value={this.state.value}
              onFocus ={()=>this.typing()}
              onBlur ={()=>this.stopType()}
              onChange={(event)=>this.handleChange(event)} />
            <div className="border"></div>
            <span className="highlight"></span>
            <span className="bar"></span>
            <button onClick={(event)=>this.handleSubmit(event)} type="submit" className="icon send" />
            {
              loadingInput ? <div className="loading-e"><LoadingScroll></LoadingScroll></div> : null
            }


            {
              typing ?
              <div className="loading-dot-here">
                <div className="lds-ellipsis"><div></div><div></div><div></div><div></div></div>
              </div> :
              null
            }


        </div>



      </div>
    );
  }
}


export default InputBox;
