/* @flow */

import React, { Component } from 'react';
import LinesEllipsis from 'react-lines-ellipsis';

export default class MyComponent extends Component {

  constructor(props) {
    super(props);
    this.state = {
      line:3
    };
  }

  toogleShow() {
    if (this.state.line === 3) {
      this.setState({
        line:999999999999999
      })
    }else {
      this.setState({
        line:3
      })
    }

  }
  render() {
    return (
      <div className="comment-content">
        <pre>
          <LinesEllipsis
            text={this.props.data}
            maxLine={this.state.line}
            ellipsis={<span>... <span style={{color:'#147efb',cursor:'pointer'}} onClick={()=>this.toogleShow()}> {this.state.line === 3 ? "続きを読む" : ''}</span></span>}
            trimRight
            basedOn='letters'
          />
        </pre>
        {
          this.state.line !== 3 ? <span style={{marginLeft:'5px',color:'#147efb',cursor:'pointer'}} onClick={()=>this.toogleShow()}> 省略する  </span> : null
        }
      </div>
    );
  }

}
