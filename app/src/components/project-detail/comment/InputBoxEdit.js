/* @flow */

import React, { Component } from 'react';
import LoadingScroll from '../../common/loadingScroll';
class InputBox extends Component {

  constructor(props) {
    super(props);
    this.state = {
      value: '',
      loadingInput:false
    };

  }

  handleChange(event) {
    this.setState({
      value:event.target.value
    })
    this.props.setDataEdit(event.target.value);
  }

  handleSubmit(event) {
    const {commentId} = this.props;
    if (this.state.value !== '') {
      var data = {
        id:commentId,
        comment:this.state.value
      }

      this.props.CommentBoxEdit(data,()=>this.LoadingInput());

      this.setState({
        value:''
      })
    }

    event.preventDefault();
  }

  LoadingInput() {
    this.setState({
      loadingInput:!this.state.loadingInput
    },function () {
      this.props.closeEdit();
    })
  }

  typing() {
    var data = {
      project_id:this.props.projectId,
      status:true
    }
    this.props.socket.emit('user-typing',data);
  }
  stopType() {
    var data = {
      project_id:this.props.projectId,
      status:false
    }
    this.props.socket.emit('user-typing',data);
  }



  render() {
    const {profile,typing , dataEdit} = this.props;
    const {loadingInput} = this.state;

    return (
      <div className="group">
        {
          profile ?
          <p className="avatar-comment">
            <img src={profile.profileImageURL} alt="img"/>
          </p>
          : null
        }


          <textarea
            placeholder="コメント"
            value={dataEdit}
            onFocus ={()=>this.typing()}
            onBlur ={()=>this.stopType()}
            onChange={(event)=>this.handleChange(event)} />
          <div className="border"></div>
          <span className="highlight"></span>
          <span className="bar"></span>
          <button onClick={(event)=>this.handleSubmit(event)} type="submit" className="icon send" />
          {
            loadingInput ? <div className="loading-e"><LoadingScroll></LoadingScroll></div> : null
          }


          {
            typing ?
            <div className="loading-dot-here">
              <div className="lds-ellipsis"><div></div><div></div><div></div><div></div></div>
            </div> :
            null
          }


      </div>
    );
  }
}


export default InputBox;
