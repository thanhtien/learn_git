/* @flow */

import React, { Component } from 'react';

import ReplyList from './ReplyList';
import InputBox from './InputBoxReply';
import InputBoxEdit from './InputBoxEdit';
import CommentContent from './CommentContent';

import CommentFunction from './CommentFunction';

import FormatFunc from '../../common/FormatFunc';

export default class Comment extends Component {
  constructor(props) {
    super(props);
    this.state = {
      toogleComment:false,
      toogleCommentEdit:false,
      dataEdit:"",
      loadingReply:false

    };
  }
  ToogleComment() {
    this.setState({
      toogleComment:!this.state.toogleComment,
      toogleCommentEdit:false
    })
  }

  toogleCommentEdit(data) {
    this.setState({
      toogleCommentEdit:!this.state.toogleCommentEdit,
      toogleComment:false,
      dataEdit:data.comment
    })
  }
  closeEdit(){
    this.setState({
      toogleCommentEdit:false,
      toogleComment:false,
      dataEdit:""
    })
  }

  setDataEdit(data) {

    this.setState({
      dataEdit:data
    })
  }

  _loadingReply(){
    this.setState({
      loadingReply:!this.state.loadingReply
    })
  }

  render() {
    const {profile,comment,projectId,ReplyBox,loadMoreReply,socket,typing,firebaseCURL,deleteComment,deleteReply,CommentBoxEdit,ownerProject,ReplyBoxEdit} = this.props;
    const {loadingReply,dataEdit} = this.state;

    return (

      <li className="animated fadeIn">

         <div className="comment-main-level">
            <div className="comment-avatar"><img src={comment.profileImageURL} alt=""/></div>
            <div className="comment-box">
               <div className="comment-head">

                  {
                    profile ?
                    <h6 className={  comment.user_id === profile.id ? "comment-name by-author" : "comment-name"}>{comment.username}</h6> : <h6 className={'comment-name'}>{comment.username}</h6>

                  }


                  <span>
                    <FormatFunc date={comment.created} ></FormatFunc>
                  </span>




               </div>

               <CommentContent data={comment.comment}></CommentContent>
              {
                /**
                 * [ToogleComment for show hide replace]
                 * [comment data object]
                 * [deleteReply]
                 * [deleteComment]
                 * @type {Object}
                 */
              }

              {
                profile && comment?
                <CommentFunction
                  toogleCommentEdit={(data)=>this.toogleCommentEdit(data)}
                  deleteComment={deleteComment}
                  deleteReply={deleteReply}
                  profile={profile}
                  comment={comment}
                  ownerProject={ownerProject}
                  ToogleComment={()=>this.ToogleComment()} >
                </CommentFunction> : null
              }




            </div>

         </div>
         <div className={(this.state.toogleComment ? "reply-here active  animated fadeIn" : "reply-here fadeOut animated")}>
          {
            profile ?
            <InputBox
              typing={typing}
              firebaseCURL={firebaseCURL}
              socket={socket}
              ReplyBox={ReplyBox}
              closeAdd={() => this.ToogleComment()}
              commentId={comment.id}
              projectId={projectId}
              profile={profile}
              selected={comment.selected}/>:null
          }
         </div>


         <div className={(this.state.toogleCommentEdit ? "reply-here active  animated fadeIn" : "reply-here fadeOut animated")}>
          {
            profile ?
            <InputBoxEdit
              typing={typing}
              dataEdit={dataEdit}
              setDataEdit={()=>this.setDataEdit()}
              firebaseCURL={firebaseCURL}
              socket={socket}
              CommentBoxEdit={CommentBoxEdit}
              closeEdit={() => this.closeEdit()}
              commentId={comment.id}
              projectId={projectId}
              profile={profile}
              selected={comment.selected}/>:null
          }
         </div>


         <ul className="comments-list reply-list">

          {/* Reply*/}
          {
            comment ?

            comment.reply.map( (item,i) => {
              return <ReplyList
                ReplyBoxEdit={ReplyBoxEdit}
                deleteReply={deleteReply}
                typing={typing}
                firebaseCURL={firebaseCURL}
                socket={socket}
                getMoreReply={comment.getMoreReply}
                _loadingReply={()=>this._loadingReply()}
                loadingReply={loadingReply}
                loadMoreReply={loadMoreReply}
                key={i}
                current={i}
                length={comment.reply.length}
                projectId={projectId}
                commentId={comment.id}
                ReplyBox={ReplyBox}
                profile={profile}
                comment={item} ></ReplyList>
            })
            :null
          }

          {/* Reply*/}

         </ul>




      </li>

    );
  }
}
