import React, { Component } from 'react';

import {Helmet} from "react-helmet";

import StyleComponent from '../components/asset/';


class ErrorPage extends Component {



  render() {

    return (

      <div className="my-app">



        {/*  Head */}
        <Helmet>
          <style type="text/css">{`
            html,button,input,select,textarea{font-family:"メイリオ","Hiragino Kaku Gothic Pro","ヒラギノ角ゴ Pro W3","ＭＳ Ｐゴシック",sans-serif}body{margin:0;font-family:"メイリオ","Hiragino Kaku Gothic Pro","ヒラギノ角ゴ Pro W3","ＭＳ Ｐゴシック",sans-serif}
           `}</style>
        </Helmet>
        {/*  Head */}






        {/* Content */}
        <div id="content">

          {this.props.children}

        </div>
        {/* Content */}






        {/*Scroll Top*/}
        <StyleComponent></StyleComponent>
      </div>
    );
  }
}


export default ErrorPage;
