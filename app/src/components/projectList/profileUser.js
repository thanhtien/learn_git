import React, { Component } from 'react';

import CardProject from '../cardProject/indexProfile';

export default class ProjectListPostNow extends Component {
  render() {
    const {data} = this.props;
    return (
      <div className="area area-list">
        <div className="wrap-content-inner">
            <div className="row-flex gutter-18-sm">
              {
                data.map( (item , i) => {
                  return(
                    <CardProject key={i} item={item}/>
                  )
                })
              }
            </div>
        </div>
      </div>
    );
  }
}
