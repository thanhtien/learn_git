import React, { Component } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSearch } from '@fortawesome/free-solid-svg-icons';
import CardProject from '../cardProject';
import { Link  } from 'react-router-dom';
import Breadcrumbs from '../Breadcrumbs';
import CardProjectInList from "../cardProject/CardProjectInList";

import {Field} from "redux-form";
import axios from "axios";
import {FOOTER_CAT} from "../../actions/types";

export default class ProjectListAll extends Component {
    constructor(props) {
        // Required step: always call the parent class' constructor
        super(props);

        // Set the state directly. Use props if necessary.
        this.state = {
            searchText: ''
        }
    }
    //Search Handeler
    searchKeyWord=() =>{
        //this.props.searchForm(formProps)
        let sV = this.state.searchText ;
       window.location ="/project-from-type/search-project-"+sV+"/page=1";

    }
    changeSearchText(event) {
        var v = event.target.value;
        this.setState((prevState, props) => {
            return {
                searchText: v
            };
        });
    }
	getJsonFromUrl(url) {
        return url.split("/");
    }
    getActiveMenu(cateUrl,forCate) {
        if(cateUrl == forCate){
            return "activeMenuLeft";
        }
        return "";
    }
  render() {
	var urlParse = this.getJsonFromUrl(window.location.href);
      console.log(urlParse);
      let mycate=urlParse[4];
    const {title , data} = this.props;
      const dataCate = this.props.cat;
      // axios.get(ROOT_URL+'API_Top/API/category')
      //     .then(response =>{
      //         let dataCate =  response.data;
      //         dataCate ? dataCate.map( (item , i) => {
      //             return(
      //                 <li key={i}>
      //                     <a href={'/categories/'+item.slug+'/page=1'}>
      //                         {item.name}
      //                     </a>
      //                 </li>
      //             )
      //         }) : null
      //     })
    return (
      <div>
        {/*<div id="banner-calc" className="banner">*/}
        {/*  <div className="wraper">*/}
        {/*    <div className="search">*/}
        {/*      <FontAwesomeIcon icon={faSearch} />*/}
        {/*      <p className="search-p">プロジェクトを探す</p>*/}
        {/*    </div>*/}
        {/*    <h1 className="banner-title">{title}</h1>*/}
        {/*  </div>*/}
        {/*</div>*/}
        {/*<div className="clearFloat"></div>*/}
          <div className={"leftMenuProject"}>
              <div className="blk-group-nav">
                  <div className="divSearchLeft">
                      <form className="form-search">


                          <input  className="btnSearchLeft" type="button" onClick={()=>{this.searchKeyWord()}}  name="" value="" />
                          <input type={"input"}   onChange={this.changeSearchText.bind(this)} className={"searchLeft"} placeholder={"プロジェクトを探す"} />

                      </form>
                  </div>
                  <ul className="list-nav">
                      <li className="list-nav-item"><a className="list-nav-item-link" href={"/project-from-type/from-your-access/page=1"}> 最近見たプロジェクト </a></li>
                      <li className="list-nav-item"><a className="list-nav-item-link" href={"/project-from-type/from-support-80/page=1"}>あとひと押しのプロジェクト</a></li>
                      <li className="list-nav-item"><a className="list-nav-item-link" href={"/project-from-type/from-news-project/page=1"}>新着のプロジェクト</a></li>
                      <li className="list-nav-item"><a className="list-nav-item-link" href={"/project-from-type/from-favorite/page=1"}>注目のプロジェクト</a></li>
                      <li className="list-nav-item"><a className="list-nav-item-link" href={"/project-from-type/from-your-expired/page=1"}>終了間近のプロジェクト</a></li>
                      <li className="list-nav-item"><a className="list-nav-item-link" href={"/project-from-type/from-fanclub/page=1"}>コミュニティ型プロジェクト</a></li>
                      <li className="list-nav-item"><a className="list-nav-item-link" href={"/project-from-type/expiredPagination/page=1"}>成約したプロジェクト</a></li>
                      <li className="list-nav-item"><a className="list-nav-item-link" href={"/project-from-type/from-project-100/page=1"}>達成したプロジェクト</a></li>

                  </ul>
              </div>
              <div className="blk-group-nav">
                  <div className="divSearchLeft">
                      <form className="form-search">


                          <input  className="btnSearchLeft" type="button" name="" value="" />
                          <input type={"input"} className={"searchLeft"} placeholder={"カテゴリから探す"}

                          />

                      </form>
                  </div>
                  <ul className="list-nav">
                      {
                          dataCate ? dataCate.map( (item , i) => {
                                              return(
                                                  <li className="list-nav-item" key={i}>
                                                      <a className="list-nav-item-link" href={'/project-from-type/'+item.slug+'/page=1'}>
                                                          {item.name}
                                                      </a>
                                                  </li>
                                              )
                                          }) : null
                      }

                  </ul>
              </div>
          </div>

          <div className={"contenRightProject"}>
               <div style={data.length === 0 ? {margin:'0px',padding:'20px'}: null} className="area-list">
          <div className="">

              <div className="cover-Breadcrumbs"> <Breadcrumbs linkBreacrm={this.props.linkBreacrm}></Breadcrumbs></div>

              <div className="blk-group-nav">
                <form className="form-search">

                  <input  className="btnSearchLeft" type="button" onClick={()=>{this.searchKeyWord()}}  name="" value="" />
                  <input type={"input"}   onChange={this.changeSearchText.bind(this)} className={"searchLeft"} placeholder={"プロジェクトを探す"} />

                </form>
              </div>

              <div className="row-flex gutter-18-sm l-grid-card ">
                {
                  data.length !== 0  ?
                  data.map( (item , i) => {
                    return(
                      <CardProjectInList key={i} item={item}/>
                    )
                  }) :
                  <div className="mar-center">
                    <p
                      id="notFound"
                      className="blank slug-blank"
                    >
                      <span>{title}はまだありません。</span>
                    </p>
                    <Link className="btn-effect" to={"/"}>トップページへ</Link>
                  </div>


                }



              </div>
          </div>
                   <div className="cover-paginate">    {this.props.parThis.getPageList()}</div>
        </div>
          </div>
      </div>
    );
  }
}
