import React, { Component } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSearch } from '@fortawesome/free-solid-svg-icons';
import CardProject from '../cardProject';
import { Link  } from 'react-router-dom';
import Breadcrumbs from '../Breadcrumbs';
export default class ProjectList extends Component {

  render() {
    const {title , data} = this.props;

    return (
      <div>
        <div id="banner-calc" className="banner">
          <div className="wraper">
            <div className="search">
              <FontAwesomeIcon icon={faSearch} />
              <p className="search-p">プロジェクトを探す</p>
            </div>
            <h1 className="banner-title">{title}</h1>
          </div>
        </div>
        <div style={data.length === 0 ? {margin:'0px',padding:'20px'}: null} className="area area-list">
          <div className="wrap-content-inner">
              <div className="cover-Breadcrumbs"><Breadcrumbs linkBreacrm={this.props.linkBreacrm}></Breadcrumbs></div>
              <div className="row-flex gutter-18-sm l-grid-card">
                {
                  data.length !== 0  ?
                  data.map( (item , i) => {
                    return(
                      <CardProject key={i} item={item}/>
                    )
                  }) :
                  <div className="mar-center">
                    <p
                      id="notFound"
                      className="blank slug-blank"
                    >
                      <span>{title}はまだありません。</span>
                    </p>
                    <Link className="btn-effect" to={"/"}>トップページへ</Link>
                  </div>


                }



              </div>
          </div>
        </div>
      </div>
    );
  }
}
