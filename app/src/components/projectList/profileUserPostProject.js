import React, { Component } from 'react';

import CardProject from '../cardProject/indexProfilePostProject';

export default class ProjectListPostNow extends Component {
  render() {
    const {data , deleteProject , pageLoad} = this.props;

    return (
      <div className="area area-list">
        <div className="wrap-content-inner">
            <div className="row-flex gutter-18-sm l-grid-card">
              {
                data.map( (item , i) => {
                  return(
                    <CardProject pageLoad={pageLoad} deleteProject={deleteProject} key={i} item={item}/>
                  )
                })
              }
            </div>
        </div>
      </div>
    );
  }
}
