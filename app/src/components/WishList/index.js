/* @flow */

import React, { Component } from 'react';
import * as actions from '../../actions/WishList';
import { connect } from 'react-redux';

import { Link } from 'react-router-dom';
import ReactPaginate from 'react-paginate';
import LoadingScroll from '../common/loadingScroll';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {faBookmark, faChevronLeft, faChevronRight} from '@fortawesome/free-solid-svg-icons';
import FormatFunc from '../common/FormatFunc';

class Notification extends Component {
  componentDidMount() {
    document.title = "お気に入り｜KAKUSEIDA";
    const {page} = this.props.match.params;
    var PageString = page.replace('page=', '');
    this.props.wishList(Number(PageString)-1);
    // if (!this.props.wishListData) {
    //   console.log('1');
    //
    // }else {
    //
    //   if (this.props.wishListData.data.length === 0) {
    //     this.props.wishList(Number(PageString)-1);
    //   }
    // }

  }

  handlePageClick = (data) => {
    let selected = data.selected;
    this.props.wishList(selected);
  };


  leaveProject(e,project_id){
    e.stopPropagation();
    e.preventDefault();

    this.props.leaveToWishList(project_id,this.props.pageLoad);
  }

  render() {
    const {wishListData,pageLoad} = this.props;

    return (
      <div className="c-area my-page">
        <div className="wraper">
          <h1 className="title-noti">お気に入り</h1>
          <ul className="personal-panel-width personal-panel notification-list" style={{display:"block",overflow: "hidden" , height:'auto'}}>
            {
              wishListData && wishListData.data.length > 0 ?
              wishListData.data.map((item,i)=>{
                return (
                  <li key={i} style={item.status === '0' ? {background: 'floralwhite'} : null}>
                    <Link  to={`/project-detail/${item.project_id}`}  className="notification">
                      <div className="notification-image">
                        <img src={item.thumbnail} alt="thum" />
                      </div>
                      <div className="notification-text">
                         <span className="limit-width">
                           {item.project_name}

                         </span>
                        <p
                          className="calendar-time calendar-fix"
                          style={{display:'block'}}>
                          <span
                            onClick={(e)=>this.leaveProject(e,item.project_id)}
                            className="text-leave">
                            <span>  <FontAwesomeIcon icon={faBookmark} /> お気に入り解除</span>

                          </span>
                          <span className="time-text">{ <FormatFunc date={item.created}></FormatFunc> }</span>

                        </p>
                      </div>

                    </Link>
                  </li>
                )

              }):
              <li className="noti-list">
                <p>お気に入りリストにお仕事が登録されていません。</p>
              </li>


            }
          </ul>
          <div className="cover-paginate noti">
            {
              this.props.loading?<div className="loading-io"><LoadingScroll></LoadingScroll></div>:null
            }
            {
              wishListData && wishListData.data.length > 0 ?
              <ReactPaginate
                previousLabel={<FontAwesomeIcon icon={faChevronLeft}/>}
                nextLabel={<FontAwesomeIcon icon={faChevronRight}/>}
                breakLabel={<span>...</span>}
                breakClassName={"break-me"}
                pageCount={wishListData.page_count}
                marginPagesDisplayed={2}
                forcePage={Number(pageLoad)}
                pageRangeDisplayed={2}
                onPageChange={this.handlePageClick}
                containerClassName={"pagination"}
                subContainerClassName={"pages pagination"}
                activeClassName={"active"} /> : null
            }
          </div>
          <div className="clear-fix"></div>

        </div>
      </div>
    );
  }
}
const mapStateToProps = (state) => {

  return {
    wishListData: state.wishlist.wishlistPage.data,
    pageLoad:state.wishlist.wishlistPage.pageLoad,
    loading:state.common.loading,
  }
}

export default connect(mapStateToProps, actions)(Notification);
