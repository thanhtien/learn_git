/* @flow */

import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {faCaretRight} from '@fortawesome/free-solid-svg-icons';


export default class Breadcrumbs extends Component {
  render() {
    const {linkBreacrm} = this.props;
    return (
      <div className="bread-crums">
        <ul>
          <li>
            <Link className="breadcrumb-link" to={'/'}>クラウドファンディングTOP</Link>
            <FontAwesomeIcon className="icon-splash" icon={faCaretRight} />
          </li>
          {
            linkBreacrm ? linkBreacrm.map( (item,i) => {
              return (
                item.last === false ?
                <li key={i}>
                  <Link className="breadcrumb-link" to={item.link}>{item.label} /</Link>
                  <FontAwesomeIcon className="icon-splash" icon={faCaretRight} />
                </li> :
                <li key={i}>
                  <span>{item.label}</span>
                </li>
              )
            }) : null

          }
        </ul>
      </div>
    );
  }
}
