/* @flow */

import React, { Component } from 'react';
import { Field, reduxForm,reset } from 'redux-form';
import { connect } from 'react-redux';

import * as actions from '../../actions';
import { Link } from 'react-router-dom';
import Rodal from 'rodal';
import AlertAuth from './AlertAuth';
class CreateUser extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showAlertAuth:false,

    };
  }
  componentDidMount() {
    if (this.props.authenticated) {
      this.props.history.push('/');
    }
    document.title = "アカウント情報登録｜KAKUSEIDA";
  }
  renderField = ({ input, label, type, meta: { touched, error } }) => (
    <div className="form-text">
      <input className="form-control" {...input} placeholder={label} type={type} />
      {touched && error && <span style={{display: 'block',color:'red'}} className="text-danger">{error}</span>}
    </div>
  );
  renderFieldCheckBox = ({ input, type, meta: { touched, error } }) => (
    <span>
      <input {...input} type={type} />
      {touched && error && <span style={{display: 'block',color:'red'}} className="text-danger">{error}</span>}
    </span>
  );
  handleFormSubmit(formProps) {
    const {dataUserLogin,justshowSocial,justhideSocial} = this.props;

    const data = {
      "user_id":dataUserLogin.id,
      "email":formProps.email,
      "username":formProps.user_name.toLowerCase(),
      "password":formProps.password
    }
    this.props.CreateSocial(data,()=>this.showAlertAuth(),justshowSocial,justhideSocial);
  }
  hidePopup(){
    this.props.hidePopup();
  }
  showAlertAuth(){
    this.setState({
      showAlertAuth:true
    })
  }
  render() {
    const { handleSubmit , modalSocial } = this.props;
    const { showAlertAuth } = this.state;
    var height;
    if (showAlertAuth) {
      height = 50;
    }else {
      height = 80;
    }
    return (
      <Rodal measure={'%'} width={80} height={height} visible={modalSocial} onClose={this.hidePopup.bind(this)}>
        {
          showAlertAuth ?
          <AlertAuth
            title={'メールをご確認ください'}
            messOne={'メールに記載されたURLをクリックの上、新規会員登録を続けてください。'}
            messTwo={'※KAKUSEIDAからの確認メールが届かない場合は、迷惑メールボックス（SPAMBOX)をご確認していただき、'}
            messThree={'KAKUSEIDAからのメール受信を許可するよう設定してください。'}
          />
          : <div className="area reg">
              <div className="wraper">
                  <div className="register-panel">
                      <h2 className="title">アカウント情報登録</h2>
                      <div className="form">
                        <form onSubmit={handleSubmit(this.handleFormSubmit.bind(this))}>
                          <div className="form-field">
                              <p className="form-label">
                                メールアドレスで新規登録<span>必須</span>
                              </p>
                              <Field
                                name="email"
                                label="Email"
                                component={this.renderField}
                                type="text" />
                          </div>
                          <div className="form-field">
                              <p className="form-label">
                                ユーザー名<span>必須</span>
                              </p>
                              <Field
                                name="user_name"
                                label="ユーザー名（英数字3-12文字）"
                                component={this.renderField}
                                type="text" />
                          </div>
                          <div className="form-field">
                              <p className="form-label">パスワード<span>必須</span></p>
                              <Field
                                name="password"
                                label="パスワード（英数字6文字以上）"
                                component={this.renderField}
                                type="password" />
                          </div>
                          <div className="form-field">
                              <p className="form-label">パスワード（確認）<span>必須</span></p>
                              <Field
                                name="re_password"
                                label="上と同じパスワードをご入力ください"
                                component={this.renderField}
                                type="password" />
                          </div>
                          <div className="form-field">
                              <p className="form-chk"><label>
                                  <Field
                                    name="check"
                                    label="上と同じパスワードをご入力ください"
                                    component={this.renderFieldCheckBox}
                                    type="checkbox" />
                                <Link rel="noopener noreferrer" target="_blank" to={'/kakuseida/term/'} className="blue-text">利用規約</Link>に同意する</label>
                              </p>
                          </div>
                          <div className="form-field">
                              <p className="form-submit"><input type="submit" value="登録"/></p>
                          </div>
                        </form>
                      </div>
                  </div>
              </div>
          </div>
        }


      </Rodal>

    );
  }
}

const validate = values => {
  const errors = {};
  var patt = new RegExp("^[a-z0-9_-]{3,100}$");

  if (!values.user_name) {
    errors.user_name = 'ユーザー名を入力してください';
  }else if(values.user_name){
    if (values.user_name.length <=2 || values.user_name.length >= 32) {
      errors.user_name = 'ユーザー名（英数字3-32文字）';
    }else if(patt.test(values.user_name.toLowerCase()) !== true){
      errors.user_name = '注：ログイン名の中にスペース、特殊文字が入らないでください。ログイン名の行頭に数字を入力しないください。';
    }
  }


  if (!values.re_password) {
    errors.re_password = 'パスワード（確認）を入力してください';
  }
  if (!values.password) {
    errors.password = 'パスワードを記入してください';
  }

  if (values.password !== values.re_password) {
    errors.password = 'パスワードが一致していません';
  }

  if (values.check !==true) {
    errors.check = '利用規約に同意する';
  }

  if (!values.email) {
      errors.email = 'メールアドレスを記入してください';
  }
  else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
      errors.email = 'メールアドレスを正しくしてください';
  }
  return errors;
};

const mapStateToProps = (state) => {
    return {
      loading: state.common.loading
    }
};
const afterSubmit = (result, dispatch) => {
  return (
    dispatch(reset('forgot'))
  )
};
export default reduxForm({
    form: 'reset-password',
    validate,
    onSubmitSuccess:afterSubmit
})(connect(mapStateToProps, actions)(CreateUser));
