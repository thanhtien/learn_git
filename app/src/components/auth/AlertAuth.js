/* @flow */

import React, { Component } from 'react';


export default class AlertAuth extends Component {
  render() {
    return (
      <div className="login-panel thank-area">
        <h2 className="title">{this.props.title}</h2>
        <p className="text">
          {this.props.messOne}<br/>{this.props.messTwo}<br/>{this.props.messThree}
        </p>
      </div>
    );
  }
}
