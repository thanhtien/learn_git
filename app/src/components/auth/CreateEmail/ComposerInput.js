
import React, { Component } from 'react';
// import {ParseUrl} from '../../../common/parseImage'

export default class ComposerInput extends Component {


  constructor(props) {
    super(props);
    this.state = {
      value: ''
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    this.setState({value: event.target.value});
  }

  handleSubmit(event) {
    if (this.state.value !== '') {
      const {ownerProject} = this.props;
      var data = {
        "project_id":this.props.project_id.toString(),
        "user_id":ownerProject.id.toString(),
        "message":this.state.value
      }
      this.props.SendEmailToOwnner(data,this.props.ToogleLoading,ownerProject.username);
    }
    event.preventDefault();
  }

  render() {
    const {ownerProject} = this.props;

    return (
      <form onSubmit={this.handleSubmit}>
        <div className="cover-box-composer">
          <div className="avatar-to">
            <p className="cover-avatar">
              <img alt={ownerProject.username} src={ownerProject.profileImageURL} />
            </p>
          </div>
          <div className="text-send">
            <div className="body-form">
              <h4 className="title-box">
                <small>To.</small>
                <span className="to-user">{ownerProject.username}</span>
              </h4>
              <div className="messages-reply" id="for-reply">
                <div className="input-box inner">
                  <h5 className="title-box-inner">
                    <label htmlFor="message_body">
                      <label htmlFor="message_body">メッセージを書く</label>
                    </label>
                  </h5>
                  <div className="form-group text required message_body">
                    <textarea
                      value={this.state.value}
                      onChange={this.handleChange}
                      className="form-control text required"
                      required="required"
                      name="message"
                      id="message_body">
                    </textarea>
                  </div>
                </div>
                <div className="submit-box">
                  <input type="submit" name="confirm" value="送信する" data-disable-with="送信する"/>
                </div>
              </div>
            </div>

          </div>
        </div>
      </form>

    );
  }
}
