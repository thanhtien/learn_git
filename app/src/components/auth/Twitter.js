import React, { Component } from 'react';
import TwitterLogin from 'react-twitter-auth';
var CONFIG = require('../../config/common');

if (process.env.NODE_ENV === "development") {
  CONFIG = CONFIG.CONFIG.DEV;
}else {
  CONFIG = CONFIG.CONFIG.PRODUCT;
}
console.log(CONFIG.ROOT_URL);
class Twitter extends Component {

  constructor() {
    super();
    this.onFailed = this.onFailed.bind(this);
    this.onSuccess = this.onSuccess.bind(this);
  }


  onSuccess(response) {
    var parseURl = response.url;
    var oauth_token = this.getJsonFromUrlOauth_token(parseURl);
    var oauth_verifier = this.getJsonFromUrlOauth_verifier(parseURl);
    const {setDataUserSocial} = this.props;
    var data = {
      "oauth_token":oauth_token,
     "oauth_verifier":oauth_verifier,
    }
    this.props.twitterToken(data,setDataUserSocial);
  }

  onFailed(error) {
    // alert(error);
    console.log(error);
  }

  getJsonFromUrlOauth_token(url) {

    var query = url.substr(1);
    var result = {};
    query.split("&").forEach(function(part) {
      var item = part.split("=");
      result[item[0]] = decodeURIComponent(item[1]);
    });
    return result.oauth_token;
  }
  getJsonFromUrlOauth_verifier(url) {

    var query = url.substr(1);
    var result = {};
    query.split("?").forEach(function(part) {
      var item = part.split("=");
      result[item[0]] = decodeURIComponent(item[1]);
    });
    return result.oauth_verifier.replace("&oauth_token", "");
  }

  render() {
    return (
      <div className="social-button">
        <TwitterLogin
          loginUrl={`${CONFIG.ROOT_URL}Auth/loginNewTwitter`}
          onFailure={this.onFailed}
          onSuccess={this.onSuccess}
          text={this.props.text}
          requestTokenUrl={`${CONFIG.ROOT_URL}Auth/loginTwitter`}
          showIcon={true}
          forceLogin={true}
          className={'auth-twitter'}

        />
      </div>
    );
  }
}

export default Twitter;
