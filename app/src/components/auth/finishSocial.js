/* @flow */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import Loading from '../common/loading';
import * as actions from '../../actions';


class Finish extends Component {
  componentDidMount() {
    const {token} = this.props.match.params;

    this.props.finishSocial(token)
  }
  render() {
    return (
      <Loading></Loading>
    );
  }
}



export default connect(null, actions)(Finish)
