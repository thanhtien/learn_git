/* @flow */

import React, { Component } from 'react';
import { Field, reduxForm ,reset} from 'redux-form';
import * as actions from '../../actions';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import Twitter from './Twitter';
import FacebookLoginButton from './FacebookLoginButton';
import CreateUserSocial from './createUserSocial';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faFacebookF } from '@fortawesome/fontawesome-free-brands';
class SigninLoginForm extends Component {

  constructor(props) {
    super(props);
    this.state = {
      dataUserLogin:null,
      modalSocial:false
    };
  }

  //Render Each Input
  renderField = ({ input, label,placeholder,type, meta: { touched, error } }) => (
    <div className="form-field">
      <p className="form-label">{label}</p>
      <input className="form-control" {...input} placeholder={placeholder} type={type} />
      {touched && error && <span className="text-danger error">{error}</span>}
    </div>
  );
    renderFieldPass = ({ input, label,placeholder,type, meta: { touched, error } }) => (
        <div className="form-field">
            <div className={"form-label"}>
                <span className="form-label lableLeft">{label}</span>
                <span className="form-label-right lableRight"><Link className="forgot-pass" to={'forgot-password'}>パスワードを思出せない場合</Link></span>
            </div>
            <div className={"form-control-wrap"}>
                <input className="form-control" {...input} placeholder={placeholder} type={type} />
                {touched && error && <span className="text-danger error">{error}</span>}
            </div>


        </div>
    );
  //render Error
  renderError() {
    if (this.props.errorMessage) {
        return (
          <p style={{color:'red', textAlign: 'center'}}>
            {this.props.errorMessage}
          </p>
        );
    }
  }

  handleFormSubmit({ email, password }) {
    this.props.signinUser({ email, password })
  }

  setDataUserSocial(data) {

    this.setState({
      dataUserLogin:data,
      modalSocial:true
    })
    document.body.className = 'block-body';
  }
  hidePopupSocial(){
    this.setState({
      dataUserLogin:null,
      modalSocial:false
    })
    document.body.className = '';
  }
  justhideSocial(){
    this.setState({
      modalSocial:false
    })
    document.body.className = '';
  }
  justshowSocial(){
    this.setState({
      modalSocial:true
    })
    document.body.className = 'block-body';
  }

  onFacebookLogin = (loginStatus, resultObject) => {

    if (loginStatus === true) {
      this.props.facebookAccessToken(resultObject.authResponse.accessToken , (data)=>this.setDataUserSocial(data));
    } else {
      alert('Facebookのログインエラー');
    }
  }

  render() {

    const { handleSubmit , twitterToken } = this.props;
    const {modalSocial , dataUserLogin} = this.state;
    return (
      <div className="form-login l-tb-flex">
          <div className="l-tb-flex-item">
          <CreateUserSocial
            dataUserLogin={dataUserLogin}
            hidePopup={()=>this.hidePopupSocial()}
            justhideSocial={()=>this.justhideSocial()}
            justshowSocial={()=>this.justshowSocial()}
            modalSocial={modalSocial}>
          </CreateUserSocial>

          <form onSubmit={handleSubmit(this.handleFormSubmit.bind(this))}>
            <Field
              className={"form-control1 loginEmai"}
              name="email"
              placeholder={"User name"}
              label="ユーザー名 or メールアドレス"
              component={this.renderField}
              type="text"
            />
            <Field
              label="パスワード"
              className="form-control1 loginPass "
              name="password"
              placeholder={"Password"}
              component={this.renderFieldPass}
              type="password" />

              <div className="">
                {this.renderError()}

                  <div className="divCheckbox ">
                      <input type={"checkbox"} className="chkbox myinput large" />
                      <span  className="chkboxLabel">ログイン状態を保存する</span>


                  </div>
                <p className="form-submit">
                  <input type="submit" value="ログイン"/>
                </p>
              </div>
          </form>
          <div className="separator login-here-separator"></div>

          {/*<p className="title-social">*/}
          {/*  外部サービスでログインする*/}
          {/*</p>*/}
          <div className={"l-grid-flex line-btn-social"}>
          <FacebookLoginButton onLogin={this.onFacebookLogin}>
            <span className="facebook-button">
              {/*<FontAwesomeIcon
                  icon={faFacebookF} style={{fontSize: 17,marginRight: 20}} />*/}
               <div className={"iconFacebook"}></div>
               Facebookを利用してログイン
            </span>
          </FacebookLoginButton>
          <Twitter
              text={'Twitterを利用してログイン'}
              setDataUserSocial={(data)=>this.setDataUserSocial(data)}
              twitterToken={twitterToken}
          />
          </div>
          </div>

      </div>
    );
  }
}

const validate = values => {
    const errors = {};
    if (!values.email) {
        errors.email = 'メールアドレスを記入してください';
    }


    if (!values.password) {
        errors.password = 'パスワードを記入してください';
    }
    return errors;
};


const mapStateToProps = (state) => {
  return {
    errorMessage: state.auth.error,
  }
};
const afterSubmit = (result, dispatch) => {
  return (
    dispatch(reset('signin'))
  )
};

export default reduxForm({
    form: 'signin',
    onSubmitSuccess:afterSubmit,
    validate,

})(connect(mapStateToProps, actions)(SigninLoginForm));
