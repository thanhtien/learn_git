/* @flow */

import React, { Component } from 'react';
import { Field, reduxForm,reset } from 'redux-form';
import * as actions from '../../actions';
import { connect } from 'react-redux';
import Loading from '../common/loading';

class resetPassword extends Component {


  componentDidMount() {
    this.props.checkTokenReset({"tokenForgot":this.props.match.params.code});
  }

  handleFormSubmit(formProps) {
    const data = {
      code:this.props.match.params.code,
      new_password:formProps.password
    }
    this.props.resetPassword(data);
  }

  renderField = ({ input, label, type, meta: { touched, error } }) => (
    <div className="form-text">
      <input className="form-control" {...input} placeholder={label} type={type} />
      {touched && error && <span className="text-danger error">{error}</span>}
    </div>
  );
  // sendReset
  render() {
    const { handleSubmit  , sendResetError , loading } = this.props;
    return (
      <div className="area reg area-form">
        <div className="wraper-small">
          {
            loading ? <Loading/> : null
          }

          <div className="register-panel">
                <h1 className="title">アカウント情報登録</h1>
                <p style={{textAlign: 'center',color:'red'}}>
                  {
                    sendResetError ?
                    sendResetError.status :
                    null
                  }
                </p>
                <form onSubmit={handleSubmit(this.handleFormSubmit.bind(this))}>
                  <div className="form-reset">
                    <div className="form-field l-grid-flex">
                        <label className="form-label">パスワード<span className="require">必須</span></label>
                        <Field
                          name="password"
                          label="パスワード（英数字6文字以上）"
                          component={this.renderField}
                          type="password" />
                    </div>
                    <div className="form-field l-grid-flex">
                        <label className="form-label">パスワード（確認）<span className="require">必須</span></label>
                        <Field
                          name="passwordConfirmation"
                          label="上と同じパスワードをご入力ください"
                          component={this.renderField}
                          type="password" />
                    </div>

                    <div className="form-field">
                        <p className="form-submit">
                          <input type="submit" value="登録"/>
                        </p>
                    </div>
                  </div>
                </form>
            </div>
        </div>

      </div>

    );
  }
}

const validate = values => {
  const errors = {};
  if (!values.password) {
    errors.password = 'パスワードを記入してください';
  }

  if (!values.passwordConfirmation) {
    errors.passwordConfirmation = 'パスワード（確認）を入力してください';
  }

  if (values.password !== values.passwordConfirmation) {
    errors.password = 'パスワードが一致していません';
  }
  return errors;
};

const mapStateToProps = (state) => {
    return {
      sendReset: state.auth.sendReset,
      sendResetError: state.auth.sendResetError,
      loading: state.common.loading
    }
};
const afterSubmit = (result, dispatch) => {
  return (
    dispatch(reset('forgot'))
  )
};
export default reduxForm({
    form: 'reset-password',
    validate,
    onSubmitSuccess:afterSubmit
})(connect(mapStateToProps, actions)(resetPassword));
