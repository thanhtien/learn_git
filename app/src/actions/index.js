import axios from 'axios';
import History from '../history.js';
import {
    AUTH_USER,
    UNAUTH_USER,
    SEND_EMAIL_FORGOT,
    LOADING_POST_START,
    LOADING_POST_STOP,
    SEND_EMAIL_FORGOT_FAILE,
    SEND_RESET,
    SEND_RESET_FAILE,
    SEND_SUB_EMAIL,
    SEND_SUB_EMAIL_FAILE,
    TOKEN_OK,
    TOKEN_FAILE,
    AUTH_ERROR
} from './types';

var CONFIG = require('../config/common');

if (process.env.NODE_ENV === "development") {
  CONFIG = CONFIG.CONFIG.DEV;
}else {
  CONFIG = CONFIG.CONFIG.PRODUCT;
}
const ROOT_URL = CONFIG.ROOT_URL;

export const finishSocial = (token) => {
    return (dispatch) => {

      axios.get(`${ROOT_URL}/Auth/finishSocial?token=${token}`)
        .then(response => {
          localStorage.setItem('token', response.data.token);
          window.location.assign("/")
        }).catch((error) => {
          if (error) {
            window.alert(error.response.data.status);
          }
        });
    };
};

export const twitterToken = (data,setDataUserSocial) => {
    return (dispatch) => {
      // submit email/password to the server
      dispatch({
        type: LOADING_POST_START,
        payload: true
      });


      axios.post(`${ROOT_URL}/Auth/loginNewTwitter`, data)
        .then(response => {
          dispatch({
            type: LOADING_POST_STOP,
            payload: false
          });

          if (response.data.loginSocial === '0') {

            setDataUserSocial(response.data)
          }else {
            localStorage.setItem('token', response.data.token);
            localStorage.setItem('socket_uuid', response.data.socket_uuid);
            window.location.assign("/")
          }



        }).catch((error) => {

          if (error) {
            window.alert(error.response.data.status);
          }

          dispatch({
            type: LOADING_POST_STOP,
            payload: false
          });
        });
    };
};

export const facebookAccessToken = (accessToken,setDataUserSocial) => {
    return (dispatch) => {
      // submit email/password to the server
      dispatch({
        type: LOADING_POST_START,
        payload: true
      });

      var data = {
        accessToken:accessToken
      }


      axios.post(`${ROOT_URL}/Auth/loginFacebook`, data)
        .then(response => {
          dispatch({
            type: LOADING_POST_STOP,
            payload: false
          });

          if (response.data.loginSocial === '0') {
            setDataUserSocial(response.data)
          }else {
            localStorage.setItem('token', response.data.token);
            localStorage.setItem('socket_uuid', response.data.socket_uuid);
            window.location.assign("/")
          }


        }).catch((error) => {
          if (error) {
            window.alert(error.response.data.status);
          }

          dispatch({
            type: LOADING_POST_STOP,
            payload: false
          });
        });
    };
};




export const signinUser = ({ email, password }) => {
    return (dispatch) => {
      // submit email/password to the server
      dispatch({
        type: LOADING_POST_START,
        payload: true
      });

    
      var data = window.btoa(unescape(encodeURIComponent(JSON.stringify({ email, password }))))

      axios.post(`${ROOT_URL}Auth/login`, {data})
        .then(response => {
          localStorage.setItem('token', response.data.token);


          localStorage.setItem('socket_uuid', response.data.socket_uuid);

          window.location.reload();

        }).catch((error) => {
          if (error) {

            dispatch({ type: AUTH_ERROR ,payload: error.response.data.status});
          }

          if (!error) {
            dispatch({ type: AUTH_ERROR ,payload: error.response.data.status});
          }
          dispatch({
            type: LOADING_POST_STOP,
            payload: false
          });
        });
    };
};



export const forgot = ({ email }) => {
    return (dispatch) => {
      dispatch({
        type: LOADING_POST_START,
        payload: true
      });
      // submit email/password to the server
      axios.post(`${ROOT_URL}Auth/forgot_pass`, { email })
        .then(response => {
          dispatch({
            type: LOADING_POST_STOP,
            payload: false
          });
          dispatch({
            type: SEND_EMAIL_FORGOT,
            payload: response.data
          });
        }).catch((e) => {
          dispatch({
            type: SEND_EMAIL_FORGOT_FAILE,
            payload: {status:"メールアドレスを正しく記入してください。"}
          });
          dispatch({
            type: LOADING_POST_STOP,
            payload: false
          });
        });
    };
};



export const resetPassword = (data) => {
    return (dispatch) => {
      dispatch({
        type: LOADING_POST_START,
        payload: true
      });
      // submit email/password to the server
      axios.post(`${ROOT_URL}Auth/reset_pass/`, data)
        .then(response => {
          dispatch({
            type: LOADING_POST_STOP,
            payload: false
          });
          dispatch({
            type: SEND_RESET,
            payload: response.data
          });
          History.push('/signin');
        }).catch((e) => {
          dispatch({
            type: SEND_RESET_FAILE,
            payload: {status:"パスワードが変更されました"}
          });
          dispatch({
            type: LOADING_POST_STOP,
            payload: false
          });
        });
    };
};


//EmailSubscribe

export const emailSubscribe = (data) => {
    return (dispatch) => {
      dispatch({
        type: LOADING_POST_START,
        payload: true
      });
      // submit email/password to the server
      axios.post(`${ROOT_URL}Auth/registerEmail/`, data)
        .then(response => {
          dispatch({
            type: LOADING_POST_STOP,
            payload: false
          });
          dispatch({
            type: SEND_SUB_EMAIL,
            payload: response.data
          });

        }).catch((error) => {
          if (error.response) {
            dispatch({
              type: LOADING_POST_STOP,
              payload: false
            });
            dispatch({
              type: SEND_SUB_EMAIL_FAILE,
              payload: error.response.data.status
            });
          } else if (error.request) {
            dispatch({
              type: SEND_SUB_EMAIL_FAILE,
              payload: error.request
            });
          } else {
            dispatch({
              type: SEND_SUB_EMAIL_FAILE,
              payload: error.message
            });
          }
        });
    };
};

export const checkToken = (data) => {
    return (dispatch) => {
      dispatch({
        type: LOADING_POST_START,
        payload: true
      });
      // submit email/password to the server
      axios.post(`${ROOT_URL}Auth/checkToken/`, data)

        .then(response => {
          dispatch({
            type: LOADING_POST_STOP,
            payload: false
          });
          dispatch({
            type: TOKEN_OK,
            payload: response.data
          });

        }).catch((error) => {
          if (error.response) {
            dispatch({
              type: LOADING_POST_STOP,
              payload: false
            });
            dispatch({
              type: TOKEN_FAILE,
              payload: {status:"Token False"}
            });
            var r = window.confirm("Token expired");
            if (r === true) {
              History.push('/signin');
            }
          } else if (error.request) {
            dispatch({
              type: TOKEN_FAILE,
              payload: error.request
            });
          } else {
            dispatch({
              type: TOKEN_FAILE,
              payload: error.message
            });
          }
        });
    };
};


export const checkTokenReset = (data) => {
    return (dispatch) => {
      dispatch({
        type: LOADING_POST_START,
        payload: true
      });

      // submit email/password to the server
      axios.post(`${ROOT_URL}Auth/checkTokenForgot/`, data)

        .then(response => {
          dispatch({
            type: LOADING_POST_STOP,
            payload: false
          });


        }).catch((error) => {
          if (error) {
            History.push('/token-reset-not-found');
          }
        });
    };
};



export const signupUser = (data) => {
    return (dispatch) => {
      dispatch({
        type: LOADING_POST_START,
        payload: true
      });


      // submit email/password to the server
      axios.post(`${ROOT_URL}Auth/createUser/`, data)

        .then(response => {
          dispatch({
            type: LOADING_POST_STOP,
            payload: false
          });
          dispatch({
            type: AUTH_USER
          });
          localStorage.setItem('token', response.data.token);



          window.location.href = "/";
        }).catch((error) => {
          if (error.response) {
            dispatch({
              type: LOADING_POST_STOP,
              payload: false
            });
            dispatch({
              type: TOKEN_FAILE,
              payload: {status:"Token False"}
            });
            alert(error.response.data.status);
          } else if (error.request) {
            dispatch({
              type: TOKEN_FAILE,
              payload: error.request
            });
          } else {
            dispatch({
              type: TOKEN_FAILE,
              payload: error.message
            });
          }
        });
    };
};


export const CreateSocial = (data,showAlertAuth,show,hide) => {
    return (dispatch) => {
      dispatch({
        type: LOADING_POST_START,
        payload: true
      });
      hide();
      // submit email/password to the server
      axios.post(`${ROOT_URL}Auth/CreateSocial/`, data)

        .then(response => {
          dispatch({
            type: LOADING_POST_STOP,
            payload: false
          });
          console.log(response);
          show();
          showAlertAuth();

        }).catch((error) => {
          dispatch({
            type: LOADING_POST_STOP,
            payload: false
          });
          dispatch({
            type: TOKEN_FAILE,
            payload: error.request
          });
          alert(error.response.data.status);
          show();
        });
    };
};



export const signoutUser = () => {
    localStorage.removeItem('token')


    setTimeout(function () {
      window.location.href = "/";
    }, 1000);
    return { type: UNAUTH_USER };
};
