import axios from 'axios';
import History from '../history.js';
import {
    LOAD_BACKER,
    LOADING_POST_STOP,
    LOADING_POST_START,
    UNAUTH_USER,
    DONATE_OK,
    DISABLE_DONATE
} from './types';

var CONFIG = require('../config/common');

if (process.env.NODE_ENV === "development") {
  CONFIG = CONFIG.CONFIG.DEV;
}else {
  CONFIG = CONFIG.CONFIG.PRODUCT;
}
const ROOT_URL = CONFIG.ROOT_URL;
const token = 'Bearer '+ localStorage.getItem('token');
const config = {
  headers: {
    "Authorization" : token
  }
};

/**
 * [getBacker Get Backer Project For Donation]
 * @param  {[type]} projectID [Project ID]
 * @param  {[type]} backerID  [Backer ID]
 * @return {[type]}           [Data Backer]
 */
export const getBacker = (projectID,backerID) => {

  return (dispatch) => {

    dispatch({
      type: LOADING_POST_START,
      payload: true
    });
    axios.get(ROOT_URL+'/api/Users/checkProjectReturn?project_id='+projectID+'&backing_level_id='+backerID , config)
    .then(response =>{
      if (response) {
        dispatch({
          type: LOADING_POST_STOP,
          payload: false
        });
        dispatch({
            type: LOAD_BACKER,
            payload: response.data
        });
      }
    })
    .catch(function (error) {
      dispatch({
        type: LOADING_POST_STOP,
        payload: false
      });
      if (error.response.data.status === 'Expired token') {
        localStorage.removeItem('token')


        setTimeout(function () {
          History.push('/signin');
        }, 1000);
        dispatch({ type: UNAUTH_USER });
      } else if(error.response.status === 400) {
        History.push('/unauthorized');
      }
      else if(error.response.status === 404) {
        History.push('/notFoundPage');
      }
      else if(error.response.status === 401) {
        History.push('/unauthorized');
      } else {
        console.log(error.response.data.status);
      }
    });
  };
};



export const getBackerNonLogin = (projectID,backerID) => {

  return (dispatch) => {

    dispatch({
      type: LOADING_POST_START,
      payload: true
    });
    var url = ROOT_URL+'v2/guest/Project2/checkProjectReturn?project_id='+projectID+'&backing_level_id='+backerID;

    axios.get(url)
    .then(response =>{
      if (response) {
        dispatch({
          type: LOADING_POST_STOP,
          payload: false
        });
        dispatch({
            type: LOAD_BACKER,
            payload: response.data
        });
      }
    })
    .catch(function (error) {
      dispatch({
        type: LOADING_POST_STOP,
        payload: false
      });
      if (error.response.data.status === 'Expired token') {
        localStorage.removeItem('token')


        setTimeout(function () {
          History.push('/signin');
        }, 1000);
        dispatch({ type: UNAUTH_USER });
      } else if(error.response.status === 400) {
        console.log(error.response.data.status);

        History.push('/unauthorized');
      }
      else if(error.response.status === 404) {


        History.push('/notFoundPage');
      }
      else if(error.response.status === 401) {
        console.log(error.response.data.status);

        History.push('/unauthorized');
      } else {
        console.log(error.response.data.status);
      }
    });
  };
};

/**
 * [Donate For Donate]
 * @param {[type]} amount           [Money Donate]
 * @param {[type]} project_id
 * @param {[type]} backing_level_id
 */

export const Donate = (typeProject,cvc,amount,project_id,backing_level_id) => {
  return (dispatch) => {

    dispatch({
      type: LOADING_POST_START,
      payload: true
    });

    var data;
    var url;
    if (typeProject === '1') {
      data = {
        "project_id":project_id,
        "backing_level_id":backing_level_id,
        'cvc':cvc,
        "comment":`”Username"は"Tên Project(${project_id})"の"${backing_level_id}"に支援しました。`
      }

      url = `${ROOT_URL}v2/user/FanClub/DonateFanClub`;


    }else {
      data = {
        "invest_amount":amount,
        "project_id":project_id,
        "backing_level_id":backing_level_id,
        'cvc':cvc,
        "comment":`”Username"は"Tên Project(${project_id})"の"${backing_level_id}"に支援しました。`
      }
      url = `${ROOT_URL}api/Users/donateStripe`;


    }

    console.log(data);
    axios.post(url , data , config)
    .then(response =>{
      if (response) {
        dispatch({
          type: LOADING_POST_STOP,
          payload: false
        });
        dispatch({
            type: DONATE_OK,
            payload: response.data
        });
      }
    })
    .catch(function (error) {
      dispatch({
        type: LOADING_POST_STOP,
        payload: false
      });

      if (error.response.data.status === 'Expired token') {
        localStorage.removeItem('token')
        setTimeout(function () {
          History.push('/signin');
        }, 1000);
        dispatch({ type: UNAUTH_USER });
      } else if(error.response.status === 401) {
        History.push('/unauthorized');
      }
      else if(error.response.status === 404) {
        History.push('/notFoundPage');
      }
      else {
        alert(error.response.data.status);
      }
    });
  };
};



export const DonateNonLogin = (data) => {
  return (dispatch) => {

    dispatch({
      type: LOADING_POST_START,
      payload: true
    });
    var url = `${ROOT_URL}v2/guest/Donate/donateStripe`;
    axios.post( url , data )
    .then(response =>{
      if (response) {
        dispatch({
          type: LOADING_POST_STOP,
          payload: false
        });
        dispatch({
            type: DONATE_OK,
            payload: response.data
        });
      }
    })
    .catch(function (error) {
      dispatch({
        type: LOADING_POST_STOP,
        payload: false
      });

      if (error.response.data.status === 'Expired token') {
        localStorage.removeItem('token')
        setTimeout(function () {
          History.push('/signin');
        }, 1000);
        dispatch({ type: UNAUTH_USER });
      } else if(error.response.status === 401) {
        History.push('/unauthorized');
      }
      else if(error.response.status === 404) {
        History.push('/notFoundPage');
      }
      else {
        alert(error.response.data.status);
      }
    });
  };
};


/**
 * [Donate For Donate]
 * @param {[type]} amount           [Money Donate]
 * @param {[type]} project_id
 * @param {[type]} backing_level_id
 */

export const DonateBank = (amount,project_id,backing_level_id) => {
  return (dispatch) => {

    dispatch({
      type: LOADING_POST_START,
      payload: true
    });

    var data;
    data = {
      "invest_amount":amount,
      "project_id":project_id,
      "backing_level_id":backing_level_id
    }
    axios.post(ROOT_URL+'/api/Users/donateBank' , data , config)
    .then(response =>{
      if (response) {
        dispatch({
          type: LOADING_POST_STOP,
          payload: false
        });
        dispatch({
            type: DONATE_OK,
            payload: response.data
        });
      }
    })
    .catch(function (error) {
      dispatch({
        type: LOADING_POST_STOP,
        payload: false
      });
      if (error.response.data.status === 'Expired token') {
        localStorage.removeItem('token')


        setTimeout(function () {
          History.push('/signin');
        }, 1000);
        dispatch({ type: UNAUTH_USER });
      } else if(error.response.status === 400) {
        History.push('/unauthorized');
      }
      else if(error.response.status === 404) {
        History.push('/notFoundPage');
      }
      else {
        console.log(error.response.data.status);
      }
    });
  };
};


export const disable_donate = () => {
  return (dispatch) => {
    dispatch({
      type: DISABLE_DONATE
    });

  };
};
