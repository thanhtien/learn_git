import axios from 'axios';
import History from '../history.js';
import {
  LIST_NOTICATION_PAGE,
  LOADING_POST_START,
  LOADING_POST_STOP,
  UNAUTH_USER
} from './types';
var CONFIG = require('../config/common');

if (process.env.NODE_ENV === "development") {
  CONFIG = CONFIG.CONFIG.DEV;
}else {
  CONFIG = CONFIG.CONFIG.PRODUCT;
}
const ROOT_URL = CONFIG.ROOT_URL;
const token = 'Bearer '+ localStorage.getItem('token');
const config = {
  headers: {
    "Authorization" : token
  }
};


export const notificationList = (start) => {
  return (dispatch) => {

      dispatch({
        type: LOADING_POST_START,
        payload: true
      });

      //Config Axios
      if (start < 0) {
        start = 0;
      }

      axios.get(`${ROOT_URL}v2/user/Notification/LoadMoreNotification?page=${start}` , config)
        .then(response => {
          dispatch({
            type: LOADING_POST_STOP,
            payload: false
          });
          dispatch({
            type: LIST_NOTICATION_PAGE,
            payload:response.data,
            page:start
          });

          var number = Number(start)+1;
          window.history.replaceState({urlPath:''},"",`page=${number}`);



        }).catch((error) => {

          dispatch({
            type: LOADING_POST_STOP,
            payload: false
          });

          if (error) {
            if (error.response.data.status === 'Expired token') {
              localStorage.removeItem('token')
              setTimeout(function () {
                History.push('/signin');
              }, 1000);
              dispatch({ type: UNAUTH_USER });
            } else if(error.response.status === 400) {
              alert(error.response.data.status);
            }
            else if(error.response.status === 401) {
              History.push('/unauthorized');
            }
            else {
              alert(error.response.data.status);
            }
          }
        });
    };
};
