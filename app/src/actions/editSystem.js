import { change , initialize} from 'redux-form'
import axios from 'axios';
import History from '../history.js';
import { reset  } from 'redux-form'
import bcryptjs from 'bcryptjs'
import {
  UPLOAD_IMAGE_PROGESS,
  UNAUTH_USER,
  LOAD_USER,
  EDIT_AUTH,
  LOADING_POST_STOP,
  LOADING_POST_START,
  GET_PROJECT_POST,
  GET_PROJECT_DONATED,
  DIS_STATUS_API,
  ADD_NEW_PROJECT_OK,
  CONFIRM_OK,
  GET_PROJECT_DEFAULT_EDIT,
  EDIT_PROJECT_OK,
  EDIT_PROJECT_NO_CONFIRM_OK,
  SETTING_VISA_OK,
  SETTING_ATM_OK,
  SETTING_VISA_OK_FIRST,
  GET_STATICTIS_OK,
  PASSWORD_CHANGE_OK,
  EDIT_AUTH_DONATE,
  EDIT_PROJECT_NEWS_DATA,
  FETCH_PROJECTS_NEWS,
  SUB_EMAIL,
  DONATE_REPORT,
  PAGEVIEW_REPORT,
  DONATE_REPORT_TOTAL,
  LIST_EDIT_PUBLIC,
  GET_PROJECT_DONATED_FAN,
  LEAVE_FAN,
  REMOVE_VISA_OK,
  SETTING_VISA_NON_LOGIN,
  CONFIRM_RETURN,
  LIST_NOTICATION_STATIC,
  LIST_NOTICATION_HEADER,
  DETAIL_NOTICATION_HEADER

} from './types';


import smoothScroll from '../components/common/smoothScroll';

var CONFIG = require('../config/common');

if (process.env.NODE_ENV === "development") {
  CONFIG = CONFIG.CONFIG.DEV;
}else {
  CONFIG = CONFIG.CONFIG.PRODUCT;
}

const ROOT_URL = CONFIG.ROOT_URL;
const token = 'Bearer '+ localStorage.getItem('token');

const config = {
  headers: {
    "Authorization" : token
  }
};



export const notificationListAdminHeader = () => {
  return (dispatch) => {

      axios.get(`${ROOT_URL}v2/user/Notification/announceFromAdmin` , config)
        .then(response => {


          dispatch({
            type: LIST_NOTICATION_HEADER,
            payload:response.data,
          });




        }).catch((error) => {

          if (error) {
            alert(error.response.data.status);
          }
        });
    };
};

export const notificationListAdminHeaderDetail = (id,loading) => {
  return (dispatch) => {



      loading();
      axios.get(`${ROOT_URL}v2/user/Notification/DetailNotificationFromAdmin?id=${id}` , config)
        .then(response => {
          loading();
          dispatch({
            type: DETAIL_NOTICATION_HEADER,
            payload:response.data,
          });

        }).catch((error) => {
          loading();

          if (error) {
            if (error.response.data.status === 'Expired token') {
              localStorage.removeItem('token')
              setTimeout(function () {
                History.push('/signin');
              }, 1000);
              dispatch({ type: UNAUTH_USER });
            } else if(error.response.status === 400) {
              alert(error.response.data.status);
            }
            else if(error.response.status === 401) {
              History.push('/unauthorized');
            }
            else {
              alert(error.response.data.status);
            }
          }
        });
    };
};

export const notificationListStatic = (id,start) => {
  return (dispatch) => {

      dispatch({
        type: LOADING_POST_START,
        payload: true
      });

      //Config Axios
      if (start < 0) {
        start = 0;
      }

      axios.get(`${ROOT_URL}v2/user/Projects/NotificationForProject?project_id=${id}&page=${start}` , config)
        .then(response => {
          dispatch({
            type: LOADING_POST_STOP,
            payload: false
          });
          dispatch({
            type: LIST_NOTICATION_STATIC,
            payload:response.data,
            page:start
          });

          // var number = Number(start)+1;
          // window.history.replaceState({urlPath:''},"",`page=${number}`);



        }).catch((error) => {

          dispatch({
            type: LOADING_POST_STOP,
            payload: false
          });

          if (error) {
            if (error.response.data.status === 'Expired token') {
              localStorage.removeItem('token')
              setTimeout(function () {
                History.push('/signin');
              }, 1000);
              dispatch({ type: UNAUTH_USER });
            } else if(error.response.status === 400) {
              alert(error.response.data.status);
            }
            else if(error.response.status === 401) {
              History.push('/unauthorized');
            }
            else {
              alert(error.response.data.status);
            }
          }
        });
    };
};
export const ConfirmReturnEmail = (data,loadingConfirm,onClose) => {
  return (dispatch) => {
      //Config Axios


      loadingConfirm();


      var url = `${ROOT_URL}api/Users/sendMailDonor`;

      axios.post(url, data , config)
        .then(response => {
          loadingConfirm();


          dispatch({
            type: CONFIRM_RETURN,
            payload: data.id,

          });

          onClose();





        }).catch((error) => {
          loadingConfirm();

          if (error) {

            if (error.response.data.status === 'Expired token') {
              localStorage.removeItem('token')
              setTimeout(function () {
                History.push('/signin');
              }, 1000);
              dispatch({ type: UNAUTH_USER });
            } else if(error.response.status === 400) {
              alert(error.response.data.status);
            }
            else if(error.response.status === 401) {
              History.push('/unauthorized');
            }
            else if(error.response.status === 404) {
              History.push('/404');
            }
            else {
              alert(error.response.data.status);
            }
          }
        });
    };
};

export const stopDonateFanClub = (project_id,backing_levels_id,loadingLeave) => {
  return (dispatch) => {

      loadingLeave();


      var url = `${ROOT_URL}v2/user/FanClub/stopDonateFanClub?project_id=${project_id}&backing_levels_id=${backing_levels_id}`;


      axios.get(url, config)
        .then(response => {
          loadingLeave();



          dispatch({
            type: LEAVE_FAN,
            payload: response.data
          });





        }).catch((error) => {
          loadingLeave();

          if (error) {

            if (error.response.data.status === 'Expired token') {
              localStorage.removeItem('token')
              setTimeout(function () {
                History.push('/signin');
              }, 1000);
              dispatch({ type: UNAUTH_USER });
            } else if(error.response.status === 400) {
              alert(error.response.data.status);
            }
            else if(error.response.status === 401) {
              History.push('/unauthorized');
            }
            else if(error.response.status === 404) {
              History.push('/404');
            }
            else {
              alert(error.response.data.status);
            }
          }
        });
    };
};




export const StopProjectFanClub = (data) => {
  return (dispatch) => {

      dispatch({
        type: LOADING_POST_START,
        payload: true
      });


      var url = `${ROOT_URL}v2/user/FanClub/StopFanClub`;


      axios.post(url, data ,config)
        .then(response => {
          dispatch({
            type: LOADING_POST_STOP,
            payload: false
          });

          History.push('/my-page/post-project/page=1');

        }).catch((error) => {
          dispatch({
            type: LOADING_POST_STOP,
            payload: false
          });

          if (error) {

            if (error.response.data.status === 'Expired token') {
              localStorage.removeItem('token')
              setTimeout(function () {
                History.push('/signin');
              }, 1000);
              dispatch({ type: UNAUTH_USER });
            } else if(error.response.status === 400) {
              alert(error.response.data.status);
            }
            else if(error.response.status === 401) {
              History.push('/unauthorized');
            }
            else if(error.response.status === 404) {
              History.push('/404');
            }
            else {
              alert(error.response.data.status);
            }
          }
        });
    };
};


export const ProjectFanClubGet = (start) => {
  return (dispatch) => {
      dispatch({
        type: LOADING_POST_START,
        payload: true
      });


      //Config Axios
      if (start < 0) {
        start = 0;
      }

      axios.get(`${ROOT_URL}v2/user/FanClub/ListDonateFanClub?start=${start}` , config)
        .then(response => {
          dispatch({
            type: LOADING_POST_STOP,
            payload: false
          });


          dispatch({
            type: GET_PROJECT_DONATED_FAN,
            payload: response.data,
            page:start
          });

          var number = Number(start)+1;
          window.history.replaceState({urlPath:''},"",`page=${number}`);
        }).catch((error) => {

          dispatch({
            type: LOADING_POST_STOP,
            payload: false
          });
          if (error) {

            if (error.response.data.status === 'Expired token') {
              localStorage.removeItem('token')
              setTimeout(function () {
                History.push('/signin');
              }, 1000);
              dispatch({ type: UNAUTH_USER });
            } else if(error.response.status === 400) {
              alert(error.response.data.status);
            }
            else if(error.response.status === 401) {
              History.push('/unauthorized');
            }
            else if(error.response.status === 404) {
              History.push('/404');
            }
            else {
              alert(error.response.data.status);
            }
          }

        });
    };
};




export const ListEditPublic = (id,start) => {
  return (dispatch) => {

      dispatch({
        type: LOADING_POST_START,
        payload: true
      });
      if (start < 0) {
        start = 0;
      }
      var url = `${ROOT_URL}v2/user/Projects/listProjectReturnClaimActive?project_id=${id}&page=${start}`;

      axios.get(url, config)
        .then(response => {
          dispatch({
            type: LOADING_POST_STOP,
            payload: false
          });
          dispatch({
            type: LIST_EDIT_PUBLIC,
            payload: response.data,
            page:start
          });

          var number = Number(start)+1;
          window.history.replaceState({urlPath:''},"",`page=${number}`);


        }).catch((error) => {
          dispatch({
            type: LOADING_POST_STOP,
            payload: false
          });
          if (error) {

            if (error.response.data.status === 'Expired token') {
              localStorage.removeItem('token')
              setTimeout(function () {
                History.push('/signin');
              }, 1000);
              dispatch({ type: UNAUTH_USER });
            } else if(error.response.status === 400) {
              alert(error.response.data.status);
            }
            else if(error.response.status === 401) {
              History.push('/unauthorized');
            }
            else if(error.response.status === 404) {
              History.push('/404');
            }
            else {
              alert(error.response.data.status);
            }
          }
        });
    };
};


export const AddNewEditPublic = (data) => {
  return (dispatch) => {


      dispatch({
        type: LOADING_POST_START,
        payload: true
      });

      axios.post(`${ROOT_URL}v2/user/Projects/updateProjectActive` , data , config)
        .then(response => {
          if (response.data) {
            dispatch({
              type: LOADING_POST_STOP,
              payload: false
            });
            dispatch(reset('new_project_public_request'));
            History.push(`/my-page/edit-project-public/${data.id}/page=1`);
          }

        }).catch((error) => {
          if (error) {
            dispatch({
              type: LOADING_POST_STOP,
              payload: false
            });
            if (error.response.data.status === 'Expired token') {
              localStorage.removeItem('token')
              setTimeout(function () {
                History.push('/signin');
              }, 1000);
              dispatch({ type: UNAUTH_USER });
            } else if(error.response.status === 400) {
              alert(error.response.data.status);
              History.push(`/my-page/edit-project-public/${data.id}/page=1`);
            }
            else if(error.response.status === 401) {
              History.push('/unauthorized');
            }
            else {
              alert(error.response.data.status);
            }
          }
        });
    };
};



export const changeSubcriber = (loading,data,subcriber) => {
  return (dispatch) => {

      loading();

      axios.post(`${ROOT_URL}v2/user/Subscribe/unSubscribeEmail2` , data , config)
        .then(response => {
          dispatch({
            type: SUB_EMAIL,
            payload:response.data
          });
          loading();
          if (subcriber) {
            History.push('/my-page/setting-user/edit-profile');
          }

        }).catch((error) => {

          loading();

          if (error) {
            if (error.response.data.status === 'Expired token') {
              localStorage.removeItem('token')
              setTimeout(function () {
                History.push('/signin');
              }, 1000);
              dispatch({ type: UNAUTH_USER });
            } else if(error.response.status === 400) {
              alert(error.response.data.status);
            }
            else if(error.response.status === 401) {
              History.push('/unauthorized');
            }
            else {
              alert(error.response.data.status);
            }
          }
        });
    };
};

export const DoChangePassword = async (data) =>{
  //data.old_password = await bcryptjs.hash(data.old_password, 7)
  data.password = await bcryptjs.hash(data.password, 7);
  data.re_password = await bcryptjs.hash(data.re_password, 7);
  console.log(data);
  return await axios.put(`${ROOT_URL}api/Users/changePass`, data , config);
}

export const ChangePassword = (data) => {
  // console.log('ChangePassword',data)
  // console.log('bcryptjs',bcryptjs)
    
  return (dispatch) => {

    dispatch({
      type: LOADING_POST_START,
      payload: true
    });
    data.password = bcryptjs.hashSync(data.password,7);

    data.re_password = bcryptjs.hashSync(data.re_password,7);
    console.log('ChangePassword',data)
    //DoChangePassword(data)
    axios.put(`${ROOT_URL}api/Users/changePass`, data , config)
    .then(response => {
      dispatch({
        type: LOADING_POST_STOP,
        payload: false
      });

      dispatch({
        type: PASSWORD_CHANGE_OK
      });
    }).catch((error) => {

      dispatch({
        type: LOADING_POST_STOP,
        payload: false
      });
      if (error) {
        if (error.response.data.status === 'Expired token') {
          localStorage.removeItem('token')


          setTimeout(function () {
            History.push('/signin');
          }, 1000);
          dispatch({ type: UNAUTH_USER });
        } else if(error.response.status === 400) {
          alert(error.response.data.status);
        }
        else if(error.response.status === 401) {
          History.push('/unauthorized');
        }
        else {
          alert(error.response.data.status);
        }
      }
    });
  };
};

export const load = (data) => {
  return (dispatch) => {
    dispatch({
        type: LOAD_USER,
        payload: data
    });
  };
};
/**
 * [InitialData For Data Default Edit Setting User]
 * @param {[type]} data [description]
 */
export const InitialData = (data) => {
  return (dispatch) => {
    dispatch(initialize('edit-setting-user', data));
  };
};

/**
 * [InitialData For SET Data Default FORM Edit Project]
 * @param {[type]} data [description]
 */
export const InitialDataProject = (data) => {
  return (dispatch) => {
    dispatch(initialize('edit-project-user', data));
  };
};


/**
 * [InitialData For Data Default Edit Setting User]
 * @param {[type]} data [description]
 */
export const InitialDataAddressReturnEdit = (data) => {
  return (dispatch) => {
    dispatch(initialize('Address-Return-Edit', data));
  };
};




/**
 * [EditProject For Edit ProJect]
 * @param {[type]} data [Data Send To Edit]
 */


/**
 * [getProjectDefaultEdit API GET BACKEND DATA EDIT PROJECT USER]
 * @param  {[type]} id [ID PROJECT]
 * @return {[type]}    [DATA PROJECT]
 */
 function centsToDollaString(x){
   return x.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
 }


export const getProjectDefaultEdit = (id) => {
  return (dispatch) => {

      dispatch({
        type: LOADING_POST_START,
        payload: true
      });



      axios.get(`${ROOT_URL}api/Users/project_detail?id=${id}` , config)
        .then(response => {
          dispatch({
            type: LOADING_POST_STOP,
            payload: false
          });

          var dataProject = response.data.project;
          dataProject.project_return = response.data.project_return;

          if (dataProject.project_type !== '1') {
            dataProject.goal_amount = centsToDollaString(dataProject.goal_amount);
          }



          for (var i = 0; i < dataProject.project_return.length; i++) {

            if (dataProject.project_return[i].invest_amount) {
              dataProject.project_return[i].invest_amount = centsToDollaString(dataProject.project_return[i].invest_amount);
            }
            if (dataProject.project_return[i].max_count) {
              dataProject.project_return[i].max_count = centsToDollaString(dataProject.project_return[i].max_count);
            }

          }

          dispatch({
            type: GET_PROJECT_DEFAULT_EDIT,
            payload:dataProject
          });



        })
        .catch((error) => {
          dispatch({
            type: LOADING_POST_STOP,
            payload: false
          });
          if (error.response.data.status === 'Expired token') {
            localStorage.removeItem('token')
            setTimeout(function () {
              History.push('/signin');
            }, 1000);
            dispatch({ type: UNAUTH_USER });
          } else if(error.response.status === 400) {
            alert(error.response.data.status);
          }
          else if(error.response.status === 401) {
            History.push('/unauthorized');
          }
          else if(error.response.status === 404) {
            History.push('/404');
          }
          else {
            alert(error.response.data.status);
          }
        });
    };
};

export const confirmProject = (id) => {
  return (dispatch) => {

      dispatch({
        type: LOADING_POST_START,
        payload: true
      });
      var data = {
        action:"create",
        project_id:id
      }

      axios.post(`${ROOT_URL}v2/Firebase/Curl` , data , config)
        .then(response => {
          console.log(response.data);
      });

      axios.get(`${ROOT_URL}api/Users/sendProjectActive?id=${id}` , config)
        .then(response => {
          dispatch({
            type: CONFIRM_OK,
            payload:true
          });
          window.location.assign("/my-page/post-project/page=1");
        }).catch((error) => {
          dispatch({
            type: LOADING_POST_STOP,
            payload: false
          });
          if (error.response.data.status === 'Expired token') {
            localStorage.removeItem('token')
            setTimeout(function () {
              History.push('/signin');
            }, 1000);
            dispatch({ type: UNAUTH_USER });
          } else if(error.response.status === 401) {
            History.push('/unauthorized');
          }
          else if(error.response.status === 400) {
            alert(error.response.data.status);
          }
          else if(error.response.status === 404) {
            History.push('/notFoundPage');
          }
          else {
            alert(error.response.data.status);
          }
        });
    };
};

export const addNewProject = (data) => {
  return (dispatch) => {

      dispatch({
        type: LOADING_POST_START,
        payload: true
      });
      console.log(data);
      axios.post(`${ROOT_URL}api/Users/createProject2`, data , config)
        .then(response => {

          dispatch({
            type: LOADING_POST_STOP,
            payload: false
          });

          dispatch({
            type: ADD_NEW_PROJECT_OK,
            payload:response
          });


        }).catch((error) => {

          dispatch({
            type: LOADING_POST_STOP,
            payload: false
          });

          if (error) {
            if (error.response.data.status === 'Expired token') {
              localStorage.removeItem('token')
              setTimeout(function () {
                History.push('/signin');
              }, 1000);
              dispatch({ type: UNAUTH_USER });
            } else if(error.response.status === 400) {
              console.log(error.response.data);
              // alert(error.response.data);
            }
            else {
              alert(error.response.data.status);
            }
          }

        });
    };
};



export const UploadImage = (files,formName,feild,path,imageStatus,imagePreview) => {
    return (dispatch) => {

      let formData = new FormData();
      formData.append('files',files);
      //Config Axios
      const config = {
          headers: {
            "Authorization" : token
          },
          onUploadProgress: progressEvent => {
          var percent = Math.round(progressEvent.loaded * 100 / progressEvent.total);
          dispatch({ type: UPLOAD_IMAGE_PROGESS,payload:percent });
        }
      };

      axios.post(`${ROOT_URL}api/Upload/${path}`, formData, config)
        .then(response => {

          dispatch(change(formName, feild, response.data.base_url+response.data.forder+response.data.name_image+response.data.mime_type));

          imageStatus(false);
          setTimeout(function () {
            dispatch({ type: UPLOAD_IMAGE_PROGESS,payload:0 });
          }, 600);

        }).catch((error) => {

          imageStatus(true);
          if (imagePreview) {
            dispatch(change(formName, feild, imagePreview));
          }else {
            dispatch(change(formName, feild, null));
          }

          setTimeout(function () {
            dispatch({ type: UPLOAD_IMAGE_PROGESS,payload:0 });
          }, 600);


          if (error.response.data.status === 'Expired token') {
            localStorage.removeItem('token')
            setTimeout(function () {
              History.push('/signin');
            }, 1000);
            dispatch({ type: UNAUTH_USER });
          }else {

            alert(error.response.data.error);
          }
        });
    };
};

export const Dis_Status_Api = (status) => {
  return(dispatch) => {
    dispatch({
      type: DIS_STATUS_API,
      payload: status
    });
  }
}

export const editUser = (data) => {
  return (dispatch) => {

      dispatch({
        type: LOADING_POST_START,
        payload: true
      });

      axios.put(`${ROOT_URL}api/Users/editUser`, data , config)
        .then(response => {
          dispatch({
            type: LOADING_POST_STOP,
            payload: false
          });

          dispatch({
            type: EDIT_AUTH,
            payload: data
          });

          smoothScroll.scrollTo('push-notification');


        }).catch((error) => {
          if (error) {
            dispatch({
              type: LOADING_POST_STOP,
              payload: false
            });
            if (error.response.data.status === 'Expired token') {
              localStorage.removeItem('token')


              setTimeout(function () {
                History.push('/signin');
              }, 1000);
              dispatch({ type: UNAUTH_USER });
            }else {
              console.log(error.response);
            }
          }
        });
    };
};



export const AddressReturn = (data,close) => {
  return (dispatch) => {

      dispatch({
        type: LOADING_POST_START,
        payload: true
      });

      axios.post(`${ROOT_URL}v2/user/AddressUsers/createAddressUser`, data , config)
        .then(response => {
          dispatch({
            type: LOADING_POST_STOP,
            payload: false
          });

          dispatch({
            type: EDIT_AUTH,
            payload: response.data
          });

          smoothScroll.scrollTo('push-notification');
          if (close) {

            close()

          }


        }).catch((error) => {
          dispatch({
            type: LOADING_POST_STOP,
            payload: false
          });
          if (error.response.data.status === 'Expired token') {
            localStorage.removeItem('token')


            setTimeout(function () {
              History.push('/signin');
            }, 1000);
            dispatch({ type: UNAUTH_USER });
          } else if(error.response.status === 400) {

            alert(error.response.data.status);

          }
          else {

            alert(error.response.data.status);

          }
        });
    };
};


export const AddressReturnDonate = (data) => {
  return (dispatch) => {

      dispatch({
        type: LOADING_POST_START,
        payload: true
      });

      axios.post(`${ROOT_URL}v2/user/AddressUsers/createAddressUser`, data , config)
        .then(response => {
          dispatch({
            type: LOADING_POST_STOP,
            payload: false
          });

          dispatch({
            type: EDIT_AUTH_DONATE,
            payload: response.data
          });
          smoothScroll.scrollTo('default-address');



        }).catch((error) => {
          dispatch({
            type: LOADING_POST_STOP,
            payload: false
          });
          if (error.response.data.status === 'Expired token') {
            localStorage.removeItem('token')


            setTimeout(function () {
              History.push('/signin');
            }, 1000);
            dispatch({ type: UNAUTH_USER });
          } else if(error.response.status === 400) {

            alert(error.response.data.status);

          }
          else {

            alert(error.response.data.status);

          }
        });
    };
};

export const AddressReturnEdit = (data, close, closeForm) => {
  return (dispatch) => {

      dispatch({
        type: LOADING_POST_START,
        payload: true
      });

      axios.put(`${ROOT_URL}v2/user/AddressUsers/editAddressUser`, data , config)
        .then(response => {
          dispatch({
            type: LOADING_POST_STOP,
            payload: false
          });

          dispatch({
            type: EDIT_AUTH,
            payload: response.data
          });

          smoothScroll.scrollTo('push-notification');
          if (close) {
            close()
          }

          if (closeForm) {
            closeForm()
          }


        }).catch((error) => {

          dispatch({
            type: LOADING_POST_STOP,
            payload: false
          });

          if (error.response.data.status === 'Expired token') {
            localStorage.removeItem('token')


            setTimeout(function () {
              History.push('/signin');
            }, 1000);
            dispatch({ type: UNAUTH_USER });
          } else if(error.response.status === 400) {

            alert(error.response.data.status);

          }
          else {

            alert(error.response.data.status);

          }
        });
    };
};


export const AddressReturnEditDonate = (data, close, closeForm) => {
  return (dispatch) => {

      dispatch({
        type: LOADING_POST_START,
        payload: true
      });

      axios.put(`${ROOT_URL}v2/user/AddressUsers/editAddressUser`, data , config)
        .then(response => {
          dispatch({
            type: LOADING_POST_STOP,
            payload: false
          });

          dispatch({
            type: EDIT_AUTH_DONATE,
            payload: response.data
          });

          smoothScroll.scrollTo('default-address');



        }).catch((error) => {

          dispatch({
            type: LOADING_POST_STOP,
            payload: false
          });

          if (error.response.data.status === 'Expired token') {
            localStorage.removeItem('token')


            setTimeout(function () {
              History.push('/signin');
            }, 1000);
            dispatch({ type: UNAUTH_USER });
          } else if(error.response.status === 400) {

            alert(error.response.data.status);

          }
          else {

            alert(error.response.data.status);

          }
        });
    };
};


export const SetDefaultAddress = (id,close) => {
  return (dispatch) => {

      dispatch({
        type: LOADING_POST_START,
        payload: true
      });

      axios.get(`${ROOT_URL}v2/user/AddressUsers/chosenDefault?id=${id}`, config)
        .then(response => {
          dispatch({
            type: LOADING_POST_STOP,
            payload: false
          });

          dispatch({
            type: EDIT_AUTH,
            payload: response.data
          });




        }).catch((error) => {
          dispatch({
            type: LOADING_POST_STOP,
            payload: false
          });
          if (error.response.data.status === 'Expired token') {
            localStorage.removeItem('token')


            setTimeout(function () {
              History.push('/signin');
            }, 1000);
            dispatch({ type: UNAUTH_USER });
          } else if(error.response.status === 400) {

            alert(error.response.data.status);

          }
          else {

            alert(error.response.data.status);

          }
        });
    };
};


export const SetDefaultAddressStepOne = (id) => {
  return (dispatch) => {

      dispatch({
        type: LOADING_POST_START,
        payload: true
      });

      axios.get(`${ROOT_URL}v2/user/AddressUsers/chosenDefault?id=${id}`, config)
        .then(response => {
          dispatch({
            type: LOADING_POST_STOP,
            payload: false
          });

          dispatch({
            type: EDIT_AUTH_DONATE,
            payload: response.data
          });
          smoothScroll.scrollTo('default-address');


        }).catch((error) => {
          dispatch({
            type: LOADING_POST_STOP,
            payload: false
          });
          if (error.response.data.status === 'Expired token') {
            localStorage.removeItem('token')


            setTimeout(function () {
              History.push('/signin');
            }, 1000);
            dispatch({ type: UNAUTH_USER });
          } else if(error.response.status === 400) {

            alert(error.response.data.status);

          }
          else {

            alert(error.response.data.status);

          }
        });
    };
};
export const getProjectPostNow = (start) => {
  return (dispatch) => {

      dispatch({
        type: LOADING_POST_START,
        payload: true
      });
      if (start < 0) {
        start = 0;
      }




      axios.get(`${ROOT_URL}api/Users/post_project?start=${start}` , config)
        .then(response => {
          dispatch({
            type: LOADING_POST_STOP,
            payload: false
          });
          dispatch({
            type: GET_PROJECT_POST,
            payload: response.data,
            page:start
          });

          var number = Number(start)+1;
          window.history.replaceState({urlPath:''},"",`page=${number}`)
        }).catch((error) => {
          dispatch({
            type: LOADING_POST_STOP,
            payload: false
          });
          if (error.response.data.status === 'Expired token') {
            localStorage.removeItem('token')


            setTimeout(function () {
              History.push('/signin');
            }, 1000);
            dispatch({ type: UNAUTH_USER });
          }else {
            console.log(error);
            console.log(token);
          }
        });

    };
};



export const getProjectdonated = (start) => {
  return (dispatch) => {
      dispatch({
        type: LOADING_POST_START,
        payload: true
      });


      if (start < 0) {
        start = 0;
      }

      axios.get(`${ROOT_URL}v2/user/InfoDonate/ListDonate?start=${start}` , config)
        .then(response => {
          dispatch({
            type: LOADING_POST_STOP,
            payload: false
          });
          dispatch({
            type: GET_PROJECT_DONATED,
            payload: response.data,
            page:start
          });

          var number = Number(start)+1;
          window.history.replaceState({urlPath:''},"",`page=${number}`);
        }).catch((error) => {

          dispatch({
            type: LOADING_POST_STOP,
            payload: false
          });

        });
    };
};





export const EditProject = (data) => {
  return (dispatch) => {

      dispatch({
        type: LOADING_POST_START,
        payload: true
      });

      axios.put(`${ROOT_URL}api/Users/editProject2/${data.id}`, data , config)

        .then(response => {
          dispatch({
            type: LOADING_POST_STOP,
            payload: false
          });



          if (response.data.project.opened === 'no') {

            dispatch({
              type: EDIT_PROJECT_OK,
              payload:response.data
            });

          }else {

            dispatch({
              type: EDIT_PROJECT_NO_CONFIRM_OK,
              payload:response.data,
              status:true
            });

            History.push('/my-page/post-project/page=1');

          }

        }).catch((error) => {
          console.log(error);
          dispatch({
            type: LOADING_POST_STOP,
            payload: false
          });
          if (error.response.data.status === 'Expired token') {
            localStorage.removeItem('token')


            setTimeout(function () {
              History.push('/signin');
            }, 1000);
            dispatch({ type: UNAUTH_USER });
          } else if(error.response.status === 400) {

            alert(error.response.data.status);

          }
          else {

            // alert(error.response.data);
            console.log(error.response.data);

          }
        });
    };
};



/**
 * [Donate For Donate]
 * @param {[type]} amount           [Money Donate]
 * @param {[type]} project_id
 * @param {[type]} backing_level_id
 */

export const SettingViSaDonate = (dataSend) => {

  return (dispatch) => {

    dispatch({
      type: LOADING_POST_START,
      payload: true
    });

    var data = window.btoa(unescape(encodeURIComponent(JSON.stringify(dataSend))));

    axios.put(ROOT_URL+'/api/Users/editStripe' , {data} , config)
    .then(response =>{
      if (response) {

        var backLink = localStorage.getItem('backLinkToDoante');
        if (backLink) {
          dispatch({
            type: LOADING_POST_STOP,
            payload: false
          });
          dispatch({
              type: SETTING_VISA_OK,
              payload: response.data
          });
          localStorage.removeItem('backLinkToDoante')

          History.push(backLink);
        }else {
          dispatch({
            type: LOADING_POST_STOP,
            payload: false
          });
          dispatch({
              type: SETTING_VISA_OK,
              payload: response.data
          });
          smoothScroll.scrollTo('push-notification');
        }



      }
    })
    .catch(function (error) {

      dispatch({
        type: LOADING_POST_STOP,
        payload: false
      });
      if (error.response.data.status === 'Expired token') {
        localStorage.removeItem('token')


        setTimeout(function () {
          History.push('/signin');
        }, 1000);
        dispatch({ type: UNAUTH_USER });
      } else if(error.response.status === 400) {

        alert(error.response.data.status);

      }
      else if(error.response.status === 401) {
        History.push('/unauthorized');


      }
      else if(error.response.status === 404) {
        History.push('/notFoundPage');
      }
      else {
        alert(error.response.data.status);

      }
    });
  };
};




export const removeCredit = (resetForm) => {

  return (dispatch) => {

    dispatch({
      type: LOADING_POST_START,
      payload: true
    });

    axios.get(ROOT_URL+'/v2/user/InfoDonate/delInfoCard' , config)
    .then(response =>{
      if (response) {

        dispatch({
          type: LOADING_POST_STOP,
          payload: false
        });
        dispatch({
            type: REMOVE_VISA_OK,
            payload: response.data
        });
        resetForm();


        smoothScroll.scrollTo('push-notification');



      }
    })
    .catch(function (error) {

      dispatch({
        type: LOADING_POST_STOP,
        payload: false
      });
      if (error) {
        console.log(error);
      }
      // if (error.response.data.status === 'Expired token') {
      //   localStorage.removeItem('token')
      //
      //   setTimeout(function () {
      //     History.push('/signin');
      //   }, 1000);
      //   dispatch({ type: UNAUTH_USER });
      // } else if(error.response.status === 400) {
      //
      //   alert(error.response.data.status);
      //
      // }
      // else if(error.response.status === 401) {
      //   History.push('/unauthorized');
      //
      //
      // }
      // else if(error.response.status === 404) {
      //   History.push('/notFoundPage');
      // }
      // else {
      //   alert(error.response.data.status);
      //
      // }
    });
  };
};


/**
 * [SettingATM For Get Money Donate]
 */

export const SettingATM = (data) => {
  return (dispatch) => {

    dispatch({
      type: LOADING_POST_START,
      payload: true
    });

    axios.put(ROOT_URL+'/api/Users/editUser' , data , config)
    .then(response =>{

      if (response) {
        var backLink = localStorage.getItem('backLinkToDoante');
        dispatch({
          type: LOADING_POST_STOP,
          payload: false
        });
        dispatch({
            type: SETTING_ATM_OK,
            payload: response.data
        });
        if (backLink) {
          localStorage.removeItem('backLinkToDoante')
          History.push(backLink);
        }else {
          smoothScroll.scrollTo('push-notification');
        }


      }
    })
    .catch(function (error) {
      dispatch({
        type: LOADING_POST_STOP,
        payload: false
      });

      if (error.response.data.status === 'Expired token') {
        localStorage.removeItem('token')


        setTimeout(function () {
          History.push('/signin');
        }, 1000);
        dispatch({ type: UNAUTH_USER });
      } else if(error.response.status === 400) {
        alert(error.response.data.status);
      }
      else if(error.response.status === 401) {

        History.push('/unauthorized');
      }
      else if(error.response.status === 404) {
        History.push('/notFoundPage');
      }
      else {
        alert(error.response.data.status);
      }
    });
  };
};




export const SettingViSaDonateFirstTime = (dataSend , fu) => {
  return (dispatch) => {

    dispatch({
      type: LOADING_POST_START,
      payload: true
    });
    var data = window.btoa(unescape(encodeURIComponent(JSON.stringify(dataSend))));

    axios.put(ROOT_URL+'/api/Users/editStripe' , {data} , config)
    .then(response =>{
      if (response) {
        dispatch({
          type: LOADING_POST_STOP,
          payload: false
        });
        dispatch({
            type: SETTING_VISA_OK_FIRST,
            payload: response.data
        });
        fu();
      }
    })
    .catch(function (error) {
      dispatch({
        type: LOADING_POST_STOP,
        payload: false
      });

      if (error.response.data.status === 'Expired token') {
        localStorage.removeItem('token')
        setTimeout(function () {
          History.push('/signin');
        }, 1000);
        dispatch({ type: UNAUTH_USER });
      } else if(error.response.status === 400) {

        alert(error.response.data.status);
        dispatch({
          type: SETTING_VISA_OK_FIRST,
          payload: null
        });
        // History.push('/unauthorized');
      }
      else if(error.response.status === 404) {
        History.push('/notFoundPage');
      }
      else {
        alert(error.response.data.status);
      }
    });
  };
};


export const SettingViSaDonateNonLogin = (dataSend , fu) => {
  return (dispatch) => {


    dispatch({
      type: LOADING_POST_START,
      payload: true
    });
    var dataBack = dataSend;
    var data = window.btoa(unescape(encodeURIComponent(JSON.stringify(dataSend))));
    axios.post(ROOT_URL+'v2/guest/Donate/createInfoCard' , {data} )
    .then(response =>{
      if (response) {
        dispatch({
          type: LOADING_POST_STOP,
          payload: false
        });

        dataBack.customerId = response.data.customerId;

        dispatch({
            type: SETTING_VISA_NON_LOGIN,
            payload: dataBack
        });
        fu();
      }
    })
    .catch(function (error) {

      dispatch({
        type: LOADING_POST_STOP,
        payload: false
      });

      if (error.response.data.status === 'Expired token') {
        localStorage.removeItem('token')
        setTimeout(function () {
          History.push('/signin');
        }, 1000);
        dispatch({ type: UNAUTH_USER });
      } else if(error.response.status === 400) {
        alert(error.response.data.status);
        dispatch({
          type: SETTING_VISA_OK_FIRST,
          payload: null
        });
        // History.push('/unauthorized');
      }
      else if(error.response.status === 404) {
        History.push('/notFoundPage');
      }
      else {
        alert(error.response.data.status);

      }
    });


  };
};



export const SettingBankDonateFirstTime = (data , fu) => {
  return (dispatch) => {

    dispatch({
      type: LOADING_POST_START,
      payload: true
    });

    axios.put(ROOT_URL+'/api/Users/editUser' , data , config)
    .then(response =>{
      if (response) {
        dispatch({
          type: LOADING_POST_STOP,
          payload: false
        });
        dispatch({
            type: SETTING_VISA_OK_FIRST,
            payload: response.data
        });
        fu();
      }
    })
    .catch(function (error) {
      dispatch({
        type: LOADING_POST_STOP,
        payload: false
      });

      if (error.response.data.status === 'Expired token') {
        localStorage.removeItem('token')


        setTimeout(function () {
          History.push('/signin');
        }, 1000);
        dispatch({ type: UNAUTH_USER });
      } else if(error.response.status === 400) {

        alert(error.response.data.status);
        dispatch({
          type: SETTING_VISA_OK_FIRST,
          payload: null
        });
        // History.push('/unauthorized');
      }
      else if(error.response.status === 404) {
        History.push('/notFoundPage');
      }
      else {

        alert(error.response.data.status);

      }
    });
  };
};




export const deleteProject = (data,page) => {
  return (dispatch) => {

    dispatch({
      type: LOADING_POST_START,
      payload: true
    });

    axios.delete(ROOT_URL+'/api/Users/deleteProject/'+data.id , config)
    .then(response =>{
      if (response) {
        window.location.reload();
      }
    })
    .catch(function (error) {
      dispatch({
        type: LOADING_POST_STOP,
        payload: false
      });
      if (error.response.data.status === 'Expired token') {
        localStorage.removeItem('token')


        setTimeout(function () {
          History.push('/signin');
        }, 1000);
        dispatch({ type: UNAUTH_USER });
      } else if(error.response.status === 401) {
        History.push('/unauthorized');
      }
      else if(error.response.status === 400) {
        alert(error.response.data.status);
      }
      else if(error.response.status === 404) {
        History.push('/notFoundPage');
      }
      else {

        alert(error.response.data.status);
      }
    });
  };
};



export const getprojectStatistic = (id,page) => {

  return (dispatch) => {

    dispatch({
      type: LOADING_POST_START,
      payload: true
    });

    axios.get(ROOT_URL+`/api/Users/statisticDonate?id=${id}&start=${page}` , config)
    .then(response =>{
      if (response) {
        dispatch({
          type: LOADING_POST_STOP,
          payload: false
        });

        dispatch({
            type: GET_STATICTIS_OK,
            payload: response.data
        });
      }
    })
    .catch(function (error) {
      dispatch({
        type: LOADING_POST_STOP,
        payload: false
      });
      if (error.response.data.status === 'Expired token') {
        localStorage.removeItem('token')


        setTimeout(function () {
          History.push('/signin');
        }, 1000);
        dispatch({ type: UNAUTH_USER });
      } else if(error.response.status === 401) {
        History.push('/unauthorized');
      }
      else if(error.response.status === 400) {
        alert(error.response.data.status);

      }
      else if(error.response.status === 404) {
        History.push('/notFoundPage');
      }
      else {
        alert(error.response.data.status);
      }
    });
  };
};


export const getGooglePageView = (id,loading,first_day,last_day) => {
  return (dispatch) => {

    var urlApi = `${ROOT_URL}/v2/user/ReportAnalytics?link=/project-detail/${id}&project_id=${id}&first_day=${first_day}&last_day=${last_day}`;

    loading();
    axios.get(urlApi  ,  config)
    .then(response =>{
      if (response) {
        dispatch({
          type: PAGEVIEW_REPORT,
          payload: response.data
        });
        loading();
      }
    })
    .catch(function (error) {
      loading();
      if (error.response.data.status === 'Expired token') {
        localStorage.removeItem('token')
        setTimeout(function () {
          History.push('/signin');
        }, 1000);
        dispatch({ type: UNAUTH_USER });
      } else if(error.response.status === 401) {
        History.push('/unauthorized');
      }
      else if(error.response.status === 400) {
        alert(error.response.data.status);

      }
      else if(error.response.status === 404) {
        History.push('/notFoundPage');
      }
      else {
        alert(error.response.data);
      }
    });

  };
}


export const getDonateChart = (id,loading,first_day,last_day) => {
  return (dispatch) => {

    var urlApi = `${ROOT_URL}/v2/user/InfoDonate/listDonateChart?project_id=${id}&first_day=${first_day}&last_day=${last_day}`;
    loading();
    axios.get(urlApi  ,  config)
    .then(response =>{
      if (response) {

        dispatch({
          type: DONATE_REPORT,
          payload: response.data
        });
        loading();
      }
    })
    .catch(function (error) {
      loading();

      if (error.response.data.status === 'Expired token') {
        localStorage.removeItem('token')


        setTimeout(function () {
          History.push('/signin');
        }, 1000);
        dispatch({ type: UNAUTH_USER });
      } else if(error.response.status === 401) {
        History.push('/unauthorized');
      }
      else if(error.response.status === 400) {
        alert(error.response.data.status);

      }
      else if(error.response.status === 404) {
        History.push('/notFoundPage');
      }
      else {
        alert(error.response.data);
      }
    });

  };
}

export const getDonateTotalChart = (id,loading,last_day) => {
  return (dispatch) => {


    var urlApi = `${ROOT_URL}/v2/user/InfoDonate/listDonateChartTotal?project_id=${id}&last_day=${last_day}`;
    loading();
    axios.get(urlApi  ,  config)
    .then(response =>{
      if (response) {

        dispatch({
          type: DONATE_REPORT_TOTAL,
          payload: response.data
        });
        loading();
      }
    })
    .catch(function (error) {
      loading();

      if (error.response.data.status === 'Expired token') {
        localStorage.removeItem('token')


        setTimeout(function () {
          History.push('/signin');
        }, 1000);
        dispatch({ type: UNAUTH_USER });
      } else if(error.response.status === 401) {
        History.push('/unauthorized');
      }
      else if(error.response.status === 400) {
        alert(error.response.data.status);

      }
      else if(error.response.status === 404) {
        History.push('/notFoundPage');
      }
      else {
        alert(error.response.data.error);
      }
    });

  };
}

export const addNewProjectPost = (data) => {

  return (dispatch) => {

    dispatch({
      type: LOADING_POST_START,
      payload: true
    });

    axios.post(ROOT_URL+`v2/user/ReportDaily/createReportDaily` , data ,  config)
    .then(response =>{
      if (response) {
        dispatch({
          type: LOADING_POST_STOP,
          payload: false
        });

        History.push('/my-page/statistic/'+data.project_id);
      }
    })
    .catch(function (error) {
      dispatch({
        type: LOADING_POST_STOP,
        payload: false
      });
      if (error.response.data.status === 'Expired token') {
        localStorage.removeItem('token')


        setTimeout(function () {
          History.push('/signin');
        }, 1000);
        dispatch({ type: UNAUTH_USER });
      } else if(error.response.status === 401) {
        History.push('/unauthorized');
      }
      else if(error.response.status === 400) {
        alert(error.response.data.status);

      }
      else if(error.response.status === 404) {
        History.push('/notFoundPage');
      }
      else {
        alert(error.response.data);
      }
    });
  };
};


export const NewProjectListGet = (project_id,start) => {

  return (dispatch) => {

    dispatch({
      type: LOADING_POST_START,
      payload: true
    });
    if (start < 0) {
      start = 0;
    }
    axios.get(ROOT_URL+`v2/user/ReportDaily/listReport?project_id=${project_id}&page=${start}`  ,  config)
    .then(response =>{
      if (response) {


        dispatch({
          type: LOADING_POST_STOP,
          payload: false
        });

        dispatch({
            type: FETCH_PROJECTS_NEWS,
            payload: response.data,
            page:start
        });
        if (start !== 0 ) {
          var number = Number(start) + 1;
          window.history.replaceState({urlPath:''},"",`page=${number}`);
        }



      }
    })
    .catch(function (error) {
      dispatch({
        type: LOADING_POST_STOP,
        payload: false
      });
      if (error.response.data.status === 'Expired token') {
        localStorage.removeItem('token')


        setTimeout(function () {
          History.push('/signin');
        }, 1000);
        dispatch({ type: UNAUTH_USER });
      } else if(error.response.status === 401) {
        History.push('/unauthorized');
      }
      else if(error.response.status === 400) {
        alert(error.response.data.status);

      }
      else if(error.response.status === 404) {
        History.push('/notFoundPage');
      }
      else {
        alert(error.response.data);
      }
    });
  };
};


/**
 * [InitialData For Data Default Edit Setting User]
 * @param {[type]} data [description]
 */
export const InitialDataReportNew = (id) => {

  return (dispatch) => {

    dispatch({
      type: LOADING_POST_START,
      payload: true
    });

    axios.get(ROOT_URL+`/v2/guest/ListReportDaily/ReportDetail?id=${id}`)
    .then(response =>{
      if (response) {
        dispatch({
          type: LOADING_POST_STOP,
          payload: false
        });
        dispatch({
          type: EDIT_PROJECT_NEWS_DATA,
          payload: response.data
        });

        dispatch(initialize('edit_report_project', response.data));
      }
    })
    .catch(function (error) {
      dispatch({
        type: LOADING_POST_STOP,
        payload: false
      });
      alert(error.response.data);
    });
  };
};




export const editNewProject = (data,project_id) => {

  return (dispatch) => {

    dispatch({
      type: LOADING_POST_START,
      payload: true
    });

    axios.put(ROOT_URL+`v2/user/ReportDaily/EditReportDaily` , data ,  config)
    .then(response =>{
      if (response) {
        dispatch({
          type: LOADING_POST_STOP,
          payload: false
        });

        History.push('/my-page/statistic/'+project_id);
      }
    })
    .catch(function (error) {
      dispatch({
        type: LOADING_POST_STOP,
        payload: false
      });
      if (error.response.data.status === 'Expired token') {
        localStorage.removeItem('token')


        setTimeout(function () {
          History.push('/signin');
        }, 1000);
        dispatch({ type: UNAUTH_USER });
      } else if(error.response.status === 401) {
        History.push('/unauthorized');
      }
      else if(error.response.status === 400) {
        alert(error.response.data.status);

      }
      else if(error.response.status === 404) {
        History.push('/notFoundPage');
      }
      else {
        alert(error.response.data);
      }
    });
  };
};
