import {
  UPLOAD_IMAGE,UPLOAD_IMAGE_PROGESS,DISABLE,UPLOAD_IMAGE_PROGESS_RESET
} from '../actions/types';

export const reducer = (state = {data:null,disable:false , progess:0}, action) => {
  switch (action.type) {
    case UPLOAD_IMAGE:
      return state = { ...state, data: action.payload}
    case UPLOAD_IMAGE_PROGESS:
      if (action.payload === 100) {
        return state = { ...state, progess: action.payload,disable:false}
      }else {
        return state = { ...state, progess: action.payload}
      }
    case UPLOAD_IMAGE_PROGESS_RESET:
    return state = { ...state, progess: action.payload } ;
    case DISABLE:
      return state = {...state,disable:true}
    default:
      return state;
  }
};
