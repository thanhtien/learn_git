import {
  LIST_NOTICATION_PAGE,
  NOTI_LIST,
  READ_NOTI
} from '../actions/types';
export const reducer = (state = { notification:{data:null,pageLoad:0} }, action) => {
  switch (action.type) {
    case LIST_NOTICATION_PAGE:
      return state = { ...state, notification: {data:action.payload , pageLoad:action.page}  }
    case NOTI_LIST:
      return {
        ...state,
        notificationHeader:action.payload
      }
    case READ_NOTI:

      if (state.notificationHeader.data) {
        for (var i = 0; i < state.notificationHeader.data.length; i++) {
          if (action.payload === state.notificationHeader.data[i].id) {
            state.notificationHeader.data[i].status = '1'
          }
        }
      }

      return {
        ...state,
        notificationHeader:{
          ...state.notificationHeader
        }
      }
    default:
      return state
  }
}
