import {
    AUTH_USER,
    AUTH_ERROR,
    SEND_EMAIL_FORGOT,
    SEND_EMAIL_FORGOT_FAILE,
    SEND_RESET,
    SEND_RESET_FAILE,
    SEND_SUB_EMAIL,
    SEND_SUB_EMAIL_FAILE,
    TOKEN_OK,
    TOKEN_FAILE,
    UNAUTH_USER,
  

} from '../actions/types';

export const reducer = (state = {   }, action) => {
  switch (action.type) {
    case AUTH_USER:
      return { ...state, error: '', authenticated: true }

    case UNAUTH_USER:
      return { ...state, error: '', authenticated: false }
    case SEND_EMAIL_FORGOT:
      return { ...state, sendEmail: action.payload ,sendEmailError:null}
    case SEND_EMAIL_FORGOT_FAILE:
      return { ...state, sendEmailError: action.payload , sendEmail:null }
    case SEND_RESET:
      return { ...state, sendReset: action.payload , sendResetError:null }
    case SEND_RESET_FAILE:
      return { ...state, sendReset: null , sendResetError:action.payload }
    case SEND_SUB_EMAIL:
      return { ...state, sendSubEmail: action.payload , sendSubEmailError:null }
    case SEND_SUB_EMAIL_FAILE:
      return { ...state, sendSubEmailError: action.payload , sendSubEmail:null }
    case TOKEN_OK:
      return { ...state, tokenOk: action.payload , tokenError:null }
    case TOKEN_FAILE:
      return { ...state, tokenOk: null , tokenError:action.payload }
    case AUTH_ERROR:
      return { ...state, error: action.payload }
    default:
      return state;
  }
};
