import {
  FETCH_PROJECT,
  FETCH_PROJECTS_NEW,
  FETCH_PROJECTS_ENOUGH_LIST,
  FETCH_PROJECTS_GOAL_LIST,
  EXPIRED_PAGINATION,
  NEWS_LIST,
  CAT_SLUG_LIST,
  FROM_TYPE_LIST,
  FANCLUB_PAGINATION,
  PROJECT_S_PAGINATION
} from '../actions/types';

export const reducer = (state = {
    data:[],
    newsProject:{data:null,page:0},
    fanclub:{data:null,page:0},
    ProjectExpried:{data:null,page:0},
    ProjectGoal:{data:null,page:0},
    ProjectEnough:{data:null,page:0},
    CatSlugList:{data:null,page:0},
    FromTypeList:{data:null,page:0},

    Project100:{data:null,page:0},
  }, action) => {
  switch (action.type) {
    case FETCH_PROJECT:
      return state = { ...state, data: action.payload, fetchProjectError:null }
    case PROJECT_S_PAGINATION:
      return state = { ...state , Project100:{data:action.payload,page:action.page } }
    case FETCH_PROJECTS_NEW:
      return state = { ...state, newsProject: {data:action.payload,page:action.page}  }
    case FETCH_PROJECTS_ENOUGH_LIST:
      return state = { ...state, ProjectEnough: {data:action.payload ,page:action.page}}
    case FETCH_PROJECTS_GOAL_LIST:
      return state = { ...state, ProjectGoal: {data:action.payload,page:action.page }, }
    case EXPIRED_PAGINATION:
      return state = { ...state, ProjectExpried: {data:action.payload,page:action.page }, }
    case FANCLUB_PAGINATION:
      return state = { ...state, fanclub: {data:action.payload,page:action.page }, }
    case NEWS_LIST:
      return state = { ...state, newsList: action.payload, }
    case CAT_SLUG_LIST:
      return state = { ...state, CatSlugList: {data:action.payload,page:action.page}, }
    case FROM_TYPE_LIST:
          return state = { ...state, FromTypeList: {data:action.payload,page:action.page}, }
    default:
      return state;
  }
};
