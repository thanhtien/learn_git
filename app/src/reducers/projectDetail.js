import {
    PROJECT_DETAIL,
    PROJECT_DETAIL_MYPAGE,
    ADD_COMMENT,
    ADD_REPLY,
    ADD_MORE_COMMENT,
    ADD_MORE_REPLY,
    ADD_MORE_REPORT,
    NEWS_DETAIL_REPORT,
    EDIT_PROJECT_NEWS_DATA,
    FETCH_PROJECTS_NEWS,
    DELETE_COMMENT,
    EDIT_COMMENT,
    DELETE_REPLY,
    EDIT_REPLY


} from '../actions/types';


export const reducer = (state = {projectDetail:null,projectDetailMypage:null,comment:null,newsProjectList:{data:[],page:0}}, action) => {
    switch (action.type) {
        
        case PROJECT_DETAIL:
          return { ...state, projectDetail: action.payload}
        case FETCH_PROJECTS_NEWS:

          return { ...state, newsProjectList: {data:action.payload,page:action.page}}

        case EDIT_PROJECT_NEWS_DATA:
          return { ...state, dataEdit: action.payload}

        case NEWS_DETAIL_REPORT:
          return { ...state, projectReportDetail: action.payload}

        case ADD_MORE_REPORT:

          return {...state , projectDetail:{
            ...state.projectDetail,
            report:{
              data:state.projectDetail.report.data.concat(action.payload.report),
              totalPage:action.payload.totalPage
            }
          } }
        case PROJECT_DETAIL_MYPAGE:
          return { ...state, projectDetailMypage: action.payload}
        case ADD_COMMENT:

          return {
            ...state ,
            projectDetail: {
              ...state.projectDetail,
              comment:{
                ...state.projectDetail.comment,
                data:state.projectDetail.comment.data.concat(action.payload)
              }
            }

        }
        case ADD_REPLY:
          for (let i = 0; i < state.projectDetail.comment.data.length; i++) {
            if(Number(action.payload.comment_id) === Number(state.projectDetail.comment.data[i].id)) {
              state.projectDetail.comment.data[i].reply = state.projectDetail.comment.data[i].reply.concat(action.payload);

               state = {
                ...state ,
                projectDetail: {
                  ...state.projectDetail,
                  comment:{
                    ...state.projectDetail.comment
                  }
                }
              }
            }
          }
          return state;
        case ADD_MORE_COMMENT:
        state = {
          ...state ,
          projectDetail: {
            ...state.projectDetail,
            comment:{
              ...state.projectDetail.comment,
              data:action.payload.data.concat(state.projectDetail.comment.data),
              page_count:action.payload.page_count
            }
          }

        }
        return state;


        case ADD_MORE_REPLY:

        for (let i = 0; i < state.projectDetail.comment.data.length; i++) {
          if(Number(action.payload.comment_id) === Number(state.projectDetail.comment.data[i].id)) {
            state.projectDetail.comment.data[i].reply = action.payload.reply;

            state = {
              ...state ,
              projectDetail: {
                ...state.projectDetail,
                comment:{
                  ...state.projectDetail.comment
                }
              }
            }
          }
        }
        return state;


        case DELETE_COMMENT:

        state.projectDetail.comment.data.map((item,i)=>{
          if (action.payload === item.id) {
            state.projectDetail.comment.data.splice(i,1);
            return false;
          }
          return false;
        })
        return {
          ...state ,
          projectDetail:{
            ...state.projectDetail,
            comment:{
              ...state.projectDetail.comment
            }
          }
        }


        case DELETE_REPLY:

        state.projectDetail.comment.data.map((item,i)=>{
          if (action.commnetId === item.id) {
            item.reply.map( (reply,j) =>  {
              if (reply.id === action.replyId) {
                state.projectDetail.comment.data[i].reply.splice(j,1);

              }
              return false;
            })
          }
          return false;
        })
        return {
          ...state ,
          projectDetail:{
            ...state.projectDetail,
            comment:{
              ...state.projectDetail.comment
            }
          }
        }


        case EDIT_COMMENT:
        var index;
        state.projectDetail.comment.data.map((item,i)=>{
          if (action.payload.data.id === item.id) {
            return index = i;
          }
          return false;
        })
        var replyStam = state.projectDetail.comment.data[index].reply;
        state.projectDetail.comment.data[index] = action.payload.data;
        state.projectDetail.comment.data[index].reply = replyStam;

        return {
          ...state,
          projectDetail:{
            ...state.projectDetail,
            comment:{
              ...state.projectDetail.comment,
            }
          }
        }

        case EDIT_REPLY :

        state.projectDetail.comment.data.map((item,i)=>{
          if (action.payload.data.comment_id === item.id) {
            item.reply.map( (reply,j) =>  {
              if (reply.id === action.payload.data.id) {
                state.projectDetail.comment.data[i].reply[j] = action.payload.data;
              }
              return false;
            })
          }
          return false;
        })

        return {
          ...state,
          projectDetail:{
            ...state.projectDetail,
            comment:{
              ...state.projectDetail.comment,
            }
          }
        }

        default:
            return state;
    }
};
