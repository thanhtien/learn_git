import {
  LIST_EDIT_PUBLIC
} from '../actions/types';
export const reducer = (state = { PublicEditProjectList:{data:null , page: 0} }, action) => {
  switch (action.type) {
    case LIST_EDIT_PUBLIC:
      return state = { ...state, PublicEditProjectList: {data:action.payload,page:action.page}  }

    default:
      return state
  }
}
