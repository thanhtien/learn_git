import {
  FOOTER_CAT,
  LOADING_POST_STOP,
  LOADING_POST_START,
  SEARCH_FORM,
  HEADER_AUTH,
  EDIT_AUTH,
  UPDATE_SEARCH_TEXT,
  GET_PROJECT_POST,
  GET_PROJECT_DONATED,
  GET_PROJECT_DONATED_FAN,
  DIS_STATUS_API,
  GET_PROJECT_DEFAULT_EDIT,
  EDIT_PROJECT_OK,
  CONFIRM_EDIT_PROJECT_OK,
  EDIT_PROJECT_NO_CONFIRM_OK,
  ADD_NEW_PROJECT_OK,
  CONFIRM_OK,
  CREDIT_MONTH_YEAR,
  GET_STATICTIS_OK,
  SETTING_ATM_OK,
  SETTING_VISA_OK,
  PASSWORD_CHANGE_OK,
  CONTACT_FORM,
  NEW_DETAIL,
  NEW_DETAIL_MORE,
  EDIT_AUTH_DONATE,
  SUB_EMAIL,
  PAGEVIEW_REPORT,
  DONATE_REPORT,
  DONATE_REPORT_TOTAL,
  CHANGE_NUMBER_NOTI,
  CHANGE_NUMBER_NOTI_RECIVE,
  LEAVE_FAN,
  LEAVE_FAN_PROJECT_DETAIL,
  REMOVE_VISA_OK,
  SETTING_VISA_NON_LOGIN,
  CONFIRM_RETURN,
  ADD_WISH_LIST,
  LEAVE_WISH_LIST,
  LIST_NOTICATION_STATIC,
  LIST_NOTICATION_HEADER,
  DETAIL_NOTICATION_HEADER, NEWS_LIST_MONTH

} from '../actions/types';

export const reducer = (state = {
    add_new_status:false ,
    contact_form_status:false ,
    searchResult:[] ,
    searchText:{} ,
    profile:null,
    loading:false ,
    tab:1 ,
    tabName:'リターン作成' ,
    postProject:{data:null,page:0} ,
    subsidized_project:{data:null,page:0} ,
    subsidized_project_fan:{data:null,page:0} ,
    edit_new_status:false
  }, action) => {

    switch (action.type) {

      case LIST_NOTICATION_HEADER:
      return {
        ...state,
        notificationListAdminHeader:action.payload
      }
      case DETAIL_NOTICATION_HEADER:
      return {
        ...state,
        notificationListAdminHeaderDetail:action.payload
      }



      case LEAVE_FAN_PROJECT_DETAIL:
      var arrayFan = state.profile.listFanClub.split(",");
      for (var i = 0; i < arrayFan.length; i++) {

        if (arrayFan[i] === action.payload) {
          arrayFan.splice(i,1);
        }
      }


      return {
        ...state,
        profile:{
          ...state.profile,
          listFanClub:arrayFan.toString()

        }
      }
      case ADD_WISH_LIST:
      if (state.profile.wish_list !== '') {
        state.profile.wish_list = state.profile.wish_list+`,${action.payload.project_id}`;
      }else {
        state.profile.wish_list = `${action.payload.project_id}`
      }
      return {
        ...state,
        profile:{
          ...state.profile,
          wish_list:state.profile.wish_list

        }
      }
      case LEAVE_WISH_LIST:

      if (state.profile.wish_list === '') {
        state.profile.wish_list = '';
      }else {
        var check = state.profile.wish_list.split(",");
        var index = check.indexOf(action.payload);
        check.splice(index,1);
      }
      return {
        ...state,
        profile:{
          ...state.profile,
          wish_list:check.toString()
        }
      }


        case CHANGE_NUMBER_NOTI:
          return {
            ...state,
            profile:{
              ...state.profile,
              number_notification:0
            }
          }

        case CHANGE_NUMBER_NOTI_RECIVE:
          return {
            ...state,
            profile:{
              ...state.profile,
              number_notification:state.profile.number_notification + 1
            }
          }

        case PAGEVIEW_REPORT:
          return {
            ...state,pageView:action.payload
          }
        case DONATE_REPORT:
          return {
            ...state,donateChart:action.payload
          }
        case DONATE_REPORT_TOTAL:
          return {
            ...state,donateChartTotal:action.payload
          }
        case SETTING_VISA_NON_LOGIN:

          return {
            ...state,visa_info_nonlogin:action.payload
          }

        case ADD_NEW_PROJECT_OK:
        return {
          ...state ,
          add_new_status:true,
          projectInfor:action.payload.data.post_project[0],
          postProject:{
            data:action.payload.data,
            page:0
          },
          tab:1,
          profile:{
            ...state.profile,
            numberProject:action.payload.data.post_project.length
          }
        };
        case SUB_EMAIL:
          return {
            ...state,
            profile:{
              ...state.profile,
              statusUnsubscribe:action.payload.statusUnsubscribe,
              unsubscribeProject:action.payload.unsubscribeProject
            }
          }
        case CREDIT_MONTH_YEAR:
          return {...state,errorMonthYear:action.payload}
        case GET_STATICTIS_OK:
          return {...state , statistic_project:action.payload }
        case LIST_NOTICATION_STATIC:
          return {...state , listNotification:action.payload }
        case CONFIRM_RETURN:

          return {
            ...state,
            statistic_project:{
              ...state.statistic_project,
              backed:state.statistic_project.backed.map( (item,i) => {
                if (item.id === action.payload) {
                  item.sendPackage = "1";
                }
                return item;
              })
            }
          }
        case NEW_DETAIL:
          return { ...state, new_detail: action.payload}
      case NEW_DETAIL_MORE:
        return { ...state, new_detail_more: action.payload}
      case NEWS_LIST_MONTH:
        return { ...state, news_list_month: action.payload}
        case CONFIRM_OK:
          return {...state , add_new_status:false , projectInfor:null}
        case "CONFIRM_OK_CANCEL":
          return {...state , add_new_status:false , projectInfor:null}
        case 'ChangeTab':
          return {...state , tab:action.payload , tabName:action.tabName}
        case FOOTER_CAT:
          return { ...state, Categories: action.payload}
        case HEADER_AUTH:
          return {...state,profile:action.payload}
        case EDIT_AUTH:
          return {...state,profile:action.payload,statusAPI:true}
        case EDIT_AUTH_DONATE:
          return {...state,profile:action.payload}
          case SETTING_ATM_OK:
            return {...state,statusAPI:true}
          case SETTING_VISA_OK:
            return {...state,statusAPI:true,}
          case REMOVE_VISA_OK:
          state = {
            ...state ,
            statusAPI:true ,
            resetBank:true,
            profile:{
              ...state.profile,
              number_card:"",
              cvc:"",
              exp_month:"",
              exp_year:""
            }
          }

            return state;
          case PASSWORD_CHANGE_OK:
            return {...state,statusAPI:true}
          case CONTACT_FORM:
            return {...state,contact_form_status:action.payload}

        case DIS_STATUS_API:
          return {...state,statusAPI:false}

        case LOADING_POST_STOP:
          return { ...state, loading: action.payload}
        case LOADING_POST_START:
          return { ...state, loading: action.payload}
        case SEARCH_FORM:
          return { ...state, searchResult: action.payload,searchText:action.searchText}
        case UPDATE_SEARCH_TEXT:
          return { ...state,searchText:action.searchText}
        case GET_PROJECT_POST:
          return { ...state,postProject:{data:action.payload,page:action.page}}
        case GET_PROJECT_DONATED:
          return { ...state,subsidized_project:{data:action.payload,page:action.page} }
        case GET_PROJECT_DONATED_FAN:
          return { ...state,subsidized_project_fan:{data:action.payload,page:action.page} }

        case LEAVE_FAN:

          return {
            ...state,
            subsidized_project_fan:{
              ...state.subsidized_project_fan,
              data:{
                ...state.subsidized_project_fan.data,
                data:state.subsidized_project_fan.data.data.map( (item,i) => {
                  if (item.id === action.payload.id) {
                    item.status_join_fanclub = "1"
                  }
                  return item
                })
              }


            }
          }
        case GET_PROJECT_DEFAULT_EDIT:
          return {...state , project_default_edit:action.payload}
        case EDIT_PROJECT_OK:

          var dataProjectUpdate = {
            post_project:action.payload.post_project,
            page_count:action.payload.page_count
          };
          return {
            ...state ,
            projectInforEdit:action.payload.project ,
            postProject:{
              data:dataProjectUpdate,
              page:0
            },
            edit_new_status:true,
            tab:1,
            profile:{
              ...state.profile,
              numberProject:action.payload.post_project.length
            }
          };

        case EDIT_PROJECT_NO_CONFIRM_OK:
          dataProjectUpdate = {
            post_project:action.payload.post_project,
            page_count:action.payload.page_count
          };
          return {
            ...state ,
            postProject:{
              data:dataProjectUpdate,
              page:0
            },
            tab:1,
            edit_new_status_flast:action.status,
            projectInforEdit:action.payload ,
            profile:{
              ...state.profile,
              numberProject:action.payload.post_project.length
            }

          }

        case CONFIRM_EDIT_PROJECT_OK:
          return {...state , edit_new_status:false }



        default:
          return state;
    }
};
